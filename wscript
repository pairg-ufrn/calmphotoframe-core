#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path
import sys

import platform
import subprocess

import waflib
import waflib.Tools
import waflib.Tools.compiler_cxx
from waflib import Utils
from waflib.Build import BuildContext
from waflib.Configure import conf
from waflib import Logs

top='.'
out='build'

APPNAME      = 'CalmPhotoFrame-Core'
VERSION_NUMS = ['0', '5', '1']
VERSION      = '.'.join(VERSION_NUMS)

VERSION_NAME = VERSION + "-beta"

def init(ctx):
    from waflib.Options import options
    # Cria variantes da build
    _setupVariant(options.target)

def try_load(ctx, tool, tooldir):
    try:
        ctx.load(tool, tooldir=tooldir);
    except:
        print("Failed to load '%s' tool" % tool)

def load_tools(ctx):
    ctx.load('compiler_c compiler_cxx')
    ctx.load('gnu_dirs')

    ctx.load('conanbuildinfo_waf', tooldir=".");
    ctx.load('profile package target_runner test_command', tooldir="buildtools");
    try_load(ctx, 'run_command', "buildtools")
    try_load(ctx, 'analyze_command', "buildtools")

def options(opt):
    load_tools(opt)

    opt.add_option('--debug', action='store_true', default=True, dest='debug', help='Do debug build')
    opt.add_option('--release', action='store_false', dest='debug', help='Do release build')
    opt.add_option('--shared', action='store_true', default=False, help='Use shared libraries')

    crossOptGroup = opt.add_option_group('cross-compile')
    crossOptGroup.add_option('--target'         , dest='target'         , default=''
                                     , help="name of target to cross-compile. If not set, compile to native."  )
    #crossOptGroup.add_option('--toolchain-path' , dest='toolchain_path' , default='/usr'
    #                                 , help="path to search toolchain")

    opt.add_option('--with-tests', action='store_true', default=False, help='Build tests', dest='with_tests')
    opt.add_option('--profile', action='store', default=None)

def configure(conf):
    configure_store_options(conf)

    load_tools(conf)
    configure_flags(conf)
    configure_static_analysis(conf)
    check_dependencies(conf)

    conf.env.APPNAME = APPNAME
    conf.env.VERSION = VERSION
    conf.env.VERSION_NUMS = VERSION_NUMS
    conf.env.VERSION_NAME = VERSION_NAME

    #Configure subdirectories
    conf.recurse('libs')
    conf.recurse('src')
    conf.recurse('test')

def build(bld):
    bld.recurse('libs')
    bld.recurse('src')
    bld.recurse('test')

    #genDbScheme(bld)

    bld.install_files(bld.env.BINDIR, ['conf.json']);

def test(ctx):
    ctx.run('tests')

def package(bld):
    #if not bld.options.packagedir:
    bld.options.packagedir = bld.srcnode.make_node(path.join("gen", "package")).abspath()
        
    bld.recurse('src')

def dist(ctx):
    '''makes a tarball for redistributing the sources'''

    excl_waf    = ['**/build/**', '**/.waf*/**', '**/.lock-waf*'];
    excl_app    = ['*.db', '*.log', 'photos/**'];
    excl_gen    = ['dist/**', '*.zip', 'gen/**'];
    excl_hidden = ['.*', '.*/'];
    excl        = excl_waf + excl_app + excl_gen + excl_hidden;
    archiveName = "%s_%s_src.zip" % (APPNAME.lower(), VERSION_NAME);

    "Configure variables used by waf when generating the zip file"
    ctx.base_name = '';
    ctx.algo      = 'zip';
    ctx.excl      = excl;
    ctx.arch_name = _getPackageFile(ctx, archiveName);

def release(ctx):
    from waflib import Options
    Options.commands = ['dist', 'package'] + Options.commands
    
    
#################################################################################################

@conf
def lib(bld, *k, **kw):
    if 'install_path' not in kw:
        kw['install_path'] = bld.env.LIBDIR

    if bld.env.shared:
        bld.shlib(*k, **kw)
    else:
        bld.stlib(*k, **kw)

    includedir = kw.get('install_includedir', bld.env.INCLUDEDIR)
    install_headers = kw.get('install_headers', None)

    if install_headers:
        bld.install_files(includedir, Utils.to_list(install_headers), relative_trick=True)

@conf
def get_env(ctx, opt, default=None):
    if opt in ctx.env:
        return ctx.env[opt]
    else:
        return default

@conf
def glob(bld, *k, **kw):
    '''Helper to execute an ant_glob search.
        See documentation at: https://waf.io/apidocs/Node.html?#waflib.Node.Node.ant_glob
    '''

    return bld.path.ant_glob(*k, **kw)

#################################################################################################


#def configure_toolchain(conf):
#    if(conf.env.target == 'android'):
#        toolchain_path = conf.options.toolchain_path
#        print "toolchain path: ", toolchain_path

#        paths = [toolchain_path, os.path.join(toolchain_path,'bin')]

#        conf.find_program('arm-linux-androideabi-gcc', path_list=paths, var='CC')
#        conf.find_program('arm-linux-androideabi-gcc', path_list=paths, var='CC2')
#        conf.find_program('arm-linux-androideabi-g++', path_list=paths, var='CXX')
#        conf.find_program('arm-linux-androideabi-ar' , path_list=paths, var='AR')
#        conf.find_program('arm-linux-androideabi-ld' , path_list=paths, var='LD')

#        #TODO: definir arquitetura
#        conf.env.platformName = 'android-arm';

def configure_store_options(ctx):
        
    ctx.env.target = ctx.options.target
    ctx.env.profile = ctx.options.profile
    ctx.env.shared = ctx.options.shared
    
    if os.environ.get('ANDROID', False):
        Logs.pprint("GREEN", "Building for android")
        ctx.env.target = 'android'
    
    is_cross = (ctx.env.target == "android")
    ctx.env.with_tests = not is_cross
    ctx.env.gen_scheme = not is_cross


def configure_flags(conf):
    conf.env.platformName = sys.platform + _getSystemArchitecture();

    if(conf.env.target == 'android'):
        conf.define("LINUX",1)
        conf.define("ANDROID",1)

    elif(sys.platform.startswith('linux')):
        conf.define("LINUX",1)
        conf.env.platformName = 'linux' + _getSystemArchitecture();

    conf.env.system_libs = ['pthread', 'dl', 'rt']

    for f in ['LDFLAGS', 'CFLAGS', 'CXXFLAGS']:
        conf.env[f] += ['-g', '-O2']

    conf.env['CXXFLAGS'] += ['-std=c++11']

def configure_static_analysis(ctx):
    #static analysis tools
    ctx.env.reportDir = path.join(top,'report')
    ctx.find_program("cccc", mandatory=False)
    ctx.find_program("cppcheck", mandatory=False)

def check_dependencies(conf):
    conf.check(
        header_name='stdio.h', 
        features='c cprogram'
        )
        
    conf.check(
        header_name ='pthread.h', 
        features    ='c cprogram',
        fragment    ='#include <pthread.h> \nint main() { pthread_t threads; return 0; }' 
        )
        
    conf.check(
        header_name ='pthread.h', 
        features    ='c cprogram',
        define      ="HAS_ROBUST_MUTEX",
        fragment    =
            ''' #include <pthread.h>
                int main() {
                    PTHREAD_MUTEX_ROBUST;
                    return 0;
                }
            ''', 
        mandatory=False )
        
    conf.check(
        defines  ='in_port_t=uint16_t', 
        fragment = '#include <arpa/inet.h> \nint main() { in_port_t var; return 0; }\n')

    if conf.env.target == 'android':
        conf.check(header_name='android/log.h', features='c cprogram')


def genDbScheme(bld):
    if not bld.env.gen_scheme:
        return

    bld.run('genscheme ${TGT}', target='dbschema.sql')
    
################################# Auxiliary methods #############################################

def _setupVariant(variantName):
    from waflib.Options import options
    from waflib.Build import BuildContext, CleanContext, InstallContext, UninstallContext
    from waflib.Configure import ConfigurationContext
    for y in (BuildContext, CleanContext, InstallContext, UninstallContext, ConfigurationContext):
        name = y.__name__.replace('Context','').lower()
        class tmp(y):
            cmd = name
            variant = variantName

def find_files(path, pattern):
    return path.ant_glob([pattern], src=True,dir=False)


def _createIfNotExists(path):
    if not os.path.exists(path):
        os.makedirs(path);

def _getVariantInstallPrefix(ctx):
    return ctx.variant if ctx.variant else ".";

def _getPlatformName(ctx):
    return ctx.env.platformName;

def _getSystemArchitecture():
    ''' platform.architecture()[0] contains a value like 32bit or 64bit.
        removes the "bit" part.
    '''
    return platform.architecture()[0].translate(None, "bit");

def _getPackageFile(ctx, filename):
    packDir     = ctx.options.packagedir;

    if(packDir):
        _createIfNotExists(packDir);
    return path.join(packDir, filename) if packDir else filename;
