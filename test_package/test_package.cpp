#include <iostream>
#include <assert.h>

#include "PhotoFrameService.h"
#include "Configurations.h"
#include "Options.h"
#include "utils/ScopedDatabase.h"

using namespace std;
using namespace calmframe;

#define ASSERT(condition, msg) assert(condition && msg)

int main(){

    cout << "************ Running test ***********" << endl;
    
    {
        ScopedSqliteDatabase scopedDb{ScopedSqliteDatabase::DbType::InMemory};
        PhotoFrameService service;
        
        cout << "starting service" << endl;
        
        ASSERT(service.startService(), "Failed to start service");
        
        service.stopService();
        cout << "finished service " << endl;
    }

    cout << "**************************************" << endl;

    return 0;
}
