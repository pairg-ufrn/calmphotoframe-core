#! /usr/bin/env python
# encoding: utf-8
# Paulo Brizolara, 2015

"""
Performance Profile Tool - profiles C/C++ executables and render the callgraph

this tool uses the 'callgrind' tool from 'valgrind' to profile executable runs
and uses gprof2dot (https://github.com/jrfonseca/gprof2dot) and
dot(http://graphviz.org/) programs to convert the generated callgraph to an image
(support for other profiling tools could be added in the future)

this tool can be used in two ways:
    1. Setting the 'profile' feature into an executable target.
        Ex.:
            bld.program(source="main.cpp", target="program", features=["profile"])
        this way, after the build, the generated target (in this case 'program') will be profiled

    2. Calling the 'profile' method binded to build context, passing the executable commands to profile.
        Ex.:
            bld.profile("./program1 --arg1=1 --arg2=2"
                        , "./subdir/program2 otherargs"
                        , cwd="/optional/path/where/the/executable/will/be/runned")

        the executables are searched from the current build path (absolute paths can be used also)

    The (optional) 'cwd' param can be set to determine the path where the executables to profile will be run.
    The default 'cwd' path is the source directory (the top directory)

to execute the profiler, use the 'profile' command, when executing waf.
Ex.:
        ./waf build profile
"""

import waflib

from waflib.TaskGen import feature, extension
from waflib.Configure import conf

from waflib import TaskGen
from waflib import Task
from waflib.Build import BuildContext

from waflib.Utils import to_list

from waflib import Logs

import os
from os import path

class ProfileBuild(BuildContext):
    """Specify the command to execute the profiler"""
    cmd = 'profile'
    fun = 'build'

def options(opt):
    #TODO: create option group
    opt.add_option("--profile-variants"
                        , action="append", default=[]
                        , help="specify in which variant builds the profiler is executed. Default is execute in all variants")

    opt.add_option("--profile-image-format", default="png"
                        , help="specify the output format to render the profile graph. See 'dot' documentation to allowed formats")
    opt.add_option("--profile-image-ext", default=None
                        , help="set explicitly the output extension used to the render the graph." \
                                + "If not set, infer the extension from the image format")

    opt.add_option("--profiler-options", default=[]
                        , help="specific profiler options")

def configure(conf):
    conf.find_program('valgrind', var='PROFILER', mandatory=False) #Todo: allow other profile tools
    envProfiler = conf.env.PROFILER

    if(not envProfiler):
        Logs.warn("Could not find profiler program. Install valgrind to use this tool.")
        return

    profiler = envProfiler if isinstance(envProfiler, basestring) else envProfiler[0]

    conf.env["PROFILER_CMD"]         = profiler + " --tool=callgrind";
    conf.env["PROFILER_ARGS"]        = "-q"
    conf.env["PROFILER_OUT_OPTION"]  = "--callgrind-out-file"

    conf.find_program("gprof2dot", var="GPROF2DOT", mandatory=False)
    conf.find_program("dot", var="DOT", mandatory=False)
    conf.env["PROFILER_VIEW_INPUTTYPE"]  = "callgrind"
    conf.env["PROFILER_VIEW_ARGS"]       = "--strip"

    opts = conf.options

    conf.env["PROFILER_EXTRA_OPTIONS"] = opts.profiler_options

    conf.env["PROFILE_IMAGE_FORMAT"]     = opts.profile_image_format
    ext = opts.profile_image_format if not opts.profile_image_ext else opts.profile_image_ext
    conf.env["PROFILE_IMAGE_EXT"]        = "." + ext

@conf
def profile(self, *k, **kw):
    """ Prepare the executable targets to run with the profiler.
        Use like:
            bld.profile("<exec1> <exec1-args>", "<exec2> <exec2-args>")
    """
    source = kw['source'] if 'source' in kw else []
    source += k

    # Use custom parameter 'execs' to avoid waf checks for extensions in source
    kw['execs'] = source
    kw['features'] = 'profilerun'
    #kw['scan'] = _scanFromExecFile

    return self(*k, **kw)


@conf
def profileFromFile(self, *k, **kw):
    kw['features'] = kw.get('features', []) + ['profile_from_file', 'profilerun']

    tskGen = self(*k, **kw)
    tskGen.mappings[''] = conf_profileFromFile

#@feature('profile_from_file')
#@extension('')
@TaskGen.before_method('conf_profileRun')
def conf_profileFromFile(self, node):
    print "conf_profileFromFile: ", node
    files = self.source

    print "source: ", files

    execs = []
    for f in files:
        execs += extract_execs(f.abspath())

    self.execs = execs
    conf_profileRun(self)

def extract_execs(exec_file):
    execs = []
    with open(exec_file) as f:
        for line in f.readlines():
            execs.append(line.strip()) #remove new line (and other blank characters)

    return execs

@feature('profilerun')
@TaskGen.after_method('apply_link')
def conf_profileRun(self):
    """Generate tasks for execute profiler"""
    print "conf_profileRun(%s)" % vars(self)

    #if(not _isProfileEnabled(self)):
    #    return


    cwd = getattr(self, 'cwd', None)
    scan = getattr(self, "scan", None)
    execs = getattr(self, "execs", [])
    _makeProfileRuns(self, execs, cwd=cwd, scan=scan)


@feature('profile')
@TaskGen.after_method('apply_link')
def conf_profileExec(self):
    """Create profiler tasks for targets with feature 'profile'
        Example:
            For
                bld.program(source="main.cpp", target="main", features=['profile'])

            execute profiler on target 'main'
    """
    if(not _isProfileEnabled(self) or not getattr(self, "link_task", None)):
        return

    executable = self.link_task.outputs[0]
    target     = self.path.get_bld().find_or_declare(self.target + ".profile")

    createProfilerChain(self, [executable], target)

def _makeProfileRuns(taskGen, executables, **args):
    execCommands = _splitExecutables(executables)
    sources = []
    for cmd in execCommands:
        nodeExec = _makeNode(taskGen.bld, cmd[0])
        sources.append( (nodeExec, cmd[1]) )

    targets = _makeTargets(taskGen, sources)

    for i in range(len(sources)):
        createProfilerChain(taskGen, sources[i], targets[i], **args)

def createProfilerChain(self, source, profileFile, **args):
    runTaskGen = self.bld(
        rule    = '${PROFILER_CMD} ${PROFILER_ARGS} ${PROFILER_EXTRA_OPTIONS}'\
                    + ' ${PROFILER_OUT_OPTION}=${TGT[0].abspath()}'\
                    + ' ${SRC[0].abspath()} ${RUN_ARGS}',
        color   = 'BLUE',
        cwd     = args.get('cwd', None),
        ext_out = ['.profile'],
        source = source[:1],
        target = profileFile,
        shell = True
    )
    runTaskGen.env['RUN_ARGS'] = source[1:]

    if not self.bld.env.GPROF2DOT:
        return

    self.bld(source=profileFile)

    waflib.TaskGen.declare_chain(
            name='Profile2DotTask'
            , rule='cat ${SRC[0].abspath()} | ${GPROF2DOT} -f ${PROFILER_VIEW_INPUTTYPE} ${PROFILER_VIEW_ARGS} -o ${TGT}'
            , shell=True
            , ext_in=['.profile']
            , ext_out=[".dot"]
            , after="RunProfilerTask")

    if self.bld.env.DOT:
        waflib.TaskGen.declare_chain(
                name='Dot2Png'
                , rule='${DOT} -T${PROFILE_IMAGE_FORMAT} -o ${TGT} ${SRC[0].abspath()}'
                , shell=True
                , ext_in=['.dot']
                , ext_out=[self.bld.env.PROFILE_IMAGE_EXT]
                , after="Profile2DotTask")


def _isProfileEnabled(taskGen):
    profile_variants = taskGen.bld.options.profile_variants
    isRightVariant = (len(profile_variants) == 0 or (taskGen.bld.variant in profile_variants))

    isProfileCommand = (ProfileBuild.cmd == taskGen.bld.cmd)

    return isRightVariant and isProfileCommand

def _makeTargets(taskGen, sources):
    ''' sources should be a list of pairs of <executable node> and <args>'''
    targets = []

    bldDir = taskGen.path.get_bld().abspath()
    for s in sources:
        sourcePath = s[0].abspath()

        relPath = os.path.relpath(sourcePath, bldDir)
        relPath += s[1]

        path = _change_ext(_convertIllegalPathChars(relPath), ".profile")

        tgt = taskGen.path.get_bld().find_or_declare(path)
        targets.append(tgt)

    return targets

def _splitExecutables(executables):
    '''Turn the list of executable commands into a list of pairs, in which is expected
        that the first item is the executable command and the second item the arguments'''
    import shlex

    execPairs = []
    for e in executables:
        #if is list, keep as list, else split in white spaces (?)
        parts = waflib.Utils.to_list(e)
        #join the arguments as 1 string
        args = " ".join(parts[1:])

        pairTuple = (parts[0], args)
        execPairs.append(pairTuple)

    return execPairs


def _makeNodes(taskGen, paths):
    nodes = []
    for p in paths:
        nodes.append(_makeNode(taskGen.bld, p))
    return nodes

def _makeNode(ctx, path, parentNode=None):
    '''Try to turn path into an waf Node'''
    if isinstance(path, waflib.Node.Node):
        return

    import os.path

    if os.path.isabs(path):
        return ctx.root.make_node(path)
    elif parentNode:
        return parentNode.make_node(path)
    else:
        return ctx.path.make_node(path)

import string
def _convertIllegalPathChars(path)  :
    translateTable = string.maketrans("/.\":()" , "-_    ")
    return str(path).translate(translateTable).replace(" ", "")

def _change_ext(p, newExt):
    base, ext = os.path.splitext(p)
    return base+newExt

def _change_node_ext(taskGen, node, newExt):
    path = _change_ext(str(node), newExt)
    return taskGen.path.get_bld().find_or_declare(path)
