#!/usr/bin/env python
# -*- coding: utf-8 -*-

############################### Test Command ###################################

import waflib
from waflib.Build import BuildContext

class TestCommand(BuildContext):
    cmd="test"
    fun="build"

#    def execute(self):
#        """
#        Restore the data from previous builds and call :py:meth:`waflib.Build.BuildContext.execute_build`. Overrides from :py:func:`waflib.Context.Context.execute`
#        """
#        self.restore()
#        if not self.all_envs:
#                self.load_envs()

#        self._recurse_test()

#        self.execute_build()
#        #tests must run after build
#        #self.add_group()

    def execute(self):
        def call_recurse(ctx):
            ctx._recurse_test()
        self.add_pre_fun(call_recurse)

        super(TestCommand, self).execute()

    def _recurse_test(self):
        initFun = self.fun
        try:
            self.fun = 'test'
            self.recurse([self.run_dir], "test")
        except waflib.Errors.WafError, e:
            print e
            print "not found test cmd"
        finally:
            self.fun = initFun
