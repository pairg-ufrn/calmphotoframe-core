#!/usr/bin/env python
# -*- coding: utf-8 -*-

############################## Profile #################################

import waflib

def _createVariant(variantName):
    from waflib.Build import BuildContext, CleanContext, InstallContext, UninstallContext
    from waflib.Configure import ConfigurationContext
    for y in (BuildContext, CleanContext, InstallContext, UninstallContext, ConfigurationContext):
        name = y.__name__.replace('Context','').lower()
        class tmp(y):
            cmd = name
            variant = variantName


from waflib.TaskGen import feature
from waflib.Configure import conf

def configure(conf):
    print("conf: ", dir(conf));
    print("configure profile for variant: ", conf.variant);


from waflib.Options import options
from waflib.Utils import to_list
from waflib import TaskGen
@feature('profile')
@TaskGen.before('process_rule')
def conf_profile(self):
    print("variant:", self.bld.variant);

    _appendFlag(self, 'cflags', '-pg')
    _appendFlag(self, 'cxxflags', '-pg')
    _appendFlag(self, 'linkflags', '-pg')

    print("cflags: ", getattr(self, 'cflags', []));
    print("cxxflags: ", getattr(self, 'cxxflags', []));
    print("linkflags: ", getattr(self, 'linkflags', []));

def _appendFlag(self, flagType, flag):
    flags = to_list(getattr(self, flagType, []));
    flags.append(flag);
    setattr(self, flagType, flags);

from waflib.Build import BuildContext
class ProfileBuild(BuildContext):
    cmd = 'profile'
    fun = 'build'
    variant = "profile"

def build(bld):
    print("profile build");
#    print(bld.cmd)
