# Visualize a source code's relative size and complexity using output from CCCC and
# Google's Treemap api.
#
# CCCC
#   http://superb-dca3.dl.sourceforge.net/project/cccc/cccc/3.1.4/cccc-3.1.4.tar.gz
#
# Google treemap
#   https://developers.google.com/chart/interactive/docs/gallery/treemap
#
# Expects a file cccc.xml in the current directory as well as the referenced xml files
# containing the information for the modules listed in cccc.xml. The default location for
# the cccc output is .cccc in the directory it is run from.
#
# Example Use:
# Run cccc
# > find <your source tree> -print0 | xargs -0 cccc
# Get to its output
# > cd .cccc
# Create the html file
# > python <path to this script>/CreateTreeMap.py > MySourceTreemap.html
#
# Now open MySourceTreemap.html in your browser. Tested on OS X 10.6 with Chrome and Safari.
#
 
import xml.etree.ElementTree as ElementTree
 
 
# name - the name of the data point, either a module/class or function
# parent - the module or class name of the function
# linesOfCode - controls size,
# complexity - controls color of the region
def PrintNodeData(name, parent, linesOfCode, complexity):
    if 'root' == parent:
        print "           ['%s', %12s, %12s, %12s]" % (name, "'" + parent + "'", linesOfCode, complexity),
    else:
        # To keep IDs unique preprend the class name to the function name
        print "           ['%s::%s', %12s, %12s, %12s]" % (parent, name, "'" + parent + "'", linesOfCode, complexity),
 
 
def PrintTreeMapModuleData(parent, xmlFile):
    moduleTree = ElementTree.parse(xmlFile)
    if None == moduleTree:
        return
    moduleRoot = moduleTree.getroot()
    functions = moduleRoot.find('procedural_detail').findall('member_function')
    for functionNode in functions:
        name = functionNode.find('name').text
        valueTag = 'value'
# Level doesn't work as well even though it's a reduced range because most are zero
#        valueTag = 'level'
        linesOfCode = functionNode.find('lines_of_code').get(valueTag)
        complexity = functionNode.find('McCabes_cyclomatic_complexity').get(valueTag)
        print ','
        PrintNodeData(name, parent, linesOfCode, complexity)
 
 
def PrintTreeMapData(parent, rootXml):
    rootTree = ElementTree.parse(rootXml)
    root = rootTree.getroot()
    modules = root.find('procedural_summary').findall('module')
    for module in modules:
        name = module.find('name').text
        # Being tricky here so that the final line doesn't have a comma
        print ","
        PrintNodeData(name, parent, '0', '0')
 
    for module in modules:
        name = module.find('name').text
        if name != 'anonymous':
            PrintTreeMapModuleData(name, name + ".xml")
   
 
# Start the HTML and Javascript code
print '''
<html>
 <head>
   <script type="text/javascript" src="https://www.google.com/jsapi"></script>
   <script type="text/javascript">
     google.load("visualization", "1", {packages:["treemap"]});
     google.setOnLoadCallback(drawChart);
     function drawChart() {
       // Create and populate the data table.
       var data = google.visualization.arrayToDataTable([
         ['Module', 'Parent', 'Lines of code (size)', 'level (color)'],
         ['root',   null,               0,                    0]''',
 
# Print the table entries based on the cccc XML output.
# Each class will have parent 'root'. Each function will have the class name as
# a parent.
PrintTreeMapData('root', 'cccc.xml')
 
# Finish the HTML and Javascript
print '''
       ]);
 
       // Create and draw the visualization.
       var tree = new google.visualization.TreeMap(document.getElementById('chart_div'));
       tree.draw(data, {
         maxDepth: 2,
         minColor: 'YellowGreen',
         midColor: 'LightGoldenRodYellow',
         maxColor: 'Red',
         headerHeight: 15,
         fontColor: 'black',
         showScale: true});
       }
   </script>
 </head>
 
 <body>
   <div id="chart_div" style="width: 900px; height: 500px;"></div>
 </body>
</html>
'''


