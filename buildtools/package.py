#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Paulo Brizolara, 2015
# Based on example from waf playground (playground/package )

"""
Create tarball(s) from the build results.
The package command aggregates the functionality of build, install and dist in a single command.

On build script, specify the files that will be installed:

    prefix = ...
    bld.program(..., install_path= os.path.join(prefix, "bin") )
    bld.install_file(..., install_path= os.path.join(prefix, "bin"))
    bld.install_file("somefile.txt", install_path= prefix)
    ...

then, on 'build' or 'package' command, specify the files to be zipped on each package.
    - target will be the name of the package
    - files is a list of regexes that define the files that will be copied from the installation
        to the packaged file (using the same syntax of waf 'ant_glob').
        ATTENTION: the file regexes are relative to the installation dir (vide install_path)
    - base_path is a path relative to the installation dir that will be used as the base to
        the packaged files.

    bld.package(target="some_package", files=["somefile.txt", "bin/*"], base_path="bin")

finally, on command line run:

    $ waf configure package

or simply:

    $ waf package

"""

import os
from os import path
import shutil
import platform

import waflib
from waflib import Build
from waflib.Configure import conf
from waflib import Logs

def options(opt):
    packageGroup = opt.add_option_group('package options')
    packageGroup.add_option('--packagedir'
                            , dest='packagedir'
                            , help="directory where project packages (.zips) are put [default: ${OUT}/package]"  )

    packageGroup.add_option('--package-format'
                            , dest='package_format'
                            , action='store'
                            , default='zip'
                            , help="Compression format to create packages."
                                    + " Valid formats includes: 'zip', 'tar.gz' and 'tar.bz2' ."
                                    + " See waf documentation for dist 'algo' formats."  )
        
@conf
def package(bld, *k, **kw):
    ''' Define a package (zip file) that groups some "installed files".
    
        files       : list of patterns for input files (ex.: ["include/**", "libsample.*"])
        base_path   : path relative to install dir used to search for files. 
        target      : output filename (ex.: my_package.zip).
    '''

    if(not _isPackageCommand(bld)):
        return

    package = Package(bld, **kw);
    
    packages = getattr(bld.env, 'packages', [])
    packages.append(package)
    bld.env.packages = packages
    
    
def _isPackageCommand(bld):
    return (PackageCommand.cmd == bld.cmd)
    

class Package(object):
    
    def __init__(self, ctx, **kw):
        self.name = kw['target']
        self.files = kw['files']
        self.base_path = kw.get('base_path', ".")

        self.format = kw.get('format', ctx.options.package_format)

    def __str__(self):
        template = "{ target: %s, files: %s, base_path: %s, format: %s}" 
        return template % (self.name, self.files, self.base_path, self.format)
        
        
class PackageCommand(Build.InstallContext):
    cmd = 'package'
    fun = 'build'

    LOG_COLOR = 'BLUE'

    def init_dirs(self, *k, **kw):
        super(PackageCommand, self).init_dirs(*k, **kw)

        self.tmp = self.bldnode.make_node('package_tmp_dir')
        try:
            shutil.rmtree(self.tmp.abspath())
        except:
            pass
        if os.path.exists(self.tmp.abspath()):
            self.fatal('Could not remove the temporary directory %r' % self.tmp)
        self.tmp.mkdir()
        self.options.destdir = self.tmp.abspath()

    def execute_build(self, *k, **kw):
        if(hasattr(self, 'tmp')):
            shutil.rmtree(self.tmp.abspath())

        back = self.options.destdir

        try:
            """Explicitly calling 'package' user function, because this
                command needs to call 'build' function, to perform installation"""
                
            # do packages configuration
            self.fun = "package"
            self.recurse([self.run_dir])
        except waflib.Errors.WafError: #Raised if there is no 'package' command
            pass

        try:
            # execute build and installation
            self.fun = 'build'
            super(PackageCommand, self).execute_build(*k, **kw)
        finally:
            self.options.destdir = back

        for pkg in getattr(self.env, 'packages', []):
            self.log("package: " + str(pkg))
            self.log("Prefix: " + str(self.env.PREFIX))
            self._archivePackage(pkg, self.tmp)

        #shutil.rmtree(self.tmp.abspath())

    def log(self, *k, **kw):
        Logs.pprint(self.LOG_COLOR, *k, **kw)

    def _archivePackage(self, package, installDir):
        from waflib import Scripting
        dist = Scripting.Dist()
    
        dist.algo  = package.format
        dist.files = self.findPackageFiles(package, installDir)
    
        # path used as base to create archive (files are saved relative to it)
        # uses the base node
        dist.base_path = self.getBaseNode(package, installDir)
        
        # archive name
        dist.arch_name = self.getPackageName(package)
        
        dist.base_name = ''
        
        # Create the archive file
        dist.archive()
        
        # Get archive location        
        packageFile = dist.base_path.make_node(dist.arch_name).abspath()
        destDir = path.join(self.getPackagePath(), dist.arch_name)
        
        # Move archive to expected dir
        import shutil
        shutil.move(packageFile, destDir)

    def getBaseNode(self, pkg, installDir):
        baseNode = installDir
        
        if pkg.base_path:
            baseNode = installDir.make_node(pkg.base_path)
            
        return baseNode

    def findPackageFiles(self, pkg, installDir):
        baseNode = self.getBaseNode(pkg, installDir)
            
        self.log("find files {} in {}".format(pkg.files, baseNode))    
            
        files = baseNode.ant_glob(pkg.files)
        
        return files 
            
    def getPackageName(self, pkg):
        return pkg.name + "." + pkg.format

    def getPackagePath(self):
        packNode = None
    
        packDir = self.options.packagedir or path.join(self.out_dir, 'package')
        #packNode = self.bldnode.make_node(packDir)
    
        Logs.pprint('PURPLE', "package dir: " + packDir)
            
        self._createIfNotExists(packDir);
            
        return packDir;
    
    def _createIfNotExists(self, path):
        if not os.path.exists(path):
            os.makedirs(path);
