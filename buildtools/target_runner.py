#! /usr/bin/env python
# encoding: utf-8
# Paulo Brizolara, 2015

"""
Target Runner Tool - executes builded targets and other arbitrary commands

To run a target:
    bld.run('targetName --arg1 --arg2', cwd='./path/to/execute/target')
"""

import waflib

from waflib.TaskGen import feature
from waflib.Configure import conf

from waflib import TaskGen
from waflib import Task
from waflib.Build import BuildContext

from waflib.Utils import to_list

from waflib import Logs

import os
from os import path

import sys

def trace(msg):
    Logs.debug("trace: %s" % msg)


@conf
def run(self, *k, **kw):
    """ Prepare the executable targets to run.
        Use like:
            bld.run("<exec1> <exec1-args>", "<exec2> <exec2-args>")
    """
    execs = kw.get('source', [])
    execs += k

    # Use custom parameter 'execs' to avoid waf checks for extensions in source
    kw['execs'] = execs
    if kw.has_key('source'):
        del kw['source']

    kw['runnable_args'] = kw.copy()

    kw['features'] = 'runnable'

    return self(*k, **kw)

@feature('runnable')
@TaskGen.after_method('apply_link')
def process_runnable(self):
    """Generate tasks to execute runnables"""
    trace("process_runnable")

    executables = getattr(self, "execs", [])

    if(getattr(self, "link_task", None) and len(self.link_task.outputs) > 0):
       executables.append(self.link_task.outputs[0])

    kw = extract_args(self)
    genRunTask(self.bld, executables, **kw)

def extract_args(tskGen):
    return tskGen.runnable_args

def genRunTask(ctx, executables, **kw):
    trace("genRunTask")

    executables = [to_list(x) for x in executables]

    tskGens = []
    for e in executables:
        tskGens.append(runExec(ctx, e, **kw))

    return tskGens

def runExec(ctx, executable, **kw):
    trace("runExec")

    cwd = kw.get('cwd', None)
    if(cwd and isinstance(cwd, waflib.Node.Node)):
        kw['cwd'] = cwd.abspath()

    execName = executable[0]
    args = " ".join(executable[1:])

    #Default task name is 'run <execName>'
    if('name' not in kw or not kw['name']):
        kw['name'] = 'run ' + execName

    _process_targets(ctx, kw)

    tskGen = targetTaskGen(ctx, execName, args, **kw)
    if(not tskGen):
        tskGen = programTaskGen(ctx, execName, args, **kw)

    trace("end runExec")

    return tskGen

def _process_targets(ctx, kw):
    trace("process_targets")

    target  = kw.get('target',[])

    if isinstance(target, str):
        target = [target]

    if target: #process the target to fix some problems with paths
        targetNodes = []
        for t in target:
            node = makeNode(ctx, t)
            targetNodes.append(node)
        kw['target'] = targetNodes

def makeNode(ctx, path, parentNode=None):
    '''Try to turn path into an waf Node'''
    if isinstance(path, waflib.Node.Node):
        return path

    import os.path

    if os.path.isabs(path):
        return ctx.root.make_node(path)
    elif parentNode:
        return parentNode.make_node(path)
    else:
        return ctx.path.make_node(path)

def targetTaskGen(ctx, execName, args, **kw):
    targetExec = tryGetTargetExec(ctx, execName)

    if not targetExec:
        return None

    rule = "${SRC[0].abspath()} %s"  % args
    taskGen = ctx(rule=rule, source=targetExec, **kw)

    return taskGen

def programTaskGen(ctx, execName, args, **kw):
    trace("programTaskGen: kw = %s" % str(kw))

    rule = "${EXECUTABLE} %s" % args

    taskGen = ctx(rule=rule, **kw)
    taskGen.env['EXECUTABLE'] = execName

    return taskGen

def tryGetTargetExec(ctx, execName):
    target = None
    try:
        tg = ctx.get_tgen_by_name(execName)
        target = tg.link_task.outputs[0]
    except:
        target = ctx.path.get_bld().find_resource(execName)

    return target
