class AnalyzeCommand(BuildContext):
        cmd = 'analyze'
        fun = 'analyze'
# Run some static analysis tools (if found)
def analyze(ctx):
    Utils.check_dir(ctx.env.reportDir)

    execCCCC(ctx)
    execCppCheck(ctx)

def execCCCC(ctx):
    if not ctx.env.CCCC:
        return

    ccccReportDir = path.join(ctx.env.reportDir, 'cccc')
    Utils.check_dir(ccccReportDir)

    cmdBase = "%s --outdir=%s %s"
    ccccCommand = cmdBase % (ctx.env.CCCC, ccccReportDir, sourceFilesToString(ctx))
    ctx.exec_command(ccccCommand)

def execCppCheck(ctx):
    if not ctx.env.CPPCHECK:
        return

    cppcheckFile = path.join(ctx.env.reportDir, 'cppcheck.xml')
    cppcheckCmd = "cppcheck -j 4 --quiet --enable=all --xml --xml-version=2 src/ 2> %s"
    ctx.exec_command(cppcheckCmd % cppcheckFile)

def sourceFilesToString(ctx):
    pattern = ['src/**/*.cpp', 'src/**/*.h', 'src/**/*.hpp']
    filesToAnalyze = ctx.path.ant_glob(pattern, src=True,dir=False)
    files_arg = ""

    for file in filesToAnalyze:
        files_arg +=str(file.srcpath()) + " "

    return files_arg
