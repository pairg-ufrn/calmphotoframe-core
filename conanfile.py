#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, ConfigureEnvironment
from conans.model.scope import Scopes

import os
from os import path

class MyConanFile(ConanFile):
    name     = "calmphotoframe"
    version  = "0.5.1"
    url      = "https://bitbucket.org/pairg-ufrn/calmphotoframe-core"
    settings = "os", "compiler", "build_type", "arch"

    #Conan dependencies
    requires = (
        "jsoncpp/1.8.0@theirix/stable",
        "sqlite3/3.14.1@pbrz/stable",
        "Poco/1.7.6@paulobrizolara/stable",
        "freeimage/3.17.0.2@pbrz/stable",
        "civetweb/1.9.1@pbrz/testing",

	# Build dependencies
	"waf/0.1.1@paulobrizolara/stable",
        "runcommand-waf-tool/1.0.0@paulobrizolara/master",
        "WafGenerator/0.0.5@noface/testing"
    )
    dev_requires = (
        "catch/1.5.0@TyRoXx/stable",
        "FakeIt/2.0.4@noface/stable"
    )

    generators = "Waf"
    exports = (
                "wscript", path.join("**", "wscript"), 
                "src/**", 
                "libs/**", 
                "daemon/**", 
                "tools/**", 
                "buildtools/*.py",
                "conf.json")
    options = {
        "shared"            : [True, False],
    }
    default_options = '''
        shared=True
        
        Poco:shared=False
        Poco:enable_util=True
        Poco:enable_xml=False
        Poco:enable_json=False
        Poco:enable_mongodb=False
        Poco:enable_net=False
        Poco:enable_netssl=False
        Poco:enable_netssl_win=False
        Poco:enable_crypto=False
        Poco:enable_data=False
        Poco:enable_data_sqlite=False
        Poco:enable_zip=False
        Poco:force_openssl=False
        
        freeimage:shared=False
        jsoncpp:shared=False
        civetweb:shared=False
        sqlite3:shared=False
        jsoncpp:shared=False
        
        jsoncpp:use_pic=True
        Poco:use_pic=True
        
        FakeIt:header_version=catch
    '''

    def imports(self):
	# Copy waf executable to project folder
	self.copy("waf", dst=".")
        self.copy("run_command.py", src="bin", dst="buildtools")

        self.copy("*.dll", dst="bin", src="bin") # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

        if self.settings.os == "Android":
            self.copy("*.so*", src="lib", dst="lib")
        
    def build(self):
        self.build_path = path.abspath("build")
        
        self.env_vars = "PATH=$PATH:{}".format(' '.join(self.deps_env_info.path))
        cmd = "waf configure build %s -o %s" % (self.get_options(), self.build_path)
        
        self.output.info("ENV:\n{}".format(self.env_vars))
        self.output.info(cmd)
        
        self.run("%s %s" % (self.env_vars, cmd), cwd=self.conanfile_directory)
        
    def package(self):
        self.run("%s waf install" %( self.env_vars))

    def get_options(self):
        opts = []

        if self.settings.build_type == "Debug":
            opts.append("--debug")
        else:
            opts.append("--release")
            
        if self.options.shared:
            self.output.info("building shared library")
            opts.append("--shared")
            
        if self.scope.dev:
            opts.append("--with-tests")

        if not hasattr(self, "package_folder"):
            self.package_folder = path.abspath(path.join(".", "package"))
            
        opts.append("--prefix=%s" % self.package_folder)
            
        return " ".join(opts)

    def package_info(self):
        self.cpp_info.libs = ['calmphotoframe']
        self.cpp_info.cppflags = ["-std=c++11"]

    def get_waf_path(self):
        for p in self.deps_env_info.path:
            if path.basename(p) == "waf":
                return p
                
        return None
