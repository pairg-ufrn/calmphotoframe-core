PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE Photo(
	createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, credits TEXT
	, date TEXT
	, description TEXT
	, id INTEGER PRIMARY KEY 
	, imagePath TEXT
	, moment TEXT
	, place TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
INSERT INTO "Photo" VALUES('2016-01-13 19:46:54','','','Lorem ipsum dolor sit amet',1,'tmp14411faaaaa.jpeg','random moment','anywhere','2016-01-13 19:46:54');
CREATE TABLE PhotoMarker(
	createdAt TIMESTAMP
	, name TEXT
	, photoId INTEGER NOT NULL 
	, photoIndex INTEGER NOT NULL 
	, relativeX REAL
	, relativeY REAL
	, updatedAt TIMESTAMP
	, FOREIGN KEY (photoId) REFERENCES Photo(id)
	, PRIMARY KEY (photoId, photoIndex));
CREATE TABLE PhotoMetadata(
	cameraMaker TEXT
	, cameraModel TEXT
	, id INTEGER PRIMARY KEY 
	, imageHeight INTEGER
	, imageSize INTEGER
	, imageWidth INTEGER
	, locationAltitude REAL
	, locationLatitude REAL
	, locationLongitude REAL
	, orientationFlag INTEGER
	, originalDate TIMESTAMP
	, originalFilename TEXT
	, FOREIGN KEY (id) REFERENCES Photo(id));
INSERT INTO "PhotoMetadata" VALUES('','',1,480,13166,640,3.40282346638528859772e+38,3.40282346638528859772e+38,3.40282346638528859772e+38,0,'','IMG_20151105_101816.jpg');
CREATE TABLE User(
	active INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, isAdmin INTEGER
	, login TEXT UNIQUE 
	, passHash TEXT
	, salt TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
INSERT INTO "User" VALUES(1,'2016-01-29 20:21:19',1,1,'user','36dcf74567777504019ccdbf992e4badf10e1e2b','FV�hd?�Q:z�l�f$user','2016-01-29 20:21:19');
CREATE TABLE Session(
	expireTimestamp INTEGER
	, id INTEGER PRIMARY KEY 
	, token TEXT
	, userId TEXT
	, FOREIGN KEY (userId) REFERENCES User(id));
CREATE TABLE PhotoCollection(
	contextId INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, name TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, FOREIGN KEY (contextId) REFERENCES PhotoContext(id) ON DELETE CASCADE);
INSERT INTO "PhotoCollection" VALUES(1,'2016-01-29 20:21:19',1,'','2016-01-29 20:21:19');
INSERT INTO "PhotoCollection" VALUES(1,'2016-01-13 19:47:19',2,'album','2016-01-13 19:47:19');
CREATE TABLE PhotoCollectionItems(
	collection_id INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, item_id INTEGER
	, item_type INTEGER
	, FOREIGN KEY (collection_id) REFERENCES PhotoCollection(id) ON DELETE CASCADE
	, PRIMARY KEY (collection_id, item_id, item_type));
INSERT INTO "PhotoCollectionItems" VALUES(1,'2016-01-13 19:46:54',1,1);
INSERT INTO "PhotoCollectionItems" VALUES(1,'2016-01-13 19:47:19',2,2);
CREATE TABLE PhotoContext(
	createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, name TEXT
	, rootCollection INTEGER
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, FOREIGN KEY (rootCollection) REFERENCES PhotoCollection(id));
INSERT INTO "PhotoContext" VALUES('2016-01-29 20:21:19',1,'',1,'CURRENT_TIMESTAMP');
PRAGMA user_version=1;
COMMIT;
