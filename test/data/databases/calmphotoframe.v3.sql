PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE Photo(
	createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, credits TEXT
	, date TEXT
	, description TEXT
	, id INTEGER PRIMARY KEY 
	, imagePath TEXT
	, moment TEXT
	, place TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
INSERT INTO "Photo" VALUES('2016-01-13 19:46:54','','','Lorem ipsum dolor sit amet',1,'tmp14411faaaaa.jpeg','random moment','anywhere','2016-01-13 19:46:54');
INSERT INTO "Photo" VALUES('2016-01-13 19:46:55','','','Descrição com acentuação e caracteres especiais, por quê?',2,'tmp14411haaaaa.jpeg','até amanhã','','2016-01-13 19:46:55');
INSERT INTO "Photo" VALUES('2016-01-13 19:46:59','','','',4,'tmp14411daaaaa.jpeg','','','2016-01-13 19:46:59');
CREATE TABLE PhotoMarker(
	createdAt TIMESTAMP
	, name TEXT
	, photoId INTEGER NOT NULL 
	, photoIndex INTEGER NOT NULL 
	, relativeX REAL
	, relativeY REAL
	, updatedAt TIMESTAMP
	, FOREIGN KEY (photoId) REFERENCES Photo(id)
	, PRIMARY KEY (photoId, photoIndex));

INSERT INTO PhotoMarker VALUES('2015-11-11 11:11:11', 'Lorem ipsum', 1, 1, 0.2, 0.13, '2016-03-12 10:46:30');
INSERT INTO PhotoMarker VALUES('2016-01-01 11:10:09', 'Proin si tellus, tempor', 1, 2, 0.82, 0.35, '2016-03-12 07:01:37');
INSERT INTO PhotoMarker VALUES('2016-02-02 08:07:06', 'vitae placerat dui'     , 1, 3, 0.42, 0.73, '2016-01-12 12:34:56');
INSERT INTO PhotoMarker VALUES('2016-03-03 23:58:30', 'In ac nunc tristique'   , 2, 1, 0.21, 0.12, '2016-03-01 20:00:00');
INSERT INTO PhotoMarker VALUES('2016-04-04 01:20:00', 'Quisque tempor mollis'  , 2, 2, 0.40, 0.56, '2016-04-01 18:10:20');
INSERT INTO PhotoMarker VALUES('2016-03-12 10:46:30', 'Pellentesque quis ipsum', 2, 3, 0.70, 0.89, '2016-04-01 11:23:20');

CREATE TABLE PhotoMetadata(
	cameraMaker TEXT
	, cameraModel TEXT
	, id INTEGER PRIMARY KEY 
	, imageHeight INTEGER
	, imageSize INTEGER
	, imageWidth INTEGER
	, locationAltitude REAL
	, locationLatitude REAL
	, locationLongitude REAL
	, orientationFlag INTEGER
	, originalDate TIMESTAMP
	, originalFilename TEXT
	, FOREIGN KEY (id) REFERENCES Photo(id));
INSERT INTO "PhotoMetadata" VALUES('','',1,480,13166,640,3.40282346638528859772e+38,3.40282346638528859772e+38,3.40282346638528859772e+38,0,'','IMG_20151105_101816.jpg');
INSERT INTO "PhotoMetadata" VALUES('','',2,480,20049,640,3.40282346638528859772e+38,3.40282346638528859772e+38,3.40282346638528859772e+38,0,'','IMG_20150915_203954.jpg');
INSERT INTO "PhotoMetadata" VALUES('','',4,480,21615,640,3.40282346638528859772e+38,3.40282346638528859772e+38,3.40282346638528859772e+38,0,'','IMG_20150915_203952.jpg');
CREATE TABLE User(
	active INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, isAdmin INTEGER
	, login TEXT UNIQUE 
	, passHash TEXT
	, salt TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
INSERT INTO "User" VALUES(1,'2015-12-16 20:53:58',1,1,'user','a33a9b8acd19ee227a3b31a8ce2f657655734c67','�YI�:X3)0�B!user','2015-12-16 20:53:58');
INSERT INTO "User" VALUES(1,'2015-12-16 22:37:48',2,0,'outro','d5360441fa31bf1192d7dfc442f9a72934c0a5ec','�Er��})��m�^�Xoutro','2015-12-16 22:37:48');
INSERT INTO "User" VALUES(0,'2015-12-23 19:29:15',3,0,'novo-usuario','c3b0623360c3d4ba5293ef795fed67d31d9325a7',':ɶ} �~9�>�
�novo-usuario','2015-12-23 19:29:15');
INSERT INTO "User" VALUES(0,'2015-12-23 19:46:09',4,0,'novaconta','4bc48b7ff80baaa1ebf250dcab965e36a304dcbf','e���2�.#%h��qnovaconta','2015-12-23 19:46:09');
INSERT INTO "User" VALUES(1,'2015-12-23 19:53:00',5,0,'outraconta','e5be8a3bf1f532e814d6f0bdbc545dd6919eddfd','���6f� �FMgt�9Loutraconta','2015-12-23 19:53:00');
INSERT INTO "User" VALUES(0,'2015-12-24 02:21:30',6,0,'maisum','73007fdfcc5d5503b83d0b58e09316024179d47f','�<�wW|�-��X$���maisum','2015-12-24 02:21:30');
CREATE TABLE Session(
	expireTimestamp INTEGER
	, id INTEGER PRIMARY KEY 
	, token TEXT
	, userId TEXT
	, FOREIGN KEY (userId) REFERENCES User(id));
INSERT INTO "Session" VALUES(1456262937,7,'46c7584e145b01ae7628b97abd54caede85c6627','1');
CREATE TABLE PhotoCollection(
	contextId INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, name TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, FOREIGN KEY (contextId) REFERENCES PhotoContext(id) ON DELETE CASCADE);
INSERT INTO "PhotoCollection" VALUES(1,'2015-12-16 20:53:57',1,'','2015-12-16 20:53:57');
INSERT INTO "PhotoCollection" VALUES(1,'2016-01-13 19:47:19',2,'album','2016-01-13 19:47:19');
INSERT INTO "PhotoCollection" VALUES(2,'2016-02-16 01:10:57',3,'','2016-02-16 01:10:57');
CREATE TABLE PhotoCollectionItems(
	collection_id INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, item_id INTEGER
	, item_type INTEGER
	, FOREIGN KEY (collection_id) REFERENCES PhotoCollection(id) ON DELETE CASCADE
	, PRIMARY KEY (collection_id, item_id, item_type));
INSERT INTO "PhotoCollectionItems" VALUES(1,'2016-01-13 19:46:54',1,1);
INSERT INTO "PhotoCollectionItems" VALUES(1,'2016-01-13 19:46:55',2,1);
INSERT INTO "PhotoCollectionItems" VALUES(1,'2016-01-13 19:47:19',2,2);
INSERT INTO "PhotoCollectionItems" VALUES(2,'2016-01-13 19:47:27',4,1);
CREATE TABLE PhotoContext(
	createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, name TEXT
	, rootCollection INTEGER
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, visibility INTEGER, FOREIGN KEY (rootCollection) REFERENCES PhotoCollection(id));
INSERT INTO "PhotoContext" VALUES('2015-12-16 20:53:57',1,'',1,'CURRENT_TIMESTAMP',4);
INSERT INTO "PhotoContext" VALUES('2016-02-16 01:10:57',2,'Outro contexto',3,'CURRENT_TIMESTAMP',1);
CREATE TABLE Events(
        createdAt TIMESTAMP
        , entityId INTEGER
        , id INTEGER PRIMARY KEY 
        , type INTEGER
        , userId INTEGER);
INSERT INTO "Events" VALUES('2016-01-26T18:17:55-03:00',1,1,2,1);
INSERT INTO "Events" VALUES('2016-01-31T15:58:35-03:00',3,2,3,1);
CREATE TABLE Permissions(contextId INTEGER, permissionLevel INTEGER, userId INTEGER, FOREIGN KEY (contextId) REFERENCES PhotoContext(id) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY (userId) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE, PRIMARY KEY (contextId, userId));
INSERT INTO "Permissions" VALUES(2,3,1);
PRAGMA user_version=3;
COMMIT;
