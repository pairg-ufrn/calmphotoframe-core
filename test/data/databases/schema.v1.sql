CREATE TABLE Photo(
	createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, credits TEXT
	, date TEXT
	, description TEXT
	, id INTEGER PRIMARY KEY 
	, imagePath TEXT
	, moment TEXT
	, place TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
CREATE TABLE PhotoMarker(
	createdAt TIMESTAMP
	, name TEXT
	, photoId INTEGER NOT NULL 
	, photoIndex INTEGER NOT NULL 
	, relativeX REAL
	, relativeY REAL
	, updatedAt TIMESTAMP
	, FOREIGN KEY (photoId) REFERENCES Photo(id)
	, PRIMARY KEY (photoId, photoIndex));
CREATE TABLE PhotoMetadata(
	cameraMaker TEXT
	, cameraModel TEXT
	, id INTEGER PRIMARY KEY 
	, imageHeight INTEGER
	, imageSize INTEGER
	, imageWidth INTEGER
	, locationAltitude REAL
	, locationLatitude REAL
	, locationLongitude REAL
	, orientationFlag INTEGER
	, originalDate TIMESTAMP
	, originalFilename TEXT
	, FOREIGN KEY (id) REFERENCES Photo(id));
CREATE TABLE User(
	active INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, isAdmin INTEGER
	, login TEXT UNIQUE 
	, passHash TEXT
	, salt TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
CREATE TABLE Session(
	expireTimestamp INTEGER
	, id INTEGER PRIMARY KEY 
	, token TEXT
	, userId TEXT
	, FOREIGN KEY (userId) REFERENCES User(id));
CREATE TABLE PhotoCollection(
	contextId INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, name TEXT
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, FOREIGN KEY (contextId) REFERENCES PhotoContext(id) ON DELETE CASCADE);
CREATE TABLE PhotoCollectionItems(
	collection_id INTEGER
	, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, item_id INTEGER
	, item_type INTEGER
	, FOREIGN KEY (collection_id) REFERENCES PhotoCollection(id) ON DELETE CASCADE
	, PRIMARY KEY (collection_id, item_id, item_type));
CREATE TABLE PhotoContext(
	createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, id INTEGER PRIMARY KEY 
	, name TEXT
	, rootCollection INTEGER
	, updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	, FOREIGN KEY (rootCollection) REFERENCES PhotoCollection(id));
