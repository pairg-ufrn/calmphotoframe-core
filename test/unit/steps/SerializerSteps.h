#ifndef SERIALIZERSTEPS_H
#define SERIALIZERSTEPS_H

#include <string>

#include <catch.hpp>
#include <fakeit.hpp>

#include "serialization/Serializable.hpp"
#include "serialization/Serializer.h"

#include "helpers/CaptureArguments.h"
#include "helpers/StringCaptor.h"

class SerializerSteps{
public:
    SerializerSteps();
    
    void configure_serializer_mockup();
    
    void when_serializing_it(Serializable & serializable);
        
    void then_it_should_have_serialized(const std::string & property, const std::string & value);
    
public:
    fakeit::Mock<Serializer> serializerMockup; 
    CaptureArguments<void, StringCaptor, StringCaptor> capturePutValue;   
};

#endif // SERIALIZERSTEPS_H
