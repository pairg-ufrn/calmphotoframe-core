#include "DatabaseMockupSteps.h"

#include "fakeit.hpp"

#include "model/user/Session.h"
#include "model/user/UserAccount.h"
#include "utils/TimeUtils.h"

using namespace fakeit;

void DatabaseMockupSteps::using_db_mockups(){
    databuilderMockup.setup().apply();    
}

void DatabaseMockupSteps::mockup_users_default(){
    When(Method(databuilderMockup.users(), insertAt)).Do(
        [](long userId, const UserAccount & userAccount){
            return userId;
        }
    ); 
    
    When(ConstOverloadedMethod(databuilderMockup.users(), exist, bool(long))).AlwaysReturn(false);
    When(ConstOverloadedMethod(databuilderMockup.users(), exist, bool(const std::string &))).AlwaysReturn(false);
}

void DatabaseMockupSteps::mockup_contexts_default(){
    When(Method(databuilderMockup.contexts(), count));
}

void DatabaseMockupSteps::mockup_logged_in_user(const UserAccount & account){
    long userId = account.getId();
    
    When(Method(databuilderMockup.sessions(), getByToken)).Do(
        [userId](const std::string & token, Session & sessionOut){
            sessionOut.setExpireTimestamp(TimeUtils::instance().getTimestamp() + 10000);
            sessionOut.setToken(token);
            sessionOut.setUserId(userId);
            sessionOut.setId(123456L);
            
            return true;
        }
    );
    
    When(ConstOverloadedMethod(databuilderMockup.users(), exist, bool(long)).Using(userId))
        .AlwaysReturn(true);
        
        
    When(Method(databuilderMockup.users(), get).Using(userId, _))
        .AlwaysDo([account](long userId, UserAccount & userOut){
            userOut = account;
            return true;
    });
}

void DatabaseMockupSteps::mockup_config_methods(){
    When(Method(databuilderMockup.configs(), get)).AlwaysDo(captureGetConfig.returnValue("mocked string"));
    When(Method(databuilderMockup.configs(), put)).AlwaysDo(capturePutConfig);
}
