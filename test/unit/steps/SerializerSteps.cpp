#include "SerializerSteps.h"

#include <catch.hpp>

using namespace std;

SerializerSteps::SerializerSteps(){
    configure_serializer_mockup();
}

#define MOCK_OVERLOAD_PUT(mockup, second_type)\
    fakeit::When(OverloadedMethod(mockup, put, void(const std::string &, second_type)))
        
void SerializerSteps::configure_serializer_mockup(){
    MOCK_OVERLOAD_PUT(serializerMockup, const std::string &).AlwaysDo(capturePutValue);
    MOCK_OVERLOAD_PUT(serializerMockup, const long &).AlwaysDo(capturePutValue);
    MOCK_OVERLOAD_PUT(serializerMockup, const bool &).AlwaysDo(capturePutValue);
    MOCK_OVERLOAD_PUT(serializerMockup, const int &).AlwaysDo(capturePutValue);
}

void SerializerSteps::when_serializing_it(Serializable & serializable){
    serializable.serialize(this->serializerMockup.get());
}

void SerializerSteps::then_it_should_have_serialized(const string & property, const string & value){
    CHECK(capturePutValue.captureCount() > 0);
    
    for(int i=0; i < capturePutValue.captureCount(); ++i){
        const auto & propName = capturePutValue.get<0>(i).value;
        
        if(propName == property){
            CAPTURE(propName);
            CHECK(capturePutValue.get<1>(i).value == value);
            return;
        }
    }

    bool serializedProperty = false;
    INFO("Property " << property << " was not serialized");
    CHECK(serializedProperty);
}
