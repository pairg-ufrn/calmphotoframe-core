#include "MultipartRequestSteps.h"

using namespace calmframe::utils;

/**************************************** steps ******************************************************/

void MultipartRequestSteps::check_request_has_multipart_content_type(Request * req, const string & expectedBoundary){
    CHECK(req->getContentType().getType() == Types::MULTIPART);

    string boundary = req->getContentType().getParameter("boundary");

    CHECK_FALSE(boundary.empty());
    CHECK(boundary == expectedBoundary);
}

void MultipartRequestSteps::when_starting_multipart_request_reader(Request * req)
{
    CHECK(multiReqReader.start(req));    
}

void MultipartRequestSteps::when_reading_all_parts(){

    int readCount = 0;
    
    MultipartRequestReader::PartPtr part;
    while((part = multiReqReader.next())){
        extractHeaders((RequestPart *)part.get());
        readBody((RequestPart *)part.get());
        
        ++readCount;
    }


    CHECK(readCount == EXPECTED_NUM_PARTS);
}

void MultipartRequestSteps::extractHeaders(RequestPart* part)
{
    vector<Header> headers;
    
    for(auto headerPair : part->getHeaders()){
        headers.push_back(Header{headerPair.first, headerPair.second});    
    }
    partHeaders.push_back(headers);    
}

void MultipartRequestSteps::readBody(RequestPart* part)
{
    stringstream content;        
    content << part->getContent().rdbuf();
    partsBodies.push_back(content.str());    
}

void MultipartRequestSteps::check_all_parts_readed(){
    CHECK(this->partHeaders.size() == expectedHeaders.size());
    CHECK(this->partsBodies.size() == expectedBodies.size());
}

void MultipartRequestSteps::check_parts_headers(){
    for (int i = 0; i < partHeaders.size() && i < expectedHeaders.size(); ++i) {
        INFO("Part: " << i);
        
        const auto & headers = partHeaders[i], expected = expectedHeaders[i];
        
        for (int j = 0; j < headers.size() && j < expected.size(); ++j) {
            CHECK(headers[j].getName() == expected[j].getName());
            CHECK(headers[j].getValue() == expected[j].getValue());
        }
    }
}

void MultipartRequestSteps::check_parts_bodys(){
    for (int i = 0; i < partsBodies.size() && i < partsBodies.size(); ++i) {
        INFO("Part: " << i);
        
        CHECK(partsBodies[i] == expectedBodies[i]);
    }
}

/*****************************************************************************************************/

int MultipartRequestSteps::EXPECTED_NUM_PARTS = 2;

vector<vector<Header>> MultipartRequestSteps::expectedHeaders = {
    {
        Header{"Content-Disposition", R"(form-data; name="submitted"; filename="loremipsum.txt")"},
        Header{"Content-Type", "text/plain"}
    },
    {
        Header{"Content-Disposition", R"(form-data; name="submitted"; filename="test_photo.json")"},
        Header{"Content-Type", "application/json"}
    }
};
vector<string> MultipartRequestSteps::expectedBodies = {
R"(Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus 
mollis ex, ut auctor augue vehicula sed.

-------------------------------------------------------------------------

Phasellus pellentesque sit amet lacus non tempus. Curabitur semper velit 
ligula, eu consequat diam pulvinar eget.)",

R"({
   "name" : "example",
   "content" : "none"
})"
};

const char * MultipartRequestSteps::BOUNDARY = "---------------------------36234065812799759491592900430";

const string MultipartRequestSteps::MULTIPART_CONTENT = buildMultipartContent();

string MultipartRequestSteps::buildMultipartContent(){
    static const string delimiter = "--" + string(MultipartRequestSteps::BOUNDARY);
    static const string closeDelimiter = delimiter + "--";

    vector<string> parts;
    
    parts.push_back("This is the preamble. This part should be ignored");
    
    for (int i = 0; i < EXPECTED_NUM_PARTS; ++i) {
        parts.push_back(delimiter);
        
        for(const auto & header : expectedHeaders[i]){
            parts.push_back(header.toString());
        }
        parts.push_back("");
        
        parts.push_back(expectedBodies[i]);
    }

    parts.push_back(closeDelimiter);
    parts.push_back("This is the epilogue. It should be ignored."); 

    return StringUtils::instance().join("\r\n", parts);
}
