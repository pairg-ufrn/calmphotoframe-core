#ifndef DATABASEMOCKUPSTEPS_H
#define DATABASEMOCKUPSTEPS_H

#include "mocks/DataBuilderMockup.h"
#include "helpers/CaptureArguments.h"

class DatabaseMockupSteps{
public:
    DataBuilderMockup databuilderMockup;
    
    void using_db_mockups();
    void mockup_users_default();
    void mockup_contexts_default();
    
    void mockup_logged_in_user(const UserAccount & account);
    
    void mockup_config_methods();

public:    
    CaptureArguments<std::string, std::string> captureGetConfig;
    CaptureArguments<void, std::string, std::string> capturePutConfig;
};

#endif // DATABASEMOCKUPSTEPS_H
