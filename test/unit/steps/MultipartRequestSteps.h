#ifndef MULTIPARTREQUESTSTEPS_H
#define MULTIPARTREQUESTSTEPS_H

#include "TestMacros.h"

#include <iostream>
#include <string>
#include <memory>
#include <sstream>

#include "network/Request.h"
#include "network/RequestPart.h"
#include "network/ContentTypes.hpp"
#include "network/MultipartRequestReader.h"

#include "utils/StringUtils.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::network;

class MultipartRequestSteps{
public:
    static int EXPECTED_NUM_PARTS;

    static const string MULTIPART_CONTENT;
    static const char * BOUNDARY;

    static string buildMultipartContent();
    
public: //steps
    void when_starting_multipart_request_reader(Request * request);
    void when_reading_all_parts();
    
    void check_request_has_multipart_content_type(Request * req, const std::string & expectedBoundary);
    void check_all_parts_readed();
    void check_parts_headers();
    void check_parts_bodys();
    void extractHeaders(RequestPart* part);
    void readBody(RequestPart* part);

public:
    MultipartRequestReader multiReqReader;
        
    vector<vector<Header>> partHeaders;
    vector<string> partsBodies;
    
    static vector<vector<Header>> expectedHeaders;
    static vector<string> expectedBodies;
};


#endif // MULTIPARTREQUESTSTEPS_H
