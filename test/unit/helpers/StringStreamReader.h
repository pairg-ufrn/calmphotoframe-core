#ifndef STRINGSTREAMREADER_H
#define STRINGSTREAMREADER_H

#include <streambuf>
#include <algorithm>
#include "network/readers/StreamReader.hpp"

using namespace std;

class StringStreamReader : public calmframe::network::StreamReader{
private:
    string * text;
    int counter;
public:
    StringStreamReader(string * text)
        : text(text)
        , counter(0)
    {}

    streamsize read(char * buffer, const streamsize & readLimit){
        if(counter >= text->size()){
            return streambuf::traits_type::eof();
        }
        else{
            streamsize bytesToRead = std::min((streamsize)(text->size() - counter), readLimit);
            int readedBytes=0;

            if(bytesToRead >= 1){
                readedBytes = text->copy(buffer, bytesToRead, counter);
                counter += readedBytes;
            }

            return readedBytes;
        }
    }
};

#endif // STRINGSTREAMREADER_H
