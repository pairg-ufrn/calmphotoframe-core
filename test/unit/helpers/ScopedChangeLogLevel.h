#ifndef SCOPEDCHANGELOGLEVEL_H
#define SCOPEDCHANGELOGLEVEL_H

#include "utils/Log.h"

namespace calmframe {

class ScopedChangeLogLevel{
public:
    ScopedChangeLogLevel(LogLevel::Level newLevel);
    ~ScopedChangeLogLevel();
    
private:
    LogLevel::Level previousLevel;
};


}//namespace
#endif // SCOPEDCHANGELOGLEVEL_H
