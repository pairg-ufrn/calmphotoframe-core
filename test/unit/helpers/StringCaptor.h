#ifndef STRINGCAPTOR_H
#define STRINGCAPTOR_H

#include <string>

#include "utils/StringCast.h"

class StringCaptor{
public:
    StringCaptor() = default;
    
    template<typename T>
    StringCaptor(const T & value){
        capture(value);
    }
    
    template<typename T>
    StringCaptor& operator=(const T & value){
        capture(value);
    }
    
    template<typename T>
    void capture(const T & value){
        this->value = calmframe::utils::StrCast().toString(value);
    }
    void capture(bool value){
        this->value = value ? "true" : "false";
    }
    
    std::string value;
};


#endif // STRINGCAPTOR_H
