#include "SessionHelper.h"
#include "PhotoContextHelper.h"

#include "network/Header.h"
#include "network/Headers.h"
#include "api/Api.h"

#include "services/SessionManager.h"

#include "data/Data.h"

using namespace calmframe;
using namespace calmframe::network;
using namespace calmframe::data;
using namespace std;

void SessionHelper::putTokenCookie(const std::string & token, MockRequest & request){
    static const string tokenEquals = string(api::cookies::sessionCookie).append("=");
    Header tokenHeader(Headers::Cookie, tokenEquals + token);
    request.putHeader(tokenHeader);
}

void SessionHelper::putTokenHeader(const std::string & token, MockRequest & request){
    Header tokenHeader(Headers::XToken, token);
    request.putHeader(tokenHeader);
}

void SessionHelper::putTokenQueryParameter(const std::string & token, MockRequest & request){
    request.putQueryParameter(api::cookies::sessionCookie, token);
}

long SessionHelper::setupSessionToken(MockRequest & request){
    Session session = createValidSession();
    putTokenCookie(session.getToken(), request);
    return session.getUserId();
}

Session SessionHelper::createValidSession(){
    return SessionManager::instance().startLocalSession("notASecret");
}

long SessionHelper::putUserWithPermission(MockRequest & request, long ctxId, PermissionLevel permissionLevel){
    long userId = SessionHelper::setupSessionToken(request);
    Data::instance().permissions().putPermission(Permission(ctxId, userId, permissionLevel));

    return userId;
}


long SessionHelper::requireContextPermission(MockRequest & request, calmframe::PermissionLevel requiredPermission){
    long ctxId = PhotoContextHelper::insertContext().getId();
    putUserWithPermission(request, ctxId, requiredPermission);

    return ctxId;
}
