#ifndef PHOTOCONTEXTHELPER_H
#define PHOTOCONTEXTHELPER_H

#include <vector>
#include <set>

#include "model/photo/PhotoContext.h"


class PhotoContextHelper{
public:
    typedef std::vector<calmframe::PhotoContext> ContextList;
public:
    static calmframe::PhotoContext insertCtxWithCollections
                        (const std::string &ctxName, int collCount, std::set<long> *outCollIds=NULL);
    static void createContexts(int ctxCount, std::vector<calmframe::PhotoContext> & outContexts);
    static calmframe::PhotoContext insertContext();
    static calmframe::PhotoContext insertContext(const std::string & ctxName);
    static calmframe::PhotoContext & insertContext(calmframe::PhotoContext& context);
    static calmframe::PhotoContext && insertContext(calmframe::PhotoContext&& context);
    static calmframe::PhotoContext insertContextWithRootCollection();
    static calmframe::PhotoContext insertContextWithRootCollection(const calmframe::PhotoContext & context);
    static void insertContexts(ContextList & contexts);
    static ContextList && insertContexts(ContextList && contexts);
    static ContextList insertContexts(int ctxCount);
    static ContextList & insertContexts(int ctxCount, ContextList & outContexts);
    static void checkCollectionContainsContexts(const std::vector<calmframe::PhotoContext> & collection
                                       , const std::vector<calmframe::PhotoContext> & containedCtxs);
    static void checkEquals(const calmframe::PhotoContext & ctx1 , const calmframe::PhotoContext & ctx2);
    static void checkEquals(std::vector<calmframe::PhotoContext> & contexts1
        , std::vector<calmframe::PhotoContext> & contexts2, bool detailedCheck=false);

    static std::string makeCtxName();


    static calmframe::PhotoContext createContextHierarchy();
};

#endif // PHOTOCONTEXTHELPER_H
