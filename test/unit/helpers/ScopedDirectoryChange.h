#ifndef SCOPEDDIRECTORYCHANGE_H
#define SCOPEDDIRECTORYCHANGE_H

#include <string>
#include "utils/FileUtils.h"
#include "utils/IOException.h"
#include "utils/Log.h"

using calmframe::utils::FileUtils;

class ScopedDirectoryChange{
private:
    std::string initialPath;
    std::string changePath;
public:
    ScopedDirectoryChange(const std::string & dirToChange)
        : initialPath(FileUtils::instance().canonicalPath(FileUtils::instance().getCurrentDir()))
        , changePath(FileUtils::instance().canonicalPath(dirToChange))
    {
        if(!FileUtils::instance().changeCurrentDir(changePath)){
            throw CommonExceptions::IOException("Could not change to directory "
                              "\"" + changePath + "\""
                              " from directory "  +
                              "\"" + initialPath + "\"");
        }
    }

    ~ScopedDirectoryChange(){
        bool success = FileUtils::instance().changeCurrentDir(initialPath);
        if(!success){
            logWarning("ScopedDirectoryChange::destructor:\t"
                       "Could not change to dir '%s' ", initialPath.c_str());
        }
    }

    const std::string & getChangePath() const{
        return changePath;
    }
    const std::string & getInitialPath() const{
        return initialPath;
    }
};

#endif // SCOPEDDIRECTORYCHANGE_H

