#ifndef STREAMSHELPER_H
#define STREAMSHELPER_H

#include <sstream>

class StreamsHelper
{
public:
    static void clearStream(std::stringstream & stream){
        stream.str("");
        stream.clear();
    }
};

#endif // STREAMSHELPER_H
