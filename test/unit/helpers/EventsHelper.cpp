#include "EventsHelper.h"

#include "data/Data.h"
#include "data/DataTransaction.h"

#include "catch.hpp"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;

std::vector<calmframe::Event> & EventsHelper::insertEvents(std::vector<calmframe::Event> & evts){
    DataTransaction transaction(Data::instance());

    for(Event & evt : evts){
        long id = Data::instance().events().insert(evt);
        evt.setId(id);
    }

    transaction.commit();

    return evts;
}


std::vector<calmframe::Event> && EventsHelper::insertEvents(std::vector<calmframe::Event> && evts){
    insertEvents(evts);

    return std::move(evts);
}


std::vector<calmframe::Event> EventsHelper::insertEvents(int count){
    vector<Event> events;

    for(int i=0; i < count; ++i){
        events.push_back({EventType::PhotoCreation});
    }

    return insertEvents(events);
}

void EventsHelper::checkEquals(const Event & evt, const Event & expectedEvt)
{
    CHECK(expectedEvt.getId() == evt.getId());
    CHECK(expectedEvt.getType() == evt.getType());
    CHECK(expectedEvt.getUserId() == evt.getUserId());
    CHECK(expectedEvt.getEntityId() == evt.getEntityId());
    CHECK(expectedEvt.getCreation() == evt.getCreation());
}

void EventsHelper::checkEquals(const ExtendedEvent & evt, const ExtendedEvent & expectedEvt){
    checkEquals((Event)evt, (Event)expectedEvt);
    CHECK(evt.hasUser() == expectedEvt.hasUser());
    CHECK(evt.getUserInfo().getUserId() == expectedEvt.getUserInfo().getUserId());
    CHECK(evt.getUserInfo().getLogin() == expectedEvt.getUserInfo().getLogin());
    CHECK(evt.getUserInfo().getName() == expectedEvt.getUserInfo().getName());
}

void EventsHelper::checkEventCreated(EventType eventType, long entityId, long userId)
{
    Event lastEvent = getLastEvent();
    CHECK(lastEvent.getEntityId() == entityId);
    CHECK(lastEvent.getType() == eventType);
    CHECK(lastEvent.getUserId() == userId);
}

Event EventsHelper::getLastEvent(){
    auto recentEvents = Data::instance().events().listRecents(1);

    bool existLastEvent = (recentEvents.size() == 1);
    REQUIRE(existLastEvent);

    return recentEvents[0];
}
