#include "ScopedStepedRunnableExecution.h"

#include "utils/Log.h"

namespace calmframe {
namespace tests {

ScopedStepedRunnableExecution::ScopedStepedRunnableExecution(StepedRunnable &someRunnable, int maxWaitTime)
    : runnable(someRunnable), maxJoinMillisWait(maxWaitTime)
{}

ScopedStepedRunnableExecution::~ScopedStepedRunnableExecution(){
    runnable.finish();
    bool joined = thread.tryJoin(maxJoinMillisWait);
    if(!joined){
        logWarning("ScopedStepedRunnableExecution::destructor :\t"
                   "Failed to join thread %lu", (unsigned long)thread.tid());
    }
}

void ScopedStepedRunnableExecution::start(){
    try{
        runnable.reset();
        thread.start(runnable);
    }
    catch(std::exception & ex){
        logError("failed to start runnable due to exception: %s", ex.what());
    }
    catch(...){
        logError("failed to start runnable due to unknown error");
    }
}

}//namespace
}//namespace

