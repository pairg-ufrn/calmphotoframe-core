#ifndef CAPTUREARGUMENTS_H
#define CAPTUREARGUMENTS_H

#include <functional>
#include <tuple>
#include <memory>
#include <vector>
#include <stdexcept>

template<typename ...Args>
class BaseCaptureArguments{
public:
    using Tuple = std::tuple<Args...>;
    using TuplePtr = std::shared_ptr<Tuple>;
    
    using InvocationList = std::vector<Tuple>;
    using SharedInvocationList = std::shared_ptr<InvocationList>;
    
    SharedInvocationList invocations;
    
    BaseCaptureArguments()
        : invocations(std::make_shared<InvocationList>())
    {}
    
    template<typename ...CalledArgs>
    void capture(CalledArgs&&... arguments){
        Tuple invocation =  std::make_tuple(std::forward<CalledArgs>(arguments)...);
        this->invocations->push_back(invocation);
    }
    
    template<size_t Index>
    typename std::tuple_element< Index, std::tuple<Args...> >::type 
    getLast(){
        if(!hasCaptured()){
            throw std::out_of_range("Not capture found");
        }
        return get<Index>((size_t)captureCount() - 1);
    }
    
    template<size_t Index>
    typename std::tuple_element< Index, std::tuple<Args...> >::type 
    get(size_t captureIndex){
        if(captureCount() <= captureIndex){
            throw std::out_of_range("Capture index out of range");
        }
    
        return std::get<Index>(captured(captureIndex));
    }
        
    Tuple & captured(size_t index){
        return invocations->at(index);
    }
    
    bool hasCaptured() const{
        return captureCount() > 0;
    }
    
    size_t captureCount() const{
        return invocations->size();
    }
};

template<typename Return, typename ...Args>
class CaptureArguments : public BaseCaptureArguments<Args...>{
public:
    
    template<typename ...CalledArgs>
    Return operator()(CalledArgs&&... arguments){
        this->capture(std::forward<CalledArgs>(arguments)...);
        
        return _returnValue;
    }

    CaptureArguments & returnValue(const Return & returnValue){
        this->_returnValue = returnValue;
        return *this;
    }
    
    const Return & returnValue(){
        return this->_returnValue;
    }

private:    
    Return _returnValue;
};

template<typename ...Args>
class CaptureArguments<void, Args...> : public BaseCaptureArguments<Args...>{
public:
    typedef BaseCaptureArguments<Args...> super;

    template<typename ...CalledArgs>
    void operator()(CalledArgs&&... arguments){
        this->capture(std::forward<CalledArgs>(arguments)...);
    }
};

#endif // CAPTUREARGUMENTS_H
