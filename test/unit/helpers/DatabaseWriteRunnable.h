#ifndef DATABASEWRITERUNNABLE_H
#define DATABASEWRITERUNNABLE_H

#include "Poco/Mutex.h"
#include "data/data-sqlite/SqliteConnector.h"

#include "StepedRunnable.h"

#include <string>

namespace calmframe {
namespace tests {

class DatabaseWriteRunnable : public StepedRunnable{
    typedef StepedRunnable super;
    calmframe::data::SqliteConnector database;
    int count;
    volatile bool writing;
    Poco::Mutex isWritingMutex;

    long createdId;
public:
    DatabaseWriteRunnable(const std::string & databasePath);
    virtual ~DatabaseWriteRunnable();

    bool isWriting();
    bool isWaitingToWrite();

    void waitFinishWrite(unsigned maxWaitTime = 0);

    bool verifyDatabaseChanges();
protected:
    virtual bool executeStep();
    void startWriting();
    void finishWriting();
    long writeToDatabase();
    std::string generateUniqueName();
};

}//namespace
}//namespace

#endif // DATABASEWRITERUNNABLE_H

