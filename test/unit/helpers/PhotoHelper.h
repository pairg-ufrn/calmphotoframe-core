#ifndef PHOTOHELPER_HPP
#define PHOTOHELPER_HPP

#include <sstream>
#include "model/photo/Photo.h"
#include "json/json.h"

#include <set>
#include <vector>

using calmframe::Photo;

template<typename T>
std::ostream & operator<<(std::ostream & out, const std::vector<T> & itens){
    out << "[";
    for(auto item : itens){
        out << " " << item;
    }
    out << "]";
    return out;
}

std::ostream & operator<<(std::ostream & outstream, const PhotoDescription & photoDescription);
std::ostream & operator<<(std::ostream & outstream, const Photo & photo);
std::ostream & operator<<(std::ostream & outstream, const PhotoMarker & photoMarker);

class PhotoHelper{
public:
    static void assertExist(const Photo & photo);
    static void assertEquals(const Photo & photo, const Json::Value & json);
    static void assertEquals(const Photo &photo, const Photo &otherPhoto);
    static long insertPhoto(const Photo & photo = Photo());
    static long insertPhotoAtContext(long ctxId, calmframe::Photo photo = Photo());
    static Photo && insert(Photo && photo = Photo());

    static std::set<long> insertPhotosAtContext(int photoNumber, long ctxId);
    static std::set<long> insertPhotos(int photoNumber=10, const Photo & basePhoto={});
    static void insertAll(std::vector<Photo> & photos);
    static Photo createPhoto(unsigned id=0);
    static void assertDescriptionEquals(const Photo &photo, const Json::Value &photoJson);
    static void assertMarkersEquals(const Photo &photo, const Json::Value &photoJson);
};

#endif // PHOTOHELPER_HPP
