#ifndef EVENTSHELPER_H
#define EVENTSHELPER_H

#include "model/events/Event.h"
#include "model/events/ExtendedEvent.h"

#include <vector>

using calmframe::Event;
using calmframe::ExtendedEvent;

class EventsHelper{
public:

    static std::vector<Event> && insertEvents(std::vector<Event> && evts = std::vector<Event>());
    static std::vector<Event> & insertEvents(std::vector<Event> & evts);
    static std::vector<Event> insertEvents(int count);

    static void checkEquals(const Event & evt, const Event & expectedEvt);
    static void checkEquals(const ExtendedEvent & evt, const ExtendedEvent & expectedEvt);
    static void checkEventCreated(calmframe::EventType eventType, long entityId, long userId);

    static Event getLastEvent();

};

#endif // EVENTSHELPER_H
