#ifndef REQUESTUTILS_H
#define REQUESTUTILS_H

#include <map>
#include <string>
#include <vector>

#include <iterator>

#include "json/json.h"
#include "mocks/MockResponse.h"
#include "serialization/Deserializer.h"

class RequestUtils{
public:

    static std::map<std::string, std::string> &extractMessageHeaders(MockResponse & response,
                std::map<std::string, std::string>& headers);
    static Json::Value assertJsonObjectResponse(MockResponse & response);
    static void assertMessageStatus(MockResponse & response, int statusCode);
    static void assertMessageStatus(MockResponse & response, const calmframe::network::StatusResponse & statusCode);

    static void assertOkMessage(MockResponse & response);
    static void assertJsonMessage(MockResponse & response);
    static void assertOkJsonResponse(calmframe::network::Response & response);
    static bool isJsonObjectMessage(const std::string & msgContent);

    static void assertBadRequestResponse(MockResponse & response);
    static void assertNotFoundResponse(MockResponse & response);

    template<typename T>
    static std::vector<T> deserializeCollection(Deserializer & deserializer, const std::string & field);
};

template<typename T>
std::vector<T> RequestUtils::deserializeCollection(Deserializer &deserializer, const std::string &field)
{
    std::vector<T> coll;
    deserializer.getChildCollection<T>(field, std::back_inserter(coll));
    return coll;
}

#endif // REQUESTUTILS_H

