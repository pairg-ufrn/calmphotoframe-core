#ifndef TESTCONTROLLERPERMISSIONSFIXTURE_H
#define TESTCONTROLLERPERMISSIONSFIXTURE_H

#include "api/Api.h"
#include "TestMacros.h"
#include "helpers/WebControllerFixture.h"
#include "helpers/SessionHelper.h"
#include "helpers/RequestUtils.h"
#include "helpers/PermissionHelper.h"
#include "helpers/PhotoContextHelper.h"

#include "utils/ScopedDatabase.h"
#include "network/StatusResponses.h"

using namespace calmframe;
using namespace calmframe::data;

class ControllerPermissionsFixture : public WebControllerFixture{
public:
    ScopedSqliteDatabase scopedDb;

    ControllerPermissionsFixture();
    virtual int createPrivateContext();
    long putUserWithPermission(long ctxId, PermissionLevel permissionLevel);
    void putUserWithoutPermission(long ctxId, PermissionLevel limitPermission);

    void testForbiddenRequest(const std::string & method,
        PermissionLevel requiredLevel, const Serializable * serializable=NULL, const std::string & route=std::string());

    virtual long createEntity(long ctxId)=0;
    void executeForbiddenRequest(const std::string& method, PermissionLevel requiredLevel
            , const Serializable* serializable=NULL
            , const std::string& routeStr=std::string());
    void verifyForbiddenResponse();

    void putContextParameter(long ctxId);

    void testForbiddenListItemsWithoutPermission(PermissionLevel permissionLevel);//test
};

inline ControllerPermissionsFixture::ControllerPermissionsFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
{
}

inline int ControllerPermissionsFixture::createPrivateContext(){
    return PhotoContextHelper::insertContextWithRootCollection(PhotoContext("", Visibility::Private)).getId();
}

inline long ControllerPermissionsFixture::putUserWithPermission(long ctxId, PermissionLevel permissionLevel)
{
    return SessionHelper::putUserWithPermission(request, ctxId, permissionLevel);
}

inline void ControllerPermissionsFixture::putUserWithoutPermission(long ctxId, PermissionLevel limitPermission)
{
    PermissionLevel permissionLevel = limitPermission == PermissionLevel::Read
                                        ? PermissionLevel::None
                                        : PermissionLevel::Read;

    putUserWithPermission(ctxId, permissionLevel);
}

inline void ControllerPermissionsFixture
::testForbiddenRequest(const std::string & method, PermissionLevel requiredLevel, const Serializable * serializable
    , const std::string & reqRoute)
{

GIVEN("some context on db"){
    long ctxId = createPrivateContext();
    long id = createEntity(ctxId);

GIVEN("a user without the required permission on that context"){
    putUserWithoutPermission(ctxId, requiredLevel);

WHEN("executing that request"){
    const std::string & routeStr = reqRoute.empty() ? route(id) : reqRoute;
    CAPTURE(routeStr);
    executeForbiddenRequest(method, requiredLevel, serializable, routeStr);

THEN("a forbidden message should be sent as response"){
    verifyForbiddenResponse();
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}

inline void ControllerPermissionsFixture::executeForbiddenRequest(const std::string& method, PermissionLevel requiredLevel, const Serializable* serializable, const std::string& routeStr)
{
    CAPTURE(method);
    CAPTURE(requiredLevel);
    CAPTURE(routeStr);

    if(serializable == NULL){
        executeRequest(method, routeStr);
    }
    else{
        executeJsonRequest(method, routeStr, serializable);
    }
}

inline void ControllerPermissionsFixture::verifyForbiddenResponse()
{
    RequestUtils::assertMessageStatus(response, network::StatusResponses::FORBIDDEN);
}

inline void ControllerPermissionsFixture::testForbiddenListItemsWithoutPermission(PermissionLevel permissionLevel){

GIVEN("some context on db"){
    long ctxId = createPrivateContext();

GIVEN("a user without read permission on that context"){
    putUserWithoutPermission(ctxId, permissionLevel);

WHEN("a request is made by that user to list items from that context"){
    putContextParameter(ctxId);
    executeForbiddenRequest("GET", permissionLevel, NULL, "/");

THEN("a forbidden message should be sent as response"){
    verifyForbiddenResponse();
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

inline void ControllerPermissionsFixture::putContextParameter(long ctxId)
{
    request.putParameter(api::parameters::PhotoContextId, std::to_string(ctxId));
}

#endif // TESTCONTROLLERPERMISSIONSFIXTURE_H
