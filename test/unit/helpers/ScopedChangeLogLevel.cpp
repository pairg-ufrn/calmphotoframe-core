#include "ScopedChangeLogLevel.h"

namespace calmframe {

ScopedChangeLogLevel::ScopedChangeLogLevel(LogLevel::Level newLevel){
    this->previousLevel = MainLog().getLogLevel();
    MainLog().setLogLevel(newLevel);
}

ScopedChangeLogLevel::~ScopedChangeLogLevel(){
    MainLog().setLogLevel(previousLevel);
}

}//namespace
