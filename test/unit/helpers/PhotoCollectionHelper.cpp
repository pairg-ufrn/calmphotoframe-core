#include "PhotoCollectionHelper.h"

#include <vector>

#include "data/Data.h"
#include "data/DataTransaction.h"

#include "model/photo/PhotoCollection.h"
#include "PhotoHelper.h"
#include "utils/StringCast.h"

#include "catch.hpp"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

PhotoCollectionHelper::PhotoCollectionHelper()
{

}

PhotoCollectionHelper::~PhotoCollectionHelper()
{

}

std::set<long> PhotoCollectionHelper::getOtherCollectionIds(long photoCollectionId){
    std::vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
    set<long> collectionIds = getIds(allCollections.begin(), allCollections.end());
    set<long> otherCollectionIds = collectionIds;
    otherCollectionIds.erase(photoCollectionId);

    return otherCollectionIds;
}

void PhotoCollectionHelper::splitSet(const std::set<long> &origin, std::set<long> &set1, std::set<long> &set2, int splitNum){
    int count =0;
    set<long>::const_iterator iter = origin.begin();
    while(iter != origin.end()){
        if(count < splitNum){
            set1.insert(*iter);
        }
        else{
            set2.insert(*iter);
        }
        ++iter;
        ++count;
    }
}

std::set<long>
PhotoCollectionHelper::insertCollections(int collectionsNumber, long parentId, long ctxId){
    DataTransaction transaction(Data::instance());

    set<long> collectionIds;
    for(int i=0; i < collectionsNumber; ++i){
        long id = insertCollection(makeCollection(i), parentId, ctxId);
        collectionIds.insert(id);
    }

    transaction.commit();

    return collectionIds;
}

long PhotoCollectionHelper::insertCollection(long parentId, long ctxId)
{
    return insertCollection(PhotoCollection(), parentId, ctxId);
}

long PhotoCollectionHelper::insertCollection(const PhotoCollection & photoCollection, long parentId, long ctxId)
{
    PhotoCollection coll = photoCollection;
    if(ctxId > PhotoContext::INVALID_ID){
        coll.setParentContext(ctxId);
    }

    if(parentId >= 0){
        return Data::instance().collections().insert(coll, parentId);
    }
    else{
        long rootCollId = Data::instance().contexts().getRootCollectionId(ctxId);
        if(rootCollId > PhotoCollection::INVALID_ID){
            return Data::instance().collections().insert(coll, rootCollId);
        }
        else{
            return Data::instance().collections().insert(coll);
        }
    }
}

PhotoCollection PhotoCollectionHelper::createCollection(const PhotoCollection & photoCollection, long parentId, long ctxId){
    return Data::instance().collections().get(insertCollection(photoCollection, parentId, ctxId));

}

PhotoCollection PhotoCollectionHelper::createCollectionWithSubCollections(const std::set<long> &subcollectionIds){
    PhotoCollection photoCollection;
    photoCollection.setSubCollections(subcollectionIds);
    long photoCollectionId = Data::instance().collections().insert(photoCollection);
    photoCollection.setId(photoCollectionId);

    return photoCollection;
}

PhotoCollection PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(int numPhotos, int numSubCollections
    , long collParent, long ctxId)
{
    DataTransaction transaction(Data::instance());

    long collId = insertCollection(collParent, ctxId);
    insertCollections(numSubCollections, collId, ctxId);

    PhotoCollection coll = insertPhotosAtCollection(collId, numPhotos);

    transaction.commit();

    return coll;
}

PhotoCollection PhotoCollectionHelper::createPhotoHierarchy(int subcollsCount, int photosByCollection, set<long> & outPhotoIds, long ctxId)
{
    PhotoCollection coll = PhotoCollectionHelper
            ::createCollectionWithPhotosAndSubCollections(subcollsCount, photosByCollection,-1, ctxId);
    extractPhotoIds(coll, outPhotoIds);

    const IdCollection::Collection & subcollections = coll.getSubCollections().getIds();
    for(IdCollection::Collection::const_iterator iter = subcollections.begin();
        iter != subcollections.end(); ++iter)
    {
        PhotoCollection coll = PhotoCollectionHelper::insertPhotosAtCollection(*iter, photosByCollection);
        extractPhotoIds(coll, outPhotoIds);
    }

    return coll;
}

PhotoCollection PhotoCollectionHelper::insertPhotosAtCollection(long collId, int numPhotos)
{
    DataTransaction transaction(Data::instance());

    set<long> photoIds = PhotoHelper::insertPhotos(numPhotos);

    //Encapsula coleção em um conjunto (para poder adicionar fotos)
    set<long> collectionSet;
    collectionSet.insert(collId);

    Data::instance().collections().addPhotos(collectionSet, photoIds);

    transaction.commit();

    return Data::instance().collections().get(collId);
}

void PhotoCollectionHelper::extractPhotoIds(const PhotoCollection & coll, std::set<long> & outIds){
    const std::set<long> & photoIds = coll.getPhotos();
    for(std::set<long>::const_iterator iter = photoIds.begin(); iter != photoIds.end(); ++iter){
        outIds.insert(*iter);
    }
}

PhotoCollection PhotoCollectionHelper::makeCollection(int collNumber){
    PhotoCollection collection;
    collection.setName(string("Collection ").append(StringCast::instance().toString(collNumber)));
    return collection;
}

void PhotoCollectionHelper::assertAllCollectionsContainsItems(const std::set<long> &collections, const std::set<long> &photos, const std::set<long> &subcollections)
{
    checkCollectionsContainsItems(collections, photos, subcollections, true);
}

void PhotoCollectionHelper::assertAllCollectionsNotContainsItems(const std::set<long> &collections, const std::set<long> &photos, const std::set<long> &subcollections)
{
    checkCollectionsContainsItems(collections, photos, subcollections, false);
}

void PhotoCollectionHelper::checkCollectionsContainsItems(const std::set<long> &collectionIds, const std::set<long> &photos, const std::set<long> &subcollections, bool shouldContain)
{
    INFO("photos to check: " << setAsString(photos));
    INFO("subcollections to check: " << setAsString(subcollections));

    std::vector<PhotoCollection> collections = Data::instance().collections().listIn(collectionIds);
    for(size_t i =0; i < collections.size(); ++i){
        const PhotoCollection & coll = collections[i];
        INFO("collection(" << coll.getId() << ") photos: " << setAsString(coll.getPhotos()));
        if(shouldContain){
            CHECK(isSuperset(coll.getPhotos(), photos));
        }
        else{
            CHECK(isDisjointSet(coll.getPhotos(), photos));
        }

        INFO("collection(" << coll.getId() << ") subcollections: " << setAsString(coll.getSubCollections().getIds()));
        if(shouldContain){
            CHECK(isSuperset(coll.getSubCollections().getIds(), subcollections));
        }
        else{
            CHECK(isDisjointSet(coll.getSubCollections().getIds(), subcollections));
        }
    }
}

void PhotoCollectionHelper::checkEquals(const PhotoCollection &coll1, const PhotoCollection &coll2){
    CHECK(coll1.getId() == coll2.getId());
    CHECK(coll1.getName() == coll2.getName());
    CHECK(coll1.getParentContext() == coll2.getParentContext());
    CHECK(coll1.getPhotos() == coll2.getPhotos());
    CHECK(coll1.getSubCollections() == coll2.getSubCollections());
}

string PhotoCollectionHelper::setAsString(const std::set<long> &someSet)
{
    stringstream sstream;

    sstream << "{";

    std::ostream_iterator<long> outIter (sstream," ");
    std::copy(someSet.begin(), someSet.end(), outIter);

    sstream << "}";

    return sstream.str();
}
