#ifndef STEPEDRUNNABLE_H
#define STEPEDRUNNABLE_H

#include "Poco/Mutex.h"
#include "Poco/Condition.h"
#include "Poco/Runnable.h"
#include "Poco/AtomicCounter.h"

namespace calmframe {
namespace tests {

class ResetableSemaphore {
public:
    ResetableSemaphore();
    ResetableSemaphore(int startCount, int maxCount);

    void wait(Poco::Mutex & mutex);
    void wait(Poco::Mutex & mutex, long millis);
    bool tryWait(Poco::Mutex & mutex, long millis);
    void signal();
    
    void signalAll();
private:
    const int maxCount;
    Poco::AtomicCounter waitingCount;
    Poco::Condition waitingCondition;
};

class StepedRunnable : public Poco::Runnable{
private:
    volatile bool finished;
    Poco::Mutex mutex;

    ResetableSemaphore waitForCallerThread;
    ResetableSemaphore waitForStep;

    volatile bool forceFinished;

    volatile long timeoutMillis;
public:
    StepedRunnable();
    virtual ~StepedRunnable();

    long getTimeoutMillis() const;
    void setTimeoutMillis(long millis);

    bool wasForceFinished() const;

    virtual void run();

    virtual void reset();

    //Thread externa espera runnable executar
    virtual bool waitStep(long waitTime = 0);

    virtual bool nextStep();
    virtual bool waitNextStep(long maxWaitTime=0);

    //Thread externa desbloqueia runnable
    void unblockRunnable();
    virtual void finish();
    virtual bool hasFinished();
protected:
    /** Override on subclasses.
     * Should return true if still has steps to execute or false to finish execution after this method.*/
    virtual bool executeStep()=0;
    void forceFinish();
    void finishDueException(const std::string & exceptionMsg);
private:
    void waitExternal();
    void unblockExternal();
};

}//namespace
}//namespace

#endif // STEPEDRUNNABLE_H
