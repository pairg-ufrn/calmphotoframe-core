#ifndef SCOPEDTEMPDIR_H
#define SCOPEDTEMPDIR_H

#include "utils/ScopedFile.h"

class ScopedTempDir : public ScopedFile{
public:
    using super = ScopedFile;
    
    ScopedTempDir(const std::string & basePath=std::string());
};

#endif // SCOPEDTEMPDIR_H
