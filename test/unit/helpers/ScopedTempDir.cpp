#include "ScopedTempDir.h"

#include "utils/FileUtils.h"

using namespace calmframe::utils;

static std::string createTempDir(const std::string & basePath){
    auto tmpPath = Files().generateTmpFilepath();
    Files().createDir(tmpPath);
    
    return tmpPath;
}

ScopedTempDir::ScopedTempDir(const std::string & basePath)
    : super(createTempDir(basePath), true)
{}
