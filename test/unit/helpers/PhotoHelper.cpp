
#include "PhotoHelper.h"

#include "data/Data.h"
#include "data/DataTransaction.h"

#include "catch.hpp"
#include "helpers/MathUtils.h"

#include <vector>
#include <algorithm>

#include "utils/CommonExceptions.h"

using namespace std;
using std::stringstream;
using namespace calmframe;
using namespace calmframe::data;

template<typename T>
bool outField(ostream & out, const std::string & str, const T & value, bool condition, bool & first){
    if(condition){
        if(!first){
            out << ",";
        }
        out << " " << str << ": " << value;

        first = false;
    }
    return first;
}

bool outField(ostream & out, const std::string & str, const std::string & value, bool & first){
    first &= outField(out, str, "\"" + value + "\"", !value.empty(), first);

    return first;
}

bool outField(ostream & out, const std::string & str, const std::string & value){
    bool first = false;

    return outField(out, str, value, first);
}

ostream &operator<<(ostream & out, const Photo & photo){
    out << "{";

    out << " id: " << photo.getId();
    outField(out, "imagePath", photo.getImagePath());
    out << ", description: " << photo.getDescription();
//    out << ", metadata: " << "{...}";
    out << "}";
    return out;
}

ostream &operator<<(ostream & out, const PhotoDescription & desc){
    out << "{";

    bool first = true;
    outField(out, "moment", desc.getMoment(), first);
    outField(out, "date", desc.getDate(), first);
    outField(out, "place", desc.getPlace(), first);
    outField(out, "desc", desc.getDescription(), first);
    outField(out, "credits", desc.getCredits(), first);
    outField(out, "markers", desc.getMarkers(), desc.getMarkerCount() > 0, first);

    out << "}";
    return out;
}

ostream &operator<<(ostream & out, const PhotoMarker & marker){
    out << "{";
    out << " idx: " << marker.getIndex();
    out << " name: " << marker.getName();
    out << " x: " << marker.getRelativeX();
    out << " y: " << marker.getRelativeY();
    out << "}";
    return out;
}

std::set<long> PhotoHelper::insertPhotos(int photoNumber, const Photo & basePhoto){
    set<long> photoIds;

    stringstream strStream;

    data::DataTransaction transaction(data::Data::instance());

    for(int i = 0; i < photoNumber; ++i){
        Photo photo = basePhoto;
        //photo.setId(i);

        if(photo.getImagePath().empty()){
            strStream.str("");
            strStream << "Image/Path/"<<i;
            photo.setImagePath(strStream.str());
        }

        long insertedId = calmframe::data::Data::instance().photos().insert(photo);

        CHECK_ASSERTION(insertedId > 0);

        photoIds.insert(insertedId);
    }

    transaction.commit();

    return photoIds;
}

void PhotoHelper::insertAll(std::vector<Photo> & photos){
    data::DataTransaction transaction(data::Data::instance());
    for(Photo & photo : photos){
        long id = insertPhoto(photo);
        photo.setId(id);
    }
    transaction.commit();
}


Photo PhotoHelper::createPhoto(unsigned id){
    Photo photo;
    photo.setId(id);
    photo.getDescription().setMoment("random Moment");
    photo.getDescription().setCredits("Anonymous author.");
    return photo;
}

//bool compareById(const PhotoMarker & markerA, const PhotoMarker & markerB){
//    return markerA.getId() < markerB.getId();
//}

void PhotoHelper::assertEquals(const Photo &photo, const Photo &otherPhoto)
{
    //CHECK(otherPhoto.getId() == photo.getId());
    CHECK(otherPhoto.getDescription().getDescription()  == photo.getDescription().getDescription());
    CHECK(otherPhoto.getDescription().getDate()         == photo.getDescription().getDate());
    CHECK(otherPhoto.getDescription().getMoment()       == photo.getDescription().getMoment());
    CHECK(otherPhoto.getDescription().getPlace()        == photo.getDescription().getPlace());
    CHECK(otherPhoto.getDescription().getCredits()      == photo.getDescription().getCredits());

    vector<PhotoMarker> markers = photo.getDescription().getMarkers();
    vector<PhotoMarker> otherMarkers = photo.getDescription().getMarkers();

    REQUIRE(otherMarkers.size() == markers.size());

    std::sort(markers.begin(), markers.end());
    std::sort(otherMarkers.begin(), otherMarkers.end());

    for(int i=0; i < markers.size(); ++i){
        CHECK(otherMarkers[i].getName() == markers[i].getName());
        CHECK(MathUtils::diff(otherMarkers[i].getRelativeX(), markers[i].getRelativeX()) < EPSILON);
        CHECK(MathUtils::diff(otherMarkers[i].getRelativeY(), markers[i].getRelativeY()) < EPSILON);
    }
}

long PhotoHelper::insertPhoto(const Photo & photo)
{
    return Data::instance().photos().insert(photo);
}

long PhotoHelper::insertPhotoAtContext(long ctxId, Photo photo){
    photo.setContextId(ctxId);
    return insertPhoto(photo);
}

std::set<long> PhotoHelper::insertPhotosAtContext(int photoNumber, long ctxId){
    return insertPhotos(photoNumber, Photo{{},{}, ctxId});
}

void PhotoHelper::assertExist(const Photo & photo)
{
    CAPTURE(photo.getId());
    CHECK(Data::instance().photos().exist(photo.getId()));
}

void PhotoHelper::assertEquals(const Photo &photo, const Json::Value &photoJson)
{
    CHECK(photoJson[PhotoFields::ID].asInt64() == photo.getId());
    assertDescriptionEquals(photo, photoJson);
    assertMarkersEquals(photo, photoJson);
}

void PhotoHelper::assertDescriptionEquals(const Photo &photo, const Json::Value &photoJson)
{
    CHECK(photoJson[PhotoFields::DESCRIPTION]  == photo.getDescription().getDescription());
    CHECK(photoJson[PhotoFields::DATE]         == photo.getDescription().getDate());
    CHECK(photoJson[PhotoFields::MOMENT]       == photo.getDescription().getMoment());
    CHECK(photoJson[PhotoFields::PLACE]        == photo.getDescription().getPlace());
    CHECK(photoJson[PhotoFields::CREDITS]      == photo.getDescription().getCredits());
}

void PhotoHelper::assertMarkersEquals(const Photo &photo, const Json::Value &photoJson)
{
    Json::Value markersJson = photoJson[PhotoFields::MARKERS];

    vector<PhotoMarker> markers = photo.getDescription().getMarkers();
    REQUIRE(markersJson.size() == markers.size());
    for(int i=0; i < markers.size(); ++i){
        unsigned jsonIndex  = markersJson[i].get(PhotoMarkerFields::INDEX, 0u).asInt();
        unsigned photoIndex = markers[i].getIndex();
        CHECK(jsonIndex == photoIndex);

        CHECK(markersJson[i][PhotoMarkerFields::IDENTIFICATION] == markers[i].getName());
        CHECK(MathUtils::diff(markersJson[i][PhotoMarkerFields::X].asFloat(), markers[i].getRelativeX()) < EPSILON);
        CHECK(MathUtils::diff(markersJson[i][PhotoMarkerFields::Y].asFloat(), markers[i].getRelativeY()) < EPSILON);
    }
}


Photo && PhotoHelper::insert(Photo && photo){
    long photoId = insertPhoto(photo);
    photo.setId(photoId);

    return std::move(photo);
}
