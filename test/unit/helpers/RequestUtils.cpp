#include "RequestUtils.h"
#include "network/StatusResponses.h"
#include "network/ContentTypes.hpp"

#include "json/json.h"
#include "catch.hpp"

#include "utils/StringUtils.h"

using namespace std;
using namespace calmframe;
using namespace network;

using calmframe::utils::StringUtils;

Json::Value RequestUtils::assertJsonObjectResponse(MockResponse &response){
    CHECK(response.getContentType().getType() == "application");
    CHECK(response.getContentType().getSubtype() == "json");

    string responseContent = response.getContentStr();
    Json::Value tmpJson;
    Json::Reader reader;
    reader.parse(responseContent, tmpJson);

    CHECK(responseContent.empty() == false);
    REQUIRE(tmpJson.type() == Json::objectValue);

    return tmpJson;
}


void RequestUtils::assertMessageStatus(MockResponse &response, int statusCode){
    CHECK(response.statusLine().getStatusCode() == statusCode);
    string headerContent = response.getHeadersStr();
    REQUIRE(!headerContent.empty());
    vector<string> lines = StringUtils::instance().split(headerContent,'\n');

    for(int i=0; i < lines.size(); ++i){
        INFO( "line " << i << ": " << lines[i]);
    }

    REQUIRE(lines.size() >= 2);

    string httpVersion = "HTTP/1.1 ";
    CHECK(StringUtils::instance().stringStartsWith(lines[0], httpVersion));
}


void RequestUtils::assertOkMessage(MockResponse &response){
    assertMessageStatus(response, StatusResponses::OK.getStatusCode());
}

void RequestUtils::assertJsonMessage(MockResponse &response)
{
    CHECK(response.getContentType().is(Types::APPLICATION, SubTypes::JSON));
}

void RequestUtils::assertOkJsonResponse(Response & response)
{
    INFO("Content type: " << response.getContentType().toString());
    CHECK(response.getContentType().is(ContentTypes::JSON));
    INFO("Status code: " << response.statusLine().getStatusCode());
    CHECK(response.statusLine().getStatusCode() == StatusResponses::OK.getStatusCode());
}

bool RequestUtils::isJsonObjectMessage(const string &msgContent)
{
    Json::Reader reader;
    Json::Value root;
    if(reader.parse(msgContent, root)){
        return root.type() == Json::objectValue;
    }
    return false;
}

void RequestUtils::assertBadRequestResponse(MockResponse & response)
{
    assertMessageStatus(response, StatusResponses::BAD_REQUEST.getStatusCode());
}

void RequestUtils::assertNotFoundResponse(MockResponse & response)
{
    assertMessageStatus(response, StatusResponses::NOT_FOUND.getStatusCode());
}


std::map<std::string, std::string> &RequestUtils::extractMessageHeaders(MockResponse &response, std::map<std::string, std::string> &headers){
    std::vector<std::string> lines = StringUtils::instance().split(response.getHeadersStr(),'\n', StringUtils::SPLIT_TRIM);

    //Ignora primeira (status line) e última linha ("\r\n")
    for(int i =1; i<lines.size() - 1; ++i){
        std::string name, value;
        StringUtils::instance().split(lines[i],':',&name,&value,StringUtils::SPLIT_TRIM);
        headers[name] = value;
    }

    return headers;
}


void RequestUtils::assertMessageStatus(MockResponse & response, const calmframe::network::StatusResponse & statusCode){
    assertMessageStatus(response, statusCode.getStatusCode());
}
