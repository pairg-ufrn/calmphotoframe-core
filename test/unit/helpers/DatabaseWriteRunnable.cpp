#include "DatabaseWriteRunnable.h"

#include "data/data-sqlite/dao/SqliteUserDAO.h"
#include "utils/StringCast.h"
#include "utils/Log.h"

#include <time.h>

namespace calmframe {
namespace tests {

using namespace std;
using namespace Poco;
using namespace calmframe::data;

DatabaseWriteRunnable::DatabaseWriteRunnable(const std::string &databasePath)
    : database(databasePath)
    , count(0)
    , writing(false)
    , createdId(0)
{}

DatabaseWriteRunnable::~DatabaseWriteRunnable(){
    try{
        database.close();
    }
    catch(const std::exception & ex){
        logError("DatabaseWriteRunnable::destructor:\t"
                 "Failed to close database due to exception: %s", ex.what());
    }
    catch(...){
        logError("DatabaseWriteRunnable::destructor:\t" "Failed to close database due to unknown error.");
    }
}

bool DatabaseWriteRunnable::isWriting(){
    bool locked = isWritingMutex.tryLock();
    if(!locked){//Other thread is writing if mutex is locked
        return true;
    }

    bool isWriting = this->writing;
    isWritingMutex.unlock();

    return isWriting;
}

bool DatabaseWriteRunnable::isWaitingToWrite(){
    return database.isOnTransaction() && isWriting();
}

void DatabaseWriteRunnable::waitFinishWrite(unsigned maxWaitTime){
    if(maxWaitTime > 0){
        isWritingMutex.lock(maxWaitTime);
    }
    else{
        isWritingMutex.lock();
    }

    isWritingMutex.unlock();
}

bool DatabaseWriteRunnable::verifyDatabaseChanges(){
    SqliteUserDAO userDao(&database);
    return userDao.exist(createdId);
}

bool DatabaseWriteRunnable::executeStep(){
    if(count >= 2){
        return false;
    }

    ++count;
    if(count == 1){
        startWriting();
        return true;
    }
    else if(count == 2){
        finishWriting();
        return false;
    }

}

void DatabaseWriteRunnable::startWriting(){
    ScopedLock<Mutex> isWritingScopedLock(isWritingMutex);
    this->writing = true;
    database.beginTransaction();
    try{
        createdId = writeToDatabase();
    }catch(const std::exception & ex){
        logError("Failed to write to database, due to exception: \n\t" "%s", ex.what());
        writing = false;
        database.rollback();
    }
}

void DatabaseWriteRunnable::finishWriting(){
    ScopedLock<Mutex> isWritingScopedLock(isWritingMutex);
    database.endTransaction();
    this->writing = false;
}

std::string DatabaseWriteRunnable::generateUniqueName()
{
    return calmframe::utils::StrCast().toString(clock());
}

long DatabaseWriteRunnable::writeToDatabase(){
    SqliteUserDAO userDAO(&database);
    UserAccount newUser;
    string generatedName = generateUniqueName();
    newUser.setLogin(generatedName);
    return userDAO.insert(newUser);
}


}//namespace
}//namespace
