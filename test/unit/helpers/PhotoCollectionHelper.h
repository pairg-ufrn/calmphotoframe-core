#ifndef PHOTOCOLLECTIONHELPER_H
#define PHOTOCOLLECTIONHELPER_H

#include <set>
#include "model/photo/PhotoCollection.h"

class PhotoCollectionHelper
{
public:
    PhotoCollectionHelper();
    ~PhotoCollectionHelper();

    static std::set<long> getOtherCollectionIds(long photoCollectionId);

    template<typename Iterator>
    static std::set<long> getIds(Iterator begin, Iterator end){
        std::set<long> ids;

        Iterator iter = begin;
        while(iter != end){
            ids.insert((*iter).getId());
            ++iter;
        }
        return ids;
    }
    static void splitSet(const std::set<long> & origin, std::set<long> & set1, std::set<long> & set2, int splitNum);
    static std::set<long> insertCollections(int collectionsNumber, long parentId=-1, long ctxId=-1);
    static long insertCollection(long parentId = -1, long ctxId=-1);
    static long insertCollection(const calmframe::PhotoCollection &photoCollection, long parentId = -1, long ctxId=-1);
    static calmframe::PhotoCollection createCollection(const calmframe::PhotoCollection &photoCollection, long parentId = -1, long ctxId=-1);
    static calmframe::PhotoCollection createCollectionWithSubCollections(const std::set<long> & subcollectionIds);
    static calmframe::PhotoCollection createCollectionWithPhotosAndSubCollections(int numPhotos, int numSubCollections
        , long collParent=-1, long ctxId=-1);

    static calmframe::PhotoCollection createPhotoHierarchy(
        int subcollsCount, int photosByCollection, std::set<long> & outPhotoIds, long ctxId=-1);

    static void extractPhotoIds(const calmframe::PhotoCollection & coll, std::set<long> & outIds);

    static calmframe::PhotoCollection insertPhotosAtCollection(long collId, int numPhotos);

    static calmframe::PhotoCollection makeCollection(int collNumber);

    static void assertAllCollectionsContainsItems(const std::set<long> & collections
                                            , const std::set<long> & photos, const std::set<long> & subcollections);
    static void assertAllCollectionsNotContainsItems(const std::set<long> & collections
                                            , const std::set<long> & photos, const std::set<long> & subcollections);
    static void checkCollectionsContainsItems(const std::set<long> & collections
                                            , const std::set<long> & photos, const std::set<long> & subcollections
                                            , bool shouldContain);
    static void checkEquals(const calmframe::PhotoCollection & coll1
                          , const calmframe::PhotoCollection & coll2);

    static std::string setAsString(const std::set<long> & someSet);

    template<typename T>
    static bool isDisjointSet(const std::set<T> & superset, const std::set<T> & otherSet);
    template<typename T>
    static bool isSuperset(const std::set<T> & superset, const std::set<T> & otherSet);
};

template<typename T>
bool PhotoCollectionHelper::isDisjointSet(const std::set<T> & set1
                                     , const std::set<T> & set2)
{
    typename std::set<T>::const_iterator iter = set1.begin();
    while(iter != set1.end()){
        if(set2.find(*iter) != set2.end()){
            return false;
        }
        ++iter;
    }
    return true;
}

template<typename T>
bool PhotoCollectionHelper::isSuperset(const std::set<T> &superset, const std::set<T> &otherSet)
{
    typename std::set<T>::const_iterator iter = otherSet.begin();
    while(iter != otherSet.end()){
        if(superset.find(*iter) == superset.end()){
            return false;
        }
        ++iter;
    }
    return true;
}

#endif // PHOTOCOLLECTIONHELPER_H
