#include "StepedRunnable.h"

#include "utils/FileUtils.h"
#include "utils/Log.h"

#include "Poco/Thread.h"

#include "utils/CommonExceptions.h"

using namespace Poco;
using namespace calmframe::utils;

namespace calmframe {
namespace tests {

ResetableSemaphore::ResetableSemaphore()
    : maxCount(1), waitingCount(0)
{}
ResetableSemaphore::ResetableSemaphore(int startCount, int maxCount)
    : maxCount(maxCount), waitingCount(startCount)
{}

void ResetableSemaphore::wait(Poco::Mutex & mutex){
    wait(mutex, 0);
}
void ResetableSemaphore::wait(Poco::Mutex & mutex, long millis){
    if(--waitingCount < 0){
        waitingCondition.wait(mutex, millis);
    }

}
bool ResetableSemaphore::tryWait(Poco::Mutex & mutex,long millis){
    if(--waitingCount < 0){
        return waitingCondition.tryWait(mutex, millis);
    }

    return true;
}
void ResetableSemaphore::signal(){
    if(++waitingCount == 0){
        waitingCondition.signal();
    }
    else if(waitingCount > maxCount){
        //throw Exception("Unblocked runnable more than once");  
        logWarning("waiting Count bigger than max: %d", (int)waitingCount);
        throw CommonExceptions::Exception("");
    }
}

void ResetableSemaphore::signalAll(){
    waitingCondition.broadcast();
    waitingCount = 0;
}



StepedRunnable::StepedRunnable()
    : finished(false)
    , forceFinished(false)
    , timeoutMillis(5000)
{}

StepedRunnable::~StepedRunnable(){
    forceFinish();
}

long StepedRunnable::getTimeoutMillis() const{
    return timeoutMillis;
}
void StepedRunnable::setTimeoutMillis(long millis){
    this->timeoutMillis = millis;
}

bool StepedRunnable::wasForceFinished() const{
    return forceFinished;
}
void StepedRunnable::run(){
    try{
        bool continueSteps = false;
        do{
            continueSteps = executeStep();
            unblockExternal();
            waitExternal();
        }while(!finished && continueSteps);
        finished = true;
    }catch(const std::exception & ex){
        finishDueException(ex.what());
        throw;
    }catch(...){
        finishDueException("unknown");
        throw;
    }
}

void StepedRunnable::reset(){
    ScopedLock<Mutex> lock(mutex);
    finished = false;
    forceFinished = false;
}

bool StepedRunnable::nextStep(){
    unblockRunnable();
    return !finished;
}

bool StepedRunnable::waitNextStep(long maxWaitTime){
    nextStep();
    return waitStep(maxWaitTime);
}

bool StepedRunnable::waitStep(long waitTime){
    waitTime = waitTime <= 0 ? getTimeoutMillis() : waitTime;

    ScopedLock<Mutex> lock(mutex);
    waitForStep.wait(mutex, waitTime);

    return true;
}

void StepedRunnable::unblockExternal(){
    ScopedLock<Mutex> lock(mutex);
    waitForStep.signal();
}

void StepedRunnable::waitExternal(){
    ScopedLock<Mutex> lock(mutex);
    waitForCallerThread.wait(mutex, getTimeoutMillis());
}

void StepedRunnable::unblockRunnable(){
    ScopedLock<Mutex> lock(mutex);
    waitForCallerThread.signal();
}


void StepedRunnable::finish(){
    ScopedLock<Mutex> lock(mutex);
    finished = true;
    nextStep();
}

bool StepedRunnable::hasFinished(){
    ScopedLock<Mutex> lock(mutex);
    return finished;
}

void StepedRunnable::forceFinish(){
    bool locked = mutex.tryLock();
    forceFinished = true;
    waitForCallerThread.signalAll();
    waitForStep.signalAll();
    if(locked){
        mutex.unlock();    
    }
}

void StepedRunnable::finishDueException(const std::string &exceptionMsg){
    logError("When running thread %lu catch exception: \n\t" "%s",
             (unsigned long)Thread::currentTid(), exceptionMsg.c_str());
    forceFinish();
}

}//namespace
}//namespace
