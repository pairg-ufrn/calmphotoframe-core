#include "PermissionHelper.h"

#include "catch.hpp"

namespace calmframe {

void PermissionHelper::checkEquals(const Permission & lhs, const Permission & rhs){
    CHECK(rhs.getContextId()        == lhs.getContextId());
    CHECK(rhs.getUserId()           == lhs.getUserId());
    CHECK(rhs.getPermissionLevel()  == lhs.getPermissionLevel());
}

}//namespace
