#include "PhotoContextHelper.h"

#include <sstream>

#include "data/Data.h"
#include "data/DataTransaction.h"

#include "catch.hpp"

#include "helpers/PhotoCollectionHelper.h"
#include "utils/TimeUtils.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;

PhotoContext PhotoContextHelper
::insertCtxWithCollections(const std::string & ctxName, int collCount, std::set<long> * outCollIds)
{
    PhotoContext createdContext = insertContextWithRootCollection(ctxName);
    long ctxId = createdContext.getId();
    std::set<long> collIds = PhotoCollectionHelper::insertCollections(collCount, -1, ctxId);
    collIds.insert(createdContext.getRootCollection());

    if(outCollIds != NULL){
        *outCollIds = collIds;
    }

    return createdContext;
}

void PhotoContextHelper::createContexts(int ctxCount, std::vector<calmframe::PhotoContext> & outContexts){
    stringstream sstream;

    for(int i=0; i < ctxCount; ++i){
        sstream << "Context " << i + 1;
        PhotoContext ctx;
        ctx.setName(sstream.str());
        outContexts.push_back(ctx);

        sstream.str("");
    }
}

std::string PhotoContextHelper::makeCtxName()
{
    return "Context " + TimeUtils::instance().getCurrentDateTime();
}

PhotoContext PhotoContextHelper::createContextHierarchy(){
    DataTransaction transaction(Data::instance());

    set<long> createdColls;
    PhotoContext createdCtx = PhotoContextHelper::insertCtxWithCollections(
                                    makeCtxName(), 2, &createdColls);

    long someCollectionID = *createdColls.begin();
    PhotoCollectionHelper::insertCollections(2, someCollectionID, createdCtx.getId());

    transaction.commit();
    return createdCtx;
}

PhotoContext PhotoContextHelper::insertContext()
{
    string ctxName = makeCtxName();
    return insertContext(ctxName);
}

PhotoContext PhotoContextHelper::insertContext(const std::string & ctxName)
{
    PhotoContext ctx(ctxName);
    return insertContext(ctx);
}

PhotoContext &PhotoContextHelper::insertContext(PhotoContext &context)
{
    long id = Data::instance().contexts().insert(context);
    context.setId(id);
    return context;
}

PhotoContext &&PhotoContextHelper::insertContext(PhotoContext && context)
{
    return std::move(insertContext((PhotoContext &) context));
}

PhotoContext PhotoContextHelper::insertContextWithRootCollection(){
    return insertContextWithRootCollection({makeCtxName()});
}


PhotoContext PhotoContextHelper::insertContextWithRootCollection(const PhotoContext & context)
{
    PhotoContext ctx(context);

    long collId = PhotoCollectionHelper::insertCollection();
    ctx.setRootCollection(collId);
    insertContext(ctx);

    PhotoCollection coll = Data::instance().collections().get(collId);
    coll.setParentContext(ctx.getId());
    Data::instance().collections().update(coll);

    return ctx;
}

void PhotoContextHelper::insertContexts(ContextList & contexts){
    DataTransaction transaction(Data::instance());

    for(size_t i=0; i < contexts.size(); ++i){
        PhotoContext & context = contexts[i];
        insertContext(context);
    }

    transaction.commit();
}

PhotoContextHelper::ContextList && PhotoContextHelper::insertContexts(PhotoContextHelper::ContextList && contexts){
    insertContexts((std::vector<PhotoContext> &)contexts);

    return std::move(contexts);
}

PhotoContextHelper::ContextList PhotoContextHelper::insertContexts(int ctxCount)
{
    vector<PhotoContext> contexts;
    insertContexts(ctxCount, contexts);

    return contexts;
}

PhotoContextHelper::ContextList & PhotoContextHelper::insertContexts(int ctxCount, ContextList & outContexts){
    createContexts(ctxCount, outContexts);
    insertContexts(outContexts);

    return outContexts;
}

void PhotoContextHelper::checkCollectionContainsContexts(const std::vector<PhotoContext> &collection, const std::vector<PhotoContext> &containedCtxs)
{
    std::map<long, size_t> ctxIndexMap;
    for(size_t i =0; i < collection.size(); ++i){
        ctxIndexMap[collection[i].getId()] = i;
    }
    for(size_t i =0; i < containedCtxs.size(); ++i){
        const PhotoContext & ctx = containedCtxs[i];
        long ctxId = ctx.getId();
        INFO("Context id: " << ctxId);
        std::map<long, size_t>::const_iterator it = ctxIndexMap.find(ctxId);
        bool containsContext = (it != ctxIndexMap.end());
        CHECK(containsContext);

        if(it != ctxIndexMap.end()){
            checkEquals(collection[it->second], ctx);
        }
    }

}

void PhotoContextHelper::checkEquals(const PhotoContext &ctx1, const PhotoContext &ctx2){
    CHECK(ctx1.getName() == ctx2.getName());
    CHECK(ctx1.getId() == ctx2.getId());
    CHECK(ctx1.getVisibility() == ctx2.getVisibility());
    CHECK(ctx1.getRootCollection() == ctx2.getRootCollection());
}

void PhotoContextHelper::checkEquals(std::vector<PhotoContext> & contexts1, std::vector<PhotoContext> & contexts2
    , bool detailedCheck)
{
    std::sort(contexts1.begin(), contexts1.end());
    std::sort(contexts2.begin(), contexts2.end());

    CHECK(contexts1.size() == contexts2.size());
    for(int i=0; i < contexts1.size(); ++i){
        CAPTURE(i);
        if(!detailedCheck){
            CHECK(contexts1[i] == contexts2[i]);
        }
        else{
            checkEquals(contexts1[i], contexts2[i]);
        }
    }
}
