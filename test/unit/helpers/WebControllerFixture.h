#ifndef WEBCONTROLLERFIXTURE_H
#define WEBCONTROLLERFIXTURE_H

#include "catch.hpp"

#include "network/ContentTypes.hpp"
#include "network/Url.h"
#include "network/controller/WebController.h"
#include "serialization/json/JsonSerializer.h"

#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"

using calmframe::network::WebController;
using calmframe::network::ContentType;

class WebControllerFixture
{
private:
    WebController * controller;
public:
    MockRequest request;
    MockResponse response;

    JsonSerializer serializer;
public:

    WebControllerFixture(WebController * controller=NULL)
        : controller(controller)
    {
    }

    template<typename ... T>
    std::string route(const T& ... args){
        std::stringstream sstream;

        route((std::ostream &)sstream, args...);

        return sstream.str();
    }

    template<typename T, typename ... Args>
    std::ostream & route(std::ostream & out, const T & path,const Args& ... args){
        route(out, path);
        route(out, args...);
        return out;
    }
    template<typename T>
    std::ostream & route(std::ostream & out, const T& arg){
        out << "/" << arg;

        return out;
    }

    template<typename T>
    T getResponse(){
        T item;
        this->serializer.clear();
        readResponse() >> item;

        return item;
    }

    Deserializer & readResponse(){
        this->response.getContentStr() >> this->serializer;
        return serializer;
    }

    template<typename T>
    std::vector<T> deserializeItems(const Json::Value & jsonContent
                                            , const std::string & fieldName)
    {
        serializer.clear();
        jsonContent >> serializer;

        return deserializeItems<T>(fieldName);
    }

    template<typename T>
    std::vector<T> deserializeItems(const std::string & fieldName)
    {
        std::vector<T> deserializedItems;
        std::back_insert_iterator< std::vector<T> > outIterator(deserializedItems);
        serializer.getChildCollection<T>(fieldName, outIterator);

        return deserializedItems;
    }
    template<typename T>
    std::vector<T> deserializeResponseItems(const std::string & fieldName){
        serializer.clear();
        response.getContentStr() >> serializer;

        return deserializeItems<T>(fieldName);
    }
    template<typename OutIterator, typename Converter>
    void deserializeResponseItems(const std::string & fieldName, OutIterator outIter, const Converter & converter){
        serializer.clear();
        response.getContentStr() >> serializer;
        serializer.getChildCollection(fieldName, outIter, converter);
    }

    WebController & getController(){
        CHECK_NOT_NULL(controller);
        return *controller;
    }
    void setController(WebController * controller){
        this->controller = controller;
    }

    void executeGetRequest(const std::string & urlRoute, const MockRequest::ParamMap & queryParams = MockRequest::ParamMap()){
        executeRequest("GET", urlRoute, true, queryParams);
    }
    void executeDeleteRequest(const std::string & urlRoute, Serializable * serializable=NULL){
        executeJsonRequest("DELETE", urlRoute, serializable);
    }
    void executePutRequest(const std::string & urlRoute, Serializable * serializable=NULL){
        executeJsonRequest("PUT", urlRoute, serializable);
    }

    void executePostRequest(const std::string & urlRoute, const Serializable & serializable){
        executePostRequest(urlRoute, &serializable);
    }
    void executePostRequest(const std::string & urlRoute, const Serializable * serializable=NULL){
        executeJsonRequest("POST", urlRoute, serializable);
    }
    void executeJsonRequest(const std::string & method, const std::string & urlRoute, const  Serializable * serializable=NULL){
        putJsonRequestContent(serializable);
        executeRequest(method, urlRoute);
    }

    void executeRequest(const std::string & method, const std::string & urlRoute, bool shouldExecute=true
        , const MockRequest::ParamMap & queryParams = MockRequest::ParamMap())
    {
        request.setMethod(method);
        request.setUrl(urlRoute);

        if(!queryParams.empty()){
            for(auto param: queryParams){
                request.putQueryParameter(param.first, param.second);
            }
        }

        bool executed = getController().onRequest(request, response);
        INFO("Route: " << urlRoute);
        CHECK(executed == shouldExecute);
    }


    void putJsonRequestContent(const Serializable* serializable)
    {
        if(serializable != NULL){
            serializer.clear();
            serializer << * serializable;
            putSerializerContentIntoRequest();
        }
    }
    void putSerializerContentIntoRequest()
    {
        std::stringstream sstream;
        sstream << serializer;
        putRequestContent(sstream, calmframe::network::ContentTypes::JSON);
    }

    void putRequestContent(const std::string & contentStr, const ContentType & contentType)
    {
        request.setContentStr(contentStr);
        request.setContentType(contentType);
    }
    void putRequestContent(const std::istream & content, const ContentType & contentType)
    {
        request.setContent(content);
        request.setContentType(contentType);
    }
};
#endif // WEBCONTROLLERFIXTURE_H
