#include "UserHelper.h"

#include "catch.hpp"

#include "services/AccountManager.h"

#include "data/Data.h"
#include "data/DataTransaction.h"

#include "utils/StringCast.h"

namespace calmframe {

using namespace std;
using namespace data;
using namespace utils;

const std::string UserHelper::DEFAULT_ADMINNAME = "admin";

std::vector<UserAccount> UserHelper::createAccounts(int numUsers)
{
    vector<UserAccount> accounts;

    DataTransaction transaction(Data::instance());

    for(int i=0; i<numUsers; ++i){
        const string name = "user " + StrCast().toString(i);
        accounts.push_back(createAccount(name));
    }

    transaction.commit();
    return accounts;
}

void UserHelper::insertAccount(UserAccount & account)
{
    long userId = Data::instance().users().insert(account);
    account.setId(userId);
}

UserAccount UserHelper::createAccount(const std::string & userName, const string & pass)
{
    return createAccountImpl(userName, pass);
}

UserAccount UserHelper::createAdminAccount(const string & userName, const string & pass)
{
    return createAccountImpl(userName, pass, true);
}

UserAccount UserHelper::createAccountImpl(const string& login, const string& pass, bool isAdmin)
{
    User user{login};
    user.setIsAdmin(isAdmin);
    return createAccount(user, pass);
}


UserAccount UserHelper::createAccount(const User & user, const string & pass)
{
    UserAccount account = AccountManager::makeAccount(user.getLogin(), pass);
    account.getUser() = user;

    insertAccount(account);

    return account;
}

void UserHelper::checkEquals(calmframe::User & userA, calmframe::User & userB)
{
    CHECK(userA.getId()    == userB.getId());
    CHECK(userA.getLogin() == userB.getLogin());
    CHECK(userA.isActive() == userB.isActive());
    CHECK(userA.isAdmin()  == userB.isAdmin());
    CHECK(userA.name()     == userB.name());
    
    CHECK(userA.profileImage() == userB.profileImage());
}

calmframe::User calmframe::UserHelper::getUser(long userId){
    User user;
    Data::instance().users().getUser(userId, user);
    return user;
}

}//namespace

