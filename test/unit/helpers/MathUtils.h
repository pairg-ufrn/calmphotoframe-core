#ifndef MATHUTILS_H
#define MATHUTILS_H

#define EPSILON 0.000001f

class MathUtils{
public:
    static float diff(float a, float b){
        return a > b ? a -b : b -a;
    }
};

#endif // MATHUTILS_H
