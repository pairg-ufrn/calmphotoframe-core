#ifndef USERHELPER_H
#define USERHELPER_H

#include "model/user/UserAccount.h"
#include <vector>

namespace calmframe {

const std::string DEFAULT_USERNAME="usernameDefault";
const std::string DEFAULT_PASSWORD="anyPassword";

class UserHelper
{
public:
    static const std::string DEFAULT_ADMINNAME;

public:
    static User getUser(long userId);
    static void checkEquals(User & userA, User & userB);
    static std::vector<UserAccount> createAccounts(int number);
    static UserAccount createAccount(const User & user, const std::string & pass=DEFAULT_PASSWORD);
    static UserAccount createAccount(const std::string & userName=DEFAULT_USERNAME, const std::string & pass=DEFAULT_PASSWORD);
    static UserAccount createAdminAccount(const std::string & userName=DEFAULT_ADMINNAME, const std::string & pass=DEFAULT_PASSWORD);
    static void insertAccount(UserAccount & account);

protected:
    static UserAccount createAccountImpl(const std::string& userName, const std::string& pass, bool isAdmin = false);
};

}//namespace

#endif // USERHELPER_H
