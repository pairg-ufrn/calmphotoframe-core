#ifndef SESSIONHELPER_H
#define SESSIONHELPER_H

#include <string>
#include "mocks/MockRequest.hpp"
#include "model/user/Session.h"
#include "model/permissions/Permission.h"

using calmframe::network::Request;

class SessionHelper{
public:
    /** Create a user session, put the token cookie on request and return the user id*/
    static long setupSessionToken(MockRequest & request);
    static long putUserWithPermission(MockRequest & request, long ctxId, calmframe::PermissionLevel permissionLevel);

    static void putTokenCookie(const std::string & token, MockRequest & request);
    static void putTokenHeader(const std::string & token, MockRequest & request);
    static void putTokenQueryParameter(const std::string & token, MockRequest & request);
    static calmframe::Session createValidSession();

    /** Create context and setup a user on request with the requiredPermission on that context.
        @return the id of the created context
    */
    static long requireContextPermission(MockRequest & request, calmframe::PermissionLevel requiredPermission);
};


#endif // SESSIONHELPER_H

