#ifndef SCOPEDSTEPEDRUNNABLEEXECUTION_H
#define SCOPEDSTEPEDRUNNABLEEXECUTION_H

#include "StepedRunnable.h"

#include "Poco/Thread.h"

namespace calmframe {
namespace tests {

class ScopedStepedRunnableExecution{
private:
    Poco::Thread thread;
    StepedRunnable & runnable;
    int maxJoinMillisWait;
public:
    ScopedStepedRunnableExecution(StepedRunnable & someRunnable, int maxWaitTime = 500);
    virtual ~ScopedStepedRunnableExecution();
    virtual void start();
};

}//namespace
}//namespace

#endif // SCOPEDSTEPEDRUNNABLEEXECUTION_H
