#ifndef PERMISSIONHELPER_H
#define PERMISSIONHELPER_H

#include "model/permissions/Permission.h"
#include <ostream>

namespace calmframe {

class PermissionHelper
{
public:
    static void checkEquals(const Permission & lhs, const Permission & rhs);
};

}//namespace

#endif // PERMISSIONHELPER_H
