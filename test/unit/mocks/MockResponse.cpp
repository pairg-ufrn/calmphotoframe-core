#include "MockResponse.h"
#include <fstream>

using namespace std;
using namespace calmframe::network;


const std::vector<std::string> & MockResponse::getSentFiles() const
{
    return sentFiles;
}

void MockResponse::finishHeaders()
{
    headerStream << "\r\n";
    this->sentHeaders = true;
}
void MockResponse::send(const std::string &data)
{
    responseStream << data;
}

void MockResponse::sendFile(const char *filepath)
{
    this->sendFile(string(filepath));
}

void MockResponse::sendFile(const std::string &filepath){
    sendHeaders();
    this->sentFiles.push_back(filepath);
}
void MockResponse::sendStatus(const StatusResponse &statusResponse){
    headerStream << "HTTP/1.1 "
                << statusResponse.getStatusCode() <<  " "
                << statusResponse.getMessage()    << "\r\n";
}

void MockResponse::sendHeader(const Header &header){
    headerStream << header.toString() << "\r\n";
}
