#ifndef MOCKDATABUILDER_HPP
#define MOCKDATABUILDER_HPP

#include "mocks/data/data-memory/MemoryData.h"

#include "data/DataBuilder.hpp"
#include "utils/ScopedPthreadLock.h"
#include "utils/PthreadMutex.h"

#include "Poco/SharedPtr.h"

#include <cstddef>
#include <vector>

using calmframe::data::Data;

class FakeDataCounters{
public:
    int buildDataCounter, cloneCounter;
    //CAUTION: do not use this pointers, they may be deleted already
    std::vector<void *> createdPointers;
    calmframe::PthreadMutex mutex;
public:
    FakeDataCounters(int buildDataCounter=0, int cloneCounter=0)
        : buildDataCounter(buildDataCounter)
        , cloneCounter(cloneCounter)
    {}

    void addCreatedPtr(void * ptr){
        calmframe::ScopedPthreadLock lock(mutex);

        createdPointers.push_back(ptr);
    }
    std::vector<void *> & getBuildedPointers(){
        calmframe::ScopedPthreadLock lock(mutex);
        return createdPointers;
    }
};

class FakeDataBuilder : public calmframe::data::DataBuilder{
private:
    Poco::SharedPtr<FakeDataCounters> counters;
public:
    FakeDataBuilder(int buildDataCounter=0, int cloneCounter=0)
        : counters(new FakeDataCounters(buildDataCounter, cloneCounter))
    {}

    FakeDataBuilder(const FakeDataBuilder * originClone){
        this->counters = originClone->counters;
        this->counters->cloneCounter++;
    }

    int getBuildCount(){
        return counters->buildDataCounter;
    }
    void setBuildCount(int count){
        counters->buildDataCounter = count;
    }
    int getCloneCount(){
        return counters->cloneCounter;
    }
    void setCloneCount(int count){
        counters->cloneCounter = count;
    }
    std::vector<void *> & getBuildedPointers(){
        return counters->getBuildedPointers();
    }

    void resetCounters(){
        setBuildCount(0);
        setCloneCount(0);
    }

    virtual calmframe::data::Data * buildData(){
        setBuildCount(getBuildCount() + 1);
        Data* createdData =  new calmframe::data::MemoryData();

        counters->addCreatedPtr(createdData);

        return createdData;
    }

    virtual DataBuilder * clone() const{
        return new FakeDataBuilder(this);
    }
};

#endif // MOCKDATABUILDER_HPP

