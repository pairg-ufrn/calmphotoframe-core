#ifndef MEMORYSESSIONDAO_H
#define MEMORYSESSIONDAO_H

#include <vector>
#include "model/user/Session.h"

#include "data/SessionDAO.h"

using calmframe::Session;

class MemorySessionDAO : public SessionDAO
{
private:
    std::vector<Session> sessions;
    typedef std::vector<Session> SessionCollection;
public:
    MemorySessionDAO() : sessions()
    {}
    std::vector<Session> listAll();

    virtual bool getUserSession(long userId, Session & sessionOut) const;
    bool get(long sessionId, Session & sessionOut) const;
    bool getByToken(const std::string & token, Session & sessionOut) const;
    bool exist(long sessionId) const;
    long insert(const Session & session);
    long insert(const Session & session, long sessionId);
    int update(const Session & session);
    bool remove(const Session & session);
    virtual int removeExpired();
private:
    bool remove(long sessionId);
    int findSession(long sessionId) const;
    int findSession(const Session & sessionId) const;
    SessionCollection::iterator find(long sessionId);
    const SessionCollection::const_iterator find(long sessionId) const;
    const SessionCollection::const_iterator findByUser(long userId) const;
    long generateId() const;
};

#endif // MEMORYSESSIONDAO_H
