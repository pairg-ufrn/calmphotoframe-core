/*
 * MemoryData.h
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#ifndef MEMORYDATA_H_
#define MEMORYDATA_H_

#include "data/Data.h"
#include "MemoryPhotoDAO.h"
#include "MemoryUserDAO.h"
#include "MemorySessionDAO.h"
#include "MemoryPhotoCollectionDAO.h"
#include "MemoryPhotoContextDAO.h"
#include "MemoryEventsDAO.h"

namespace calmframe {
namespace data {

class MemoryData : public Data{
private:
	MemoryPhotoDAO photoDAO;
    MemoryUserDAO userDAO;
    MemorySessionDAO sessionDAO;
    MemoryPhotoCollectionDAO photoCollectionDAO;
    MemoryPhotoContextDAO photoContextDAO;
    MemoryEventsDAO eventsDAO;
public:
    MemoryData();
    virtual ~MemoryData();

    PhotoDAO            & photos()      override;
    UserDAO             & users()       override;
    SessionDAO          & sessions()    override;
    PhotoCollectionDAO  & collections() override;
    PhotoContextDAO     & contexts()    override;
    EventsDAO           & events()      override;
    PermissionsDAO      & permissions() override;
    ConfigurationsDAO   & configs() override;

    bool allowTransactions() const override;
    bool isOnTransaction() const override;
    void beginTransaction(TransactionType type=TransactionType::Default) override {}
    void commit() override {}
    void rollback() override {}
};

} /* namespace data */
} /* namespace calmframe */

#endif /* MEMORYDATA_H_ */
