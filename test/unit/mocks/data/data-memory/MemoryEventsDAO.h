#ifndef MEMORYEVENTSDAO_H
#define MEMORYEVENTSDAO_H

#include "data/EventsDAO.h"
#include "GenericMemoryDao.hpp"

namespace calmframe {

class MemoryEventsDAO : public EventsDAO
{
public:
    virtual Event get(long id) override;
    virtual long insert(const Event & evt) override;
    virtual bool exist(long eventId) override;
    virtual EventsList listRecents(int limit=-1) override;

private:
    GenericMemoryDao<Event> dao;
};

}//namespace


#endif // MEMORYEVENTSDAO_H
