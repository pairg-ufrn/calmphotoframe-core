#include "MemoryPhotoContextDAO.h"

#include "utils/CommonExceptions.h"

namespace calmframe {

MemoryPhotoContextDAO::MemoryPhotoContextDAO()
{}

MemoryPhotoContextDAO::~MemoryPhotoContextDAO()
{}

long MemoryPhotoContextDAO::count(){
    return dao.count();
}

PhotoContext MemoryPhotoContextDAO::get(long contextId){
    return dao.get(contextId);
}

PhotoContext MemoryPhotoContextDAO::getFirst(){
    NOT_IMPLEMENTED();
}

bool MemoryPhotoContextDAO::exist(long contextId)
{
    return dao.exist(contextId);
}

data::PhotoContextDAO::ContextList MemoryPhotoContextDAO::list(Visibility minVisibility)
{
    NOT_IMPLEMENTED(MemoryPhotoContextDAO::list);
}

data::PhotoContextDAO::ContextList MemoryPhotoContextDAO::listAll()
{
    NOT_IMPLEMENTED(MemoryPhotoContextDAO::listAll);
}

data::PhotoContextDAO::ContextList MemoryPhotoContextDAO::listIn(const data::PhotoContextDAO::SetId &collIds)
{
    NOT_IMPLEMENTED(MemoryPhotoContextDAO::listIn);
}

long MemoryPhotoContextDAO::getRootCollectionId(long ctxId)
{
    return dao.get(ctxId).getRootCollection();
}

std::set<long> MemoryPhotoContextDAO::listRootCollections()
{
    NOT_IMPLEMENTED();
}

bool MemoryPhotoContextDAO::isRootCollection(long collectionId)
{
    NOT_IMPLEMENTED();
}

long MemoryPhotoContextDAO::insert(const PhotoContext &photoContext){
    return this->dao.insert(photoContext);
}

bool MemoryPhotoContextDAO::update(const PhotoContext &photoContext)
{
    NOT_IMPLEMENTED();
}

bool MemoryPhotoContextDAO::remove(long contextId)
{
    NOT_IMPLEMENTED();
}

}//namespace
