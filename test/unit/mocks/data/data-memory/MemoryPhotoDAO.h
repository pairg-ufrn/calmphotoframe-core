#ifndef MEMORYPHOTODAO_H_
#define MEMORYPHOTODAO_H_

#include "model/photo/Photo.h"
#include <vector>
#include "data/PhotoDAO.hpp"

class MemoryPhotoDAO : public PhotoDAO {
private:
	std::vector<Photo> photos;
public:
    MemoryPhotoDAO() : photos()
    {}
    PhotoList listAll(const std::string & query=std::string());
    PhotoList listIn(const std::set<long> & photoIds, const std::string & query=std::string());
    PhotoList listByContext(long contextId);
	bool getPhoto(long photoId, Photo & photoOut);
    bool exist(long photoId);
    long insert(const Photo & photo);
    long insert(const Photo & photo, long photoId);
	bool update(const Photo & photo);
	bool remove(const Photo & photo);
	//TODO: rever método updatePhotoDescription
    bool updatePhotoDescription(long photoId, const PhotoDescription & photoDescription);

protected:
    std::vector<Photo> filterPhotos(const std::vector<calmframe::Photo> & photos, const std::string& query);
private:
    int findPhoto(long photoId) const;
    int findPhoto(const Photo & photo) const;
    long generatePhotoId() const;
};

#endif /* MEMORYPHOTODAO_H_ */
