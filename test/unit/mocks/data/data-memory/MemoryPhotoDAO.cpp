#include "MemoryPhotoDAO.h"
#include "utils/IllegalArgumentException.hpp"
#include <algorithm>
#include <cassert>

#include "utils/CommonExceptions.h"

using namespace std;


static bool match(const std::string & str, const string& toMatch)
{
    //TODO: make case insensitive
    return str.find(toMatch) != string::npos;
}

bool photoMatch(const Photo & photo, const string& query)
{
    const PhotoDescription& desc = photo.getDescription();

    return match(desc.getDescription(), query)
            || match(desc.getMoment(), query)
            || match(desc.getPlace(), query)
            || match(desc.getDate(), query)
            || match(desc.getCredits(), query);
}

std::vector<calmframe::Photo> MemoryPhotoDAO::filterPhotos(const std::vector<calmframe::Photo> & photos, const std::string& query)
{
    std::vector<Photo> filteredPhotos;
    for(const Photo & photo : photos){
        if(photoMatch(photo, query)){
            filteredPhotos.push_back(photo);
        }
    }

    return filteredPhotos;
}

PhotoDAO::PhotoList MemoryPhotoDAO::listAll(const string & query) {
    if(query.empty()){
        return this->photos;
    }
    else{
        return filterPhotos(this->photos, query);
    }
}

PhotoDAO::PhotoList MemoryPhotoDAO::listIn(const std::set<long> &photoIds, const string & query)
{
    if(!query.empty()){
         NOT_IMPLEMENTED(MemoryPhotoDAO::listIn(const std::set<long> &photoIds, const string & query != string()));
    }

    vector<Photo> outPhotos;
    for(size_t i=0; i < photos.size(); ++i){
        long photoId = photos[i].getId();
        if(photoIds.find(photoId) != photoIds.end()){
            outPhotos.push_back(photos[i]);
        }
    }
    return outPhotos;
}

PhotoDAO::PhotoList MemoryPhotoDAO::listByContext(long contextId)
{
    NOT_IMPLEMENTED();
}

long MemoryPhotoDAO::insert(const Photo& photo) {
    return this->insert(photo, -1);
}

long MemoryPhotoDAO::insert(const Photo &photo, long photoId){
    if(photoId > 0 && exist(photoId)){
        throw CommonExceptions::IllegalArgumentException("Trying insert already existent id!");
    }

    long createPhotoId = photoId;
    if(photoId <= 0){
        createPhotoId = generatePhotoId();
    }

    this->photos.push_back(photo);
    int photoIndex = photos.size() -1;
    Photo & insertedPhoto = this->photos[photoIndex];
    insertedPhoto.setId(createPhotoId); //Não utilizar 0 como id válido

    assert(insertedPhoto.getId() > 0);
    return insertedPhoto.getId();
}

bool MemoryPhotoDAO::update(const Photo& photo) {
	int index = findPhoto(photo);

	if(index >= 0){
		this->photos[index] = photo;
		return true;
	}
	return false;
}

bool MemoryPhotoDAO::remove(const Photo& photo) {
	int index = findPhoto(photo);

	if(index >= 0){
		this->photos.erase(photos.begin() + index);
		return true;
	}
	return false;
}

bool MemoryPhotoDAO::getPhoto(long photoId, Photo& photoOut) {
	Photo photoTemp(photoId);
	int index = findPhoto(photoTemp);
	if(index >= 0){
		photoOut = photos[index];
		return true;
	}
    return false;
}

bool MemoryPhotoDAO::exist(long photoId){
    return findPhoto(photoId) >= 0;
}

int MemoryPhotoDAO::findPhoto(const Photo& photo) const{
    return findPhoto(photo.getId());
}

long MemoryPhotoDAO::generatePhotoId() const
{
    long maxId = -1;
    for(size_t i=0; i < this->photos.size(); ++i){
        //maxId = std::max(maxId, photos[i].getId());
        if(photos[i].getId() > maxId){
            maxId = photos[i].getId();
        }
    }
    int generatedId = maxId <=0 ? 1 : maxId + 1;

    assert(generatedId > 0);

    return generatedId;
}
int MemoryPhotoDAO::findPhoto(long photoId) const{
    for(size_t i=0; i < photos.size(); ++i){
        if(photos[i].getId() == photoId){
            return i;
        }
    }
    return -1;
}


bool MemoryPhotoDAO::updatePhotoDescription(long photoId, const PhotoDescription& photoDescription) {
	int photoIndex = findPhoto(Photo(photoId));
	if(photoIndex >= 0){
		Photo & photo = photos[photoIndex];
		photo.setDescription(photoDescription);
		return true;
	}
    return false;
}
