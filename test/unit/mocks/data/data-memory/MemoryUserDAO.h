#ifndef MEMORYUSERDAO_H
#define MEMORYUSERDAO_H

#include "data/UserDAO.h"
#include <vector>
#include "model/user/UserAccount.h"

#include "GenericMemoryDao.hpp"

namespace calmframe {

class MemoryUserDAO : public UserDAO
{
public:
    typedef std::vector<UserAccount> UserAccountCollection;
private:
    GenericMemoryDao<UserAccount> usersDAO;
public:
    MemoryUserDAO();
    ~MemoryUserDAO();

    virtual UserCollection listUsers();
    UserCollection listUsersIn(const data::SetId & userIds) override;
    virtual bool get(long userId, UserAccount & userOut);
    virtual bool getUser(long userId, User & userOut);
    virtual bool getByLogin(const std::string & userName, UserAccount & userOut);
    virtual bool exist(long userId) const;
    virtual bool exist(const std::string & userName) const;
    virtual long insert(const UserAccount & user);
    virtual long insertAt(long userId, const UserAccount & userAccount);
    virtual bool remove(long userId);
    virtual bool update(const User & user);
    virtual bool updateAccount(long id, const UserAccount & account);
//    virtual long insert(const User & user, long userId) =0;
//    virtual bool update(const User & user) =0;
    //    virtual bool remove(const User & user) =0;
protected:
    template<typename Comparator>
    bool getUser(UserAccount & userOut, const Comparator & comparator);
    const UserAccountCollection::const_iterator find(long userId) const;
    UserAccountCollection::iterator find(long userId);
};

}

#endif // MEMORYUSERDAO_H
