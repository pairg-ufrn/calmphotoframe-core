#include "MemoryEventsDAO.h"

#include "utils/CommonExceptions.h"

#include <algorithm>

namespace calmframe {

Event MemoryEventsDAO::get(long id){
    return dao.get(id);
}

long MemoryEventsDAO::insert(const Event & evt){
    return dao.insert(evt);
}

bool MemoryEventsDAO::exist(long eventId){
    return dao.exist(eventId);
}

EventsDAO::EventsList MemoryEventsDAO::listRecents(int limit){
    EventsList events = dao.listAll();

    std::sort(events.begin(), events.end());

    if(limit >= 0 && limit <= events.size()){
        events.erase(events.begin() + limit, events.end());
    }

    return events;
}

}//namespace

