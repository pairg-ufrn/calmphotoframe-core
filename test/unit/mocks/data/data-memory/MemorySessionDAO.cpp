#include "MemorySessionDAO.h"

#include "model/user/Session.h"
#include "utils/IllegalArgumentException.hpp"
#include "utils/Log.h"

#include <algorithm>
#include <string>

using namespace std;

class IdComparator{
private:
    long expectedId;
public:
    IdComparator(const long & expectedId)
        : expectedId(expectedId)
    {}

    bool operator()(const Session & session) {
        return session.getId() == expectedId;
    }
};
class UserIdComparator{
private:
    long expectedId;
public:
    UserIdComparator(const long & expectedId)
        : expectedId(expectedId)
    {}

    bool operator()(const Session & session) {
        return session.getUserId() == expectedId;
    }
};
class TokenComparator{
private:
    string expectedToken;
public:
    TokenComparator(const string & expectedToken)
        : expectedToken(expectedToken)
    {}

    bool operator()(const Session & session) {
        return session.getToken() == expectedToken;
    }
};

std::vector<Session> MemorySessionDAO::listAll() {
    return this->sessions;
}

bool MemorySessionDAO::getUserSession(long userId, calmframe::Session &sessionOut) const{
    SessionCollection::const_iterator iter = findByUser(userId);
    if(iter!= sessions.end()){
        sessionOut = *iter;
        return true;
    }
    return false;
}

long MemorySessionDAO::insert(const Session& session) {
    return this->insert(session, -1);
}

long MemorySessionDAO::insert(const Session &session, long sessionId){
    if(sessionId > 0 && exist(sessionId)){
        throw CommonExceptions::IllegalArgumentException("Trying insert already existent id!");
    }

    long createSessionId = sessionId;
    if(sessionId <= 0){
        createSessionId = generateId();
    }

    this->sessions.push_back(session);
    int sessionIndex = sessions.size() -1;
    Session & insertedSession = this->sessions[sessionIndex];
    insertedSession.setId(createSessionId); //Não utilizar 0 como id válido

    return insertedSession.getId();
}

int MemorySessionDAO::update(const Session& session) {
    int index = findSession(session);

    if(index >= 0){
        this->sessions[index] = session;
        return 1;
    }
    return 0;
}

bool MemorySessionDAO::remove(const Session& session) {
    return remove(session.getId());
}

int MemorySessionDAO::removeExpired(){
    vector<long> expiredIds;
    for(SessionCollection::iterator iter = sessions.begin();
        iter != sessions.end(); ++iter)
    {
        if(iter->expired()){
            expiredIds.push_back(iter->getId());
        }
    }
    int removedCount = 0;
    for(size_t i=0; i < expiredIds.size(); ++i){
        if(remove(expiredIds[i])){
            ++removedCount;
        }
    }
    return removedCount;
}

bool MemorySessionDAO::remove(long sessionId){
    SessionCollection::iterator iter = find(sessionId);

    if(iter != sessions.end()){
        this->sessions.erase(iter);
        return true;
    }
    return false;
}

bool MemorySessionDAO::get(long sessionId, Session& sessionOut) const {
    Session sessionTemp;
    sessionTemp.setId(sessionId);
    int index = findSession(sessionTemp);
    if(index >= 0){
        sessionOut = sessions[index];
        return true;
    }
    return false;
}

bool MemorySessionDAO::getByToken(const string & token, calmframe::Session &sessionOut) const{
    MemorySessionDAO::SessionCollection::const_iterator iter =
            std::find_if(sessions.begin(), sessions.end(), TokenComparator(token));
    if(iter != sessions.end()){
        sessionOut = *iter;
        return true;
    }
    return false;
}

bool MemorySessionDAO::exist(long sessionId) const{
    return findSession(sessionId) >= 0;
}

int MemorySessionDAO::findSession(const Session& session) const{
    return findSession(session.getId());
}

std::vector<Session>::iterator MemorySessionDAO::find(long sessionId){
    return std::find_if(sessions.begin(), sessions.end(), IdComparator(sessionId));
}

const std::vector<Session>::const_iterator MemorySessionDAO::find(long sessionId) const
{
    return std::find_if(sessions.begin(), sessions.end(), IdComparator(sessionId));
}

const std::vector<Session>::const_iterator MemorySessionDAO::findByUser(long userId) const{
    return std::find_if(sessions.begin(), sessions.end(), UserIdComparator(userId));
}

long MemorySessionDAO::generateId() const
{
    long maxId = -1;
    for(size_t i=0; i < this->sessions.size(); ++i){
        if(sessions[i].getId() > maxId){
            maxId = sessions[i].getId();
        }
    }
    int generatedId = maxId <=0 ? 1 : maxId + 1;


    return generatedId;
}
int MemorySessionDAO::findSession(long sessionId) const{
    for(size_t i=0; i < sessions.size(); ++i){
        if(sessions[i].getId() == sessionId){
            return i;
        }
    }
    return -1;
}
