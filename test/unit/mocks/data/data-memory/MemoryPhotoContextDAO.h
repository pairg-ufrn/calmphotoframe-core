#ifndef MEMORYPHOTOCONTEXTDAO_H
#define MEMORYPHOTOCONTEXTDAO_H

#include "data/PhotoContextDAO.h"

#include "GenericMemoryDao.hpp"

namespace calmframe {

class MemoryPhotoContextDAO : public data::PhotoContextDAO
{
public:
    MemoryPhotoContextDAO();
    ~MemoryPhotoContextDAO();

    virtual long count();
    PhotoContext get(long contextId);
    PhotoContext getFirst();
    bool exist(long contextId);

    ContextList list(Visibility minVisibility = Visibility::Unknown) override;
    ContextList listAll() override;
    ContextList listIn(const SetId & collIds) override;

    long getRootCollectionId(long ctxId);
    std::set<long> listRootCollections();
    bool isRootCollection(long collectionId);
    long insert(const PhotoContext & photoContext);
    bool update(const PhotoContext & photoContext);
    bool remove(long contextId);

private:
    GenericMemoryDao<PhotoContext> dao;
};

}//namespace

#endif // MEMORYPHOTOCONTEXTDAO_H
