/*
 * MemoryData.cpp
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#include "MemoryData.h"

namespace calmframe {
namespace data {

MemoryData::MemoryData()
    : photoDAO()
    , photoCollectionDAO()
{
}

MemoryData::~MemoryData(){
}

PhotoDAO& MemoryData::photos() {
    return photoDAO;
}

UserDAO &MemoryData::users(){
    return userDAO;
}

SessionDAO &MemoryData::sessions(){
    return sessionDAO;
}

PhotoCollectionDAO &calmframe::data::MemoryData::collections(){
    return this->photoCollectionDAO;
}

PhotoContextDAO &MemoryData::contexts()
{
    return this->photoContextDAO;
}

EventsDAO &MemoryData::events(){
    return eventsDAO;
}

PermissionsDAO &MemoryData::permissions(){
    NOT_IMPLEMENTED(MemoryData::permissions);
}

bool MemoryData::allowTransactions() const
{
    return false;
}

bool MemoryData::isOnTransaction() const
{
    return false;
}

ConfigurationsDAO & MemoryData::configs(){
    NOT_IMPLEMENTED(MemoryData::configs);
}

} /* namespace data */
} /* namespace calmframe */
