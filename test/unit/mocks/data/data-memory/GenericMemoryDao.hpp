#ifndef GENERICMEMORYDAO_HPP
#define GENERICMEMORYDAO_HPP

#include "utils/CommonExceptions.h"
#include <vector>
#include <algorithm>

template<typename T>
class GenericMemoryDao{
protected:
    std::vector<T> collection;
public:
    int count() const;

    const std::vector<T> & listAll() const;
    std::vector<T> & listAll();
    bool exist(long id) const;
    bool get(long id, T & out) const;
    T get(long id) const;
    template<typename WherePredicate, typename CompareItem>
    std::vector<T> getWhere(const WherePredicate & pred, const CompareItem & item) const;

    long insert(const T & item);
    long insertAt(long id, const T &item);
    bool update(const T & item);
    bool remove(long id);
protected:
    int findItem(const T & item) const;
    int findItem(long id) const;
    long generateId() const;
};


template<typename T>
int GenericMemoryDao<T>::count() const{
    return collection.size();
}

template<typename T>
const std::vector<T> &GenericMemoryDao<T>::listAll() const{
    return this->collection;
}

template<typename T>
std::vector<T> &GenericMemoryDao<T>::listAll()
{
    return collection;
}

template<typename T>
bool GenericMemoryDao<T>::exist(long id) const{
    return findItem(id) >= 0;
}

template<typename T>
bool GenericMemoryDao<T>::get(long id, T & out) const{
    int index = findItem(id);
    if(index >=0){
        out = this->collection[index];
        return true;
    }
    return false;
}

template<typename T>
T GenericMemoryDao<T>::get(long id) const{
    T item;
    get(id, item);
    return item;
}

template<typename T>
template<typename WherePredicate, typename CompareItem>
std::vector<T> GenericMemoryDao<T>::getWhere(const WherePredicate &pred, const CompareItem &item) const
{
    std::vector<T> items;
    for(size_t i=0; i < this->collection.size(); ++i){
        if(pred(collection[i], item)){
            items.push_back(collection[i]);
        }
    }

    return items;
}

template<typename T>
long GenericMemoryDao<T>::insert(const T &item){
    size_t size = collection.size();
    long id = size + 1;

    return insertAt(id, item);
}

template<typename T>
long GenericMemoryDao<T>::insertAt(long id, const T &item){
    this->collection.push_back(item);
    collection[collection.size() - 1].setId(id);

    return id;
}

template<typename T>
bool GenericMemoryDao<T>::update(const T &item){
    int index = findItem(item);
    if(index >= 0 && index < this->collection.size()){
        this->collection[index] = item;
        return true;
    }
    return false;
}

template<typename T>
bool GenericMemoryDao<T>::remove(long id){
    int index = findItem(id);
    if(index >=0){
        collection.erase(collection.begin() + index);
        return true;
    }
    return true;
}

template<typename T>
int GenericMemoryDao<T>::findItem(const T & item) const{
    return this->findItem(item.getId());
}
template<typename T>
int GenericMemoryDao<T>::findItem(long id) const{
    for(size_t i=0; i < this->collection.size(); ++i){
        if(collection[i].getId() == id){
            return i;
        }
    }
    return -1;
}

template<typename T>
long GenericMemoryDao<T>::generateId() const{
    long maxId = -1;
    for(size_t i=0; i < this->collection.size(); ++i){
        long currentId = collection[i].getId();
        if(currentId > maxId){
            maxId = currentId;
        }
    }
    int generatedId = maxId <=0 ? 1 : maxId + 1;

do{
    if(!(generatedId > 0)){
        std::string conditionStr = "generatedId > 0";
        std::string msgError = std::string("'").append(conditionStr).append("' is false.");
        throw CommonExceptions::AssertionError(msgError);
    }
}while(false);
    return generatedId;
}

#endif // GENERICMEMORYDAO_HPP
