#include "MemoryUserDAO.h"

#include <algorithm>
#include "utils/CommonExceptions.h"

namespace calmframe{

using namespace std;

class IdComparator{
private:
    long expectedId;
public:
    IdComparator(const long & expectedId)
        : expectedId(expectedId)
    {}

    bool operator()(const UserAccount & user) {
        return user.getId() == expectedId;
    }
};
class NameComparator{
private:
    string expectedName;
public:
    NameComparator(const string & expectedName)
        : expectedName(expectedName)
    {}

    bool operator()(const UserAccount & user) {
        return user.getLogin() == expectedName;
    }
};

MemoryUserDAO::MemoryUserDAO()
{

}

MemoryUserDAO::~MemoryUserDAO()
{

}

UserCollection MemoryUserDAO::listUsers()
{
    const UserAccountCollection & accounts = usersDAO.listAll();
    UserCollection usersCollections;
    for(size_t i=0; i < accounts.size(); ++i){
        usersCollections.push_back(accounts[i].getUser());
    }
    return usersCollections;
}

UserCollection MemoryUserDAO::listUsersIn(const data::SetId & userIds)
{
    NOT_IMPLEMENTED(MemoryUserDAO::listUsersIn);
}

bool MemoryUserDAO::get(long userId, UserAccount &userOut){
    return usersDAO.get(userId, userOut);
}

bool MemoryUserDAO::getUser(long userId, User & userOut)
{
    UserAccount account;
    if(get(userId, account)){
        userOut = account.getUser();
        return true;
    }
    return false;
}

bool calmframe::MemoryUserDAO::getByLogin(const std::string &userName, calmframe::UserAccount &userOut){
    return getUser(userOut, NameComparator(userName));
}
template<typename Comparator>
bool MemoryUserDAO::getUser(UserAccount &userOut, const Comparator &comparator){
    const std::vector<UserAccount> & users = this->usersDAO.listAll();
    UserAccountCollection::const_iterator iter= std::find_if(users.begin(), users.end(), comparator);
    if(iter != users.end()){
        userOut = *iter;
        return true;
    }
    return false;
}


long MemoryUserDAO::insert(const UserAccount &user){
    return usersDAO.insert(user);
}

long MemoryUserDAO::insertAt(long userId, const UserAccount & userAccount){
    return usersDAO.insertAt(userId, userAccount);
}

bool MemoryUserDAO::remove(long userId)
{
    return usersDAO.remove(userId);
}

bool MemoryUserDAO::update(const User & user)
{
    UserAccount account;
    if(get(user.getId(), account)){
        account.getUser() = user;
        usersDAO.update(account);
     }
     return false;
}

bool MemoryUserDAO::exist(long userId) const {
    bool exist = this->usersDAO.exist(userId);

    return exist;
}
bool MemoryUserDAO::exist(const std::string &userName) const{
    const std::vector<UserAccount> & users = this->usersDAO.listAll();
    return std::find_if(users.begin(), users.end(), NameComparator(userName)) != users.end();
}

const std::vector<UserAccount>::const_iterator MemoryUserDAO::find(long userId) const{
    const std::vector<UserAccount> & users = this->usersDAO.listAll();
    return std::find_if(users.begin(), users.end(), IdComparator(userId));
}

std::vector<UserAccount>::iterator MemoryUserDAO::find(long userId){
    std::vector<UserAccount> & users = this->usersDAO.listAll();
    return std::find_if(users.begin(), users.end(), IdComparator(userId));
}

bool calmframe::MemoryUserDAO::updateAccount(long id, const calmframe::UserAccount & account)
{
    NOT_IMPLEMENTED(MemoryUserDAO::updateAccount);
}


} //namespace

