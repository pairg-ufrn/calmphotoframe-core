/*
 * MemoryDataBuilder.hpp
 *
 *  Created on: 05/10/2014
 *      Author: noface
 */

#ifndef MEMORYDATABUILDER_HPP_
#define MEMORYDATABUILDER_HPP_

#include "data/DataBuilder.hpp"
#include "MemoryData.h"
#include "utils/Log.h"
#include "cassert"

namespace calmframe {
namespace data {

class MemoryDataBuilder : public DataBuilder{
public:
	virtual Data * buildData(){
		return new MemoryData();
	}
    virtual DataBuilder * clone() const{
        DataBuilder * clonedBuilder = new MemoryDataBuilder();
        return clonedBuilder;
    }
};

} /* namespace data */
} /* namespace calmframe */

#endif /* MEMORYDATABUILDER_HPP_ */
