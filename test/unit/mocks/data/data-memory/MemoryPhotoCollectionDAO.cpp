#include "MemoryPhotoCollectionDAO.h"
#include "data/PhotoDAO.hpp"
#include "utils/NullPointerException.hpp"
#include "utils/CommonExceptions.h"

#include <set>

using namespace std;
using namespace  calmframe;
using namespace  calmframe::data;

MemoryPhotoCollectionDAO::MemoryPhotoCollectionDAO()
{}

calmframe::PhotoCollection MemoryPhotoCollectionDAO::get(long collectionId){
    return this->genericMemoryDao.get(collectionId);
}

bool MemoryPhotoCollectionDAO::get(long collectionId, PhotoCollection & outCollection){
    return this->genericMemoryDao.get(collectionId, outCollection);
}

bool MemoryPhotoCollectionDAO::exist(long collectionId){
    return this->genericMemoryDao.exist(collectionId);
}

std::vector<calmframe::PhotoCollection> MemoryPhotoCollectionDAO::listAll(){
    return this->genericMemoryDao.listAll();
}

std::vector<calmframe::PhotoCollection> MemoryPhotoCollectionDAO::listIn(const std::set<long> &collIds)
{
    std::vector<PhotoCollection> allCollections = listAll();
    std::vector<PhotoCollection> filtered;
    for(size_t i=0; i < allCollections.size(); ++i){
        if(collIds.find(allCollections[i].getId()) != collIds.end()){
            filtered.push_back(allCollections[i]);
        }
    }
    return filtered;
}

static bool hasName(const PhotoCollection & collection, const std::string & name){
    return collection.getName() == name;
}

std::vector<calmframe::PhotoCollection> MemoryPhotoCollectionDAO::getByName(const std::string &name){
    return this->genericMemoryDao.getWhere(hasName, name);
}

PhotoCollectionDAO::CollectionList &MemoryPhotoCollectionDAO::getByContext(long ctxId, CollectionList &list)
{
    NOT_IMPLEMENTED(MemoryPhotoCollectionDAO::getByContext);
}

SetId MemoryPhotoCollectionDAO::listIdsByContext(long ctxId)
{
    NOT_IMPLEMENTED(MemoryPhotoCollectionDAO::listIdsByContext);
}

SetId MemoryPhotoCollectionDAO::getCollectionsWithoutParent() const
{
    NOT_IMPLEMENTED(MemoryPhotoCollectionDAO::getCollectionsWithoutParent);
}

std::map<long, calmframe::PhotoCollection> &MemoryPhotoCollectionDAO::getCollectionTree(long collId, std::map<long, calmframe::PhotoCollection> & collTree)
{
    PhotoCollection coll = get(collId);
    addToTree(coll, collTree);

    return collTree;
}

void MemoryPhotoCollectionDAO::addPhotos(const std::set<long> &collectionIds, const std::set<long> &photoIds)
{
    for(set<long>::iterator collIter = collectionIds.begin();
        collIter != collectionIds.end(); ++collIter)
    {
        PhotoCollection coll = get(*collIter);
        for(set<long>::iterator photoIter = photoIds.begin();
                photoIter != photoIds.end(); ++photoIter)
        {
            coll.putPhoto(*photoIter);
        }
        update(coll);
    }
}

void MemoryPhotoCollectionDAO::removePhotosFromCollections(const std::set<long> &collectionIds, const std::set<long> &photoIds)
{
    for(set<long>::iterator collIter = collectionIds.begin();
        collIter != collectionIds.end(); ++collIter)
    {
        PhotoCollection coll = get(*collIter);
        for(set<long>::iterator photoIter = photoIds.begin();
                photoIter != photoIds.end(); ++photoIter)
        {
            coll.removePhoto(*photoIter);
        }
        update(coll);
    }
}

void MemoryPhotoCollectionDAO::addItemsToCollections(const std::set<long> &targetCollections, const std::set<long> &photoIds, const std::set<long> &subCollections)
{
    changeItemsInCollectionImpl(photoIds, subCollections, targetCollections, true);
}

void MemoryPhotoCollectionDAO::removeItemsFromCollections(const std::set<long> &targetCollections, const std::set<long> &photoIds, const std::set<long> &subCollections)
{
    changeItemsInCollectionImpl(photoIds, subCollections, targetCollections, false);
}


void MemoryPhotoCollectionDAO::changeItemsInCollectionImpl(
                    const std::set<long> &photoIds
                    , const std::set<long> &subCollections
                    , const std::set<long> &targetCollections
                    , bool shouldAdd)
{
    std::set<long>::const_iterator iter = targetCollections.begin();
    while(iter != targetCollections.end()){
        PhotoCollection coll = get(*iter);
        if(coll.getId() == PhotoCollection::INVALID_ID){
            ++iter;
            continue;
        }

        for(std::set<long>::const_iterator photoIter = photoIds.begin();
                photoIter != photoIds.end(); ++photoIter)
        {
            if(shouldAdd){
                coll.putPhoto(*photoIter);
            }
            else{
                coll.removePhoto(*photoIter);
            }
        }

        for(std::set<long>::const_iterator collIter = subCollections.begin();
                collIter != subCollections.end(); ++collIter)
        {
            if (shouldAdd) {
                coll.getSubCollections().put(*collIter);
            } else {
                coll.getSubCollections().remove(*collIter);
            }
        }
        update(coll);
        ++iter;
    }
}

void MemoryPhotoCollectionDAO::addToTree(const calmframe::PhotoCollection & coll, PhotoCollectionDAO::CollectionTree & collTree){
    collTree[coll.getId()] = coll;

    const IdCollection::Collection & subcolls = coll.getSubCollections().getIds();
    IdCollection::Collection::const_iterator iter = subcolls.begin();
    while(iter != subcolls.end()){
        if(collTree.find(*iter) == collTree.end()){
            addToTree(get(*iter), collTree);
        }
        ++iter;
    }
}

void MemoryPhotoCollectionDAO::addPhoto(long photoId, long collectionId)
{
    PhotoCollection coll = get(collectionId);
    coll.putPhoto(photoId);
    update(coll);
}

void MemoryPhotoCollectionDAO::moveCollectionItems(long collectionId, const SetId & targetCollections
                                             , const SetId & photosToMove
                                             , const SetId & subCollectionsToMove)
{
    SetId collectionSet;
    collectionSet.insert(collectionId);

    this->removeItemsFromCollections(collectionSet, photosToMove, subCollectionsToMove);
    this->addItemsToCollections(targetCollections, photosToMove, subCollectionsToMove);
}

std::vector<calmframe::PhotoCollection> MemoryPhotoCollectionDAO::getSubCollections(long parentCollectionId)
{
    PhotoCollection coll = get(parentCollectionId);
    return listIn(coll.getSubCollections().getIds());
}

long MemoryPhotoCollectionDAO::insert(const calmframe::PhotoCollection &photoCollection){
    return this->genericMemoryDao.insert(photoCollection);
}

long MemoryPhotoCollectionDAO::insert(const calmframe::PhotoCollection &photoCollection, long parentCollId)
{
    long createdCollection = insert(photoCollection);
    PhotoCollection parentColl = get(parentCollId);
    parentColl.getSubCollections().put(createdCollection);
    update(parentColl);

    return createdCollection;
}

long MemoryPhotoCollectionDAO::insertAt(long collectionId, const calmframe::PhotoCollection &photoCollection)
{
    return genericMemoryDao.insertAt(collectionId, photoCollection);
}

bool MemoryPhotoCollectionDAO::update(const calmframe::PhotoCollection &photoCollection)
{
    return this->genericMemoryDao.update(photoCollection);
}

bool MemoryPhotoCollectionDAO::remove(long collectionId)
{
    return this->genericMemoryDao.remove(collectionId);
}

int MemoryPhotoCollectionDAO::removeCollectionsWithoutParent()
{
    throw CommonExceptions::NotImplementedException();
}

std::set<long> MemoryPhotoCollectionDAO::getPhotosWithoutParent(const std::set<long> &photoIds)
{
    throw CommonExceptions::NotImplementedException();
}

int MemoryPhotoCollectionDAO::remove(const std::set<long> &collectionIds)
{
    int removeCount = 0;
    std::set<long>::const_iterator iter = collectionIds.begin();
    while(iter != collectionIds.end()){
        if(this->remove(*iter)){
            ++removeCount;
        }
        ++iter;
    }
    return removeCount;
}

calmframe::data::SetId MemoryPhotoCollectionDAO::getCollectionPhotos(long collectionId){
    return get(collectionId).getPhotos();
}

void MemoryPhotoCollectionDAO::removePhotoFromCollections(long photoId)
{
    std::vector<PhotoCollection> & collections = genericMemoryDao.listAll();
    for(size_t i=0; i < collections.size(); ++i){
        PhotoCollection & coll = collections[i];
        coll.removePhoto(photoId);

    }
}
