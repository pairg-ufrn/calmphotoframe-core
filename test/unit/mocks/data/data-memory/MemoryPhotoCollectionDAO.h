#ifndef MEMORYPHOTOCOLLECTIONDAO_H
#define MEMORYPHOTOCOLLECTIONDAO_H

#include "data/PhotoCollectionDAO.h"
#include "GenericMemoryDao.hpp"

class PhotoDAO;

using calmframe::PhotoCollection;
using calmframe::data::SetId;

class MemoryPhotoCollectionDAO : public calmframe::data::PhotoCollectionDAO
{
private:
    GenericMemoryDao<PhotoCollection> genericMemoryDao;
public:
    MemoryPhotoCollectionDAO();

    virtual PhotoCollection get(long collectionId);
    virtual bool get(long collectionId, PhotoCollection & outCollection);
    virtual bool exist(long collectionId);
    virtual std::vector<PhotoCollection> listAll();
    virtual std::vector<PhotoCollection> listIn(const SetId & collIds);
    virtual std::vector<PhotoCollection> getByName(const std::string & name);
    virtual CollectionList & getByContext(long ctxId, CollectionList & list);
    virtual SetId listIdsByContext(long ctxId);
    virtual SetId getCollectionsWithoutParent() const;
    virtual std::map<long, PhotoCollection> & getCollectionTree(
                                                long collId, std::map<long, PhotoCollection> & collTree);

    virtual long insert(const PhotoCollection & photoCollection);
    virtual long insert(const PhotoCollection &photoCollection, long parentCollId);
    virtual long insertAt(long collectionId, const PhotoCollection & photoCollection);
    virtual bool update(const PhotoCollection & photoCollection);
    virtual bool remove(long collectionId);
    virtual int remove(const SetId & collectionIds);
    virtual int removeCollectionsWithoutParent();

    virtual std::set<long> getPhotosWithoutParent(const std::set<long> & photoIds);
    virtual SetId getCollectionPhotos(long collectionId);
    virtual std::vector<PhotoCollection> getSubCollections(long parentCollectionId);

    virtual void addPhoto(long photoId, long collectionId);
    virtual void addPhotos(const SetId & collectionIds, const SetId & photoIds);
    virtual void removePhotoFromCollections(long photoId);
    virtual void removePhotosFromCollections(const SetId & collectionIds, const SetId & photoIds);

    virtual void addItemsToCollections(const SetId & targetCollections
                                  , const SetId & photoIds, const SetId & subCollections);
    virtual void removeItemsFromCollections(const SetId & targetCollections
                                  , const SetId & photoIds, const SetId & subCollections);

    virtual void moveCollectionItems(long collectionId, const SetId & targetCollections
                                             , const SetId & photosToMove
                                             , const SetId & subCollectionsToMove);
protected:
    void changeItemsInCollectionImpl(const std::set<long> &photoIds, const std::set<long> &subCollections, const std::set<long> &targetCollections, bool shouldAdd);
    void addToTree(const PhotoCollection & coll, CollectionTree & collTree);
};

#endif // MEMORYPHOTOCOLLECTIONDAO_H
