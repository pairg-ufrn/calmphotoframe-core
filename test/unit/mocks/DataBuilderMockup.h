#ifndef DATABUILDERMOCKUP_H
#define DATABUILDERMOCKUP_H

#include <catch.hpp>
#include <fakeit.hpp>

#include "data/DataBuilder.hpp"
#include "data/Data.h"

using namespace calmframe;
using namespace calmframe::data;
using fakeit::Mock;

class DataBuilderMockup : public DataBuilder{
public:

    template<typename T>
    using Ptr = std::shared_ptr<T>;
    
    template<typename T>
    using MockPtr = Ptr<Mock<T>>;
    
    Mock<Data> dataMock;
    Mock<UserDAO> userDaoMock;
    Mock<SessionDAO> sessionDaoMock;
    Mock<ConfigurationsDAO> configsDaoMock;
    Mock<PhotoContextDAO> contextsDaoMock;

	virtual Data * buildData(){
        return &dataMock.get();
	}
    virtual DataBuilder * clone() const{ 
        return new DataBuilderMockup(*this);
    }

    Mock<Data> & data(){
        return dataMock;
    }
    Mock<UserDAO> & users(){
        return userDaoMock;
    }
    Mock<SessionDAO> & sessions(){
        return sessionDaoMock;
    }
    Mock<ConfigurationsDAO> & configs(){
        return configsDaoMock;
    }
    Mock<PhotoContextDAO> & contexts(){
        return contextsDaoMock;
    }

    DataBuilderMockup & setup(){
        fakeit::When(Method(data(), users)).AlwaysReturn(users().get());
        fakeit::When(Method(data(), sessions)).AlwaysReturn(sessions().get());
        fakeit::When(Method(data(), configs)).AlwaysReturn(configs().get());
        fakeit::When(Method(data(), contexts)).AlwaysReturn(contexts().get());
        
        fakeit::Fake(Dtor(dataMock));

        return *this;
    }
    
    void apply(){
        Data::setup(std::shared_ptr<DataBuilderMockup>(this, [](DataBuilderMockup *){}));
    }
};

#endif // DATABUILDERMOCKUP_H
