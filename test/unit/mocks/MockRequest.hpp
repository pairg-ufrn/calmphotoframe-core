#ifndef MOCKREQUEST_HPP
#define MOCKREQUEST_HPP

#include "network/Request.h"
#include "utils/NullPointerException.hpp"
#include "network/ContentTypes.hpp"
#include "network/ContentType.h"
#include "network/Header.h"
#include "network/Url.h"

#include <string>
#include <istream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <map>

using calmframe::network::ContentType;
using calmframe::network::Header;

class MockRequest : public calmframe::network::Request{
public:
    typedef Request super;
    typedef std::map<std::string, Header> HeadersMap;

private:
    std::stringstream content;
    int msgIndex;
    int partCount;
    std::vector<Request *> parts;

    HeadersMap headers;
public:
    MockRequest(const std::string & url="");

    const HeadersMap & getHeaders() const;
    void putHeader(const Header & header);

    virtual bool containsHeader(const std::string & headerName) const;
    virtual std::string getHeaderValue(const std::string & headerName, const std::string & defaultValue="") const;

    virtual std::string getQueryParameter(const std::string &paramName, const std::string &defaultValue="") const;
    void putQueryParameter(const std::string & paramName, const std::string & paramValue);

    void setMethod(const std::string & method);
    void setContentType(const ContentType & contentType);
    void setContentStr(const std::string & messageContent);
    void setContent(const std::istream & contentStream);

    const std::string getContentStr() const;
    
    bool hasContent() override;
    bool hasData() override;
    
    std::istream & getContent() override;

    int ignoreData(int maxBytes=-1) override;
    int readData(std::ostream * output, int maxBytes = -1) override;
    int readData(std::string * output, int maxBytes = -1) override;

    bool readUntilFind(const std::string & str, std::string * output=NULL, int maxBytes=-1) override;

    void addPart(Request * part);

    virtual Request * receivePart();

    void finishPart(Request* & part);

    virtual bool readUntilFindImplementation(const std::string * str = NULL, std::string * output=NULL, int maxBytes=-1);
    void clearContent();
};

#endif // MOCKREQUEST_HPP
