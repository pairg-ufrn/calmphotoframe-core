#include "MockController.h"

#include "network/Request.h"
#include "network/Response.h"

MockController::MockController(std::string baseUrl)
    : superclass(baseUrl)
    , respondEnabled(false)
{
    const char * methods[] = {"GET", "POST", "PUT", "DELETE"};
    for(int i=0; i < 4; ++i){
        methodsToHandle.insert(methods[i]);
    }
}

MockController::~MockController(){
    //Nothing to do
}

bool MockController::isHandlingMethod(const std::string & method)
{
    return methodsToHandle.find(method) != methodsToHandle.end();
}

void MockController::setHandleMethod(const std::string & method, bool handle)
{
    if(handle){
        this->methodsToHandle.insert(method);
    }
    else{
        this->methodsToHandle.erase(method);
    }
}

void MockController::putHeader(const Header &header)
{
    this->headers[header.getName()] = header;
}

bool MockController::onRequest(Request & request, Response & response)
{
    std::string method = request.getMethod();
    ++this->callsCount[method];
    if(!isHandlingMethod(method)){
        return false;
    }
    respond(response, method);
    return true;
}

void MockController::respond(Response &response, const std::string & method){

    const StatusResponse &statusResponse = getStatusResponse(method);
    const std::string &responseBody = getResponse(method);

    if(isResponseEnabled()){
        response.setStatusLine(statusResponse);
        if(!this->headers.empty()){
            HeadersMap::const_iterator iter = headers.begin();
           while(iter != headers.end())
           {
               response.putHeader(iter->second);

               ++iter;
           }
        }

        response.sendHeaders();
        if(!responseBody.empty()){
            response.send(responseBody);
        }
    }
}
