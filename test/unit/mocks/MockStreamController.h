#ifndef MOCKSTREAMCONTROLLER_H
#define MOCKSTREAMCONTROLLER_H

#include <istream>
#include <string>
#include "MockController.h"
#include "network/StatusResponse.h"

class MockStreamController : public WebController
{
    typedef WebController super;
    typedef std::map<std::string, Header> HeadersMap;
private:
    std::istream * responseStream;
    StatusResponse statusResponse;
    HeadersMap headers;
public:
    MockStreamController(std::string baseUrl = "")
        : super(baseUrl)
        , responseStream(NULL)
    {
    }
    virtual ~MockStreamController(){}

    Header & getHeader(const std::string & headerName){
        return headers[headerName];
    }
    void putHeader(const Header & header){
        headers[header.getName()] = header;
    }
    void removeHeader(const std::string & headerName){
        headers.erase(headerName);
    }

    virtual bool onGet   (Request & request, Response & response)
    { return respond(response);}
    virtual bool onPost  (Request & request, Response & response)
    { return respond(response);}
    virtual bool onPut   (Request & request, Response & response)
    { return respond(response);}
    virtual bool onDelete(Request & request, Response & response)
    { return respond(response);}

    StatusResponse getStatusResponse() const;
    void setStatusResponse(const StatusResponse &value);
    std::istream *getResponseStream() const;
    void setResponseStream(std::istream *value);
protected:
    bool respond(Response & response);
};

#endif // MOCKSTREAMCONTROLLER_H
