#include "MockRequest.hpp"

#include "network/Url.h"
#include "utils/StringUtils.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;
using namespace calmframe::network;

MockRequest::MockRequest(const std::string &url)
:super()
, msgIndex(0)
, partCount(0)
{
    this->method = "GET";
    this->contentType = calmframe::network::ContentTypes::TEXT_PLAIN;
    
    this->url(url);
}

void MockRequest::putHeader(const calmframe::network::Header &header){
    headers[header.getName()]= header;
}

bool MockRequest::containsHeader(const std::string &headerName) const{
    HeadersMap::const_iterator iter = headers.find(headerName);
    return iter != headers.end();
}

std::string MockRequest::getHeaderValue(const std::string &headerName, const std::string &defaultValue) const{
    HeadersMap::const_iterator iter = headers.find(headerName);
    if(iter != headers.end()){
        return iter->second.getValue();
    }
    return std::string();
}

std::string MockRequest::getQueryParameter(const std::string &paramName, const std::string &defaultValue) const{
    return url().query().get(paramName, defaultValue);
}

void MockRequest::setMethod(const std::string &method){
    this->method = method;
}

void MockRequest::setContentType(const calmframe::network::ContentType &contentType){
    this->contentType = contentType;
}

void MockRequest::setContentStr(const std::string &messageContent){
    this->content.str(messageContent);
}

void MockRequest::setContent(const std::istream &contentStream){
    if(contentStream.good()){
        clearContent();
        this->content << contentStream.rdbuf();
    }
}

void MockRequest::clearContent()
{
    content.clear();
    content.str(std::string());
}

const std::string MockRequest::getContentStr() const{
    return this->content.str();
}

bool MockRequest::hasContent(){
    return content.peek() != EOF;
}

//NOTE: this method may be redundant. See CivetRequest implementation. 
bool MockRequest::hasData(){
    return hasContent();
}

std::istream &MockRequest::getContent(){
    return this->content;
}

void MockRequest::putQueryParameter(const std::string &paramName, const std::string &paramValue){
    this->_url.query().put(paramName, paramValue);
    this->urlCache.clear();
}

int MockRequest::ignoreData(int maxBytes) {
    return readData((std::string *)NULL, maxBytes);
}

int MockRequest::readData(std::ostream *output, int maxBytes){
    std::string str;
    int readedData = this->readData(&str, maxBytes);
    if(output != NULL){
        *output << str;
    }
    return readedData;
}

int MockRequest::readData(std::string *output, int maxBytes){
    int initialIndex = this->msgIndex;
    //int initialIndex = this->content.tellg();

    readUntilFindImplementation(NULL, output, maxBytes);
    int finalIndex = msgIndex;
    return finalIndex - initialIndex;
}

bool MockRequest::readUntilFind(const std::string &str, std::string *output, int maxBytes)
{
    return readUntilFindImplementation(&str, output, maxBytes);
}

void MockRequest::addPart(calmframe::network::Request *part){
    CommonExceptions::assertNotNull(part, "Part should not be NULL");
    this->parts.push_back(part);
}

calmframe::network::Request *MockRequest::receivePart(){
    if(this->partCount >= this->parts.size()){
        return NULL;
    }

    return this->parts[partCount];
}

void MockRequest::finishPart(calmframe::network::Request *&part) {
    ++partCount;
}

bool MockRequest::readUntilFindImplementation(const std::string *str, std::string *output, int maxBytes)
{
    int contentSize = this->content.str().size();
    int maxFinalIndex = contentSize;
    bool unlimited = (maxBytes < 0);
    if(!unlimited){
        maxFinalIndex = std::min(maxBytes + this->msgIndex, contentSize);
    }

    bool foundStr = false;
    int posToRead = maxFinalIndex;
    if(str != NULL){
        int findPos = this->getContentStr().find(str->c_str());
        if(findPos != std::string::npos){
            foundStr = true;
            if(!unlimited){
                int endFindStr = findPos + str->size();
                posToRead = endFindStr > maxFinalIndex
                ? findPos - 1
                : endFindStr;
                foundStr = endFindStr <= maxFinalIndex;
            }
        }
    }

    int dataToRead = posToRead - this->msgIndex;
    if(dataToRead > 0){
        if(output != NULL){
            *output = this->content.str().substr(msgIndex, dataToRead);
        }
        this->msgIndex = posToRead;
    }

    return foundStr;
}

const MockRequest::HeadersMap &MockRequest::getHeaders() const{
    return headers;
}
