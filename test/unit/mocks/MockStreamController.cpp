#include "MockStreamController.h"

#include "network/Request.h"
#include "network/Response.h"

bool MockStreamController::respond(Response &response){
    response.setStatusLine(this->getStatusResponse());
    for(HeadersMap::const_iterator it = this->headers.begin();
            it != headers.end(); ++it)
    {
        response.putHeader(it->second);
    }
    response.sendHeaders();
    if(this->responseStream != NULL){
        int sizeChar = 256;
        char buffer[sizeChar];
        int readed = 0;
        do{
            responseStream->read(buffer, sizeChar);
            readed = responseStream->gcount();
            response.sendBinary((void *)buffer, readed);
        }while(responseStream->good() && readed > 0);
    }
    return true;
}


std::istream *MockStreamController::getResponseStream() const
{
    return responseStream;
}

void MockStreamController::setResponseStream(std::istream *value)
{
    responseStream = value;
}
StatusResponse MockStreamController::getStatusResponse() const
{
    return statusResponse;
}

void MockStreamController::setStatusResponse(const StatusResponse &value)
{
    statusResponse = value;
}
