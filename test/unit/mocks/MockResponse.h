#ifndef MOCKRESPONSE_H
#define MOCKRESPONSE_H

#include "network/Response.h"
#include "network/StatusResponse.h"
#include <string>
#include <sstream>
#include <vector>

class MockResponse : public calmframe::network::Response
{
private:
    std::stringstream headerStream;
    std::stringstream responseStream;
    std::vector<std::string> sentFiles;
public:
    MockResponse()
        : Response(NULL, NULL)
    {}

    virtual void send(const std::string &data) override;
    virtual void sendFile(const std::string & filepath) override;
    virtual void sendFile(const char * filepath) override;

    std::string getContentStr() const { return responseStream.str();}
    std::string getHeadersStr() const { return headerStream.str();}
    const std::vector<std::string> &getSentFiles() const;

    void finishHeaders();
protected:
    void sendStatus(const calmframe::network::StatusResponse & statusResponse) override;
    void sendHeader(const calmframe::network::Header & header) override;
};

#endif // MOCKRESPONSE_H
