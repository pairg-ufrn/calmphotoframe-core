#ifndef MOCKCONTROLLER_H
#define MOCKCONTROLLER_H

#include <istream>
#include <map>
#include <string>
#include <set>

#include "network/controller/WebController.h"
#include "network/Header.h"
#include "network/HttpMethods.h"
#include "network/StatusResponse.h"

using namespace calmframe::network;

class MockController : public WebController{
    typedef WebController superclass;
    typedef std::map<std::string, Header> HeadersMap;
private:
    std::map<std::string, std::string> responses;
    std::map<std::string, StatusResponse> statusResponses;

    bool respondEnabled;
    HeadersMap headers;

    std::map<std::string, int> callsCount;
    std::set<std::string> methodsToHandle;
public:
    MockController(std::string baseUrl = "");
    virtual ~MockController();

    bool isHandlingMethod(const std::string & method);
    void setHandleMethod(const std::string & method, bool handle=true);

    void putHeader(const Header & header);

    int getMethodCallsCount(const std::string & method) const {
        return callsCount.find(method) == callsCount.end()
                            ? 0 : callsCount.find(method)->second;
    }
    void resetMethodCallsCount(const std::string & method)   {
        callsCount[method] = 0;
    }
    void resetCallsCount(){
        callsCount.clear();
    }

    bool isResponseEnabled() const {return respondEnabled;}
    void setResponseEnabled(bool enabled) {this->respondEnabled = enabled;}

    const std::string & getResponse(const std::string & method){
        return responses[method];
    }
    void setResponse(const std::string &method, const std::string &value){
        responses[method] = value;
    }

    const StatusResponse & getStatusResponse(const std::string & method){
        return statusResponses[method];
    }
    void setStatusResponse(const std::string & method, const StatusResponse & value) {
        statusResponses[method] = value;
    }

    bool onRequest(Request & request, Response & response);

protected:
    void respond(Response & response, const std::string & method);
};



#endif // MOCKCONTROLLER_H
