#ifndef MOCKREQUESTROUTER_H
#define MOCKREQUESTROUTER_H

#include "network/controller/RequestRouter.h"
#include "network/Request.h"
#include "network/Response.h"
#include "network/Url.h"

using calmframe::network::Request;
using calmframe::network::Response;
using calmframe::network::Url;

class MockRouter : public ::calmframe::RequestRouter{
public:
    bool handleForward;
    Url forwardedUrl;

    MockRouter()
        : handleForward(true)
    {}

    bool forward(Request &request, Response &response, const Url &url){
        this->forwardedUrl = url;
        return handleForward;
    }
};

#endif // MOCKREQUESTROUTER_H

