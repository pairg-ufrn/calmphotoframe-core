#include "catch.hpp"

#include "JsonConfigurationLoader.h"
#include "Configurations.h"

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
using namespace std;
using namespace calmframe;

namespace Constants{
    const char * configurationFile = "test/data/conftest.json";

    const int numberOfKeys = 4;
    const char * expectedKeys[]   = {"text", "integer", "real", "boolean"};
    const char * expectedValues[] = {"value", "10", "0.01", "true"};
}

class JsonConfigurationLoaderFixture{
public:
    string configurationFilename;
    ifstream fileStream;
    JsonConfigurationLoader configurationLoader;

    JsonConfigurationLoaderFixture()
        : configurationFilename(Constants::configurationFile)
    {}
    ~JsonConfigurationLoaderFixture(){
        fileStream.close();
    }

    ifstream * openConfigurationFile(){
        fileStream.open(configurationFilename.c_str(),ios_base::in);
        return &fileStream;
    }
};

SCENARIO("Loading configuration from json file", "[JsonConfigurationLoader]"){
    GIVEN("an json configuration file"){
        JsonConfigurationLoaderFixture fixture;
        ifstream * fileStream = fixture.openConfigurationFile();
        REQUIRE(fileStream != NULL);
        REQUIRE(fileStream->good());
    GIVEN("a Configurations instance"){
        Configurations configurations;
    WHEN("load configurations"){
        fixture.configurationLoader.load(*fileStream, configurations);
    THEN("it should read all configurations from json file"){
        for(unsigned i=0; i<Constants::numberOfKeys; ++i){
            CHECK(configurations.get(Constants::expectedKeys[i]) == Constants::expectedValues[i]);
        }
    }//THEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}//SCENARIO
