#include "QueryTest.h"

#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/tables/PhotoTable.h"
#include "data/data-sqlite/tables/PhotoMetadataTable.h"
#include "data/data-sqlite/readers/GenericDbReader.h"
#include "data/data-sqlite/parsers/PhotoDbParser.h"
#include "helpers/PhotoHelper.h"

#include <set>

using Catch::Contains;

FIXTURE_SCENARIO("Query initialization"){
    testQueryInitialization();
}

FIXTURE_SCENARIO("Query can be initialized using a list of columns"){
    testQueryInitializeWithColumnsList();
}

FIXTURE_SCENARIO("Read data from a query"){
    testReadData();
}

FIXTURE_SCENARIO("Query should allow read data using a DatabaseReader"){
    testReadWithReader();
}

FIXTURE_SCENARIO("Query should support use of 'where' templates"){
    testWhereTemplate();
}

FIXTURE_SCENARIO("Query should support the use of QueryArgs"){
    testCreateFromQueryArgs();
}

/* ************************************* Test implementation **********************************************/

using namespace Tables;

void QueryFixture::testQueryInitialization(){

GIVEN("a SqliteConnector to an existent database"){
    SqliteConnector & connector = getConnector();

GIVEN("a Table instance related to some table on db"){
    Table photoTable;
    Tables::PhotoTable::buildTable(photoTable);

WHEN("when creating a SqlQuery from that table"){
    Query query(photoTable);

THEN("it should create a default query statement template"){
    CHECK(query.getStatementTemplate() == "SELECT * FROM " + photoTable.getName());

AND_THEN("it should not be initialized yet"){
    CHECK(!query.isInitialized());

    AND_WHEN("Calling initialize passing an SqliteConnector"){
        query.initialize(connector);

    THEN("it should be initialized"){
        CHECK(query.isInitialized());
    }//THEN
    }//WHEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void QueryFixture::testQueryInitializeWithColumnsList()
{
    long expectedId = PhotoHelper::insertPhoto();

WHEN("initializing a Query instance with a list of columns"){
    Query query(createPhotoTable(), {}, {PhotoTable::Columns::ID, PhotoTable::Columns::CREATEDAT});

THEN("it should create a select statement using that columns"){
    CHECK_THAT(query.getStatementTemplate(), Contains(PhotoTable::Columns::ID));
    CHECK_THAT(query.getStatementTemplate(), Contains(PhotoTable::Columns::CREATEDAT));

AND_WHEN("trying to read using that query"){
    query.initialize(getConnector());
    REQUIRE(query.read());

THEN("the absent columns should return the default values"){

    const string defaultStr = "default";
    CHECK(query.getString(PhotoTable::Columns::DATE, defaultStr) == defaultStr);
    CHECK(query.getString(PhotoTable::Columns::MOMENT, defaultStr) == defaultStr);

THEN("the specified columns should be normally readable"){
    CHECK(query.getLong(PhotoTable::Columns::ID) == expectedId);
    CHECK(! query.getString(PhotoTable::Columns::CREATEDAT).empty());

}//THEN
}//THEN
}//WHEN
}//THEN

}//GIVEN

}//test

void QueryFixture::testReadData(){

GIVEN("some rows on a db table"){
    std::vector<PhotoMetadata> rows = {{-1, 40000, 100, 100, "somefile.jpg"}, {-1, 4 * 1024 * 1024,1024,1024, "otherfile.png"}};
    rows[0].setLocation({0.0, 1.0, 2000});
    rows[1].setLocation({0.0, -5.0, 100});

    for(auto m : rows){
        PhotoHelper::insertPhoto({PhotoDescription{}, m});
    }

WHEN("making a query to that table"){
    Table metadataTable;
    Tables::PhotoMetadataTable::buildTable(metadataTable);

    Query query(metadataTable);
    query.initialize(getConnector());

THEN("it should allow read the data on each column"){
    int countReads = 0;

    while(query.read()){
        const PhotoMetadata & expectedRow = rows[countReads];

        CHECK(expectedRow.getOriginalFilename() == query.getString(PhotoMetadataTable::Columns::ORIGINALFILENAME));
        CHECK(expectedRow.getImageSize() == query.getLong(PhotoMetadataTable::Columns::IMAGESIZE));
        CHECK(expectedRow.getImageWidth() == query.getInt(PhotoMetadataTable::Columns::IMAGEWIDTH));
        CHECK(expectedRow.getLocation().getLongitude()
                                          == Approx(query.getFloat(PhotoMetadataTable::Columns::LOCATION_LONGITUDE)));

        ++countReads;
    }
    CHECK(countReads == rows.size());
}//THEN
}//WHEN
}//GIVEN

}

void QueryFixture::testReadWithReader(){

GIVEN("some data on database"){
    set<long> ids = PhotoHelper::insertPhotos();

GIVEN("a database reader to read that data"){
    vector<Photo> readedData;
    GenericDbReader<Photo, PhotoDbParser> reader(&readedData);

WHEN("using that reader to read data"){
    Query query(createPhotoTable());
    query.initialize(getConnector());

    query.read(reader);

THEN("it should read until finish data or when the reader has finished"){
    CHECK(readedData.size() == ids.size());

    std::sort(readedData.begin(), readedData.end());
    for(const Photo & photo : readedData){
        CHECK(ids.find(photo.getId()) != ids.end());
    }

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void QueryFixture::testWhereTemplate(){
GIVEN("some data on a db table"){
    const string queryString = "query";

    vector<Photo> filteredOutPhotos = {
         {PhotoDescription{"", queryString}} , {PhotoDescription{queryString}} //filtered out because of id number
        ,{} //filtered out because has no query string
    };

    vector<Photo> expectedPhotos = {
        {PhotoDescription{"desc: " + queryString}}
        , {PhotoDescription{"", queryString  + " lorem ipsum"}}
    };

    PhotoHelper::insertAll(filteredOutPhotos);
    PhotoHelper::insertAll(expectedPhotos);
    PhotoHelper::insertPhotos(2); //other filtered photos

GIVEN("a valid sql WHERE condition (optionally) with sqlite parameters"){
    Table photoTable = createPhotoTable();

    string whereCond = Table::AND(Table::WhereCompare(PhotoTable::Columns::ID, ">", "?")
                                , photoTable.queryTemplate("?2"));

WHEN("Initializing a query with that condition and the specified table"){
    Query query(photoTable, whereCond);
    query.initialize(getConnector());

THEN("it should create a statement template with the where condition"){
    CHECK_THAT(query.getStatementTemplate(), Contains("WHERE"));
    CHECK_THAT(query.getStatementTemplate(), Contains(whereCond));

THEN("it allow bind values to Query"){
    query.appendValue(2);
    query.appendTextValue(queryString);

WHEN("reading data using that query"){
    vector<Photo> readedData;
    GenericDbReader<Photo, PhotoDbParser> reader(&readedData);

    query.read(reader);

THEN("it should filter the returned data based on the where condition"){
    REQUIRE(readedData.size() == expectedPhotos.size());

    std::sort(readedData.begin(), readedData.end());
    std::sort(expectedPhotos.begin(), expectedPhotos.end());

    for(int i=0; i < readedData.size(); ++i){
        //Compare only description because of id
        CHECK(readedData[i].getDescription() == expectedPhotos[i].getDescription());
    }
}//THEN
}//WHEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void QueryFixture::testCreateFromQueryArgs(){

WHEN("creating a Query with a QueryArgs instance"){
    QueryArgs args(
        Table::WhereEquals(PhotoTable::Columns::PLACE, "?")
        , 200 //TODO:: allow parameterize
        ,  PhotoTable::Columns::ID //order by
        , QueryArgs::DESC
    );

    Query query(createPhotoTable(), args);

THEN("it should create statement using that arguments"){
    CHECK_THAT(query.getStatementTemplate(), Contains(args.getWhereCondition()));
    CHECK_THAT(query.getStatementTemplate(), Contains("LIMIT " + std::to_string(args.getLimit())));
    CHECK_THAT(query.getStatementTemplate(), Contains("ORDER BY " + args.getOrderBy()));
    CHECK_THAT(query.getStatementTemplate(), Contains("DESC"));
}//THEN
}//WHEN

}//test


/* ************************************* Fixture implementation **********************************************/

SqliteConnector &QueryFixture::getConnector(){
    SqliteData & sqliteData = static_cast<SqliteData &>(Data::instance());
    return sqliteData.getDatabaseConnector();
}

Table QueryFixture::createPhotoTable(){
    Table photoTable;
    Tables::PhotoTable::buildTable(photoTable);
    return photoTable;
}
