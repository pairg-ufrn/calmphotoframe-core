#include "TestMacros.h"
#include "helpers/StepedRunnable.h"
#include "helpers/Timer.h"

#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/SqlException.hpp"
#include "exceptions/BusyDatabaseException.h"
#include "utils/ScopedFile.h"
#include "utils/FileUtils.h"

#include "Poco/Thread.h"
#include "sqlite3.h"


using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace calmframe::tests;

class SqliteConnectorFixture{
public:
    void testBeginWriteTransaction();
public:
    ScopedFile scopedDbFile;
    SqliteConnector connector;

    SqliteConnectorFixture();
    SqliteConnectorFixture(const string & dbPath);
};

#define TEST_TAG "[SqliteConnector]"
#define FIXTURE SqliteConnectorFixture


/* ********************************************* Tests definition **********************************************/

FIXTURE_SCENARIO("Sqlite should be thread-safe compatible"){
    CHECK(sqlite3_threadsafe());
}

FIXTURE_SCENARIO("Begin Writable transaction"){
    testBeginWriteTransaction();
}


/* ***************************************** Test cases implementation *****************************************/

void SqliteConnectorFixture::testBeginWriteTransaction(){

GIVEN("some write transaction started in a given SqliteConnector"){
    connector.beginTransaction(TransactionType::Write);
    CHECK(connector.isOnTransaction());

WHEN("starting another write transaction with other thread"){
    SqliteConnector otherConnector(connector.getDatabasePath());
    int timeoutMillis = 100;
    otherConnector.setBusyTimeout(timeoutMillis);

THEN("it should wait until timeout and throws an LockedDatabaseException"){
    Timer timer;

    CHECK_THROWS_AS(otherConnector.beginTransaction(TransactionType::Write), BusyDatabaseException);

    double elapsedMillis = timer.elapsed()*1000;
    CHECK(elapsedMillis >= timeoutMillis);

WHEN("finishing the first transaction"){
    connector.endTransaction();

THEN("other write transaction can be started on the second connector"){
    otherConnector.beginTransaction(TransactionType::Write);
    otherConnector.endTransaction();
}//THEN
}//WHEN
}//THEN
}//WHEN
}//GIVEN

}//test

/* ***************************************** Fixture implementation *****************************************/

SqliteConnectorFixture::SqliteConnectorFixture()
    : SqliteConnectorFixture(Files().generateUniqueFilepath())
{}

SqliteConnectorFixture::SqliteConnectorFixture(const string & dbPath)
    : connector(dbPath)
    , scopedDbFile(dbPath)
{}
