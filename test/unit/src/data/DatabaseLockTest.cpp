#include "TestMacros.h"

#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/dao/SqliteSessionDAO.h"
#include "data/data-sqlite/SqliteData.h"

#include "utils/FileUtils.h"

#include <string>
#include <Poco/Thread.h>
#include <Poco/Runnable.h>
#include <Poco/Condition.h>
#include <Poco/ScopedLock.h>

#include "helpers/StepedRunnable.h"
#include "utils/ScopedFile.h"
#include "helpers/DatabaseWriteRunnable.h"
#include "helpers/ScopedStepedRunnableExecution.h"
#include "utils/ScopedDatabase.h"


#include "mocks/FakeDataBuilder.hpp"

#include <thread>

using namespace std;
using namespace Poco;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace calmframe::tests;

namespace Constants {
    static const char * DB_PATH = "/tmp/testdatabase.db";
    static const unsigned MAX_WAIT_MILLIS = 1500;
    static const unsigned WAIT_BLOCKED_MILLIS = 400;

}

class DatabaseMultithreadFixture{
public:
    void testDataCreationFromDifferentThreads();
    void testInsertOnDatabaseFromDifferentThreads();
public:
    std::shared_ptr<FakeDataBuilder> mockBuilder;

    DatabaseMultithreadFixture() : mockBuilder(std::make_shared<FakeDataBuilder>())
    {}

    void createDatabase(SqliteConnector & database);

    void startDataWritableThread(DatabaseWriteRunnable & runnable1, Thread & thread1);
};

#define FIXTURE DatabaseMultithreadFixture
#define TEST_TAG "[Data],[Threads]"

class DataWriterRunnable : public StepedRunnable{
private:
    volatile int executeCount;
public:
    DataWriterRunnable() : executeCount(0)
    {}

    bool executeStep(){
        if(executeCount == 0){
            Data::instance().users().insert(UserAccount());
        }
        ++executeCount;

        //Executes twice to avoid one thread be deleted before the other have been created
        return executeCount < 2;
    }
};


/* ******************************************************************************************/

FIXTURE_SCENARIO("Data creation from different threads"){
    this->testDataCreationFromDifferentThreads();
}//SCENARIO

//FIXTURE_SCENARIO("Insert on database from different threads"){
//    this->testInsertOnDatabaseFromDifferentThreads();
//}

/* ******************************************************************************************/

void DatabaseMultithreadFixture::testDataCreationFromDifferentThreads(){


GIVEN("that the Data instance is configured with some DataBuilder"){
    Data::setup(mockBuilder);

WHEN("trying to access the Data singleton from different threads"){
    DataWriterRunnable runnable1, runnable2;
    Poco::Thread t1, t2;

    t1.start(runnable1);
    t2.start(runnable2);

THEN("a different Data instance should be created for each thread"){
    runnable1.waitStep();
    runnable2.waitStep();

    runnable1.waitNextStep();
    runnable2.waitNextStep();

    runnable1.finish();
    runnable2.finish();

    t1.join();
    t2.join();

    CHECK(mockBuilder->getBuildCount() == 2);
    CHECK(mockBuilder->getCloneCount() == 0);

    const vector<void *> & buildedPtrs = mockBuilder->getBuildedPointers();
    REQUIRE(buildedPtrs.size() == 2);
    CHECK(buildedPtrs[0] != buildedPtrs[1]);

    CHECK(runnable1.hasFinished());
    CHECK(runnable2.hasFinished());
    CHECK(!runnable1.wasForceFinished());
    CHECK(!runnable2.wasForceFinished());
}//THEN
}//WHEN
}

}//test

class Runner : public Poco::Runnable{
public:
    vector<long> photoIds;

    ResetableSemaphore * externalSemaphore;
    long id;
    ResetableSemaphore localSemaphore;
    Poco::Mutex mutex;

    Runner(ResetableSemaphore * externalSemaphore=NULL)
        : externalSemaphore(externalSemaphore), id(0)
    {}

    virtual void run(){
        std::cout << "Runner "<<id<<" begin!" << std::endl;
        beginInsertion();
        finishInsertion();
        std::cout << "Runner "<<id<<" finished insertion" << std::endl;
    }

    void beginInsertion(){
        Data::instance().beginTransaction();

        std::cout << "Runner "<<id<<" started transaction!" << std::endl;

        try{

            std::cout << "Runner "<<id<<" before insert!" << std::endl;
            long photoId = Data::instance().photos().insert(Photo(PhotoDescription{"blah"}));

            std::cout << "Runner "<<id<<" finished insertion!" << std::endl;
            cout << "photoId: " << photoId << endl;

            photoIds.push_back(photoId);
        }
        catch(std::exception & ex){
            cout << "error: " << ex.what() << endl;
            externalSemaphore->signal();
            throw;
        }

        std::cout << "Runner "<<id<<" inserted photo" << std::endl;

        externalSemaphore->signal();
        Poco::ScopedLock<Poco::Mutex> lock(mutex);

        std::cout << "Runner "<<id<<" wait condition" << std::endl;
        bool success = localSemaphore.tryWait(mutex, 60000);
        std::cout << "Runner "<<id<<" wait success: " << success << std::endl;

    }
    void finishInsertion(){
        try{
            cout << "num photos: "<< Data::instance().photos().listAll().size() << endl;
            Data::instance().commit();
            cout << "num photos(after commit): "<< Data::instance().photos().listAll().size() << endl;

        }
        catch(std::exception & ex){
            cout << "error on finish: " << ex.what() << endl;
            throw;
        }
    }
};

void DatabaseMultithreadFixture::testInsertOnDatabaseFromDifferentThreads(){
    ScopedSqliteDatabase scopedDb;

    int numThreads = 3;

    ResetableSemaphore externalSemaphore(-numThreads, 0);
    Poco::Mutex mutex;

    Runner runners[numThreads];

    for(int i=0; i < numThreads; ++i){
        runners[i].id = i;
        runners[i].externalSemaphore = &externalSemaphore;
    }

    Poco::Thread threads[numThreads];

//    t1.start(r1);
//    t2.start(r2);

    for(int i=0; i < numThreads; ++i){
        threads[i].start(runners[i]);
    }

//    CHECK(r1.mutex.tryLock());
//    CHECK(r1.externalSemaphore.tryWait(r1.mutex, 500));
//    r1.mutex.unlock();

//    CHECK(r2.mutex.tryLock());
//    CHECK(r2.externalSemaphore.tryWait(r2.mutex, 500));
//    r2.mutex.unlock();

    CHECK(mutex.tryLock());
    std::cout << "test wait condition" << std::endl;
    bool waited = externalSemaphore.tryWait(mutex, 60000);
    std::cout << "test finished wait. success: " << waited << std::endl;
    CHECK(waited);
    mutex.unlock();

    for(int i=0; i < numThreads; ++i){
        std::cout << "test (before) signal "<< i <<"!" << std::endl;
        runners[i].localSemaphore.signal();
    }

    for(int i=0; i < numThreads; ++i){
        threads[i].join();

        std::cout << "joined thread "<< i << std::endl;

    }
    for(int i=0; i < numThreads; ++i){
        CAPTURE(i);
        CHECK(runners[i].photoIds.size() == 1);
        if(!runners[i].photoIds.empty()){
            CHECK(Data::instance().photos().exist(runners[i].photoIds[0]));
        }
    }

    auto photos = Data::instance().photos().listAll();
    CHECK(photos.size() == numThreads);


}//test


/* ******************************************************************************************/

void DatabaseMultithreadFixture::createDatabase(SqliteConnector & database){
    SqliteData data(database);
}

void DatabaseMultithreadFixture::startDataWritableThread(DatabaseWriteRunnable & runnable1, Thread & thread1){
    runnable1.reset();
    thread1.start(runnable1);
    //Wait until first step
    runnable1.waitStep();
}
