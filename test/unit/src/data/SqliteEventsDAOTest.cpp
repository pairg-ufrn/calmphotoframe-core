#include "TestMacros.h"

#include "utils/ScopedDatabase.h"
#include "utils/ScopedFile.h"
#include "helpers/PhotoHelper.h"

#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/dao/SqliteEventsDAO.h"
#include "data/data-sqlite/tables/EventsTable.h"
#include "data/DataTransaction.h"

#include "model/events/Event.h"

#include <vector>
#include <algorithm>

using namespace calmframe;
using namespace calmframe::utils;
using namespace calmframe::data;
using namespace std;

#define TEST_TAG "[EventsDAO]"
#define FIXTURE EventsDAOFixture

class EventsDAOFixture{
public:
    void testTableCreation();
    void testEventsInsertion();
    void testListLastEvents();

public:
    vector<Event> insertEvents(int count);
    vector<Event> & insertEvents(vector<Event> & events);
public:
    ScopedSqliteDatabase scopedDb{ScopedSqliteDatabase::DbType::InMemory};
};


/* **********************************************************************************************/

FIXTURE_SCENARIO("Events table creation"){
    testTableCreation();
}//SCENARIO

FIXTURE_SCENARIO("Events insertion"){
    testEventsInsertion();
}//SCENARIO

FIXTURE_SCENARIO("List the last N events"){
    testListLastEvents();
}//SCENARIO

/* **********************************************************************************************/


void EventsDAOFixture::testTableCreation(){

GIVEN("a SqliteConnector"){
    SqliteConnector connector = SqliteConnector::createInMemoryDb();

GIVEN("a SqliteEventsDAO instance"){
    SqliteEventsDAO eventsDAO(&connector);

WHEN("creating table"){
    REQUIRE(!connector.existTable(Tables::EventsTable::NAME));
    CHECK(eventsDAO.createTableIfNotExist(connector));

THEN("a new Event Table should be created on database"){
    CHECK(connector.existTable(Tables::EventsTable::NAME));

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void EventsDAOFixture::testEventsInsertion(){

GIVEN("a EventsDAO instance"){
    SqliteEventsDAO & eventsDAO = (SqliteEventsDAO &)static_cast<SqliteData &>(Data::instance()).events();

GIVEN("some Event instance"){
    long photoId = PhotoHelper::insertPhoto({});
    Event evt{EventType::PhotoCreation};
    evt.setEntityId(photoId);
    
WHEN("inserting it on database"){
    long id = eventsDAO.insert(evt);

THEN("a new valid id should be returned"){
    CHECK(id != Event::InvalidId);
    CHECK(eventsDAO.exist(id));

AND_WHEN("getting an Event instance with that id"){
    Event dbEvt = eventsDAO.get(id);

THEN("the DAO should return a Event instance equals to the inserted one"){
    CHECK(dbEvt.getId() == id);
    CHECK(dbEvt.getType()  == evt.getType());
    CHECK(dbEvt.getCreation() == evt.getCreation());
    CHECK(dbEvt.getEntityId() == photoId);
}//THEN
}//WHEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void EventsDAOFixture::testListLastEvents(){

GIVEN("a EventsDAO instance"){
    EventsDAO & dao = Data::instance().events();

GIVEN("some events on database"){
    vector<Event> events = {
        {EventType::PhotoCreation   , Event::InvalidUserId, Event::InvalidId, Date{2016, 1, 1}}
        , {EventType::PhotoCreation , Event::InvalidUserId, Event::InvalidId, Date{2000, 12, 10, 10, 10, 10}}
        , {EventType::PhotoRemove   , Event::InvalidUserId, Event::InvalidId, Date{2000, 12, 10, 10, 10, 9}}
        , {EventType::PhotoCreation , Event::InvalidUserId, Event::InvalidId, Date{2000, 12, 10, 10, 10, 10}}
        , {EventType::PhotoUpdate   , Event::InvalidUserId, Event::InvalidId, Date{2015, 2, 20, 5, 3}}
        , {EventType::PhotoUpdate   , Event::InvalidUserId, Event::InvalidId, Date{2015, 2, 20, 6, 3}}
    };

    insertEvents(events);

WHEN("listing the last N events"){
    const int N = 4;
    vector<Event> recentEvents = dao.listRecents(N);

THEN("it should return the N (or less) most recent events"){
    CHECK(recentEvents.size() == N);
    std::sort(events.begin(), events.end(), std::greater<Event>());

    for(int i=0; i < N; ++i){
        CAPTURE(i);
        //Query uses only date to order, so can occur variations if there is more than one event with same date
        CHECK(events[i].getCreation() == recentEvents[i].getCreation());
    }

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

/* **********************************************************************************************/

vector<Event> EventsDAOFixture::insertEvents(int count){
    vector<Event> events;

    for(int i=0; i < count; ++i){
        events.push_back({EventType::PhotoCreation});
    }

    return insertEvents(events);
}

vector<Event> &EventsDAOFixture::insertEvents(vector<Event> & events){
    DataTransaction transaction(Data::instance());

    for(Event & evt : events){
        long id = Data::instance().events().insert(evt);
        evt.setId(id);
    }

    transaction.commit();

    return events;
}
