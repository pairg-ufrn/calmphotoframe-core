#include "TestMacros.h"

#include "utils/ScopedDatabase.h"

#include <string>


using namespace std;
using namespace calmframe;
using namespace calmframe::data;

#define TEST_TAG "[ConfigurationsDAO]"
#define FIXTURE ConfigurationsDAOFixture

class ConfigurationsDAOFixture{
public:
    void testPutAndGetValue();
    
public:
    void when_putting_value(const std::string & key, const std::string & value);
    string when_getting_value(const std::string & key);
    
public:
    ScopedSqliteDatabase scopedDb{ScopedSqliteDatabase::DbType::InMemory};
};

/******************************************************************************************************/

TEST_SCENARIO("Put and get configuration")  IS_DEFINED_BY(testPutAndGetValue)

/******************************************************************************************************/

void ConfigurationsDAOFixture::testPutAndGetValue(){
    GIVEN("an string key and a string value"){
        string key = "anyKey", value = "anyValue";
        
    WHEN("putting it on database"){
        this->when_putting_value(key, value);
    
    WHEN("getting the same key again"){
        auto result = when_getting_value(key);
    
    THEN("both values should be equals"){
        CHECK(value == result);
        
    }//THEN
    }//WHEN
    }//WHEN
    }//GIVEN
}


/******************************************** Steps ***************************************************/

void ConfigurationsDAOFixture::when_putting_value(const string & key, const string & value){
    Data::instance().configs().put(key, value);
}

string ConfigurationsDAOFixture::when_getting_value(const string & key){
    return Data::instance().configs().get(key);
}

