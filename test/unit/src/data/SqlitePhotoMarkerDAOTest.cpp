#include "TestMacros.h"

#include "data/data-sqlite/dao/SqlitePhotoMetadataDAO.h"
#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/ScopedTransaction.h"

#include "model/photo/PhotoMetadata.h"
#include "services/image/MetadataExtractor.h"
#include "utils/FileUtils.h"

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "helpers/PhotoHelper.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace calmframe::photo;

namespace calmframe {
namespace Constants{
    static const int PhotoListSize = 5;
    static const int INEXISTENT_ID = 100000;

    static const char * NEW_PHOTOMARKER_NAME = "New Name";
}
}

class SqliteDatabaseTestFixture{
public:
    static void testPhotoMarkCrud();
    static void testEraseMarkers();
    void testQueryMarkersPhoto();
public:
    SqliteData database;

    SqliteDatabaseTestFixture()
     :  database(SqliteConnector::createInMemoryDb())
    {
    }

    PhotoMetadata createMetadata() const;
    vector<PhotoMarker> createMarkers() const;

    PhotoDescription createDescription() const;

    Photo createPhoto() const;
    Photo insertPhoto(const Photo & photo);
    Photo insertPhotoWithMarkers();

    template<typename MarkersCollection>
    set<long> insertPhotosForMarkers(const MarkersCollection & markersWithQuery);

    template<typename MarkersCollection>
    void insertMarkers(Photo & photo, const MarkersCollection & markers);
};

#define FIXTURE SqliteDatabaseTestFixture
#define TEST_TAG "[PhotoMarkerDAO][data]"

SCENARIO("CRUD photomark on database", TEST_TAG){
    SqliteDatabaseTestFixture::testPhotoMarkCrud();
}

SCENARIO("Erase PhotoMarkers from Photo", TEST_TAG){
    SqliteDatabaseTestFixture::testEraseMarkers();
}

FIXTURE_SCENARIO("Query on PhotoMarkers"){
    this->testQueryMarkersPhoto();
}

/* ****************************************** Test Implementation **********************************************/

void SqliteDatabaseTestFixture::testPhotoMarkCrud(){
    GIVEN("a database instance"){
        SqliteDatabaseTestFixture fixture;
    GIVEN("some photo instance on database with markers"){
        Photo photo = fixture.insertPhotoWithMarkers();
        const vector<PhotoMarker> & createdMarkers = photo.getDescription().getMarkers();
        REQUIRE(createdMarkers.size() > 0);

    GIVEN("some PhotoMarker from that Photo"){
        PhotoMarker marker = photo.getDescription().getMarker(0);

    WHEN("updating this PhotoMarker"){
        marker.setName(Constants::NEW_PHOTOMARKER_NAME);

        CHECK_NOTHROW(fixture.database.getPhotoMarkerDAO().update(marker));

    THEN("it should change on database"){
        vector<PhotoMarker> updatedMarkers = fixture.database.getPhotoMarkerDAO().listAll(marker.getPhotoId());
        PhotoMarker & updatedMarker = updatedMarkers[0];
        CHECK(updatedMarker.getName() == marker.getName());
        CHECK(updatedMarker.getIndex() == marker.getIndex());
    WHEN("deleting that photo marker from database"){
        fixture.database.getPhotoMarkerDAO().remove(updatedMarker);
    THEN("the database should not contain that instance anymore"){
        vector<PhotoMarker> photoMarkers = fixture.database.getPhotoMarkerDAO().listAll(photo.getId());
        REQUIRE(photoMarkers.size() == createdMarkers.size() - 1);
        for(size_t i=0; i < photoMarkers.size(); ++i){
            CHECK(photoMarkers[i] == createdMarkers[i + 1]);
        }
    }
    }
    }
    }
    }
    }
    }
}

void SqliteDatabaseTestFixture::testEraseMarkers(){
    GIVEN("a database instance"){
        SqliteDatabaseTestFixture fixture;
    GIVEN("some photo instance on database with markers"){
        Photo photo = fixture.insertPhotoWithMarkers();
        const vector<PhotoMarker> & createdMarkers = photo.getDescription().getMarkers();
        REQUIRE(createdMarkers.size() > 0);
    WHEN("updating that photo with 0 markers"){
        photo.getDescription().setMarkers(vector<PhotoMarker>());
        bool updated = fixture.database.photos().update(photo);
        REQUIRE(updated == true);
    THEN("the markers on photo should be erased"){
        Photo updatedPhoto;
        fixture.database.photos().getPhoto(photo.getId(), updatedPhoto);

        CHECK(updatedPhoto.getDescription().getMarkers().size() == 0);
    }//THEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}//test

void SqliteDatabaseTestFixture::testQueryMarkersPhoto(){

GIVEN("some photomarkers on database"){
    string queryString = "Lorem ipsum";
    vector<PhotoMarker> markersWithQuery = {
        PhotoMarker("any " + queryString),
        PhotoMarker(queryString),
        PhotoMarker("joined with word" + queryString),
        PhotoMarker("Case: LOREM ipsum")
    };

    vector<PhotoMarker> markersWithoutQuery = {
        PhotoMarker("similar: Lor em ipsum"),
        PhotoMarker("incomplete: Lorem ipsu"),
        PhotoMarker("order: ipsum Lorem"),
        PhotoMarker("")
    };

    set<long> matchPhotoIds = insertPhotosForMarkers(markersWithQuery);
    insertPhotosForMarkers(markersWithoutQuery);

WHEN("searching for the photos that contains some queryString within a marker description"){
    set<long> resultPhotoIds = database.getPhotoMarkerDAO().queryPhotos(queryString);

THEN("all markers with that queryString on description(independent of case) should be returned"){
    REQUIRE(matchPhotoIds.size() == resultPhotoIds.size());

    CHECK(matchPhotoIds == resultPhotoIds);
}//THEN
}//WHEN
}//GIVEN

}//test


/* ****************************************** Fixture **********************************************/

PhotoMetadata SqliteDatabaseTestFixture::createMetadata() const{
    PhotoMetadata metadata;
    metadata.setCameraMaker("CameraMaker");
    metadata.setCameraModel("CameraModel");
    metadata.setImageSize(100000);
    metadata.setImageHeight(100);
    metadata.setImageWidth(100);

    return metadata;
}

vector<PhotoMarker> SqliteDatabaseTestFixture::createMarkers() const{
    vector<PhotoMarker> markers;
    for(int i=0; i < Constants::PhotoListSize; ++i){
        markers.push_back(PhotoMarker("id", i,i));
    }
    return markers;
}

PhotoDescription SqliteDatabaseTestFixture::createDescription() const{
    PhotoDescription description;
    description.setDate("now");
    description.setMarkers(createMarkers());
    return description;
}

Photo SqliteDatabaseTestFixture::createPhoto() const{
    Photo photo;
    photo.setMetadata(this->createMetadata());
    photo.setDescription(createDescription());
    return photo;
}

Photo SqliteDatabaseTestFixture::insertPhoto(const Photo & photo){
    long photoId = this->database.photos().insert(photo);

    Photo outPhoto;
    this->database.photos().getPhoto(photoId, outPhoto);
    return outPhoto;
}

Photo SqliteDatabaseTestFixture::insertPhotoWithMarkers(){
    Photo createdPhoto = createPhoto();
    return insertPhoto(createdPhoto);
}

template<typename MarkersCollection>
inline void SqliteDatabaseTestFixture::insertMarkers(Photo & photo, const MarkersCollection & markers){
    ScopedTransaction transaction(&database.getDatabaseConnector());

    for(auto const & marker : markers){
        photo.getDescription().addMarker(marker);
        int markerIndex = photo.getDescription().getMarkerCount() - 1;
        database.getPhotoMarkerDAO().insert(photo.getId(), photo.getDescription().getMarker(markerIndex));
    }

    transaction.commit();
}


template<typename MarkersCollection>
inline set<long> SqliteDatabaseTestFixture::insertPhotosForMarkers(const MarkersCollection & markers){
    set<long> photoIds;

    ScopedTransaction transaction(&database.getDatabaseConnector());

    for(auto const & marker : markers){
        Photo photo = insertPhoto(Photo{});
        photo.getDescription().addMarker(marker);

        INFO("Insert Photo " << photo.getId() << " with marker: " << photo.getDescription().getMarker(0).getIndex());

        database.getPhotoMarkerDAO().insert(photo.getId(), photo.getDescription().getMarker(0));

        photoIds.insert(photo.getId());
    }

    transaction.commit();

    return photoIds;
}
