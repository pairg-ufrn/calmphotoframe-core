#include "TestMacros.h"

#include "data/data-sqlite/Sql.h"
#include "data/data-sqlite/DatabaseMigration.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/SchemaManager.h"
#include "data/data-sqlite/readers/CountReader.h"
#include "data/data-sqlite/readers/EntityReader.h"
#include "data/data-sqlite/QueryArgs.h"
#include "data/data-sqlite/ScopedTransaction.h"
#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/Query.h"
#include "data/data-sqlite/tables/PhotoTable.h"
#include "data/data-sqlite/tables/MarkersTable.h"
#include "utils/FileUtils.h"
#include "utils/ScopedFile.h"
#include "utils/StringCast.h"
#include "utils/StringUtils.h"
#include "utils/Log.h"

#include <map>
#include <sstream>
#include <fstream>

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace Tables;

using Entity = EntityReader::Entity;

class DataMigrationFixture{
public:
public://tests
    void testUpgradeFromV1ToV2();
    void testUpgradeToV3();
    void testUpgradeToV4();
    void testUpgradeToV5();
    void testUpgradeToV6();
    void testUpgradeToV7();
    void testOnUpgradeToV8_AddMissingForeignKeysConstraints();
    void testOnUpgradeToV8_UpdateLocalUserId();
    void testUpgradeToInvalidVersion();
    void testUpgradeAlreadyUpgradedDb();
    void testSqliteDataUpgrade();

public:
    DataMigrationFixture();

    bool checkExistEntity(SqliteConnector & connector, const std::string & table
                            , const Entity & entity);
    std::set<long> insertEntities(SqliteConnector & connector, const std::string & table, const vector<string> & columns, const vector<vector<string> > & values);
    std::set<long> insertEntity(SqliteConnector & connector, 
        const string & table,
        const Entity & entity);
    
    void setupDbV1();
    void setupDbV3();
    void importDb(const char * dbName);
    string getTestDb(const char * dbName);

    void checkInvalidMigration(SqliteConnector & conn, int targetVersion);
    void debugEntities(SqliteConnector connector, int numEntities);

    vector<vector<string> > insertBasicPhotoContexts(SqliteConnector & connector, vector<string> * outInsertCols=NULL);
    vector<Entity> queryEntity(SqliteConnector & connector, const string & statement);
    void executeSqlFile(const string & dbPath);

public:
    Table given_table(const std::string & tableName);
    void given_a_database_version(int version);
    Entity given_an_existent_user_instance_in_a_v7_db(const string & userId);
    map<std::string, Entity> given_some_user_related_instances(const string & userId);
    
    void when_migrating_to_version(int version);
    
    void then_all_entities_should_have_been_updated(
        std::map<std::string, Entity> relatedInstances, 
        const std::map<string, string> & valuesToUpdate);
    
    bool has_table(const std::string & tableName);
    
    std::string entityToString(const Entity & e);
public:
    SqliteConnector connector;
    SchemaManager schemaManager;
    DatabaseMigration migration;

    
    
    
    void printEntities(const string & table);
    
protected:
    static constexpr const char * const PhotoContextTable_v4 = "PhotoContext";
};

#define TEST_TAG "[DataMigration]"
#define FIXTURE DataMigrationFixture

namespace Constants{
    const char * const testDbsDir = "test/data/databases";
}

/* **********************************************************************************************/

TEST_SCENARIO("Upgrade from v1 to v2")  IS_DEFINED_BY(testUpgradeFromV1ToV2)
TEST_SCENARIO("Upgrade db to v3") IS_DEFINED_BY(testUpgradeToV3)
TEST_SCENARIO("Upgrade db to v4") IS_DEFINED_BY(testUpgradeToV4)
TEST_SCENARIO("Upgrade db to v5") IS_DEFINED_BY(testUpgradeToV5)
TEST_SCENARIO("Upgrade db to v6") IS_DEFINED_BY(testUpgradeToV6)
TEST_SCENARIO("Upgrade db to v7") IS_DEFINED_BY(testUpgradeToV7)

TEST_SCENARIO("On migrate db to v8 table missing 'cascades'' should be added") 
    IS_DEFINED_BY(testOnUpgradeToV8_AddMissingForeignKeysConstraints)
TEST_SCENARIO("On migrate db to v8 local user id should be updated")
    IS_DEFINED_BY(testOnUpgradeToV8_UpdateLocalUserId)

TEST_SCENARIO("Try to upgrade to invalid version") IS_DEFINED_BY(testUpgradeToInvalidVersion)
TEST_SCENARIO("Try upgrade db already upgraded")   IS_DEFINED_BY(testUpgradeAlreadyUpgradedDb)

TEST_SCENARIO("SqliteData should upgrade the database on creation") IS_DEFINED_BY(testSqliteDataUpgrade)

/* **********************************************************************************************/

void DataMigrationFixture::testUpgradeFromV1ToV2(){

GIVEN("a existent database in version1"){
    setupDbV1();

    std::map<string, string> entity = {{"id", "1"}, {"description", "\"Lorem ipsum dolor sit amet\""}};
    checkExistEntity(connector, "Photo", entity);

WHEN("upgrading to version2"){
    migration.migrate(connector, 2);

THEN("an 'Events' table should be created"){
    CHECK(connector.existTable("Events"));

THEN("other data should not be changed"){
    checkExistEntity(connector, "Photo", entity);

THEN("the database user_version should change to 2"){
    CHECK(migration.queryDatabaseVersion(connector) == 2);

}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN

}

void DataMigrationFixture::debugEntities(SqliteConnector connector, int numEntities)
{
    Query query(connector, Table{"PhotoContext"},{});

    for(int i=0; i< numEntities; ++i){
        CHECK(query.read());
        int colsCount = query.getColumnsCount();
        for(int i=0; i<colsCount; ++i){
            string colName = query.getColumnName(i);
            string value = query.getString(i);
            logInfo("%s=%s", colName.c_str(), value.c_str());
        }
    }
}

void DataMigrationFixture::testUpgradeToV3(){
GIVEN("an existent database with a version smaller than 3 and with some PhotoContext instances"){
    setupDbV1();

    vector<string> insertCols;
    vector<vector<string>> insertValues = insertBasicPhotoContexts(connector, &insertCols);

WHEN("upgrading it to version 3"){
    migration.migrate(connector, 3);

THEN("a new 'visibility' column should be added to the PhotoContext Table"){
AND_THEN("the existent contexts should have public visibility"){
    int expectedVisibility = static_cast<int>(Visibility::EditableByOthers);
    for(auto values : insertValues){
        Entity entity;
        for(size_t i=0; i < insertCols.size(); ++i){
            entity[insertCols[i]] = values[i];
        }
        entity["visibility"] = std::to_string(expectedVisibility);
        checkExistEntity(connector, PhotoContextTable_v4, entity);
    }

AND_THEN("a new table should be created to hold permission entities"){
    CHECK(connector.existTable("Permissions"));
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN

}

void DataMigrationFixture::testUpgradeToV4(){
GIVEN("an database on version 3"){
    setupDbV3();
    REQUIRE(migration.queryDatabaseVersion(connector) == 3);

GIVEN("some photos related with some photo collections in contexts"){
    const string selectAllPhotos = Table::getQueryStatement("Photo", {});
    vector<Entity> photos = queryEntity(connector, selectAllPhotos);
    REQUIRE(photos.size() > 0);

WHEN("upgrading it to version 4"){
    migration.migrate(connector, 4);

THEN("the Photo table should contains an foreign key to the context table"){
    vector<Entity> migratedPhotos = queryEntity(connector, selectAllPhotos);
    CHECK(photos.size() == migratedPhotos.size());

    const string ctxIdColumn = "contextId";
    CHECK(migratedPhotos[0].find(ctxIdColumn) != migratedPhotos[0].end());

AND_THEN("each photo in a collection should be related with the collection context"){
    Query existCtx(connector, Table::getQueryStatement(PhotoContextTable_v4, {"id"}, Table::WhereEquals("id", "?")));

    CountReader countReader;
    for(auto p : migratedPhotos){
        const string & contextOfPhoto = p[ctxIdColumn];

        existCtx.appendValue((long)StrCast().tryToLong(contextOfPhoto, -1));
        existCtx.read(countReader);

        CAPTURE(contextOfPhoto);
        CHECK(countReader.getCounter() == 1);

        countReader.reset();
        existCtx.reset();

    }
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void DataMigrationFixture::testUpgradeToV5(){
GIVEN("a database on version 4 with some PhotoMarkers"){
    setupDbV3();
    migration.migrate(connector, 4);

    const std::string photoMarkerTable = "PhotoMarker";

    Table initialTable = schemaManager.readTable(photoMarkerTable);

    auto initialMarkers = this->queryEntity(connector, initialTable.getQueryStatement());
    REQUIRE(initialMarkers.size() > 0);

WHEN("upgrading it to version 5"){
    when_migrating_to_version(5);

THEN("PhotoMarker table will have default timestamp values in createdAt and updatedAt columns"){
    Table changedTable = given_table(photoMarkerTable);
    CAPTURE(changedTable.getCreateStatement());

    CHECK(changedTable.getColumnsList() == initialTable.getColumnsList());
    CHECK(changedTable.getColumn("updatedAt").hasConstraint("DEFAULT"));
    CHECK(changedTable.getColumn("createdAt").hasConstraint("DEFAULT"));

THEN("foreign keys should be preserved"){
    CHECK(changedTable.getForeignKey("photoId").getForeignTable() == "Photo");

THEN("previous values should be preserved also"){
    auto entities = queryEntity(connector, changedTable.getQueryStatement());

    CHECK(entities.size() == initialMarkers.size());
    CHECK(entities == initialMarkers);
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test

void DataMigrationFixture::testUpgradeToV6(){
    GIVEN("a version 5 database"){
        given_a_database_version(5);
        Table userTable = given_table("User");
        
    WHEN("migrating to version 6"){
        when_migrating_to_version(6);
    
    THEN("the user table should contain a new 'name' column"){
        Table changedTable = given_table("User");
        
        CHECK(!userTable.hasColumn("name"));
        CHECK(changedTable.hasColumn("name"));
        
    THEN("the user table should contain a new 'profileImage' column"){
        CHECK(!userTable.hasColumn("profileImage"));
        CHECK(changedTable.hasColumn("profileImage"));
        
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}//test

void DataMigrationFixture::testUpgradeToV7(){
    GIVEN("a version 6 database"){
        given_a_database_version(6);

        bool hasConfigurationsTable = has_table("Configurations");
        REQUIRE_FALSE(hasConfigurationsTable);
        
    WHEN("migrating to version 7"){
        when_migrating_to_version(7);
    
    THEN("a new 'Configurations' table should be created"){
        CHECK(has_table("Configurations"));
        
    }//THEN
    }//WHEN
    }//GIVEN
    
}

void DataMigrationFixture::testOnUpgradeToV8_AddMissingForeignKeysConstraints(){
    GIVEN("a version 7 database"){
        given_a_database_version(7);
        
        Table oldSessionTable = schemaManager.readTable("Session");

    WHEN("migrating to version 8"){
        when_migrating_to_version(8);
    
    THEN("Session table should have a foreign key to user with ON UPDATE and ON DELETE cascades"){
        Table newSessionTable = schemaManager.readTable("Session");
    
        ForeignKey oldUserFK = oldSessionTable.getForeignKey("userId");
        ForeignKey newUserFK = newSessionTable.getForeignKey("userId");
        
        CHECK(oldUserFK.getOnDeleteAction().empty());
        CHECK(oldUserFK.getOnUpdateAction().empty());
        CHECK(newUserFK.getOnDeleteAction() == "CASCADE");
        CHECK(newUserFK.getOnUpdateAction() == "CASCADE");
    }//THEN
    }//WHEN
    }//GIVEN
}//test

void DataMigrationFixture::testOnUpgradeToV8_UpdateLocalUserId(){
    GIVEN("a version 7 database"){
        given_a_database_version(7);
        
    GIVEN("there is an user with old local user id: '0'"){
        const char * oldLocalUserId = "0";
        
        Entity userEntity = given_an_existent_user_instance_in_a_v7_db(oldLocalUserId);
    
    GIVEN("some related entities"){
        map<std::string, Entity> relatedInstances = given_some_user_related_instances(oldLocalUserId);    
        
    WHEN("migrating to version 8"){
        when_migrating_to_version(8);
    
    THEN("that user should be updated to new local user id"){
        string newId = "-1";
        
        then_all_entities_should_have_been_updated(
            {{"User", userEntity}}, {{"id", newId}});
        
    AND_THEN("all related entities should be updated also"){
        then_all_entities_should_have_been_updated(relatedInstances, {{"userId", newId}});
        
    }//THEN    
    }//THEN
    }//WHEN
    }//GIVEN
    }//GIVEN
    }//GIVEN
}//test

void DataMigrationFixture::testUpgradeToInvalidVersion(){

GIVEN("an database on some version"){
    setupDbV1();

WHEN("trying to migrate to an negative or inexistent version"){
THEN("an exception should be thrown"){
    checkInvalidMigration(connector, 0);
    checkInvalidMigration(connector, -1);
    checkInvalidMigration(connector, 30);
}//THEN
}//WHEN
}//GIVEN


}

void DataMigrationFixture::testUpgradeAlreadyUpgradedDb(){

GIVEN("an database already upgraded to some version"){
    setupDbV1();

    migration.migrate(connector, 2);

WHEN("trying to upgrade to the same version"){
THEN("it should not change anything"){
    CHECK_NOTHROW(migration.migrate(connector, 2));
}//THEN
}//WHEN
}//GIVEN

}

void DataMigrationFixture::testSqliteDataUpgrade(){

WHEN("creating an SqliteData instance with an not updated database"){
    setupDbV1();

    REQUIRE(migration.queryDatabaseVersion(connector) == 1);

    SqliteData data(connector);

THEN("it should upgrade the database to current version"){
    CHECK(migration.queryDatabaseVersion(data.getDatabaseConnector()) == DatabaseMigration::MAX_VERSION);

}//WHEN
}//GIVEN

}//test


/* *************************************** steps ************************************************/

void DataMigrationFixture::given_a_database_version(int version){
    if(version >= 3){
        setupDbV3();
    }
    else{
        setupDbV1();
    }
    migration.migrate(connector, version);
}


Entity DataMigrationFixture::given_an_existent_user_instance_in_a_v7_db(const std::string & userId){
    Entity userEntity = {
        {"id", userId},
        {"login", "'anylogin'"},
        {"passHash", "'anyhash'"},
        {"salt", "'anysalt'"}
    };
    
    insertEntity(connector, "User", userEntity);

    return userEntity;
}

map<string, Entity> DataMigrationFixture::given_some_user_related_instances(const std::string & userId)
{
    Entity contextEntity = {
        {"id", "100"},
        {"name", "'ctx'"}
    };
    
    insertEntity(connector, "PhotoContext", contextEntity);
    
    
    Entity sessionEntity = {
        {"userId", userId},
        {"token", "'sample'"},
        {"expireTimestamp", "0"}
    };
    
    Entity eventsEntity = {
        {"userId", userId},
        {"entityId", "-1"},
        {"type", "0"}
    };
    Entity permissionEntity = {
        {"contextId", "100"},
        {"permissionLevel", "1"},
        {"userId", userId}
    };
      
    map<std::string, Entity> relatedInstances = {
        {"Session", sessionEntity}, 
        {"Events", eventsEntity}, 
        {"Permissions", permissionEntity}
    };
    
    for(const auto & p : relatedInstances){
        insertEntity(connector, p.first, p.second);
    }
    
    return relatedInstances;
}

void DataMigrationFixture::when_migrating_to_version(int version){
    migration.migrate(connector, version);
}


void DataMigrationFixture::printEntities(const std::string& table)
{
    auto entities = queryEntity(connector, "select * from " + table);
    
    cout << entities.size() << " entities in table " << table << endl;
    for(const auto & e : entities){
        std::cout << entityToString(e) << endl;
    }    
}

void DataMigrationFixture::then_all_entities_should_have_been_updated(
    map<std::string, Entity> relatedInstances, 
    const std::map<std::string, std::string> & valuesToUpdate)
{
    for(auto & tableEntity : relatedInstances){
        const auto & table = tableEntity.first;
        auto & entity = tableEntity.second;
        
        for (const auto & p : valuesToUpdate) {
            entity[p.first] = p.second;
        }
        
        if(!checkExistEntity(connector, table, entity)){
            printEntities(table);
        }
    }    
}

bool DataMigrationFixture::has_table(const string & tableName){
    return schemaManager.existTable(tableName);
}

string DataMigrationFixture::entityToString(const Entity & e){
    stringstream out;
    out << "{";
    
    for(const auto & p : e){
        out << p.first << ": " << p.second << ", ";
    }
    
    out << "}";
    
    return out.str();
}

/* **********************************************************************************************/


DataMigrationFixture::DataMigrationFixture()
    : connector(std::move(SqliteConnector::createInMemoryDb()))
    , schemaManager(&connector)
{}

bool DataMigrationFixture::checkExistEntity(SqliteConnector & connector, const std::string & table
    , const Entity & entity)
{
    stringstream whereCond;

    bool first = true;
    for(auto p : entity){
        if(!first){
            whereCond << " AND ";
        }
        else{
            first = false;
        }

        whereCond << Table::WhereEquals(p.first, p.second);
    }

    string statement = Table::getQueryStatement(table, {}, QueryArgs{whereCond.str()});
    CAPTURE(statement);

    CountReader countReader;
    connector.query(countReader, statement);

    CHECK(countReader.getCounter() >= 1);
    
    return countReader.getCounter() >= 1;
}

std::set<long> DataMigrationFixture::insertEntity(SqliteConnector & connector, const string & table
    , const Entity & entity){
    
    vector<string> columns, values;
    
    for(auto p : entity){
        columns.push_back(p.first);
        values.push_back(p.second);
    }
    
    return insertEntities(connector, table, columns, {values});
}
std::set<long> DataMigrationFixture::insertEntities(SqliteConnector & connector, const string & table
    , const vector<string> & columns, const vector<vector<string>> & valuesList)
{
    ostringstream insertStmt;
    insertStmt  << "INSERT INTO " << table;
    Table::listColumns(insertStmt, columns);
    insertStmt << " VALUES ";

    std::set<long> ids;

    for(auto values: valuesList){
        ostringstream valuesStream;
        Table::listColumns(valuesStream, values);

        auto stmt = Statement(connector, insertStmt.str() + valuesStream.str());

        long id = connector.insert(stmt);
        ids.insert(id);
    }

    return ids;
}

void DataMigrationFixture::setupDbV1(){
    importDb("calmphotoframe.v1.sql");
}

void DataMigrationFixture::setupDbV3(){
    importDb("calmphotoframe.v3.sql");
}

void DataMigrationFixture::importDb(const char * dbName){
    string dbPath = getTestDb(dbName);

    //Cannot use transaction, because file alread have (or should have) a transaction
//    ScopedTransaction transaction(&connector);

    executeSqlFile(dbPath);

//    transaction.commit();
}

void DataMigrationFixture::executeSqlFile(const string & dbPath){
    std::ifstream dbfile(dbPath);
    string statement;
    while(std::getline(dbfile, statement, ';')){
        connector.execute(statement);
    }
}

Table DataMigrationFixture::given_table(const string & tableName){
    return schemaManager.readTable(tableName);
}

string DataMigrationFixture::getTestDb(const char * dbName){
    return Files().joinPaths(Constants::testDbsDir, dbName);
}

void DataMigrationFixture::checkInvalidMigration(SqliteConnector & connector, int targetVersion)
{
    ScopedTransaction transaction(&connector);
    CAPTURE(targetVersion);
    CHECK_THROWS_AS(migration.migrate(connector, targetVersion), InvalidMigrationVersion);

    transaction.rollback();
}

vector<vector<string> > DataMigrationFixture::insertBasicPhotoContexts(SqliteConnector & connector, vector<string> * outInsertCols)
{
    vector<string> insertCols = {"name"};

    vector<vector<string>> insertValues = { {R"("some context")"}, {R"("other context")"}};
    insertEntities(connector, PhotoContextTable_v4, insertCols, insertValues);

    if(outInsertCols != NULL){
        *outInsertCols = std::move(insertCols);
    }

    return insertValues;
}


vector<Entity> DataMigrationFixture::queryEntity(SqliteConnector &connector, const string & statement){
    EntityReader reader;
    connector.query(reader, statement);

    return reader.entities;
}
