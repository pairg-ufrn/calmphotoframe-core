#include "catch.hpp"

#include "data/Data.h"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"
#include "utils/Log.h"

#include "Poco/Thread.h"
#include "Poco/Runnable.h"

using namespace calmframe;
using namespace calmframe::data;
using namespace Poco;

class GetDataPointer : public Runnable{
private:
    volatile Data * outDataPtr;
public:
    GetDataPointer()
        : outDataPtr(NULL)
    {}

    void run(){
        this->outDataPtr = &Data::instance();
    }

    volatile Data * getDataPtr(){
        return this->outDataPtr;
    }
};

SCENARIO("Thread-safe access to database", "[Data][data]"){

GIVEN("that data is configured with some data builder"){
    Data::setup(std::make_shared<MemoryDataBuilder>());

WHEN("acessing Data::instance from another Poco thread"){
    GetDataPointer runnable;

    /*Create local Data::instance before launch the other thread to force Data pointers be 
        created on different memory areas. Otherwise, the other Data instance could be freed
        before the local Data be created, and the same memory area could be used, causing the test to fail. 
    */
    Data * localDataPtr = &Data::instance();

    Thread thread;
    thread.start(runnable);
    thread.join();
THEN("it should return a different Data::instance"){
    CHECK(runnable.getDataPtr() != NULL);
    CHECK(runnable.getDataPtr() != localDataPtr);
}
}//WHEN
}//GIVEN

}//SCENARIO


void * thread_GetDataPointer(void * data){
    Data ** dataPtr = (Data **)data;
    *dataPtr =  &Data::instance();

    return NULL;
}

SCENARIO("Thread-safe access to database (pthread)", "[Data][data][pthread]"){

GIVEN("that data is configured with some data builder"){
    Data::setup(std::make_shared<MemoryDataBuilder>());

WHEN("acessing Data::instance from another pthread"){
    pthread_t thread;
    Data * dataPtr = NULL;
    
    /*Create local Data::instance before launch the other thread to force Data pointers be 
        created on different memory areas. Otherwise, the other Data instance could be freed
        before the local Data be created, and the same memory area could be used, causing the test to fail. 
    */
    void * localDataPtr = (void *)&Data::instance();

    int pthreadRet = pthread_create( &thread, NULL, thread_GetDataPointer, &dataPtr);

    pthread_join( thread, NULL);
THEN("it should return a different Data::instance from the local thread"){
    CHECK(dataPtr != NULL);
    CHECK((void *)dataPtr != localDataPtr);
}
}//WHEN
}//GIVEN

}//SCENARIO

TEST_CASE("DataBuilder::clone should produce a diferent instance", "[Data]"){
    MemoryDataBuilder builder;
    DataBuilder * clone = builder.clone();
    CHECK(&builder != clone);

    DataBuilder * clone2 = clone->clone();

    CHECK(clone2 != NULL);
    CHECK(clone2 != clone);

    delete clone;
}

SCENARIO("Change DataBuilder", "[Data][data][pthread]"){

GIVEN("that data is configured with some data builder"){
    Data::setup(std::make_shared<MemoryDataBuilder>());

WHEN("acessing Data::instance"){
    Data * dataPtr = &Data::instance();
THEN("it should have built that instance"){
    CHECK(dataPtr != NULL);
    WHEN("change the data builder"){
        Data::setup(std::make_shared<MemoryDataBuilder>());
    AND_WHEN("acessing Data::instance again"){
        Data * dataPtr2 = &Data::instance();
    THEN("it should build another Data instance"){
        CHECK(dataPtr2 != NULL);
        CHECK(dataPtr != dataPtr2);
    }
    }
    }
}
}//WHEN
}//GIVEN

}//SCENARIO


SCENARIO("Data instance should persist between data calls","[Data]"){
GIVEN("the Data singleton setup with some data builder"){
    Data::setup(std::make_shared<MemoryDataBuilder>());
WHEN("calling Data::instance"){
    Data * dataInstance = &Data::instance();
THEN("it should return a data instance"){
    CHECK(dataInstance != NULL);
AND_WHEN("calling Data::instance again"){
    Data * otherData = &Data::instance();
THEN("the same data instance should be returned"){
    CHECK(dataInstance == otherData);
}
}
}
}
}
}//SCENARIO
