#include "TestMacros.h"

#include "utils/ScopedFile.h"
#include "helpers/PhotoHelper.h"
#include "helpers/PhotoContextHelper.h"

#include "data/data-sqlite/dao/SqlitePhotoDAO.h"
#include "data/data-sqlite/SqliteData.h"
#include "utils/FileUtils.h"

#include "utils/ScopedDatabase.h"

#include <algorithm>

using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

using namespace std;

#define TEST_TAG "[PhotoDAO][Database]"
#define FIXTURE PhotoDAOFixture

class PhotoDAOFixture{
public:
    void testListAll();
    void testListIn();
    void testFilterPhotosBasedOnQuery();
    void testFilterPhotosBasedOnQueryAndIds();
    void testListByQueryString_WithPhotoMarkers();
    void testQueryStringSqlInjection();
    void testPhotoCrud();
    void testCreatePhotoAtContext();

public:
    PhotoDAOFixture();

    PhotoDAO & getPhotoDAO();

    void updatePhoto(Photo & photo, PhotoDAO & photoDAO);

    void assertPhotoEqualsOnDatabase(const Photo & photo);

    string givenPhotosWithQuery(vector<Photo> & outExpectedPhotos);
    string givenPhotosWithQuery(vector<Photo> & outExpectedPhotos, vector<Photo> & outOtherPhotos);
    void checkPhotos(vector<Photo> & photos, vector<Photo> & expectedPhotos);
    void extractIds(const vector<Photo> & photos, set<long> & outPhotoIds);

private:
    ScopedSqliteDatabase scopedDb;
};

void testListPhotosFromList();

FIXTURE_SCENARIO("PhotoDAO should allow list all photos on database"){
    testListAll();
}//SCENARIO

FIXTURE_SCENARIO("PhotoDAO should allow list photos from a set of given ids"){
    testListIn();
}//SCENARIO

FIXTURE_SCENARIO("Photo CRUD"){
    testPhotoCrud();
}//SCENARIO

SCENARIO("List photos from photo id list", TEST_TAG "[SqlitePhotoDAO]"){
    testListPhotosFromList();
}//SCENARIO

FIXTURE_SCENARIO("PhotoDAO should allow filter photos with a given search string"){
    testFilterPhotosBasedOnQuery();
}//SCENARIO

FIXTURE_SCENARIO("PhotoDAO should allow filter photos with a given search string and a list of ids"){
    testFilterPhotosBasedOnQueryAndIds();
}//SCENARIO

FIXTURE_SCENARIO("PhotoDAO search by photos should include photo markers info"){
    testListByQueryString_WithPhotoMarkers();
}//SCENARIO

FIXTURE_SCENARIO("PhotoDAO should not allow sql injection through query string"){
    testQueryStringSqlInjection();
}//SCENARIO

FIXTURE_SCENARIO("Photo can be associated with a context"){
    this->testCreatePhotoAtContext();
}//SCENARIO

/* ******************************************************************************************/


void PhotoDAOFixture::testListAll(){
GIVEN("some photos on database"){
    const int numPhotos = 5;
    set<long> photoIds = PhotoHelper::insertPhotos(numPhotos);

WHEN("list all photos using a PhotoDAO instance"){
    vector<Photo> photos = getPhotoDAO().listAll();

THEN("it should return all photos on database"){
    CHECK(photos.size() == numPhotos);
    for(auto photo : photos){
        CHECK(photoIds.find(photo.getId()) != photoIds.end());
    }
}//THEN
}//WHEN
}//GIVEN

}

void PhotoDAOFixture::testListIn(){

GIVEN("some photos on db"){
GIVEN("a subset of those photoIds"){

    set<long> photosToFilter = PhotoHelper::insertPhotos(2);
    PhotoHelper::insertPhotos(3);
    auto morePhotoIds = PhotoHelper::insertPhotos(2);
    for(auto id : morePhotoIds){
        photosToFilter.insert(id);
    }

WHEN("listing the photos with that ids"){
    PhotoDAO::PhotoList photos = getPhotoDAO().listIn(photosToFilter);

THEN("only those photos should be retrieved from db"){
    CHECK(photos.size() == photosToFilter.size());

    set<long> photoIds;
    extractIds(photos, photoIds);

    CHECK(photosToFilter == photoIds);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void PhotoDAOFixture::testPhotoCrud(){

GIVEN("an PhotoDAO instance"){
    PhotoDAO & photoDAO = getPhotoDAO();

WHEN("inserting some photo"){
    Photo photo = PhotoHelper::createPhoto();
    long createdId = photoDAO.insert(photo);
    photo.setId(createdId);

THEN("the photo should exist on database"){
    CHECK(photoDAO.exist(createdId));
    assertPhotoEqualsOnDatabase(photo);

AND_WHEN("updating the photo"){
    updatePhoto(photo, photoDAO);

THEN("the database should keep the changes"){
    assertPhotoEqualsOnDatabase(photo);

AND_WHEN("removing the photo"){
    CHECK(photoDAO.remove(photo));
THEN("the photo should no more exist on database"){
    CHECK_FALSE(photoDAO.exist(createdId));
}//THEN
}//WHEN
}//THEN
}//WHEN
}//THEN
}//WHEN
}//GIVEN

}

void testListPhotosFromList(){

GIVEN("some photos on database"){
    PhotoDAOFixture fixture;
    PhotoHelper::insertPhotos(5);
    vector<Photo> photos = Data::instance().photos().listAll();
    REQUIRE(!photos.empty());
GIVEN("a collection of some photo ids"){
    set<long> photoIds;
    for(size_t i=0; i < photos.size(); ++i){
        if(i%2 != 0){
            photoIds.insert(photos[i].getId());
        }
    }
    REQUIRE(!photoIds.empty());
WHEN("calling listIn on the PhotoDAO, passing that ids"){
    vector<Photo> filteredPhotos = Data::instance().photos().listIn(photoIds);
THEN("it should return the collection with all photos that contain one of that ids"){
    CHECK(filteredPhotos.size() == photoIds.size());
    for(size_t i=0; i < filteredPhotos.size(); ++i){
        CHECK(photoIds.find(filteredPhotos[i].getId()) != photoIds.end());
    }
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void PhotoDAOFixture::testFilterPhotosBasedOnQuery(){

GIVEN("some photos on database"){
    vector<Photo> expectedPhotos;
    string queryString = givenPhotosWithQuery(expectedPhotos);

WHEN("listing those photos using some query string as filter"){
    vector<Photo> photos = getPhotoDAO().listAll(queryString);

THEN("only the photos that contains that string on some field should be returned"){
    checkPhotos(photos, expectedPhotos);
}//THEN
}//WHEN
}//GIVEN

}

void PhotoDAOFixture::testFilterPhotosBasedOnQueryAndIds(){

GIVEN("some photos on database"){
    vector<Photo> photosWithQuery, otherPhotos;
    string queryString = givenPhotosWithQuery(photosWithQuery, otherPhotos);

    set<long> photoIds;

    extractIds(photosWithQuery, photoIds);
    extractIds(otherPhotos, photoIds);

    //remove some ids
    photoIds.erase(photoIds.begin());
    photoIds.erase(otherPhotos.begin()->getId());

    vector<Photo> expectedPhotos(photosWithQuery.begin() + 1, photosWithQuery.end());

WHEN("listing those photos using some query string and a list of photo ids as filters"){
    vector<Photo> photos = getPhotoDAO().listIn(photoIds, queryString);

THEN("only the photos that contains that string on some field and withing the specified ids should be returned"){
    checkPhotos(photos, expectedPhotos);
}//THEN
}//WHEN
}//GIVEN

}

void PhotoDAOFixture::testListByQueryString_WithPhotoMarkers(){
GIVEN("some photos with markers"){
    string queryString = "consectetur";

    vector<Photo> expectedResult{
        Photo(PhotoDescription().setMarkers({
            PhotoMarker("Lorem ipsum dolor"), PhotoMarker("consectetur adipiscing")
        })),
        Photo(PhotoDescription().setMarkers({
            PhotoMarker("Sed consectetur ante"), PhotoMarker("non mollis iaculis")
        })),
        Photo(PhotoDescription{"Aenean nibh velit", "dolor sit amet, consectetur, adipisci velit"})
    };
    PhotoHelper::insertAll(expectedResult);
    //insert others
    PhotoHelper::insertPhotos(3);

WHEN("using a PhotoDAO to search those photos based on a query string"){
    auto result = getPhotoDAO().listAll(queryString);

THEN("photos whose markers contains the query string should be included on result"){
    checkPhotos(result, expectedResult);

}//THEN
}//WHEN
}//GIVEN

}

void PhotoDAOFixture::testQueryStringSqlInjection(){
    PhotoHelper::insertPhotos();

GIVEN("some query string with an sql content"){
    string badQueryString = "' ||; SELECT error; --";

WHEN("trying to list photos with that query string"){
    PhotoDAO::PhotoList photos = getPhotoDAO().listAll(badQueryString);

THEN("it should be treated as data and not throw errors"){
    CHECK(photos.empty());

}//THEN
}//WHEN
}//GIVEN

}//test


void PhotoDAOFixture::testCreatePhotoAtContext(){

GIVEN("some PhotoContext"){
    PhotoContext ctx = PhotoContextHelper::insertContext();

WHEN("inserting a photo with that context id"){
    Photo photo{PhotoDescription{"some photo", "on some moment"}, PhotoMetadata{}, ctx.getId()};
    long photoId = getPhotoDAO().insert(photo);

THEN("the context id should be stored on db"){
    Photo dbPhoto;
    CHECK(getPhotoDAO().getPhoto(photoId, dbPhoto));
    CHECK(dbPhoto.getContextId() == photo.getContextId());

}//THEN
}//WHEN
}//GIVEN

}//test

/* ********************************************************************************************/

PhotoDAOFixture::PhotoDAOFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
{}

PhotoDAO &PhotoDAOFixture::getPhotoDAO(){
    return Data::instance().photos();
}

void PhotoDAOFixture::updatePhoto(Photo & photo, PhotoDAO & photoDAO){
    photo.getDescription().setCredits(photo.getDescription().getCredits() + string("_change"));
    photo.getDescription().setDescription(photo.getDescription().getDescription() + "_change");

    CHECK(photoDAO.update(photo));
}

void PhotoDAOFixture::assertPhotoEqualsOnDatabase(const Photo & photo){
    Photo photoFromDb;
    CHECK(getPhotoDAO().getPhoto(photo.getId(), photoFromDb));
    PhotoHelper::assertEquals(photo, photoFromDb);
}

string PhotoDAOFixture::givenPhotosWithQuery(vector<Photo> & outExpectedPhotos){
    vector<Photo> outOtherPhotos;
    return givenPhotosWithQuery(outExpectedPhotos, outOtherPhotos);
}

string PhotoDAOFixture::givenPhotosWithQuery(vector<Photo> & expectedPhotos, vector<Photo> & otherPhotos){

    const string queryString = "lorem ipsum";
    const string queryStringCase = "lOrem IpSUm";

    expectedPhotos = {
        PhotoDescription{queryString + " description"}
      , PhotoDescription{"", "middle " + queryString + " moment"}
      , PhotoDescription{"", "", "", "place:" + queryString}
      , PhotoDescription{"multiple", queryString, queryString}
      , PhotoDescription{queryStringCase}};

    const string almostQuery = queryString.substr(0, queryString.size()/2);
    otherPhotos = {
        PhotoDescription{}
      , PhotoDescription{"", almostQuery}
      , PhotoDescription{"", almostQuery, queryString.substr(almostQuery.size())}};

    PhotoHelper::insertAll(expectedPhotos);
    PhotoHelper::insertAll(otherPhotos);

    return queryString;
}

void PhotoDAOFixture::checkPhotos(vector<Photo> & photos, vector<Photo> & expectedPhotos)
{
    std::sort(photos.begin(), photos.end());
    std::sort(expectedPhotos.begin(), expectedPhotos.end());

    CHECK(photos.size() == expectedPhotos.size());
    CHECK(photos == expectedPhotos);
}

void PhotoDAOFixture::extractIds(const vector<Photo> & photos, set<long> & photoIds)
{
    for(auto photo: photos){
        photoIds.insert(photo.getId());
    }
}
