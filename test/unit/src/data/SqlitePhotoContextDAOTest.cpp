#include "TestMacros.h"

#include "model/photo/PhotoContext.h"
#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "data/data-sqlite/SqliteConnector.h"

#include "utils/ScopedDatabase.h"
#include "helpers/PhotoContextHelper.h"
#include "helpers/PhotoCollectionHelper.h"

#include <string>
#include <sstream>

using namespace calmframe;
using namespace calmframe::data;
using namespace std;

namespace Test {
namespace Constants {
    static const char * TABLE_NAME = "PhotoContext";
    static const char * CONTEXT_NAME = "AnyName";
    static const int MIN_VALID_ID = 1;
    static const int CTX_COUNT = 5;
}//namespace
}//namespace

class PhotoContextDAOFixture{
public:
    void testCreatePhotoContextTable();
    void testInsertPhotoContext();
    void testListPhotoContexts();
    void testCountContextRows();
    void testFilterByVisibility();
public:
    PhotoContextDAO & getDAO();

    PhotoContext createPhotoContext(const string & name);

    void createContexts(vector<PhotoContext> & contexts, int ctxCount);
    void insertContexts(vector<PhotoContext> & contexts);

    void checkEquals(const PhotoContext & ctx1 , const PhotoContext & ctx2);

    void checkCollectionContainsContexts(const std::vector<PhotoContext> & collection
                                       , const std::vector<PhotoContext> & containedCtxs);
};

#define TEST_TAG "[SqlitePhotoContextDAO][PhotoContextDAO]"
#define FIXTURE PhotoContextDAOFixture

/* *********************************************************************************************/

FIXTURE_SCENARIO("create photo context table on database"){
    this->testCreatePhotoContextTable();
}//SCENARIO

FIXTURE_SCENARIO("put PhotoContext on database"){
    this->testInsertPhotoContext();
}

FIXTURE_SCENARIO("list created photo contexts"){
    this->testListPhotoContexts();
}

FIXTURE_SCENARIO("PhotoContextDAO should be able of return the count of rows on database"){
    this->testCountContextRows();
}//SCENARIO

FIXTURE_SCENARIO("PhotoContextDAO allows filter contexts by visibility level"){
    this->testFilterByVisibility();
}//SCENARIO

/* *********************************************************************************************/

void PhotoContextDAOFixture::testCreatePhotoContextTable(){

GIVEN("an SqliteDataBuilder instance"){
WHEN("building database"){
    ScopedSqliteDatabase scopedDatabase;
    SqliteData & data = (SqliteData &)Data::instance();
THEN("PhotoContext's table should exist on database"){
    SqliteConnector & dbConnector = data.getDatabaseConnector();
    CHECK(dbConnector.existTable(Test::Constants::TABLE_NAME));
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextDAOFixture::testInsertPhotoContext(){
GIVEN("a PhotoContextDAO instance"){
    ScopedSqliteDatabase scopedDb;

GIVEN("some PhotoContext instance"){
    PhotoContext ctx = createPhotoContext(Test::Constants::CONTEXT_NAME);
    long rootCollectionId = PhotoCollectionHelper::insertCollection();
    ctx.setRootCollection(rootCollectionId);
    ctx.setVisibility(Visibility::EditableByOthers);

WHEN("inserting that instance on database"){
    long createdId = Data::instance().contexts().insert(ctx);

THEN("it should return a new instance Id"){
    CHECK(createdId >= Test::Constants::MIN_VALID_ID);

THEN("that instance id should exist on database"){
    CHECK(Data::instance().contexts().exist(createdId));

AND_WHEN("get the instance with that id"){
    PhotoContext savedCtx = Data::instance().contexts().get(createdId);

THEN("it should be equals to the created PhotoContext"){
    checkEquals(ctx, savedCtx);
}//THEN
}//WHEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoContextDAOFixture::testListPhotoContexts(){
GIVEN("some PhotoContextDAO instance"){
    ScopedSqliteDatabase scopedDb;
WHEN("inserting a collection of PhotoContexts"){
    vector<PhotoContext> createdContexts;
    createContexts(createdContexts, Test::Constants::CTX_COUNT);
    insertContexts(createdContexts);

WHEN("listing the collection of Contexts on db"){
    vector<PhotoContext> dbContexts = Data::instance().contexts().listAll();

THEN("the returned collection should contains all the created PhotoContexts"){
    checkCollectionContainsContexts(dbContexts, createdContexts);

}//THEN
}//WHEN
}//WHEN
}//GIVEN

}

void PhotoContextDAOFixture::testCountContextRows(){
GIVEN("some database with some rows"){
    ScopedSqliteDatabase scopedDatabase;

    int numberOfCtxs = 4;
    vector<PhotoContext> ctxs;
    PhotoContextHelper::insertContexts(numberOfCtxs, ctxs);
    REQUIRE(ctxs.size() == numberOfCtxs);

WHEN("calling 'count' on a PhotoContextDAO"){
    int foundNumberOfContexts = Data::instance().contexts().count();

THEN("it should return the number of PhotoContexts on database"){
    REQUIRE(foundNumberOfContexts == numberOfCtxs);

}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextDAOFixture::testFilterByVisibility(){
GIVEN("some contexts with different visibility levels"){
    const Visibility filterVisibility = Visibility::VisibleByOthers;

    vector<PhotoContext> expectedContexts =
        {{"some context", Visibility::EditableByOthers}, {"other", Visibility::VisibleByOthers}};
    PhotoContextHelper::insertContexts(expectedContexts);
    //others
    PhotoContextHelper::insertContexts({{"blah", Visibility::Unknown}, {"a", Visibility::Private}});

WHEN("using a Context DAO to filter contexts with a given visibility"){
    auto filteredContexts = Data::instance().contexts().list(filterVisibility);

THEN("all contexts with that visibility or greater should be returned"){
    PhotoContextHelper::checkEquals(expectedContexts, filteredContexts);
}//THEN
}//WHEN
}//GIVEN

}//test


/* *********************************************************************************************/

PhotoContextDAO &PhotoContextDAOFixture::getDAO(){
    return Data::instance().contexts();
}

PhotoContext PhotoContextDAOFixture::createPhotoContext(const string & name){
    PhotoContext ctx;
    ctx.setName(name);
    return ctx;
}

void PhotoContextDAOFixture::createContexts(vector<PhotoContext> & contexts, int ctxCount){
    PhotoContextHelper::createContexts(ctxCount, contexts);
}

void PhotoContextDAOFixture::insertContexts(vector<PhotoContext> & contexts){
    PhotoContextHelper::insertContexts(contexts);
}

void PhotoContextDAOFixture::checkEquals(const PhotoContext & ctx1, const PhotoContext & ctx2){
    CHECK(ctx1.getName()            == ctx2.getName());
    CHECK(ctx1.getRootCollection()  == ctx2.getRootCollection());
    CHECK(ctx1.getVisibility()      == ctx2.getVisibility());
}

void PhotoContextDAOFixture::checkCollectionContainsContexts(const std::vector<PhotoContext> & collection, const std::vector<PhotoContext> & containedCtxs)
{
    PhotoContextHelper::checkCollectionContainsContexts(collection, containedCtxs);
}
