#include "TestMacros.h"

#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/dao/SqliteUserDAO.h"

#include "model/user/UserAccount.h"
#include "services/SessionManager.h"
#include "services/AccountManager.h"

#include "utils/FileUtils.h"
#include "utils/TimeUtils.h"
#include "utils/EncodingUtils.h"
#include "utils/StringCast.h"
#include "utils/ScopedDatabase.h"
#include "utils/Log.h"

#include <string>
#include <sstream>

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

namespace calmframe {

namespace Constants{
    static const char * UserName     = "anyname";
    static const char * UserPassword = "password";
    static const char * otherUserName     = "othername";
    static const char * PasswordHash = "ee11cbb19052e40b07aac0ca060c23ee";
    static const char * PasswordSalt = "";
    static const char * newSessionToken = "someRandomToken";
    static const unsigned expiredSessionsCount = 10;
    static const unsigned decreaseTime = 10;
}
}

#define TEST_TAG "[SqliteSessionDAO][SessionDAO]"
#define FIXTURE SqliteSessionDAOFixture

class SqliteSessionDAOFixture{
private:
    bool hasCreatedUser;
public:
    ScopedSqliteDatabase scopedDb;
    string imagePath;

    UserAccount createdUser;
    string userPassword;
    AccountManager accountManager;

    SqliteSessionDAOFixture();

    void createUserOnDb();

    Session createSession();

    int createValidSessions();
    int createExpiredSessions();

    string generateToken(const int number);
    long createExpiredTimestamp();
};


/* *****************************************************************************************************/

SCENARIO("Insert Session instance", "[SessionDAO],[database]"){
GIVEN("a database instance"){
    SqliteSessionDAOFixture fixture;

GIVEN("some userAccount on db"){
    UserAccount userAccount;
    userAccount.setLogin(Constants::UserName);
    long userId = Data::instance().users().insert(userAccount);

WHEN("inserting some session on database for that userAccount"){
    Session newSession;
    newSession.setToken(Constants::newSessionToken);
    newSession.setUserId(userId);

    long sessionId = Data::instance().sessions().insert(newSession);

THEN("it should be stored on database"){
    CHECK(Data::instance().sessions().exist(sessionId));
    Session dbSession;
    CHECK(Data::instance().sessions().get(sessionId, dbSession) == true);
    CHECK(dbSession.getToken() == newSession.getToken());
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//SCENARIO

SCENARIO("Create userAccount", "[UserDAO][database]"){

GIVEN("a new database instance"){
    SqliteSessionDAOFixture fixture;
THEN("should not exist any userAccount on database"){
    UserAccount someUser;
    CHECK(Data::instance().users().getByLogin(Constants::UserName, someUser) == false);
}
}//GIVEN

}//SCENARIO

SCENARIO("Manage UserAccount session", "[UserDAO][SessionDAO][database]"){

GIVEN("a database instance"){
    SqliteSessionDAOFixture fixture;
GIVEN("that exist an userAccount on database"){
    CHECK(Data::instance().users().exist(Constants::UserName) == false);
    fixture.createUserOnDb();
    CHECK(Data::instance().users().exist(fixture.createdUser.getId()));
WHEN("Starting a session using that userAccount's login and password"){
    Session session = SessionManager::instance().startSession(fixture.createdUser,fixture.userPassword);
THEN("should exist an session instance on database for that userAccount"){
    INFO("Session id: " << session.getId());
    CHECK(Data::instance().sessions().exist(session.getId()));
    Session tmpSession;
    CHECK(Data::instance().sessions().getUserSession(fixture.createdUser.getId(),tmpSession) == true);
AND_WHEN("getting that session from database"){
    Session sessionFromDb;
    Data::instance().sessions().get(session.getId(), sessionFromDb);
THEN("it should be equals to the created session"){
    CHECK(sessionFromDb.getId()     == session.getId());
    CHECK(sessionFromDb.getToken()  == session.getToken());
    CHECK(sessionFromDb.getUserId() == session.getUserId());
    CHECK(sessionFromDb.getExpireTimestamp() == session.getExpireTimestamp());
AND_WHEN("starting another session to the same userAccount"){
    Session otherSession = SessionManager::instance().startSession(fixture.createdUser,fixture.userPassword);
THEN("it should create other session with a different token"){
    CHECK(otherSession.getId() != session.getId());
    CHECK(otherSession.getToken() != session.getToken());
    CHECK(otherSession.getUserId() == session.getUserId());
AND_WHEN("listing all sessions"){
    vector<Session> sessions = Data::instance().sessions().listAll();
THEN("it should return the two created sessions"){
    CHECK(sessions.size() == 2);
    if(sessions.size() >= 2){
        if(sessions[0].getId() == session.getId()){
            CHECK(sessions[0] == session);
            CHECK(sessions[1] == otherSession);
        }
        else{
            CHECK(sessions[0] == otherSession);
            CHECK(sessions[1] == session);
        }
    }
AND_WHEN("finalizing the first session"){
    SessionManager::instance().finishSession(session);
THEN("the database should not contain that session instance anymore"){
    CHECK(!Data::instance().sessions().exist(session.getId()));
}//THEN
}//AND_WHEN
}//THEN
}//AND_WHEN
}//THEN
}//AND_WHEN
}
}
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO


SCENARIO("Update session timestamp", "[UserDAO][SessionDAO][database]"){

GIVEN("a database instance"){
    SqliteSessionDAOFixture fixture;
GIVEN("a session instance on database"){
    Session existentSession = fixture.createSession();
    CHECK(Data::instance().sessions().exist(existentSession.getId()));
WHEN("updating that session with a new expire timestamp"){
    long newExpireTimestamp = existentSession.getExpireTimestamp() + 10;
    existentSession.setExpireTimestamp(newExpireTimestamp);
    CHECK_NOTHROW(Data::instance().sessions().update(existentSession));
THEN("it should change that session expire timestamp on database"){
    Session updatedSession;
    Data::instance().sessions().get(existentSession.getId(), updatedSession);
    CHECK(updatedSession.getExpireTimestamp() == newExpireTimestamp);
AND_THEN("it should not change other parameters"){
    CHECK(updatedSession.getToken() == existentSession.getToken());
    CHECK(updatedSession.getUserId() == existentSession.getUserId());
}
}
}
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Remove all expired sessions", "[UserDAO][SessionDAO][database]"){

GIVEN("a database instance"){
    SqliteSessionDAOFixture fixture;
GIVEN("some sessions on database"){
    int validSessionsCount = fixture.createValidSessions();
    int expiredSessionsCount = fixture.createExpiredSessions();
    CHECK(validSessionsCount > 0);
    CHECK(expiredSessionsCount > 0);
WHEN("removing all expired sessions"){
    int removedCount = Data::instance().sessions().removeExpired();
THEN("should not exist any expired session on database"){
    CHECK(removedCount == expiredSessionsCount);
    long now = TimeUtils::instance().getTimestamp();
    vector<Session> sessions = Data::instance().sessions().listAll();
    CHECK(sessions.size() == validSessionsCount);
    for(size_t i=0; i < sessions.size(); ++i){
        CHECK(!sessions[i].expired());
        CHECK(now < sessions[i].getExpireTimestamp());
    }
AND_THEN("the sessions with a valid expire time should not be removed"){
    CHECK(sessions.size() == validSessionsCount);
}
}
}
}//GIVEN
}//GIVEN

}//SCENARIO


/* *****************************************************************************************************/


SqliteSessionDAOFixture::SqliteSessionDAOFixture()
    : hasCreatedUser(false)
    , scopedDb(ScopedSqliteDatabase::DbType::InMemory)
{

}

void SqliteSessionDAOFixture::createUserOnDb(){
    UserAccount userAccount;
    if(!Data::instance().users().getByLogin(Constants::UserName,userAccount)){
        userPassword = Constants::UserPassword;
        createdUser = accountManager.create(Constants::UserName, userPassword);

        hasCreatedUser = true;
    }
    else{
        createdUser = userAccount;
    }
}

Session SqliteSessionDAOFixture::createSession(){
    createUserOnDb();

    return SessionManager::instance().startSession(createdUser, userPassword);
}

int SqliteSessionDAOFixture::createValidSessions(){
    createSession();
    return 1;
}

int SqliteSessionDAOFixture::createExpiredSessions(){
    createUserOnDb();
    for(unsigned i=0; i < Constants::expiredSessionsCount; ++i){
        Session session;
        session.setExpireTimestamp(createExpiredTimestamp());
        session.setUserId(createdUser.getId());
        session.setToken(generateToken(i));

        Data::instance().sessions().insert(session);
    }
    return Constants::expiredSessionsCount;
}

string SqliteSessionDAOFixture::generateToken(const int number){
    return EncodingUtils::instance().generateSHA1(StringCast::instance().toString(number));
}

long SqliteSessionDAOFixture::createExpiredTimestamp(){
    long now = TimeUtils::instance().getTimestamp();
    return now - Constants::decreaseTime;
}
