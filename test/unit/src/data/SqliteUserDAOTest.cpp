#include "catch.hpp"

#include "TestMacros.h"
#include "utils/ScopedDatabase.h"
#include "helpers/UserHelper.h"
#include "helpers/ScopedChangeLogLevel.h"

#include "data/DataTransaction.h"
#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/dao/SqliteUserDAO.h"

#include "model/user/UserAccount.h"
#include "services/SessionManager.h"
#include "services/AccountManager.h"

#include "utils/FileUtils.h"
#include "utils/StringCast.h"

#include <string>
#include <algorithm>

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

namespace calmframe {
namespace Constants{
    constexpr const char * UserName      = "name";
    constexpr const char * UserPassword  = "password";
    constexpr const char * otherUserName = "othername";
    constexpr const char * PasswordHash  = "ee11cbb19052e40b07aac0ca060c23ee";
    constexpr const char * PasswordSalt  = "";
    constexpr const char * OtherHash     = "10acf03469bae03480ac0ca060c23ee";
    constexpr const char * OtherSalt     = "acf2e40b0";

    static const long NON_EXISTENT_ID = 0;
}
}

#define FIXTURE SqliteUserDAOFixture
#define TEST_TAG "[UserDAO][SqliteUserDAO][database]"

class SqliteUserDAOFixture{
public:
    string imagePath;

    /** (last) account created by fixture */
    UserAccount createdAccount;
    UserAccount updatedAccount;
    
    UserAccount createdUser;
    string userPassword;

    AccountManager accountManager;
    ScopedSqliteDatabase scopedDb{ScopedSqliteDatabase::DbType::InMemory};

    void createUserOnDb();
    vector<UserAccount> createUsers(int numUsers);
    UserAccount insertUser();
    UserAccount createAccount();
   
public:
    void testInsertAccount();
    void testInsertAccountWithSameLogin();
    void testIdGenerationOnInsertion();
    
    void testGetAccount();
    void testGetAccountByLogin();
    
    void testUpdateAccount();
    void testUpdateUserInfo();
    void testCascadeUserDelete();
    
    void testListUsers();
    void testDeleteUser();
    void testCreateAccountWithGivenId();
    
public:
    void given_account_instance();
    long given_an_existent_account();
    
    /** @return created session id*/
    long given_session_for_user(long accountId);
    /** @return created event id*/
    long given_event_with_user(long accountId);
    /** @return context to that the created permission relates*/
    long given_permission_in_context_for_user(long accountId);
    
    long when_inserting_account(const string & login=string());
    UserAccount when_getting_that_user(long userId);
    UserAccount when_changing_account_password(const UserAccount & account);
    void when_updating_account(const UserAccount & updatedAccount);
    UserAccount when_getting_account_by_login(const string & accountLogin);
        
    void then_a_valid_account_should_exist(long userId);
    void these_accounts_should_be_equal(const UserAccount & account, const UserAccount & expected);
    void check_event_exist_but_user_is_empty(long eventId);
    void check_permission_not_exist(long accountId, long ctxId);
};

bool accountComparator(const UserAccount & accountA, const UserAccount & accountB);
bool userComparator(const User & userA, const User & userB);

/* ************************************** tests ******************************************/

TEST_SCENARIO("Insert account on database")             IS_DEFINED_BY(testInsertAccount)
TEST_SCENARIO("Insert account with existent login")     IS_DEFINED_BY(testInsertAccountWithSameLogin)
TEST_SCENARIO("New accounts should have different ids") IS_DEFINED_BY(testIdGenerationOnInsertion)
TEST_SCENARIO("Create account with a given id")         IS_DEFINED_BY(testCreateAccountWithGivenId)

TEST_SCENARIO("Get account from database")              IS_DEFINED_BY(testGetAccount)
TEST_SCENARIO("Get account by login")                   IS_DEFINED_BY(testGetAccountByLogin)
TEST_SCENARIO("List users from database")               IS_DEFINED_BY(testListUsers)

TEST_SCENARIO("Update user account")                    IS_DEFINED_BY(testUpdateAccount)
TEST_SCENARIO("Update user")                            IS_DEFINED_BY(testUpdateUserInfo)

TEST_SCENARIO("Delete user")                            IS_DEFINED_BY(testDeleteUser)
TEST_SCENARIO("Cascade user id delete")                 IS_DEFINED_BY(testCascadeUserDelete)

/* ***************************************************************************************/

void SqliteUserDAOFixture::testInsertAccount(){
    GIVEN("an UserAccount instance"){
        given_account_instance();
        
    WHEN("inserting that account on database"){
        long userId = when_inserting_account();
        
    THEN("a new valid account should exist on database"){
        then_a_valid_account_should_exist(userId);
    }
    }//WHEN
    }//GIVEN
}

void SqliteUserDAOFixture::testGetAccount(){
    GIVEN("an existing user account"){
        long userId = given_an_existent_account();
    
    AND_WHEN("Getting an userAccount with that id"){
        UserAccount userById = when_getting_that_user(userId);
        
    THEN("it should return an userAccount with same attributes"){
        these_accounts_should_be_equal(userById, this->createdAccount);
    }
    }
    }//GIVEN
}

void SqliteUserDAOFixture::testInsertAccountWithSameLogin(){
    GIVEN("an existent account"){
        this->given_an_existent_account();
        
    AND_WHEN("trying to insert another userAccount with same login"){
        string initialLogin = createdAccount.getLogin();
        
    THEN("it should fail with an exception"){
        ScopedChangeLogLevel muteLog{LogLevel::Quiet};
        
        CHECK_THROWS(this->when_inserting_account(initialLogin));
    }
    }
    }
}

void SqliteUserDAOFixture::testIdGenerationOnInsertion(){
    GIVEN("an existent account"){
        long initialId = this->given_an_existent_account();
        
    WHEN("inserting another account"){
        long id = this->when_inserting_account("random login");
    
    THEN("it should have a different id"){
        CHECK(id != initialId);
    }//THEN
    }//WHEN
    }//GIVEN
}

void SqliteUserDAOFixture::testGetAccountByLogin(){
    GIVEN("an existing account"){
        given_an_existent_account();
        
        string accountLogin = createdAccount.getLogin();
        CAPTURE(accountLogin);
        
    AND_WHEN("getting that userAccount by login"){
        UserAccount foundAccount = when_getting_account_by_login(accountLogin);
        
    THEN("it should correctly found the userAccount"){
        these_accounts_should_be_equal(foundAccount, createdAccount);
    }
    }
    }//GIVEN
}

void SqliteUserDAOFixture::testUpdateAccount(){
    GIVEN("an existing account"){
        long userId = given_an_existent_account();
        
    WHEN("changing that account's password and updating it"){
        auto changedAccount = when_changing_account_password(createdAccount);
        when_updating_account(changedAccount);
    
    THEN("it should be correctly saved on database"){
        auto account = when_getting_that_user(updatedAccount.getId());
        these_accounts_should_be_equal(account, updatedAccount);
        
    }//THEN
    }//WHEN
    }//GIVEN
}

void SqliteUserDAOFixture::testUpdateUserInfo(){

GIVEN("an existent user account on database"){
    UserAccount account = insertUser();
    User & user = account.getUser();

WHEN("trying to update that user info of that account"){
    user.setLogin("otherLogin");
    user.name("other name");
    user.setActive(!user.isActive());
    user.setIsAdmin(!user.isAdmin());
    user.profileImage("myimage.jpeg");

    Data::instance().users().update(user);

THEN("it should change the user data on db"){
    UserAccount changedAccount;
    Data::instance().users().get(user.getId(), changedAccount);

//    CHECK(user == changedData.getUser());
    UserHelper::checkEquals(user, changedAccount.getUser());
    CHECK(account.getPassHash() == changedAccount.getPassHash());
    CHECK(account.getPassSalt() == changedAccount.getPassSalt());
}//THEN
}//WHEN
}//GIVEN
}

void SqliteUserDAOFixture::testCascadeUserDelete(){
    GIVEN("an existent account with some related entities"){
        long accountId = given_an_existent_account();
        long sessionId = given_session_for_user(accountId);
        long eventId = given_event_with_user(accountId);
        long ctxId = given_permission_in_context_for_user(accountId);

    WHEN("deleting this user"){
        Data::instance().users().remove(accountId);
    
    THEN("all entities should cascade the change"){
        CHECK_FALSE(Data::instance().sessions().exist(sessionId));
        
        check_event_exist_but_user_is_empty(eventId);
        check_permission_not_exist(accountId, ctxId);
    }//THEN
    }//WHEN
    }//GIVEN
}

void SqliteUserDAOFixture::testListUsers(){
    GIVEN("some users account on database"){
        vector<UserAccount> createdUsers = createUsers(2);
    
    WHEN("list users"){
        vector<User> users = Data::instance().users().listUsers();
    
    THEN("it should return a collection with all created users"){
        std::sort(createdUsers.begin(), createdUsers.end(), accountComparator);
        std::sort(users.begin(), users.end(), userComparator);
    
        REQUIRE(createdUsers.size() == users.size());
    
        for(size_t i=0; i < createdUsers.size(); ++i){
            UserHelper::checkEquals(createdUsers[i].getUser(), users[i]);
        }
    }//THEN
    }//WHEN
    }//GIVEN
}

void SqliteUserDAOFixture::testDeleteUser(){
    GIVEN("Given an id of an existent user"){
        UserAccount user = insertUser();
        long userId = user.getId();
    
    WHEN("calling delete on a UserDAO instance"){
        Data::instance().users().remove(userId);
    
    THEN("that user should not exist anymore"){
        CHECK(!Data::instance().users().exist(userId));
    }//THEN
    }//WHEN
    }//GIVEN
}

void SqliteUserDAOFixture::testCreateAccountWithGivenId(){
    GIVEN("some non existent user id"){
    WHEN("insert an account with that id"){
        long createdId = Data::instance().users().insertAt(Constants::NON_EXISTENT_ID, UserAccount());
    
    THEN("a new account should be created with that id"){
        CHECK(createdId == Constants::NON_EXISTENT_ID);
        CHECK(Data::instance().users().exist(createdId));
    
    }//THEN
    }//WHEN
    }//GIVEN
}

/* ***************************************************************************************/

bool accountComparator(const UserAccount & accountA, const UserAccount & accountB) {
    return (accountA.getId() < accountB.getId());
}
bool userComparator(const User & userA, const User & userB) {
    return (userA.getId() < userB.getId());
}

void SqliteUserDAOFixture::createUserOnDb(){
    createdUser = accountManager.create(Constants::UserName, Constants::UserPassword);
    userPassword = Constants::UserPassword;
}

vector<UserAccount> SqliteUserDAOFixture::createUsers(int numUsers){
    return UserHelper::createAccounts(numUsers);
}

UserAccount SqliteUserDAOFixture::insertUser(){
    return UserHelper::createAccount();
}

UserAccount SqliteUserDAOFixture::createAccount(){
    UserAccount userAccount;
    userAccount.setLogin(Constants::UserName);
    userAccount.setPassHash(Constants::PasswordHash);
    userAccount.setPassSalt(Constants::PasswordSalt);
    
    return userAccount;
}

/* ************************************* steps *******************************************/

void SqliteUserDAOFixture::given_account_instance(){
    this->createdAccount = createAccount();
}

long SqliteUserDAOFixture::given_an_existent_account(){
    given_account_instance();
    return when_inserting_account();
}

long SqliteUserDAOFixture::given_session_for_user(long accountId){
    return Data::instance().sessions().insert(Session(0L, "", accountId));
}

long SqliteUserDAOFixture::given_event_with_user(long accountId){
    return Data::instance().events().insert(Event{EventType::PhotoCreation, accountId, 0L});
}

long SqliteUserDAOFixture::given_permission_in_context_for_user(long accountId){
    long ctxId = Data::instance().contexts().insert(PhotoContext{"new ctx"});
    Data::instance().permissions().putPermission(Permission{ctxId, accountId, PermissionLevel::Read});
    
    return ctxId;
}

long SqliteUserDAOFixture::when_inserting_account(const std::string & login){
    if(!login.empty()){
        createdAccount.setLogin(login);
    }

    long userId = Data::instance().users().insert(this->createdAccount);
    this->createdAccount.setId(userId);

    return userId;
}

UserAccount SqliteUserDAOFixture::when_getting_that_user(long userId)
{
    UserAccount userById;
    bool foundUser = Data::instance().users().get(userId, userById);
    CHECK(foundUser);

    return userById;
}

UserAccount SqliteUserDAOFixture::when_changing_account_password(const UserAccount & account){
    UserAccount updated = account;
    updated.setPassHash(Constants::OtherHash);
    updated.setPassSalt(Constants::OtherSalt);
    
    return updated;
}

void SqliteUserDAOFixture::when_updating_account(const UserAccount & updatedAccount){
    bool success = Data::instance().users().updateAccount(updatedAccount.getId(), updatedAccount);
    
    CHECK(success);
    
    this->updatedAccount = updatedAccount;
}


UserAccount SqliteUserDAOFixture::when_getting_account_by_login(const string & accountLogin){
    UserAccount account;
    bool foundUser = Data::instance().users().getByLogin(accountLogin, account);
    CHECK(foundUser == true);

    return account;
}

void SqliteUserDAOFixture::then_a_valid_account_should_exist(long userId){
    CHECK(userId > 0);
    CHECK(Data::instance().users().exist(userId));    
}

void SqliteUserDAOFixture
    ::these_accounts_should_be_equal(const UserAccount & account, const UserAccount & expected){
    CHECK(account.getId()       == expected.getId());
    CHECK(account.getLogin()    == expected.getLogin());
    CHECK(account.getPassHash() == expected.getPassHash());
    CHECK(account.getPassSalt() == expected.getPassSalt());    
}

void SqliteUserDAOFixture::check_event_exist_but_user_is_empty(long eventId){
    CHECK(Data::instance().events().exist(eventId));
    Event evt = Data::instance().events().get(eventId);
    CHECK(evt.getUserId() == User::INVALID_ID);    
}

void SqliteUserDAOFixture::check_permission_not_exist(long accountId, long ctxId){
    Permission p;
    CHECK_FALSE(Data::instance().permissions().tryGetPermission(accountId, ctxId, p));    
}
