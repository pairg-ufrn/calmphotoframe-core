#include "catch.hpp"

#include "data/data-sqlite/dao/SqlitePhotoMetadataDAO.h"
#include "data/data-sqlite/SqliteData.h"
#include "utils/ScopedDatabase.h"

#include "model/photo/PhotoMetadata.h"
#include "services/image/MetadataExtractor.h"
#include "utils/FileUtils.h"

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "helpers/PhotoHelper.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace calmframe::photo;

namespace calmframe {

namespace Constants{
    static const char * imageWithMetadata = "test/data/test-images/test1.jpg";

    static const int PhotoListSize = 5;
    static const int INEXISTENT_ID = 100000;
}
}

class SqlitePhotoMetadataFixture{
private:
    PhotoMetadata photoMetadata;
public:
    ScopedSqliteDatabase scopedDb;
    string imagePath;
    SqliteData & database;

    SqlitePhotoMetadataFixture()
     : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
     , imagePath(Constants::imageWithMetadata)
     , database(static_cast<SqliteData&>(Data::instance()))
    {
    }

    void assertMetadataEquals(const PhotoMetadata & metadataA, const PhotoMetadata & metadataB){

        CHECK(metadataA.getPhotoId()      == metadataB.getPhotoId());
        CHECK(metadataA.getCameraMaker()  == metadataB.getCameraMaker());
        CHECK(metadataA.getCameraModel()  == metadataB.getCameraModel());
        CHECK(metadataA.getImageSize()    == metadataB.getImageSize());
        CHECK(metadataA.getImageWidth()   == metadataB.getImageWidth());
        CHECK(metadataA.getImageHeight()  == metadataB.getImageHeight());
        CHECK(metadataA.getOriginalDate() == metadataB.getOriginalDate());
        CHECK(metadataA.getLocation()     == metadataB.getLocation());
        CHECK(metadataA.getOrientation().getFlag()  == metadataB.getOrientation().getFlag());
    }

    PhotoMetadata & getMetadata(){
        if(photoMetadata.getImageSize() == PhotoMetadata::UNKNOWN_SIZE){
            photoMetadata = MetadataExtractor::instance().extractMetadata(imagePath);
        }
        return photoMetadata;
    }

    Photo insertPhotoWithMetadata(){
        Photo photo;
        photo.setMetadata(this->getMetadata());
        long photoId = this->database.photos().insert(photo);
        this->database.photos().getPhoto(photoId, photo);
        return photo;
    }
};
class SqlitePhotoMetadataFixture_WithListOfPhotos : public SqlitePhotoMetadataFixture{
public:
    vector<Photo> photos;

    void fillPhotos(int photoNumber, bool differentiate = true){
        PhotoMetadata  & basePhotoMetadata = getMetadata();

        for(int i=0; i < photoNumber; ++i){
            Photo photo;
            photo.setMetadata(basePhotoMetadata);
            if(differentiate){
               photo.getMetadata().setImageSize(basePhotoMetadata.getImageSize() + i);
            }
            photos.push_back(photo);
        }
    }

    vector<Photo> copyPhotosWithoutMetadata(){
        vector<Photo> photos;
        for(int i=0; i < this->photos.size(); ++i){
            Photo photo;
            photo.setId(this->photos[i].getId());
            photos.push_back(photo);
        }

        return photos;
    }

    Photo createInexistentPhoto(){
        Photo inexistentPhoto;
        inexistentPhoto.setId(Constants::INEXISTENT_ID);
        return inexistentPhoto;
    }
};


SCENARIO("Saving metadata on database","[SqlitePhotoMetadataDAO][data]"){
    GIVEN("a database instance"){
        SqlitePhotoMetadataFixture fixture;
    GIVEN("some photo with metadata"){
        Photo photo;
        photo.setMetadata(MetadataExtractor::instance().extractMetadata(fixture.imagePath));
    WHEN("insert it on database"){
        long photoId = fixture.database.photos().insert(photo);
    THEN("the database should contain that metadata"){
        vector<PhotoMetadata> metadataList = fixture.database.getPhotoMetadataDAO().listAll();
        CHECK(metadataList.size() == 1);

        PhotoMetadata savedMetadata = fixture.database.getPhotoMetadataDAO().get(photoId);
        CHECK(savedMetadata.getPhotoId() == photoId);
        photo.getMetadata().setPhotoId(photoId);
        CHECK(savedMetadata == photo.getMetadata());
    }
    }
    }
    }
}

bool comparePhotoById(const Photo & photoA, const Photo & photoB){
    return photoA.getId() < photoB.getId();
}
bool compareById(const PhotoMetadata & metadataA, const PhotoMetadata & metadataB){
    return metadataA.getPhotoId() < metadataB.getPhotoId();
}

SCENARIO("Reading metadata list from database","[SqlitePhotoMetadataDAO][data]"){
    GIVEN("a database instance"){
        SqlitePhotoMetadataFixture_WithListOfPhotos fixture;
    GIVEN("a list of photos with metadata"){
        fixture.fillPhotos(Constants::PhotoListSize);
    WHEN("inserting the photos on database"){
        for(int i=0; i < fixture.photos.size(); ++i){
            Photo & photo = fixture.photos[i];
            long photoId = fixture.database.photos().insert(photo);
            photo.setId(photoId);

            //TODO: (?) talvez Photo::setId devesse chamar PhotoMetadata.setPhotoId
            photo.getMetadata().setPhotoId(photoId);
        }

        AND_WHEN("listing all photos on database"){
            vector<Photo> photoDbList = fixture.database.photos().listAll();
        THEN("it should return a photo list with correspondent PhotoMetadata instances"){
            REQUIRE(photoDbList.size() == fixture.photos.size());
            //Ordena com base no id
            std::sort(fixture.photos.begin(), fixture.photos.end(), comparePhotoById);
            std::sort(photoDbList.begin(), photoDbList.end(),comparePhotoById);

            for(int i=0; i < fixture.photos.size(); ++i){
                CHECK(fixture.photos[i].getMetadata() == photoDbList[i].getMetadata());
            }
        }
        }

        AND_WHEN("calling PhotoMetadataDAO::listAll passing an vector of Photos"){
            //Ordena com base no id
            std::sort(fixture.photos.begin(), fixture.photos.end(), comparePhotoById);

            vector<Photo> photos = fixture.copyPhotosWithoutMetadata();
            //Adiciona foto inexistente à lista
            photos.push_back(fixture.createInexistentPhoto());

            fixture.database.getPhotoMetadataDAO().listAll(photos);
        THEN("for each photo on vector with a valid id it should put the correspondent PhotoMetadata"){

            for(int i=0; i < fixture.photos.size(); ++i){
                INFO("Photo " << i);
                fixture.assertMetadataEquals(fixture.photos[i].getMetadata(), photos[i].getMetadata());
            }
        AND_THEN("photos with no metadata on database should not be changed"){
            Photo & inexistentPhoto = photos[photos.size() - 1];
            Photo emptyPhoto;
            emptyPhoto.setId(Constants::INEXISTENT_ID);

            PhotoHelper::assertEquals(inexistentPhoto, emptyPhoto);

            PhotoMetadata emptyMetadata;
            fixture.assertMetadataEquals(inexistentPhoto.getMetadata(), emptyMetadata);
        }
        }
        }
    }
    }
    }
}

SCENARIO("Deleting metadata from database","[SqlitePhotoMetadataDAO][data][slow]"){
    GIVEN("a database instance"){
        SqlitePhotoMetadataFixture fixture;
    GIVEN("some photo with metadata on database"){
        Photo photo;
        photo.setMetadata(fixture.getMetadata());
        long photoId = fixture.database.photos().insert(photo);
        fixture.database.photos().getPhoto(photoId, photo);
        REQUIRE(fixture.database.getPhotoMetadataDAO().exist(photoId) == true);
    WHEN("deleting the photo"){
        bool removed = fixture.database.photos().remove(photo);
        REQUIRE(removed == true);
    THEN("the photo metadata should also be deleted"){
        REQUIRE(fixture.database.getPhotoMetadataDAO().exist(photoId) == false);
    }
    }
    }
    }
}

SCENARIO("Updating metadata in database","[SqlitePhotoMetadataDAO][data][slow]"){
    GIVEN("a database instance"){
        SqlitePhotoMetadataFixture fixture;
    GIVEN("some photo with metadata on database"){
        Photo photo = fixture.insertPhotoWithMetadata();
        REQUIRE(fixture.database.photos().exist(photo.getId()) == true);
        REQUIRE(fixture.database.getPhotoMetadataDAO().exist(photo.getId()) == true);
    WHEN("changing some attributes on metadata"){
        photo.getMetadata().setImageWidth(10000);
        photo.getMetadata().getLocation().setAltitude(0.12f);
        photo.getMetadata().getOrientation().setFlag(Orientation::RIGHT_BOTTOM);
        photo.getMetadata().setCameraModel("Other camera model");
    AND_WHEN("updating the photo on database"){
        fixture.database.photos().update(photo);
    THEN("the photo metadata should be updated correctly"){
        PhotoMetadata updatedMetadata = fixture.database.getPhotoMetadataDAO().get(photo.getId());

        fixture.assertMetadataEquals(photo.getMetadata(), updatedMetadata);
    }
    }
    }
    }
    }
}
