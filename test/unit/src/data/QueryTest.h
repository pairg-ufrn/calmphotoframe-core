#ifndef QUERYSTATEMENTTEST_H
#define QUERYSTATEMENTTEST_H

#include "data/data-sqlite/Query.h"

#include "TestMacros.h"
#include "utils/ScopedDatabase.h"

#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/Table.h"


#define TEST_TAG "[Query]"
#define FIXTURE QueryFixture

using namespace std;
using namespace calmframe;
using namespace calmframe::data;

class QueryFixture{

    ScopedSqliteDatabase scopedDatabase;
public:

    SqliteConnector & getConnector();

    Table createPhotoTable();

public: //tests
    void testQueryInitialization();
    void testQueryInitializeWithColumnsList();
    void testReadData();
    void testReadWithReader();
    void testWhereTemplate();
    void testCreateFromQueryArgs();
};

#endif // QUERYSTATEMENTTEST_H
