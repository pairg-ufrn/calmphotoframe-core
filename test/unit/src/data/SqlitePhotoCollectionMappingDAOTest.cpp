#include "catch.hpp"

#include "data/data-sqlite/dao/SqlitePhotoCollectionMappingDAO.h"
#include "data/data-sqlite/tables/PhotoCollectionTable.h"
#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "model/photo/PhotoCollection.h"

#include "utils/FileUtils.h"
#include "utils/StringCast.h"

#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <algorithm>

#include "helpers/PhotoHelper.h"
#include "utils/ScopedDatabase.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

namespace calmframe {

namespace Constants{
    static const char * COLLECTION_NAME = "no name";
    static const long PHOTO_IDS[5] = {2, 3, 5,6, 8};
    static const set<long> PHOTO_IDS_SET(PHOTO_IDS, PHOTO_IDS + 5);
}
}


class TestFixture{
private:
    ScopedSqliteDatabase scopedDb;
public:
    vector<Photo> photos;
    SqliteData & getDb(){
        return (SqliteData &)Data::instance();
    }
    Photo insertPhoto(){
        PhotoHelper::insertPhotos(1);
        this->photos = getDb().photos().listAll();
        REQUIRE(!photos.empty());
        return photos[photos.size() - 1];
    }
    void insertPhotoCollection(PhotoCollection & coll)
    {
        long id = getDb().collections().insert(coll);
        coll.setId(id);
    }

    PhotoCollection insertPhotoCollection(const std::string & name){
        PhotoCollection coll;
        coll.setName(name);
        insertPhotoCollection(coll);

        return coll;
    }

    PhotoCollection insertPhotoCollectionWithPhotos(){
        PhotoCollection coll;
        coll.setName(Constants::COLLECTION_NAME);
        coll.setPhotos(Constants::PHOTO_IDS_SET);

        insertPhotoCollection(coll);

        return coll;
    }

    void changePhotoIds(PhotoCollection & photoCollection){
        int count=0;
        set<long>::const_iterator iter = Constants::PHOTO_IDS_SET.begin();
        while(iter != Constants::PHOTO_IDS_SET.end()){
            if(count % 2 == 0){
                photoCollection.removePhoto(*iter);
            }
            ++count;
            ++iter;
        }

        for(int i=20; i < 23; ++i){
            photoCollection.putPhoto(i);
        }
    }

    set<long> getOtherCollectionIds(long photoCollectionId){
        vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
        set<long> collectionIds = getIds(allCollections.begin(), allCollections.end());
        set<long> otherCollectionIds = collectionIds;
        otherCollectionIds.erase(photoCollectionId);

        return otherCollectionIds;
    }

    SqlitePhotoCollectionMappingDAO & collectionMappingDAO(){
        return getDb().getCollectionMappingDAO();
    }

    long putCollectionsOnDatabase(int collectionsNumber = 5){
        long lastCollectionId = 0;
        for(int i=0; i < collectionsNumber; ++i){
            lastCollectionId = Data::instance().collections().insert(makeCollection(i));
        }
        return lastCollectionId;
    }
    PhotoCollection makeCollection(int collNumber){
        PhotoCollection collection;
        collection.setName(string("Collection ").append(StringCast::instance().toString(collNumber)));
        return collection;
    }
    PhotoCollection putPhotosOnCollection(long collectionId, int numPhotos){
        PhotoHelper::insertPhotos(numPhotos);
        vector<Photo> photos = Data::instance().photos().listAll();
        PhotoCollection coll = Data::instance().collections().get(collectionId);
        for(size_t i=0; i < numPhotos && i< photos.size(); ++i){
            coll.putPhoto(photos[i].getId());
        }
        CHECK(Data::instance().collections().update(coll));

        return coll;
    }

    template<typename Iterator>
    set<long> getIds(Iterator begin, Iterator end){
        set<long> ids;

        Iterator iter = begin;
        while(iter != end){
            ids.insert((*iter).getId());
            ++iter;
        }
        return ids;
    }

    void assertAllCollectionsContainsPhotos(const vector<PhotoCollection> & allCollections
            , const set<long> & collectionIds, const set<long> & photoIds)
    {
        map<long, PhotoCollection> allCollectionsById = PhotoCollection::mapById(allCollections.begin(), allCollections.end());
        for(set<long>::iterator i=collectionIds.begin(); i!=collectionIds.end(); ++i){
            const PhotoCollection & coll = allCollectionsById[*i];
            assertCollectionContainsPhotos(coll, photoIds);
        }
    }
    void assertCollectionContainsPhotos(const PhotoCollection & coll, const set<long> & photoIds){
        for(set<long>::iterator i=photoIds.begin(); i!= photoIds.end(); ++i){
            INFO("Collection id: " << coll.getId() << "; photo id: " << *i);
            CHECK(coll.containsPhoto(*i));
        }
    }

    void assertAllCollectionsNotContainsPhotos(const vector<PhotoCollection> & allCollections
            , const set<long> & collectionIds, const set<long> & photoIds)
    {
        map<long, PhotoCollection> allCollectionsById = PhotoCollection::mapById(allCollections.begin(), allCollections.end());
        for(set<long>::iterator i=collectionIds.begin(); i!=collectionIds.end(); ++i){
            const PhotoCollection & coll = allCollectionsById[*i];
            assertCollectionNotContainsPhotos(coll, photoIds);
        }
    }
    void assertCollectionNotContainsPhotos(const PhotoCollection & coll, const set<long> & photoIds){
        for(set<long>::iterator i=photoIds.begin(); i!= photoIds.end(); ++i){
            INFO("Collection id: " << coll.getId() << "; photo id: " << *i);
            CHECK(!coll.containsPhoto(*i));
        }
    }

    string collectionPhotosToStr(const PhotoCollection & coll){
        stringstream sstream;
        sstream << "Collection " << coll.getId() << ". Photos: ";
        const set<long> & photoIds = coll.getPhotos();
        std::copy(photoIds.begin(), photoIds.end(), std::ostream_iterator<long>(sstream, " "));
        return sstream.str();
    }

    void splitSet(const set<long> & origin, set<long> & set1, set<long> & set2, int splitNum){
        int count =0;
        set<long>::const_iterator iter = origin.begin();
        while(iter != origin.end()){
            if(count < splitNum){
                set1.insert(*iter);
            }
            else{
                set2.insert(*iter);
            }
            ++iter;
            ++count;
        }
    }
};

SCENARIO("insert photo collection mapping on database", "[SqlitePhotoCollectionMappingDAO]"){
    TestFixture fixture;
GIVEN   ("an SqliteData instance"){
    SqliteData & data = fixture.getDb();
    SqlitePhotoCollectionMappingDAO & photoCollectionMappingDAO = fixture.getDb().getCollectionMappingDAO();
GIVEN   ("some photo on database"){
    Photo dbPhoto = fixture.insertPhoto();
GIVEN   ("some photoCollection on database"){
    PhotoCollection dbCollection = fixture.insertPhotoCollection(Constants::COLLECTION_NAME);
WHEN    ("inserting an PhotoCollectionMap on database"){
    photoCollectionMappingDAO.insert(dbCollection.getId(), dbPhoto.getId());
AND_WHEN("get photos of photoCollection"){
    set<long> photoIds = photoCollectionMappingDAO.getCollectionPhotos(dbCollection.getId());
THEN    ("it should return a set containing the associated photo id"){
    CHECK(photoIds.size() == 1);
    CHECK(photoIds.find(dbPhoto.getId()) != photoIds.end());
}//THEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN
}//SCENARIO

SCENARIO("insert subcollection mappings on database", "[SqlitePhotoCollectionMappingDAO]"){
    TestFixture fixture;

GIVEN   ("an SqliteData instance"){
    SqliteData & data = fixture.getDb();
    SqlitePhotoCollectionMappingDAO & photoCollectionMappingDAO = fixture.getDb().getCollectionMappingDAO();

GIVEN   ("some photoCollection on database with some photos"){
    long photoCollectionId = fixture.putCollectionsOnDatabase(5);
    PhotoCollection photoCollection = fixture.putPhotosOnCollection(photoCollectionId, 5);

GIVEN   ("other photoCollections on database"){
    set<long> otherCollectionIds = fixture.getOtherCollectionIds(photoCollectionId);
    REQUIRE(!otherCollectionIds.empty());
    REQUIRE(otherCollectionIds.find(photoCollectionId) == otherCollectionIds.end());

WHEN    ("inserting an PhotoCollectionMap on database"){
    int insertCount = photoCollectionMappingDAO.insertSubCollections(photoCollectionId, otherCollectionIds);
    CHECK(insertCount == otherCollectionIds.size());

AND_WHEN("get subcollections of photoCollection"){
    set<long> subcollectionIds = photoCollectionMappingDAO.getSubCollections(photoCollectionId);
    CHECK(!subcollectionIds.empty());

THEN    ("it should return a set with all subcollection ids of that PhotoCollection"){
    CHECK(subcollectionIds.size() == otherCollectionIds.size());
    CHECK(subcollectionIds == otherCollectionIds);
}//THEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN
}//SCENARIO

SCENARIO("replace photo collection mappings","[SqlitePhotoCollectionMappingDAO]"){
    TestFixture fixture;
GIVEN   ("some photo collection mappings on database with the same photo collection id"){
    PhotoCollection photoCollection = fixture.insertPhotoCollectionWithPhotos();
    set<long> initialPhotos = photoCollection.getPhotos();
    REQUIRE(!initialPhotos.empty());
    set<long> photosOnDb = fixture.collectionMappingDAO().getCollectionPhotos(photoCollection.getId());
    REQUIRE(initialPhotos == photosOnDb);

WHEN    ("replacing the photo ids of that PhotoCollection"){
    fixture.changePhotoIds(photoCollection);
    REQUIRE(photoCollection.getPhotos() != initialPhotos);
    fixture.collectionMappingDAO().updateCollectionItems(photoCollection.getId(), photoCollection.getPhotos());

AND_WHEN("getting the photo ids of that PhotoCollection"){
    set<long> photoIds = fixture.collectionMappingDAO().getCollectionPhotos(photoCollection.getId());

THEN    ("it should contain only the new photos"){
    CHECK(photoIds != initialPhotos);
    CHECK(photoIds == photoCollection.getPhotos());
}//THEN
}//WHEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Add multiple photos to one PhotoCollection","[SqlitePhotoCollectionMappingDAO]"){
    TestFixture fixture;
GIVEN("some PhotoCollection on db"){
    long collectionId = fixture.putCollectionsOnDatabase(1);
    REQUIRE(Data::instance().collections().exist(collectionId));
GIVEN("some photos on db"){
    int photoCount = 5;
    PhotoHelper::insertPhotos(photoCount);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());
    CHECK(photos.size() >= photoCount);
    CHECK(photoIds.size() == photos.size());
GIVEN("some SqlitePhotoCOllectionMappingDAO instance"){
    SqlitePhotoCollectionMappingDAO & dao = fixture.collectionMappingDAO();
WHEN("adding these photo ids on this collection"){
    int changedRows = dao.addCollectionPhotos(collectionId, photoIds);
    CHECK(changedRows == photoIds.size());
WHEN("getting this photo collection"){
    PhotoCollection coll = fixture.getDb().collections().get(collectionId);
THEN("it should contain all added photos"){
    INFO(fixture.collectionPhotosToStr(coll));
    fixture.assertCollectionContainsPhotos(coll, photoIds);

}//THEN
}//WHEN
}//WHEN
}//GIVEN

}//GIVEN
}//GIVEN

}//SCENARIO


SCENARIO("Remove multiple photos from one PhotoCollection","[SqlitePhotoCollectionMappingDAO]"){
    TestFixture fixture;
GIVEN("some PhotoCollection on db with some photos"){
    long collectionId = fixture.putCollectionsOnDatabase(1);
    REQUIRE(Data::instance().collections().exist(collectionId));

    int numPhotos = 4;
    PhotoCollection collection = fixture.putPhotosOnCollection(collectionId, numPhotos);
    REQUIRE(collection.getPhotos().size() == numPhotos);

GIVEN("some SqlitePhotoCOllectionMappingDAO instance"){
    SqlitePhotoCollectionMappingDAO & dao = fixture.collectionMappingDAO();
WHEN("removing some photo ids from this collection"){
    int removeNumPhotos = 2;
    set<long> photosToRemove;
    set<long> notRemovedPhotos;

    fixture.splitSet(collection.getPhotos(), photosToRemove, notRemovedPhotos, removeNumPhotos);

    int changedRows = dao.removeCollectionPhotos(collectionId, photosToRemove);
    CHECK(changedRows == photosToRemove.size());
AND_WHEN("getting this photo collection"){
    PhotoCollection coll = fixture.getDb().collections().get(collectionId);
THEN("it should not contain the removed photos"){
    INFO(fixture.collectionPhotosToStr(coll));
    fixture.assertCollectionNotContainsPhotos(coll, photosToRemove);
THEN("it should contain the not removed photos"){

}//THEN

}//THEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Add photos to multiple collections","[SqlitePhotoCollectionMappingDAO]"){
    TestFixture fixture;
GIVEN("some collections on db"){
    fixture.putCollectionsOnDatabase(3);
GIVEN("some photos on db"){
    PhotoHelper::insertPhotos(5);
WHEN("inserting these photos on those collections"){
    vector<PhotoCollection> collections = Data::instance().collections().listAll();
    map<long, PhotoCollection> initialCollectionsById = PhotoCollection::mapById(collections.begin(), collections.end());

    REQUIRE(collections.size() > 2);
    collections.erase(collections.begin()); //Deixar alguma coleção sem fotos

    vector<Photo> photos = Data::instance().photos().listAll();
    REQUIRE(photos.size() > 2);

    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());
    set<long> collectionIds = fixture.getIds(collections.begin(), collections.end());

    Data::instance().collections().addPhotos(collectionIds, photoIds);
THEN("all added collections should now contain those photos"){
    vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
    fixture.assertAllCollectionsContainsPhotos(allCollections, collectionIds, photoIds);
THEN("other collections should not change"){
    for(size_t i=0; i < allCollections.size(); ++i){
        const PhotoCollection & coll = allCollections[i];
        long collId = coll.getId();
        if(collectionIds.find(collId) == collectionIds.end()){//Collectiono não mudou
            CHECK(initialCollectionsById[collId] == coll);
        }
    }

}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Remove photos from multiple collections","[SqlitePhotoCollectionMappingDAO], [SqlitePhotoCollectionDAO]"){
    TestFixture fixture;
GIVEN("some collections on db"){
    fixture.putCollectionsOnDatabase(2);
    vector<PhotoCollection> collections = Data::instance().collections().listAll();
    set<long> collectionIds = fixture.getIds(collections.begin(), collections.end());

GIVEN("some photos on db in those collections"){
    PhotoHelper::insertPhotos(4);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());
    Data::instance().collections().addPhotos(collectionIds, photoIds);

GIVEN("other collections without these photos"){
    PhotoCollection otherColl = Data::instance().collections().get(fixture.putCollectionsOnDatabase(1));

    set<long> collectionIdsRemoved = collectionIds;
    collectionIdsRemoved.insert(otherColl.getId());

WHEN("removing some photos from those collections"){
    int removeCount = 2;
    set<long> photosToRemove;
    set<long> photosNotRemoved;
    fixture.splitSet(photoIds, photosToRemove, photosNotRemoved, removeCount);
    REQUIRE(photosToRemove.size() == removeCount);
    int notRemoveCount =(photoIds.size() - removeCount);
    REQUIRE(photosNotRemoved.size() == notRemoveCount);
    REQUIRE(notRemoveCount > 0);

    Data::instance().collections().removePhotosFromCollections(collectionIdsRemoved, photosToRemove);

THEN("the removed collections should now contain the removed photos"){
    vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
    map<long, PhotoCollection> allCollectionsById = PhotoCollection::mapById(allCollections.begin(), allCollections.end());
    fixture.assertAllCollectionsNotContainsPhotos(allCollections, collectionIds, photosToRemove);

AND_THEN("other photos should be keep"){
    fixture.assertAllCollectionsContainsPhotos(allCollections, collectionIds, photosNotRemoved);

THEN("other collections should not change"){
    CHECK(allCollectionsById[otherColl.getId()] == otherColl);

}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO

