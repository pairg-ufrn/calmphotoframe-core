#include "catch.hpp"

#include "data/data-sqlite/dao/SqlitePhotoCollectionDAO.h"
#include "data/data-sqlite/tables/PhotoCollectionTable.h"
#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "model/photo/PhotoCollection.h"

#include "utils/FileUtils.h"
#include "utils/StringCast.h"
#include "utils/ScopedDatabase.h"

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "helpers/PhotoHelper.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/PhotoContextHelper.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

namespace Test {

namespace Constants{
    static const int PhotoListSize = 5;
    static const int INEXISTENT_ID = 100000;

    static const char * COLLECTION_NAME = "no name";
    static const long PHOTO_IDS[5] = {2, 3, 5,6, 8};
    static const set<long> PHOTO_IDS_SET(PHOTO_IDS, PHOTO_IDS + 5);
    static const int OTHER_PHOTOID = 11;

    static const char * CONTEXT_NAME = "context name";

    static const int COLLECTIONS_COUNT = 3;
}
}

static string makeDbDirPath(){
    return Files().getDefaultTmpDir();
}

#define TEST_TAG "[SqlitePhotoCollectionDAO]"

class PhotoCollectionDAOFixture{
private:
public:
    ScopedSqliteDatabase scopedDb;

    SqliteData & getDb(){
        return (SqliteData &)Data::instance();
    }

    PhotoCollectionDAO & getDAO(){
        return Data::instance().collections();
    }

    PhotoCollection createCollection(){
        return PhotoCollection();
    }
    PhotoCollection createCollectionWithPhotos(const vector<Photo> & photos){
        PhotoCollection coll;
        coll.setPhotos(photos.begin(), photos.end());
        return coll;
    }
    PhotoCollection changePhotoCollection(const PhotoCollection & photoCollection){
        PhotoCollection coll = photoCollection;
        coll.setName("other " + coll.getName());
        coll.putPhoto(Test::Constants::OTHER_PHOTOID);
        return coll;
    }

    PhotoCollection insertCollection(long ctxId=PhotoContext::INVALID_ID){
        PhotoCollection coll;
        coll.setName(Test::Constants::COLLECTION_NAME);
        coll.setPhotos(Test::Constants::PHOTO_IDS_SET);
        if(ctxId != PhotoContext::INVALID_ID){
            coll.setParentContext(ctxId);
        }
        insertCollection(coll);
        return coll;
    }
    PhotoCollection insertCollectionWithPhotos(const vector<Photo> & photos){
        PhotoCollection coll = createCollectionWithPhotos(photos);
        coll.setName(Test::Constants::COLLECTION_NAME);
        insertCollection(coll);

        return coll;
    }
    PhotoCollection insertCollectionWithSubCollections(const set<long> & subcollectionIds){
        return PhotoCollectionHelper::createCollectionWithSubCollections(subcollectionIds);
    }
    PhotoCollection createCollectionWithPhotosAndSubCollections(int numPhotos=3, int numSubCollections=3){
        return PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(numPhotos, numSubCollections);
    }

    void insertCollection(PhotoCollection & coll)
    {
        long createdId = Data::instance().collections().insert(coll);
        coll.setId(createdId);
    }
    set<long> insertCollections(int collectionsNumber){
        set<long> collectionIds;
        for(int i=0; i < collectionsNumber; ++i){
            long id = Data::instance().collections().insert(makeCollection(i));
            collectionIds.insert(id);
        }
        return collectionIds;
    }

    long putCollectionsOnDatabase(int collectionsNumber = 5){
        long lastCollectionId = 0;
        for(int i=0; i < collectionsNumber; ++i){
            lastCollectionId = Data::instance().collections().insert(makeCollection(i));
        }
        return lastCollectionId;
    }
    PhotoCollection makeCollection(int collNumber){
        PhotoCollection collection;
        collection.setName(string("Collection ").append(StringCast::instance().toString(collNumber)));
        return collection;
    }
    PhotoContext insertPhotoContext(const std::string & name=Test::Constants::CONTEXT_NAME){
        long id = Data::instance().contexts().insert(PhotoContext(name));
        return Data::instance().contexts().get(id);
    }

    bool existCollection(const vector<PhotoCollection> & collections, long collectionId){
        vector<PhotoCollection>::const_iterator iter;
        for(iter = collections.begin(); iter != collections.end(); ++iter){
            const PhotoCollection & coll = *iter;
            if(coll.getId() == collectionId){
                return true;
            }
        }
        return false;
    }

    void checkEquals(const PhotoCollection & coll1, const PhotoCollection & coll2){
        CHECK(coll1 == coll2);
    }

    vector<Photo> insertPhotos(){
        PhotoHelper::insertPhotos(5);
        return Data::instance().photos().listAll();
    }

    void checkPhotos(const PhotoCollection & coll, const vector<Photo> & photos){
        const set<long> & photoIds = coll.getPhotos();
        for(size_t i=0; i < photos.size(); ++i){
            long photoId = photos[i].getId();
            CHECK(photoIds.find(photoId) != photoIds.end());
        }
    }
    void checkContainsPhotoCollections(const vector<PhotoCollection> & photoCollections
                                     , const set<long> & collIds)
    {
        for(size_t i =0; i < photoCollections.size(); ++i){
            CHECK(collIds.find(photoCollections[i].getId()) != collIds.end());
        }
    }
    set<long> getOtherCollectionIds(long photoCollectionId){
        vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
        set<long> collectionIds = getIds(allCollections.begin(), allCollections.end());
        set<long> otherCollectionIds = collectionIds;
        otherCollectionIds.erase(photoCollectionId);

        return otherCollectionIds;
    }

    template<typename Iterator>
    set<long> getIds(Iterator begin, Iterator end){
        return PhotoCollectionHelper::getIds<Iterator>(begin, end);
    }
    void splitSet(const set<long> & origin, set<long> & set1, set<long> & set2, int splitNum){
        PhotoCollectionHelper::splitSet(origin,set1, set2, splitNum);
    }

    void assertAllCollectionsContainsItems(const set<long> & collections, const set<long> & items
                                        , PhotoCollectionMap::MappingType itemsType)
    {
        checkCollectionsContainsItems(collections, items, itemsType, true);
    }
    void assertAllCollectionsNotContainsItems(const set<long> & collections, const set<long> & items
                                        , PhotoCollectionMap::MappingType itemsType)
    {
        checkCollectionsContainsItems(collections, items, itemsType, false);
    }
    void checkCollectionsContainsItems(const set<long> & collections, const set<long> & items
                                        , PhotoCollectionMap::MappingType itemsType, bool shouldContain)
    {
        SqlitePhotoCollectionMappingDAO collectionMappingDAO = ((SqliteData &)Data::instance()).getCollectionMappingDAO();
        for(set<long>::iterator collIter =collections.begin(); collIter != collections.end(); ++ collIter){
            std::set<long> collectionItems = collectionMappingDAO.getCollectionItems(*collIter, itemsType);

            for(set<long>::iterator itemsIter = items.begin(); itemsIter != items.end(); ++ itemsIter){
                INFO("Collection " << *collIter << " item " << *itemsIter << " of type " << itemsType);
                bool containsItem = (collectionItems.find(*itemsIter) != collectionItems.end());
                if(shouldContain){
                    CHECK(containsItem);
                }
                else{
                    CHECK_FALSE(containsItem);
                }
            }
        }
    }

    void assertCollectionMappingsRemoved(long collectionId){
        std::vector<PhotoCollectionMap> mappings = getDb().getCollectionMappingDAO().listAll();
        for(size_t i=0; i < mappings.size(); ++i){
            const PhotoCollectionMap & photocollectionMap = mappings[i];
            CHECK(photocollectionMap.getCollectionId() != collectionId);
            if(photocollectionMap.getType() == PhotoCollectionMap::SubCollectionMapping){
                CHECK(photocollectionMap.getItemId() != collectionId);
            }
        }
    }

    void checkCollectionsExistence(const std::set<long> & colls, bool shouldExist){
        std::set<long>::const_iterator iter = colls.begin();
        while(iter != colls.end()){
            long collId = *iter;
            bool collExist = Data::instance().collections().exist(collId);
            INFO("Collection " << collId << " exist? " << std::boolalpha << collExist);
            CHECK(collExist == shouldExist);

            ++iter;
        }
    }
};
typedef PhotoCollectionDAOFixture Fixture;

SCENARIO("create photo collection table on database", TEST_TAG){

GIVEN("an SqliteDataBuilder instance"){
    PhotoCollectionDAOFixture fixture;

WHEN("building database"){
THEN("PhotoCollection's table should exist on database"){
    SqliteConnector & dbConnector = fixture.getDb().getDatabaseConnector();
    CHECK(dbConnector.existTable(Tables::PhotoCollectionTable::NAME));
AND_THEN("should exist another table with mapping between photos and collections"){
    CHECK(dbConnector.existTable(Tables::PhotoCollectionItemsTable::NAME));
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Get PhotoCollection with subcollections",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("some PhotoCollection with subcollections on database"){
    set<long> subcollectionIds = fixture.insertCollections(2);
    PhotoCollection photoCollection = fixture.insertCollectionWithSubCollections(subcollectionIds);
    long photoCollectionId = photoCollection.getId();

WHEN("getting that PhotoCollection from db"){
    PhotoCollection photoCollection = Data::instance().collections().get(photoCollectionId);

THEN("it should contains the subcollection ids"){
    CHECK(photoCollection.getSubCollections().getIds() == subcollectionIds);

AND_WHEN("getting all PhotoCollection from db"){
    vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
    map<long, PhotoCollection> allCollectionsById = PhotoCollection::mapById(allCollections.begin(), allCollections.end());

THEN("that PhotoCollection should also contains his subcollections"){
    CHECK(allCollectionsById[photoCollectionId].getSubCollections().getIds() == subcollectionIds);
}//THEN
}//WHEN
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO


SCENARIO("insert photo collection on database", TEST_TAG){

GIVEN("an database with 0 collections, but some photos"){
    PhotoCollectionDAOFixture fixture;
    fixture.insertPhotos();
    vector<Photo> photos = Data::instance().photos().listAll();
    REQUIRE(!photos.empty());

GIVEN("an PhotoCollectionDAO instance"){
    PhotoCollectionDAO & collectionDAO =  Data::instance().collections();

WHEN("list PhotoCollection instances"){
    vector<PhotoCollection> collections = collectionDAO.listAll();

THEN("it should return an empty collection"){
    CHECK(collections.size() == 0);

WHEN("inserting a new photo collection, with some photo ids"){
    PhotoCollection newCollection = fixture.createCollectionWithPhotos(photos);
    REQUIRE(!newCollection.getPhotos().empty());

    long createdId = collectionDAO.insert(newCollection);
THEN("it should return a valid id"){
    CHECK(createdId != PhotoCollection::INVALID_ID);

WHEN("call get(newId)"){
    PhotoCollection dbCollection = collectionDAO.get(createdId);
THEN("it should return a PhotoCollection equals to the created PhotoCollection"){
    newCollection.setId(createdId);
    CHECK(newCollection == dbCollection);

AND_THEN("that photoCollection should contain an set of the related photo ids"){
    const set<long> & photoIds = dbCollection.getPhotos();

    CHECK(!photoIds.empty());
    fixture.checkPhotos(dbCollection, photos);

AND_WHEN("list PhotoCollection instances"){
    collections = collectionDAO.listAll();

THEN("it should return some collection that contains the new PhotoCollection"){
    CHECK_FALSE(collections.empty());
    CHECK(fixture.existCollection(collections, createdId));

AND_WHEN("call exist(newId)"){
    bool existPhotoCollection = collectionDAO.exist(createdId);

THEN("it should return true"){
    CHECK(existPhotoCollection == true);

}//THEN
}//WHEN
}//THEN
}//WHEN
}//THEN
}//THEN
}//WHEN
}//THEN
}//WHEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//SCENARIO

SCENARIO("Insert PhotoCollection with parent collection",TEST_TAG){
    PhotoCollectionDAOFixture fixture;

GIVEN("some PhotoCollectionDAO"){
    PhotoCollectionDAO & dao = Data::instance().collections();

GIVEN("some PhotoCollection on db"){
    PhotoContext ctx = PhotoContextHelper::insertContext();
    long ctxId = ctx.getId();
    REQUIRE(Data::instance().contexts().exist(ctxId));

    PhotoCollection parentCollection = fixture.insertCollection(ctxId);
    long parentCollectionId = parentCollection.getId();
    REQUIRE(parentCollection.getParentContext() == ctxId);

WHEN("inserting an PhotoCollection and passing that collectionId as parent"){
    PhotoCollection newCollection = fixture.makeCollection(parentCollectionId + 1);
    long collId = dao.insert(newCollection, parentCollectionId);

WHEN("getting the parent collection from database"){
    parentCollection = dao.get(parentCollectionId);

THEN("it should contains the created PhotoCollection as subcollection"){
    CHECK(parentCollection.getSubCollections().contains(collId));

THEN("the inserted PhotoCollection should have the same PhotoContext id as the parent"){
    PhotoCollection createdPhotoColl = Data::instance().collections().get(collId);
    CHECK(createdPhotoColl.getParentContext() == ctxId);
}//THEN
}//THEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN


}//SCENARIO


SCENARIO("Insert at PhotoCollection",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("an PhotoCollectionDAO instance"){
    PhotoCollectionDAO & dao = Data::instance().collections();
GIVEN("some unexistent photoCollection id"){
    long unexistentId = Test::Constants::INEXISTENT_ID;
    REQUIRE(!dao.exist(unexistentId));

WHEN("calling insertAt and passing that id and some PhotoCollection"){
    PhotoCollection collection;
    long createdId = dao.insertAt(unexistentId, collection);
THEN("an PhotoCollection should be created with that id"){
    CHECK(createdId == unexistentId);
    CHECK(dao.exist(createdId));

    collection.setId(createdId);
    PhotoCollection dbCollection = dao.get(createdId);
    CHECK(dbCollection == collection);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Insert PhotoCollection with PhotoContext","[tag]"){

}//SCENARIO


SCENARIO("Update PhotoCollection",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN   ("some PhotoCollection on database"){
    PhotoCollection photoCollection = fixture.insertCollection();
    REQUIRE(Data::instance().collections().exist(photoCollection.getId()));
WHEN    ("changing that photoCollection"){
    PhotoCollection changedCollection = fixture.changePhotoCollection(photoCollection);
    REQUIRE(changedCollection != photoCollection);
WHEN    ("updating that photoCollection on database"){
    Data::instance().collections().update(changedCollection);
WHEN    ("reading the photoCollection from db"){
    PhotoCollection updatedCollection = Data::instance().collections().get(photoCollection.getId());
THEN    ("it should be equals to the changed instance"){
    CHECK(updatedCollection == changedCollection);
    CHECK(photoCollection != updatedCollection);
}//THEN
}//WHEN
}//WHEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("list photos from collection",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("some photos on database"){
    vector<Photo> photos = fixture.insertPhotos();
GIVEN("a PhotoCollection containing that photos"){
    PhotoCollection photoCollection = fixture.insertCollectionWithPhotos(photos);
GIVEN("an PhotoCollectionDAO"){
    PhotoCollectionDAO & collectionDAO = Data::instance().collections();
WHEN ("calling getCollectionPhotos passing the collection id"){
    calmframe::data::SetId photoIds = collectionDAO.getCollectionPhotos(photoCollection.getId());
THEN ("it should return a container with the collection photos"){
    CHECK(photos.size() == photoIds.size());
    for(long photoId : photoIds){
        CHECK(photoCollection.containsPhoto(photoId));
    }
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("List subcollections from Collection",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("some PhotoCollection with subcollections"){
    int collNumber =3;
    set<long> subcollectionIds = fixture.insertCollections(collNumber);
    PhotoCollection photoCollection = fixture.insertCollectionWithSubCollections(subcollectionIds);
    long photoCollectionId = photoCollection.getId();

GIVEN("a PhotoCollectionDAO"){
    PhotoCollectionDAO & photoCollectionDAO = Data::instance().collections();

WHEN("getting the subcollections of that collection"){
    vector<PhotoCollection> subcollections = photoCollectionDAO.getSubCollections(photoCollectionId);

THEN("it should return a container with the subcollections"){
    CHECK(subcollections.size() == subcollectionIds.size());
    CHECK(fixture.getIds(subcollections.begin(), subcollections.end()) == subcollectionIds);

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO


SCENARIO("Remove PhotoCollection",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("some PhotoCollection on database"){
    PhotoCollection photoCollection = fixture.insertCollection();
    long collId = photoCollection.getId();
    REQUIRE(Data::instance().collections().exist(collId));

WHEN("removing the collection from db"){
    bool removed = Data::instance().collections().remove(collId);
    CHECK(removed == true);

THEN("it should not exist anymore"){
    CHECK(!Data::instance().collections().exist(collId));

}//THEN
}//WHEN
}//GIVEN

}//SCENARIO

SCENARIO("Remove multiple PhotoCollections",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("some PhotoCollections on database"){
    int collectionNumber = 5;
    fixture.putCollectionsOnDatabase(collectionNumber);

GIVEN("some PhotoCollectionDAO"){
    PhotoCollectionDAO & dao = Data::instance().collections();

GIVEN("a list of ids of PhotoCollections"){
    vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
    set<long> allIds = fixture.getIds(allCollections.begin(), allCollections.end());
    set<long> idsToRemove, idsNotRemoved;
    fixture.splitSet(allIds, idsToRemove, idsNotRemoved, collectionNumber / 2);
    REQUIRE(!idsToRemove.empty());

WHEN("requesting to delete PhotoCollections, passing a list of IDs"){
    int removeCount = dao.remove(idsToRemove);
    CHECK(removeCount == idsToRemove.size());
THEN("all PhotoCollections with id on that list should be removed"){
    set<long>::const_iterator iter = allIds.begin();
    while(iter != allIds.end()){
        long collId = *iter;
        if(idsToRemove.find(collId) != idsToRemove.end()){
            CHECK_FALSE(Data::instance().collections().exist(collId));
        }
        else{
            CHECK(Data::instance().collections().exist(collId));
        }
        ++iter;
    }
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Remove collection with collection items",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("some PhotoCollection with an collection parent"){
    PhotoCollection parentCollection = fixture.insertCollection();
    PhotoCollection collection;
    long collectionId = Data::instance().collections().insert(collection, parentCollection.getId());
    collection.setId(collectionId);

GIVEN("that this collection has some items"){
    PhotoHelper::insertPhotos(3);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());
    set<long> collectionIds;
    collectionIds.insert(collectionId);
    Data::instance().collections().addPhotos(collectionIds, photoIds);

WHEN("removing this PhotoCollection"){
    bool removed = Data::instance().collections().remove(collectionId);
    CHECK(removed);

THEN("all the mappings with that collection should be removed"){
    fixture.assertCollectionMappingsRemoved(collectionId);

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Remove multiple collections with collection items",TEST_TAG){
    PhotoCollectionDAOFixture fixture;
GIVEN("some PhotoCollections with some collection parents"){
    int collectionCount = 4;
    fixture.insertCollections(collectionCount);

    vector<PhotoCollection> collections = Data::instance().collections().listAll();
    set<long> collectionsIds = fixture.getIds(collections.begin(), collections.end());
    set<long> parentCollections, subcollections;

    fixture.splitSet(collectionsIds, parentCollections, subcollections, collectionCount/2);
    REQUIRE(!parentCollections.empty());
    REQUIRE(!subcollections.empty());

    Data::instance().collections().addItemsToCollections(parentCollections, set<long>(), subcollections);

GIVEN("that these subcollections has some photos"){
    PhotoHelper::insertPhotos(3);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());

    Data::instance().collections().addPhotos(subcollections, photoIds);

WHEN("removing these subcollections"){
    int removeCount = Data::instance().collections().remove(subcollections);
    CHECK(removeCount == subcollections.size());

THEN("all the mappings with that collection should be removed"){
    set<long>::const_iterator iter = subcollections.begin();
    while(iter != subcollections.end()){
        fixture.assertCollectionMappingsRemoved(*iter);
        ++iter;
    }
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Add multiple items to many collections",TEST_TAG){
    PhotoCollectionDAOFixture fixture;

GIVEN("a PhotoCollectionDAO instance"){
    PhotoCollectionDAO & dao = Data::instance().collections();

GIVEN("some Photos on Db"){
    int photoCount = 5;
    PhotoHelper::insertPhotos(photoCount);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());
    REQUIRE(!photoIds.empty());

GIVEN("some collections on db"){
    int collectionsCount = 4;
    fixture.putCollectionsOnDatabase(collectionsCount);
    vector<PhotoCollection> collections = Data::instance().collections().listAll();
    set<long> allCollectionsIds = fixture.getIds(collections.begin(), collections.end());
    REQUIRE(!allCollectionsIds.empty());

GIVEN("other target collections on db"){
    set<long> targetCollections, sourceCollections;
    fixture.splitSet(allCollectionsIds, targetCollections, sourceCollections, collectionsCount/2);
    REQUIRE(!targetCollections.empty());
    REQUIRE(!sourceCollections.empty());

WHEN("passing target collection ids, the photos and collections ids to addItemsToCollections method"){
    dao.addItemsToCollections(targetCollections, photoIds, sourceCollections);

THEN("all the photos and collections should be added to the target collections"){
    fixture.assertAllCollectionsContainsItems(targetCollections, photoIds, PhotoCollectionMap::PhotoMapping);
    fixture.assertAllCollectionsContainsItems(targetCollections, sourceCollections, PhotoCollectionMap::SubCollectionMapping);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO


SCENARIO("Remove multiple items from many collections",TEST_TAG){
    PhotoCollectionDAOFixture fixture;

GIVEN("a PhotoCollectionDAO instance"){
    PhotoCollectionDAO & dao = Data::instance().collections();

GIVEN("some Photos on Db"){
    int photoCount = 5;
    PhotoHelper::insertPhotos(photoCount);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());
    REQUIRE(!photoIds.empty());

GIVEN("some collections on db"){
    int collectionsCount = 6;
    fixture.putCollectionsOnDatabase(collectionsCount);
    vector<PhotoCollection> collections = Data::instance().collections().listAll();
    set<long> allCollectionsIds = fixture.getIds(collections.begin(), collections.end());
    REQUIRE(!allCollectionsIds.empty());

GIVEN("other collections on db, parent of the collections and photos"){
    set<long> parentCollections, subcollections;
    int parentCollectionsCount = collectionsCount/2;
    fixture.splitSet(allCollectionsIds, parentCollections, subcollections, parentCollectionsCount);
    REQUIRE(!parentCollections.empty());
    REQUIRE(!subcollections.empty());
    CHECK(parentCollections.size() == parentCollectionsCount);

    dao.addItemsToCollections(parentCollections, photoIds, subcollections);

WHEN("removing the photos and subcollections ids from some of the parent collections"){
    set<long> targetCollections, otherCollections;
    fixture.splitSet(parentCollections, targetCollections, otherCollections, parentCollectionsCount/2);
    REQUIRE(!targetCollections.empty());
    REQUIRE(!otherCollections.empty());

    dao.removeItemsFromCollections(targetCollections, photoIds, subcollections);

THEN("the photos and subcollections should be removed from the target collections"){
    fixture.assertAllCollectionsNotContainsItems(targetCollections, photoIds, PhotoCollectionMap::PhotoMapping);
    fixture.assertAllCollectionsNotContainsItems(targetCollections, subcollections, PhotoCollectionMap::SubCollectionMapping);

THEN("the other collections should still contain those photos and subcollection"){
    fixture.assertAllCollectionsContainsItems(otherCollections, photoIds, PhotoCollectionMap::PhotoMapping);
    fixture.assertAllCollectionsContainsItems(otherCollections, subcollections, PhotoCollectionMap::SubCollectionMapping);

}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Move multiple collection items from one collection to anothers",TEST_TAG){
    PhotoCollectionDAOFixture fixture;

GIVEN("some PhotoCollection with photos and subcollections"){
    PhotoCollection collection = fixture.createCollectionWithPhotosAndSubCollections();
    REQUIRE(!collection.getPhotos().empty());
    REQUIRE(!collection.getSubCollections().empty());

GIVEN("others collections"){
    set<long> targetCollections = fixture.insertCollections(3);

WHEN("moving some of the subcollections and photos from the first PhotoCollection to the others"){
    set<long> photosToMove, otherPhotos;
    fixture.splitSet(collection.getPhotos(), photosToMove, otherPhotos, collection.getPhotos().size()/2);

    set<long> subcollectionsToMove, otherSubCollections;
    fixture.splitSet(collection.getSubCollections().getIds(), subcollectionsToMove, otherSubCollections,
                        collection.getSubCollections().size()/2);

    Data::instance().collections().moveCollectionItems(collection.getId(), targetCollections, photosToMove, subcollectionsToMove);

THEN("the first PhotoCollection should not contain the moved photos and subcollections anymore"){
    set<long> collectionSet;
    collectionSet.insert(collection.getId());

    fixture.assertAllCollectionsNotContainsItems(collectionSet, photosToMove, PhotoCollectionMap::PhotoMapping);
    fixture.assertAllCollectionsNotContainsItems(collectionSet, subcollectionsToMove, PhotoCollectionMap::SubCollectionMapping);

AND_THEN("the first PhotoCollection should still contains the not moved photos and subcollections"){
    fixture.assertAllCollectionsContainsItems(collectionSet, otherPhotos, PhotoCollectionMap::PhotoMapping);
    fixture.assertAllCollectionsContainsItems(collectionSet, otherSubCollections, PhotoCollectionMap::SubCollectionMapping);

AND_THEN("the target collections should now contain the moved photos and subcollections"){
    fixture.assertAllCollectionsContainsItems(targetCollections, photosToMove, PhotoCollectionMap::PhotoMapping);
    fixture.assertAllCollectionsContainsItems(targetCollections, subcollectionsToMove, PhotoCollectionMap::SubCollectionMapping);
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

namespace Catch{
template<typename T>
std::ostream & operator<<(std::ostream & outStream
                        , const std::set<T> & container)
{
    typename std::set<T>::const_iterator iter = container.begin();

    const std::string & separator=", ";

    while(iter != container.end()){
        if(iter != container.begin()){
            outStream << separator;
        }
        outStream << *iter;
        ++iter;
    }
    return outStream;
}
}

SCENARIO("Remove collections without parent",TEST_TAG){
    PhotoCollectionDAOFixture fixture;

GIVEN("some PhotoCollectionDAO"){
    PhotoCollectionDAO & dao = Data::instance().collections();

GIVEN("some PhotoContext"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();

GIVEN("some PhotoCollections created on this PhotoContext"){
    long parentColl = PhotoCollectionHelper::insertCollection(-1, ctx.getId());
    set<long> collectionsToRemove = PhotoCollectionHelper::insertCollections(3, parentColl, ctx.getId());
    set<long> otherCollections = PhotoCollectionHelper::insertCollections(2,parentColl, ctx.getId());

    PhotoCollection root = Data::instance().collections().get(ctx.getRootCollection());
    CHECK(root.getSubCollections().contains(parentColl));

GIVEN("that some of these collections was removed from their parents"){
    set<long> targetSet;
    targetSet.insert(parentColl);
    dao.removeItemsFromCollections(targetSet, set<long>(), collectionsToRemove);

WHEN("Removing the collections without parent"){
    CHECK(Data::instance().collections().exist(parentColl));

    INFO("Collections to remove: " << collectionsToRemove);
    dao.removeCollectionsWithoutParent();

THEN("the collections with no parents should be removed"){
    fixture.checkCollectionsExistence(collectionsToRemove, false);

AND_THEN("other collections should not be affected"){
    fixture.checkCollectionsExistence(otherCollections, true);

THEN("the root collection should not be removed"){
    INFO("Parent collection: " << parentColl)
    CHECK(Data::instance().collections().exist(parentColl));
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO


SCENARIO("Get photos without collection",TEST_TAG){
    PhotoCollectionDAOFixture fixture;

GIVEN("a set of photo ids with some photos without collection"){
    int photoOrphansCount = 2, photoChildCount = 2;
    set<long> expectedPhotosWithoutParent = PhotoHelper::insertPhotos(photoOrphansCount);
    PhotoCollection coll = PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(photoChildCount,0);
    const set<long> & photosWithParent = coll.getPhotos();

    set<long> photoIds;
    photoIds.insert(expectedPhotosWithoutParent.begin(), expectedPhotosWithoutParent.end());
    photoIds.insert(photosWithParent.begin(), photosWithParent.end());

GIVEN("a PhotoCollectionDAO instance"){
    PhotoCollectionDAO & dao = Data::instance().collections();

WHEN("get photos without parent, passing that set of photo ids"){
    set<long> photosWithNoParent = dao.getPhotosWithoutParent(photoIds);

THEN("it should respond with the subset of those photo ids that has no photo collection"){
    set<long> collsIds;
    collsIds.insert(coll.getId());
    PhotoCollectionHelper::checkCollectionsContainsItems(collsIds, photosWithNoParent, set<long>(), false);
    CHECK(photosWithNoParent.size() == expectedPhotosWithoutParent.size());
    CHECK(photosWithNoParent == expectedPhotosWithoutParent);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO_METHOD(PhotoCollectionDAOFixture,
    "Create PhotoCollection in some PhotoContext",TEST_TAG){

GIVEN("some PhotoContext on db"){
    PhotoContext ctx = insertPhotoContext();

WHEN("creating some PhotoCollection with that PhotoContext as parent"){
    PhotoCollection photoColl = createCollection();
    photoColl.setParentContext(ctx.getId());
    long photoCollId = Data::instance().collections().insert(photoColl);

THEN("the created PhotoCollection on db should contains that PhotoContext as parent"){
    PhotoCollection dbPhotoColl = Data::instance().collections().get(photoCollId);
    CHECK(dbPhotoColl.getParentContext() == photoCollId);

}//THEN
}//GIVEN
}//GIVEN

}//SCENARIO


SCENARIO_METHOD(Fixture, "Get all PhotoCollections of some PhotoContext",TEST_TAG){

GIVEN("some PhotoContext on db"){
    PhotoContext ctx = insertPhotoContext();

GIVEN("some PhotoCollections on that PhotoContext"){
    set<long> collIds = PhotoCollectionHelper::insertCollections(
                                               Test::Constants::COLLECTIONS_COUNT,-1, ctx.getId());

    //inserting other non-related collections
    PhotoCollectionHelper::insertCollections(Test::Constants::COLLECTIONS_COUNT);

WHEN("getting the PhotoCollections, passing that PhotoContext id"){
    PhotoCollectionDAO::CollectionList ctxCollections;
    getDAO().getByContext(ctx.getId(), ctxCollections);

THEN("a collection with all PhotoCollections of that context should be returned"){
    CHECK(collIds.size() == ctxCollections.size());
    checkContainsPhotoCollections(ctxCollections, collIds);

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO
