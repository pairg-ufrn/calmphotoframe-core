#include "catch.hpp"

#include "Environment.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "data/data-sqlite/SqliteData.h"
#include "utils/ScopedFile.h"
#include "utils/FileUtils.h"

#include <string>

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

namespace Constants {
    static const char * DbPath = "build/";
    static const char * DbName = "otherName.db";
}

void checkDatabasePath(const string & dbDir, const string & dbName, const string & expectedPath){
    INFO("expected path is: " << expectedPath);
    CHECK(FileUtils::instance().exists(expectedPath) == false);
    SqliteDataBuilder dataBuilder(dbDir, dbName);
    SqliteData * sqliteData = dynamic_cast<SqliteData *>(dataBuilder.buildData());

    REQUIRE(sqliteData != NULL);
    CHECK(FileUtils::instance().absolutePath(sqliteData->getDatabasePath())
            ==
          FileUtils::instance().absolutePath(expectedPath));

    CHECK(FileUtils::instance().remove(sqliteData->getDatabasePath()) == true);

    delete sqliteData;
}

#define TEST_TAG "[SqliteDataBuilder]"

TEST_CASE("Generate database path", TEST_TAG){
    const string & dbDir = ::Constants::DbPath;
    const string & dbName = ::Constants::DbName;
    const string & defaultDbName = Environment::instance().getDataFilenameDefault();

    checkDatabasePath(dbDir, dbName, FileUtils::instance().joinPaths(dbDir, dbName));
    checkDatabasePath(dbDir, string(), FileUtils::instance().joinPaths(dbDir, defaultDbName));
}
