#include "TestMacros.h"
#include "helpers/UserHelper.h"
#include "helpers/PhotoContextHelper.h"

#include "utils/ScopedDatabase.h"
#include "utils/ScopedFile.h"

#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/dao/SqlitePermissionDAO.h"
#include "data/data-sqlite/tables/PermissionsTable.h"
#include "data/DataTransaction.h"
#include "utils/FileUtils.h"

using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

class PermissionDaoFixture{
public:
    void testTableCreation();
    void testPutPermission();
    void testListContextsWithPermission();
    void testListPermissions();
    void testListPermissionsByContext();
public:
    ScopedSqliteDatabase scopedDb;

    SqlitePermissionDAO & getDao(){
        SqliteData & data = static_cast<SqliteData &>(Data::instance());
        return static_cast<SqlitePermissionDAO &>(data.permissions());
    }

    void insertPermissions(const std::vector<Permission> & permissions) const;
    void checkEquals(const Permission & expectedP, const Permission & responseP);
};

#define TEST_TAG "[PermissionDao]"
#define FIXTURE PermissionDaoFixture

/* ********************************************* Tests definition **********************************************/

FIXTURE_SCENARIO("Permission table creation"){
    this->testTableCreation();
}

FIXTURE_SCENARIO("Put user permission at context"){
    this->testPutPermission();
}

FIXTURE_SCENARIO("List ids of contexts that a user has some permission level"){
    this->testListContextsWithPermission();
}

FIXTURE_SCENARIO("List permissions of a given user"){
    this->testListPermissions();
}

FIXTURE_SCENARIO("List permissions with a given context"){
    this->testListPermissionsByContext();
}

/* ***************************************** Test cases implementation *****************************************/

void PermissionDaoFixture::testTableCreation(){

GIVEN("a SqliteConnector"){
    ScopedFile dbFile(Files().generateTmpFilepath() + ".db");
    SqliteConnector connector(dbFile.getFilepath());


WHEN("creating table of a SqlitePermissionDAO instance"){
    SqlitePermissionDAO dao(&connector);

    REQUIRE(!connector.existTable(Tables::PermissionsTable::NAME));
    CHECK(dao.createTableIfNotExist(connector));

THEN("a new Permission Table should be created on database"){
    CHECK(connector.existTable(Tables::PermissionsTable::NAME));

}//THEN
}//WHEN
}//GIVEN

}

void PermissionDaoFixture::testPutPermission(){

GIVEN("an user and a context id"){
    PhotoContext ctx = PhotoContextHelper::insertContext();
    UserAccount account = UserHelper::createAccount();

    long ctxId = ctx.getId(), userId = account.getUser().getId();

WHEN("putting some permission level to that user on the given context"){
    getDao().putPermission({ctxId, userId, PermissionLevel::Write});

THEN("the user permisson should be set to that level on database"){
    Permission permission = getDao().getPermission(userId, ctxId);
    CHECK(permission.getPermissionLevel() == PermissionLevel::Write);

}//WHEN
}//WHEN
}//GIVEN

}//test

void PermissionDaoFixture::testListContextsWithPermission(){

GIVEN("some photo contexts on db"){
    auto contexts = PhotoContextHelper::insertContexts(5);

GIVEN("some user with different permission levels on those contexts"){
    UserAccount account = UserHelper::createAccount();

    REQUIRE(Data::instance().users().exist(account.getId()));

    const PermissionLevel filterPermission = PermissionLevel::Write;
    SetId expectedContextIds;

    for(int i=0; i < contexts.size(); ++i){
        int permissionLevel = i % ((int)PermissionLevel::Admin + 1);
        long ctxId = contexts[i].getId();

        REQUIRE(Data::instance().contexts().exist(ctxId));

        if(permissionLevel >= (int)filterPermission){
            expectedContextIds.insert(ctxId);
        }

        INFO("CtxId: " << ctxId << " accountId: " << account.getId() << " permissionLevel: " << permissionLevel);
        getDao().putPermission({ctxId, account.getId(), (PermissionLevel)permissionLevel});
    }

    REQUIRE(!expectedContextIds.empty());

WHEN("using a PermissionDAO to list the contexts that this user has permission to access"){
    auto response = getDao().listContextsWithPermission(account.getId(), filterPermission);

THEN("each context ids for which this user has the given permission (or greater) should be returned on a set"){
    CHECK(expectedContextIds.size() == response.size());
    CHECK(expectedContextIds == response);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PermissionDaoFixture::testListPermissions(){

GIVEN("some permissions on database"){
    auto ctxs = PhotoContextHelper::insertContexts(3);
    auto userAccount = UserHelper::createAccount();
    long userId = userAccount.getId();
    long otherUser = UserHelper::createAccount("other login").getId();

    std::vector<Permission> expectedPermissions = {
        {ctxs[0].getId(), userId, PermissionLevel::Write},
        {ctxs[1].getId(), userId, PermissionLevel::Admin}
    };

    insertPermissions(expectedPermissions);
    //others
    insertPermissions({
        {ctxs[0].getId(), otherUser, PermissionLevel::Admin},
        {ctxs[1].getId(), otherUser, PermissionLevel::Write},
        {ctxs[2].getId(), userId   , PermissionLevel::Read}
    });

GIVEN("some user id and some permission level"){
    PermissionLevel minLevel = PermissionLevel::Write;

WHEN("using them to list permissions from a PermissionsDAO"){
    auto response = Data::instance().permissions().list(userId, minLevel);

THEN("it should respond with all Permissions associated with that user and with at least that permission level"){
    std::sort(response.begin(), response.end());
    std::sort(expectedPermissions.begin(), expectedPermissions.end());
    CHECK(response.size() == expectedPermissions.size());
    for(int i=0; i < response.size(); ++i){
        CAPTURE(i);
        Permission & responseP = response[i];
        Permission & expectedP = expectedPermissions[i];

        checkEquals(responseP, expectedP);
    }
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PermissionDaoFixture::testListPermissionsByContext(){

GIVEN("some permissions associated with some contexts"){
    auto accounts = UserHelper::createAccounts(3);
    auto contexts = PhotoContextHelper::insertContexts(3);

    long ctxId = contexts[0].getId();
    std::vector<Permission> expectedPermissions, others;
    for(auto account : accounts){
        expectedPermissions.push_back(Permission(ctxId, account.getId(), PermissionLevel::Read));
        for(int i=1; i < contexts.size(); ++i){
            others.push_back(Permission(contexts[i].getId(), account.getId(), PermissionLevel::Read));
        }
    }
    insertPermissions(expectedPermissions);
    insertPermissions(others);

WHEN("listing the permissions of one context using a PermissionDAO instance"){
    auto permissions = Data::instance().permissions().listByContext(ctxId);

THEN("all permissions related to that context should be returned"){
    REQUIRE(permissions.size() == expectedPermissions.size());

    std::sort(permissions.begin(), permissions.end());
    std::sort(expectedPermissions.begin(), expectedPermissions.end());
    for(size_t i=0; i < permissions.size(); ++i){
        checkEquals(expectedPermissions[i], permissions[i]);
    }
}//THEN
}//WHEN
}//GIVEN

}//test


/* ***************************************** Fixture implementation *****************************************/

void PermissionDaoFixture::insertPermissions(const std::vector<Permission> & permissions) const{
    DataTransaction transaction(Data::instance());

    for(auto & p : permissions){
        Data::instance().permissions().putPermission(p);
    }

    transaction.commit();
}

void PermissionDaoFixture::checkEquals(const Permission & lhs, const Permission & rhs)
{
    CHECK(rhs.getContextId()        == lhs.getContextId());
    CHECK(rhs.getUserId()           == lhs.getUserId());
    CHECK(rhs.getPermissionLevel()  == lhs.getPermissionLevel());
}
