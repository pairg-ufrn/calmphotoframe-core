#include "TestMacros.h"

#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/QueryArgs.h"
#include "utils/StringUtils.h"

using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace std;

namespace Constants {
    static const string COL1 = "c1";
    static const string COL2 = "c2";
}//namespace

class TableFixture{
public:
    void testMultiplePrimaryKeys();
public:
    string tableName;

    string selectAll;
    string whereCond, whereExpr;
    string selectWithColumns;

    Table table;
    vector<string> columns;

    TableFixture();

    string queryStatement(const QueryArgs & args=QueryArgs());
    string queryStatement(const std::vector<std::string> & columns, const QueryArgs & args);
};

#define FIXTURE TableFixture
#define TEST_TAG "[Table]"

using Constants::COL1;
using Constants::COL2;

FIXTURE_SCENARIO("Table can generate query statements from QueryArgs"){
    CHECK(queryStatement()                                  == selectAll);
    CHECK(queryStatement(QueryArgs(whereCond))              == selectAll + whereExpr);
    CHECK(queryStatement(columns, QueryArgs(whereCond))     == selectWithColumns + whereExpr);
    CHECK(queryStatement(QueryArgs("", 1))                  == selectAll + " LIMIT 1");
    CHECK(queryStatement(QueryArgs("", 0, COL1))            == selectAll + " ORDER BY " + COL1);
    CHECK(queryStatement(QueryArgs("", 0, COL1
                                        , QueryArgs::DESC)) == selectAll + " ORDER BY " + COL1 + " DESC");
    CHECK(queryStatement(QueryArgs("", 1, COL2
                                        , QueryArgs::ASC))  == selectAll + " ORDER BY " + COL2 + " ASC LIMIT 1");

}

FIXTURE_SCENARIO("Multiple primary keys"){
    this->testMultiplePrimaryKeys();
}


void TableFixture::testMultiplePrimaryKeys(){

WHEN("adding more than one column with primary key constraint on a given table"){
    Table table("t");
    table.putColumn(Column(COL1, ColumnTypes::INTEGER).addConstraint(ColumnConstraints::PRIMARY_KEY()));
    table.putColumn(Column(COL2, ColumnTypes::INTEGER).addConstraint(ColumnConstraints::PRIMARY_KEY()));

THEN("the primary keys set should contains those primary key columns"){
    auto && PKs = table.getPKs();

    CHECK(PKs.size() == 2);
    CHECK(PKs.find(COL1) != PKs.end());
    CHECK(PKs.find(COL2) != PKs.end());

THEN("the generated create statement should contains a composed PRIMARY_KEY constraint at end"){
    string createTableStmt = table.getCreateStatement();
    CAPTURE(createTableStmt);

    string composedPk = string(ColumnConstraints::PRIMARY_KEY().getName())
                            .append("(").append(COL1).append(", ").append(COL2).append(")");
    CHECK(createTableStmt.find(composedPk) != string::npos);

THEN("the PRIMARY_KEY keyword should be omitted from each column"){
    int countPks = StringUtils::instance()
                    .countOcurrences(createTableStmt, ColumnConstraints::PRIMARY_KEY().getName());
    CHECK(countPks == 1);

}//THEN
}//THEN
}//THEN
}//GIVEN
}//test

TableFixture::TableFixture()
    : tableName("Table")
    , selectAll("SELECT * FROM " + tableName)
    , whereCond("a=b")
    , whereExpr(" WHERE " + whereCond)
    , selectWithColumns("SELECT " + Constants::COL1 + " FROM " + tableName)
{
    columns.push_back(Constants::COL1);

    table.setName(tableName);
    table.putColumn(Column(Constants::COL1, ColumnTypes::INTEGER));
    table.putColumn(Column(Constants::COL2, ColumnTypes::TEXT));
}

string TableFixture::queryStatement(const QueryArgs & args){
    return queryStatement(vector<string>(), args);
}

string TableFixture::queryStatement(const std::vector<string> & columns, const QueryArgs & args){
    return table.getQueryStatement(columns, args);
}
