#include "TestMacros.h"

#include "data/DataTransaction.h"

#include "data/Data.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"

#include "utils/ScopedDatabase.h"

using namespace calmframe::data;

class DataTransactionFixture{
    ScopedSqliteDatabase scopedDb{ScopedSqliteDatabase::DbType::InMemory};
public:
    void testDataTransactionRAII();
};

#define TEST_TAG "[DataTransaction]"
#define FIXTURE DataTransactionFixture

TEST_SCENARIO("DataTransaction should follow RAII") IS_DEFINED_BY(testDataTransactionRAII)

void DataTransactionFixture::testDataTransactionRAII(){
    GIVEN("some Data instance that supports transactions"){
        Data & data = Data::instance();
        REQUIRE(data.allowTransactions());
    
    GIVEN("that it is not on transaction"){
        REQUIRE(!data.isOnTransaction());
    
    WHEN("creating a DataTransaction using that Data instance"){
        {//reduced scope
    
            DataTransaction dataTransaction(data);
            THEN("a transaction should be started"){
                CHECK(data.isOnTransaction());
            }//THEN
    
        }//reduced scope
    
    AND_WHEN("the DataTransaction is destroyed"){
    THEN("the transaction should be finished"){
        CHECK(!data.isOnTransaction());
    }//THEN
    }//WHEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}
