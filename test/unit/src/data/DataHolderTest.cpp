#include "catch.hpp"

#include "data/DataHolder.h"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"
#include "utils/IllegalStateException.h"

#include "exceptions/DatabaseException.h"

using namespace calmframe;
using namespace calmframe::data;
using namespace CommonExceptions;

class TestData : public MemoryData{
public:
    static int createCount, destroyCount;
    static void resetCounters(){
        createCount = destroyCount = 0;
    }

public:
    TestData()
    {
        ++createCount;
    }
    ~TestData(){
        ++destroyCount;
    }
};
int TestData::createCount = 0;
int TestData::destroyCount = 0;

class TestDataBuilder : public MemoryDataBuilder{
    bool * destroyed;
public:
    TestDataBuilder(bool * destroyedPtr)
        : destroyed(destroyedPtr)
    {
        *destroyed = false;
    }
    ~TestDataBuilder(){
        *destroyed = true;
    }
    virtual DataBuilder * clone() const{
        return new TestDataBuilder(this->destroyed);
    }
    virtual Data * buildData(){
        return new TestData();
    }
};

SCENARIO("Change DataBuilder on DataHolder", "[DataHolder]"){
GIVEN("Some DataHolder instance"){
    TestData::resetCounters();

    DataHolder dataHolder;

GIVEN("Some DataBuilder shared_ptr"){
    bool destroyed = false;
    std::shared_ptr<DataBuilder> dataBuilder = std::make_shared<TestDataBuilder>(&destroyed);

WHEN("Setting the databuilder"){
    CHECK(dataHolder.getDataBuilder() == NULL);
    dataHolder.setDataBuilder(dataBuilder);

THEN("the dataBuilder should be copied"){
    CHECK(dataHolder.getDataBuilder() == dataBuilder.get());

AND_THEN("it should allow get a valid Data instance"){
    CHECK(TestData::createCount == 0);
    CHECK(TestData::destroyCount == 0);
    Data * createdData = dataHolder.getData();
    CHECK(createdData != NULL);
    CHECK(TestData::createCount == 1);
    CHECK(TestData::destroyCount == 0);

    AND_WHEN("setting another dataBuilder"){
        const DataBuilder * oldDataBuilder = dataHolder.getDataBuilder();

        bool destroyed2;
        std::shared_ptr<DataBuilder> dataBuilder2 = std::make_shared<TestDataBuilder>(&destroyed2);
        dataHolder.setDataBuilder(dataBuilder2);

    AND_WHEN("other references to the pointer are destroyed"){
        CHECK_FALSE(destroyed);
        dataBuilder.reset();

    THEN("the first clone should be destroyed"){
        CHECK(destroyed == true);

    AND_THEN("the new builder should be copied"){
        CHECK(oldDataBuilder != dataHolder.getDataBuilder());
        CHECK(dataHolder.getDataBuilder() == dataBuilder2.get());

        AND_WHEN("getting a data instance again"){
            Data * newData = dataHolder.getData();
        THEN("it should delete the previous data"){
            CHECK(TestData::destroyCount == 1);
        AND_THEN("it should create a new data instance"){
            CHECK(newData != createdData);
            CHECK(TestData::createCount == 2);
        }
        }//THEN
        }//WHEN
    }//THEN
    }//THEN
    }//WHEN
    }
}
}//THEN
}//WHEN
}//GIVEN
}
}

SCENARIO("DataHolder should keep the same Data instance between calls","[DataHolder]"){
GIVEN("a DataHolder setup with some data builder"){
    TestData::resetCounters();
    bool destroyed;
    std::shared_ptr<DataBuilder> dataBuilder = std::make_shared<TestDataBuilder>(&destroyed);
    DataHolder dataHolder;
    dataHolder.setDataBuilder(dataBuilder);
WHEN("calling getData"){
    Data * dataInstance = dataHolder.getData();
THEN("it should build a data instance"){
    CHECK(dataInstance != NULL);
AND_WHEN("calling getData again"){
    Data * otherData = dataHolder.getData();
THEN("the same data instance should be returned"){
    CHECK(dataInstance == otherData);
}
}
}
}
}
}//SCENARIO



SCENARIO("Trying to get data before set builder","[DataHolder]"){
GIVEN("a DataHolder instance with no builder"){
    DataHolder dataHolder;
WHEN("trying to get data instance"){
THEN("it should throw a DatabaseCreationException"){
    CHECK_THROWS_AS(dataHolder.getData(), DatabaseCreationException);
}
}
}//GIVEN
}
