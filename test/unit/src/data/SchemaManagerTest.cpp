#include "TestMacros.h"

#include "data/data-sqlite/SchemaManager.h"

#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/tables/MarkersTable.h"
#include "data/data-sqlite/tables/PhotoTable.h"
#include "utils/ScopedDatabase.h"

#define TEST_TAG "[SchemaManager]"
#define FIXTURE SchemaManagerFixture

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace Tables;
using namespace Tables::MarkersTable;

class SchemaManagerFixture{
public:
    void testReadTable();

public:
    SchemaManagerFixture();

public:
    ScopedSqliteDatabase scopedDb;
    SchemaManager schemaManager;
};

/******************************************************************************************/

FIXTURE_SCENARIO("SchemaManager can read a Table data from sqlite"){
    testReadTable();
}


/******************************************************************************************/

void SchemaManagerFixture::testReadTable(){

GIVEN("some table on database"){
    string tableName = MarkersTable::NAME;
    ColumnList expectedColumns{
        Columns::PHOTO_ID, Columns::INDEX, Columns::CREATED_AT, Columns::IDENTIFICATION,
        Columns::RELATIVEX, Columns::RELATIVEY, Columns::UPDATED_AT};

WHEN("reading it using a SchemaManager instance"){
    Table t = schemaManager.readTable(tableName);

THEN("all columns should be retrieved"){
    for(auto&& columnName : expectedColumns){
        CHECK(t.getColumn(columnName).getName() == columnName);
    }

THEN("ForeignKeys should be preserved"){
    CHECK(t.getForeignKey(Columns::PHOTO_ID).getForeignTable() == Tables::PhotoTable::NAME);

}//THEN
}//THEN
}//WHEN
}//GIVEN
}//test

/******************************************************************************************/

SchemaManagerFixture::SchemaManagerFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
    , schemaManager(&((SqliteData&)Data::instance()).getDatabaseConnector())
{}
