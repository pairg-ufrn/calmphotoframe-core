#include <catch.hpp>

#include <string>

#include "utils/Value.h"
#include "model/photo/Photo.h"
using namespace std;

class testClass{

};

#include <iostream>
#include <typeinfo>
#include <cstring>

SCENARIO("Storing and Retriving generic values", "[Value]"){

    GIVEN("Some ValueHolders"){
        GenericValueHolder<std::string> valueHolder(string("someString"));
        GenericValueHolder<int> otherHolder(100);
        GenericValueHolder<float> realHolder(0.5f);
        GenericValueHolder<bool> boolHolder(true);
        GenericValueHolder<testClass> classHolder = GenericValueHolder<testClass>(testClass());

        WHEN("calling isTypeOf"){
            THEN("it should return true if the same of created class"){
                CHECK( boolHolder.isTypeOf<bool>());
                CHECK( !classHolder.isTypeOf<std::string>());
                CHECK( classHolder.isTypeOf<testClass>());
            }
        }

        AND_WHEN("testing if value is number, primite or real"){

            THEN("it should return value based on created class type"){
                CHECK(!valueHolder.isNumber());
                CHECK(!valueHolder.isPrimitive());
                CHECK(!valueHolder.isReal());
                CHECK(otherHolder.isNumber());
                CHECK(otherHolder.isPrimitive());
                CHECK(!otherHolder.isReal());
                CHECK(realHolder.isNumber());
                CHECK(realHolder.isPrimitive());
                CHECK(realHolder.isReal());
            }
        }
    }

    GIVEN("some primitive values"){
        bool  boolOriginValue  = false;
        int   intOriginValue   = 100;
        float floatOriginValue = 0.5f;
        char  charOriginValue  = 'b';
        WHEN("create a Value instance"){

            Value boolValue = Value(boolOriginValue);
            Value intValue = Value(intOriginValue);
            Value floatValue = Value(floatOriginValue);
            Value charValue = Value(charOriginValue);
            AND_WHEN("calling getTypeId"){
                THEN("it should return the same type of the primitive type"){
                    CHECK(boolValue.getTypeId()  == typeid(bool));
                    CHECK(intValue.getTypeId()   == typeid(int));
                    CHECK(floatValue.getTypeId() == typeid(float));
                    CHECK(charValue.getTypeId()  == typeid(char));
                }
            }
            AND_WHEN("calling getValue"){
                THEN("it should return the same initial value"){
                    CHECK(boolValue.getValue(!boolOriginValue) == boolOriginValue);
                    CHECK(intValue.getValue(intOriginValue - 100) == intOriginValue);
                    CHECK(floatValue.getValue(0.5f) == floatOriginValue);
                    CHECK(charValue.getValue('d') == charOriginValue);
                }
            }
            AND_WHEN("copying some value"){
                Value copyValue = Value(boolValue);
                THEN("it should return the same value of the copied instance"){
                    CHECK(copyValue.getValue<bool>() == boolOriginValue);
                }
            }

        }
        WHEN("create a Value with default constructor"){
            Value emptyValue;
            THEN("it should contain an invalid value"){
                CHECK(emptyValue.isEmpty());

                WHEN("assigning to it a new not null value"){
                    emptyValue = Value(true);

                    THEN("it should not be null anymore"){
                        CHECK(!emptyValue.isEmpty());
                        CHECK(emptyValue.isTypeOf<bool>());
                    }
                }
            }
        }

        WHEN("assigning an primitive type to a Value"){
            Value value;
            value = boolOriginValue;
            THEN("it should contains the value and type of that primitive"){
                CHECK(value.isTypeOf<bool>());
                CHECK(value.getValue<bool>() == boolOriginValue);
            }
        AND_WHEN("assigning an object to a Value"){
            value = string("str");
            THEN("it should also contains the value and type of that object"){
                CHECK(value.isTypeOf<string>());
                CHECK(value.getValue<string>() == string("str"));
            }
        }
        }

        WHEN("calling 'as' template method"){
            Value someValue = Value(100);
            THEN("it should not allow conversion between different types"){
                CHECK(someValue.isTypeOf<int>());
                CHECK(!someValue.isTypeOf<long>());
                CHECK_THROWS(someValue.as<long>());
            }
        }
        WHEN("calling 'coerce'"){
            Value someValue = "asdasd";
            THEN("There is no exception when converting to different types!! (this may not be safe!!)"){
                int unknown = someValue.coerceTo<int>();
            }
        }
        WHEN("putting a pointer"){
            const char * cstr = "cstr";
            Value charPointerValue(cstr);

            THEN("it should allow to return a pointer of same type"){
                CHECK(charPointerValue.isTypeOf<const char *>());
                const char * otherPtr = charPointerValue.getValue<const char *>();
                CHECK(cstr == otherPtr);
            }
        }
        WHEN("creating a cstring value"){
            Value value = "cstr";

        THEN("it should convert to a const char pointer"){
            CHECK(value.isTypeOf<const char *>());
        AND_THEN("the string should be the same"){
            const char * otherPtr = value.getValue<const char *>();
            CHECK(strcmp("cstr", otherPtr) == 0);
        }//THEN
        }//THEN
        }//WHEN

        WHEN("putting an array"){
            int someInts[] = {1,2,3,4};
            Value intsValue(someInts);

            THEN("it should convert to an pointer to that type"){
                CHECK(intsValue.isTypeOf<int *>());
                CHECK(typeid(someInts) == typeid(int [4]));
                CHECK(!intsValue.isTypeOf<int [4]>());

                int * arrAsPointer = intsValue.getValue<int *>();
                CHECK(someInts == arrAsPointer);

            AND_THEN("should allow get the same values"){
                for(int i=0; i < 4; ++i){
                    CHECK(someInts[i] == arrAsPointer[i]);
                }
            }
            }
        }
    }
}

