#include "catch.hpp"

#include "utils/FileUtils.h"
#include "utils/IOException.h"
#include <string>

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;
using namespace CommonExceptions;

namespace Constants{
    const char * dirToCreate = "test/data/output";
    const char * existentDir = "test/data";
    const char * baseHierarchyToCreate = existentDir;
    const char *const hierarchyPaths[] = {"output","hierarchy","test"};
    const int hierarchySize = 3;

    const char * inexistentPath = "inexistent/path";

}

class CreateDirectoryIfNotExistsFixture{
public:
    void testDirCreation(const std::string & dir){
        bool alreadyExist = FileUtils::instance().exists(dir);
        FileUtils::instance().createDirIfNotExists(dir);
        CHECK(FileUtils::instance().exists(dir));
        if(!alreadyExist){
            FileUtils::instance().remove(dir);
        }
        CHECK(FileUtils::instance().exists(dir) == alreadyExist);
    }
    void testDirHierarchyCreation(const std::string & baseDir, const char * const * paths, int numberOfPaths){
        vector<string> dirsToRemove;
        std::string finalDir = baseDir;
        for(int i=0; i < numberOfPaths; ++i){
            finalDir = FileUtils::instance().joinPaths(finalDir, paths[i]);
            dirsToRemove.push_back(finalDir);
        }
        INFO("Final Path: " << finalDir);
        FileUtils::instance().createDirIfNotExists(finalDir, true);

        CHECK(FileUtils::instance().exists(finalDir));
        for(unsigned i=dirsToRemove.size(); i > 0; --i){
            bool removed = FileUtils::instance().remove(dirsToRemove[i-1]);
            CHECK(removed);
        }
    }
};

#define TEST_TAG "[FileUtils]"

TEST_CASE("getExtension", TEST_TAG){
    CHECK(FileUtils::instance().getFilenameExtension("foo.bar") == string("bar"));
    CHECK(FileUtils::instance().getFilenameExtension("foo.bar.buzz") == string("buzz"));
    CHECK(FileUtils::instance().getFilenameExtension("foo_bar") == string(""));
    CHECK(FileUtils::instance().getFilenameExtension("/hsdkjahskd/asdasd/foo_bar.ext") == string("ext"));
    CHECK(FileUtils::instance().getFilenameExtension("./foo_bar.ext") == string("ext"));
    CHECK(FileUtils::instance().getFilenameExtension("./foo_bar") == string(""));
}

TEST_CASE("createDirectoryIfNotExists", TEST_TAG){
    CreateDirectoryIfNotExistsFixture fixture;

    fixture.testDirCreation(Constants::dirToCreate);
    fixture.testDirCreation(Constants::existentDir);
}
TEST_CASE("createDirectoryIfNotExists with parents", TEST_TAG){
    CreateDirectoryIfNotExistsFixture fixture;

    fixture.testDirHierarchyCreation(Constants::baseHierarchyToCreate, Constants::hierarchyPaths, Constants::hierarchySize);
}
TEST_CASE("createDirectoryIfNotExists fail should trow Exception", TEST_TAG){
    CreateDirectoryIfNotExistsFixture fixture;

    CHECK_THROWS_AS(fixture.testDirCreation(""), IOException);
    CHECK_THROWS_AS(fixture.testDirCreation(Constants::inexistentPath), IOException);
}

TEST_CASE("pathEquals should ignore the last path terminator", TEST_TAG){
    string pathWithoutTerminator = "/tmp/applicationDir";
    string pathWithTerminator = "/tmp/applicationDir/";

    CHECK(Files().pathEquals(pathWithTerminator, pathWithoutTerminator));
    CHECK(Files().pathEquals(pathWithoutTerminator, pathWithTerminator));
    CHECK(Files().pathEquals(pathWithoutTerminator, pathWithTerminator));
    CHECK(Files().pathEquals(pathWithTerminator, pathWithTerminator));
}

TEST_CASE("canonical path", TEST_TAG){
    auto currentPath = Files().getCurrentDir();
    if(currentPath.back() == '/' && currentPath != "/"){
        currentPath.pop_back();
    }
    
    auto data = vector<vector<string>>({
        {".", currentPath},
        {"/", "/"},
        {"/lorem/../ipsum", "/ipsum"},
        {"/lorem/ipsum/", "/lorem/ipsum"},
        {"/lorem/../ipsum/.", "/ipsum"},
        {"relative/path", Files().joinPaths(currentPath, "relative/path")}
    });
    
    for(auto p: data){
        const auto & input = p[0];
        auto canonical = Files().canonicalPath(input);
        const auto & expected = p[1];
        
        CAPTURE(input);
        CHECK(canonical == expected);
    }
}

//TEST_CASE("make relative path", "[FileUtils]"){
//    int length = 6;
//    string data[][3] = {
//            {"/a/b/c/"          , "/a"      , "b/c/"}
//           ,{"/a/b/c"           , "/d/e/f"  , "../../../a/b/c"}
//           ,{"/a/b/c/d"         , "/a/b/e"  , "../c/d"}
//           ,{"a/b/c"            , "b/c"     , "../../a/b/c"}
//           ,{"/a/b/c/file.txt"  , "/a/b/e"  , "../c/file.txt"}
//           ,{"a/b/file.txt"     , "b/c"     , "../../a/b/file.txt"}
//           ,{"/tmp/tmp24652aaaaaa.jpg"    , "/tmp/"    , "tmp24652aaaaaa.jpg"}};

//    for(int i=0; i < length; ++i){
//        const string & path = data[i][0];
//        const string & basePath = data[i][1];
//        const string & expectedPath = data[i][2];
//        INFO("path: '" << path << "'; base path: '" << basePath << "';"
//                    << " expected path: '" << expectedPath << "'" );
//        CHECK(Files().relativePath(path, basePath) == expectedPath);
//    }
//}
