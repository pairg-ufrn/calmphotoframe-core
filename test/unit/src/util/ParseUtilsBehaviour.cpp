#include "catch.hpp"

#include "utils/ParseUtils.h"

#include <string>

using namespace std;
using namespace calmframe;

SCENARIO("Parse from boolean string to bool", "[ParseUtils]"){
GIVEN("an boolean text representation"){
    string trueBool = "true", falseBool = "false";
WHEN("converting that to boolean"){
THEN("it should return the correspondent values"){
    CHECK(ParseUtils::instance().toBoolean(trueBool) == true);
    CHECK(ParseUtils::instance().toBoolean(falseBool) == false);
}
}
}
}

SCENARIO("Parse from incorrect boolean string to bool", "[ParseUtils]"){
GIVEN("an incorrect boolean text representation"){
    string randomText="asda", emptyText="", numberText = "1";
WHEN("converting the incorrect texto to boolean"){
THEN("it should return the value on default parameter"){
    CHECK(ParseUtils::instance().toBoolean(randomText) == false);
    CHECK(ParseUtils::instance().toBoolean(randomText, true) == true);

    CHECK(ParseUtils::instance().toBoolean(emptyText, false) == false);
    CHECK(ParseUtils::instance().toBoolean(emptyText, true) == true);

    CHECK(ParseUtils::instance().toBoolean(numberText, false) == false);
    CHECK(ParseUtils::instance().toBoolean(numberText, true) == true);
}
}
}
}
