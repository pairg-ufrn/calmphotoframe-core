#include "PhotoManagerTest.h"

#include "TestMacros.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>//time(), clock()

#include "mocks/data/data-memory/MemoryDataBuilder.hpp"

#include "model/photo/PhotoCollection.h"
#include "model/photo/PhotoMetadata.h"
#include "services/PhotoCollectionManager.h"
#include "services/image/FreeimageLibrary.h"
#include "services/image/MetadataExtractor.h"
#include "exceptions/NotFoundMember.h"

#include "utils/FileUtils.h"
#include "utils/StringCast.h"

#include "helpers/PhotoHelper.h"
#include "utils/ScopedFile.h"
#include "utils/ScopedDatabase.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/EventsHelper.h"

using namespace std;
using namespace calmframe::photo;
using namespace calmframe::utils;


FIXTURE_SCENARIO("PhotoManager allows retrieve photos")     { testGetPhotos();}

FIXTURE_SCENARIO("PhotoManager get photo"){
    testGetPhoto();
}

FIXTURE_SCENARIO("PhotoManager should not allow get invalid photo"){
    testTryGetInvalidPhoto();
}

SCENARIO("PhotoManager allows updating a photo's data", TEST_TAG "[UpdatePhoto]"){
    testUpdatePhoto();
}//SCENARIO

SCENARIO("PhotoManager should reject updates to Non-Existent photos", TEST_TAG "[UpdatePhoto]"){
    testRejectUpdateUnexistentPhoto();
}//SCENARIO

SCENARIO("PhotoManager allows getting a photo image", TEST_TAG){
    testGetPhotoImage();
}//SCENARIO

SCENARIO("PhotoManager allows insert a photo image", "[PhotoManager],[slow]"){
    testInsertImage();
}//SCENARIO


FIXTURE_SCENARIO("PhotoManager allows remove existing photos"){
    testRemovePhoto();
}//SCENARIO

SCENARIO("PhotoManager allows get an thumbnail from some photo image", TEST_TAG "[slow]"){
    testGetPhotoThumbnail();
}//SCENARIO

SCENARIO("Photo frame should preserve marker indexes", "[PhotoManager][slow]"){
    testPreserveMarkerIndexes();
}//SCENARIO

FIXTURE_SCENARIO("PhotoManager allows get photos from a given collection"){
    testGetPhotosFromCollection();
}//SCENARIO

FIXTURE_SCENARIO("PhotoManager allows get all photos from a given collection hierarchy"){
    testGetPhotosFromCollectionHierarchy();
}//SCENARIO

/* ***************************************** Test cases implementation *****************************************/

void PhotoManagerFixture::testGetPhotos(){

GIVEN("a PhotoManager instance and some photos on database"){
    PhotoHelper::insertPhotos();

WHEN("getPhotos is called"){
    vector<Photo> photos = photoManager.getPhotos();
THEN("all photos on database should be returned"){
    vector<Photo> allPhotos = Data::instance().photos().listAll();

    sort(photos.begin(), photos.end());
    sort(allPhotos.begin(), allPhotos.end());
    CHECK(photos == allPhotos);
}
}
}
}//test

void PhotoManagerFixture::testGetPhoto(){

GIVEN("some photo on db"){
    Photo expectedPhoto{PhotoDescription{"some description"}};
    long photoId = PhotoHelper::insertPhoto(expectedPhoto);
    expectedPhoto.setId(photoId);

WHEN("getPhoto is called with an existing id"){
    Photo photo = photoManager.getPhoto(photoId);

THEN("it should return the photo with that id"){
    CHECK(photo.getId() == photoId);
    CHECK(photo == expectedPhoto);
}
}
}//GIVEN

}//test

void PhotoManagerFixture::testTryGetInvalidPhoto(){

GIVEN("some photos on db"){
    PhotoHelper::insertPhotos(2);

WHEN("getPhoto is called with an unexisting id"){
THEN("it should throw an NotFoundMember exception"){
    CHECK_THROWS_AS(photoManager.getPhoto(NONEXISTENT_ID), NotFoundMember);
}
}
}//GIVEN

}//test

void testUpdatePhoto(){
GIVEN("a PhotoManager instance and an existing photo on database"){
    PhotoManagerFixture fixture;

    Photo photoToUpdate = PhotoHelper::createPhoto(EXISTING_ID);
    photoToUpdate.setImagePath("some/image/path");
    long photoId = Data::instance().photos().insert(photoToUpdate);


WHEN("updatePhoto is called passing a photo instance with an existent id"){
    Photo otherPhoto = PhotoHelper::createPhoto(photoId);
    otherPhoto.getDescription().setDescription("Any description value");
    Photo updatedPhoto = fixture.photoManager.updatePhoto(otherPhoto);

THEN("retrieve the updated value"){
    CHECK(otherPhoto.getId() == updatedPhoto.getId());
    CHECK(otherPhoto.getDescription().getDescription()
          == updatedPhoto.getDescription().getDescription());

    AND_THEN("the database should contain the updated value."){
        Photo fromDatabase;
        CHECK(Data::instance().photos().getPhoto(photoId, fromDatabase) == true);
        CHECK(fromDatabase == otherPhoto);
    }
}
}//WHEN
}//GIVEN
}

void testRejectUpdateUnexistentPhoto(){
GIVEN("a PhotoManager instance"){
    PhotoManagerFixture fixture;

    Photo nonExistentPhoto = PhotoHelper::createPhoto(NONEXISTENT_ID);
    nonExistentPhoto.getDescription().setDescription("Any description value");

WHEN("trying to update a photo with an unexistent id"){
THEN("it should throw a exception"){
    CHECK_THROWS_AS(fixture.photoManager.updatePhoto(nonExistentPhoto), NotFoundMember);
}
}//WHEN
}//GIVEN

}//test

void testGetPhotoImage(){
    PhotoManagerFixture fixture;

GIVEN("an existing photo on database with a valid image"){
    Photo photoToUpdate = PhotoHelper::createPhoto(-1);
    //Using absolute path to avoid copying files (because photomanager dir is set to another directory)
    photoToUpdate.setImagePath(Files().absolutePath(Constants::testImage));
    long photoId = Data::instance().photos().insert(photoToUpdate);

WHEN("calling getImage on a PhotoManager instance passing that photo id"){
    Image image = fixture.photoManager.getImage(photoId);

THEN("it should return a valid image"){
    CHECK(image.isValid());
}//THEN
}//WHEN
}//GIVEN
}//test

void testInsertImage(){
    PhotoManagerFixtureWithImage fixture;

GIVEN("a PhotoManager instance, with some image dir"){
    PhotoManager & photoManager = fixture.photoManager;
    photoManager.setImagesDir(Files().getDefaultTmpDir());

    INFO("photo frame image dir: " << fixture.photoManager.getImagesDir());
    REQUIRE(!fixture.photoManager.getImagesDir().empty());

GIVEN("an image with exif metadata"){
    INFO("imagePath: " << fixture.imagePath);
    REQUIRE(FileUtils::instance().exists(fixture.imagePath));
    REQUIRE(fixture.image.isValid());

WHEN("createPhoto is called passing an valid image instance and a photo collection id"){
    long collId = PhotoCollectionHelper::insertCollection();
    Photo photo = fixture.photoManager.createPhoto(fixture.image, collId);

THEN("it should return a valid photo"){
    REQUIRE(photo.getId() > 0);
    INFO("photoId: " << photo.getId());

AND_THEN("that image should be created on database"){
    CHECK(Data::instance().photos().exist(photo.getId()));

AND_THEN("the image should have been copied to the PhotoManager image directory"){
    string imageDir = fixture.photoManager.getImagesDir();

    CHECK(photo.getImagePath() != fixture.imagePath);

    Image image = photoManager.getImage(photo);

    INFO("Photo image path: '" << photo.getImagePath() << "'; image dir: '" << imageDir << "'"
            << "; Image path: '" << image.getImagePath() << "'");

    REQUIRE(image.isValid());

AND_THEN("the photo image path should be a relative path to the image dir"){
    CHECK(Files().isRelative(photo.getImagePath()));
    CHECK(Files().pathEquals(image.getImagePath()
                    , Files().joinPaths(photoManager.getImagesDir(), photo.getImagePath())));

THEN("the photo should be added to the passed PhotoCollection"){
    PhotoCollection rootCollection = Data::instance().collections().get(collId);
    CHECK(rootCollection.containsPhoto(photo.getId()));
}//THEN
}//THEN
}//THEN
}//THEN
}//THEN

    //Remove file
    FileUtils::instance().remove(photo.getImagePath());
    CHECK(!FileUtils::instance().exists(photo.getImagePath()));
}//WHEN
}//GIVEN
}//GIVEN
}//test

void testGetPhotoThumbnail(){
    //Generate a random dir to avoid conflicts
    string tmpImagesDir = Files().generateTmpFilepath(Files().getDefaultTmpDir());
    Files().createDirIfNotExists(tmpImagesDir);
    ScopedFile scopedImagesDir(tmpImagesDir, true);

    PhotoManagerFixture fixture;
    fixture.photoManager.setImagesDir(tmpImagesDir);
GIVEN("some created photo with image"){
    Photo photo = fixture.createPhotoWithImage();
    Image photoImage = fixture.photoManager.getImage(photo);
    REQUIRE(photoImage.isValid());

    INFO("Photo(" << photo.getId() << ") image path: '" << photo.getImagePath() << "'");

WHEN("get the photo thumbnail"){
    Image thumbnail = fixture.photoManager.getThumbnail(photo.getId(), Constants::thumbnailSize);

    INFO("Created thumbnail: '" << thumbnail.getImagePath() << "'");

THEN("the PhotoManager should respond with an valid image"){
    CHECK(thumbnail.isValid());

AND_THEN("the thumbnail should be created under the image dir"){
    CHECK(Files().isSubpath(thumbnail.getImagePath(), fixture.photoManager.getImagesDir()));

AND_WHEN("getting the thumbnail of the same photo, with the same size"){
    Image sameThumbnail = fixture.photoManager.getThumbnail(photo.getId(), Constants::thumbnailSize);
    INFO("Same thumbnail: \"" << sameThumbnail.getImagePath().c_str() << "\"");

THEN("it should return the same thumbnail"){
    INFO("First thumbnail path: " << thumbnail.getImagePath()
                << "; current thumbnail path: " << sameThumbnail.getImagePath());
    CHECK(Files().pathEquals(thumbnail.getImagePath(), sameThumbnail.getImagePath()));
}//THEN
}//WHEN
}//THEN
}//THEN
    //Remover thumbnail criada
    CHECK(Files().remove(thumbnail.getImagePath()));
}//WHEN
    CHECK(Files().remove(photoImage.getImagePath()));
}//GIVEN
}//test


void testPreserveMarkerIndexes(){

GIVEN("a PhotoManager instance"){
GIVEN("an valid image"){
    PhotoManagerFixtureWithImage fixture;

    CAPTURE(fixture.imagePath);
    REQUIRE(FileUtils::instance().exists(fixture.imagePath));
GIVEN("some photo description instance with a set of markers"){
    PhotoDescription photoDescription;
    vector<PhotoMarker> markers = fixture.createMarkersWithValidIndexes(5);
    photoDescription.setMarkers(markers);
    fixture.assertHasValidIndexes(markers);
WHEN("calling PhotoManager::createPhoto using that PhotoDescription instance" ){
    Photo createdPhoto = fixture.photoManager.createPhoto(fixture.image,photoDescription);
THEN("it should return an valid photo"){
    fixture.assertPhotoExists(createdPhoto);
AND_THEN("the created photo markers should keep the original indexes"){
    createdPhoto.getDescription().fixPhotoMarkers();
    const vector<PhotoMarker> & createdMarkers= createdPhoto.getDescription().getMarkers();
    fixture.assertHasValidIndexes(createdMarkers);
    fixture.assertEqualsMarkersCollection(createdMarkers, photoDescription.getMarkers());
WHEN("updating that photo with markers containing invalid indexes"){
    vector<PhotoMarker> markersWithInvalidIndexes = fixture.createMarkersWithInvalidIndexes(4);
    createdPhoto.getDescription().setMarkers(markersWithInvalidIndexes);
    Photo updatedPhoto = fixture.photoManager.updatePhoto(createdPhoto);
THEN("the indexes should have been fixed"){
    const vector<PhotoMarker> & updatedMarkers = updatedPhoto.getDescription().getMarkers();
    fixture.assertHasValidIndexes(updatedMarkers,false);
AND_THEN("they should keep the original order"){
    REQUIRE(markersWithInvalidIndexes.size() == updatedMarkers.size());
    std::sort(markersWithInvalidIndexes.begin(), markersWithInvalidIndexes.end());
    //std::sort(updatedMarkers.begin(), updatedMarkers.end());

    for(int i=0; i < updatedMarkers.size(); ++i){
        fixture.assertMarkersEquals(markersWithInvalidIndexes[i], updatedMarkers[i],true,false);
    }
}
}//THEN
}//WHEN
}//THEN
}//THEN
    //Remove file
    FileUtils::instance().remove(createdPhoto.getImagePath());
    CHECK(!FileUtils::instance().exists(createdPhoto.getImagePath()));
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//test

void PhotoManagerFixture::testRemovePhoto(){
GIVEN("an existing photo on database"){
GIVEN("that the photo is contained is some photo collection"){
    long collId = PhotoCollectionHelper::insertCollection();
    PhotoCollectionHelper::insertPhotosAtCollection(collId, 1);
    PhotoCollection coll = Data::instance().collections().get(collId);

    long photoId = *coll.getPhotos().begin();

WHEN("a PhotoManager instance remove that photo"){
    Photo removedPhoto = photoManager.deletePhoto(photoId);

THEN("it should not exist on database anymore"){
    CHECK(!Data::instance().photos().exist(photoId));

AND_THEN("no one PhotoCollection should contain it anymore"){
    PhotoCollection updatedColl = Data::instance().collections().get(collId);
    CHECK(!updatedColl.containsPhoto(photoId));

THEN("a new PhotoRemoved event should be created on database"){
    EventsHelper::checkEventCreated(EventType::PhotoRemove, removedPhoto.getId(), Event::InvalidUserId);
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//test

void PhotoManagerFixture::testGetPhotosFromCollection(){

GIVEN("an id of a PhotoCollection with photos"){
    PhotoCollection coll = PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(2, 0);
    long collId = coll.getId();

WHEN("using a PhotoManager instance to get the photos from that collection"){
    std::vector<Photo> photos = this->photoManager.getPhotosInCollection(collId);

THEN("all photos in that collection should be retrieved"){
    CHECK(photos.size() == coll.getPhotos().size());

    for(int i=0; i < photos.size(); ++i){
        long photoId = photos[i].getId();
        CAPTURE(photoId);
        CHECK(coll.containsPhoto(photoId));
    }
}//THEN
}//WHEN
}//GIVEN

}

void PhotoManagerFixture::testGetPhotosFromCollectionHierarchy(){

GIVEN("a photoCollection  with sub-collections that contains photos"){
    set<long> expectedPhotoIds;

    PhotoCollection coll = PhotoCollectionHelper::createPhotoHierarchy(2, 2, expectedPhotoIds);

WHEN("using a PhotoManager instance to get the photos from collection recursively"){
    vector<Photo> photos = photoManager.getPhotosInCollection(coll.getId(), true);

THEN("all photos from the parent collection and its children should be retrieved"){
    CHECK(photos.size() == expectedPhotoIds.size());

    for(int i=0; i < photos.size(); ++i){
        CHECK(expectedPhotoIds.find(photos[i].getId()) != expectedPhotoIds.end());
    }
}//THEN
}//WHEN
}//GIVEN

}//test

/* ***************************************** Fixture implementation *****************************************/


PhotoManagerFixture::PhotoManagerFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
{
    photoManager.setImagesDir(Files().getDefaultTmpDir());
}


bool PhotoManagerFixture::assertPhotoExists(const Photo & photo){

    INFO("photoId: " << photo.getId());
    INFO("photo path: " << photo.getImagePath());

    bool idIsValid = photo.getId() > 0;
    bool photoExistOnDatabase = Data::instance().photos().exist(photo.getId());

    Image img = photoManager.getImage(photo.getId());
    bool imageFileExist = FileUtils::instance().exists(img.getImagePath());
    CHECK(idIsValid);
    CHECK(photoExistOnDatabase);
    CHECK(imageFileExist);

    return idIsValid && photoExistOnDatabase && imageFileExist;
}

vector<PhotoMarker> PhotoManagerFixture::createMarkersWithValidIndexes(int count){
    return createMarkers(count, true);
}

vector<PhotoMarker> PhotoManagerFixture::createMarkersWithInvalidIndexes(int count){
    return createMarkers(count, false);
}

vector<PhotoMarker> PhotoManagerFixture::createMarkers(int count, bool validIndexes){
    vector<PhotoMarker> markers;
    markers.reserve(count);
    for(int i=0; i < count; ++i){
        markers.push_back(PhotoMarker("identification", i,i));
        int index = i +1;
        if(!validIndexes){
            //Pseudo-aleatoriza valores de índice
            //index = (time(0) + clock()) % count;
            index = count -(i + 1);
        }
        markers[i].setIndex(index);
    }

    return markers;
}

void PhotoManagerFixture::assertHasValidIndexes(const vector<PhotoMarker> & markers, bool require){
    for(int i=0; i < markers.size(); ++i){
        if(require){
            REQUIRE(markers[i].getIndex() == i + 1);
        }
        else{
            CHECK(markers[i].getIndex() == i + 1);
        }
    }
}

void PhotoManagerFixture::assertMarkersEquals(const PhotoMarker & markerA, const PhotoMarker & markerB, bool ignoreDatabaseIds, bool checkIndexes)
{
    if(!ignoreDatabaseIds){
        //            CHECK(markerA.getId() == markerB.getId());
        CHECK(markerA.getPhotoId() == markerB.getPhotoId());
    }
    if(checkIndexes){
        CHECK(markerA.getIndex() == markerB.getIndex());
    }
    CHECK(markerA.getName() == markerB.getName());
    CHECK(markerA.getRelativeX() == markerB.getRelativeX());
    CHECK(markerA.getRelativeX() == markerB.getRelativeY());
}

void PhotoManagerFixture::assertEqualsMarkersCollection(const vector<PhotoMarker> & markersA, const vector<PhotoMarker> & markersB){
    REQUIRE(markersA.size() == markersB.size());
    for(size_t i=0; i < markersA.size(); ++i){
        assertMarkersEquals(markersA[i], markersB[i], true);
    }
}

Photo PhotoManagerFixture::createPhotoWithImage(){
    Image image(Constants::testImage);
    return photoManager.createPhoto(image);
}

PhotoManagerFixtureWithImage::PhotoManagerFixtureWithImage()
    : imagePath(Constants::testImage)
    , image(imagePath)
{
}
