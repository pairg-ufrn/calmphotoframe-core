#ifndef PHOTOMANAGER_METADATATEST_H
#define PHOTOMANAGER_METADATATEST_H

#include "TestMacros.h"

#include "services/PhotoManager.h"
#include "model/photo/PhotoMetadata.h"

#include "utils/FileUtils.h"
#include "utils/StringCast.h"

#include "helpers/PhotoHelper.h"
#include "utils/ScopedFile.h"
#include "helpers/PhotoCollectionHelper.h"

#include "utils/ScopedDatabase.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;

#define TEST_TAG "[PhotoManager]"
#define FIXTURE PhotoManagerMetadataFixture

class PhotoManagerMetadataFixture{
public:
    void testExtractMetadataInCreation();

public:
    ScopedSqliteDatabase scopedDb;
    string imagePath;
    PhotoMetadata expectedMetadata;
    PhotoManager photoManager;

    PhotoManagerMetadataFixture();

    void setupMetadata();

    void assertMetadataEquals(const PhotoMetadata & metadata, const PhotoMetadata & expectedMetadata);
};


#define EXISTING_ID 1
#define NONEXISTENT_ID 10000
#define VALID_IMAGE_PATH "test/data/test_exif.jpg"

namespace Constants{
    static const char * imageWithMetadata = "test/data/test-images/test1.jpg";
    static const char * imageWithMetadata_maker         = "Apple";
    static const char * imageWithMetadata_model         = "iPhone 4S";
    //static const long imageWithMetadata_size            = 27521; //Este é o tamanho obtido pelo sistema
    static const long imageWithMetadata_size            = 23828;   //Este é o tamanho obtido pelas funções c++
    static const int imageWithMetadata_width            = 180;
    static const int imageWithMetadata_height           = 135;
    static const char * imageWithMetadata_date          = "2013:02:06 16:00:03";
    static const float imageWithMetadata_latitude       = 37.885000f;
    static const float imageWithMetadata_longitude      = -122.622500f;
    static const float imageWithMetadata_altitude       = 122.000000;
    static const Orientation::OrientationFlag imageWithMetadata_orientationFlag  = Orientation::TOP_LEFT;

    static const int thumbnailSize = 200;
}
#endif // PHOTOMANAGER_METADATATEST_H

