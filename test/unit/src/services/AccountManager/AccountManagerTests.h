#ifndef ACCOUNTMANAGERTESTS_H
#define ACCOUNTMANAGERTESTS_H

#include "TestMacros.h"
#include <fakeit.hpp>

#include "services/AccountManager.h"
#include "services/SessionManager.h"
#include "services/ImageManager.h"

#include "model/user/UserLoginInfo.h"
#include "model/image/Image.h"

#include "exceptions/NotFoundMember.h"

#include "utils/IllegalArgumentException.hpp"

#include "TestMacros.h"
#include "helpers/UserHelper.h"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"
#include "mocks/DataBuilderMockup.h"
#include "helpers/CaptureArguments.h"

#define FIXTURE AccountManagerFixture
#define TEST_TAG "[AccountManager]"

using namespace calmframe;
using namespace calmframe::data;
using namespace std;
using namespace fakeit;

class AccountManagerFixture{
public:
    using ImagePtr = ImageManager::ImagePtr;

    AccountManager accountManager;
    SessionManager sessionManager;

    UserAccount account, capturedAccount;
    std::string newPassword;

    DataBuilderMockup databuilderMockup;
    
    Mock<Image> imageMockup;
    Mock<ImageManager> imageManagerMockup;
    ImagePtr imagePtr, resultImagePtr;
    string saveImageResult, existingUserImage;
    
    CaptureArguments<bool, long, UserAccount> captureUpdateAccount;
    CaptureArguments<bool, User> captureUpdateUser;
    CaptureArguments<ImagePtr, std::string, unsigned> captureGetThumbnail;
    CaptureArguments<bool, std::string> captureRemoveImage;
    CaptureArguments<long, long, UserAccount> captureInsertAt;
    
    AccountManagerFixture(){
        Data::setup(std::make_shared<MemoryDataBuilder>());
    }
    
    ~AccountManagerFixture(){
        Data::destroyInstance();
    }

    string loginUser(const UserAccount & user, const string & password){
        return sessionManager.startSession(user, password).getToken();
    }
    
public:
    void testUpdateUserPassword();
    void testPutUserProfileImage();
    void testUpdateExistingUserProfileImage();
    void testGetUserProfileImage();
    void testGetUnexistentUserProfileImage();
    
    void testDisableUserAccount();
    
public://admin tests
    void testAdminCreation();
    
public:
    void using_mockups();
    void mocking_save_image();
    
    void using_image_manager_mockup();
    void using_database_mockup();
    long given_existent_account_mockup(const UserAccount & baseAccount={});
    void given_a_mocked_image_instance();
    void given_a_user_with_image_profile();
    void given_admin_user_does_not_exist();
    
    void when_updating_password();
    void when_updating_the_user_profile_image();
    void when_getting_the_user_profile_image();
    void when_accountManager_is_initialized();
    
    void then_account_pass_hash_should_match_new_pass();
    void then_account_password_should_be_updated();
    void then_image_manager_should_create_a_new_image();
    void then_user_profile_image_should_be_updated_on_db();
    void then_a_user_profile_thumbnail_should_be_get();
    void then_this_image_should_be_removed(const std::string & expectedImage);
    void then_account_should_have_been_activated();
    void then_the_admin_account_should_have_been_created();
    
    void check_invalid_creation(AccountManager & accountManager, const UserLoginInfo & loginInfo);
    void check_user_was_updated();
};

namespace Constants {
    const long UNEXISTENT_ID = 1000;
    const char * const INVALID_TOKEN = "any-string";
}//namespace

#endif // ACCOUNTMANAGERTESTS_H
