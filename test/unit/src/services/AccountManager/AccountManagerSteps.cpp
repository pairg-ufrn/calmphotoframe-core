#include "AccountManagerTests.h"

/**************************************** steps ***********************************************/

void AccountManagerFixture::using_mockups(){
    using_database_mockup();
    using_image_manager_mockup();
}


void AccountManagerFixture::using_database_mockup()
{
    databuilderMockup.setup().apply();    
    
    When(Method(databuilderMockup.users(), updateAccount))
        .AlwaysDo(captureUpdateAccount);
        
    When(Method(databuilderMockup.users(), update))
        .AlwaysDo(captureUpdateUser);
        
    When(ConstOverloadedMethod(databuilderMockup.users(), exist, bool(long)))
        .AlwaysReturn(true);
        
    When(Method(databuilderMockup.users(), insertAt))
        .AlwaysDo([this](long id, const UserAccount & account){
            captureInsertAt.capture(id, account);
            return id;
        });
        
    captureUpdateUser.returnValue(true);    
}

void AccountManagerFixture::using_image_manager_mockup()
{
    When(Dtor(imageManagerMockup));
    shared_ptr<ImageManager> managerPtr{&imageManagerMockup.get(), [](...){} };
    accountManager.imageManager(managerPtr);   
    
    When(Method(imageManagerMockup, removeImage))
        .AlwaysDo(captureRemoveImage.returnValue(true)); 
}

long AccountManagerFixture::given_existent_account_mockup(const UserAccount & baseAccount){
    account = baseAccount;
    if(account.getId() == UserAccount::INVALID_ID){
        account.setId(12345);
    }
    long userId = account.getId();
    
    When(Method(databuilderMockup.users(), get).Using(userId, _)).AlwaysDo(
        [this](long, UserAccount & out){
            out = this->account;
            return true;
    });
    When(Method(databuilderMockup.users(), getUser).Using(userId, _)).AlwaysDo(
        [this](long, User & out){
            out = this->account.getUser();
            return true;
    });
    
    return userId;
}

void AccountManagerFixture::given_a_mocked_image_instance(){
    mocking_save_image();
}

void AccountManagerFixture::mocking_save_image()
{
    When(Method(imageMockup, isValid)).AlwaysReturn(true);
    
    saveImageResult = "sampleImage.jpeg";
    
    When(Method(imageManagerMockup, saveImage))
            .AlwaysReturn(saveImageResult);    
}

void AccountManagerFixture::given_a_user_with_image_profile(){
    given_existent_account_mockup();
    
    existingUserImage = "anyimage.jpeg";
    account.getUser().profileImage(existingUserImage);
    
    imagePtr = std::make_shared<Image>("");
    When(Method(imageManagerMockup, getThumbnail))
            .Do(captureGetThumbnail.returnValue(imagePtr));
}

void AccountManagerFixture::given_admin_user_does_not_exist(){
    When(ConstOverloadedMethod(databuilderMockup.users(), exist, bool(long))
            ).Return(false);
            
    When(ConstOverloadedMethod(databuilderMockup.users(), exist, bool(const std::string & )))
        .Do([](const std::string & str){
            return false;
        });
}

void AccountManagerFixture::when_updating_password(){
    newPassword = "other password";
    accountManager.updatePassword(this->account.getId(), newPassword);    
}

void AccountManagerFixture::when_updating_the_user_profile_image(){
    accountManager.updateProfileImage(account.getId(), imageMockup.get());
}

void AccountManagerFixture::when_getting_the_user_profile_image(){
    accountManager.getProfileImage(account.getId());
}

void AccountManagerFixture::when_accountManager_is_initialized(){
    accountManager.initialize();
}

void AccountManagerFixture::then_account_password_should_be_updated()
{
    CHECK_NOTHROW(
        Verify(
            Method(databuilderMockup.users(), get)
        ));
        
    CHECK_NOTHROW(Verify(Method(databuilderMockup.users(), updateAccount)));
    
    CHECK(captureUpdateAccount.getLast<0>() == account.getId());
}

void AccountManagerFixture::then_image_manager_should_create_a_new_image(){
    CHECK_NOTHROW(
        Verify(Method(imageManagerMockup, saveImage))
    );
}

void AccountManagerFixture::then_user_profile_image_should_be_updated_on_db(){
    CHECK_NOTHROW(
        Verify(Method(databuilderMockup.users(), update))
    );
    
    User capturedUser = captureUpdateUser.getLast<0>();
    
    CHECK(capturedUser.profileImage() == saveImageResult);
}

void AccountManagerFixture::then_a_user_profile_thumbnail_should_be_get(){
    CHECK_NOTHROW(
        Verify(Method(imageManagerMockup, getThumbnail))
    );
    
    CHECK(captureGetThumbnail.getLast<0>() == account.getUser().profileImage());
}

void AccountManagerFixture::then_this_image_should_be_removed(const string & expectedImage){
    CHECK_NOTHROW(
        Verify(Method(imageManagerMockup, removeImage))
    );
    
    CHECK(captureRemoveImage.getLast<0>() == expectedImage);
}

void AccountManagerFixture::then_account_pass_hash_should_match_new_pass(){
    UserAccount capturedAccount = captureUpdateAccount.getLast<1>();

    string expectedPasswordHash = accountManager.generatePassHash(capturedAccount, newPassword);
    
    CHECK(capturedAccount.getPassHash() == expectedPasswordHash);
}

void AccountManagerFixture::then_account_should_have_been_activated(){
    check_user_was_updated();
    
    User capturedUser = captureUpdateUser.getLast<0>();
    CHECK_FALSE(capturedUser.isActive());    
}

void AccountManagerFixture::then_the_admin_account_should_have_been_created(){
    CHECK_NOTHROW(Verify(
                Method(databuilderMockup.users(), insertAt)
            ));    
                
    capturedAccount = captureInsertAt.getLast<1>();
    
    CHECK(capturedAccount.isAdmin());
}

void AccountManagerFixture::check_user_was_updated(){
    CHECK_NOTHROW(Verify(Method(databuilderMockup.users(), update)));    
}

void AccountManagerFixture::check_invalid_creation(AccountManager & accountManager, const UserLoginInfo & loginInfo){
    THEN("it should throw a IllegalArgumentException subclass"){\
        CHECK_THROWS_AS(accountManager.create(loginInfo), CommonExceptions::IllegalArgumentException);\
    }
}
