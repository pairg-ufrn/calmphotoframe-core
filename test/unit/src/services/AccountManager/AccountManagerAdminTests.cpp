#include "AccountManagerTests.h"

TEST_SCENARIO("User admin should be created on startup") IS_DEFINED_BY(testAdminCreation)


void AccountManagerFixture::testAdminCreation(){
    using_database_mockup();
    
    GIVEN("admin user does not exist"){
        this->given_admin_user_does_not_exist();
    
    WHEN("accountManager is initialized"){
        this->when_accountManager_is_initialized();
    
    THEN("the admin account should be created"){
        this->then_the_admin_account_should_have_been_created();
        
    THEN("admin should init with default password and login"){
        CHECK(capturedAccount.getLogin() == AccountManager::ROOT_LOGIN);
        
        auto expectedPasswordHash = AccountManager::generatePassHash(capturedAccount, AccountManager::ROOT_DEFAULT_PASS);
        CHECK(expectedPasswordHash == capturedAccount.getPassHash());
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}
