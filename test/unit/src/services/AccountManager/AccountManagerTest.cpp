#include "AccountManagerTests.h"


TEST_SCENARIO("AccountManager updates user password") IS_DEFINED_BY(testUpdateUserPassword)
TEST_SCENARIO("Save user profile image")              IS_DEFINED_BY(testPutUserProfileImage)
TEST_SCENARIO("Update existing user profile image")   IS_DEFINED_BY(testUpdateExistingUserProfileImage)
TEST_SCENARIO("Get user profile image")               IS_DEFINED_BY(testGetUserProfileImage)
TEST_SCENARIO("Get unexistent profile image")         IS_DEFINED_BY(testGetUnexistentUserProfileImage)

TEST_SCENARIO("Change account 'active' state")        IS_DEFINED_BY(testDisableUserAccount)

/**********************************************************************************************/

TEST_SCENARIO("Trying to create account with already existent login"){
    UserLoginInfo loginInfo("userName", "password");
    accountManager.create(loginInfo);
    
    check_invalid_creation(accountManager, loginInfo);
}//FIXTURE

TEST_SCENARIO("Trying to create account with empty login"){
    check_invalid_creation(accountManager, UserLoginInfo("", "blah"));
}//FIXTURE

FIXTURE_SCENARIO("Trying to create account with empty password"){
    check_invalid_creation(accountManager, UserLoginInfo("blah", ""));
}//FIXTURE

FIXTURE_SCENARIO("Trying to remove unexistent account"){
    WHEN("trying to remove a user that does not exist with a AccountManager"){
    THEN("it should throw an exception"){
        CHECK_THROWS(accountManager.removeUser(Constants::UNEXISTENT_ID));
    }//THEN
    }//WHEN
}//FIXTURE

FIXTURE_SCENARIO("Trying to update unexistent user"){
    WHEN("trying to update a user that does not exist with a AccountManager"){
        User user;
        user.setId(Constants::UNEXISTENT_ID);
    THEN("it should throw an NotFoundMember exception"){
        CHECK_THROWS_AS(accountManager.updateUser(user), NotFoundMember);
    }//THEN
    }//WHEN
}//SCENARIO

FIXTURE_SCENARIO("Get user id from session token"){
    GIVEN("a session token from a logged user"){
        UserAccount account = UserHelper::createAccount();
        string sessionToken = loginUser(account, DEFAULT_PASSWORD);

    WHEN("calling get user id in a AccountManager using that token"){
        long userId = accountManager.getSessionUser(sessionToken);

    THEN("the user id should be returned"){
        CHECK(userId == account.getId());

    }//THEN
    }//WHEN
    }//GIVEN
}

FIXTURE_SCENARIO("Get user id from invalid token"){
    WHEN("trying to get an user id from an token with no session"){
        long userId = accountManager.getSessionUser(Constants::INVALID_TOKEN);

    THEN("it should respond with the user INVALID_ID"){
        CHECK(userId == User::INVALID_ID);

    }//THEN
    }//WHEN
}//SCENARIO

FIXTURE_SCENARIO("Local user should be admin"){
    WHEN("getting a user id from a local session token"){
        string localSessionToken = sessionManager.startLocalSession("anySecret").getToken();
        long localUserId = accountManager.getSessionUser(localSessionToken);

    THEN("that user id should be equals to the 'local user id'"){
        CHECK(localUserId == SessionManager::LOCALUSER_ID);

    THEN("that user should be an admin"){
        User user = accountManager.getUser(localUserId);
        CHECK(user.isAdmin());

    }//THEN
    }//THEN
    }//WHEN

}//SCENARIO

/**********************************************************************************************/

void AccountManagerFixture::testUpdateUserPassword(){
    using_database_mockup();
    
    GIVEN("an existent account"){
        given_existent_account_mockup();
        
    WHEN("updating that user password"){
        when_updating_password();
    
    THEN("the account password and salt should have been updated"){
        then_account_password_should_be_updated();
    
    THEN("the new hash should correspond to given password"){
        then_account_pass_hash_should_match_new_pass();
    
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}

void AccountManagerFixture::testPutUserProfileImage(){
    using_mockups();
    
    GIVEN("an existent account"){
        given_existent_account_mockup();

    GIVEN("a valid image instance"){
        given_a_mocked_image_instance();
    
    WHEN("updating that user profile image"){
        when_updating_the_user_profile_image();
    
    THEN("the given image should be stored for that user"){
        then_image_manager_should_create_a_new_image();
    
    THEN("the user instance should be updated on database to reference the new image"){
        then_user_profile_image_should_be_updated_on_db();
        
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}

void AccountManagerFixture::testUpdateExistingUserProfileImage(){
    using_mockups();
    
    GIVEN("an existent account with a profile image"){
        given_a_user_with_image_profile();
        
    WHEN("updating that account profile image"){
        mocking_save_image();
        when_updating_the_user_profile_image();
        
    THEN("the new image should be stored"){
        then_image_manager_should_create_a_new_image();
    
    AND_THEN("the old image should be removed"){
        then_this_image_should_be_removed(existingUserImage);
    
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
    
}

void AccountManagerFixture::testGetUserProfileImage(){
    using_mockups();
    
    GIVEN("a user with image profile"){
        given_a_user_with_image_profile();
    
    WHEN("getting this user profile image"){
        when_getting_the_user_profile_image();
    
    THEN("it should return a thumbnail of the saved image"){
        then_a_user_profile_thumbnail_should_be_get();
        
    }//THEN
    }//WHEN
    }//GIVEN
}

void AccountManagerFixture::testGetUnexistentUserProfileImage(){
    using_database_mockup();

    GIVEN("a user without profile image"){
        given_existent_account_mockup();
        
    WHEN("getting this user profile image"){
        when_getting_the_user_profile_image();
        
    THEN("an null thumbnail pointer should be returned"){
        CHECK_FALSE(resultImagePtr);
    }//THEN
    }//WHEN
    }//GIVEN    
}

void AccountManagerFixture::testDisableUserAccount(){
    using_database_mockup();
    
    GIVEN("an existent active account"){
        long userId = given_existent_account_mockup();
        
    WHEN("setting the account to inactive"){
        accountManager.setAccountEnabled(userId, false);
    
    THEN("it should been updated on database"){
        then_account_should_have_been_activated();
    }//THEN
    }//WHEN
    }//GIVEN
    
}


