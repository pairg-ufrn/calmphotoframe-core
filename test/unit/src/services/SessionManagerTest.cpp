#include "catch.hpp"

#include "services/SessionManager.h"
#include "model/user/UserAccount.h"
#include "services/AccountManager.h"

#include "data/Data.h"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"

#include "exceptions/InvalidTokenException.h"

#include "utils/TimeUtils.h"
#include "utils/Log.h"
#include "utils/ScopedDatabase.h"

#include "TestMacros.h"
#include "helpers/UserHelper.h"

#define TEST_TAG "[SessionManager]"
#define FIXTURE SessionManagerFixture

using namespace calmframe;
using namespace calmframe::data;
using namespace std;

namespace Constants{
    const char * const validToken = "e86f1ce3810a0e44c81c05bdea45459e";
    const char * const userName = "user";
    const char * const userPassword = "password";
    const string invalidToken = "deadbeef42dc0fee";

    const long decreaseTime = 10;
}


class SessionManagerFixture{
public:
    static void testStartAndFinishSession();
    void testLoginAttemptOfInactiveAccount();
    void testRejectExternalLoginWithLocalUser();
    static void testSessionValidation();
    static void testStartLocalSession();

    void testStartUnlimitedSession();
    void testStartLocalUnlimitedSession();

    void testLocalSessionShouldHaveTheSameUser();
    static void testUpdateExistentSession();
    void testGetSessionFromToken();
    void testTryGetSessionFromInexistentToken();
public:
    ScopedSqliteDatabase scopedDb;

    SessionManager sessionManager;
    UserAccount existingUser;
    MemoryDataBuilder memoryDataBuilder;
    AccountManager accountManager;

    SessionManagerFixture();

    Session createValidSession();
    Session createOldValidSession();

    bool existUserId(long userId);

    UserAccount & createUser();
    void checkUnlimitedSession(const Session & session);
};

/* *****************************************************************************************************/

SCENARIO("Client starting and finishing session",TEST_TAG){
    SessionManagerFixture::testStartAndFinishSession();
}//SCENARIO

FIXTURE_SCENARIO("login attempt of inactive account"){
    this->testLoginAttemptOfInactiveAccount();
}//SCENARIO

FIXTURE_SCENARIO("SessionManager should not allow external login with LocalUser"){
    this->testRejectExternalLoginWithLocalUser();
}

SCENARIO("session validation",TEST_TAG){
    SessionManagerFixture::testSessionValidation();
}

SCENARIO("start local session", TEST_TAG){
    SessionManagerFixture::testStartLocalSession();
}//SCENARIO

FIXTURE_SCENARIO("Local sessions should be associated to the same user"){
    testLocalSessionShouldHaveTheSameUser();
}//SCENARIO

SCENARIO("update an existent session", TEST_TAG){
    SessionManagerFixture::testUpdateExistentSession();
}//SCENARIO

FIXTURE_SCENARIO("Get session from token"){
    testGetSessionFromToken();
}//SCENARIO

FIXTURE_SCENARIO("Try get session from invalid token"){
    this->testTryGetSessionFromInexistentToken();
}//SCENARIO

FIXTURE_SCENARIO("SessionManager should allow create sessions that does not expire"){
    this->testStartUnlimitedSession();
}

FIXTURE_SCENARIO("SessionManager should allow create local sessions that does not expire"){
    this->testStartLocalUnlimitedSession();
}
/* *****************************************************************************************************/

void SessionManagerFixture::testStartAndFinishSession(){

    SessionManagerFixture fixture;
GIVEN("an existing userAccount instance"){
    UserAccount & existingUser = fixture.existingUser;
GIVEN("that userAccount password"){
    string userPassword = Constants::userPassword;
    REQUIRE(fixture.sessionManager.validatePassword(existingUser, userPassword));
WHEN ("starting a session with a valid login, password pair"){
    Session createdSession = fixture.sessionManager.startSession(existingUser, Constants::userPassword);
THEN ("it should create a valid session instance"){
    CHECK(fixture.sessionManager.validateSession(createdSession));
WHEN("finishing that session"){
    fixture.sessionManager.finishSession(createdSession);
THEN("that should not be valid anymore"){
    CHECK(!fixture.sessionManager.validateSession(createdSession));
}
}//WHEN
}
}
}//GIVEN
}//GIVEN

}//test

void SessionManagerFixture::testLoginAttemptOfInactiveAccount(){

GIVEN("an inative user account"){
    existingUser.getUser().setActive(false);
    Data::instance().users().update(existingUser.getUser());

WHEN("trying to start a session with that account in a SessionManager"){
    Session session = sessionManager.startSession(existingUser, Constants::userPassword);

THEN("it should respond with a invalid session"){
    CHECK(!sessionManager.validateSession(session));
    CHECK(!Data::instance().sessions().exist(session.getId()));

}//THEN
}//WHEN
}//GIVEN

}//test

void SessionManagerFixture::testRejectExternalLoginWithLocalUser(){
    sessionManager.createLocalUser();
    UserAccount localUser;
    CHECK(Data::instance().users().get(SessionManager::LOCALUSER_ID, localUser));

    Session s;
    CHECK(s.getUserId() == User::INVALID_ID);

    Session session = sessionManager.startSession(localUser, "");
    CHECK(!Data::instance().sessions().exist(session.getId()));
    CHECK(!sessionManager.validateSession(session.getToken()));
    CHECK(session.getUserId() == User::INVALID_ID);
    CHECK(session == Session());

}//test

void SessionManagerFixture::testSessionValidation(){
    SessionManagerFixture fixture;
GIVEN("a valid session instance"){
    Session validSession = fixture.createValidSession();
THEN("it should contain a non-empty Token"){
    CHECK(validSession.getToken().empty() == false);
AND_THEN("it contains a non-expired timestamp"){
    CHECK(validSession.getExpireTimestamp() > TimeUtils::instance().getTimestamp());
    CHECK(!validSession.expired());
AND_THEN("it contains an existent userAccount id"){
    bool existUser = fixture.existUserId(validSession.getUserId());
    CHECK(existUser == true);
AND_THEN("it is stored on database"){
    Session dbSession;
    Data::instance().sessions().getUserSession(validSession.getUserId(), dbSession);
    CHECK(dbSession.getUserId() == validSession.getUserId());
    CHECK(dbSession == validSession);
}
}
}
}

}
}//test

void SessionManagerFixture::testStartLocalSession(){
GIVEN("a SessionManager instance"){
    SessionManagerFixture fixture;
GIVEN("a client secret"){
    string secret = "secret";

WHEN("starting a local session"){
    Session localSession = fixture.sessionManager.startLocalSession(secret);

THEN("it should create a valid session with the 'local user id'"){
    CHECK(!localSession.expired());
    CHECK(Data::instance().sessions().exist(localSession.getId()));
    CHECK(fixture.sessionManager.validateSession(localSession));

    CHECK(localSession.getUserId() == SessionManager::LOCALUSER_ID);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void SessionManagerFixture::testStartUnlimitedSession(){

GIVEN("some valid user on database"){
    UserAccount & user = this->existingUser;

WHEN("creating a session with unlimited time for that user"){
    Session createdSession = sessionManager.startSession(user, Constants::userPassword, true);
    checkUnlimitedSession(createdSession);
//THEN
}//WHEN
}//GIVEN

}//test

void SessionManagerFixture::testStartLocalUnlimitedSession(){
    WHEN("creating a local session with unlimited time"){
        Session createdSession = sessionManager.startLocalSession("anysecret", true);
        checkUnlimitedSession(createdSession);
    }//GIVEN

}//test

void SessionManagerFixture::checkUnlimitedSession(const Session & session){
    THEN("a new session that does not expire should be created"){
        CHECK(Data::instance().sessions().exist(session.getId()));
        CHECK(session.isUnlimited());
        CHECK(!session.expired());

    THEN("the session should be valid"){
        CHECK(sessionManager.validateSession(session));

    }//THEN
    }//WHEN
}//check

void SessionManagerFixture::testLocalSessionShouldHaveTheSameUser(){
    Session firstSession = sessionManager.startLocalSession("anySecret");
    Session secondSession = sessionManager.startLocalSession("canBeOtherSecret");

    CHECK(firstSession.getUserId() == secondSession.getUserId());
}

void SessionManagerFixture::testUpdateExistentSession(){

GIVEN("a SessionManager instance"){
    SessionManagerFixture fixture;
GIVEN("a valid session on database"){
    Session session = fixture.createOldValidSession();
    REQUIRE(fixture.sessionManager.validateSession(session));
    long initialExpireTimestamp = session.getExpireTimestamp();
WHEN("updating that session"){
    long updateTime = TimeUtils::instance().getTimestamp();
    CHECK_NOTHROW(fixture.sessionManager.updateSession(session));
THEN("it should extend its expire time on database"){
    Session extendedSession;
    CHECK(Data::instance().sessions().get(session.getId(), extendedSession));
    CHECK(initialExpireTimestamp < extendedSession.getExpireTimestamp());

AND_THEN("current expire time should be equals to the time when it "
         "was updated increased by \"sessionTime\" seconds")
{
    long now = TimeUtils::instance().getTimestamp();
    long sessionDuration = fixture.sessionManager.getSessionDuration();
    CHECK(extendedSession.getExpireTimestamp() >= updateTime + sessionDuration);
    CHECK(extendedSession.getExpireTimestamp() <= now + sessionDuration);
}
}
}
}
}//GIVEN
}

void SessionManagerFixture::testGetSessionFromToken(){

GIVEN("a token of  a valid session on database"){
    Session validSession = createValidSession();
    const std::string & token = validSession.getToken();

WHEN("get that session from token"){
    Session session = sessionManager.getSession(token);

THEN("an equals Session instance should be returned"){
    CHECK(validSession == session);

}//THEN
}//WHEN
}//GIVEN

}

void SessionManagerFixture::testTryGetSessionFromInexistentToken(){

WHEN("trying to get a session from a SessionManager using a invalid token"){
THEN("it should throw a IllegalTokenException"){
    CHECK_THROWS_AS(sessionManager.getSession(Constants::invalidToken), InvalidTokenException);

}//THEN
}//WHEN

}//test

/* *****************************************************************************************************/

SessionManagerFixture::SessionManagerFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
{
//    Data::setup(memoryDataBuilder);
    createUser();
}

Session SessionManagerFixture::createValidSession(){
    return sessionManager.startSession(existingUser, Constants::userPassword);
}

Session SessionManagerFixture::createOldValidSession(){
    Session session = createValidSession();

    session.setExpireTimestamp(session.getExpireTimestamp() - Constants::decreaseTime);

    CHECK(Data::instance().sessions().update(session));
    CHECK(Data::instance().sessions().get(session.getId(), session));
    return session;
}

bool SessionManagerFixture::existUserId(long userId){
    bool exist = Data::instance().users().exist(userId);
    return exist;
}

UserAccount &SessionManagerFixture::createUser(){
    existingUser = accountManager.create(Constants::userName,Constants::userPassword);
    return existingUser;
}
