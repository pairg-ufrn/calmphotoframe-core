#include "TestMacros.h"

#include "services/ImageManager.h"
#include "model/image/ImageExternalStream.h"
#include "model/image/Size.h"

#include "exceptions/NotFoundMember.h"
#include "utils/CommonExceptions.h"

#include "helpers/ScopedTempDir.h"
#include "utils/FileUtils.h"

#include <sstream>
#include <fstream>
#include <memory>

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;

#define TEST_TAG "[ImageManager]"
#define FIXTURE ImageManagerFixture

class ImageManagerFixture{
public:
    ImageManagerFixture();

    void testSaveValidImage();
    void testSaveInvalidImage();
    void testGetImage();
    void testGetUnexistentImage();
    void testGetThumbnail();
    void testRemoveImage();
    void testImageDirCreation();
    
public: //steps
    void given_directories_are_set();
    void given_an_image_instance();
    void given_an_invalid_image();
    void given_existent_image();
    void given_thumbnail_size();
    void given_existent_image_with_tumbnails();
    void createThumbnail(const std::string & img, int thumbnailSize);
    std::string given_non_existent_dir();
    void given_image_dir_does_not_exist();
    
    void when_saving_image();
    void when_getting_the_image_from_filename();
    void when_getting_reduced_and_square_image();
    void when_removing_existent_image();

    void then_result_image_is_valid();
    void check_saving_result_is_a_valid_key();
    void check_is_subpath(const string & path, const string & parent);
    void check_image_type();
    void check_result_image_filename(const string & filename);

    void check_image_file_deleted();
    void check_thumbnails_deleted();
    
    void check_dimensions_are_smaller_or_equal_than(const Size & size, int maxValue);
    void check_is_square(const Size & size);
    void checkFileNotExist(const string & path);
public:
    static constexpr const char * VALID_IMAGE = "test/data/test-images/test1.jpg";
        
public:
    ScopedTempDir temporaryDir;
    ImageManager imageManager;
    shared_ptr<Image> image;
    shared_ptr<Image> resultImage;
    shared_ptr<istream> imagecontent;
    string expectedImageType;
    string imageFilename;
    string savedFilename;
    string existentImagePath;
    vector<string> thumbnailsPath;
    
    Size resultSize;
    int thumbnailSize;
};

/**********************************************************************************************/

TEST_SCENARIO("ImageManager get image")     IS_DEFINED_BY(testGetImage)
TEST_SCENARIO("Try get unexistent image")   IS_DEFINED_BY(testGetUnexistentImage)
TEST_SCENARIO("Get thumbnail image")        IS_DEFINED_BY(testGetThumbnail)

TEST_SCENARIO("Save valid image")           IS_DEFINED_BY(testSaveValidImage)
TEST_SCENARIO("Try save invalid image")     IS_DEFINED_BY(testSaveInvalidImage)

TEST_SCENARIO("Remove image and thumbnails") IS_DEFINED_BY(testRemoveImage)

TEST_SCENARIO("Images dir should be created if not exist") IS_DEFINED_BY(testImageDirCreation)

/**********************************************************************************************/

void ImageManagerFixture::testGetImage(){
    GIVEN("an existent image"){
        given_existent_image();
        
    WHEN("getting this image using its filename"){
        when_getting_the_image_from_filename();
    
    THEN("a valid image instance should be returned"){
        then_result_image_is_valid();
        check_result_image_filename(imageFilename);
        
    }//THEN
    }//WHEN
    }//GIVEN
}

void ImageManagerFixture::testGetUnexistentImage(){
    WHEN("Trying to get unexistent image"){
        imageFilename = "image_not_exist.jpg";
    THEN("a not found exception should be thrown"){
        CHECK_THROWS_AS(when_getting_the_image_from_filename(), NotFoundMember);
    }//THEN
    }//WHEN
}

void ImageManagerFixture::testGetThumbnail()
{
    GIVEN("an existent image"){
        given_existent_image();
        
    GIVEN("a thumbnail max size"){
        given_thumbnail_size();
    
    WHEN("trying to get a thumbnail with that size"){
        when_getting_reduced_and_square_image();
    
    THEN("a new image should be created under thumbnails subdir"){
        then_result_image_is_valid();
        check_is_subpath(resultImage->getImagePath(), imageManager.getThumbnailsDir());
    
    THEN("the new image should have both dimensions equal or smaller thant given size"){
        check_dimensions_are_smaller_or_equal_than(resultSize, thumbnailSize);
    
    THEN("both image dimensions should have same size"){
        check_is_square(resultSize);
        
    }//THEN
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}

void ImageManagerFixture::testRemoveImage(){
    GIVEN("an existent image with thumbnails"){
        given_existent_image_with_tumbnails();
    
    WHEN("removing this image"){
        when_removing_existent_image();
    
    THEN("the image file should be deleted"){
        check_image_file_deleted();
    
    THEN("all related thumbnails should be deleted"){
        check_thumbnails_deleted();
        
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}

void ImageManagerFixture::testImageDirCreation(){
    GIVEN("image dir is set to a non existent directory"){
        given_image_dir_does_not_exist();

    WHEN("saving an image instance"){
        given_an_image_instance();
        when_saving_image();
    
    THEN("the image directory should be created"){
        CHECK(Files().exists(imageManager.getImagesDir()));
    }//THEN
    }//WHEN
    }//GIVEN
}

void ImageManagerFixture::testSaveValidImage(){
    
    GIVEN("an Image instance"){
        given_an_image_instance();
    
    WHEN("saving it"){
        when_saving_image();
    
    THEN("the image key(filename) should be returned as response"){
        check_saving_result_is_a_valid_key();
    
    AND_THEN("that image name should lead to an valid image"){
        then_result_image_is_valid();
    
    THEN("the image content should be saved on images directory"){
        check_is_subpath(resultImage->getImagePath(), imageManager.getImagesDir());
        
    }//THEN
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}

void ImageManagerFixture::testSaveInvalidImage(){
    
    GIVEN("an invalid image instance"){
        given_an_invalid_image();
    
    WHEN("trying to save it"){
    THEN("an IOException should be thrown"){
        CHECK_THROWS_AS(when_saving_image(), CommonExceptions::IOException);
    
    }//THEN
    }//WHEN
    }//GIVEN
}

/**********************************************************************************************/

ImageManagerFixture::ImageManagerFixture(){
    this->imageManager.setImagesDir(temporaryDir.getFilepath());
}

void ImageManagerFixture::given_an_image_instance(){
    image = make_shared<Image>(VALID_IMAGE);
    expectedImageType = "jpeg";
}

void ImageManagerFixture::given_an_invalid_image(){
    imagecontent = std::make_shared<stringstream>("Invalid image content.");
    image = make_shared<ImageExternalStream>(imagecontent.get());
}

void ImageManagerFixture::given_existent_image(){
    imageFilename = "image.jpeg";
    existentImagePath = Files().joinPaths(imageManager.getImagesDir(), imageFilename);
    
    CHECK(Files().copyFile(VALID_IMAGE, existentImagePath));
}

void ImageManagerFixture::given_thumbnail_size(){
    this->thumbnailSize = 48;
}


void ImageManagerFixture::given_existent_image_with_tumbnails(){
    given_existent_image();
    for(int size : vector<int>{48, 72, 96}){
        createThumbnail(imageFilename, size);
    }    
}

void ImageManagerFixture::createThumbnail(const string & img, int thumbnailSize){
    auto thumbImagePtr = imageManager.getThumbnail(imageFilename, thumbnailSize);
    this->thumbnailsPath.push_back(thumbImagePtr->getImagePath());
}

string ImageManagerFixture::given_non_existent_dir(){
    return Files().joinPaths(temporaryDir.getFilepath(), Files().joinPaths("non_existent", "image_dir"));
}

void ImageManagerFixture::given_image_dir_does_not_exist(){
    auto dir = given_non_existent_dir();
    imageManager.setImagesDir(dir);
    REQUIRE(!Files().exists(dir));
    REQUIRE(Files().isSubpath(dir, temporaryDir.getFilepath()));    
}

void ImageManagerFixture::when_saving_image(){
    savedFilename = imageManager.saveImage(*image);    
}

void ImageManagerFixture::when_getting_the_image_from_filename(){
    resultImage = imageManager.getImage(imageFilename);
}

void ImageManagerFixture::when_getting_reduced_and_square_image(){
    resultImage = imageManager.getThumbnail(imageFilename, thumbnailSize);
    resultSize = imageManager.getSize(*resultImage);
    
    CHECK(resultSize.width() > 0);
    CHECK(resultSize.height() > 0);
}

void ImageManagerFixture::when_removing_existent_image(){
    CHECK(imageManager.removeImage(imageFilename));
}

void ImageManagerFixture::then_result_image_is_valid(){
    REQUIRE(resultImage);
    CHECK(resultImage->isValid());
}

void ImageManagerFixture::check_saving_result_is_a_valid_key(){
    CHECK_NOTHROW(resultImage = imageManager.getImage(savedFilename));
}

void ImageManagerFixture::check_is_subpath(const string & path, const string & parent){
    CAPTURE(path);
    CAPTURE(parent);
    CHECK(Files().isSubpath(path, parent));
}

void ImageManagerFixture::check_image_type(){
    REQUIRE(resultImage);
    CHECK(resultImage->getType() == expectedImageType);
}

void ImageManagerFixture::check_result_image_filename(const string & expectedFilename){
    auto filename = Files().extractFilename(resultImage->getImagePath());
    CHECK(filename == expectedFilename);
}

void ImageManagerFixture::check_image_file_deleted(){
    checkFileNotExist(existentImagePath);
}

void ImageManagerFixture::check_thumbnails_deleted(){
    for(const auto & thumbPath : thumbnailsPath){
        checkFileNotExist(thumbPath);
    }
}

void ImageManagerFixture::checkFileNotExist(const std::string & path)
{
    CAPTURE(path);
    CHECK_FALSE(Files().exists(path));    
}

void ImageManagerFixture::check_dimensions_are_smaller_or_equal_than(const Size & size, int maxValue){
    CHECK(size.width() <= maxValue);
    CHECK(size.height() <= maxValue);
}

void ImageManagerFixture::check_is_square(const Size & size){
    CHECK(size.width() == size.height());
}
