#include "PhotoManager_MetadataTest.h"

FIXTURE_SCENARIO("Extracting photo metadata in photo creation"){
    testExtractMetadataInCreation();
}

/* ******************************************************************************/

void PhotoManagerMetadataFixture::testExtractMetadataInCreation(){

GIVEN("a PhotoFrame instance"){
GIVEN("an image with exif metadata"){
    Image image(imagePath);

    INFO("imagePath: " << imagePath);
    REQUIRE(FileUtils::instance().exists(imagePath));

WHEN("calling create photo" ){
    Photo photo = photoManager.createPhoto(image);

THEN("the image instance should not be open"){
    CHECK(!image.isOpen());

THEN("it should return an valid image"){
    PhotoHelper::assertExist(photo);

THEN("the photo should contain valid metadata"){
    const PhotoMetadata & metadata = photo.getMetadata();

    assertMetadataEquals(metadata, expectedMetadata);

}//THEN
}//THEN
}//THEN
    //Remove file
    FileUtils::instance().remove(photo.getImagePath());
    CHECK(!FileUtils::instance().exists(photo.getImagePath()));

}//WHEN
}//GIVEN
}//GIVEN

}

/* ******************************************************************************/

PhotoManagerMetadataFixture::PhotoManagerMetadataFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
    , imagePath(Constants::imageWithMetadata)
{
    setupMetadata();
}//test

void PhotoManagerMetadataFixture::setupMetadata(){
    expectedMetadata.setCameraMaker(Constants::imageWithMetadata_maker);
    expectedMetadata.setCameraModel(Constants::imageWithMetadata_model);
    expectedMetadata.setImageSize(Constants::imageWithMetadata_size);
    expectedMetadata.setImageWidth(Constants::imageWithMetadata_width);
    expectedMetadata.setImageHeight(Constants::imageWithMetadata_height);
    expectedMetadata.setOriginalDate(Constants::imageWithMetadata_date);
    expectedMetadata.getOrientation().setFlag(Constants::imageWithMetadata_orientationFlag);
    expectedMetadata.getLocation().setLatitude(Constants::imageWithMetadata_latitude);
    expectedMetadata.getLocation().setLongitude(Constants::imageWithMetadata_longitude);
    expectedMetadata.getLocation().setAltitude(Constants::imageWithMetadata_altitude);
}

void PhotoManagerMetadataFixture::assertMetadataEquals(const PhotoMetadata & metadata, const PhotoMetadata & expectedMetadata){
    CHECK(metadata.getCameraMaker()  == expectedMetadata.getCameraMaker());
    CHECK(metadata.getCameraModel()  == expectedMetadata.getCameraModel());
    CHECK(metadata.getImageSize()    == expectedMetadata.getImageSize());
    CHECK(metadata.getImageWidth()   == expectedMetadata.getImageWidth());
    CHECK(metadata.getImageHeight()  == expectedMetadata.getImageHeight());
    CHECK(metadata.getOriginalDate() == expectedMetadata.getOriginalDate());
    CHECK(metadata.getOrientation().getFlag() == expectedMetadata.getOrientation().getFlag());
    CHECK(metadata.getLocation().getLatitude()  ==  expectedMetadata.getLocation().getLatitude());
    CHECK(metadata.getLocation().getLongitude() ==  expectedMetadata.getLocation().getLongitude());
    CHECK(metadata.getLocation().getAltitude()  ==  expectedMetadata.getLocation().getAltitude());
}
