#ifndef PHOTOMANAGERTEST_H
#define PHOTOMANAGERTEST_H

#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "utils/ScopedDatabase.h"

#include "services/PhotoManager.h"

#include <string>

#define TEST_TAG "[PhotoManager]"
#define FIXTURE PhotoManagerFixture

using namespace std;
using namespace calmframe;
using namespace calmframe::data;

namespace Constants{
    static const string testImage = "test/data/test_exif.jpg";

    static const int thumbnailSize = 200;
}

#define EXISTING_ID 1
#define NONEXISTENT_ID 10000

void testUpdatePhoto();
void testRejectUpdateUnexistentPhoto();
void testInsertImage();
void testGetPhotoImage();
void testGetPhotoThumbnail();
void testPreserveMarkerIndexes();

class PhotoManagerFixture{

public: //Test cases
    void testGetPhotos();
    void testGetPhoto();
    void testTryGetInvalidPhoto();
    void testRemovePhoto();
    void testGetPhotosFromCollection();
    void testGetPhotosFromCollectionHierarchy();

public:
    ScopedSqliteDatabase scopedDb;
    PhotoManager photoManager;


    PhotoManagerFixture();

    bool assertPhotoExists(const Photo & photo);

    vector<PhotoMarker> createMarkersWithValidIndexes(int count);
    vector<PhotoMarker> createMarkersWithInvalidIndexes(int count);
    vector<PhotoMarker> createMarkers(int count, bool validIndexes = true);
    void assertHasValidIndexes(const vector<PhotoMarker> & markers, bool require=true);
    void assertMarkersEquals(const PhotoMarker & markerA, const PhotoMarker & markerB
                             , bool ignoreDatabaseIds=false, bool checkIndexes=true);

    void assertEqualsMarkersCollection(const vector<PhotoMarker> & markersA, const vector<PhotoMarker> & markersB);
    Photo createPhotoWithImage();
};

class PhotoManagerFixtureWithImage : public PhotoManagerFixture{
    typedef PhotoManagerFixture super;
public:
    string imagePath;
    Image image;

    PhotoManagerFixtureWithImage();
};

#endif // PHOTOMANAGERTEST_H

