#include "catch.hpp"

#include "services/PhotoCollectionManager.h"
#include "data/Data.h"

#include "utils/IllegalArgumentException.hpp"

#include "utils/ScopedDatabase.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/PhotoContextHelper.h"
#include "TestMacros.h"

#include <set>

using namespace std;
using namespace calmframe;
using namespace calmframe::data;

namespace Constants {
    static const long ANY_ID = 10000;
    static const int NUM_PHOTOS = 3;
    static const int NUM_SUBCOLLECTIONS = 3;
}//namespace

class CollectionManagerFixture{
public:
    ScopedSqliteDatabase scopedDatabase;
    PhotoCollectionManager collectionManager;

    bool isSubCollection(long collection, long parentCollection){
        PhotoCollection parent = collectionManager.getCollection(parentCollection);
        return parent.getSubCollections().contains(collection);
    }
    long createCollectionWithSubCollectionTree(){
        PhotoCollection rootColl = PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(
                                                    Constants::NUM_PHOTOS, Constants::NUM_SUBCOLLECTIONS);
        PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(
                                Constants::NUM_PHOTOS, Constants::NUM_SUBCOLLECTIONS, rootColl.getId());

        return rootColl.getId();
    }

    void checkMapTree(long collId, const std::map<long, PhotoCollection> & collTree){
        set<long> visited;
        checkMapTree(collId, collTree, visited);
        CHECK(visited.size() == collTree.size());
    }
    void checkMapTree(long collId, const std::map<long, PhotoCollection> & collTree, std::set<long> & visited){
        visited.insert(collId);
        bool exist = existItem(collId, collTree);
        INFO("coll id: " << collId);
        CHECK(exist);
        if(exist){
            const PhotoCollection & coll = collTree.find(collId)->second;
            const set<long> & subcollectionIds = coll.getSubCollections().getIds();
            set<long>::const_iterator iter = subcollectionIds.begin();
            while(iter != subcollectionIds.end()){
                long subCollId = *iter;
                if(!existItem(subCollId, visited)){
                    checkMapTree(subCollId, collTree, visited);
                }
                ++iter;
            }
        }
    }

    template<typename Item, typename T>
    bool existItem(const Item & i, const T & collTree)
    {
        return collTree.find(i) != collTree.end();
    }

    void assertCollectionExist(long collId){
        CHECK(Data::instance().collections().exist(collId));
    }

    long insertRootCollection(){
        return PhotoContextHelper::insertContextWithRootCollection().getRootCollection();
    }
};

#define FIXTURE CollectionManagerFixture

#define TEST_TAG "[PhotoCollectionManager]"

FIXTURE_SCENARIO("Get context root collection"){

GIVEN("some context id"){
    long collRootId = PhotoCollectionHelper::insertCollection();
    PhotoContext ctx;
    ctx.setRootCollection(collRootId);
    long ctxId = Data::instance().contexts().insert(ctx);

WHEN("getting that context root collection"){
    PhotoCollection rootCollection = this->collectionManager.getContextRoot(ctxId);

THEN("it should return the PhotoCollection with id equals to the Context rootCollection"){
    PhotoContext dbCtx = Data::instance().contexts().get(ctxId);
    CHECK(dbCtx.getRootCollection() == collRootId);
    CHECK(rootCollection.getId() == collRootId);

}//THEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("PhotoCollection root should not be removed",TEST_TAG){
    CollectionManagerFixture fixture;

GIVEN("an PhotoCollectionManager"){
    PhotoCollectionManager & collectionManager = fixture.collectionManager;

GIVEN("an root collection of some PhotoContext"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();

WHEN("Trying to remove the root PhotoCollection"){
    long rootCollectionId = ctx.getRootCollection();
THEN("an exception should be throwed"){
    CHECK_THROWS(collectionManager.removeCollection(rootCollectionId));
AND_THEN("the root collection should still exist"){
    fixture.assertCollectionExist(rootCollectionId);
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("PhotoCollection root should not be removed on bulk delete",TEST_TAG){

    CollectionManagerFixture fixture;
GIVEN("a PhotoCollectionManager"){
    PhotoCollectionManager & collectionManager = fixture.collectionManager;

GIVEN("a root collection of some PhotoContext"){
    long rootCollectionId = fixture.insertRootCollection();

WHEN("this root id is on a container of collections ids to be removed"){
    set<long> idsToRemove;
    idsToRemove.insert(rootCollectionId);
AND_WHEN("Trying to remove these collections"){
THEN("an exception should be throwed"){
    CHECK_THROWS(collectionManager.removeCollections(idsToRemove));
AND_THEN("the root collection should still exist"){
    fixture.assertCollectionExist(rootCollectionId);
}//THEN
}//THEN
}//WHEN

}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

FIXTURE_SCENARIO("Insert collection with parent id"){

GIVEN("a PhotoCollectionManager instance"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();

WHEN("inserting a new PhotoCollection with parent id"){
    PhotoCollection newColl;
    PhotoCollection parentCollection = collectionManager.createCollectionAtContext(newColl, ctx.getId());
    long parentId = parentCollection.getId();

    PhotoCollection createdCollection = collectionManager.createCollection(PhotoCollection(), parentId);
    long collectionId = createdCollection.getId();
    CHECK(collectionManager.existCollection(collectionId));

THEN("it should be added as a child of the used parent collection"){
    CHECK(isSubCollection(collectionId, parentId));

WHEN("the parent collection has a valid PhotoContext"){
    REQUIRE(parentCollection.hasPhotoContext());

THEN("the created child should also be added to that PhotoContext"){
    CHECK(createdCollection.hasPhotoContext());
    CHECK(createdCollection.getParentContext() == parentCollection.getParentContext());
}//THEN
}//WHEN
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO

FIXTURE_SCENARIO("Insert collection at some PhotoContext"){

GIVEN("some valid PhotoContext id"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
    long ctxId = ctx.getId();

WHEN("creating a PhotoCollection at that context id"){
    PhotoCollection newColl;
    PhotoCollection createdColl = collectionManager.createCollectionAtContext(newColl, ctxId);

THEN("the collection should be created as child of the root collection of that context"){
    PhotoCollection rootColl = collectionManager.getCollection(ctx.getRootCollection());
    CHECK(rootColl.getSubCollections().contains(createdColl.getId()));

THEN("it should be added to that PhotoContext"){
    CHECK(createdColl.hasPhotoContext());
    CHECK(createdColl.getParentContext() == ctx.getId());
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO


FIXTURE_SCENARIO("Trying to insert collection with invalid parent and context"){

WHEN("tryng to insert a new PhotoCollection with a invalid parent id and no PhotoContext"){
    PhotoCollection newColl;

THEN("an IllegalArgumentException should be thrown"){
    CHECK_THROWS_AS(collectionManager.createCollection(newColl, PhotoCollection::INVALID_ID)
                    , CommonExceptions::IllegalArgumentException);
}//WHEN
}//WHEN
}//SCENARIO

SCENARIO("Get collection tree",TEST_TAG){
    CollectionManagerFixture fixture;
GIVEN("some PhotoCollectionManager"){
    PhotoCollectionManager & collectionManager = fixture.collectionManager;

GIVEN("some collection with subcollections"){
    long collId = fixture.createCollectionWithSubCollectionTree();

WHEN("get the collection tree using this collection id"){
    map<long, PhotoCollection> collTree = collectionManager.getCollectionTree(collId);

THEN("the collectionManager should return a map with the given collection and all descendants"){
    fixture.checkMapTree(collId, collTree);

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Photos removed from all collections should be moved to root of PhotoContext",TEST_TAG){
    CollectionManagerFixture fixture;

GIVEN("a PhotoCollectionManager instance"){
    PhotoCollectionManager & manager = fixture.collectionManager;

GIVEN("some PhotoContext with PhotoCollections"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
    long ctxId = ctx.getId();
    long rootId = ctx.getRootCollection();

GIVEN("some photos on those subcollections"){
    const int photoNum = 5;
    PhotoCollection coll = PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(photoNum, rootId);

WHEN("removing those photos from their parent collections"){
    set<long> parentSet;
    parentSet.insert(coll.getId());
    manager.removeItemsFromCollections(parentSet, coll.getPhotos(), std::set<long>(), rootId);

THEN("they should be moved to root collection"){
    PhotoCollection root = manager.getCollection(rootId);
    set<long> rootSet;
    rootSet.insert(root.getId());
    CHECK(root.getPhotos() == coll.getPhotos());
    CHECK(!root.getPhotos().empty());
    PhotoCollectionHelper::checkCollectionsContainsItems(rootSet, coll.getPhotos(), set<long>(), true);

}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO

