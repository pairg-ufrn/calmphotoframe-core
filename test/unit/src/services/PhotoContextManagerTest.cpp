#include "TestMacros.h"

#include "services/PhotoContextManager.h"
#include "exceptions/NotFoundMember.h"

#include "data/Data.h"
#include "data/DataTransaction.h"

#include "utils/ScopedDatabase.h"
#include "helpers/PhotoContextHelper.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/UserHelper.h"

#include "model/permissions/ProtectedEntity.h"

#include <vector>

using namespace std;
using namespace calmframe;
using namespace calmframe::data;

namespace Constants {
    static const char * CTX_NAME = "Any context";
}//namespace

class ContextManagerFixture{
public:
    void testRemoveContextEntities();
    void testInitializationEnsureContextExistence();
    void testInitializationNotDuplicateContext();
    void testRemoveAllContextsRecreateDefault();
    void testRejectRemoveUnexistentContext();
    void testListContextsWithUserPermission();
    void testGetUserPermissionFromVisibility();
public:
    ScopedSqliteDatabase scopedDb;
    PhotoContextManager contextManager;

    PhotoContext createContextHierarchy();
    vector<PhotoCollection> getContextCollections(long ctxId);
    vector<Photo> getContextPhotos(long ctxId);

    void checkExistence(const std::vector<PhotoCollection> & collections, bool shouldExist);
    void checkExistence(const std::vector<Photo> & photos, bool shouldExist);
};

#define FIXTURE ContextManagerFixture
#define TEST_TAG "[PhotoContextManager]"


FIXTURE_SCENARIO("When Removing a Context, PhotoContextManager should remove all related entities"){
    this->testRemoveContextEntities();
}//SCENARIO

FIXTURE_SCENARIO("PhotoContextManager initialization should ensure that exist a PhotoContext on Db"){
    this->testInitializationEnsureContextExistence();
}//SCENARIO


FIXTURE_SCENARIO("PhotoContextManager initialization should not create a PhotoContext if already exist one"){
    this->testInitializationNotDuplicateContext();
}//SCENARIO

FIXTURE_SCENARIO("When removing the last PhotoContext, ContextManager should create a new default Context"){
    this->testRemoveAllContextsRecreateDefault();
}//SCENARIO

FIXTURE_SCENARIO("ContextManager should reject tentatives to remove non existent contexts"){
    this->testRejectRemoveUnexistentContext();
}//SCENARIO

FIXTURE_SCENARIO("List contexts with permission"){
    this->testListContextsWithUserPermission();
}

FIXTURE_SCENARIO("ContextManager should allow get the user permission based on the context visibility"){
    this->testGetUserPermissionFromVisibility();
}

/* **********************************************************************************************************/

void ContextManagerFixture::testRemoveContextEntities(){

GIVEN("some PhotoContext with collections and photos"){
    PhotoContext context = createContextHierarchy();
    long ctxId = context.getId();

    REQUIRE(context.getRootCollection() != PhotoContext::INVALID_COLLECTION_ID);

    vector<PhotoCollection> contextCollections = getContextCollections(ctxId);
//    vector<Photo> contextPhotos = getContextPhotos(ctxId);

    CHECK(!contextCollections.empty());
//    CHECK(!contextPhotos.empty());

WHEN("removing that PhotoContext"){
    contextManager.deleteContext(ctxId);

THEN("all photocollections related to that context should be removed"){
    checkExistence(contextCollections, false);

//THEN("all photos related to that context should be removed"){
//    checkExistence(contextPhotos, false);

//}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test

void ContextManagerFixture::testInitializationEnsureContextExistence(){

GIVEN("some database with no PhotoContext"){
    REQUIRE(contextManager.listContexts().empty());

WHEN("call initialize on a PhotoContextManager"){
    contextManager.initialize();

THEN("it should create a PhotoContext with a root PhotoCollection"){
    PhotoContext defaultContext = contextManager.getDefaultContext();
    CHECK(defaultContext.getId() != PhotoContext::INVALID_ID);
    CHECK(Data::instance().collections().exist(defaultContext.getRootCollection()));

THEN("the created context should have public visibility"){
    CHECK(defaultContext.getVisibility() == Visibility::EditableByOthers);

THEN("any user should have permission to edit it"){
    auto account = UserHelper::createAccount();
    CHECK_NOTHROW(contextManager.assertUserHasPermission(
                    account.getId(), defaultContext.getId(), PermissionLevel::Write));
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test

void ContextManagerFixture::testInitializationNotDuplicateContext(){

GIVEN("some database with one or more PhotoContexts"){
    contextManager.createContext(PhotoContext());
    vector<PhotoContext> contexts = contextManager.listContexts();
    REQUIRE(!contexts.empty());

WHEN("call initialize on a PhotoContextManager"){
    contextManager.initialize();

THEN("it should not create another PhotoContext"){
    vector<PhotoContext> currentContexts = contextManager.listContexts();
    CHECK(currentContexts.size() == contexts.size());

}//THEN
}//WHEN
}//GIVEN

}//test

void ContextManagerFixture::testRemoveAllContextsRecreateDefault(){

GIVEN("a database with only one PhotoContext"){
    PhotoContext initialContext = contextManager.createContext(PhotoContext());
    REQUIRE(contextManager.listContexts().size() == 1);

WHEN("the ContextManager remove that context"){
    contextManager.deleteContext(initialContext.getId());

THEN("another PhotoContext should be created as default"){
    CHECK(!contextManager.existContext(initialContext.getId()));
    CHECK(contextManager.existContext(contextManager.getDefaultContext().getId()));
    CHECK(Data::instance().contexts().count() == 1);
}//THEN
}//WHEN
}//GIVEN

}//test

void ContextManagerFixture::testRejectRemoveUnexistentContext(){

WHEN("trying to remove an non existent id"){
THEN("an NotFoundMember exception should be thrown"){
    long nonExistentId = 100000;
    CHECK_THROWS_AS(contextManager.deleteContext(nonExistentId), calmframe::NotFoundMember);
}//THEN
}//WHEN

}//test

void ContextManagerFixture::testListContextsWithUserPermission(){

GIVEN("some photocontexts with different visibility levels"){
    vector<PhotoContext> ctxsNoPermission = PhotoContextHelper::insertContexts({
        {"", Visibility::EditableByOthers}, {"", Visibility::VisibleByOthers}
    });
    vector<PhotoContext> ctxsWithPermission = PhotoContextHelper::insertContexts({
        {"", Visibility::EditableByOthers}, {"", Visibility::VisibleByOthers}, {"", Visibility::Private}
    });
    //others
    PhotoContextHelper::insertContexts({{"", Visibility::Private}, {"", Visibility::Private}});

GIVEN("some user with permission levels on that contexts"){
    auto userAccount = UserHelper::createAccount();
    long userId = userAccount.getId();
    for(auto ctx : ctxsWithPermission){
        Data::instance().permissions().putPermission({ctx.getId(), userId, PermissionLevel::Admin});
    }

WHEN("listing the contexts with permissions for that user"){
    auto resultCtxs = contextManager.listWithPermission(userId, PermissionLevel::Read);

THEN("a list of the contexts with at least the given permission should be returned"){
    int expectedSize = ctxsWithPermission.size() + ctxsNoPermission.size();
    CHECK(resultCtxs.size() == expectedSize);

THEN("the contexts without explicit permission user permission should derive its value from context visibility"){
    for(auto ctx: ctxsNoPermission){
        auto iter = std::find_if(resultCtxs.begin(), resultCtxs.end(),
        [&ctx](const PhotoContext & otherCtx){
            return ctx.getId() == otherCtx.getId();
        });
        if(iter != resultCtxs.end()){
            CHECK(iter->getPermissionLevel() == Permission::fromVisibility(ctx.getVisibility()));
        }
    }

THEN("other contexts should include the greater permission"){

}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void ContextManagerFixture::testGetUserPermissionFromVisibility(){

GIVEN("some context visible by any user"){
    PhotoContext ctx = PhotoContextHelper::insertContext(PhotoContext("nome", Visibility::VisibleByOthers));

GIVEN("some user with no explicity permission on that context"){
    UserAccount account = UserHelper::createAccount();

WHEN("getting that user permission"){
    Permission userPermission = this->contextManager.getUserPermission(ctx.getId(), account.getId());

THEN("it should return a value based on the context visibility"){
    CHECK(userPermission.getPermissionLevel() == PermissionLevel::Read);
    CHECK(userPermission.getContextId() == ctx.getId());
    CHECK(userPermission.getUserId() == account.getId());
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

/* **********************************************************************************************************/

PhotoContext ContextManagerFixture::createContextHierarchy(){
    return PhotoContextHelper::createContextHierarchy();
}

vector<PhotoCollection> ContextManagerFixture::getContextCollections(long ctxId){
    return Data::instance().collections().getByContext(ctxId);
}

vector<Photo> ContextManagerFixture::getContextPhotos(long ctxId){
    return Data::instance().photos().listByContext(ctxId);
}

void ContextManagerFixture::checkExistence(const std::vector<PhotoCollection> & collections, bool shouldExist){
    for(size_t i=0; i < collections.size(); ++i){
        const PhotoCollection & coll = collections[i];
        INFO("PhotoCollection: " << coll.getId());
        CHECK(Data::instance().collections().exist(coll.getId()) == shouldExist);
    }
}

void ContextManagerFixture::checkExistence(const std::vector<Photo> & photos, bool shouldExist){
    for(size_t i=0; i < photos.size(); ++i){
        const Photo & photo = photos[i];
        INFO("Photo: " << photo.getId());
        CHECK(Data::instance().photos().exist(photo.getId()) == shouldExist);
    }
}
