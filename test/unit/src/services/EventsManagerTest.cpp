#include "TestMacros.h"
#include "helpers/UserHelper.h"
#include "helpers/EventsHelper.h"
#include "utils/ScopedDatabase.h"

#include "services/EventsManager.h"

#include "model/events/ExtendedEvent.h"

using namespace calmframe;
using namespace std;

class EventsManagerFixture{
public: //test
    void testGetEventsWithUserData();

public:
    ScopedSqliteDatabase scopedDb;
    EventsManager manager;
};

#define TEST_TAG "[EventsManager]"
#define FIXTURE EventsManagerFixture

/* **********************************************************************************************/

FIXTURE_SCENARIO("EventsManager should allow get events together with user data"){
    this->testGetEventsWithUserData();
}//test

/* **********************************************************************************************/


void EventsManagerFixture::testGetEventsWithUserData(){

GIVEN("some events created with relation to some users"){

    vector<ExtendedEvent> expectedEvents;

    auto accounts = UserHelper::createAccounts(3);
    for(auto userAccount : accounts){
        auto addedEvts = EventsHelper::insertEvents({
            Event{EventType::PhotoCreation, userAccount.getId(), Event::InvalidId},
            Event{EventType::PhotoRemove, userAccount.getId(), Event::InvalidId}
        });

        for(auto evt : addedEvts){
            expectedEvents.push_back(ExtendedEvent{evt, userAccount.getUser()});
        }
    }

WHEN("using a EventsManager to list the recent events"){
    auto events = this->manager.listRecentEvents();

THEN("each extended user should contains the related user data"){
    std::sort(expectedEvents.begin(), expectedEvents.end());
    std::sort(events.begin(), events.end());

    CHECK(expectedEvents.size() == events.size());
    for(int i=0; i < events.size(); ++i){
        CAPTURE(i);
        EventsHelper::checkEquals(events[i], expectedEvents[i]);
    }
}//THEN
}//WHEN
}//GIVEN

}
