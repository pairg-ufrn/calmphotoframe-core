#include "catch.hpp"

#include "model/photo/PhotoMetadata.h"
#include "services/image/MetadataExtractor.h"
#include "model/photo/Location.h"

#include "FreeImagePlus.h"
#include <string>
#include <iostream>

using namespace std;
using namespace calmframe;
using namespace calmframe::photo;

namespace Constants{
    const char * IMAGE_FILE = "test/data/test_exif.jpg";
    const char * GPSIMAGE_FILE = "test/data/test-images/test1.jpg";
}

TEST_CASE("Extracting Basic PhotoMetadata","[Metadata Extractor]"){
    PhotoMetadata metadata = MetadataExtractor::instance().extractMetadata(Constants::IMAGE_FILE);

    CHECK(!metadata.getCameraMaker().empty());
    CHECK(!metadata.getCameraModel().empty());
    CHECK(!metadata.getOriginalDate().empty());

    CHECK(metadata.getCameraMaker() == "Nokia");
    CHECK(metadata.getCameraModel()== "202");
    CHECK(metadata.getOriginalDate() == "2013:08:10 17:16:33");
    CHECK(metadata.getImageSize() == 226183);
    CHECK(metadata.getImageWidth() == 1200);
    CHECK(metadata.getImageHeight() == 1600);

    CHECK(metadata.getLocation().getLatitude()  == Location::UNKNOWN_COORD);
    CHECK(metadata.getLocation().getLongitude() == Location::UNKNOWN_COORD);
    CHECK(metadata.getLocation().getAltitude()  == Location::UNKNOWN_ALTITUDE);
}

TEST_CASE("Extracting GPS PhotoMetadata","[Metadata Extractor]"){
    PhotoMetadata metadata = MetadataExtractor::instance().extractMetadata(Constants::GPSIMAGE_FILE);

    CHECK(metadata.getLocation().getLatitude()  == 37.885000f);
    CHECK(metadata.getLocation().getLongitude() == -122.622500f);
    CHECK(metadata.getLocation().getAltitude()  == 122.000000f);
}

