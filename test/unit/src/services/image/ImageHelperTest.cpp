#include "catch.hpp"

#include <string>
#include <FreeImagePlus.h>

#include "services/image/FreeimageLibrary.h"
#include "services/image/ImageHelper.h"
#include "model/image/Image.h"

#include "utils/FileUtils.h"

using namespace std;
using namespace calmframe::photo;
using namespace calmframe::utils;

namespace Constants {
    #define DATA_DIR "test/data/"
    static const char * TEST_IMAGE =  DATA_DIR "test_exif.jpg";
    static const char * TEST_IMAGE_TYPES[] = {
        DATA_DIR "test_exif.jpg",
        DATA_DIR "image.png",
        DATA_DIR "image.gif",
        DATA_DIR "image.tga",
        DATA_DIR "image.tiff"};
    static const int TEST_IMAGE_TYPES_SIZE = 5;
}//namespace


SCENARIO("Creating thumbnail", "[ImageHelper][image][slow]"){
    FreeimageLibrary::instance().require();

GIVEN("a valid image path"){
    string imagePath(Constants::TEST_IMAGE);
GIVEN("a thumbnail size smaller than the original image size"){
    int thumbnailSize = 200;
    fipImage originalImage;
    originalImage.load(Constants::TEST_IMAGE);
    REQUIRE(true);
    REQUIRE(originalImage.getWidth() > thumbnailSize);
    REQUIRE(originalImage.getHeight() > thumbnailSize);

WHEN("calling make thumbnail"){
    Image thumbnailImage = ImageHelper::instance().makeThumbnail(imagePath, thumbnailSize);

THEN("it should return a valid image"){
    REQUIRE(thumbnailImage.isValid());
AND_THEN("the thumbnail image shoud be a square."){

    fipImage fipThumbnail;
    fipThumbnail.load(thumbnailImage.getImagePath().c_str());

    CHECK(fipThumbnail.getWidth() == fipThumbnail.getHeight());
AND_THEN("the image width and height should be equals to thumbnail size"){
    CHECK(fipThumbnail.getWidth() == thumbnailSize);
    CHECK(fipThumbnail.getHeight() == thumbnailSize);

    CHECK(FileUtils::instance().remove(thumbnailImage.getImagePath()));
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//SCENARIO

TEST_CASE("Test thumbnail creation types", "[ImageHelper]"){
    int thumbnailSize = 100;
    for(int i=0; i < Constants::TEST_IMAGE_TYPES_SIZE; ++i){
        string imagePath = Constants::TEST_IMAGE_TYPES[i];

        Image thumbnailImage = ImageHelper::instance().makeThumbnail(imagePath, thumbnailSize);
        INFO("source image path: " << imagePath);
        INFO("dest image path: " << thumbnailImage.getImagePath());

        CHECK(thumbnailImage.isValid());
        if(thumbnailImage.isValid()){
            CHECK(FileUtils::instance().remove(thumbnailImage.getImagePath()));
        }

    }
}



