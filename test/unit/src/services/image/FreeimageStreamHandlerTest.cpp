#include "catch.hpp"

#include "services/image/FreeimageStreamHandler.h"
#include "services/image/FreeimageLibrary.h"
#include "services/image/ImageHelper.h"
#include "model/image/Image.h"
#include "utils/FileUtils.h"

#include <string>
#include <fstream>
#include <FreeImagePlus.h>

#define TEST_IMAGE "test/data/test_exif.jpg"

using namespace std;
using namespace calmframe::photo;
using namespace calmframe::utils;

FreeImageIO nullFreeImageIO(){
    FreeImageIO handler;
    handler.read_proc = NULL;
    handler.write_proc = NULL;
    handler.seek_proc = NULL;
    handler.tell_proc = NULL;

    return handler;
}

SCENARIO("Image load"){
    FreeimageLibrary::instance().require();

    GIVEN("an valid image path"){
        string imagePath = TEST_IMAGE;
        REQUIRE(FileUtils::instance().exists(imagePath));
    GIVEN("a file stream open with that path"){
        ifstream imageStream(imagePath.c_str());
        REQUIRE(imageStream.good());
    WHEN("calling FreeimageStreamHandler::createHandler passing an FreeImageIO instance"){
        FreeImageIO imageHandler = nullFreeImageIO();
        FreeimageStreamHandler::createInputHandler(imageHandler);
    THEN("it should fill that FreeImageIO instance with valid read functions"){
        CHECK(imageHandler.write_proc != NULL);
        CHECK(imageHandler.read_proc != NULL);
        CHECK(imageHandler.tell_proc != NULL);
        CHECK(imageHandler.seek_proc != NULL);

    AND_WHEN("calling fipImage::loadFromHandler using that FreeImageIO instance and passing an istream pointer as handle"){
        fipImage image;
        bool loaded = image.loadFromHandle(&imageHandler, &imageStream);
    THEN("it should load successfully"){
        CHECK(loaded);
    }
    }
    }
    }
    }
    }
}




