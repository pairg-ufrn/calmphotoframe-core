#include "TestMacros.h"

#include "api/Api.h"
#include "model/user/User.h"

#include <fakeit.hpp>

#include "serialization/Serializer.h"

#include "steps/SerializerSteps.h"

using namespace calmframe;

#define TEST_TAG "[User]"
#define FIXTURE UserTests

class UserTests : public SerializerSteps{
public:
    void testProfileImageSerialization();
    void testEmptyProfileImageSerialization();
    void then_has_image_should_be(bool hasImage);
    
public:
    User & given_a_user_with_profile_image(const string & profileImg);
public:
    User user;
};

/****************************************************************************************************************/

TEST_SCENARIO("Serialize user profile image")               IS_DEFINED_BY(testProfileImageSerialization)
TEST_SCENARIO("Serialize user with no profile image")       IS_DEFINED_BY(testEmptyProfileImageSerialization)


/***************************************************************************************************************/

void UserTests::testProfileImageSerialization(){
    GIVEN("an user with profile image"){
        given_a_user_with_profile_image("anything.png");
    
    WHEN("serializing it"){
        when_serializing_it(user);
    
    THEN("it should include a property indicating it has an profile image"){
        then_has_image_should_be(true);
        
    }//THEN
    }//WHEN
    }//GIVEN
}

void UserTests::testEmptyProfileImageSerialization(){
    GIVEN("an user without profile image"){
        given_a_user_with_profile_image("");
    
    WHEN("serializing it"){
        when_serializing_it(user);
    
    THEN("it should include a property indicating it does not have a profile image"){
        then_has_image_should_be(false);
        
    }//THEN
    }//WHEN
    }//GIVEN
}

/***************************************************************************************************************/

User &UserTests::given_a_user_with_profile_image(const std::string & profileImg){
    user.setId(123456);
    user.profileImage(profileImg);

    return user;
}

void UserTests::then_has_image_should_be(bool hasImage)
{
    const char * expected = hasImage ? "true" : "false";
    then_it_should_have_serialized(User::Fields::hasProfileImage, expected);    
}
