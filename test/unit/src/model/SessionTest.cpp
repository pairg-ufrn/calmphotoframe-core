#include "TestMacros.h"

#include "model/user/Session.h"
#include "utils/TimeUtils.h"

using namespace calmframe;
using namespace std;

class SessionFixture{
public:
    void testSessionExpiration();
    void testUnlimitedSession();
public:
};

#define TEST_TAG "[Session]"
#define FIXTURE SessionFixture

/* **********************************************************************************************/

FIXTURE_SCENARIO("Session expiration time"){
    testSessionExpiration();
}

FIXTURE_SCENARIO("Session with unlimited expiration"){
    testUnlimitedSession();
}


/* **********************************************************************************************/

void SessionFixture::testSessionExpiration(){
    long now = TimeUtils::instance().getTimestamp();

    CHECK(Session().expired());
    CHECK(Session(now - 1).expired());
    CHECK_FALSE(Session(now + 500).expired());
}

void SessionFixture::testUnlimitedSession(){
    CHECK_FALSE(Session(Session::unlimitedExpiration).expired());
}
