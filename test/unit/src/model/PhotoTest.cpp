
#include "catch.hpp"

#include "model/photo/Photo.h"



using namespace std;
using namespace calmframe;

Photo createPhoto(){
    Photo photo;
    photo.setId(1);
    photo.setImagePath("someImagePath");
    photo.getDescription().setDate("20/02/2002");
    photo.getDescription().setDescription("someDescription");
    photo.getDescription().setMoment("someMoment");
    photo.getDescription().setPlace("somePlace");
    PhotoMarker someMarkers[] ={PhotoMarker("SomeMarker",0.7f,0.7f, photo.getId(), 1)};
    photo.getDescription().setMarkers(
         vector<PhotoMarker>(someMarkers, someMarkers + sizeof(someMarkers)/sizeof(PhotoMarker) ) );

    return photo;
}

SCENARIO("Comparing photos"){

    GIVEN("some photo"){
        Photo photo = createPhoto();
        WHEN("Comparing the photo with itself"){
            THEN("It should be equals"){
                CHECK(photo == photo);
            }
        }

        WHEN("Comparing the photo with another equals photo"){
            Photo otherPhoto = createPhoto();

            THEN("they should be equals in any comparison order"){
                CHECK(photo == otherPhoto);
                CHECK(otherPhoto == photo);
            }
            AND_THEN("they should not be less or greater than other"){
                CHECK(! (photo < otherPhoto));
                CHECK(! (otherPhoto < photo));
                CHECK(! (photo > otherPhoto));
                CHECK(! (otherPhoto > photo));
            }
        }

        WHEN("Comparing the photo with another with greater id"){
            Photo anotherPhoto = createPhoto();
            anotherPhoto.setId(100);
            THEN("the photo should be less than the other"){
                CHECK(photo < anotherPhoto);
                CHECK(anotherPhoto > photo);
            }
            AND_THEN("they should not be equals in any comparison order"){
                CHECK(!(photo == anotherPhoto));
                CHECK(!(anotherPhoto == photo));
            }
            AND_THEN("operator '!=' should return true in any comparison order"){
                CHECK(photo != anotherPhoto);
                CHECK(anotherPhoto != photo);
            }
        }
        WHEN("Comparing the photo with another different photo with same id"){
            Photo anotherPhoto = createPhoto();
            anotherPhoto.getDescription().setDate("otherDate");
            anotherPhoto.getDescription().setDescription("otherDescription");
            THEN("we do not guarantee that they are less or greater"){
                CHECK(!(photo < anotherPhoto));
                CHECK(!(anotherPhoto < photo));
                CHECK(!(photo > anotherPhoto));
                CHECK(!(anotherPhoto > photo));
            }
        }
        WHEN("Comparing the photo with another with different description"){
            Photo anotherPhoto = createPhoto();
            anotherPhoto.getDescription().setDate("otherDate");
            anotherPhoto.getDescription().setDescription("otherDescription");
            THEN("they should not be equals "){
                CHECK(!(photo == anotherPhoto));
                CHECK(anotherPhoto != photo);
            }
        }

        WHEN("Comparing the photo with another with different markers"){
            Photo anotherPhoto = createPhoto();
            vector<PhotoMarker> markers = anotherPhoto.getDescription().getMarkers();
            markers[0].setRelativeX(1.0f);
            anotherPhoto.getDescription().setMarkers(markers);
            THEN("they should not be equals"){
                CHECK(!(photo == anotherPhoto));
                CHECK(!(anotherPhoto == photo));
            }
        }
        AND_WHEN("Comparing photos with same markers in different orders"){
            Photo photo1 = createPhoto();
            Photo photo2 = createPhoto();
            PhotoMarker marker1("mk1"), marker2("mk2");

            vector<PhotoMarker> markers1 = vector<PhotoMarker>();
            markers1.push_back(marker1);
            markers1.push_back(marker2);

            vector<PhotoMarker> markers2 = vector<PhotoMarker>();
            markers2.push_back(marker2);
            markers2.push_back(marker1);

            photo1.getDescription().setMarkers(markers1);
            photo2.getDescription().setMarkers(markers2);

            THEN("they should be equals"){
                CHECK(photo1.getDescription().getMarkers() == photo2.getDescription().getMarkers());
                CHECK(photo1 == photo2);
                CHECK(photo2 == photo1);
            }
        }
    }
}


