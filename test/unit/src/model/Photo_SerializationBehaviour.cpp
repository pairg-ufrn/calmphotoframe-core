#include "catch.hpp"
#include "json/json.h"
#include <sstream>
#include <fstream>
#include <cstdlib> //rand
#include <ctime> //rand
#include <iostream>

#include "serialization/json/JsonSerializer.h"
#include "helpers/PhotoHelper.h"
using namespace std;

#include "model/photo/Photo.h"
#include "model/photo/PhotoMarker.h"

using namespace calmframe;

namespace Constants{
    const char * jsonWithEmptyMarkers =
            "{ "
                "\"markers\" : " "[]"
            " }";
}

#define PHOTO_JSON_FILE "test/data/test_photo.json"
        #define EPSILON 0.000001f

        float randomFloat(){
            return rand()/((float)RAND_MAX);
        }
        float diff(float a, float b){
            return a > b ? a -b : b -a;
        }

vector<PhotoMarker> createMarkers(long photoId){
    vector<PhotoMarker> marks;

    for(int i=0; i < 10; ++i){
        float x = randomFloat();
        float y = randomFloat();
        PhotoMarker marker("name",x,y, photoId, i + 1);
        marks.push_back(marker);
    }

    return marks;
}

class PhotoSerializationFixture{
public:
    stringstream helperStrStream;
    Json::Reader reader;
    Json::Value tmpValue;
    JsonSerializer jsonSerializer;

    PhotoSerializationFixture(){
        srand(time(0));
    }
};

SCENARIO("Photo (de)serialization", "[Photo]"){
    GIVEN("a JsonSerializer instance"){
        PhotoSerializationFixture fixture;
        GIVEN("a photo instance"){
            Photo photo = PhotoHelper::createPhoto(100);
            vector<PhotoMarker> markers = createMarkers(photo.getId());
            photo.getDescription().setMarkers(markers);
        WHEN("serializing that photo instance"){
            photo.serialize(fixture.jsonSerializer);
        THEN("its json representation should have the same values of the instance"){
            fixture.tmpValue << fixture.jsonSerializer;
            PhotoHelper::assertDescriptionEquals(photo, fixture.tmpValue);
            PhotoHelper::assertMarkersEquals(photo, fixture.tmpValue);
        }
        }
        }
    }
}
SCENARIO("Photo deserialization from file", "[Photo]"){
    GIVEN("a JsonSerializer instance"){
        PhotoSerializationFixture fixture;
    GIVEN("a json representation of some photo"){
        fstream jsonFile(PHOTO_JSON_FILE);
        REQUIRE(jsonFile.good());
        fixture.jsonSerializer.deserialize(jsonFile);

        Json::Value photoJson;
        fixture.jsonSerializer >> photoJson;
        INFO("Deserialized json: " << photoJson);

        WHEN("deserializing a photo instance from that json"){
            Photo photo;
            fixture.jsonSerializer.deserialize(photo);
            fixture.jsonSerializer << photo;
            INFO("generated json:" << fixture.jsonSerializer);

            THEN("it should correspond to that json"){
                PhotoHelper::assertEquals(photo, photoJson);
            }
        }
    }
    }
}

SCENARIO("Erasing markers on deserialization", "[Photo]"){
    GIVEN("a JsonSerializer instance"){
        PhotoSerializationFixture fixture;
    GIVEN("some photo with photo markers"){
        Photo photo = PhotoHelper::createPhoto(1);
        photo.getDescription().setMarkers(createMarkers(1));
    GIVEN("some photo json, without markers"){
        Json::Value photoJson;
        bool parsed = fixture.reader.parse(string(Constants::jsonWithEmptyMarkers), photoJson);
        INFO("parsed "<< parsed);
        CHECK(parsed);
        fixture.jsonSerializer.deserialize(photoJson);
        fixture.jsonSerializer >> photo;

        CHECK(photoJson.getMemberNames().size() == 1);
        REQUIRE(photoJson.isMember(PhotoFields::MARKERS));
        WHEN("deserializing the photo with markers from that json"){
            fixture.jsonSerializer.deserialize(photo);
        THEN("the serializer should contain a reference to the marker"){
            CHECK(fixture.jsonSerializer.getDeserialized().contains(PhotoFields::MARKERS));
        THEN("it should not contain markers anymore"){
            REQUIRE(photo.getDescription().getMarkers().size() == 0);
        }
        REQUIRE(photo.getDescription().getMarkers().size() == 0);
        }
        }
    }//GIVEN
    }//GIVEN
    }//GIVEN
}

