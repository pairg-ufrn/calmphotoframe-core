#include "catch.hpp"

#include "serialization/json/JsonSerializer.h"
#include "model/photo/PhotoCollection.h"

#include "json/json.h"

#include <sstream>
#include <iostream>
#include <string>

using namespace calmframe;
using namespace std;

namespace Constants {
    static const long COLLECTION_ID = 1;
    static const char * COLLECTION_NAME = "alguma coleção";
    static const long PHOTO_IDS[5] = {2, 3, 5,6, 8};
    static const set<long> PHOTO_IDS_SET(PHOTO_IDS, PHOTO_IDS + 5);
}

class PhotoCollectionFixture{
public:
    stringstream helperStrStream;
    Json::Reader reader;
    Json::Value tmpValue;
    JsonSerializer jsonSerializer;

    string createPhotoCollectionString(){
        Json::Value jsonObject(Json::objectValue);
        jsonObject["name"] = Constants::COLLECTION_NAME;
        jsonObject["id"] = 1;
        return jsonObject.toStyledString();
    }
    void assertEqualsJson(const PhotoCollection & photoCollection, const Json::Value & photoCollectionJson){
        CHECK(photoCollection.getId()   == photoCollectionJson["id"].asInt());
        CHECK(photoCollection.getName() == photoCollectionJson["name"].asString());
    }

    PhotoCollection createPhotoCollection(){
        PhotoCollection coll;
        coll.setId(Constants::COLLECTION_ID);
        coll.setName(Constants::COLLECTION_NAME);
        coll.setPhotos(Constants::PHOTO_IDS_SET);

        return coll;
    }
    PhotoCollection createPhotoCollectionWithPhotos(int numPhotos = 3){
        PhotoCollection coll = createPhotoCollection();

        for(int i=0; i < numPhotos; ++i){
            coll.putPhoto(i + 1);
        }

        return coll;
    }
};

SCENARIO("PhotoCollection deserialization from string", "[PhotoCollection]"){
GIVEN("a JsonSerializer instance"){
    PhotoCollectionFixture fixture;
GIVEN("a json representation of some photoCollection"){
    string jsonString = fixture.createPhotoCollectionString();
    fixture.jsonSerializer.deserialize(jsonString);

    Json::Value photoCollectionJson;
    fixture.jsonSerializer >> photoCollectionJson;
    INFO("Deserialized json: " << photoCollectionJson);

WHEN("deserializing a photoCollection instance from that json"){
    PhotoCollection photoCollection;
    fixture.jsonSerializer.deserialize(photoCollection);
    INFO("generated json:" << fixture.jsonSerializer);

THEN("it should correspond to that json"){
    fixture.assertEqualsJson(photoCollection, photoCollectionJson);
}
}
}
}
}

SCENARIO("Serializing and deserializing PhotoCollection to Json","[PhotoCollection]"){
    PhotoCollectionFixture fixture;
GIVEN("some PhotoCollection instance"){
    PhotoCollection coll = fixture.createPhotoCollection();
WHEN("serializing it with an json serializer"){
    string collJsonString;
    coll >> fixture.jsonSerializer >> collJsonString;

THEN("it should produce an json representation of that PhotoCollection"){
    CHECK(!collJsonString.empty());
    Json::Reader reader;
    Json::Value value;
    bool parsed = reader.parse(collJsonString, value);
    CHECK(parsed == true);

THEN("it should contains the PhotoCover id"){
    CHECK(coll.getPhotoCover() == value.get(PhotoCollectionFields::PHOTO_COVER_ID, -1).asLargestInt());

WHEN("Deserializing that json in a PhotoCollection instance"){
    PhotoCollection deserializedCollection;
    INFO(collJsonString);
    fixture.jsonSerializer.clear();
    CHECK(fixture.jsonSerializer.empty());
    collJsonString >> fixture.jsonSerializer >> deserializedCollection;

THEN("it should create a PhotoCollection equals to the first one"){
    CHECK(deserializedCollection.getId()     == coll.getId());
    CHECK(deserializedCollection.getName()   == coll.getName());
    CHECK(deserializedCollection.getPhotos() == coll.getPhotos());
}//THEN
}//WHEN
}//THEN
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO


SCENARIO("Get collection cover","[PhotoCollection]"){
    PhotoCollectionFixture fixture;

GIVEN("some PhotoCollection with some photos"){
    PhotoCollection collection = fixture.createPhotoCollectionWithPhotos();

GIVEN("that this PhotoCollection has not an cover set"){
    collection.clearPhotoCover();

WHEN("get the PhotoCollection cover"){
    long photoCover = collection.getPhotoCover();

THEN("it should return one of its photo ids"){
    CHECK(collection.containsPhoto(photoCover));

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO


