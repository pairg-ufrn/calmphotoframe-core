#include "TestMacros.h"
#include "helpers/EventsHelper.h"

#include "model/events/Event.h"
#include "model/events/ExtendedEvent.h"
#include "model/user/User.h"
#include "serialization/json/JsonSerializer.h"

using namespace calmframe;
using namespace std;

class EventFixture{
public: //test
    void testEventSerialization();
    void testEventDeserialization();

    void testExtendedEventSerialization();
    void testExtendedEventDeserialization();

    void testExtendedEventUserInfo();
public:
    Event makeSomeEvent();
    Json::Value checkEventJson(JsonSerializer & serializer, const Event & evt, const string & eventType);
    ExtendedEvent buildSomeExtendedEvent();
};

#define TEST_TAG "[Event]"
#define FIXTURE EventFixture

/* **********************************************************************************************/

FIXTURE_SCENARIO("Event serialization") IS_DEFINED_BY(testEventSerialization)

FIXTURE_SCENARIO("Event deserialization") IS_DEFINED_BY(testEventDeserialization)

FIXTURE_SCENARIO("ExtendedEvent serialization") IS_DEFINED_BY(testExtendedEventSerialization)

FIXTURE_SCENARIO("ExtendedEvent deserialization") IS_DEFINED_BY(testExtendedEventDeserialization)

FIXTURE_SCENARIO("ExtendedEvent user info") IS_DEFINED_BY(testExtendedEventUserInfo)

/* **********************************************************************************************/

void EventFixture::testEventSerialization(){
    GIVEN("some event instance"){
        Event evt = makeSomeEvent();
    
    WHEN("serializing it using a json serializer"){
        JsonSerializer serializer;
    
        serializer << evt;
    
    THEN("it should produce a valid json with the same values"){
        checkEventJson(serializer, evt, "PhotoCreation");
    
    }//THEN
    }//WHEN
    }//GIVEN
}

void EventFixture::testEventDeserialization(){
    GIVEN("a json string serialized from a Event instance"){
        Event evt = makeSomeEvent();
        JsonSerializer serializer;
    
        evt >> serializer;
    
        serializer.getDeserialized();
    
    WHEN("deserializing it using a JsonSerializer"){
        Event deserializedEvt;
        serializer >> deserializedEvt;
    
    THEN("it should produce an correspondent Events instance"){
        EventsHelper::checkEquals(deserializedEvt, evt);
    }//THEN
    }//WHEN
    }//GIVEN
}

void EventFixture::testExtendedEventSerialization(){
    GIVEN("some ExtendedEvent instance"){
        ExtendedEvent extEvt = buildSomeExtendedEvent();
    
    WHEN("serializing it"){
        JsonSerializer serializer;
        serializer << extEvt;
    
    THEN("it should produce an representation with a child object containing the user data"){
        auto jsonValue = checkEventJson(serializer, extEvt, "PhotoUpdate");
    
        const auto userValue = jsonValue[Fields::Event::user];
    
        CHECK(userValue.isObject());
        CHECK(userValue[User::Fields::id].asInt64()     == extEvt.getUserInfo().getUserId());
        CHECK(userValue[User::Fields::login]            == extEvt.getUserInfo().getLogin());
        CHECK(userValue[User::Fields::name]             == extEvt.getUserInfo().getName());
        CHECK(!userValue[User::Fields::hasProfileImage].empty());
        CHECK(userValue[User::Fields::hasProfileImage].asBool() == extEvt.getUserInfo().hasProfileImage());
    
    }//THEN
    }//WHEN
    }//GIVEN
}

void EventFixture::testExtendedEventDeserialization(){
    GIVEN("some ExtendedEvent serialized representation"){
        ExtendedEvent expectedEvt = buildSomeExtendedEvent();
    
        JsonSerializer serializer;
        serializer << expectedEvt;
    
    WHEN("deserializing it into a ExtendedEvent instance"){
        ExtendedEvent deserialized;
    
        serializer >> deserialized;
    
    THEN("it should fill its fields with correspondent values"){
        EventsHelper::checkEquals(deserialized, expectedEvt);
    
    }//THEN
    }//WHEN
    }//GIVEN
}

void EventFixture::testExtendedEventUserInfo(){
    CHECK_FALSE(ExtendedEvent().hasUser());
    CHECK_FALSE(ExtendedEvent({}, User{}).hasUser());
    CHECK(ExtendedEvent{User{1}}.getUserId() == 1);
}//test


/* **********************************************************************************************/

Event EventFixture::makeSomeEvent(){
    return {1, EventType::PhotoCreation, 1, 1, Date(1900, 1, 1, 1, 1, 1, 0)};
}

ExtendedEvent EventFixture::buildSomeExtendedEvent(){
    auto user = User{2, "userlogin"}.name("John Smith").profileImage("someimage.jpeg");
    
    return ExtendedEvent{user, EventType::PhotoUpdate, user.getId(), 3, {}};
}

Json::Value EventFixture::checkEventJson(JsonSerializer & serializer, const Event & evt, const std::string & eventType)
{
    Json::Value evtJson;
    serializer >> evtJson;

    CHECK(evtJson.type() == Json::objectValue);
    CHECK(evtJson[Fields::Event::id]            == (Json::Value::Int64)evt.getId());
    CHECK(evtJson[Fields::Event::type]          == eventType);
    CHECK(evtJson[Fields::Event::userId]        == (Json::Value::Int64)evt.getUserId());
    CHECK(evtJson[Fields::Event::entityId]      == (Json::Value::Int64)evt.getEntityId());
    CHECK(evtJson[Fields::Event::createdAt]     == evt.getCreation().toString());

    return evtJson;
}
