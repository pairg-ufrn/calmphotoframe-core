#include "TestMacros.h"

#include "model/permissions/ProtectedEntity.h"
#include "model/photo/PhotoContext.h"
#include "model/user/User.h"
#include "serialization/json/JsonSerializer.h"

#include "helpers/PhotoContextHelper.h"

using namespace calmframe;
using namespace std;

class ProtectedEntityFixture{
public: //test
    void testProtectedEntitySerialization();
    void testProtectedEntityDeserialization();
public:
    JsonSerializer serializer;

    Json::Value checkProtectedEntityJson(JsonSerializer & serializer, const ProtectedEntity<PhotoContext> & entity);
    const char * permissionToString(PermissionLevel permissionLevel) const;
};

namespace Catch {
    template<> struct StringMaker<PermissionLevel> {
        static std::string convert( PermissionLevel const& value ) {
            return calmframe::toString( value );
        }
    };
}

#define TEST_TAG "[ProtectedEntity]"
#define FIXTURE ProtectedEntityFixture

/* **********************************************************************************************/

FIXTURE_SCENARIO("ProtectedEntity serialization"){
    this->testProtectedEntitySerialization();
}//SCENARIO

FIXTURE_SCENARIO("ProtectedEntity deserialization"){
    this->testProtectedEntityDeserialization();
}//SCENARIO

/* **********************************************************************************************/

void ProtectedEntityFixture::testProtectedEntitySerialization(){

GIVEN("some ProtectedEntity instance"){
    ProtectedEntity<PhotoContext> pEntity{1, PermissionLevel::Admin, PhotoContext{"some context"}};

WHEN("serializing it using a json serializer"){

//    serializer << pEntity;
    serializer.serialize(pEntity);

    CHECK(dynamic_cast<Serializable *>(&pEntity) == &pEntity);
    Serializable & serializable = pEntity;
    CHECK(&serializable == &pEntity);


THEN("it should serialize the ProtectedEntity json fields"){
    checkProtectedEntityJson(serializer, pEntity);

THEN("it should also serialize the stored entity"){
    PhotoContext deserializedCtx;
    serializer >> deserializedCtx;

    CHECK(deserializedCtx == pEntity.getEntity());
}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test

void ProtectedEntityFixture::testProtectedEntityDeserialization(){

GIVEN("some ProtectedEntity with a deserializable entity"){
    ProtectedEntity<PhotoContext> protectedEntity{
        Permission{1, 1, PermissionLevel::Read}
        , PhotoContext(1, "some name", Visibility::EditableByOthers)};

GIVEN("a serialized representation of it"){
    serializer << protectedEntity;

WHEN("deserializing it into another instance"){
    ProtectedEntity<PhotoContext> copyEntity;
    serializer >> copyEntity;

THEN("it should create an equals representation"){
    PhotoContextHelper::checkEquals(copyEntity.getEntity(), protectedEntity.getEntity());
    CHECK(copyEntity.getUserId() == protectedEntity.getUserId());
    CHECK(copyEntity.getPermissionLevel() == protectedEntity.getPermissionLevel());
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

/* **********************************************************************************************/
Json::Value ProtectedEntityFixture::checkProtectedEntityJson(JsonSerializer & serializer
    , const ProtectedEntity<PhotoContext> & entity)
{
    Json::Value content;
    serializer >> content;

    CAPTURE(content.toStyledString());

    auto permissionField = content[ProtectedEntityFields::permission];
    CHECK(permissionField.isObject());
    CHECK(permissionField[EntityPermission::Fields::permissionLevel] == calmframe::toString(entity.getPermissionLevel()));
    CHECK(permissionField[EntityPermission::Fields::userId].asInt() == entity.getUserId());

    return content;
}


/* **********************************************************************************************/

