#include "TestMacros.h"

#include "model/image/Image.h"

#include <string>

using namespace std;
using namespace calmframe;

static const string VALID_IMAGE_PATH = "test/data/test_exif.jpg";

class ImageFixture{
public:
    void testOpenAndCloseImage();
    void testMoveImageValues();
};

#define FIXTURE ImageFixture
#define TEST_TAG "[Image]"

FIXTURE_SCENARIO("Open and close image"){
    this->testOpenAndCloseImage();
}

FIXTURE_SCENARIO("Move Image"){
    this->testMoveImageValues();
}


void ImageFixture::testOpenAndCloseImage(){

GIVEN("some valid Image object"){
    Image image;
    image.setImagePath(VALID_IMAGE_PATH);

WHEN("open is called"){
    REQUIRE(image.open());
    THEN("the image should be open"){
        REQUIRE(image.isOpen());
    }//THEN
AND_WHEN("calling getContent"){
    std::istream & imageStream = image.getContent();
    THEN("it should return an reference to a good stream"){
        CHECK(imageStream.good());
    }//THEN
AND_WHEN("calling close"){
    image.close();
    THEN("the image should not be open"){
        CHECK(!image.isOpen());
    }//THEN
AND_WHEN("calling getContent"){
    THEN("it should throw an exception"){
        CHECK_THROWS(image.getContent());
    }//THEN

}//WHEN
}//WHEN
}//WHEN
}//WHEN

}//GIVEN

}

void ImageFixture::testMoveImageValues(){
GIVEN("some valid open image"){
    Image img{VALID_IMAGE_PATH};
    REQUIRE(img.open());
    REQUIRE(img.isValid());
    REQUIRE(img.isOpen());

WHEN("moving it to another variable"){
    Image other = std::move(img);

THEN("it should be empty and closed"){
    CHECK_FALSE(img.isOpen());
    CHECK(img.getImagePath().empty());
    CHECK(img.getType().empty());

THEN("the other variable should keep the initial values"){
    CHECK(other.isOpen());
    CHECK(other.isValid());
    CHECK(other.getImagePath() == VALID_IMAGE_PATH);

}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test
