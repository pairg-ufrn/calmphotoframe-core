#include "DateTest.h"

#include "Poco/Timezone.h"
#include "utils/Log.h"

FIXTURE_SCENARIO("Date created from current time"){
    testDefaultCreation();
}//SCENARIO

FIXTURE_SCENARIO("Date can be created from a time_t value"){
    testCreateFromTime_t();
}//SCENARIO

FIXTURE_SCENARIO("Convert to string"){
    testConversionToString();
}//SCENARIO

FIXTURE_SCENARIO("Convert from ISO-8601 string"){
    testConversionFromString();
}//SCENARIO

FIXTURE_SCENARIO("Equals comparison"){
    testEqualsComparison();
}//SCENARIO


/* **********************************************************************************************/

void DateFixture::testDefaultCreation(){
WHEN("creating a Date instance with no arguments"){
    Date now;

    std::time_t nowTime = std::time(0);

THEN("it should be set to current time with local timezone offset"){
    checkEquals(now, nowTime);

}//THEN
}//WHEN

}

void DateFixture::testCreateFromTime_t(){

GIVEN("a time_t value"){
    time_t time = createSomeTime();
    const std::tm  expectedTime = *std::localtime(&time);
    CAPTURE(expectedTime.tm_isdst);

WHEN("creating a Date time from that value"){
    Date date(time);

THEN("they should have correspondent values"){
    checkEquals(date, time);

}//THEN
}//WHEN
}//GIVEN

}//test

void DateFixture::testConversionToString(){

GIVEN("some date instance"){
    time_t time = createTime(Constants::someYear, Constants::someMonth, Constants::someDay
                            , Constants::someHour, Constants::someMinute, Constants::someSecond);
    Date now(time);

WHEN("converting it to string"){
    string nowStr = now.toString();

THEN("it should generate a UTC string representation of that date on current timezone"){

    char tmpStr[100];
    snprintf(tmpStr, 100, "%4d-%02d-%02dT%02d:%02d:%02d%+03d:%02d"
            , now.getYear(), now.getMonth(), now.getDay()
            , now.getHour(), now.getMinute(), now.getSecond()
            , now.getZoneOffset(), 0);

    CHECK(nowStr == string(tmpStr));
}//THEN
}//WHEN
}//GIVEN

}

void DateFixture::testConversionFromString(){

GIVEN("a date represented as a ISO 8601 string"){
    Date expectedDate;
    string dateStr = expectedDate.toString();

WHEN("creating a Date from it"){
    Date date(dateStr);

THEN("it should have the correspondent field values"){
    CHECK(date.getYear()        == expectedDate.getYear());
    CHECK(date.getMonth()       == expectedDate.getMonth());
    CHECK(date.getDay()         == expectedDate.getDay());
    CHECK(date.getHour()        == expectedDate.getHour());
    CHECK(date.getMinute()      == expectedDate.getMinute());
    CHECK(date.getSecond()      == expectedDate.getSecond());
    CHECK(date.getZoneOffset()  == expectedDate.getZoneOffset());
}//THEN
}//WHEN
}//GIVEN

}

void DateFixture::testEqualsComparison(){
    CHECK(Date(createSomeTime()) == Date(createSomeTime()));
    CHECK(Date() == Date());
    CHECK(Date(createSomeTime()) != Date());
}//test

/* **********************************************************************************************/


void DateFixture::checkEquals(const Date & date, std::time_t time){
    const std::tm  expectedTime = *std::localtime(&time);
    CHECK(date.getYear()         == (expectedTime.tm_year + 1900));
    CHECK(date.getMonth()        == expectedTime.tm_mon + 1); //tm months starts in 0
    CHECK(date.getDay()          == expectedTime.tm_mday);
    CHECK(date.getHour()         == expectedTime.tm_hour);
    CHECK(date.getMinute()       == expectedTime.tm_min);
    CHECK(date.getSecond()       == expectedTime.tm_sec);
    CHECK(date.getZoneOffset()   == getTimezoneOffset(time));
}

time_t DateFixture::createTime(int year, int month, int day, int hour, int minute, int second){
    std::time_t t = std::time(0);
    std::tm some_time = *std::localtime(&t);
    some_time.tm_year = year - 1900;
    some_time.tm_mon = month - 1;
    some_time.tm_mday = day;
    some_time.tm_hour = hour;
    some_time.tm_min = minute;
    some_time.tm_sec = second;

    return std::mktime(&some_time);
}

int DateFixture::getTimezoneOffset(time_t nowTime){
    int offset = Poco::Timezone::utcOffset()/3600; //convert to seconds
    Poco::Timestamp t = Poco::Timestamp::fromEpochTime(nowTime);
    //If is Daylight Saving Time, add 1 hour
    return Poco::Timezone::isDst(t) ? offset + 1 : offset;
}

time_t DateFixture::createSomeTime()
{
    return createTime(Constants::someYear, Constants::someMonth, Constants::someDay
                           , Constants::someHour, Constants::someMinute, Constants::someSecond);
}
