#ifndef DATETEST_H
#define DATETEST_H

#include "TestMacros.h"

#include "model/events/Date.h"

#include <ctime>

using namespace calmframe;
using namespace std;

class DateFixture{
public:
    void testDefaultCreation();
    void testCreateFromTime_t();
    void testConversionToString();
    void testConversionFromString();
    void testEqualsComparison();
public:
    int getTimezoneOffset(time_t nowTime);
    void checkEquals(const Date & date, std::time_t time);
    time_t createTime(int year, int month, int day, int hour, int minute, int second);
    time_t createSomeTime();
};

#define TEST_TAG "[Date]"
#define FIXTURE DateFixture

namespace Constants {
    const int someYear  = 2000;
    const int someMonth = 1;
    const int someDay   = 1;
    const int someHour  = 12;
    const int someMinute= 2;
    const int someSecond= 56;
}//namespace


#endif // DATETEST_H

