#include "TestMacros.h"
#include "helpers/PhotoContextHelper.h"

#include "model/photo/PhotoContext.h"
#include "serialization/json/JsonSerializer.h"

using namespace calmframe;

class PhotoContextFixture{
public:
    void testSerialization();
    void testDeserialization();
    void testSerializeInvalidValues();
    void testContextOrdering();
public:
    JsonSerializer serializer;

    void checkSerializerValue(const std::map<std::string, Json::Value> & expectedValue);
};

#define TEST_TAG "[PhotoContext]"
#define FIXTURE PhotoContextFixture

/*********************************************************************************************************************/

FIXTURE_SCENARIO("PhotoContext serialization"){
    this->testSerialization();
}

FIXTURE_SCENARIO("PhotoContext deserialization"){
    this->testDeserialization();
}

FIXTURE_SCENARIO("Invalid field values should not be included on serialization"){
    this->testSerializeInvalidValues();
}

FIXTURE_SCENARIO("PhotoContext ordering"){
    this->testContextOrdering();
}

/*********************************************************************************************************************/

void PhotoContextFixture::testSerialization(){

GIVEN("some PhotoContext instance"){
    PhotoContext ctx{"some name", Visibility::VisibleByOthers};

WHEN("serializing it to json"){
    serializer << ctx;

THEN("it should produce a json instance with correspondent field values"){
    checkSerializerValue({
        {PhotoContextFields::name, ctx.getName()}
      , {PhotoContextFields::visibility, ctx.getVisibility().toString()} });

}//THEN
}//WHEN
}//GIVEN

}

void PhotoContextFixture::testDeserialization(){
GIVEN("some serialized representation of a PhotoContext"){
    PhotoContext ctx{"Any name", Visibility::Private};
    serializer << ctx;

WHEN("deserializing it"){
    PhotoContext deserialized;
    serializer >> deserialized;

THEN("it should contains the same values of serialized context"){
    PhotoContextHelper::checkEquals(ctx, deserialized);
    CHECK(ctx == deserialized);

}//THEN
}//WHEN
}//GIVEN

}

void PhotoContextFixture::testSerializeInvalidValues(){
GIVEN("a PhotoContext instance with a unknown visibility"){
    PhotoContext ctx;

WHEN("serializing it"){
    serializer << ctx;

THEN("it should not contains a visibility field"){
    CHECK(serializer.contains(PhotoContextFields::name) == true);
    CHECK(serializer.contains(PhotoContextFields::visibility) == false);
    CHECK(serializer.contains(PhotoContextFields::rootCollectionId) == false);
    CHECK(serializer.contains(PhotoContextFields::id) == false);
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextFixture::testContextOrdering(){
THEN("PhotoContext should be ordenable"){
    CHECK(PhotoContext(1, "", Visibility::EditableByOthers) < PhotoContext(2 , "", Visibility::VisibleByOthers));
    CHECK(PhotoContext(1, "", Visibility::VisibleByOthers) < PhotoContext(1 , "", Visibility::EditableByOthers));

    std::vector<PhotoContext> contexts = {{
        {1, "ctx", Visibility::Private}, {2, "", Visibility::EditableByOthers}, {3, "", Visibility::VisibleByOthers}
    }};

    CHECK(contexts[0] < contexts[1]);
    CHECK(contexts[0] < contexts[2]);
    CHECK(contexts[1] < contexts[2]);

    CHECK_FALSE(contexts[2] < contexts[1]);
    CHECK_FALSE(contexts[2] < contexts[0]);
    CHECK_FALSE(contexts[1] < contexts[0]);

    std::sort(contexts.begin(), contexts.end());

    for(int i=0;i< contexts.size(); ++i){
        CHECK(contexts[i].getId() == i + 1);
    }


}//THEN

}//test


/*********************************************************************************************************************/

void PhotoContextFixture::checkSerializerValue(const std::map<std::string, Json::Value> & expectedValue){
    Json::Value value;
    serializer >> value;

    for(auto nameValue : expectedValue){
        CHECK(value[nameValue.first] == nameValue.second);
    }
}
