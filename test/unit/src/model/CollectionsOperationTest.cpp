#include "catch.hpp"

#include "model/CollectionsOperation.h"
#include "serialization/json/JsonSerializer.h"

#include <string>

using namespace std;
using namespace calmframe;

namespace Test {
namespace Constants {
    static const char * COLLECTIONOPERATION_JSON_NULLOPERATION =
            "{\"operation\":null,\"photoIds\":[1,3],\"subcollectionsIds\":[],\"targetCollectionIds\":[9]}";
}//namespace
}//namespace

class CollectionOperationFixture{
public:
    JsonSerializer jsonSerializer;
};

SCENARIO("CollectionsOperation deserialization with null operation","[CollectionsOperation]"){
    CollectionOperationFixture fixture;

GIVEN("some json representation of a CollectionsOperation with null as the operation value"){
    const char * collectionOperationJson = Test::Constants::COLLECTIONOPERATION_JSON_NULLOPERATION;

WHEN("deserializing this into an CollectionsOperation"){
    CollectionsOperation collectionOperation;
    string(collectionOperationJson) >> fixture.jsonSerializer >> collectionOperation;
THEN("the operation value should be unknown"){
    CHECK(collectionOperation.getOperationType() == CollectionsOperation::UNKNOWN);

}//THEN
}//WHEN
}//GIVEN

}//SCENARIO

