#include "helpers/ControllerPermissionsFixture.h"
#include "helpers/PhotoHelper.h"
#include "helpers/PhotoCollectionHelper.h"

#include "api/Api.h"
#include "api/PhotoController.h"
#include "services/PhotoManager.h"



using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::api;

class PhotoController_PermissionsFixture : public ControllerPermissionsFixture{
public:
    void testRejectListPhotosFromContextWithoutReadPermission();
    void testRejectListPhotosFromCollectionWithoutReadPermission();
    void testRejectGetPhotoWithoutReadPermission();
    void testRejectGetPhotoImageWithoutReadPermission();
    void testRejectUpdatePhotoWithoutWritePermission();
    void testRejectDeletePhotoWithoutWritePermission();
    void testRejectCreatePhotoWithoutWritePermission();
public:
    PhotoManager photoManager;
    PhotoController photoController;
    PhotoController_PermissionsFixture();

    long createEntity(long ctxId);
    void putCollectionParameter(long collId);
};

#define FIXTURE PhotoController_PermissionsFixture
#define TEST_TAG "[PhotoController]"

/*********************************************************************************************************************/

FIXTURE_SCENARIO("Api: a user cannot list photos of some context without read permission on it"){
    this->testRejectListPhotosFromContextWithoutReadPermission();
}

FIXTURE_SCENARIO("Api: a user cannot list photos of some photoCollection without read permission on its context"){
    this->testRejectListPhotosFromCollectionWithoutReadPermission();
}

FIXTURE_SCENARIO("Api: a user cannot get a photo without read permission on its context"){
    this->testRejectGetPhotoWithoutReadPermission();
}

FIXTURE_SCENARIO("Api: a user cannot get a photo image without read permission on its context"){
    this->testRejectGetPhotoImageWithoutReadPermission();
}

FIXTURE_SCENARIO("API: a user cannot update a photo without write permission on its context"){
    this->testRejectUpdatePhotoWithoutWritePermission();
}

FIXTURE_SCENARIO("API: a user cannot remove a photo without write permission on its context"){
    this->testRejectDeletePhotoWithoutWritePermission();
}

FIXTURE_SCENARIO("API: a user cannot create a photo on a context, withou write permission on it"){

}

/*********************************************************************************************************************/

void PhotoController_PermissionsFixture::testRejectListPhotosFromContextWithoutReadPermission(){
//    this->testForbiddenRequest("GET", PermissionLevel::Read, NULL, "/");
    this->testForbiddenListItemsWithoutPermission(PermissionLevel::Read);
}

void PhotoController_PermissionsFixture::testRejectListPhotosFromCollectionWithoutReadPermission(){

GIVEN("some context on db"){
    long ctxId = createPrivateContext();
    PermissionLevel permissionLevel = PermissionLevel::Read;

GIVEN("a user without read permission on that context"){
    putUserWithoutPermission(ctxId, permissionLevel);

GIVEN("a PhotoCollection on the given context with some photos"){
    long collId = PhotoCollectionHelper::insertCollection(-1, ctxId);
    PhotoCollectionHelper::insertPhotosAtCollection(collId, 3);

WHEN("a request is made by that user to list photos route and the request has a collection parameter"){
    putCollectionParameter(collId);
    executeForbiddenRequest("GET", permissionLevel, NULL, "/");

THEN("a forbidden message should be sent as response"){
    verifyForbiddenResponse();
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//test

void PhotoController_PermissionsFixture::testRejectGetPhotoWithoutReadPermission(){
    this->testForbiddenRequest("GET", PermissionLevel::Read);
}//test

void PhotoController_PermissionsFixture::testRejectGetPhotoImageWithoutReadPermission(){

GIVEN("some context on db"){
    long ctxId = createPrivateContext();

GIVEN("a user without read permission on that context"){
    PermissionLevel permissionLevel = PermissionLevel::Read;
    putUserWithoutPermission(ctxId, permissionLevel);

GIVEN("a some photo on that context"){
    long photoId = PhotoHelper::insertPhoto(Photo{{},{}, ctxId});

WHEN("a request is made to get the image of that photo"){
    executeForbiddenRequest("GET", permissionLevel, NULL, route(photoId, "image"));

THEN("a forbidden message should be sent as response"){
    verifyForbiddenResponse();
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//test

void PhotoController_PermissionsFixture::testRejectUpdatePhotoWithoutWritePermission(){
    Photo updatePhoto = {PhotoDescription{"other description"}};
    testForbiddenRequest("POST", PermissionLevel::Write, &updatePhoto);
}//test

void PhotoController_PermissionsFixture::testRejectDeletePhotoWithoutWritePermission(){
    testForbiddenRequest("DELETE", PermissionLevel::Write);
}//test

void PhotoController_PermissionsFixture::testRejectCreatePhotoWithoutWritePermission(){
    Photo photoToInsert;
    testForbiddenRequest("POST", PermissionLevel::Write, &photoToInsert, "/");
}//test

/*********************************************************************************************************************/


PhotoController_PermissionsFixture::PhotoController_PermissionsFixture()
    : photoManager()
    , photoController(&photoManager)
{
    setController(&photoController);
}

long PhotoController_PermissionsFixture::createEntity(long ctxId){
    return PhotoHelper::insertPhoto(Photo{{}, {}, ctxId});
}

void PhotoController_PermissionsFixture::putCollectionParameter(long collId)
{
    request.putParameter(api::parameters::PhotoCollectionId, to_string(collId));
}
