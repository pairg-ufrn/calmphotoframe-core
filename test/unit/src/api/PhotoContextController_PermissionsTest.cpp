#include "helpers/ControllerPermissionsFixture.h"

#include "api/PhotoContextController.h"
#include "services/PhotoContextManager.h"

using namespace calmframe;
using namespace calmframe::data;

class PhotoContextController_PermissionsFixture : public ControllerPermissionsFixture{
public:
    void testGetContextWithReadPermission();
    void testRejectGetContextWithoutReadPermission();
    void testRejectUpdateContextWithoutAdminPermission();
    void testRejectRemoveContextWithoutAdminPermission();

public:
    PhotoContextManager contextManager;
    PhotoContextController contextController;

    PhotoContextController_PermissionsFixture();

    long createEntity(long ctxId){
        return ctxId;
    }
};

#define FIXTURE PhotoContextController_PermissionsFixture
#define TEST_TAG "[PhotoContextController]"


/*********************************************************************************************************************/

FIXTURE_SCENARIO("Api: Allow GET a context if user has read permission (or greater)"){
    this->testGetContextWithReadPermission();
}
FIXTURE_SCENARIO("Api: Reject GET a context if user does not have read permission (or greater)"){
    this->testRejectGetContextWithoutReadPermission();
}

FIXTURE_SCENARIO("Api: Reject UPDATE a context if user does not have admin permission"){
    this->testRejectUpdateContextWithoutAdminPermission();
}
FIXTURE_SCENARIO("Api: Reject REMOVE a context if user does not have admin permission"){
    this->testRejectRemoveContextWithoutAdminPermission();
}

/*********************************************************************************************************************/

void PhotoContextController_PermissionsFixture::testGetContextWithReadPermission(){

GIVEN("some context on db"){
    long ctxId = createPrivateContext();

GIVEN("a user with read permission"){
    putUserWithPermission(ctxId, PermissionLevel::Read);

WHEN("getting that context"){
    executeGetRequest(route(ctxId));

THEN("a successful json message should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoContextController_PermissionsFixture::testRejectGetContextWithoutReadPermission(){
    testForbiddenRequest("GET", PermissionLevel::Read);
}

void PhotoContextController_PermissionsFixture::testRejectUpdateContextWithoutAdminPermission(){
    PhotoContext updateCtx{"any name", Visibility::EditableByOthers};
    testForbiddenRequest("POST", PermissionLevel::Admin, &updateCtx);
}

void PhotoContextController_PermissionsFixture::testRejectRemoveContextWithoutAdminPermission()
{
    this->testForbiddenRequest("DELETE", PermissionLevel::Admin);
}

/*********************************************************************************************************************/

PhotoContextController_PermissionsFixture::PhotoContextController_PermissionsFixture()
{
    contextController.setContextManager(&contextManager);
    setController(&contextController);
}


