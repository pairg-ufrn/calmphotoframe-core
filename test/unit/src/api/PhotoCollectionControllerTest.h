#ifndef PHOTOCOLLECTIONCONTROLLERTEST_H
#define PHOTOCOLLECTIONCONTROLLERTEST_H

#include "TestMacros.h"

#include "api/Api.h"
#include "api/PhotoCollectionController.h"

#include "model/photo/PhotoCollection.h"
#include "services/PhotoCollectionManager.h"

#include "model/CollectionsOperation.h"

#include "network/ContentTypes.hpp"
#include "network/StatusResponses.h"
#include "network/Url.h"

#include "serialization/json/JsonSerializer.h"
#include "utils/StringCast.h"

#include "data/data-sqlite/SqliteData.h"

#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"
#include "mocks/MockRequestRouter.h"

#include "helpers/RequestUtils.h"
#include "helpers/PhotoContextHelper.h"
#include "utils/ScopedDatabase.h"
#include "helpers/PhotoHelper.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/WebControllerFixture.h"

#include <vector>
#include <map>
#include <iterator>
#include <sstream>

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;
using namespace calmframe::data;
using namespace calmframe::network;
using namespace calmframe::api;

namespace Test {
namespace Constants {
    #define PHOTOS_URL_PATTERN "/:ctxId/photos"

    static const char * COLLECTION_NAME = "alguma coleção";
    static const char * OTHER_COLLECTION_NAME = "outra coleção";
    static const char * INVALID_JSON = "\"name\" : \"invalid json. No curly braces.\"";

    static const char * PHOTOIDS_RESPONSE_MEMBER = PhotoCollectionFields::PHOTO_IDS;

    static const char * COLLECTION_NAME_FIELD    = PhotoCollectionFields::NAME;

    static const char * MOVE_ITEMS_SUBPATH = CollectionsControllerFields::MoveItemsSubpath;

    static const char * CTX_NAME = "Context";
    static const int COLLECTIONS_COUNT = 4;
    static const char * PARAM_PHOTOCONTEXT = "context";
}
}//namespace

#define TEST_TAG "[PhotoCollectionController]"
#define FIXTURE PhotoCollectionControllerFixture

class PhotoCollectionControllerFixture;

typedef PhotoCollectionControllerFixture Fixture;


class PhotoCollectionControllerFixture : public WebControllerFixture
{
public: //tests
    void testListCollectionsOfContext();

    void testGetCollection();
    void testGetFullCollection();
    void testGetFullCollectionWithoutItems();
    void testGetContextRootCollection(bool hasContextParam);

    void testCreateCollection();
    void testCreateSubcollectionRequest();
    void testRejectCreateCollectionWithoutContextOrParent();

    void testUpdateCollectionRequest();
    void testMoveCollectionItems();

    void testDeleteCollection();
public:
    PhotoCollectionManager collectionManager;
    PhotoCollectionController controller;

    ScopedSqliteDatabase tmpDatabase;
public:
    PhotoCollectionControllerFixture();

    long requireContextPermission(MockRequest & request, PermissionLevel requiredPermission);
    long setupRequestUserAndContext(PermissionLevel requiredPermission);
    string buildPhotosUrl(long collId);

    void setupJsonRequest();
    void setupNonJsonRequest();
    void setupCollectionRequestUrl(long collId);
//    void setupCollectionRequestJSONBody(const std::string & collName);

    void setupRequestPhotoCollection(const string & collectionName);
    void setupRequestPhotoCollection(const string &collectionName, long collectionParent);

    void setupRequestInvalidJsonContent();
    string getCollectionUrl(long collectionId);

    void setupGetCollectionRequest(long collectionId);
    void setupAddPhotosRequest(long collectionId, const std::vector<Photo> & photosToAdd);

    void setupAddPhotosToCollectionsRequest(const vector<PhotoCollection> & collections, const vector<Photo> & photos);

    void setupRemovePhotosFromCollectionsRequest(const vector<PhotoCollection> & collections, const vector<Photo> & photos);

    void setupAddPhotosAndSubCollectionsRequest(const std::set<long> & targetCollectionsId
            , const std::set<long> & photoIds, const std::set<long> & subcollectionIds);

    void setupChangePhotosAndCollectionsRequest(CollectionsOperation::OperationTypes operationType,
            const vector<Photo>& photos, const vector<PhotoCollection>& collections);

    void setupChangePhotosAndCollectionsRequest(CollectionsOperation::OperationTypes operationType,
            const std::set<long> & targetCollectionsId
            , const std::set<long> & photoIds, const std::set<long> & subcollectionIds);
    void setupBulkDeleteCollectionRequest(const set<long> & collectionIds);

    template<typename Iterator>
    set<long> getIds(Iterator begin, Iterator end){
        set<long> ids;

        Iterator iter = begin;
        while(iter != end){
            ids.insert((*iter).getId());
            ++iter;
        }
        return ids;
    }

    size_t getCollectionCount(){
        return Data::instance().collections().listAll().size();
    }

    string getCollectionOperationContent(CollectionsOperation::OperationTypes type,
            const std::set<long> & targetCollectionsId
            , const std::set<long> & photoIds, const std::set<long> & subcollectionIds);

//    string makeMoveItemsPath(long collectionId);

    CollectionsOperation makeMoveCollectionOperation(const set<long> & targetCollections
        , const set<long> & photosToMove, const set<long> & collectionsToMove);

    PhotoCollection putPhotosOnCollection(long collectionId, int numPhotos);

    void insertSubCollections(PhotoCollection & coll);

    PhotoCollection makeCollection(int collNumber);

    bool isBadRequestResponse(MockResponse & response);

    bool isSuccessfulJsonResponse(MockResponse & response);

//    vector<PhotoCollection> deserializeCollectionsResponse(const std::string & collectionsJsonResponse);
    vector<Photo> deserializePhotos(const std::string & property);

    set<long> getRequestIds(const string & memberName);

    CollectionsOperation getCollectionOperation(MockRequest & request);
    set<long> getCollectionOperationRequestPhotoIds();

    set<long> getCollectionOperationRequestCollectionIds();

    void assertAllCollectionsContainsPhotos(const vector<PhotoCollection> & allCollections
            , const set<long> & collectionIds, const set<long> & photoIds);

    void assertCollectionContainsPhotos(const PhotoCollection & coll, const set<long> & photoIds);

    void assertAllCollectionsNotContainsPhotos(const vector<PhotoCollection> & allCollections
            , const set<long> & collectionIds, const set<long> & photoIds);
    void assertCollectionNotContainsPhotos(const PhotoCollection & coll, const set<long> & photoIds);

    void insertCollectionsWithPhotosAndSubCollections(set<long> & outParentCollectionIds, set<long> & outPhotoIds, set<long> & outSubCollections,
            long ctxId=-1);

    void insertCollectionsWithPhotos(vector<PhotoCollection> & outCollections, vector<Photo> & outPhotos);

    vector<PhotoCollection> assertResponseContainsCollections(const set<long>& expectedCollectionIds);

    void assertResponseContainsChangedCollections(const set<long> & expectedCollectionIds);
    void assertCollectionIdsDoesNotExist(const set<long> & collectionIds);

    PhotoCollection assertCollectionWasCreated(const string & collName);

    void assertResponseContainsCollectionAsJson(const PhotoCollection collection);

    Json::Value assertPhotoCollectionResponse(long collectionId, const PhotoCollection & collection, PhotoCollection * outCollectionResponse = NULL);

    void assertResponseContainsExpandedPhotos(const PhotoCollection & collection, Json::Value & jsonResponse
            , Deserializer & serializer);

    void assertResponseContainsExpandedSubCollections(const PhotoCollection & collection, Json::Value & jsonResponse
            , Deserializer & serializer);

    void assertCollectionsContainsSubCollections(const set<long> & parentCollectionsIds, const set<long> & subcollections);
};

#endif // PHOTOCOLLECTIONCONTROLLERTEST_H

