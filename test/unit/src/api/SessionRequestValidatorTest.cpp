#include "TestMacros.h"
#include "helpers/SessionHelper.h"

#include "api/SessionRequestValidator.h"

#include "data/Data.h"

#include "network/Request.h"
#include "network/ContentTypes.hpp"
#include "network/Header.h"
#include "network/Headers.h"
#include "network/ParameterizedHeader.h"
#include "network/controller/ApiRoute.h"

#include "utils/TimeUtils.h"
#include "utils/Log.h"

#include "services/SessionManager.h"

#include <sstream>

#include "mocks/MockResponse.h"
#include "mocks/MockRequest.hpp"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"

using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::network;

namespace Constants{
    static const char * token = "a2dba81e0a0";
    static const char * invalidToken = "a2dba81e0a0a2dba81e0a0";

    static const long userId = 1;
    static const long decreaseTime = 2;
}

#define FIXTURE SessionRequestValidatorFixture
#define TEST_TAG "[SessionRequestValidator]"

class SessionRequestValidatorFixture{

public: //tests
    void testValidateRequestWithValidCookie();
    void testValidateRequestWithValidTokenHeader();
    void testValidateGETRequestWithTokenOnUrl();
    void testRejectRequestWithInvalidTokenCookie();
    void testRemoveExpiredSessionsFromDatabase();
    void testNonAuthenticatedRoutes();

public:
    std::shared_ptr<DataBuilder> memoryDataBuilder;
    MockRequest request;

    Session validSession;
    SessionRequestValidator validator;

    SessionRequestValidatorFixture();

    void createValidSession();

    Session createExpiredSession();

    void setupRequestWithToken(const string & token);
    void setupRequestWithHeaderToken(const string & token);
    void setupRequestWithParameterToken(const string & token);

    string generateInvalidToken();
    void checkValidate();
};

/* *****************************************************************************************************/

FIXTURE_SCENARIO("SessionRequestValidator should validate requests with valid cookie tokens"){
    testValidateRequestWithValidCookie();
}//SCENARIO

FIXTURE_SCENARIO("SessionRequestValidator should validate requests with valid token on application defined token header"){
    testValidateRequestWithValidTokenHeader();
}//SCENARIO

FIXTURE_SCENARIO("SessionRequestValidator should validate GET requests with token on url parameter"){
    testValidateGETRequestWithTokenOnUrl();
}//SCENARIO

FIXTURE_SCENARIO("SessionRequestValidator should reject requests with invalid tokens"){
    testRejectRequestWithInvalidTokenCookie();
}//SCENARIO

FIXTURE_SCENARIO("Removing expired session from database"){
    testRemoveExpiredSessionsFromDatabase();
}//SCENARIO

FIXTURE_SCENARIO("SessionRequestValidator should support non-authenticated routes"){
    testNonAuthenticatedRoutes();
}//SCENARIO

/* *****************************************************************************************************/

void SessionRequestValidatorFixture::checkValidate()
{
    CHECK(validator.validateRequest(request));

AND_THEN("that session should have had its expire time updated"){
    Session updatedSession;
    CHECK(Data::instance().sessions().get(validSession.getId(), updatedSession));
    CHECK(updatedSession.getExpireTimestamp() > validSession.getExpireTimestamp());
}
}

void SessionRequestValidatorFixture::testValidateRequestWithValidCookie(){

GIVEN("an existing and valid session on database"){
    createValidSession();
    REQUIRE(!validSession.expired());

WHEN ("trying to validate a request with a cookie containing a token to that session"){
    setupRequestWithToken(validSession.getToken());
    CHECK(request.containsHeader(Headers::Cookie));

THEN ("it should validate"){
    checkValidate();
}
}
}

}

void SessionRequestValidatorFixture::testValidateRequestWithValidTokenHeader(){
GIVEN("an existing and valid session on database"){
    createValidSession();

WHEN ("trying to validate a request with an header 'X-Token' containing a token to that session"){
    setupRequestWithHeaderToken(validSession.getToken());
    CHECK(request.containsHeader(Headers::XToken));

THEN ("it should validate"){
    checkValidate();
}
}
}

}

void SessionRequestValidatorFixture::testValidateGETRequestWithTokenOnUrl(){
GIVEN("an existing and valid session on database"){
    createValidSession();

WHEN ("trying to validate a GET request with an parameter 'token'"){
    setupRequestWithParameterToken(validSession.getToken());
    CHECK(request.getQueryParameter(string("token")).empty() == false);

THEN ("it should validate"){
    checkValidate();
}
}
}

}

void SessionRequestValidatorFixture::testRejectRequestWithInvalidTokenCookie(){

GIVEN("that there are sessions on database"){
    createValidSession();
WHEN ("trying to validate a request with a cookie containing a invalid token"){
    setupRequestWithToken(generateInvalidToken());
    CHECK(request.containsHeader(Headers::Cookie));

THEN ("it should be rejected"){
    CHECK(validator.validateRequest(request) == false);
}
}
}

}

void SessionRequestValidatorFixture::testRemoveExpiredSessionsFromDatabase(){

GIVEN("that there is an expired session on database"){
    Session expiredSession = createExpiredSession();
    CHECK(Data::instance().sessions().exist(expiredSession.getId()));
    CHECK(expiredSession.getExpireTimestamp() < TimeUtils::instance().getTimestamp());

WHEN ("trying to validate any request that contains a token cookie"){
    setupRequestWithToken(generateInvalidToken());
    CHECK(request.containsHeader(Headers::Cookie));
THEN("the request should be rejected"){
    CHECK(validator.validateRequest(request) == false);
AND_THEN ("the expired sessions should be removed"){
    CHECK(Data::instance().sessions().exist(expiredSession.getId()) == false);
}
}
}
}

}

void SessionRequestValidatorFixture::testNonAuthenticatedRoutes(){
GIVEN("some ApiRoute register on a SessionRequestValidator as non-authenticate route"){
    const string someRoute = "/path";

    ApiRoute route(someRoute, HttpMethods::POST);
    validator.putNonAuthenticatedRoute(route);

WHEN("that validator receives a request that matches that route"){
    request.setMethod(HttpMethods::names::POST);
    request.setUrl(someRoute);

THEN("it should not require an authenticated session to validate that request"){
    CHECK(validator.validateRequest(request));
}//THEN
}//WHEN
}//GIVEN

}//test


/* *****************************************************************************************************/

SessionRequestValidatorFixture::SessionRequestValidatorFixture()
    : memoryDataBuilder(std::make_shared<MemoryDataBuilder>())
{
    Data::setup(memoryDataBuilder);
}

void SessionRequestValidatorFixture::createValidSession(){
    long sessionDuration = SessionManager::instance().getSessionDuration();
    long startSessionTime = TimeUtils::instance().getTimestamp() - Constants::decreaseTime;
    validSession.setExpireTimestamp(startSessionTime + sessionDuration);
    validSession.setToken(Constants::token);
    validSession.setUserId(1);

    long id = Data::instance().sessions().insert(validSession);
    validSession.setId(id);

    CHECK(Data::instance().sessions().exist(id));

    Session dbSession;
    CHECK(Data::instance().sessions().getByToken(validSession.getToken(), dbSession));
    CHECK(dbSession == validSession);


    REQUIRE(!validSession.expired());
}

Session SessionRequestValidatorFixture::createExpiredSession(){
    long expiredTimestamp = TimeUtils::instance().getTimestamp() - Constants::decreaseTime;

    Session expiredSession;
    expiredSession.setExpireTimestamp(expiredTimestamp);
    expiredSession.setId(Constants::userId);
    expiredSession.setToken(Constants::token);

    long id = Data::instance().sessions().insert(validSession);
    Data::instance().sessions().get(id, expiredSession);

    return expiredSession;
}

void SessionRequestValidatorFixture::setupRequestWithToken(const string & token){
    SessionHelper::putTokenCookie(token, request);
}

void SessionRequestValidatorFixture::setupRequestWithHeaderToken(const string & token){
    SessionHelper::putTokenHeader(token, request);
}

void SessionRequestValidatorFixture::setupRequestWithParameterToken(const string & token){
    SessionHelper::putTokenQueryParameter(token, request);
}

string SessionRequestValidatorFixture::generateInvalidToken(){
    return string(Constants::invalidToken);
}
