#ifndef PHOTOCONTROLLERTEST_H
#define PHOTOCONTROLLERTEST_H

#include <fakeit.hpp>

#include "api/Api.h"
#include "api/PhotoController.h"

#include "network/controller/ControllerRouter.h"
#include "network/MultipartRequestReader.h"

#include "helpers/WebControllerFixture.h"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"
#include "utils/ScopedFile.h"
#include "utils/ScopedDatabase.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::network;
using namespace calmframe::data;
using namespace calmframe::api;
using namespace fakeit;

class PhotoControllerFixture : public WebControllerFixture{

public: //Test Cases
    void testListPhotosByContext();
    void testGetPhotosFromCollection();
    void testGetPhotosFromCollectionsHierarchy();
    void testFilterPhotosFromCollectionsUsingQuery(bool createHierarchy);

    void testGetPhotosWithInvalidSubUrl();
    void testGetSomePhoto();
    void testGetPhotoWithPermission();
    void testGetInexistentPhoto();
    void testGetImage();
    void testPhotoCreationAtCollection();
    void testPhotoCreationAtContext();
    void testTryCreatePhotoWithInvalidContentType();
    void testTryCreatePhotoWithoutContextOrCollection();
    void testDeletePhoto();
    void testUpdatePhoto();
    void testUpdatePhotoWithNotAllFields();
    void testUpdatePhotoEvent();
    void testTryUpdatePhotoWithInvalidMessage();

public:
    void verify_CreatePhoto_fromImageRequest_Behaviour(long collId=-1, long ctxId=-1, long userId=-1);
    void verify_CreatePhoto_fromMultipartRequest_Behaviour(long collId=-1, long ctxId=-1, long userId=-1);
    void verify_CreatePhotoFromImage_successful(long collId, long ctxId, long userId);
    void verifyInvalidContentType(const ContentType &contentType);
    void verifyPhotoCreation(long ctxId, long collId, long userId=-1);
    void verifyPhotoContainer(Photo createdPhoto, long ctxId, long collId);
    void verifyBadRequestResponse();

    void verify_GetSomePhoto_Behaviour();
    void verify_GetPhotos_InvalidSubUrl_Behaviour();
    void verify_GetImage_Behaviour();
    string verifyPhotoImagePath(const Photo & createdPhoto);
public:
    ScopedSqliteDatabase scopedDb;
    PhotoManager photoFrame;
    PhotoController photoController;
    ControllerRouter controllerRouter;
    shared_ptr<MockRequest> imagePart, jsonPart;

    Mock<MultipartRequestReader> multipartReaderMockup;

    PhotoControllerFixture();
    
    ~PhotoControllerFixture(){
        photoController.setMultipartReader({});
    }

    void attachControllerToUrl(const std::string & url);

    void putContextParameter(long ctxId);
    void putCollectionIdParameter(long collId);
    void checkPhotosJsonResponse(const std::set<long>& expectedPhotoIds);
    std::vector<Photo> genHierarchyWithQueryString(long ctxId, const string & queryString
                , bool createSubCollections, PhotoCollection & outRootColl, std::set<long> & outHierarchyPhotos);

    Photo assertValidJsonPhotoResponse(MockResponse & response);

    std::string getValidImage();
    Session createValidSession();
    string createTmpDir();
    void checkCreatePhotoFromMultipartResponse(MockRequest & jsonPart, long collId, long ctxId, long userId);
    void setupMultipartRequest();
    long setupSessionToken();
    void setup_CreatePhoto_ImagePart();
    void setup_CreatePhoto_JsonPart();
    void checkEventCreated(const Photo & responsePhoto, EventType eventType, long userId);
    void verifyContextIdOfCreatedPhoto(const Photo & responsePhoto, long ctxId, long collId);
    void mockupMultipartRequestReader();
};
typedef PhotoControllerFixture Fixture;

//CONSTANTS

#define PHOTOS_URL              "/photos"
#define PHOTO_1_URL             PHOTOS_URL "/1"
#define PHOTO_INVALID_URL       PHOTOS_URL "/40000"
#define GET_IMAGE_URL           PHOTOS_URL "1/image"
#define DELETE_PHOTO_URL_BASE   PHOTOS_URL
#define DELETE_PHOTO_URL        DELETE_PHOTO_URL_BASE "/1"
#define PHOTO_TO_DELETE 1

#define VALID_IMAGE_PATH "test/data/test_exif.jpg"
#define VALID_JSON_PATH "test/data/test_photo.json"
#define VALID_MULTIPART_MESSAGE "test/data/multipart_message_body.txt"
#define IMAGE_CONTENT_TYPE "image/jpeg"
#define VALID_PHOTO_ID 1

namespace Constants{
    const static string & PhotosPath = calmframe::api::paths::photos;
    const int UnexistentId = 1000000;

    const char * const PhotosUrl = "/photos";
}

#endif // PHOTOCONTROLLERTEST_H

