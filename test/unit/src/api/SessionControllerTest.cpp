#include "catch.hpp"

#include "api/SessionController.h"
#include "model/user/UserAccount.h"
#include "model/user/UserLoginInfo.h"
#include "services/AccountManager.h"
#include "services/SessionManager.h"

#include "data/Data.h"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"

#include "network/Request.h"
#include "network/ContentTypes.hpp"
#include "network/Header.h"
#include "network/Headers.h"
#include "network/Cookie.h"
#include "network/StatusResponses.h"

#include "serialization/json/JsonSerializer.h"

#include "utils/TimeUtils.h"

#include "mocks/MockResponse.h"
#include "mocks/MockRequest.hpp"
#include "helpers/RequestUtils.h"
#include "utils/Log.h"

#include <sstream>

using namespace std;
using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::network;

namespace Constants{
    static const char * userName = "userAccount";
    static const char * userPassword = "password";
    static const char * loginUrl = "/login";
    static const char * logoffUrl = "/logoff";
}

class SessionControllerFixture{
public:
    UserAccount existingUser;
    std::shared_ptr<DataBuilder> memoryDataBuilder;
    SessionController sessionController;
    MockRequest mockRequest;
    MockResponse response;
    JsonSerializer jsonSerializer;

    AccountManager accountManager;

    SessionControllerFixture(){
        memoryDataBuilder = std::make_shared<MemoryDataBuilder>();
        Data::setup(memoryDataBuilder);
        createUser();
    }

    UserAccount & createUser(){
        existingUser = accountManager.create(Constants::userName,Constants::userPassword);

        return existingUser;
    }

    void loginRequest(MockRequest & mockRequest,const std::string & login, const std::string & password){
        mockRequest.setUrl(Constants::loginUrl);
        mockRequest.setContentType(ContentTypes::JSON);
        UserLoginInfo userLoginInfo(login, password);

        jsonSerializer << userLoginInfo;
        stringstream sstream;
        sstream << jsonSerializer;

        mockRequest.setContentStr(sstream.str());
    }

    void makeLogoffRequest(MockRequest & request, const Session & session){
        request.setUrl(Constants::logoffUrl);

        putTokenCookie(request, session.getToken());
    }
    MockRequest & makeCheckConnectionRequest(MockRequest & request, const std::string & token=""){
        request.setUrl(string("/").append(Constants::ApiPaths::checkSessionCommand));

        if(!token.empty()){
            putTokenCookie(request, token);
        }
        return request;
    }

    Session authenticateUser(const UserAccount & userAccount, const std::string & password){
        return SessionManager::instance().startSession(userAccount, password);
    }
    Session createSession(const UserAccount & userAccount){
        return authenticateUser(userAccount, Constants::userPassword);
    }

    void putTokenCookie(MockRequest& request, const std::string & sessionToken){
        ParameterizedHeader cookieHeader(Headers::Cookie, "");
        cookieHeader.putParameter(Constants::sessionCookieName, sessionToken);
        request.putHeader(cookieHeader);
    }

    void postLoginRequest(){
        INFO("login: " << Constants::userName << " senha: " << Constants::userPassword);
        this->loginRequest(this->mockRequest, Constants::userName, Constants::userPassword);
        INFO("Created request");
        bool executed = this->sessionController.onPost(this->mockRequest, this->response);
        CHECK(executed == true);
    }

    Session checkCreatedSession(long userId){
        Session userSession;

        bool foundUserSession = Data::instance().sessions().getUserSession(userId, userSession);
        vector<Session> sessions = Data::instance().sessions().listAll();
        INFO("found " << sessions.size() << " sessions");
        for(int i=0; i < sessions.size(); ++i){
            Session & session = sessions[i];
            INFO("UserAccount " << session.getId() << "; token: " << session.getToken());
        }
        CHECK(foundUserSession == true);
        return userSession;
    }

    void checkSessionCookie(const Session & userSession){
        Cookie sessionCookie = this->checkResponseCookie(userSession);
        THEN("the session cookie should contain security parameters"){
            CHECK(sessionCookie.containsParameter(CookieParameters::Secure));
            CHECK(sessionCookie.containsParameter(CookieParameters::HttpOnly));
        AND_THEN("the cookie path should be the root"){
            CHECK(sessionCookie.getCookiePath() == "/");
        }//THEN
        }//THEN
    }

    Cookie checkResponseCookie(const Session & userSession){
        Header & header = response.getHeader(Headers::SetCookie);
        CHECK(!header.empty());
        Cookie sessionCookie(header);
        INFO("Cookie header name: \"" << header.getName()<< "\""<<" value : \"" << header.getValue() << "\"");
        INFO("Cookie value : \"" << sessionCookie.getValue() << "\"");
        CHECK(sessionCookie.getCookieValue() == userSession.getToken());

        return sessionCookie;
    }
    void checkLoginResponse(const Session & userSession){
        CHECK(response.getContentType().getType()    == ContentTypes::JSON.getType());
        CHECK(response.getContentType().getSubtype() == ContentTypes::JSON.getSubtype());

        Json::Reader reader;
        string responseContent = response.getContentStr();
        Json::Value jsonResponse;
        bool successfulParse = reader.parse(responseContent, jsonResponse);

        CHECK(successfulParse);
        CHECK(jsonResponse["token"] == userSession.getToken());
    }
};

SCENARIO("Client login","[SessionController]"){

GIVEN("an session controller instance"){
    SessionControllerFixture fixture;
GIVEN("an existent userAccount"){
WHEN ("POSTing to SessionController a json message with that userAccount login and password"){
    fixture.postLoginRequest();
THEN ("a session should be created to that userAccount"){
    Session userSession = fixture.checkCreatedSession(1);
THEN("the controller should put the token info on a cookie"){
    fixture.checkSessionCookie(userSession);
AND_THEN("the controller should respond with an json message containing the session token"){
    fixture.checkLoginResponse(userSession);
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("Client logoff","[SessionController]"){
GIVEN("an authenticated client"){
    SessionControllerFixture fixture;
    UserAccount & userAccount = fixture.existingUser;
    Session session = fixture.authenticateUser(userAccount, Constants::userPassword);
    CHECK(!session.expired());
    CHECK(session.getId() != Session::INVALID_ID);
    CHECK(Data::instance().sessions().exist(session.getId()));
WHEN("POSTing a logoff request passing his session token"){
    MockRequest logoffRequest;
    fixture.makeLogoffRequest(logoffRequest, session);
    INFO(logoffRequest.getHeaderValue(Headers::Cookie));
    CHECK(logoffRequest.containsHeader(Headers::Cookie));
    CHECK(fixture.sessionController.onPost(logoffRequest, fixture.response) == true);
THEN("it should respond with a ok response"){
    RequestUtils::assertOkMessage(fixture.response);
AND_THEN("that session should be removed from db"){
    CHECK(!Data::instance().sessions().exist(session.getId()));
}//THEN
}
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Check invalid session","[SessionController]"){
GIVEN("an request without a valid session token"){
    SessionControllerFixture fixture;
    MockRequest & request = fixture.makeCheckConnectionRequest(fixture.mockRequest);
WHEN("requesting to check connection to a session controller"){
    bool executed = fixture.sessionController.onGet(request, fixture.response);
    CHECK(executed == true);
THEN("it should respond with Unauthorized error"){
    CHECK(fixture.response.statusLine() == StatusResponses::UNAUTHORIZED);
    CHECK(fixture.response.getContentStr().empty());
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Check established session","[SessionController]"){
GIVEN("an request with a valid session token"){
    SessionControllerFixture fixture;
    Session session = fixture.createSession(fixture.existingUser);
    MockRequest & request = fixture.makeCheckConnectionRequest(fixture.mockRequest, session.getToken());
WHEN("requesting to check connection to a session controller"){
    bool executed = fixture.sessionController.onGet(request, fixture.response);
    CHECK(executed == true);
THEN("it should respond with an OK message"){
    CHECK(fixture.response.statusLine() == StatusResponses::OK);
AND_THEN("this message should contain an Json Object"){
    string responseContent = fixture.response.getContentStr();
    CHECK(!responseContent.empty());
    Json::Value responseJson;
    Json::Reader reader;
    CHECK(reader.parse(responseContent, responseJson) == true);
    CHECK(responseJson.isObject());
AND_THEN("this object should contain the session expiration time"){
    CHECK(responseJson.isMember(FieldNames::Session::EXPIRE_TIMESTAMP));
    long expirationTime = responseJson.get(FieldNames::Session::EXPIRE_TIMESTAMP, 0).asInt64();
    CHECK(expirationTime > TimeUtils::instance().getTimestamp());
}
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO
