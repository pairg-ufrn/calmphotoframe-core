#include "PhotoCollectionControllerTest.h"

#include "helpers/SessionHelper.h"

FIXTURE_SCENARIO("Api: List collections of some PhotoContext"){
    this->testListCollectionsOfContext();
}//SCENARIO

FIXTURE_SCENARIO("Api: Get collection"){
    this->testGetCollection();
}//SCENARIO

FIXTURE_SCENARIO("Api: Get full collection"){
    this->testGetFullCollection();
}//SCENARIO

FIXTURE_SCENARIO("Api: Get expanded collection with no items"){
    this->testGetFullCollectionWithoutItems();
}//SCENARIO

FIXTURE_SCENARIO("Api: Get context root collection"){
    this->testGetContextRootCollection(true);
}//SCENARIO

FIXTURE_SCENARIO("Api: Reject Get context root collection without context parameter"){
    this->testGetContextRootCollection(false);
}//SCENARIO

FIXTURE_SCENARIO("Update collection request"){
    testUpdateCollectionRequest();
}//SCENARIO

FIXTURE_SCENARIO("DELETE collection request"){
    this->testDeleteCollection();
}//SCENARIO

FIXTURE_SCENARIO("Create photoCollection via Api"){
    this->testCreateCollection();
}//SCENARIO


FIXTURE_SCENARIO("Create subcollection request"){
    this->testCreateSubcollectionRequest();
}//SCENARIO

FIXTURE_SCENARIO("PhotoCollectionController should reject create requests with no context or no parent id"){
    this->testRejectCreateCollectionWithoutContextOrParent();
}//SCENARIO

SCENARIO("Invalid creation collection request",TEST_TAG){
    PhotoCollectionControllerFixture fixture;
    GIVEN("an json request"){
        fixture.setupJsonRequest();
        REQUIRE(fixture.request.getContentType().getType() == Types::APPLICATION);
        REQUIRE(fixture.request.getContentType().getSubtype() == SubTypes::JSON);

        GIVEN("that this request has an invalid json"){
            fixture.setupRequestInvalidJsonContent();

        WHEN("POSTing this request to the PhotoCollectionController"){
            CHECK(fixture.controller.onPost(fixture.request, fixture.response) == true);

        THEN("it should respond with an BadRequestMessage"){
            CHECK(fixture.isBadRequestResponse(fixture.response));
        }//THEN
        }//WHEN
        }//GIVEN
    }//GIVEN

    GIVEN("an non-json request"){
        fixture.setupNonJsonRequest();
        REQUIRE(fixture.request.getContentType().getType() != Types::APPLICATION);
        REQUIRE(fixture.request.getContentType().getSubtype() != SubTypes::JSON);

    WHEN("POSTing this request to the PhotoCollectionController"){
        CHECK(fixture.controller.onPost(fixture.request, fixture.response) == true);

    THEN("it should respond with an BadRequestMessage"){
        INFO(fixture.response.getContentStr());
        CHECK(fixture.isBadRequestResponse(fixture.response));
    }//THEN
    }//WHEN
    }//GIVEN

}//SCENARIO

SCENARIO("Add multiple photos and subcollections to multiple collections",TEST_TAG){
    PhotoCollectionControllerFixture fixture;

GIVEN("some PhotoContext on db"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
    long ctxId = ctx.getId();

GIVEN("some collections on that PhotoContext"){
    int collectionCount = 5;
    PhotoCollectionHelper::insertCollections(collectionCount,-1, ctxId);
    vector<PhotoCollection> collections = Data::instance().collections().listAll();
    //inserting other non-related collection
    PhotoCollectionHelper::insertCollection(-1, ctxId);

GIVEN("some photos on db"){
    PhotoHelper::insertPhotos(6);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> photoIds = fixture.getIds(photos.begin(), photos.end());
    //inserting non-related photo
    PhotoHelper::insertPhotos(1);

GIVEN("some other collections on db"){
    set<long> allCollectionIds = fixture.getIds(collections.begin(), collections.end());
    set<long> parentCollections, subcollections;
    PhotoCollectionHelper::splitSet(allCollectionIds, parentCollections, subcollections, collectionCount/2);

GIVEN("a PhotoCollectionController"){
    PhotoCollectionController & controller = fixture.controller;

WHEN("making a POST request to bulk collections route"){
    fixture.setupAddPhotosAndSubCollectionsRequest(parentCollections, photoIds, subcollections);

AND_WHEN("this request contains an json body with a CollectionsOperation representation"){
    CollectionsOperation operation = fixture.getCollectionOperation(fixture.request);

AND_WHEN("this collection operation method is ADD"){
    CHECK(operation.getOperationType() == CollectionsOperation::ADD);

AND_WHEN("this CollectionsOperation contains the photos to add the target collections"){
    const set<long> & requestPhotoIds = operation.getPhotoIds();
    CHECK(requestPhotoIds.size() == photos.size());

    const set<long> & requestCollectionIds = operation.getTargetCollectionIds();
    CHECK(requestCollectionIds.size() == parentCollections.size());

AND_WHEN("the request contains subcollections ids on body"){
    CHECK(!operation.getSubCollectionsIds().empty());

AND_WHEN("the request contains the PhotoContext id as parameter"){
    fixture.request.putParameter(api::parameters::PhotoContextId, StrCast().toString(ctxId));

    bool handled = controller.onPost(fixture.request, fixture.response);
    REQUIRE(handled == true);

THEN("those photos should be added to the target collections"){
    vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
    fixture.assertAllCollectionsContainsPhotos(allCollections, requestCollectionIds, requestPhotoIds);

THEN("the subcollections should also be added to all target collections"){
    fixture.assertCollectionsContainsSubCollections(parentCollections, subcollections);

THEN("the controller should respond with an ok json message"){
    RequestUtils::assertOkMessage(fixture.response);
THEN("the response should contain a list with the changed collections"){
    RequestUtils::assertJsonObjectResponse(fixture.response);
    fixture.serializer.clear();
    fixture.response.getContentStr() >> fixture.serializer;

    vector<PhotoCollection> responseCollections;
    fixture.serializer.getChildCollection<PhotoCollection>(api::fields::collections
                                            , std::back_inserter<vector<PhotoCollection> >(responseCollections));

    CHECK(requestCollectionIds.size() == responseCollections.size());

    map<long, PhotoCollection> responseCollectionsById = PhotoCollection::mapById(responseCollections.begin()
                                                                       , responseCollections.end());
    vector<PhotoCollection> expectedResponseCollections = Data::instance().collections().listIn(requestCollectionIds);
    REQUIRE(expectedResponseCollections.size() == requestCollectionIds.size());

    for(size_t i=0; i < expectedResponseCollections.size(); ++i){
        long collId = expectedResponseCollections[i].getId();
        CHECK(responseCollectionsById.find(collId) != responseCollectionsById.end());
        CHECK(responseCollectionsById[collId] == expectedResponseCollections[i]);
    }
}//THEN
}//THEN
}//THEN
}//THEN
}//WHEN
}//WHEN
}//WHEN
}//WHEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO_METHOD(Fixture, "Remove multiple photos and subcollections from multiple collections",TEST_TAG){

GIVEN("some PhotoContext"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
    long ctxId = ctx.getId();

GIVEN("some collections with photos and subcollections on that PhotoContext"){
    set<long> parentCollections, subcollections, photoIds;
    insertCollectionsWithPhotosAndSubCollections(parentCollections, photoIds, subcollections, ctxId);

    //inserting other non-related collection
    PhotoCollectionHelper::insertCollection();
    //inserting non-related photo
    PhotoHelper::insertPhotos(1);

GIVEN("a PhotoCollectionController"){

WHEN("making a POST request to bulk collections route"){
    setupChangePhotosAndCollectionsRequest(CollectionsOperation::REMOVE, parentCollections, photoIds, subcollections);

AND_WHEN("this request contains an json body with a CollectionsOperation representation"){
    CollectionsOperation operation = getCollectionOperation(request);

AND_WHEN("this collection operation method is REMOVE"){
    CHECK(operation.getOperationType() == CollectionsOperation::REMOVE);

AND_WHEN("this CollectionsOperation contains the photos to remove from the target collections"){
    const set<long> & requestPhotoIds = operation.getPhotoIds();
    CHECK(requestPhotoIds == photoIds);

    const set<long> & requestCollectionIds = operation.getTargetCollectionIds();
    CHECK(requestCollectionIds == parentCollections);

AND_WHEN("the request contains subcollections ids on body"){
    CHECK(operation.getSubCollectionsIds() == subcollections);

AND_WHEN("the request contains the PhotoContext id as parameter"){
    request.putParameter(api::parameters::PhotoContextId, StrCast().toString(ctxId));

    bool handled = controller.onPost(request, response);
    REQUIRE(handled == true);
THEN("those photos should be removed from these collections"){
    vector<PhotoCollection> allCollections = Data::instance().collections().listAll();
    assertAllCollectionsNotContainsPhotos(allCollections, requestCollectionIds, requestPhotoIds);

THEN("the controller should respond with an ok json message"){
    RequestUtils::assertOkMessage(response);
    RequestUtils::assertJsonObjectResponse(response);

THEN("the response should contain a list with the changed collections"){
    assertResponseContainsChangedCollections(parentCollections);

}//THEN
}//THEN
}//THEN
}//WHEN
}//WHEN
}//WHEN
}//WHEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("DELETE multiple collections request",TEST_TAG){
    PhotoCollectionControllerFixture fixture;
GIVEN("some PhotoCollections on database"){
    int collectionNumber = 5;
    PhotoCollectionHelper::insertCollections(collectionNumber);
    vector<PhotoCollection> collections = Data::instance().collections().listAll();
GIVEN("some PhotoCollectionController instance"){
    PhotoCollectionController & controller = fixture.controller;
WHEN("making an DELETE request to controller url"){
    set<long> idsToRemove = fixture.getIds(collections.begin(), collections.end());

    fixture.setupBulkDeleteCollectionRequest(idsToRemove);

    bool handled = controller.onDelete(fixture.request, fixture.response);
    CHECK(handled == true);
WHEN("the request content is json"){
    REQUIRE(fixture.request.getContentType().is(Types::APPLICATION, SubTypes::JSON));
WHEN("the request body contains an object with a list of collection ids"){
    set<long> collectionIds = fixture.getRequestIds(CollectionsControllerFields::PhotoCollectionIdsField);
    CHECK_FALSE(collectionIds.empty());

THEN("the collections with that ids should be removed from database"){
    fixture.assertCollectionIdsDoesNotExist(collectionIds);
THEN("the controller should respond with an json message"){
    RequestUtils::assertJsonObjectResponse(fixture.response);
    RequestUtils::assertOkMessage(fixture.response);
THEN("the message should contain the removed collections"){
    INFO("Response: \n" << fixture.response.getContentStr());
    fixture.assertResponseContainsCollections(collectionIds);
}//THEN
}//THEN
}//THEN
}//WHEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

FIXTURE_SCENARIO("Move photos and subcollections from one collection to anothers"){
    this->testMoveCollectionItems();
}//SCENARIO


SCENARIO_METHOD(Fixture, "Redirect request to PhotoController", TEST_TAG){

GIVEN("some PhotoCollectionController with a non-null RequestRouter"){
    MockRouter router;
    controller.setRouter(&router);

GIVEN("some PhotoCollection on Db with some Photos"){
    int numPhotos= 3;
    PhotoCollection collection = PhotoCollectionHelper::
                                            createCollectionWithPhotosAndSubCollections(numPhotos,0);

WHEN("a request is made to the photos subpath of that collection route"){
    request.setUrl(buildPhotosUrl(collection.getId()));

    bool handled = controller.onRequest(request, response);
    REQUIRE(handled);

THEN("it should put the collection id as request parameter"){
    string collIdParam = request.getParameter(api::parameters::PhotoCollectionId);
    CHECK(StringCast::cast().toLong(collIdParam) == collection.getId());

THEN("the request should be redirected to a PhotoController route"){
    Url expectedUrl = Url(buildPhotosUrl(collection.getId()));
    expectedUrl.path().remove(0);

    CHECK(expectedUrl.toString() == router.forwardedUrl.toString());
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//SCENARIO

/***************************************** Tests Implementation ******************************************************/

void PhotoCollectionControllerFixture::testListCollectionsOfContext(){

GIVEN("some PhotoCollections associated with some PhotoContext on db"){
    const int numCollections = 4;
    std::set<long> collIds;
    long ctxId = PhotoContextHelper::insertCtxWithCollections("any name", numCollections, &collIds).getId();

    REQUIRE(collIds.size() == numCollections + 1);//includes also the root collection

GIVEN("some user with read permission on the given context"){
    SessionHelper::putUserWithPermission(request, ctxId, PermissionLevel::Read);

GIVEN("an GET request with that PhotoContext id as parameter"){
    request.putParameter("context", StrCast().toString(ctxId));

WHEN("that request is made to some PhotoCollectionController"){
    executeGetRequest("/");

THEN("it should respond with the collections of that PhotoContext as json"){
    vector<PhotoCollection> collections = deserializeResponseItems<PhotoCollection>(api::fields::collections);

    CHECK(collections.size() == collIds.size());
    for(size_t i=0; i < collections.size(); ++i){
        PhotoCollection & coll = collections[i];
        long id = coll.getId();
        CAPTURE(id);

        CHECK(collIds.find(id) != collIds.end());
        CHECK(coll.getParentContext() == ctxId);
    }
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//test

void PhotoCollectionControllerFixture::testGetCollection(){

GIVEN("a user with read access to some context"){
    long ctxId = setupRequestUserAndContext(PermissionLevel::Read);

GIVEN("some PhotoCollection on database"){
    set<long> collIds = PhotoCollectionHelper::insertCollections(2, -1, ctxId);
    REQUIRE(!collIds.empty());
    long photoCollectionId = *collIds.begin();

    PhotoCollection someCollection = Data::instance().collections().get(photoCollectionId);

GIVEN("that the PhotoCollection has subcollections"){
    set<long> subcollectionIds = PhotoCollectionHelper::insertCollections(2, someCollection.getId());
    someCollection.setSubCollections(subcollectionIds);

    REQUIRE(!subcollectionIds.empty());

WHEN("some GET request is made to that collection route "){
    executeGetRequest(route(someCollection.getId()));

THEN("that PhotoCollection should be sent as response"){
    PhotoCollection responseColl = getResponse<PhotoCollection>();

    CHECK(someCollection == responseColl);
    CHECK(responseColl.getSubCollections().getIds() == subcollectionIds);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//test

void PhotoCollectionControllerFixture::testGetFullCollection(){

GIVEN("a user with read access to some context"){
    long ctxId = setupRequestUserAndContext(PermissionLevel::Read);

GIVEN("some PhotoCollection with photos and subcollections on that context"){
    PhotoCollection collection = PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(3, 2, -1, ctxId);
    REQUIRE(!collection.getSubCollections().empty());

WHEN("making an GET request to a PhotoCollectionController with fullcollection argument"){
    executeGetRequest(route(collection.getId()),{{api::parameters::expandItems , "true"}});

THEN("a successful message with a json representation of the PhotoCollection should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);
    Json::Value jsonResponse = RequestUtils::assertJsonObjectResponse(response);

    PhotoCollection photoCollectionResponse = getResponse<PhotoCollection>();

THEN("the response should contain a list with the expanded photos of that collection"){
    assertResponseContainsExpandedPhotos(photoCollectionResponse, jsonResponse, serializer);

THEN("the response should contain a list with the expanded subcollections of that collection"){
    assertResponseContainsExpandedSubCollections(photoCollectionResponse, jsonResponse, serializer);
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//test

void PhotoCollectionControllerFixture::testGetFullCollectionWithoutItems(){

GIVEN("an empty PhotoCollection"){
    long ctxId = setupRequestUserAndContext(PermissionLevel::Read);
    PhotoCollection coll = PhotoCollectionHelper::createCollection({}, -1, ctxId);

GIVEN("another collections with photos and subcollections"){
    PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(2, 2, -1, ctxId);
    PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(1, 2, -1, ctxId);

WHEN("trying to access it from Api with the parameter to includes its children content"){
    executeGetRequest(route(coll.getId()),{{api::parameters::expandItems , "true"}});

THEN("the response should not contain any item"){
    PhotoCollection response = getResponse<PhotoCollection>();

    CHECK(response.getId() == coll.getId());
    CHECK(response.getPhotos().empty());
    CHECK(response.getSubCollections().empty());

    vector<Photo> photos = deserializeResponseItems<Photo>(api::fields::photos);
    CHECK(photos.size() == 0);

    vector<PhotoCollection> subcollections = deserializeResponseItems<PhotoCollection>(api::fields::subcollections);
    CHECK(subcollections.size() == 0);
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoCollectionControllerFixture::testGetContextRootCollection(bool hasContextParam){

GIVEN("some PhotoContext with some root photoCollection"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
    REQUIRE(Data::instance().collections().exist(ctx.getRootCollection()));

GIVEN("a logged user with read permission on that context"){
    SessionHelper::putUserWithPermission(request, ctx.getId(), PermissionLevel::Read);

WHEN("that a PhotoCollectionController receives a GET request to the '/root' url made by that user"){

if(hasContextParam){
    AND_WHEN("the request has the context id as parameter"){
        request.putParameter(api::parameters::PhotoContextId
                           , StrCast().toString(ctx.getId()));
        executeGetRequest(route("root"));

    THEN("it should return the root PhotoCollection of that context"){
        PhotoCollection rootCollection = collectionManager.getCollection(ctx.getRootCollection());
        RequestUtils::assertOkMessage(response);
        assertPhotoCollectionResponse(rootCollection.getId(), rootCollection);
    }//THEN
    }//WHEN
}
else{
    AND_WHEN("the request has no context id parameter"){
        executeGetRequest(route("root"));

    THEN("it should return a not found response"){
        RequestUtils::assertMessageStatus(response, StatusResponses::NOT_FOUND.getStatusCode());
    }//THEN
    }//WHEN
}//else

}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoCollectionControllerFixture::testCreateCollection(){

GIVEN("some PhotoContext"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
    long ctxId = ctx.getId();
    REQUIRE(collectionManager.existCollection(ctx.getRootCollection()));

GIVEN("a user with write permission on that context"){
    SessionHelper::putUserWithPermission(request, ctxId, PermissionLevel::Write);

WHEN("a POST request is made to the collections route on some PhotoCollectionController"){
    string relativeRoute = "/";

WHEN("the request has the target PhotoContext id as parameter"){
    request.putParameter(api::parameters::PhotoContextId, StrCast().toString(ctxId));

WHEN("that request has a json content with the collection name"){
    PhotoCollection coll(Test::Constants::COLLECTION_NAME);

    executePostRequest(relativeRoute, &coll);

THEN("the created PhotoCollection should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);
    PhotoCollection createdColl = getResponse<PhotoCollection>();

AND_THEN("that collection should have the specified name and context"){
    CHECK(createdColl.getName() == coll.getName());
    CHECK(createdColl.getParentContext() == ctxId);

THEN("the created collection should be put as a subcollection of that context root collection"){
    PhotoCollection rootCtxCollection = collectionManager.getCollection(ctx.getRootCollection());
    CHECK(rootCtxCollection.getSubCollections().contains(createdColl.getId()));

}//THEN
}//THEN
}//THEN
}//WHEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoCollectionControllerFixture::testCreateSubcollectionRequest(){

GIVEN("some PhotoCollection on some context"){
    PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
    PhotoCollection parentColl = collectionManager.createCollection(PhotoCollection()
                                                                    , PhotoCollection::INVALID_ID, ctx.getId());
    long parentId = parentColl.getId();
    REQUIRE(parentColl.getParentContext() == ctx.getId());

GIVEN("a user with write permission on the given context"){
    SessionHelper::putUserWithPermission(request, ctx.getId(), PermissionLevel::Write);

WHEN("a POST request is made by that user to the collections route on some PhotoCollectionController"){
    string relativeRoute = "/";

WHEN("that request has a json content with the collection name and parent id as fields"){
    request.setContentType(ContentTypes::JSON);
    string collName = Test::Constants::COLLECTION_NAME;
    setupRequestPhotoCollection(collName, parentId);

    executePostRequest(relativeRoute);

THEN("the created PhotoCollection should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);
    PhotoCollection createdColl = getResponse<PhotoCollection>();

AND_THEN("that collection should have the specified name and the same context of the parent collection"){
    CHECK(createdColl.getName() == collName);
    CHECK(createdColl.getParentContext() == parentColl.getParentContext());

THEN("the created collection should be put as a subcollection of that collection"){
    parentColl = collectionManager.getCollection(parentId);
    CHECK(parentColl.getSubCollections().contains(createdColl.getId()));

}//THEN
}//THEN
}//THEN
}//WHEN
}//WHEN
}//GIVEN
}//GIVEN

}

void PhotoCollectionControllerFixture::testRejectCreateCollectionWithoutContextOrParent(){
    size_t initialCollCount = getCollectionCount();

WHEN("a create collection request is made without a context id or parent collection id"){
    PhotoCollection requestColl(Test::Constants::COLLECTION_NAME);
    executePostRequest("/", &requestColl);

THEN("a bad request message should be sent as response"){
    RequestUtils::assertMessageStatus(response, StatusResponses::BAD_REQUEST.getStatusCode());

AND_THEN("no collection should be created"){
    CHECK(initialCollCount == getCollectionCount());

}//THEN
}//THEN
}//WHEN

}//test

void PhotoCollectionControllerFixture::testUpdateCollectionRequest(){

GIVEN("some user with write permission on a given context"){
    long ctxId = setupRequestUserAndContext(PermissionLevel::Write);

GIVEN("an existent PhotoCollection on db"){
    PhotoCollection coll= PhotoCollectionHelper::createCollection({Test::Constants::COLLECTION_NAME}, -1, ctxId);

WHEN("a POST request is made to that collection Url"){
    const string newName = Test::Constants::OTHER_COLLECTION_NAME;
    executePostRequest(route(coll.getId()), PhotoCollection{newName});

THEN("the referenced fields on the corresponding collection should be updated"){
    PhotoCollection updatedColl = collectionManager.getCollection(coll.getId());
    CHECK(updatedColl.getName() == newName);

THEN("the controller should respond with an JSON representation of the changed collection"){
    RequestUtils::assertOkJsonResponse(response);

    PhotoCollection responseColl = getResponse<PhotoCollection>();
    CHECK(responseColl == updatedColl);
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//test

void PhotoCollectionControllerFixture::testMoveCollectionItems(){

GIVEN("some user with write permission on a given context"){
    long ctxId = setupRequestUserAndContext(PermissionLevel::Write);

GIVEN("some PhotoCollection with photos and subcollections on that context"){
    PhotoCollection collection = PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(3,3, -1, ctxId);

GIVEN("others collections"){
    set<long> targetCollections = PhotoCollectionHelper::insertCollections(3, -1, ctxId);

WHEN("'move-items' request is made with CollectionsOperation as body"){
    set<long> collectionsToMove, collectionsNotMoved;
    set<long> photosToMove, photosNotMoved;
    PhotoCollectionHelper::splitSet(collection.getSubCollections().getIds(), collectionsToMove, collectionsNotMoved
                                                                  , collection.getSubCollections().size()/2);
    PhotoCollectionHelper::splitSet(collection.getPhotos(), photosToMove, photosNotMoved
                                                                  , collection.getPhotos().size()/2);

    CollectionsOperation collOperation = makeMoveCollectionOperation(targetCollections, photosToMove, collectionsToMove);

    executePostRequest(route(collection.getId(), "move-items"), collOperation);

THEN("the CollectionsOperation photos and subcollections should be added to the CollectionsOperation targets"){
    PhotoCollectionHelper::assertAllCollectionsContainsItems(targetCollections
                                                        , collOperation.getPhotoIds()
                                                        , collOperation.getSubCollectionsIds());

AND_THEN("the same photos and subcollections should be removed from the source PhotoCollection"){
    set<long> collectionSet;
    collectionSet.insert(collection.getId());
    PhotoCollectionHelper::assertAllCollectionsNotContainsItems(collectionSet
                                                        , collOperation.getPhotoIds()
                                                        , collOperation.getSubCollectionsIds());
AND_THEN("other photos and subcollections on the PhotoCollection should be preserved"){
    PhotoCollectionHelper::assertAllCollectionsContainsItems(collectionSet
                                                        , photosNotMoved
                                                        , collectionsNotMoved);

AND_THEN("the controller should respond with an OK Json Message"){
    INFO("Response body: " << response.getContentStr());
    RequestUtils::assertOkJsonResponse(response);

AND_THEN("the response body should be equals to the changed collection"){
    PhotoCollection changedCollection =Data::instance().collections().get(collection.getId());
    assertResponseContainsCollectionAsJson(changedCollection);
}//THEN
}//THEN
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN

}//test

void PhotoCollectionControllerFixture::testDeleteCollection(){

    long ctxId = requireContextPermission(request, PermissionLevel::Write);

GIVEN("some PhotoCollection on database"){
    long collectionId = PhotoCollectionHelper::insertCollection(-1, ctxId);
    REQUIRE(Data::instance().collections().exist(collectionId));
    PhotoCollection collectionToRemove = Data::instance().collections().get(collectionId);

WHEN("a DELETE request is made to that collection route"){
    executeDeleteRequest(route(collectionId));

THEN("the PhotoCollection should be removed from Db"){
    CHECK_FALSE(Data::instance().collections().exist(collectionId));

AND_THEN("the controller should respond with a object json message"){
    RequestUtils::assertJsonObjectResponse(response);

AND_THEN("the message should contain an representation of the removed collection"){
    PhotoCollection removedCollection = getResponse<PhotoCollection>();
    CHECK(removedCollection == collectionToRemove);

}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//test


template<typename Collection>
class PhotoIdsSerializer : public Serializable{

    string fieldName;
    Collection * photoList;
public:
    PhotoIdsSerializer(const std::string & fieldName, Collection * coll)
        : fieldName(fieldName), photoList(coll)
    {}

    virtual void serialize(Serializer & serializer) const{
        set<long> photoIds;
        std::transform(photoList->begin(), photoList->end(), std::inserter< set<long> >(photoIds, photoIds.end()),
            [](const Photo & photo){
                return photo.getId();
            }
        );
        serializer.putCollection(fieldName, photoIds.begin(), photoIds.end());
    }

};

/***************************************** Fixture Implementation ******************************************************/

PhotoCollectionControllerFixture::PhotoCollectionControllerFixture()
    : collectionManager()
    , controller(&collectionManager)
    , tmpDatabase(ScopedSqliteDatabase::DbType::InMemory)
{

    setController(&controller);

    Data::instance();
}

long PhotoCollectionControllerFixture::setupRequestUserAndContext(PermissionLevel requiredPermission){
    return requireContextPermission(this->request, requiredPermission);
}

long PhotoCollectionControllerFixture::requireContextPermission(MockRequest & request, PermissionLevel requiredPermission){
    long ctxId = PhotoContextHelper::insertContext().getId();
    SessionHelper::putUserWithPermission(request, ctxId, requiredPermission);

    return ctxId;
}

string PhotoCollectionControllerFixture::buildPhotosUrl(long collId){
    return "/" + StringCast::cast().toString(collId) + "/photos";
}

void PhotoCollectionControllerFixture::setupJsonRequest(){
    request.setContentType(ContentTypes::JSON);
}

void PhotoCollectionControllerFixture::setupNonJsonRequest(){
    request.setContentType(ContentTypes::TEXT_PLAIN);
    this->setupRequestPhotoCollection(Test::Constants::COLLECTION_NAME);
}

void PhotoCollectionControllerFixture::setupCollectionRequestUrl(long collId){
    this->request.setUrl(getCollectionUrl(collId));
}

//void PhotoCollectionControllerFixture::setupCollectionRequestJSONBody(const string &collName){
//    Json::Value collJson;
//    collJson[PhotoCollectionFields::NAME] = collName;

//    string content;
//    collJson >> serializer >> content;
//    request.setContentStr(content);

//    request.setContentType(ContentTypes::JSON);
//}

void PhotoCollectionControllerFixture::setupRequestPhotoCollection(const string &collectionName){
    setupRequestPhotoCollection(collectionName, -1);
}

void PhotoCollectionControllerFixture::setupRequestPhotoCollection(const string &collectionName, long collectionParent){
    serializer.clear();
    serializer.put(Test::Constants::COLLECTION_NAME_FIELD, collectionName);

    if(collectionParent > 0){
        serializer.put(api::fields::parentCollectionId, collectionParent);
    }
    string content;
    serializer >> content;
    request.setContentStr(content);
}

void PhotoCollectionControllerFixture::setupRequestInvalidJsonContent(){
    request.setContentStr(Test::Constants::INVALID_JSON);
}

string PhotoCollectionControllerFixture::getCollectionUrl(long collectionId){
    stringstream url;
    url << controller.getBaseUrl() << "/" << collectionId;
    return url.str();
}

void PhotoCollectionControllerFixture::setupGetCollectionRequest(long collectionId){
    string url = getCollectionUrl(collectionId);
    request.setUrl(url);
}

void PhotoCollectionControllerFixture::setupAddPhotosRequest(long collectionId, const std::vector<Photo> & photosToAdd){
    set<long> photoIds;
    for(size_t i=0; i < photosToAdd.size(); ++i){
        photoIds.insert(photosToAdd[i].getId());
    }

    string url = route(collectionId, api::paths::photos);

    serializer.clear();
    serializer.putCollection(PhotoCollectionFields::PHOTO_IDS, photoIds.begin(), photoIds.end());
    REQUIRE(!serializer.empty());

    stringstream content;
    content << serializer;

    REQUIRE(!content.str().empty());

    request.setContentType(ContentTypes::JSON);
    request.setUrl(url);
    request.setContentStr(content.str());
}

void PhotoCollectionControllerFixture::setupAddPhotosToCollectionsRequest(const vector<PhotoCollection> & collections, const vector<Photo> & photos){
    setupChangePhotosAndCollectionsRequest(CollectionsOperation::ADD, photos, collections);
}

void PhotoCollectionControllerFixture::setupRemovePhotosFromCollectionsRequest(const vector<PhotoCollection> & collections, const vector<Photo> & photos){
    setupChangePhotosAndCollectionsRequest(CollectionsOperation::REMOVE, photos, collections);
}

void PhotoCollectionControllerFixture::setupAddPhotosAndSubCollectionsRequest(const std::set<long> & targetCollectionsId, const std::set<long> & photoIds, const std::set<long> & subcollectionIds)
{
    setupChangePhotosAndCollectionsRequest(CollectionsOperation::ADD, targetCollectionsId, photoIds, subcollectionIds);
}

void PhotoCollectionControllerFixture::setupChangePhotosAndCollectionsRequest(CollectionsOperation::OperationTypes operationType, const vector<Photo> &photos, const vector<PhotoCollection> &collections)
{
    set<long> photoIds = getIds(photos.begin(), photos.end());
    set<long> targetCollectionsIds = getIds(collections.begin(), collections.end());
    this->setupChangePhotosAndCollectionsRequest(operationType, targetCollectionsIds, photoIds, set<long>());
}

void PhotoCollectionControllerFixture::setupChangePhotosAndCollectionsRequest(CollectionsOperation::OperationTypes operationType, const std::set<long> &targetCollectionsId, const std::set<long> &photoIds, const std::set<long> &subcollectionIds)
{
    string removePhotosContent = getCollectionOperationContent(operationType
    , targetCollectionsId, photoIds, subcollectionIds);
    REQUIRE(!removePhotosContent.empty());

    string url = controller.getBaseUrl() + "/" + CollectionsControllerFields::BulkActionSubpath;
    request.setContentType(ContentTypes::JSON);
    request.setUrl(url);
    request.setContentStr(removePhotosContent);
}

void PhotoCollectionControllerFixture::setupBulkDeleteCollectionRequest(const set<long> &collectionIds){
    serializer.clear();
    serializer.putCollection(CollectionsControllerFields::PhotoCollectionIdsField, collectionIds.begin(), collectionIds.end());
    REQUIRE(!serializer.empty());

    stringstream content;
    content << serializer;

    request.setContentStr(content.str());
    request.setUrl(controller.getBaseUrl());
    request.setContentType(ContentTypes::JSON);
}

string PhotoCollectionControllerFixture::getCollectionOperationContent(CollectionsOperation::OperationTypes type, const std::set<long> &targetCollectionsId, const std::set<long> &photoIds, const std::set<long> &subcollectionIds)
{
    CollectionsOperation collectionOperation;
    collectionOperation.setOperationType(type);
    collectionOperation.setPhotoIds(photoIds);
    collectionOperation.setTargetCollectionIds(targetCollectionsId);
    collectionOperation.setSubCollectionsIds(subcollectionIds);

    serializer.clear();
    serializer << collectionOperation;
    REQUIRE(!serializer.empty());

    stringstream content;
    content << serializer;

    return content.str();
}

//string PhotoCollectionControllerFixture::makeMoveItemsPath(long collectionId){;
//    stringstream sstream;
//    sstream << controller.getBaseUrl() << "/" << collectionId << "/" << Test::Constants::MOVE_ITEMS_SUBPATH;
//    return sstream.str();
//}

CollectionsOperation PhotoCollectionControllerFixture::makeMoveCollectionOperation(const set<long> &targetCollections, const set<long> &photosToMove, const set<long> &collectionsToMove)
{
    CollectionsOperation collOperation;
    collOperation.setOperationType(CollectionsOperation::MOVE);
    collOperation.setTargetCollectionIds(targetCollections);
    collOperation.setSubCollectionsIds(collectionsToMove);
    collOperation.setPhotoIds(photosToMove);

    return collOperation;
}

PhotoCollection PhotoCollectionControllerFixture::putPhotosOnCollection(long collectionId, int numPhotos){
    PhotoHelper::insertPhotos(numPhotos);
    vector<Photo> photos = Data::instance().photos().listAll();
    PhotoCollection coll = Data::instance().collections().get(collectionId);
    for(size_t i=0; i < numPhotos && i< photos.size(); ++i){
        coll.putPhoto(photos[i].getId());
    }
    CHECK(Data::instance().collections().update(coll));

    return coll;
}

void PhotoCollectionControllerFixture::insertSubCollections(PhotoCollection &coll){
    set<long> otherCollections = PhotoCollectionHelper::getOtherCollectionIds(coll.getId());
    if(otherCollections.empty()){
        PhotoCollectionHelper::insertCollections(2);
        otherCollections = PhotoCollectionHelper::getOtherCollectionIds(coll.getId());
    }
    coll.setSubCollections(otherCollections);
    CHECK(Data::instance().collections().update(coll));
}

PhotoCollection PhotoCollectionControllerFixture::makeCollection(int collNumber){
    PhotoCollection collection;
    collection.setName(string("Collection ").append(StringCast::instance().toString(collNumber)));
    return collection;
}

bool PhotoCollectionControllerFixture::isBadRequestResponse(MockResponse &response){
    return response.statusLine().getStatusCode() == StatusResponses::BAD_REQUEST.getStatusCode();
}

bool PhotoCollectionControllerFixture::isSuccessfulJsonResponse(MockResponse &response){
    return response.statusLine().getStatusCode() == StatusResponses::OK.getStatusCode()
    && response.getContentType().is(Types::APPLICATION, SubTypes::JSON);
}

//vector<PhotoCollection> PhotoCollectionControllerFixture::deserializeCollectionsResponse(const string &collectionsJsonResponse){
//    this->serializer.clear();
//    collectionsJsonResponse >> serializer;
//    CHECK(!serializer.empty());
//    std::vector<PhotoCollection> collections;

//    serializer.getChildCollection<PhotoCollection>("collections", back_inserter(collections));

//    return collections;
//}

vector<Photo> PhotoCollectionControllerFixture::deserializePhotos(const string &property)
{
    vector<Photo> photos;
    serializer.getChildCollection<Photo>(property, back_inserter(photos));

    return photos;
}

set<long> PhotoCollectionControllerFixture::getRequestIds(const string &memberName){
    serializer.clear();
    INFO(request.getContentStr());
    request.getContentStr() >> serializer;

    set<long> ids;

    serializer.getChildCollection(memberName
    , std::inserter< set<long> >(ids, ids.end())
    , GenericIterator::NumberConverter<long>());
    return ids;
}

CollectionsOperation PhotoCollectionControllerFixture::getCollectionOperation(MockRequest &request){
    CollectionsOperation operation;

    serializer.clear();
    request.getContentStr() >> serializer >> operation;

    return operation;
}

set<long> PhotoCollectionControllerFixture::getCollectionOperationRequestPhotoIds(){
    return getRequestIds(CollectionsOperationFields::PHOTO_ITEMS_IDS);
}

set<long> PhotoCollectionControllerFixture::getCollectionOperationRequestCollectionIds(){
    return getRequestIds(CollectionsOperationFields::COLLECTIONS_TARGETS_IDS);
}

void PhotoCollectionControllerFixture::assertAllCollectionsContainsPhotos(const vector<PhotoCollection> &allCollections, const set<long> &collectionIds, const set<long> &photoIds)
{
    map<long, PhotoCollection> allCollectionsById = PhotoCollection::mapById(allCollections.begin(), allCollections.end());
    for(set<long>::iterator i=collectionIds.begin(); i!=collectionIds.end(); ++i){
        const PhotoCollection & coll = allCollectionsById[*i];
        assertCollectionContainsPhotos(coll, photoIds);
    }
}

void PhotoCollectionControllerFixture::assertCollectionContainsPhotos(const PhotoCollection &coll, const set<long> &photoIds){
    for(set<long>::iterator i=photoIds.begin(); i!= photoIds.end(); ++i){
        INFO("Collection id: " << coll.getId() << "; photo id: " << *i);
        CHECK(coll.containsPhoto(*i));
    }
}

void PhotoCollectionControllerFixture::assertAllCollectionsNotContainsPhotos(const vector<PhotoCollection> &allCollections, const set<long> &collectionIds, const set<long> &photoIds)
{
    map<long, PhotoCollection> allCollectionsById = PhotoCollection::mapById(allCollections.begin(), allCollections.end());
    for(set<long>::iterator i=collectionIds.begin(); i!=collectionIds.end(); ++i){
        const PhotoCollection & coll = allCollectionsById[*i];
        assertCollectionNotContainsPhotos(coll, photoIds);
    }
}

void PhotoCollectionControllerFixture::assertCollectionNotContainsPhotos(const PhotoCollection &coll, const set<long> &photoIds){
    for(set<long>::iterator i=photoIds.begin(); i!= photoIds.end(); ++i){
        INFO("Collection id: " << coll.getId() << "; photo id: " << *i);
        CHECK(!coll.containsPhoto(*i));
    }
}

void PhotoCollectionControllerFixture::insertCollectionsWithPhotosAndSubCollections(
        set<long> &outParentCollectionIds
        , set<long> &outPhotoIds
        , set<long> &outSubCollections
        , long ctxId)
{
    const int numParents = 2, numSubCollections=3, numPhotos=3;
    long rootId = PhotoCollection::INVALID_ID;
    if(ctxId != PhotoContext::INVALID_ID){
        rootId = collectionManager.getContextRootId(ctxId);
    }

    set<long> parentIds = PhotoCollectionHelper
                                                        ::insertCollections(numParents, rootId, ctxId);
    set<long> subCollIds    = PhotoCollectionHelper
                                                        ::insertCollections(numSubCollections, -1, ctxId);
    set<long> photoIds = PhotoHelper::insertPhotos(numPhotos);

    Data::instance().collections().addItemsToCollections(parentIds, photoIds, subCollIds);

    outParentCollectionIds = parentIds;
    outPhotoIds = photoIds;
    outSubCollections = subCollIds;
}

void PhotoCollectionControllerFixture::insertCollectionsWithPhotos(vector<PhotoCollection> &outCollections, vector<Photo> &outPhotos){
    PhotoCollectionHelper::insertCollections(3);
    vector<PhotoCollection> initCollections = Data::instance().collections().listAll();

    PhotoHelper::insertPhotos(6);
    vector<Photo> photos = Data::instance().photos().listAll();
    set<long> collectionIds = getIds(initCollections.begin(), initCollections.end());
    set<long> photoIds = getIds(photos.begin(), photos.end());

    Data::instance().collections().addPhotos(collectionIds, photoIds);

    outCollections = Data::instance().collections().listAll();
    outPhotos = photos;
}

vector<PhotoCollection> PhotoCollectionControllerFixture::assertResponseContainsCollections(const set<long> &expectedCollectionIds)
{
    serializer.clear();
    response.getContentStr() >> serializer;

    vector<PhotoCollection> responseCollections;
    serializer.getChildCollection<PhotoCollection>(api::fields::collections
    , std::back_inserter<vector<PhotoCollection> >(responseCollections));

    CHECK(expectedCollectionIds.size() == responseCollections.size());

    return responseCollections;
}

void PhotoCollectionControllerFixture::assertResponseContainsChangedCollections(const set<long> &expectedCollectionIds){
    vector<PhotoCollection> responseCollections = assertResponseContainsCollections(expectedCollectionIds);

    map<long, PhotoCollection> responseCollectionsById = PhotoCollection::mapById(responseCollections.begin()
    , responseCollections.end());

    vector<PhotoCollection> expectedResponseCollections = Data::instance().collections().listIn(expectedCollectionIds);
    REQUIRE(expectedResponseCollections.size() == expectedCollectionIds.size());

    for(size_t i=0; i < expectedResponseCollections.size(); ++i){
        long collId = expectedResponseCollections[i].getId();
        CHECK(responseCollectionsById.find(collId) != responseCollectionsById.end());
        CHECK(responseCollectionsById[collId] == expectedResponseCollections[i]);
    }
}

void PhotoCollectionControllerFixture::assertCollectionIdsDoesNotExist(const set<long> &collectionIds){
    set<long>::const_iterator iter = collectionIds.begin();
    while(iter != collectionIds.end()){
        long collId = *iter;
        INFO("Collection id " << collId)
        CHECK(Data::instance().collections().exist(collId) == false);
        ++iter;
    }
}

PhotoCollection PhotoCollectionControllerFixture::assertCollectionWasCreated(const string &collName){
    vector<PhotoCollection> collections = Data::instance().collections().getByName(collName);
    REQUIRE(!collections.empty());
    PhotoCollection createdPhotoCollection = collections[0];
    CHECK(createdPhotoCollection.getName() == collName);
    CHECK(createdPhotoCollection.getId() != PhotoCollection::INVALID_ID);

    return createdPhotoCollection;
}

void PhotoCollectionControllerFixture::assertResponseContainsCollectionAsJson(const PhotoCollection collection){
    serializer.clear();
    CHECK(response.getContentType().is(Types::APPLICATION, SubTypes::JSON));

    PhotoCollection photoCollectionResponse;
    response.getContentStr() >> serializer >> photoCollectionResponse;
    CHECK(photoCollectionResponse == collection);
}

Json::Value PhotoCollectionControllerFixture::assertPhotoCollectionResponse(long collectionId, const PhotoCollection &collection, PhotoCollection *outCollectionResponse){
    Json::Value jsonResponse = RequestUtils::assertJsonObjectResponse(response);
    PhotoCollection photoCollectionResponse;
    serializer.clear();
    jsonResponse >> serializer >> photoCollectionResponse;
    CHECK(photoCollectionResponse.getId() == collectionId);
    PhotoCollectionHelper::checkEquals(collection, photoCollectionResponse);

    if(outCollectionResponse != NULL){
        *outCollectionResponse = photoCollectionResponse;
    }
    return jsonResponse;
}

void PhotoCollectionControllerFixture::assertResponseContainsExpandedPhotos(const PhotoCollection &collection, Json::Value &jsonResponse, Deserializer &serializer)
{
    CHECK(jsonResponse.isMember(api::fields::photos));
    vector<Photo> photos;
    serializer.getChildCollection<Photo>(api::fields::photos, std::back_inserter<vector<Photo> >(photos));

    CHECK(photos.size() == collection.getPhotos().size());
    for(size_t i=0; i < photos.size(); ++i){
        CHECK(collection.containsPhoto(photos[i].getId()));
    }
}

void PhotoCollectionControllerFixture::assertResponseContainsExpandedSubCollections(const PhotoCollection &collection, Json::Value &jsonResponse, Deserializer &serializer)
{
    CHECK(jsonResponse.isMember(api::fields::subcollections));
    vector<PhotoCollection> collections;
    serializer.getChildCollection<PhotoCollection>(api::fields::subcollections
    , std::back_inserter<vector<PhotoCollection> >(collections));

    CHECK(collections.size() == collection.getSubCollections().size());
    for(size_t i=0; i < collections.size(); ++i){
        CHECK(collection.getSubCollections().contains(collections[i].getId()));
    }
}

void PhotoCollectionControllerFixture::assertCollectionsContainsSubCollections(const set<long> &parentCollectionsIds, const set<long> &subcollections){
    vector<PhotoCollection> parentCollections = Data::instance().collections().listIn(parentCollectionsIds);
    for(size_t i=0; i < parentCollections.size(); ++i){
        const PhotoCollection & parentCollection = parentCollections[i];

        for(set<long>::const_iterator iter = subcollections.begin();
        iter != subcollections.end(); ++iter)
        {
            INFO("Parent collection: " << parentCollection.getId() << "; subcollection: " << *iter);
            CHECK(parentCollection.getSubCollections().contains(*iter));
        }
    }
}
