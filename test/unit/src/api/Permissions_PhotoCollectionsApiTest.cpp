#include "helpers/ControllerPermissionsFixture.h"
#include "helpers/PhotoCollectionHelper.h"

#include "api/Api.h"
#include "api/PhotoCollectionController.h"
#include "services/PhotoCollectionManager.h"



using namespace calmframe;
using namespace calmframe::data;

class PhotoCollectionController_PermissionsFixture : public ControllerPermissionsFixture{
public:
    void testRejectGetCollectionWithoutReadPermission();
    void testRejectCreateCollectionWithoutWritePermission();
    void testRejectListCollectionsWithoutReadPermission();
public:
    PhotoCollectionManager collectionsManager;
    PhotoCollectionController photoCollectionsController;
    PhotoCollectionController_PermissionsFixture();

    long createEntity(long ctxId);
};

#define FIXTURE PhotoCollectionController_PermissionsFixture
#define TEST_TAG "[PhotoCollectionController]"

/*********************************************************************************************************************/

FIXTURE_SCENARIO("Api: a user cannot get a collection without read permission on its context"){
    this->testRejectGetCollectionWithoutReadPermission();
}

FIXTURE_SCENARIO("Api: a user cannot update a collection without write permission on its context"){
    PhotoCollection updateColl{"any name"};
    this->testForbiddenRequest("POST", PermissionLevel::Write, &updateColl);
}

FIXTURE_SCENARIO("Api: a user cannot remove a collection without write permission on its context"){
    this->testForbiddenRequest("DELETE", PermissionLevel::Write);
}

FIXTURE_SCENARIO("Api: a user cannot create a collection on a context without write permission on it"){
    this->testRejectCreateCollectionWithoutWritePermission();
}

FIXTURE_SCENARIO("Api: a user cannot list collections from a context without read permission on it"){
    this->testRejectListCollectionsWithoutReadPermission();
}

/*********************************************************************************************************************/


void PhotoCollectionController_PermissionsFixture::testRejectGetCollectionWithoutReadPermission(){
    this->testForbiddenRequest("GET", PermissionLevel::Read);
}

void PhotoCollectionController_PermissionsFixture::testRejectCreateCollectionWithoutWritePermission(){

GIVEN("some context on db"){
    long ctxId = createPrivateContext();

GIVEN("a user without write permission on that context"){
    putUserWithoutPermission(ctxId, PermissionLevel::Write);

WHEN("making a request to create a collection on that context"){
    putContextParameter(ctxId);
    executePostRequest("/", PhotoCollection{"any collection name"});

THEN("a forbidden message should be sent as response"){
    verifyForbiddenResponse();

}//THEN
}//WHEN
}//GIVEN

}//GIVEN
}//test

void PhotoCollectionController_PermissionsFixture::testRejectListCollectionsWithoutReadPermission(){

GIVEN("some context on db"){
    long ctxId = createPrivateContext();

GIVEN("a user without read permission on that context"){
    putUserWithoutPermission(ctxId, PermissionLevel::Read);

WHEN("a request is made by that user to list collections from that context"){
    putContextParameter(ctxId);
    executeForbiddenRequest("GET", PermissionLevel::Read, NULL, "/");

THEN("a forbidden message should be sent as response"){
    verifyForbiddenResponse();

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

/*********************************************************************************************************************/

PhotoCollectionController_PermissionsFixture::PhotoCollectionController_PermissionsFixture()
{
    photoCollectionsController.setCollectionManager(&collectionsManager);
    setController(&photoCollectionsController);
}

long PhotoCollectionController_PermissionsFixture::createEntity(long ctxId){
    PhotoContext ctx = Data::instance().contexts().get(ctxId);
    PhotoCollection coll = collectionsManager.createCollectionAtContext({},ctx.getId());
//    return PhotoCollectionHelper::insertCollection(-1, ctxId);
    return coll.getId();
}

