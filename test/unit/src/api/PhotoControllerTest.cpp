#include "TestMacros.h"

#include "PhotoControllerTest.h"

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <vector>

#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "json/json.h"

#include "model/permissions/ProtectedEntity.h"
#include "network/StatusResponses.h"
#include "network/Url.h"

#include "serialization/json/JsonSerializer.h"

#include "services/EventsManager.h"
#include "services/SessionManager.h"

#include "utils/FileUtils.h"
#include "utils/StringUtils.h"
#include "utils/StringCast.h"

#include "helpers/PhotoHelper.h"
#include "helpers/RequestUtils.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/PhotoContextHelper.h"
#include "utils/ScopedDatabase.h"
#include "helpers/SessionHelper.h"
#include "helpers/EventsHelper.h"

using namespace calmframe::utils;

#define FIXTURE PhotoControllerFixture
#define TEST_TAG "[PhotoController]"

FIXTURE_SCENARIO("Api: List photos of some context"){
    testListPhotosByContext();
}//SCENARIO

FIXTURE_SCENARIO("Get photos with invalid sub url"){
    testGetPhotosWithInvalidSubUrl();
}
FIXTURE_SCENARIO("Get some photo"){
    testGetSomePhoto();
}
FIXTURE_SCENARIO("Api: Get photo with user permission"){
    this->testGetPhotoWithPermission();
}
FIXTURE_SCENARIO("Get inexistent photo"){
    testGetInexistentPhoto();
}
FIXTURE_SCENARIO("Get image"){
    testGetImage();
}
FIXTURE_SCENARIO("Delete photo","[api][slow]"){
    testDeletePhoto();
}
FIXTURE_SCENARIO("Update photo",",[api]"){
    testUpdatePhoto();
}
FIXTURE_SCENARIO("Update photo with not all fields"){
    testUpdatePhotoWithNotAllFields();
}
FIXTURE_SCENARIO("Update photo with invalid message"){
    testTryUpdatePhotoWithInvalidMessage();
}
FIXTURE_SCENARIO("Update photo should create an Event on database"){
    testUpdatePhotoEvent();
}

FIXTURE_SCENARIO("Create photo at given collection"){
    this->testPhotoCreationAtCollection();
}//SCENARIO

FIXTURE_SCENARIO("Create photo at given context"){
    this->testPhotoCreationAtContext();
}//SCENARIO

FIXTURE_SCENARIO("Try create photo without context or collection"){
    this->testTryCreatePhotoWithoutContextOrCollection();
}//SCENARIO

FIXTURE_SCENARIO("Try create photo with invalid content type"){
    this->testTryCreatePhotoWithInvalidContentType();
}//SCENARIO

FIXTURE_SCENARIO("PhotoController should allow get photos from some collection"){
    testGetPhotosFromCollection();
}//SCENARIO

FIXTURE_SCENARIO("PhotoController should allow get photos from a hierarchy of photo collections"){
    this->testGetPhotosFromCollectionsHierarchy();
}//SCENARIO

FIXTURE_SCENARIO("PhotoController should allow filter the photos got from some collection with a query string"){
    this->testFilterPhotosFromCollectionsUsingQuery(false);
}//SCENARIO

FIXTURE_SCENARIO("PhotoController should allow filter the photos got from a hierarchy of collections with a query string"){
    this->testFilterPhotosFromCollectionsUsingQuery(true);
}//SCENARIO

/* ***************************************** Test cases implementation *****************************************/

void PhotoControllerFixture::verify_GetPhotos_InvalidSubUrl_Behaviour(){
    WHEN("the ControllerRouter received a request to " PHOTOS_URL "/anything"){
    THEN("it should not be called"){
        executeRequest("GET", PHOTOS_URL "/anything", false);
    }
    }
}

void PhotoControllerFixture::verifyPhotoCreation(long ctxId, long collId, long userId)
{
    ScopedFile scopedImageDir(createTmpDir(), true);
    photoFrame.setImagesDir(scopedImageDir.getFilepath());

    verify_CreatePhoto_fromImageRequest_Behaviour(collId, ctxId, userId);
    verify_CreatePhoto_fromMultipartRequest_Behaviour(collId, ctxId, userId);
}

void PhotoControllerFixture::verifyPhotoContainer(Photo createdPhoto, long ctxId, long collId)
{
    if(collId >= 0){
        THEN("the photo should be put on the passed PhotoCollection"){
            PhotoCollection coll = Data::instance().collections().get(collId);
            CHECK(coll.containsPhoto(createdPhoto.getId()));
        }//THEN
    }
    else if(ctxId >= 0){
        THEN("the photo should be put on the root collection of the passed PhotoContext"){
            PhotoContext ctx = Data::instance().contexts().get(ctxId);
            PhotoCollection coll = Data::instance().collections().get(ctx.getRootCollection());
            CHECK(coll.containsPhoto(createdPhoto.getId()));
        }//THEN
    }
}

void PhotoControllerFixture::verifyBadRequestResponse()
{
    THEN("it should respond with a bad request message"){
        CHECK(response.statusLine().getStatusCode()
                    == StatusResponses::BAD_REQUEST.getStatusCode());
    }
}

string PhotoControllerFixture::verifyPhotoImagePath(const Photo & createdPhoto)
{
    string imgPath = Files().joinPaths(photoFrame.getImagesDir()
                                     , createdPhoto.getImagePath());
    INFO("Images dir: '" << photoFrame.getImagesDir()
                         << "' ; photo image path: '" << createdPhoto.getImagePath() << "'");
    REQUIRE(FileUtils::instance().exists(imgPath));
    REQUIRE(createdPhoto.getImagePath() != VALID_IMAGE_PATH);

    return imgPath;
}

void PhotoControllerFixture::testListPhotosByContext(){

    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Read);

GIVEN("some photos on a context"){
    set<long> photoIds = PhotoHelper::insertPhotosAtContext(3, ctxId);
    //other photos
    PhotoHelper::insertPhotosAtContext(3, PhotoContextHelper::insertContext().getId());

    vector<Photo> photos = Data::instance().photos().listIn(photoIds);

    REQUIRE(!photoIds.empty());
    REQUIRE(photos.size() == photoIds.size());

WHEN("a GET request is made to some PhotoController photos route with the id of some context as parameter"){
    putContextParameter(ctxId);

    executeGetRequest(Constants::PhotosUrl);

THEN("it should sent all photos on the given context as response"){
    RequestUtils::assertOkJsonResponse(response);

    vector<Photo> responsePhotos = deserializeResponseItems<Photo>("photos");

    REQUIRE(photos.size() == responsePhotos.size());

    std::sort(photos.begin(), photos.end());
    std::sort(responsePhotos.begin(), responsePhotos.end());
    for(int i=0; i < photos.size(); ++i){
        PhotoHelper::assertEquals(photos[i], responsePhotos[i]);
    }
}//THEN
}//WHEN
}//GIVEN
}

void PhotoControllerFixture::testGetPhotosWithInvalidSubUrl(){

GIVEN("some photos on database"){
GIVEN("a PhotoController instantiated with some PhotoFrame"){
GIVEN("that PhotoController is attached with URL \"" PHOTOS_URL "\" at a ControllerRouter"){
    PhotoHelper::insertPhotos();
    verify_GetPhotos_InvalidSubUrl_Behaviour();
}
}
}

}

void PhotoControllerFixture::testGetSomePhoto(){
GIVEN("some photos on database"){
    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Read);

    auto photoIds = PhotoHelper::insertPhotos(3, Photo{{}, {}, ctxId});
    long photoId = *photoIds.begin();

WHEN("a PhotoController receive a request to some photo route"){
    Photo requestedPhoto;
    REQUIRE(Data::instance().photos().getPhoto(photoId, requestedPhoto));

    executeGetRequest(route("photos", photoId));

THEN("it should respond with an ok json message"){
    RequestUtils::assertOkMessage(response);

    Json::Value responseJson = RequestUtils::assertJsonObjectResponse(response);
    INFO("Readed json: " << responseJson);

AND_THEN("the response should correspond with the photo on database"){
    PhotoHelper::assertEquals(requestedPhoto, responseJson);
}//THEN
}//THEN
}//WHEN
}

}//test

void PhotoControllerFixture::testGetPhotoWithPermission(){

GIVEN("some photo on some photoContext"){
    long ctxId = PhotoContextHelper::insertContext().getId();
    long photoId= PhotoHelper::insertPhotoAtContext(ctxId);

WHEN("a GET request is made by a user to that photo with an option to include the permission"){
    PermissionLevel permLevel = PermissionLevel::Admin;
    long userId = SessionHelper::putUserWithPermission(request, ctxId, permLevel);

    executeGetRequest(route(Constants::PhotosUrl, photoId), {{api::parameters::include_permission, "true"}});

THEN("a representation of that photo, with the user permission should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    ProtectedEntity<Photo> photoWithPermission = getResponse<ProtectedEntity<Photo>>();

    CHECK(photoWithPermission.getPermissionLevel() == permLevel);
    CHECK(photoWithPermission.getUserId() == userId);
    CHECK(photoWithPermission.getEntity().getId() == photoId);
    CHECK(photoWithPermission.getEntity().getContextId() == ctxId);

}//THEN
}//WHEN

}//GIVEN

}//test

void PhotoControllerFixture::testGetInexistentPhoto(){
GIVEN("some photos on database"){
    PhotoHelper::insertPhotos();

GIVEN("a PhotoController instance"){
WHEN("it receive a request to GET an inexistent photo"){
    executeGetRequest(route(Constants::PhotosPath, Constants::UnexistentId));

THEN("it should respond with an error message"){
    CHECK(response.statusLine().getStatusCode() == StatusResponses::NOT_FOUND.getStatusCode());
    RequestUtils::assertMessageStatus(response, StatusResponses::NOT_FOUND.getStatusCode());
}//THEN
}//WHEN
}
}
}

void PhotoControllerFixture::testGetImage(){
    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Read);

GIVEN("some photo with a valid image"){
    Photo photoWithImage;
    photoWithImage.setImagePath(VALID_IMAGE_PATH);
    photoWithImage.setContextId(ctxId);

    Photo photo = Data::instance().photos().insert(photoWithImage);

WHEN("a GET request is made to a PhotoController using the image route of that photo"){
    executeGetRequest(Constants::PhotosUrl + route(photo.getId(), "image"));

THEN("it should sent the photo image as response"){
    RequestUtils::assertOkMessage(response);

    const vector<std::string> sentFiles = response.getSentFiles();
    REQUIRE(sentFiles.size() > 0);
    CHECK(sentFiles[0] == string(VALID_IMAGE_PATH));

}//THEN
}//WHEN
}//GIVEN

}

void PhotoControllerFixture::testDeletePhoto(){
    long ctxId = PhotoContextHelper::insertContext().getId();

GIVEN("some photo with a valid image"){
    std::string imageFile = getValidImage();
    INFO("image file path: " << imageFile);

    Photo photoToDelete = PhotoHelper::insertPhotoAtContext(ctxId, Photo{imageFile});

WHEN("calling DELETE using that photo id and with a valid session token on request"){
    long userId = SessionHelper::putUserWithPermission(request, ctxId, PermissionLevel::Write);

    executeDeleteRequest(route(Constants::PhotosPath, photoToDelete.getId()));

THEN("it should respond with an ok json message"){
    RequestUtils::assertOkJsonResponse(response);

AND_THEN("the json response should be equivalent to the deleted photo"){
    Photo responsePhoto = getResponse<Photo>();
    PhotoHelper::assertEquals(photoToDelete, responsePhoto);

AND_THEN("the deleted photo should be removed from database"){
    CHECK(!Data::instance().photos().exist(VALID_PHOTO_ID));

AND_THEN("the deleted image should be removed from filesystem"){
    CHECK(!FileUtils::instance().exists(imageFile));

AND_THEN("a PhotoRemoved event should be created on database"){
    checkEventCreated(responsePhoto, EventType::PhotoRemove, userId);
}//then
}//then
}//then
}//then
}//then
}//when
}//given

}//test

void PhotoControllerFixture::testUpdatePhoto(){
    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Write);

GIVEN("some valid photo on database to update"){
    long photoId = PhotoHelper::insertPhotoAtContext(ctxId);

    Photo photo;
    REQUIRE(Data::instance().photos().getPhoto(photoId, photo));

    photo.getDescription().setMoment("other moment");
    photo.getDescription().setDescription("Other description");

WHEN("calling POST using that photo id and a photo json representation on message body"){
    executePostRequest(PHOTO_1_URL, &photo);

    INFO("Response: \n" << response.getContentStr());

THEN("it should update the photo on database"){
    Photo databasePhoto;
    REQUIRE(Data::instance().photos().getPhoto(photoId, databasePhoto));

    CHECK(photo.getId() == databasePhoto.getId());
    CHECK(photo.getImagePath() == databasePhoto.getImagePath());
    CHECK(photo.getDescription() == databasePhoto.getDescription());
    CHECK(photo == databasePhoto);

AND_THEN("it should respond with an json representation of the updated photo"){
    RequestUtils::assertOkMessage(response);
    Json::Value photoJson = RequestUtils::assertJsonObjectResponse(response);
    PhotoHelper::assertEquals(databasePhoto, photoJson);
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//test

void PhotoControllerFixture::testUpdatePhotoWithNotAllFields(){
    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Write);

GIVEN("a PhotoController instance"){

GIVEN("some valid photo on database to update"){
    Photo photo = PhotoHelper::insertPhotoAtContext(ctxId);
    long photoId = photo.getId();
    photo.getDescription().setDescription("Other description");
    photo.getDescription().setMoment("this field will not change");
    photo.getDescription().addMarker(PhotoMarker("this will not be inserted"));

GIVEN("a json representation of that photo with not all fields"){
    JsonSerializer jsonSerializer;
    Json::Value outJson;
    photo >> jsonSerializer >> outJson;
    outJson.removeMember(PhotoFields::MARKERS);
    outJson.removeMember(PhotoFields::MOMENT);

WHEN("making an request to update that photo using a photo json representation on the message body"){
    outJson >> jsonSerializer;

    stringstream strStream;
    strStream << jsonSerializer;
    request.setContentStr(strStream.str());
    request.setContentType(ContentTypes::JSON);

    executePostRequest(route(Constants::PhotosPath, photoId));
    INFO("Response: \n" << response.getContentStr());

THEN("it should update the photo on database, but only the fields on json"){
    Photo databasePhoto;
    bool foundPhoto = Data::instance().photos().getPhoto(photoId, databasePhoto);
    REQUIRE(foundPhoto);

    INFO("photo To Update:\n " << (jsonSerializer << photo));
    jsonSerializer.clear();
    INFO("database photo:\n" << (jsonSerializer << databasePhoto));

    CHECK(photo.getId() == databasePhoto.getId());
    CHECK(photo.getImagePath() == databasePhoto.getImagePath());
    CHECK(photo.getDescription().getDate() == databasePhoto.getDescription().getDate());
    CHECK(photo.getDescription().getDescription() == databasePhoto.getDescription().getDescription());
    CHECK(photo.getDescription().getPlace() == databasePhoto.getDescription().getPlace());

    CHECK(photo.getDescription().getMarkers() != databasePhoto.getDescription().getMarkers());
    CHECK(photo.getDescription().getMoment() != databasePhoto.getDescription().getMoment());
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}

}//test

void PhotoControllerFixture::testUpdatePhotoEvent(){

GIVEN("an update photo request with a valid session token"){
    long ctxId = PhotoContextHelper::insertContext().getId();
    long userId = SessionHelper::putUserWithPermission(request, ctxId, PermissionLevel::Write);

WHEN("a PhotoController handle this request"){
    Photo updatePhoto = PhotoHelper::insertPhotoAtContext(ctxId);

    string reqRoute = route(api::paths::photos, updatePhoto.getId());
    executePostRequest(reqRoute, &updatePhoto);

THEN("a new PhotoUpdate Event should be created on database"){
    RequestUtils::assertOkJsonResponse(response);

    checkEventCreated(updatePhoto, EventType::PhotoUpdate, userId);
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoControllerFixture::testTryUpdatePhotoWithInvalidMessage(){
    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Write);

GIVEN("some photos on database"){
    PhotoHelper::insertPhotoAtContext(ctxId);

    WHEN("calling POST using an invalid id"){
        executePostRequest(PHOTO_INVALID_URL);
        THEN("it should respond with an not found message"){
            RequestUtils::assertMessageStatus(response, StatusResponses::NOT_FOUND.getStatusCode());
        }//THEN
    }//WHEN
    WHEN("calling PUT or POST using a valid id"){
        AND_WHEN("passing an empty message body"){
            request.setContentStr("");
            executePutRequest(PHOTO_1_URL);
            THEN("it should respond with an bad request message"){
                RequestUtils::assertMessageStatus(response, StatusResponses::BAD_REQUEST.getStatusCode());
            }
        }//WHEN
        AND_WHEN("passing an non-json message body"){
            request.setContentStr("Lorem ipsum");
            executePutRequest(PHOTO_1_URL);
            THEN("it should respond with an bad request message"){
                RequestUtils::assertMessageStatus(response, StatusResponses::BAD_REQUEST.getStatusCode());
            }
        }//WHEN
        AND_WHEN("passing an malformed-json message body"){
            request.setContentStr("{\"invalid json\": true ");
            executePutRequest(PHOTO_1_URL);
            THEN("it should respond with an bad request message"){
                INFO("Response:\n" << response.getContentStr());
                RequestUtils::assertMessageStatus(response, StatusResponses::BAD_REQUEST.getStatusCode());
            }
        }//WHEN
    }//WHEN
}//GIVEN

}//test

void PhotoControllerFixture::testGetPhotosFromCollection(){

    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Read);

GIVEN("some PhotoCollection with photos"){
    PhotoHelper::insertPhotos(2); //Photos not on collection

    PhotoCollection coll = PhotoCollectionHelper::createCollectionWithPhotosAndSubCollections(2, 0, -1, ctxId);
    const std::set<long> & collPhotos = coll.getPhotos();

WHEN("a GET request is made to a PhotoController base url with that PhotoCollection id as parameter"){
    putCollectionIdParameter(coll.getId());
    executeGetRequest(Constants::PhotosUrl);

THEN("it should respond with a json representation of the photos in that collection"){
    checkPhotosJsonResponse(collPhotos);
}//THEN
}//WHEN
}//GIVEN

}

void PhotoControllerFixture::testGetPhotosFromCollectionsHierarchy(){

    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Read);

GIVEN("some PhotoCollection with photos and subcollections"){
    std::set<long> hierarchyPhotos;
    PhotoCollection rootColl = PhotoCollectionHelper::createPhotoHierarchy(2,2, hierarchyPhotos, ctxId);

WHEN("a GET request is made to a PhotoController base url using that collection id as parameter"){
    putCollectionIdParameter(rootColl.getId());

AND_WHEN("the request the 'recursive' query parameter set to true"){
    executeGetRequest(string(Constants::PhotosUrl) + "?" + api::parameters::recursive + "=true");

THEN("it should respond with the photos from all 'descendents' of the given collection"){
    checkPhotosJsonResponse(hierarchyPhotos);
}//THEN
}//WHEN
}//WHEN
}//GIVEN

}

void PhotoControllerFixture::testFilterPhotosFromCollectionsUsingQuery(bool createHierarchy){

GIVEN("a hierarchy of photo collections in which some photos contains some query string"){
    long ctxId = SessionHelper::requireContextPermission(request, PermissionLevel::Read);

    const string queryString = "any string";

    std::set<long> hierarchyPhotos;
    PhotoCollection rootCollection;
    std::vector<Photo> expectedPhotos = genHierarchyWithQueryString(ctxId, queryString, createHierarchy, rootCollection, hierarchyPhotos);

WHEN("a request is made to a PhotoController using that query string and the root controller id"){
    putCollectionIdParameter(rootCollection.getId());
    executeGetRequest(Constants::PhotosUrl, {{api::parameters::recursive, "true"}, {api::parameters::query, queryString}});

THEN("it should respond with all photos that match the query and are 'descendents' from the given collection"){

    RequestUtils::assertOkJsonResponse(response);

    vector<Photo> photos = deserializeResponseItems<Photo>(api::fields::photos);
    std::sort(photos.begin(), photos.end());
    std::sort(expectedPhotos.begin(), expectedPhotos.end());

    REQUIRE(photos.size() == expectedPhotos.size());
    for(size_t i=0; i < photos.size(); ++i){
        //Some field are not (de)serialized
        CHECK(photos[i].getDescription() == expectedPhotos[i].getDescription());
    }
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoControllerFixture::testPhotoCreationAtCollection(){

    GIVEN("a valid session token cookie associated with some user account"){
        long userId = setupSessionToken();

    WHEN("a POST request is made to the photos route of some PhotoController"){
        request.setUrl(Constants::PhotosUrl);

    WHEN("the request contains a PhotoCollection id as parameter"){
        long ctxId = PhotoContextHelper::insertContext().getId();
        long collId = PhotoCollectionHelper::insertCollection(-1, ctxId);
        request.putParameter(api::parameters::PhotoCollectionId, StrCast().toString(collId));
        INFO("Collection id: " << collId);

        verifyPhotoCreation(-1, collId, userId);
    }//WHEN
    }
    }//GIVEN
}

void PhotoControllerFixture::testPhotoCreationAtContext(){
    GIVEN("a valid session token cookie associated with some user account"){
        long userId = setupSessionToken();

    WHEN("a POST request is made to the photos route of some PhotoController"){
        request.setUrl(Constants::PhotosUrl);

    WHEN("the request contains a PhotoContext id as parameter"){
        PhotoContext ctx = PhotoContextHelper::insertContextWithRootCollection();
        long ctxId = ctx.getId();
        request.putParameter(api::parameters::PhotoContextId, StrCast().toString(ctxId));

        INFO("Context id: " << ctxId);

        verifyPhotoCreation(ctxId, -1, userId);
    }//WHEN
    }
    }
}//test

void PhotoControllerFixture::testTryCreatePhotoWithInvalidContentType(){

    WHEN("a POST request is made to the photos route of some PhotoController"){
    AND_WHEN("passing an invalid content Type on message"){
        request.setContentType(ContentTypes::TEXT_PLAIN);

        executePostRequest(Constants::PhotosUrl);
    THEN("it should respond with a bad request message"){
        INFO("Response: " << response.getContentStr());
        RequestUtils::assertMessageStatus(response, StatusResponses::BAD_REQUEST.getStatusCode());
    }
    }
    }

}//test

void PhotoControllerFixture::testTryCreatePhotoWithoutContextOrCollection(){
    WHEN("a POST request is made to the photos route of some PhotoController"){
    request.setUrl(Constants::PhotosUrl);

    WHEN("the request has no PhotoContext or PhotoCollection id as parameter"){
        verifyPhotoCreation(-1, -1);
    }//WHEN
    }
}//test

void PhotoControllerFixture::verify_CreatePhoto_fromImageRequest_Behaviour(long collId, long ctxId, long userId)
{
    AND_WHEN("passing an image data on the message body"){
        fstream image(VALID_IMAGE_PATH);
        REQUIRE(image.good());
        request.setContent(image);
        request.setContentType(ContentType(IMAGE_CONTENT_TYPE));

        executePostRequest(request.getUrl());

        if(collId < 0 && ctxId < 0){
            verifyBadRequestResponse();
        }
        else{
            verify_CreatePhotoFromImage_successful(collId, ctxId, userId);
        }//else
    }//WHEN
}

void PhotoControllerFixture::verify_CreatePhoto_fromMultipartRequest_Behaviour(long collId, long ctxId, long userId)
{
    AND_WHEN("passing an multipart content Type on message"){
        setupMultipartRequest();

    AND_WHEN("the multipart message contains an valid json part and an valid image part"){
        executePostRequest(request.getUrl());

        if(collId < 0 && ctxId < 0){
            verifyBadRequestResponse();
        }
        else{
            checkCreatePhotoFromMultipartResponse(*jsonPart, collId, ctxId, userId);
        }
    }
    }
}

void PhotoControllerFixture::checkCreatePhotoFromMultipartResponse(MockRequest & jsonPart
    , long collId, long ctxId, long userId)
{
    THEN("it should respond with a valid json representation of the created photo"){
        Photo returnedPhoto = assertValidJsonPhotoResponse(response);

    AND_THEN("it should create an event on database"){
        checkEventCreated(returnedPhoto, EventType::PhotoCreation, userId);

    AND_THEN("The json description of the return photo should be equals to the send json"){
        Photo expectedPhoto;
        serializer.clear();
        jsonPart.getContentStr() >> serializer >> expectedPhoto;

        CHECK(returnedPhoto.getDescription() == expectedPhoto.getDescription());

    AND_THEN("the create photo should have the context id in which it was created"){
        Photo photoOnDb = photoFrame.getPhoto(returnedPhoto.getId());
        verifyContextIdOfCreatedPhoto(photoOnDb, ctxId, collId);

    AND_THEN("the created image should be put on image folder"){
        string imgPath = verifyPhotoImagePath(photoOnDb);
        REQUIRE(Files().remove(imgPath));
    }//THEN
    }//THEN
    }//THEN
    }//THEN
    }//THEN
}

void PhotoControllerFixture::verify_CreatePhotoFromImage_successful(long collId, long ctxId, long userId)
{
    THEN("it should create a new photo and respond with a json representation of the created photo"){
        Photo responsePhoto = assertValidJsonPhotoResponse(response);

    AND_THEN("a new PhotoCreation event should be added to the database"){
        checkEventCreated(responsePhoto, EventType::PhotoCreation, userId);

    AND_THEN("the photo on response should not contain image path"){
        CHECK(responsePhoto.getImagePath().empty());

    THEN("the created photo should have the context in which it was created"){
        Photo createdPhoto;
        Data::instance().photos().getPhoto(responsePhoto.getId(), createdPhoto);
        verifyContextIdOfCreatedPhoto(createdPhoto, ctxId, collId);

    AND_THEN("it should copy the image to the image folder"){

        verifyPhotoContainer(createdPhoto, ctxId, collId);

        string imgPath = verifyPhotoImagePath(createdPhoto);

        //Removendo cópia
        REQUIRE(FileUtils::instance().remove(imgPath));
    }//THEN
    }//THEN
    }//THEN
    }//THEN
    }//THEN
}

/* ***************************************** Fixture implementation *****************************************/

PhotoControllerFixture::PhotoControllerFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
    , photoFrame()
    , photoController(&photoFrame)
{

    this->attachControllerToUrl(Constants::PhotosUrl);
    setController(&controllerRouter);
}

void PhotoControllerFixture::attachControllerToUrl(const std::string & url){
    photoController.setBaseUrl(url);
    controllerRouter.add(url, &photoController);
}

void PhotoControllerFixture::putContextParameter(long ctxId){
    request.putParameter(api::parameters::PhotoContextId, std::to_string(ctxId));
}

void PhotoControllerFixture::putCollectionIdParameter(long collId)
{
    request.putParameter(api::parameters::PhotoCollectionId, StringCast::cast().toString(collId));
}

void PhotoControllerFixture::checkPhotosJsonResponse(const std::set<long>& expectedPhotoIds)
{
    RequestUtils::assertOkJsonResponse(response);

    vector<Photo> photos = deserializeResponseItems<Photo>(api::fields::photos);

    CHECK(photos.size() == expectedPhotoIds.size());
    for(int i=0; i < photos.size(); ++i){
        long photoId = photos[i].getId();
        CAPTURE(photoId);
        CHECK(expectedPhotoIds.find(photoId) != expectedPhotoIds.end());
    }
}


std::vector<Photo> PhotoControllerFixture::genHierarchyWithQueryString(long ctxId, const string & queryString
    , bool createSubCollections
    , PhotoCollection & outRootColl, std::set<long> & outHierarchyPhotos)
{
    std::vector<Photo> expectedPhotos;

    int subcollectionsCount = createSubCollections ? 2 : 0;

    outRootColl = PhotoCollectionHelper::createPhotoHierarchy(subcollectionsCount,2, outHierarchyPhotos, ctxId);

    for(long photoId : outHierarchyPhotos){
        if(photoId %2 != 0){
            Photo photo;
            Data::instance().photos().getPhoto(photoId, photo);

            photo.getDescription().setDescription(queryString);

            Data::instance().photos().update(photo);

            expectedPhotos.push_back(photo);
        }
    }

    return expectedPhotos;
}

Photo PhotoControllerFixture::assertValidJsonPhotoResponse(MockResponse & response){
    RequestUtils::assertOkMessage(response);
    Json::Value jsonResponse = RequestUtils::assertJsonObjectResponse(response);

    JsonSerializer serializer;
    Photo deserializedPhoto;
    jsonResponse >> serializer >> deserializedPhoto;

    CHECK(Data::instance().photos().exist(deserializedPhoto.getId()));

    return deserializedPhoto;
}

string PhotoControllerFixture::getValidImage(){
    //Copying the image
    std::string imageFile = FileUtils::instance().copyTmpFile(VALID_IMAGE_PATH);
    REQUIRE(!imageFile.empty());
    REQUIRE(FileUtils::instance().exists(imageFile));

    return imageFile;
}

Session PhotoControllerFixture::createValidSession(){
    return SessionManager::instance().startLocalSession("notASecret");
}

string PhotoControllerFixture::createTmpDir(){
    string tmpDir = Files().generateTmpFilepath();
    Files().createDirIfNotExists(tmpDir);

    return tmpDir;
}

void PhotoControllerFixture::checkEventCreated(const Photo & photo, EventType eventType, long userId){
    EventsHelper::checkEventCreated(eventType, photo.getId(), userId);
}

long PhotoControllerFixture::setupSessionToken(){
    return SessionHelper::setupSessionToken(request);
}


void PhotoControllerFixture::setupMultipartRequest()
{
    //This data is specific to this test file.
    string boundaryMessage = "---------------------------36234065812799759491592900430";

    ContentType messageContentType = ContentTypes::MULTIPART_FORM;
    messageContentType.putParameter("boundary", boundaryMessage);
    request.setContentType(messageContentType);
    REQUIRE(request.getContentType().getParameter("boundary").empty() == false);
    
    setup_CreatePhoto_ImagePart();
    setup_CreatePhoto_JsonPart();

    mockupMultipartRequestReader();
}

void PhotoControllerFixture::mockupMultipartRequestReader()
{
    multipartReaderMockup.Reset();
    Fake(Dtor(multipartReaderMockup));
    When(Method(multipartReaderMockup, start)).AlwaysReturn(true);
    When(Method(multipartReaderMockup, next)).Return(imagePart).Return(jsonPart).Return({});
    When(Method(multipartReaderMockup, clear)).AlwaysReturn();

    auto mock_ptr=std::shared_ptr<MultipartRequestReader>(&multipartReaderMockup.get(),[](...){});
    photoController.setMultipartReader(mock_ptr);    
}

void PhotoControllerFixture::setup_CreatePhoto_ImagePart()
{
    imagePart = make_shared<MockRequest>();
    
    fstream imagePartContent(VALID_IMAGE_PATH);
    REQUIRE(imagePartContent.good());
    imagePart->setContent(imagePartContent);
    imagePart->setContentType(ContentTypes::IMAGE_JPEG);
    imagePartContent.close();
}

void PhotoControllerFixture::setup_CreatePhoto_JsonPart()
{
    jsonPart = make_shared<MockRequest>();
    
    fstream jsonPartContent(VALID_JSON_PATH);
    REQUIRE(jsonPartContent.good());
    jsonPart->setContent(jsonPartContent);
    jsonPart->setContentType(ContentTypes::JSON);
    jsonPartContent.close();
}

void PhotoControllerFixture::verifyContextIdOfCreatedPhoto(const Photo & responsePhoto, long ctxId, long collId){
    if(ctxId > PhotoContext::INVALID_ID){
        CAPTURE(ctxId);
        CHECK(responsePhoto.getContextId() == ctxId);
    }
    else if(collId > PhotoContext::INVALID_COLLECTION_ID){
        CAPTURE(collId);
        long collCtxId = Data::instance().collections().get(collId).getParentContext();
        CHECK(collCtxId > PhotoContext::INVALID_ID);
        CHECK(responsePhoto.getContextId() == collCtxId);
    }
}
