#ifndef USERCONTROLLERTEST_H
#define USERCONTROLLERTEST_H

#include "TestMacros.h"

#include <fakeit.hpp>

#include "api/Api.h"
#include "api/UsersController.h"
#include "api/InvalidRequestException.h"

#include "model/user/UserAccount.h"
#include "model/user/UserLoginInfo.h"
#include "model/user/Session.h"
#include "model/image/Image.h"
#include "services/AccountManager.h"
#include "services/SessionManager.h"

#include "exceptions/UnsupportedContentTypeError.h"

#include "network/StatusResponses.h"
#include "network/Cookie.h"

#include "data/Data.h"
#include "mocks/data/data-memory/MemoryDataBuilder.hpp"

#include "utils/StringCast.h"

#include "helpers/WebControllerFixture.h"
#include "helpers/RequestUtils.h"
#include "helpers/UserHelper.h"
#include "helpers/CaptureArguments.h"

#include "serialization/SerialMap.h"

#include <fstream>

using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace calmframe::network;
using namespace std;
using namespace fakeit;

class UsersControllerFixture : public WebControllerFixture{
public:
    void testListUsers();
    void testGetCurrentUserData();
    void testGetUserData();

    void testCreateUser();
    void testCreateAccountByNonAdmin();
    void testRejectCreateAccountWithExistentLogin();

    void testUpdateUserPassword();

    void testGetUserProfileImage();
    void testUpdateUserProfileImage();
    void testUpdateProfileImageInvalidContentType();
    void testUpdateProfileImageInvalidImage();

    void testAdminDeleteAccount();
    void testRejectUnauthorizedDelete();
    void testDeleteOwnAccount();

    void testAdminEnableAccount();
    void testRejectUnauthorizedUpdate();
    void testUserCannotTurnItselfAdmin();

    void testMeRedirection();
    void testUserIdRequestParam();
public:
    UsersController controller;
    AccountManager accountManager;
    SessionManager sessionManager;

    fakeit::Mock<AccountManager> accountManagerMockup;

    UserAccount account;
    UserAccount loggedInUser;
    
    shared_ptr<Image> returnedImage;

    UsersControllerFixture();

    void setupAdminRequest();
    void setupNonAdminRequest();

    void setupSession(UserAccount account, const std::string & password);

    void givenCreateUserRequest(const string & login, const string & password);
    
    UserAccount createUser(const string & login, const string & password);
    UserAccount createOtherUser();

    Session loginUser();
    Session loginUser(const UserAccount & account, const string & password=DEFAULT_PASSWORD);
    void putTokenHeader(const string & token);

    void checkUsersExist(const std::vector<User> & users);
    void checkForbiddenResponse();
    
    UserAccount given_existent_account(long userId=1);
    UserAccount given_existent_account(UserAccount account);
    long given_a_logged_in_user_mockup(const UserAccount & baseAccount={});
    long given_a_logged_in_admin_mockup();
    void given_post_image_request();
    long given_invalid_image_request();
    
    void when_doing_a_request(WebController & controller, const string & method, const string & path);
    void when_receives_update_profile_image_request(long userId);
    void when_receiving_get_image_profile(long userId);
    void when_receiving_an_activate_user_request(const UserAccount & account);
    
    void check_account_was_activated(long userId);
    void using_account_manager_mockup();
    void check_update_profile_image_called();
    void check_ok_response();
    void check_sent_image_file();
    
public:
    static constexpr const char * VALID_IMAGE_PATH = "test/data/test-images/test1.jpg";
};

#define FIXTURE UsersControllerFixture
#define TEST_TAG "[UsersController]"

namespace Constants {
    const string someLogin = "userLogin";
    const string somePassword = "123456";
}//namespace

#endif // USERCONTROLLERTEST_H
