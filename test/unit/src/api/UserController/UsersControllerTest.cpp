#include "UserControllerSteps.h"

TEST_SCENARIO("List users")                     IS_DEFINED_BY(testListUsers)
TEST_SCENARIO("Get current user data")          IS_DEFINED_BY(testGetCurrentUserData)
TEST_SCENARIO("Get user data")                  IS_DEFINED_BY(testGetUserData)
TEST_SCENARIO("Create user")                    IS_DEFINED_BY(testCreateUser)
TEST_SCENARIO("A user can update his password") IS_DEFINED_BY(testUpdateUserPassword)

TEST_SCENARIO("account created by non-admin should start inactive and non-admin") IS_DEFINED_BY(testCreateAccountByNonAdmin)

TEST_SCENARIO("Redirect request to current user")                        IS_DEFINED_BY(testMeRedirection)
TEST_SCENARIO("UsersController should put user id as request parameter") IS_DEFINED_BY(testUserIdRequestParam)

TEST_SCENARIO("Try create user with already existent login") IS_DEFINED_BY(testRejectCreateAccountWithExistentLogin)


/*************************************** Test implementation *****************************************/

void UsersControllerFixture::testListUsers(){
    GIVEN("some usersAccount on database"){
        UserHelper::createAccounts(2);
    
    WHEN("it is a GET request is to the base route of a UsersController"){
        this->executeGetRequest("/");
    
    THEN("it should respond with a list of Users from database"){
        RequestUtils::assertOkJsonResponse(response);
    
        readResponse();
        std::vector<User> users = this->deserializeItems<User>(api::fields::users);
        CHECK(users.size() > 0);
    
        checkUsersExist(users);
    }//THEN
    }//WHEN
    }//GIVEN
}

void UsersControllerFixture::testGetCurrentUserData(){
    GIVEN("a logged user"){
        Session userSession = loginUser();
    
    GIVEN("a get request with a session token related to that user"){
        putTokenHeader(userSession.getToken());
    
    WHEN("that request is made to the 'current user' route of a UsersController"){
        executeGetRequest(route(api::paths::current_user));
    
    THEN("a successful response should be sent"){
        RequestUtils::assertOkJsonResponse(response);
    
    THEN("that response should contain a json representation of that user"){
        User userResponse = getResponse<User>();
        UserHelper::checkEquals(account.getUser(), userResponse);
    
    }//THEN
    }//THEN
    }//WHEN
    }//WHEN
    }//GIVEN
}

void UsersControllerFixture::testGetUserData(){
GIVEN("some user account"){
    UserAccount someAccount = UserHelper::createAccount(User{Constants::someLogin, "User Name"}, Constants::somePassword);

WHEN("a get request is made to that user route"){
    executeGetRequest(route(someAccount.getId()));

THEN("user representation should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    User userResponse = getResponse<User>();
    UserHelper::checkEquals(someAccount.getUser(), userResponse);
}//THEN
}//WHEN
}//GIVEN

}

void UsersControllerFixture::testCreateUser(){

GIVEN("a POST request with a LoginInfo json body"){
    UserLoginInfo loginInfo(Constants::someLogin, Constants::somePassword);
    this->putJsonRequestContent(&loginInfo);

WHEN("that request is made to the base route of a UsersController"){
    this->executePostRequest("/");

THEN("it should respond with a new User json representation"){
    RequestUtils::assertOkJsonResponse(response);

    User createdUser = getResponse<User>();
    CHECK(createdUser.getLogin() == loginInfo.getLogin());

THEN("the created user should exist on database"){
    CHECK(Data::instance().users().exist(createdUser.getId()));

}//THEN
}//THEN
}//WHEN
}//GIVEN

}

void UsersControllerFixture::testCreateAccountByNonAdmin(){
    setupNonAdminRequest();

WHEN("a create request is sent by a non admin user"){
    UserLoginInfo newUserLogin("other", "password");

    executePostRequest("/", &newUserLogin);

THEN("the created account should not be active and not be admin"){
    User createdAccount = getResponse<User>();
    CHECK(!createdAccount.isActive());
    CHECK(!createdAccount.isAdmin());

}//THEN
}//WHEN

}

void UsersControllerFixture::testRejectCreateAccountWithExistentLogin(){

GIVEN("an existent login"){
    UserAccount account = createUser(Constants::someLogin, Constants::somePassword);

WHEN("a create request is made with same username to a UsersController"){
    givenCreateUserRequest(account.getLogin(), Constants::somePassword);
    this->executePostRequest("/");

THEN("it should respond with a BadRequest message"){
    RequestUtils::assertBadRequestResponse(response);

}//THEN
}//WHEN
}//GIVEN

}

void UsersControllerFixture::testUpdateUserPassword(){
    controller.setAccountManager(&accountManagerMockup.get());

    GIVEN("a logged in user"){
       long userId = given_a_logged_in_user_mockup();
    
    GIVEN("a change password request is made by that user passing new password"){
        string newPassword = "new pass";    

        CaptureArguments<void, long, string> captureUpdatePass;
        fakeit::When(Method(accountManagerMockup, updatePassword)).Do(captureUpdatePass);                
        
        auto data = SerialMap({{"password", newPassword}});
        auto routeReq = this->route(userId, api::paths::change_password);
        
    WHEN("a user controller receives the request"){
        executePostRequest(routeReq, &data);
        
    THEN("a successfull response should be sent"){
        RequestUtils::assertOkMessage(response);
    
    THEN("his password should change to the new password"){
        CHECK(captureUpdatePass.getLast<0>() == userId);
        CHECK(captureUpdatePass.getLast<1>() == newPassword);
        
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}

void UsersControllerFixture::testMeRedirection(){
    using_account_manager_mockup();
    fakeit::Mock<UsersController> controllerSpy{controller};
    
    GIVEN("a logged in user"){
        long userId = given_a_logged_in_user_mockup();    
    
    WHEN("a request is made to 'current user' route with any method"){
        When(Method(controllerSpy, onUpdateMember)).AlwaysReturn(true);
        when_doing_a_request(controllerSpy.get(), "POST", route(api::paths::current_user));
    
    THEN("that request should be redirected to that user route"){
        Verify(Method(controllerSpy, onUpdateMember)
                .Using(_, _, _, userId));
        
    }//THEN
    }//WHEN
    }//GIVEN
}


void UsersControllerFixture::testUserIdRequestParam(){
WHEN("a UsersController receive a request with a valid session token"){
    Session session = loginUser();
    putTokenHeader(session.getToken());

    executeGetRequest("/");
THEN("it should put the user Id as parameter on request"){
    long userId = StrCast().toLong(request.getParameter(api::parameters::userId));
    CHECK(userId == session.getUserId());

}//THEN
}//WHEN

}

