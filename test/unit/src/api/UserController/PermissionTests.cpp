#include "UserControllerSteps.h"

TEST_SCENARIO("Admin can delete any account")                IS_DEFINED_BY(testAdminDeleteAccount)
TEST_SCENARIO("Non admin cannot remove other users")         IS_DEFINED_BY(testRejectUnauthorizedDelete)
TEST_SCENARIO("Any user can remove itself")                  IS_DEFINED_BY(testDeleteOwnAccount)

TEST_SCENARIO("Admin can enable or disable an user account") IS_DEFINED_BY(testAdminEnableAccount)
TEST_SCENARIO("A users data can be changed only by himself") IS_DEFINED_BY(testRejectUnauthorizedUpdate)
TEST_SCENARIO("Non admin user cannot turn itself admin")     IS_DEFINED_BY(testUserCannotTurnItselfAdmin)


void UsersControllerFixture::testAdminDeleteAccount(){
    setupAdminRequest();

    GIVEN("an existent user on db"){
        long userId = account.getId();
    
    WHEN("a UserController receive a DELETE request to that user route"){
        string route = "/" + utils::StrCast().toString(userId);
    
        this->executeRequest("DELETE", route);
    
    THEN("it should respond with a json representation of that user"){
        User responseData = getResponse<User>();
        CHECK(responseData == account.getUser());
    
    THEN("that user should not exist on database anymore"){
        CHECK(!Data::instance().users().exist(userId));
    
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}

void UsersControllerFixture::testRejectUnauthorizedDelete(){
    WHEN("a non admin do a request to remove another user"){
        setupNonAdminRequest();
        UserAccount otherUser = UserHelper::createAccount("otherUser");
    
        executeRequest("DELETE", route(otherUser.getId()));
    
    THEN("a Forbidden error should be sent as response"){
        checkForbiddenResponse();
    
    }//THEN
    }//WHEN
}

void UsersControllerFixture::testDeleteOwnAccount(){
    setupNonAdminRequest();

    WHEN("a non admin do a request to remove itself"){
        executeRequest("DELETE", route(account.getId()));
    
    THEN("the request should succeed"){
        RequestUtils::assertOkJsonResponse(response);
    
    }//THEN
    }//WHEN
}

void UsersControllerFixture::testAdminEnableAccount(){
    using_account_manager_mockup();

    GIVEN("an inactive user account"){
        UserAccount account = given_existent_account(UserAccount().setActive(false));

    WHEN("the api receives an admin request to activate the account"){
        given_a_logged_in_admin_mockup();
        when_receiving_an_activate_user_request(account);
    
    THEN("that account should be activated"){
        check_account_was_activated(account.getId());
    
    THEN("an ok message should be sent with the updated field as response"){
        check_ok_response();
        
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}

void UsersControllerFixture::testRejectUnauthorizedUpdate(){
    using_account_manager_mockup();
    
    GIVEN("an admin account"){
        given_a_logged_in_admin_mockup();
    
    WHEN("trying to update another user account"){
        auto otherAccount = given_existent_account(100);
        
        executePostRequest(route(otherAccount.getId()), otherAccount.getUser());
    
    THEN("a forbidden message should be sent as response"){
        checkForbiddenResponse();
    
    AND_THEN("the user should not have been updated"){
        Verify(Method(accountManagerMockup, updateUser)).Never();
    
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}

void UsersControllerFixture::testUserCannotTurnItselfAdmin(){
    WHEN("an update request is made by an non admin user, trying to change privileged fields"){
        setupNonAdminRequest();
    
        account.setIsAdmin(true);
        executePostRequest(route(account.getId()), &account.getUser());
    
    THEN("an forbidden message should be sent as response"){
        checkForbiddenResponse();
    }//THEN
    }//WHEN
}
