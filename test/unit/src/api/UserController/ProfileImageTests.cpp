#include "UserControllerSteps.h"

TEST_SCENARIO("Update user profile image from request")         IS_DEFINED_BY(testUpdateUserProfileImage)
TEST_SCENARIO("Update profile image with invalid content type") IS_DEFINED_BY(testUpdateProfileImageInvalidContentType)
TEST_SCENARIO("Update profile image with invalid image")        IS_DEFINED_BY(testUpdateProfileImageInvalidImage)

TEST_SCENARIO("Get user profile image request")                 IS_DEFINED_BY(testGetUserProfileImage)

/*************************************** Test implementation *****************************************/

void UsersControllerFixture::testGetUserProfileImage(){
    using_account_manager_mockup();
    
    GIVEN("an existent user account with a profile image"){
        long accountId = 10;
        given_existent_account(accountId);
        
    GIVEN("any logged in user"){
        long loggedUser = given_a_logged_in_user_mockup();
    
        REQUIRE(accountId != loggedUser);
        
    WHEN("a request to get that image is received"){
        when_receiving_get_image_profile(accountId);
    
    THEN("an ok response should be sent"){
        check_ok_response();
    
    THEN("an image file should be sent as response"){
        check_sent_image_file();
        
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}

void UsersControllerFixture::testUpdateUserProfileImage(){
    using_account_manager_mockup();
    
    GIVEN("a logged in user account"){
        long userId = given_a_logged_in_user_mockup();
    
    GIVEN("a POST image request is made by that account"){
        given_post_image_request();
    
    WHEN("the 'profile image' route receives that request"){
        when_receives_update_profile_image_request(userId);
    
    THEN("user profile image should be updated with the request content"){
        check_update_profile_image_called();
          
    }//THEN
    }//WHEN
    }//WHEN
    }//GIVEN
}

void UsersControllerFixture::testUpdateProfileImageInvalidContentType(){
    using_account_manager_mockup();
    
    GIVEN("a logged in account"){
        long userId = given_a_logged_in_user_mockup();
    WHEN("trying to update its profile image with a non image content type"){
        request.setContentType(ContentTypes::JSON);
    
    THEN("an UnsupportedContentTypeError should be thrown"){
        CHECK_THROWS_AS(when_receives_update_profile_image_request(userId), UnsupportedContentTypeError);
    
    }//THEN
    }//WHEN
    }//GIVEN
}

void UsersControllerFixture::testUpdateProfileImageInvalidImage(){
    using_account_manager_mockup();
    
    WHEN("trying to update a user profile image with invalid image"){
        long userId = given_invalid_image_request();

    THEN("an InvalidRequestException should be thrown"){
        CHECK_THROWS_AS(when_receives_update_profile_image_request(userId), api::InvalidRequestException);
    }//THEN
    }//WHEN
    
}
