#include "UserControllerSteps.h"

#include "serialization/GenericField.h"

UsersControllerFixture::UsersControllerFixture(){
    setController(&controller);
    controller.setAccountManager(&accountManager);

    Data::setup(std::make_shared<MemoryDataBuilder>());

    account = UserHelper::createAccount();
}

void UsersControllerFixture::setupAdminRequest(){
    UserAccount adminAccount = UserHelper::createAdminAccount();
    setupSession(adminAccount, DEFAULT_PASSWORD);

}

void UsersControllerFixture::setupNonAdminRequest(){
    setupSession(account, DEFAULT_PASSWORD);
}

void UsersControllerFixture::setupSession(UserAccount account, const string & password){
    Session session = sessionManager.startSession(account, password);
    putTokenHeader(session.getToken());
}
UserAccount UsersControllerFixture::createUser(const string & login, const string & password){
    return accountManager.create(login, password);
}

UserAccount UsersControllerFixture::createOtherUser(){
    return createUser("other", DEFAULT_PASSWORD);
}

Session UsersControllerFixture::loginUser(){
    return loginUser(account);
}

Session UsersControllerFixture::loginUser(const UserAccount & account, const string & password){
    return sessionManager.startSession(account, password);
}

void UsersControllerFixture::putTokenHeader(const string & token){
    string cookieValue = string(api::cookies::sessionCookie).append("=").append(token);
    request.putHeader(network::Header(Headers::Cookie,  cookieValue));
}

void UsersControllerFixture::checkUsersExist(const std::vector<User> & users){
    for(size_t i=0; i < users.size(); ++i){
        long userId = users[i].getId();

        INFO("User id: " << userId);

        CHECK(Data::instance().users().exist(userId));
    }
}

void UsersControllerFixture::checkForbiddenResponse(){
    RequestUtils::assertMessageStatus(response, StatusResponses::FORBIDDEN.getStatusCode());
}

/***********************************************************************************************/

void UsersControllerFixture::using_account_manager_mockup(){
    controller.setAccountManager(&accountManagerMockup.get()); 
    
    When(Method(accountManagerMockup, updateProfileImage)).AlwaysDo([](long, Image &){return User{};});
    
    When(Method(accountManagerMockup, updateUser)).AlwaysDo(
        [](User  & user) -> User &{
            return user;
        }
    );
}

void UsersControllerFixture::givenCreateUserRequest(const string & login, const string & password){
    UserLoginInfo loginInfo(login, password);
    this->putJsonRequestContent(&loginInfo);
}

void UsersControllerFixture::given_post_image_request(){
    fstream image(VALID_IMAGE_PATH);
    REQUIRE(image.good());
    request.setContent(image);
    request.setContentType(ContentTypes::IMAGE_JPEG);
}


long UsersControllerFixture::given_invalid_image_request()
{
    long userId = given_a_logged_in_user_mockup();
    request.setContentType(ContentTypes::IMAGE_JPEG);
    request.setContentStr("Invalid content");

    When(Method(accountManagerMockup, updateProfileImage)).Throw(CommonExceptions::IOException());

    return userId;
}


UserAccount UsersControllerFixture::given_existent_account(long userId){
    return given_existent_account(UserAccount{}.setId(userId));
}

UserAccount UsersControllerFixture::given_existent_account(UserAccount account){
    if(account.getId() == UserAccount::INVALID_ID){
        account.setId(1);
    }
    
    When(Method(accountManagerMockup, exist).Using(account.getId())).AlwaysReturn(true);
    When(Method(accountManagerMockup, getUser).Using(account.getId())).AlwaysReturn(account.getUser());
    
    return account;
}

long UsersControllerFixture::given_a_logged_in_user_mockup(const UserAccount & baseAccount)
{
    loggedInUser = baseAccount;
    
    if(loggedInUser.getId() == UserAccount::INVALID_ID){
        loggedInUser.setId(12345);
    }
    
    long userId = loggedInUser.getId();
    
    string token = "anyrandomtoken";
    ParameterizedHeader cookieHeader;
    cookieHeader.setName(Headers::Cookie);
    cookieHeader.putParameter(api::cookies::sessionCookie, token);

    request.putHeader(cookieHeader);
    When(Method(accountManagerMockup, getSessionUser).Using(token)).Return(userId);
    When(Method(accountManagerMockup, exist).Using(userId)).AlwaysReturn(true);
    When(Method(accountManagerMockup, getUser).Using(userId)).AlwaysReturn(loggedInUser.getUser());

    return userId;
}

long UsersControllerFixture::given_a_logged_in_admin_mockup(){
    UserAccount adminAccount;
    adminAccount.setIsAdmin(true);
    long userId =  given_a_logged_in_user_mockup(adminAccount);  
    
    When(Method(accountManagerMockup, isAdmin).Using(userId)).AlwaysReturn(true);
    
    return userId;  
}

void UsersControllerFixture::when_receives_update_profile_image_request(long userId){
    auto profile_image_route = route(userId, api::paths::user_profile_image);
    executePostRequest(profile_image_route);    
}

void UsersControllerFixture::when_receiving_get_image_profile(long userId){
    returnedImage = std::make_shared<Image>("imagePath.jpeg");
    
    When(Method(accountManagerMockup, getProfileImage))
        .Return(returnedImage);
        
    auto profile_image_route = route(userId, api::paths::user_profile_image);
    executeGetRequest(profile_image_route);
}

void UsersControllerFixture::check_update_profile_image_called(){
    CHECK_NOTHROW(
        Verify(Method(accountManagerMockup, updateProfileImage))
                );
}

void UsersControllerFixture::check_ok_response(){
    CAPTURE(response.statusLine().getStatusCode());
    CAPTURE(response.statusLine().getMessage());
    CAPTURE(response.getContentStr());
    
    RequestUtils::assertOkMessage(response);
}

void UsersControllerFixture::check_sent_image_file(){
    REQUIRE_FALSE(response.getSentFiles().empty());
    
    auto lastSentFile = response.getSentFiles().back();
    
    CHECK(lastSentFile == returnedImage->getImagePath());
}

void UsersControllerFixture
    ::when_doing_a_request(WebController & controller, const string & method, const string & path)
{
    request.setUrl(path);
    request.setMethod(method);
    
    controller.onRequest(request, response);    
}

void UsersControllerFixture::when_receiving_an_activate_user_request(const UserAccount & account){
    long userId = account.getId();
    auto requestRoute = route(userId, api::paths::user_active);
    auto content = GenericField<bool>(api::fields::user_active, true);
    
    When(Method(accountManagerMockup, setAccountEnabled))
        .AlwaysDo([&account](long userId, bool enabled){
            auto user = account.getUser();
            user.setActive(enabled);
            return user;
    });
    
    executePostRequest(requestRoute, content);    
}

void UsersControllerFixture::check_account_was_activated(long userId){
    CHECK_NOTHROW(
        Verify(Method(accountManagerMockup, setAccountEnabled).Using(userId, true))
                );
}
