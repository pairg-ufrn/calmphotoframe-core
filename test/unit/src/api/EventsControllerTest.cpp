#include "TestMacros.h"
#include "helpers/WebControllerFixture.h"
#include "helpers/EventsHelper.h"
#include "helpers/RequestUtils.h"
#include "helpers/UserHelper.h"

#include "mocks/data/data-memory/MemoryDataBuilder.hpp"

#include "api/Api.h"
#include "api/EventsController.h"

#include "services/EventsManager.h"

using namespace calmframe;
using namespace calmframe::data;
using namespace std;

class EventsControllerFixture : public WebControllerFixture{
public: //tests
    void testListRecentEvents();
    void testRecentEventsShouldContainsUserInfo();
public:
    EventsControllerFixture();
public:
    EventsController eventsController;
    EventsManager eventsManager;
};

#define TEST_TAG "[EventsController]"
#define FIXTURE EventsControllerFixture

/* **********************************************************************************************************/

FIXTURE_SCENARIO("EventsController list recent events"){
    this->testListRecentEvents();
}

FIXTURE_SCENARIO("events listed by EventsController should contains the UserInfo"){
    this->testRecentEventsShouldContainsUserInfo();
}//SCENARIO

/* **********************************************************************************************************/


void EventsControllerFixture::testListRecentEvents(){

GIVEN("some event instances on database"){
    EventsHelper::insertEvents(5);

GIVEN("a GET request with an integer limit parameter on query"){
    int limit = 2;

WHEN("a EventsController receives that request"){
    executeGetRequest(route(""), {{api::parameters::limit, std::to_string(limit)}});

THEN("it should respond with an OK json message"){
    RequestUtils::assertOkJsonResponse(response);

THEN("that message should represent the most recents events on database (limited by the given query parameter)"){
    auto events = deserializeResponseItems<Event>(api::fields::events);
    auto expectedEvents = Data::instance().events().listRecents(limit);

    INFO("response: " << response.getContentStr());

    CHECK(events.size() == expectedEvents.size());
    CHECK(events == expectedEvents);
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void EventsControllerFixture::testRecentEventsShouldContainsUserInfo(){

GIVEN("some events on database, associated with some users"){
    auto accounts = UserHelper::createAccounts(2);

    //initialize with events without user
    vector<Event> events = {
        Event{EventType::PhotoUpdate}
      , Event{EventType::PhotoCreation}};

    //add events with accounts
    for(auto account : accounts){
        events.push_back(Event{EventType::PhotoUpdate, account.getId(), Event::InvalidId});
        events.push_back(Event{EventType::PhotoRemove, account.getId(), Event::InvalidId});
    }

    EventsHelper::insertEvents(events);

    auto expectedEvents = eventsManager.listRecentEvents();

WHEN("an EventsController receives an request to list those events"){
    executeGetRequest(route(""));

THEN("the representation of each event that are associated with a valid user should contains that user info"){
    auto responseItems = deserializeResponseItems<ExtendedEvent>(api::fields::events);

    CHECK(responseItems.size() == expectedEvents.size());

    for(int i=0; i < responseItems.size(); ++i){
        CAPTURE(i);
        CHECK(expectedEvents[i].hasUser() == responseItems[i].hasUser());
    }

    CHECK(responseItems == expectedEvents);

}//THEN
}//WHEN
}//GIVEN

}

/* **********************************************************************************************************/

EventsControllerFixture::EventsControllerFixture(){
    setController(&eventsController);

    Data::setup(std::make_shared<MemoryDataBuilder>());
}
