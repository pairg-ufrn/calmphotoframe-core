#include "TestMacros.h"

#include "api/Api.h"
#include "api/PhotoContextController.h"
#include "api/PhotoCollectionController.h"
#include "api/PhotoController.h"
#include "services/PhotoManager.h"
#include "services/PhotoCollectionManager.h"
#include "services/PhotoContextManager.h"

#include "data/Data.h"
#include "data/DataTransaction.h"
#include "data/data-sqlite/dao/GenericDAO.hpp"

#include "network/Url.h"
#include "network/StatusResponses.h"

#include "serialization/json/JsonSerializer.h"

#include "utils/StringCast.h"
#include "utils/Log.h"

#include "utils/ScopedDatabase.h"
#include "helpers/PhotoContextHelper.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/SessionHelper.h"
#include "helpers/UserHelper.h"
#include "helpers/PermissionHelper.h"

#include "helpers/WebControllerFixture.h"
#include "helpers/RequestUtils.h"
#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"
#include "mocks/MockRequestRouter.h"

#include <vector>
#include <iterator>
#include <string>

using namespace std;
using namespace calmframe;
using namespace calmframe::network;
using namespace calmframe::data;
using namespace calmframe::api;
using namespace calmframe::utils;

namespace Constants {
    static const int CTX_NUM = 4;
    static const char * FIELD_PHOTOCONTEXTS = "contexts";

    static const char * CTX_NAME = "Context";
    static const int COLL_NUM = 3;

    static const long INEXISTENT_ID = 100000;

}//namespace

class PhotoContextControllerFixture : public WebControllerFixture{
public:
    void testRetrieveContextList();
    void testRetrievePrivateContexts();
    void testGetDefaultContext();
    void testGetPhotoContext();
    void testGetContextWithPermission();
    void testGetCollaborators();
    void testPhotoContextCreation(const Visibility & visibility=Visibility::Unknown);
    void testContextCreationAdmin();
    void testUpdateContext();
    void testRejectUpdateFromNonAdminUser();
    void testDeleteContext();
    void testRejectNonJsonRequest();
    void testNotFoundContextRequest();
    void testRedirectToCollectionController();
    void testNullRequestRouter();
    void testUpdateCollaborators();
    void testNotUpdateCollaboratorsWhenNotSpecified();
public:
    ScopedSqliteDatabase scopedDb;
    PhotoContextController ctxController;
    PhotoContextManager contextManager;
    MockRouter requestRouter;
public:
    PhotoContextControllerFixture();

    string buildGetCollectionsUrl(long ctxId);
    string buildCollectionPhotosUrl(long ctxId, long collId);
    string buildGetRootUrl(long ctxId);

    PhotoContext createPhotoContext();
    PhotoContext getSomePhotoContext();
    void checkRootCollectionCreation( const PhotoContext & responseContext);
    template<typename UsersCollection>
    vector<Permission> insertPermissions(long ctxId, const UsersCollection & users, PermissionLevel permissionLevel);
    Permission putPermission(long ctxId, long userId, PermissionLevel permissionLevel);

    void checkEquals(const ProtectedEntity<UserInfo> & lhs, const ProtectedEntity<UserInfo> & rhs);
    long setupCtxAdminRequest(long ctxId);
    void sortCollaborators(PhotoContextManager::CollaboratorsList & collabs);
};
typedef PhotoContextControllerFixture Fixture;

#define TEST_TAG "[PhotoContextController]"
#define FIXTURE PhotoContextControllerFixture

/*********************************************************************************************************************/

FIXTURE_SCENARIO("Retrieve context list"){
    this->testRetrieveContextList();
}//SCENARIO

FIXTURE_SCENARIO("List private contexts"){
    this->testRetrievePrivateContexts();
}//SCENARIO

FIXTURE_SCENARIO("Get default context"){
    this->testGetDefaultContext();
}//SCENARIO

FIXTURE_SCENARIO("PhotoContextController should allow get some PhotoContext"){
    this->testGetPhotoContext();
}//SCENARIO

FIXTURE_SCENARIO("Api allows get a PhotoContext together with the requester permission"){
    this->testGetContextWithPermission();
}//SCENARIO

FIXTURE_SCENARIO("PhotoContext creation with name"){
    this->testPhotoContextCreation();
}//SCENARIO

FIXTURE_SCENARIO("PhotoContext creation with name and visibility"){
    this->testPhotoContextCreation(Visibility::Private);
}//SCENARIO

FIXTURE_SCENARIO("ContextController should put the context creator as admin"){
    this->testContextCreationAdmin();
}//SCENARIO

FIXTURE_SCENARIO("PhotoContextController rejects create request that is not json"){
    this->testRejectNonJsonRequest();
}//SCENARIO

FIXTURE_SCENARIO("Update PhotoContext request"){
    this->testUpdateContext();
}//SCENARIO

FIXTURE_SCENARIO("PhotoContext updates should only be allowed to an admin user"){
    this->testRejectUpdateFromNonAdminUser();
}//SCENARIO

FIXTURE_SCENARIO("ContextController should reject updates to unexistent contexts"){
    this->testNotFoundContextRequest();
}//SCENARIO

FIXTURE_SCENARIO("Redirect request to PhotoCollectionController"){
    this->testRedirectToCollectionController();
}//SCENARIO

FIXTURE_SCENARIO("Context Controller should not handle a forward request if RequestRouter is NULL"){
    this->testNullRequestRouter();
}//SCENARIO

FIXTURE_SCENARIO("Delete PhotoContext request"){
    this->testDeleteContext();
}//SCENARIO

FIXTURE_SCENARIO("Api: Get PhotoContext collaborators"){
    this->testGetCollaborators();
}//SCENARIO

FIXTURE_SCENARIO("Api: Update PhotoContext collaborators"){
    this->testUpdateCollaborators();
}//SCENARIO

FIXTURE_SCENARIO("Api: should not change collaborators when not set on request"){
    this->testNotUpdateCollaboratorsWhenNotSpecified();
}//SCENARIO

/*********************************************************************************************************************/

void PhotoContextControllerFixture::testRetrieveContextList(){
GIVEN("some PhotoContexts on database with different visibility levels"){
    vector<PhotoContext> expectedCtxs = {
        {"", Visibility::EditableByOthers}, {"", Visibility::VisibleByOthers}, {"", Visibility::VisibleByOthers}
    };
    PhotoContextHelper::insertContexts(expectedCtxs);
    //others
    PhotoContextHelper::insertContexts({
        {"", Visibility::Private}, {"", Visibility::Private}
    });

WHEN("a get request is made to the root path of a PhotoContextController"){
    executeGetRequest("/");

THEN("it should respond with an json representation of all visible contexts"){
    RequestUtils::assertOkJsonResponse(response);

    vector<PhotoContext> responseItems = deserializeResponseItems<PhotoContext>(Constants::FIELD_PHOTOCONTEXTS);

    INFO(response.getContentStr());
    CHECK(responseItems.size() == expectedCtxs.size());
    PhotoContextHelper::checkEquals(expectedCtxs, responseItems, true);
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testRetrievePrivateContexts(){
GIVEN("some private PhotoContext that some user has a visibility permission"){
    long userId = SessionHelper::setupSessionToken(request);

    vector<PhotoContext> expectedContexts = PhotoContextHelper::insertContexts({
            {"ctx", Visibility::Private}, {"", Visibility::EditableByOthers}, {"", Visibility::VisibleByOthers}});
    //non visible contexts
    PhotoContextHelper::insertContexts({{"", Visibility::Private}});

    long privateCtxId = expectedContexts[0].getId();
    Data::instance().permissions().putPermission(Permission{privateCtxId, userId, PermissionLevel::Read});

WHEN("that user makes a request to list contexts"){
    executeGetRequest("/");

THEN("that context should be listed on response"){
    auto responseCtxs = deserializeResponseItems<PhotoContext>(api::fields::contexts);
    PhotoContextHelper::checkEquals(responseCtxs, expectedContexts);
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testGetDefaultContext(){

GIVEN("some PhotoContexts on database"){
    createPhotoContext();
    PhotoContext firstContext = createPhotoContext()
                ,otherContext = createPhotoContext();

GIVEN("a user that can read some of that contexts"){
    long userId = SessionHelper::putUserWithPermission(request, firstContext.getId(), PermissionLevel::Read);
    Data::instance().permissions().putPermission(Permission{otherContext.getId(), userId, PermissionLevel::Write});

WHEN("a GET request is made to the 'default context' route"){
    executeGetRequest("/default");

THEN("the PhotoContext with smaller id that is visible to the user should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);
    CHECK(firstContext == getResponse<PhotoContext>());
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testGetPhotoContext(){

GIVEN("some existing PhotoContext on database"){
    PhotoContext someContext = createPhotoContext();
    PhotoContext otherContext = createPhotoContext();

GIVEN("some user that can read that context"){
    SessionHelper::putUserWithPermission(request, someContext.getId(), PermissionLevel::Read);

WHEN("a GET request is made to that context route"){
    executeGetRequest(Url().path().add(someContext.getId()).toString());

THEN("that PhotoContext should be sent as response"){
    CHECK(someContext == getResponse<PhotoContext>());

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testGetContextWithPermission(){

GIVEN("some context on db"){
    PhotoContext expectedContext = PhotoContextHelper::insertContext();
    long ctxId = expectedContext.getId();

GIVEN("some user with a permission level on that context"){
    PermissionLevel userPermissionLevel = PermissionLevel::Write;
    long userId = SessionHelper::putUserWithPermission(request, ctxId, userPermissionLevel);

WHEN("that user request the context with a parameter to include his permissions"){
    executeGetRequest(route(ctxId), {{api::parameters::include_permission, "true"}});

THEN("a json representation of the  context should be returned as response"){
    RequestUtils::assertOkJsonResponse(response);

    ProtectedEntity<PhotoContext> ctxWithPermission = getResponse<ProtectedEntity<PhotoContext>>();

    PhotoContextHelper::checkEquals(expectedContext, ctxWithPermission.getEntity());

THEN("it should contains a field with the permission of the user on this context"){
    CHECK(userId == ctxWithPermission.getUserId());
    CHECK(userPermissionLevel == ctxWithPermission.getPermissionLevel());

}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test


void PhotoContextControllerFixture::testGetCollaborators(){

GIVEN("some photo context on database"){
    PhotoContext ctx = createPhotoContext();
    long ctxId = ctx.getId();

GIVEN("some users with permissions on that context"){
    PermissionLevel permissionLevel = PermissionLevel::Read;
    auto users = std::vector<User>{UserHelper::createAccount("user1").getUser(), UserHelper::createAccount("user2").getUser()};
    insertPermissions(ctxId, users, permissionLevel);

    long sessionUserId = SessionHelper::putUserWithPermission(request, ctxId, permissionLevel);
    users.push_back(UserHelper::getUser(sessionUserId));

WHEN("some of those users make a GET request is to the collaborators path of that context"){

    executeGetRequest(route(ctxId, api::paths::collaborators));

THEN("it should return a json instance with an attribute containing the users associated with their permissions"){
    auto responseItems = this->deserializeResponseItems<ProtectedEntity<UserInfo>>(api::fields::collaborators);

    CHECK(responseItems.size() == users.size());
    for(const ProtectedEntity<UserInfo> & collab : responseItems){
        CHECK(collab.getUserId() == collab.getEntity().getUserId());
        CHECK(collab.getPermissionLevel() == PermissionLevel::Read);
    }

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testPhotoContextCreation(const Visibility & visibility){
WHEN("a POST request is made to the context '/' route"){
AND_WHEN("the request has an json content with a photo context name property"){
    PhotoContext requestContext(Constants::CTX_NAME, visibility);

    executePostRequest("/", &requestContext);

THEN("a new PhotoContext should be created with that name and sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    PhotoContext responseContext = getResponse<PhotoContext>();
    CHECK(Data::instance().contexts().exist(responseContext.getId()));
    CHECK(responseContext.getName() == requestContext.getName());

THEN("the created PhotoContext should have a new PhotoCollection as root"){
    checkRootCollectionCreation(responseContext);

if(visibility == Visibility::Unknown){
    AND_WHEN("the request content has no visibility property"){
    THEN("the context should have a public visibility"){
        CHECK(responseContext.getVisibility() == Visibility::EditableByOthers);
    }//THEN
    }//WHEN
}
else{
    AND_WHEN("the request content has a visibility property"){
    THEN("the context visibility should be set to that value"){
        CHECK(responseContext.getVisibility() == visibility);
    }//THEN
    }//WHEN
}

}//THEN
}//THEN
}//WHEN
}//WHEN

}

void PhotoContextControllerFixture::testContextCreationAdmin(){

GIVEN("a valid session token"){
    long userId = SessionHelper::setupSessionToken(request);

WHEN("a create context request is made with that token"){
    PhotoContext requestCtx{"some user", Visibility::VisibleByOthers};
    executePostRequest("/", &requestCtx);

THEN("a new photoContext should be sent as response"){
    PhotoContext ctxResponse = getResponse<PhotoContext>();
    CHECK(ctxResponse.getName()         == requestCtx.getName());
    CHECK(ctxResponse.getVisibility()   == requestCtx.getVisibility());

THEN("an admin permission should be put to the user that created the context"){
    Permission permission;
    CHECK(Data::instance().permissions().tryGetPermission(userId, ctxResponse.getId(), permission));
    CHECK(permission.getPermissionLevel() == PermissionLevel::Admin);
}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testRejectNonJsonRequest(){

WHEN("a POST request is made to the context '/' route"){
AND_WHEN("the request has an non json content"){
    request.setContentStr("non json content");
    request.setContentType(ContentTypes::TEXT_PLAIN);

    executePostRequest("/");

THEN("a 'bad request' response should be sent"){
    RequestUtils::assertMessageStatus(response, StatusResponses::BAD_REQUEST.getStatusCode());

}//THEN
}//WHEN
}//WHEN

}

void PhotoContextControllerFixture::testUpdateContext(){

GIVEN("some existing PhotoContext"){
    PhotoContext existingContext = createPhotoContext();

WHEN("a POST request is made to ContextController using that context route '/<ctxId>'"){
    const string route = Url().path().add(existingContext.getId()).toString();

AND_WHEN("the request has a session token associated with an admin of that context"){
    setupCtxAdminRequest(existingContext.getId());

AND_WHEN("that request contains a json representation of some PhotoContext"){
    PhotoContext requestContext{"Other Name", Visibility::EditableByOthers};
    executePostRequest(route, &requestContext);

THEN("that PhotoContext should be updated and sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    PhotoContext responseCtx = getResponse<PhotoContext>();

    CHECK(requestContext.getName()       == responseCtx.getName());
    CHECK(requestContext.getVisibility() == responseCtx.getVisibility());

THEN("the PhotoContext id and rootContext should not change"){
    CHECK(responseCtx.getId()             == existingContext.getId());
    CHECK(responseCtx.getRootCollection() == existingContext.getRootCollection());

}//THEN
}//THEN
}//WHEN
}//WHEN
}//WHEN
}//GIVEN

}

void PhotoContextControllerFixture::testRejectUpdateFromNonAdminUser(){

GIVEN("some update request to an existent context"){
    PhotoContext ctx = PhotoContextHelper::insertContext();
    string reqRoute = this->route(ctx.getId());

    PhotoContext updateCtx = ctx;
    updateCtx.setName("new name");
    updateCtx.setVisibility(Visibility::Private);

WHEN("the associated client does not have admin privileges on that context"){
    SessionHelper::setupSessionToken(request);
    executePostRequest(reqRoute, &updateCtx);

THEN("the response should represent a forbidden error"){
    RequestUtils::assertMessageStatus(response, StatusResponses::FORBIDDEN.getStatusCode());

THEN("the context should not be updated"){
    PhotoContext currentCtx = Data::instance().contexts().get(ctx.getId());
    CHECK(currentCtx == ctx);
    CHECK(!(currentCtx == updateCtx));
}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testDeleteContext(){
GIVEN("some existing PhotoContext"){
    PhotoContext context = createPhotoContext();
    long ctxId = context.getId();

GIVEN("a user with admin privileges on that context"){
    SessionHelper::putUserWithPermission(request, ctxId, PermissionLevel::Admin);

WHEN("a DELETE request is made to that PhotoContext route /<ctxId> on a PhotoContextController"){
    executeRequest("DELETE", Url().path().add(ctxId).toString());

THEN("the controller should delete that context and sent it as response"){
    RequestUtils::assertOkJsonResponse(response);

    PhotoContext response = getResponse<PhotoContext>();
    CHECK(response == context);
    CHECK(!Data::instance().contexts().exist(ctxId));
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testNotFoundContextRequest(){
WHEN("a POST request is made to some ContextController using a context route '/<ctxId>' with an unexistent id"){
    string route = Url().path().add(Constants::INEXISTENT_ID).toString();

    PhotoContext requestContext = getSomePhotoContext();
    executePostRequest(route, &requestContext);

THEN("the controller should respond with a not found message"){
    RequestUtils::assertMessageStatus(response, StatusResponses::NOT_FOUND.getStatusCode());

}//THEN
}//WHEN

}

void PhotoContextControllerFixture::testRedirectToCollectionController(){

GIVEN("a PhotoContextController with a non-null RequestRouter"){
    ctxController.setRouter(&requestRouter);

GIVEN("some PhotoContext"){
    PhotoContext ctx = createPhotoContext();

WHEN("a request is made to a PhotoContextController using the 'root collection route' of that context"){
    auto routePath = route(ctx.getId(), "collections", "root");
    executeGetRequest(routePath);

THEN("it should forward the request to a PhotoCollectionController path"){
    Url rootCollectionsUrl{routePath};
    rootCollectionsUrl.path().remove(0);

    CHECK(requestRouter.forwardedUrl.toString() == rootCollectionsUrl.toString());

}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}

void PhotoContextControllerFixture::testNullRequestRouter(){
GIVEN("a PhotoContextController with a null RequestRouter"){
    ctxController.setRouter(NULL);

WHEN("a request is made to that controller using the 'root collection' route of some context"){
    PhotoContext ctx = createPhotoContext();
    request.setUrl(route(ctx.getId(), "collections", "root"));

THEN("it should not handle the request"){
    CHECK(ctxController.onRequest(request, response) == false);

}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoContextControllerFixture::sortCollaborators(PhotoContextManager::CollaboratorsList & collabs)
{
    std::sort(collabs.begin(), collabs.end(), [](const PhotoContextManager::Collaborator & lhs, const PhotoContextManager::Collaborator & rhs){
        return lhs.getUserId() < rhs.getUserId();
    });
}

void PhotoContextControllerFixture::testUpdateCollaborators(){

GIVEN("some photocontext instance"){
    PhotoContext ctx = createPhotoContext();

    auto accounts = UserHelper::createAccounts(3);
    insertPermissions(ctx.getId(), accounts, PermissionLevel::Read);

WHEN("an update request is received to that context"){
    long adminId = setupCtxAdminRequest(ctx.getId());
    User admin;
    Data::instance().users().getUser(adminId, admin);

WHEN("the request has a field with a list of collaborators"){
    PhotoContextManager::CollaboratorsList expectCollabs;
    expectCollabs.push_back(PhotoContextManager::Collaborator{
                            accounts[1].getId(), PermissionLevel::Write, UserInfo{accounts[1].getId(), accounts[1].getLogin()}});
    expectCollabs.push_back(PhotoContextManager::Collaborator{
        Data::instance().permissions().getPermission(adminId, ctx.getId()), UserInfo{adminId, admin.getLogin()}
    });

    serializer.clear();
    serializer.putCollection(api::fields::collaborators, expectCollabs.begin(), expectCollabs.begin() + 1);
    putSerializerContentIntoRequest();

    INFO(request.getContentStr());
    executePostRequest(route(ctx.getId()));

THEN("the collaborators of that context should be replaced by the list content"){
    RequestUtils::assertOkJsonResponse(response);

    auto collabs = contextManager.listCollaborators(ctx.getId());

    sortCollaborators(collabs);
    sortCollaborators(expectCollabs);

    REQUIRE(collabs.size() == expectCollabs.size());
    for(int i=0; i < collabs.size(); ++i){
        checkEquals(collabs[i], expectCollabs[i]);
    }
THEN("the permission of the admin that updated the context should not change"){
    CHECK(Data::instance().permissions().hasPermission(adminId, ctx.getId(), PermissionLevel::Admin));
}//THEN
}//THEN
}//WHEN
}//WHEN
}//GIVEN

}//test

void PhotoContextControllerFixture::testNotUpdateCollaboratorsWhenNotSpecified(){

GIVEN("some photocontext with some permissions"){
    PhotoContext ctx = createPhotoContext();
    insertPermissions(ctx.getId(), UserHelper::createAccounts(2), PermissionLevel::Write);

GIVEN("an update request to that context"){
    setupCtxAdminRequest(ctx.getId());
    PhotoContext ctxToUpdate = ctx;
    ctxToUpdate.setName("other ctx");
    auto permissions = Data::instance().permissions().listByContext(ctx.getId());

WHEN("the request does not have an collaborator field"){
    executePostRequest(route(ctx.getId()), &ctxToUpdate);

THEN("it should not change collaborators of context"){
    auto currentPermissions = Data::instance().permissions().listByContext(ctx.getId());

    std::sort(permissions.begin(), permissions.end());
    std::sort(currentPermissions.begin(), currentPermissions.end());
    REQUIRE(currentPermissions.size() == permissions.size());
    for(int i=0; i < permissions.size(); ++i){
        CAPTURE(i);
        PermissionHelper::checkEquals(currentPermissions[i], permissions[i]);
    }
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//test

/*********************************************************************************************************************/

PhotoContextControllerFixture::PhotoContextControllerFixture()
    : scopedDb(ScopedSqliteDatabase::DbType::InMemory)
{
    ctxController.setContextManager(&contextManager);

    setController(&ctxController);

    //Force db creation
    Data::instance();
}

string PhotoContextControllerFixture::buildGetCollectionsUrl(long ctxId){
    return route(ctxId, "collections");
}

string PhotoContextControllerFixture::buildCollectionPhotosUrl(long ctxId, long collId){
    return route(ctxId, "collections", collId, "photos");
}

string PhotoContextControllerFixture::buildGetRootUrl(long ctxId){
    return buildGetCollectionsUrl(ctxId).append("/root");
}

PhotoContext PhotoContextControllerFixture::createPhotoContext(){
    long collId = PhotoCollectionHelper::insertCollection();
    PhotoCollection rootColl = Data::instance().collections().get(collId);
    PhotoContext ctx;
    ctx.setName(Constants::CTX_NAME);
    ctx.setRootCollection(rootColl.getId());
    PhotoContextHelper::insertContext(ctx);
    return Data::instance().contexts().get(ctx.getId());
}

PhotoContext PhotoContextControllerFixture::getSomePhotoContext(){
    PhotoContext randomCtx(string("other ").append(Constants::CTX_NAME));
    randomCtx.setId(Constants::INEXISTENT_ID);
    randomCtx.setRootCollection(Constants::INEXISTENT_ID);

    return randomCtx;
}

void PhotoContextControllerFixture::checkRootCollectionCreation(const PhotoContext & responseContext){
    PhotoCollection rootCollection = Data::instance().collections().get(responseContext.getRootCollection());
    CHECK(rootCollection.getId() != PhotoCollection::INVALID_ID);
    CHECK(rootCollection.getParentContext() == responseContext.getId());
}

template<typename UsersCollection>
vector<Permission> PhotoContextControllerFixture::insertPermissions(long ctxId, const UsersCollection & users, PermissionLevel permissionLevel)
{
    vector<Permission> permissions;

    DataTransaction transaction(Data::instance());
    for(auto user : users){
        permissions.push_back(putPermission(ctxId, user.getId(), permissionLevel));
    }
    transaction.commit();

    return permissions;
}

Permission PhotoContextControllerFixture::putPermission(long ctxId, long userId, PermissionLevel permissionLevel)
{
    Permission p{ctxId, userId, permissionLevel};
    Data::instance().permissions().putPermission(p);

    return p;
}

void PhotoContextControllerFixture::checkEquals(const ProtectedEntity<UserInfo> & lhs, const ProtectedEntity<UserInfo> & rhs){
    CHECK(lhs.getUserId() == rhs.getUserId());
    CHECK(lhs.getPermissionLevel() == rhs.getPermissionLevel());
    CHECK(lhs.getEntity() == rhs.getEntity());
}

long PhotoContextControllerFixture::setupCtxAdminRequest(long ctxId)
{
    long userId = SessionHelper::setupSessionToken(request);
    Data::instance().permissions().putPermission(Permission{ctxId, userId, PermissionLevel::Admin});

    return userId;
}

