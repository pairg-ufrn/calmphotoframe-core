#include "TestMacros.h"

#include "api/Api.h"
#include "api/PhotoFrameApi.h"

#include "data/Data.h"

#include "network/StatusResponses.h"
#include "network/Url.h"
#include "network/Cookie.h"

#include "services/PhotoManagersLayer.h"
#include "services/SessionManager.h"
#include "services/AccountManager.h"
#include "model/user/UserLoginInfo.h"

#include "utils/StringCast.h"
#include "utils/ScopedDatabase.h"

#include "helpers/WebControllerFixture.h"
#include "helpers/RequestUtils.h"
#include "helpers/SessionHelper.h"
#include "helpers/PhotoCollectionHelper.h"
#include "helpers/ScopedChangeLogLevel.h"

#include <fstream>
#include <memory>

using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::network;
using namespace calmframe::utils;

namespace Constants {
    const char * const IMAGE_FILE = "test/data/test-images/test1.jpg";
    const char * const Username = "sampleUserName";
    const char * const Password = "samplePassword";
}//namespace

class PhotoFrameApiFixture : public WebControllerFixture{
public: //tests
    void testRouteRequireAuthorization(const std::string & method, const std::string & route);
    void testGetRootCollectionFromApi();
    void testGetCollectionsFromDefaultContext();
    void testGetPhotosFromRootCollectionOfDefaultContext();
    void testCreatePhotoOnContext();
    void testGetDefaultContextUsingApi();
    void testUsersRoute();
    void testAccessSignupRoute();
    void testGetRootRoute();
  
public:
    void log_only_errors();
      
public:
    PhotoFrameApiFixture();

    PhotoContext getDefaultContext();
    long getSomeCtxId();

    void fillDatabase();

    PhotoContext putContext();
    long makeLogin();
    /** Setup request with a logged user that has the requiredPermission on given context.*/
    long makeLogin(long ctxId, PermissionLevel requiredPermission);
    void makeSignupRequest();
    
public:
    ScopedSqliteDatabase scopedDatabase{ScopedSqliteDatabase::DbType::InMemory};
    PhotoFrameApi photoFrameApi;
    PhotoManagersLayer photoManagersLayer;
    AccountManager accountManager;
    
    std::shared_ptr<ScopedChangeLogLevel> scopedLogLevel;
};

typedef PhotoFrameApiFixture Fixture;

#define FIXTURE PhotoFrameApiFixture
#define TEST_TAG "[PhotoFrameApi]"

FIXTURE_SCENARIO("Api should reject non-authenticated requests to secured resources"){
    this->testRouteRequireAuthorization("GET", route(api::paths::contexts));
}//SCENARIO


FIXTURE_SCENARIO("Events route require authorization"){
    this->testRouteRequireAuthorization("GET", route(api::paths::events));
}//SCENARIO

TEST_SCENARIO("Get context root collection from PhotoFrameApi") IS_DEFINED_BY(testGetRootCollectionFromApi)

TEST_SCENARIO("Get default context collections") IS_DEFINED_BY(testGetCollectionsFromDefaultContext)

TEST_SCENARIO("Get photos from root collection of default context") 
    IS_DEFINED_BY(testGetPhotosFromRootCollectionOfDefaultContext)

TEST_SCENARIO("Get default context from PhotoFrameApi") IS_DEFINED_BY(testGetDefaultContextUsingApi)

TEST_SCENARIO("Create photo on a given context through the Api") IS_DEFINED_BY(testCreatePhotoOnContext)

TEST_SCENARIO("Check users route") IS_DEFINED_BY(testUsersRoute)
TEST_SCENARIO("Signup route should be accessible by non authenticated users") IS_DEFINED_BY(testAccessSignupRoute)
TEST_SCENARIO("GET root route should be acessible by non authenticated users") IS_DEFINED_BY(testGetRootRoute)

/* **********************************************************************************************************/

void PhotoFrameApiFixture::testRouteRequireAuthorization(const string & method, const string & route){
    WHEN("making a " + method + " request to route '" + route + "'"){
        executeRequest(method, route);

    THEN("the Api should respond with a Unauthorized status code"){
        CHECK(response.statusLine().getStatusCode() == StatusResponses::UNAUTHORIZED.getStatusCode());

    }//THEN
    }//WHEN
}

void PhotoFrameApiFixture::testGetRootCollectionFromApi(){

GIVEN("an authorized user"){
    PhotoContext context = putContext();
    long ctxId = context.getId();
    makeLogin(ctxId, PermissionLevel::Read);

WHEN("making a request to GET the root collection of some existing PhotoContext"){
    executeGetRequest(route("contexts", ctxId, "collections", "root"));

THEN("that collection should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    INFO("Response:" << response.getContentStr());
    PhotoCollection rootCollection = getResponse<PhotoCollection>();
    CHECK(rootCollection.getId() == context.getRootCollection());
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoFrameApiFixture::testGetCollectionsFromDefaultContext(){
GIVEN("some collections on default PhotoContext"){
    PhotoContext defaultCtx = getDefaultContext();
    std::set<long> collIds = PhotoCollectionHelper::insertCollections(3,-1, defaultCtx.getId());

    long otherCtxId = putContext().getId();
    PhotoCollectionHelper::insertCollections(2, -1, otherCtxId);

    makeLogin(defaultCtx.getId(), PermissionLevel::Read);

WHEN("making an request to  GET the collections of the default PhotoContext"){
    executeGetRequest(route(api::paths::contexts, api::paths::defaultContext, api::paths::collections));


THEN("the defaultContext id should be put as parameter on request"){
    long defaultCtxId = utils::StrCast().toLong(request.getParameter(api::parameters::PhotoContextId));
    CHECK(defaultCtxId == defaultCtx.getId());

THEN("those collections should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    vector<PhotoCollection> colls = deserializeResponseItems<PhotoCollection>(api::fields::collections);
    REQUIRE(colls.size() == collIds.size() + 1); //Adiciona 1 p/ a raiz das coleções

    for(size_t i =0; i < colls.size(); ++i){
        PhotoCollection & coll = colls[i];
        if(coll.getId() != defaultCtx.getRootCollection()){
            CHECK(collIds.find(coll.getId()) != collIds.end());
        }
    }

}//THEN
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoFrameApiFixture::testGetPhotosFromRootCollectionOfDefaultContext(){
GIVEN("that the default context has some photos on root collection"){
    fillDatabase();
    makeLogin(getDefaultContext().getId(), PermissionLevel::Read);

WHEN("a GET request is made to '/contexts/default/collections/root/photos'"){
    executeGetRequest("/contexts/default/collections/root/photos");

THEN("those photos should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    vector<Photo> photos = deserializeResponseItems<Photo>("photos");

    PhotoContext defaultCtx = photoManagersLayer.getContextManager().getDefaultContext();
    PhotoCollection rootCollection =
            photoManagersLayer.getCollectionsManager().getCollection(defaultCtx.getRootCollection());

    REQUIRE(rootCollection.getPhotos().size() > 0);
    CHECK(photos.size() == rootCollection.getPhotos().size());
    for(size_t i=0; i < photos.size(); ++i){
        CHECK(rootCollection.containsPhoto(photos[i].getId()));
    }
}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoFrameApiFixture::testCreatePhotoOnContext(){

GIVEN("some context id"){
    makeLogin();
    long ctxId = getSomeCtxId();

WHEN("a POST request is made to the photos subpath of that context route"){
    const string reqRoute = route(api::paths::contexts, ctxId, api::paths::photos);
    ifstream imgFile(Constants::IMAGE_FILE);
    REQUIRE(imgFile.good());

    putRequestContent(imgFile, ContentTypes::IMAGE_JPEG);
    executePostRequest(reqRoute);

THEN("a photo should be created on that context root collection"){
    RequestUtils::assertOkJsonResponse(response);

    Photo photo = getResponse<Photo>();
    CHECK(Data::instance().photos().exist(photo.getId()));
    this->photoManagersLayer.getPhotoManager().deletePhoto(photo.getId());

}//THEN
}//WHEN
}//GIVEN

}//test

void PhotoFrameApiFixture::testGetDefaultContextUsingApi(){
GIVEN("a logged user with read access to default context"){
    long userId = makeLogin();
    PhotoContext defaultCtx = photoManagersLayer.getContextManager().getDefaultContext();
    Data::instance().permissions().putPermission(Permission(defaultCtx.getId(), userId, PermissionLevel::Read));

WHEN("a GET request is made to default context route"){
    executeGetRequest(route("contexts","default"));

THEN("the default PhotoContext should be sent as response"){
    RequestUtils::assertOkJsonResponse(response);

    PhotoContext responseCtx =  getResponse<PhotoContext>();


    INFO("Response: " << response.getContentStr());
    CHECK(responseCtx == defaultCtx);
}//THEN
}//WHEN
}//GIVEN

}

void PhotoFrameApiFixture::testUsersRoute(){
    makeLogin();

WHEN("a GET request is made to the users route"){
    executeGetRequest(api::entries::users);

THEN("it should respond with a valid json response"){
    RequestUtils::assertOkJsonResponse(response);

}//THEN
}//WHEN

}

void PhotoFrameApiFixture::testAccessSignupRoute(){
    WHEN("a valid create user request is made by a non authenticated client"){
        makeSignupRequest();
    
    THEN("it should not be rejected by security policies"){
        RequestUtils::assertOkJsonResponse(response);
    
    }//THEN
    }//WHEN
}//test

void PhotoFrameApiFixture::testGetRootRoute(){
    WHEN("a get request is made to root api route"){
        executeGetRequest("/");
        
    THEN("it should not be rejected by security policies"){
        RequestUtils::assertOkJsonResponse(response);
    }//THEN
    }//WHEN
    
}//test

/* **********************************************************************************************************/

void PhotoFrameApiFixture::log_only_errors(){
    this->scopedLogLevel = std::make_shared<ScopedChangeLogLevel>(LogLevel::Error);
}

/* **********************************************************************************************************/

PhotoFrameApiFixture::PhotoFrameApiFixture(){
    //build database
//    Data::instance();
    log_only_errors();

    photoFrameApi.initialize(&photoManagersLayer, &accountManager);
    setController(photoFrameApi.getApiRoot());

    photoManagersLayer.initialize();
}

PhotoContext PhotoFrameApiFixture::getDefaultContext(){
    return photoManagersLayer.getContextManager().getDefaultContext();
}

long PhotoFrameApiFixture::getSomeCtxId(){
    return getDefaultContext().getId();
}

void PhotoFrameApiFixture::fillDatabase(){
    PhotoCollectionHelper::insertPhotosAtCollection(getDefaultContext().getRootCollection(), 3);
}

PhotoContext PhotoFrameApiFixture::putContext(){
    return photoManagersLayer.getContextManager().createContext(PhotoContext());
}

long PhotoFrameApiFixture::makeLogin(){
    return SessionHelper::setupSessionToken(request);
}

long PhotoFrameApiFixture::makeLogin(long ctxId, PermissionLevel requiredPermission){
    return SessionHelper::putUserWithPermission(request,ctxId, requiredPermission);
}

void PhotoFrameApiFixture::makeSignupRequest(){
    UserLoginInfo loginInfo(Constants::Username, Constants::Password);

    executePostRequest(api::entries::users, &loginInfo);
}
