#include "TestMacros.h"

#include "helpers/WebControllerFixture.h"
#include "helpers/RequestUtils.h"
#include "steps/DatabaseMockupSteps.h"

#include "api/Api.h"
#include "api/controllers/ApiController.h"
#include "model/ServerInfo.h"

#include "data/Data.h"
#include "model/user/UserAccount.h"

#include "serialization/json/JsonSerializer.h"
#include "serialization/GenericField.h"

#include "Environment.h"
#include "Options.h"

#include "utils/ScopedDatabase.h"

#define TEST_TAG "[ApiController]"
#define FIXTURE ApiControllerFixture

using namespace calmframe;
using namespace calmframe::data;
using namespace calmframe::network;

class ApiControllerFixture : public WebControllerFixture, public DatabaseMockupSteps{
public:
    void testBuildServerInfo();
    void testGetServerInfo();
    void testTryGetSubpath();
    void testUpdateServerName();
    
    void testServerInfoSerialization();
    void testServerInfoDeserialization();
public:
    void given_a_saved_server_name();
    void given_logged_in_admin();
    void when_a_update_server_name_request_is_done();
    void then_server_name_should_be_updated();
    
public:
    ApiControllerFixture(){
        setController(&apiController);
    }

    ScopedSqliteDatabase scopedDb{ScopedSqliteDatabase::DbType::InMemory};
    
    ApiController apiController;
    
    std::string currentServerName;
    
    static constexpr const char * const EXPECTED_SERVER_NAME = "new server name";
    ServerInfo given_a_server_info();
    void check_response_is_server_info(const string & expectedServerName);
};


TEST_SCENARIO("Build server info")                        IS_DEFINED_BY(testBuildServerInfo)
TEST_SCENARIO("Get server info")                          IS_DEFINED_BY(testGetServerInfo)
TEST_SCENARIO("Update server name")                       IS_DEFINED_BY(testUpdateServerName)
TEST_SCENARIO("ApiController should not handle subpaths") IS_DEFINED_BY(testTryGetSubpath)

TEST_SCENARIO("ServerInfo serialization")   IS_DEFINED_BY(testServerInfoSerialization)
TEST_SCENARIO("ServerInfo deserialization") IS_DEFINED_BY(testServerInfoDeserialization)


void ApiControllerFixture::testBuildServerInfo(){
    GIVEN("there is a saved server name"){
        given_a_saved_server_name();
        
    WHEN("building a server info instance"){
        ServerInfo serverInfo = apiController.buildServerInfo();
        
    THEN("it should contain the server name"){
        CHECK(serverInfo.getServerName() == currentServerName);
    
    THEN("it should contain the application versions"){
        auto appVersion = Environment::instance().getAppVersion();
        
        CHECK(serverInfo.getAppVersion()[0] == appVersion[0]);
        CHECK(serverInfo.getAppVersion()[1] == appVersion[1]);
        CHECK(serverInfo.getAppVersion()[2] == appVersion[2]);
    }//THEN
    }//THEN
    }//WHEN
    }
}

void ApiControllerFixture::testGetServerInfo(){
    GIVEN("there is a saved server name"){
        given_a_saved_server_name();
    
    WHEN("a get request is received by the controller"){
        executeGetRequest("/");
    
    THEN("it should respond with an ServerInfo instance"){
        RequestUtils::assertOkJsonResponse(response);
        
        check_response_is_server_info(currentServerName);
    }//THEN
    }//WHEN
    }//GIVEN
}

void ApiControllerFixture::testTryGetSubpath(){
    WHEN("an ApiController receives a (GET) request to a subpath"){
    THEN("it should not handle it"){
        executeRequest("GET", route("subpath"), false);
        
    }//THEN
    }//GIVEN
}

void ApiControllerFixture::testUpdateServerName(){
    using_db_mockups();
    mockup_config_methods();
    
    
    fakeit::When(Method(databuilderMockup.configs(), get).Using(Options::SERVER_NAME))
        .AlwaysDo(captureGetConfig.returnValue(EXPECTED_SERVER_NAME));
    
    GIVEN("an admin user is logged in"){
        given_logged_in_admin();
        
    WHEN("it makes a POST request to the api root route"){
    AND_WHEN("the request contains a new server name"){
        when_a_update_server_name_request_is_done();
    
    THEN("the server name should be changed"){
       then_server_name_should_be_updated();
       
    AND_THEN("it should respond with the updated server info instance"){
        check_response_is_server_info(EXPECTED_SERVER_NAME);
    }
    }//THEN
    }//WHEN 
    }//WHEN
    }//GIVEN
}

void ApiControllerFixture::testServerInfoSerialization(){
    GIVEN("a server info instance"){
        ServerInfo serverInfo = given_a_server_info();
        
    WHEN("serializing it"){
        JsonSerializer serializer;
        serializer << serverInfo;

        Json::Value value;
        serializer >> value;
        
    THEN("it should produce version and versionNumbers fields"){
        CHECK(value[ServerInfo::Fields::versionNumbers][0] == 1);
        CHECK(value[ServerInfo::Fields::versionNumbers][1] == 2);
        CHECK(value[ServerInfo::Fields::versionNumbers][2] == 3);
        CHECK(value[ServerInfo::Fields::version] == "1.2.3");
    
    AND_THEN("it should produce the server name field"){
        CHECK(value[ServerInfo::Fields::serverName] == serverInfo.getServerName());
        
    }//THEN
    }//THEN
    }//WHEN
    }//GIVEN
}

void ApiControllerFixture::testServerInfoDeserialization(){
    GIVEN("a server info representation"){
        ServerInfo serverInfo = given_a_server_info();
        
        JsonSerializer serializer;
        serializer << serverInfo;
        stringstream content;
        content << serializer;
    
        serializer.clear();
        content >> serializer;
    
        CAPTURE(content.str());
        
    WHEN("deserializing it"){
        ServerInfo deserialized;
        serializer >> deserialized;
    
    THEN("it should produce an equivalent instance"){
        CHECK(deserialized.getServerName() == serverInfo.getServerName());
        CHECK(deserialized.getVersionString() == serverInfo.getVersionString());
        
        CHECK(deserialized.getAppVersion()[0] == serverInfo.getAppVersion()[0]);
        CHECK(deserialized.getAppVersion()[1] == serverInfo.getAppVersion()[1]);
        CHECK(deserialized.getAppVersion()[2] == serverInfo.getAppVersion()[2]);
        
    }//THEN
    }//WHEN
    }//GIVEN
}

void ApiControllerFixture::given_a_saved_server_name(){
    currentServerName = "my server name";
    Data::instance().configs().put(Options::SERVER_NAME, currentServerName);    
}

void ApiControllerFixture::given_logged_in_admin(){
    mockup_logged_in_user(UserAccount().setId(123123L).setActive(true).setIsAdmin(true).setLogin("login"));
    
    request.putHeader(Header(Headers::Cookie, string(api::cookies::sessionCookie).append("=").append("anysession")));
}

ServerInfo ApiControllerFixture::given_a_server_info(){
    ServerInfo serverInfo;
    serverInfo.setServerName("server name");
    serverInfo.setAppVersion({1, 2, 3});

    return serverInfo;
}

void ApiControllerFixture::when_a_update_server_name_request_is_done(){
    ServerInfo serverInfo;
    serverInfo.setServerName(EXPECTED_SERVER_NAME);
    
    executeJsonRequest("POST", "/", &serverInfo);
}

void ApiControllerFixture::then_server_name_should_be_updated(){
    REQUIRE_FALSE(capturePutConfig.invocations->empty());
    
    CHECK(capturePutConfig.getLast<0>() == Options::SERVER_NAME);
    CHECK(capturePutConfig.getLast<1>() == string(EXPECTED_SERVER_NAME));
}


void ApiControllerFixture::check_response_is_server_info(const std::string & expectedServerName){
    ServerInfo response = getResponse<ServerInfo>();
    
    CHECK(response.getServerName() == expectedServerName);
    
    auto expectedVersion = Environment::instance().getAppVersion();
    CHECK(response.getAppVersion()[0] == expectedVersion[0]);
    CHECK(response.getAppVersion()[1] == expectedVersion[1]);
    CHECK(response.getAppVersion()[2] == expectedVersion[2]);    
}
