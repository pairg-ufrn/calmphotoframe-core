#include "catch.hpp"

#include "Environment.h"
#include "Configurations.h"
#include "Options.h"
#include "utils/FileUtils.h"
#include "utils/StringUtils.h"

#include "helpers/ScopedDirectoryChange.h"

#include <string>

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;

namespace Constants {
    static const string dirToChange = "./test/data";
    static const string relativeDirToChange = "./test";
    static const string otherImageDir = "./test/output/images";
    static const string imageName = "image.jpg";
    static const string someDataDir = "someDir";
    static const string someDataFilename = "datafilename.example";
}

class EnvironmentTestFixture{
public:
    FileUtils & files;
    Environment environment;
public:
    EnvironmentTestFixture()
        : files(FileUtils::instance())
    {}

    bool pathsAreEquals(const string & filepathA, const string & filepathB){
        return FileUtils::instance().pathEquals(filepathA, filepathB);
    }

    void checkIsRelativeTo(const string & path, const string & parentCandidate){
        CHECK(!path.empty());
        CHECK(!parentCandidate.empty());

        string canonicalParentPath = files.canonicalPath(parentCandidate);
        string canonicalPath = files.canonicalPath(path);
        bool isRelative = StringUtils::instance().stringStartsWith(canonicalPath,canonicalParentPath);
        INFO("path " << canonicalPath << " is relative to "
             << canonicalParentPath << " ? " << isRelative );
        CHECK(isRelative);
    }

    void checkFilename(const string & path, const string & expectedFilename){
        string filename = files.extractFilename(path);
        CHECK(!filename.empty());
        CHECK(filename == expectedFilename);
    }
};

SCENARIO("Get images from default image dir", "[Environment]"){
GIVEN("an environment instance"){
    EnvironmentTestFixture fixture;
    Environment environment;
WHEN("getting the image dir"){
    string imageDir = environment.getImageDir();
    CHECK(!imageDir.empty());

THEN("it should give the default image dir relative to the current path"){
    string imageDirConf = Configurations::instance().get(Options::IMAGE_DIR, Options::Defaults::IMAGE_DIR);
    string currentPath = fixture.files.getCurrentDir();

    INFO("Image dir configuration: " << imageDirConf);
    INFO("Current path: " << currentPath);

    string expectedImageDir = fixture.files.joinPaths(currentPath, imageDirConf);

    CHECK(fixture.pathsAreEquals(imageDir, expectedImageDir));

AND_WHEN("getting an image path"){
    string imagePath = environment.getImagePath(Constants::imageName);
    CHECK(!imagePath.empty());
THEN("it should return an path relative to that dir"){
    string expectedImagePath = fixture.files.joinPaths(imageDir, Constants::imageName);
    CHECK(fixture.pathsAreEquals(imagePath, expectedImagePath));
}//THEN
}//WHEN

}//THEN

}//WHEN
}//GIVEN
}//SCENARIO


SCENARIO("Get images from custom absolute path", "[Environment]"){
GIVEN("an environment instance"){
    EnvironmentTestFixture fixture;
    Environment environment;
WHEN("setting the image dir to an absolute path"){
    string absoluteImageDir = fixture.files.absolutePath(Constants::otherImageDir);
    environment.setImageDir(absoluteImageDir);
THEN("Current image dir should change to that directory"){
    string imageDir = environment.getImageDir();
    CHECK(!imageDir.empty());
    CHECK(fixture.pathsAreEquals(absoluteImageDir, imageDir));

    AND_WHEN("Changing the environment base path"){
        environment.setBasePath(Constants::dirToChange);
    THEN("the image dir should not change"){
        string newImageDir = environment.getImageDir();
        CHECK(fixture.pathsAreEquals(imageDir, newImageDir));
    }
    }
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Get images from custom relative path", "[Environment]"){
GIVEN("an environment instance"){
    EnvironmentTestFixture fixture;
    Environment environment;
WHEN("setting the image dir to an absolute path"){
    string relativeImageDir = Constants::otherImageDir;
    environment.setImageDir(relativeImageDir);
THEN("Current image dir should change to that directory"){
    string imageDir = environment.getImageDir();
    INFO("Current dir: " << fixture.files.getCurrentDir());
    INFO("Environment Base path: " << environment.getBasePath());
    INFO("Relative image dir: " << relativeImageDir);
    INFO("Current image dir: " << imageDir);

    CHECK(!imageDir.empty());
    CHECK(fixture.pathsAreEquals(relativeImageDir, imageDir));

    AND_WHEN("Changing the environment base path"){
        environment.setBasePath(Constants::dirToChange);
    THEN("the image dir should change also"){
        string newImageDir = environment.getImageDir();
        CHECK(newImageDir != imageDir);
        CHECK(! fixture.pathsAreEquals(newImageDir, imageDir));
        string expectedImageDir = fixture.files.joinPaths(environment.getBasePath(), relativeImageDir);
        CHECK(fixture.pathsAreEquals(newImageDir, expectedImageDir));
    }
    }
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO


SCENARIO("Get default database path", "[Environment]"){
GIVEN("an environment instance"){
    EnvironmentTestFixture fixture;
    Environment environment;
WHEN("getting a data file without arguments"){
    string databasePath = environment.getDataFile();
THEN    ("it should return a path relative to the current base path"){
    INFO("database path: " << databasePath);
    fixture.checkIsRelativeTo(databasePath, fixture.environment.getBasePath());
AND_THEN("the path should point to a file with the default name"){
    CHECK(fixture.files.isFilePath(databasePath));
    fixture.checkFilename(databasePath, fixture.environment.getDataFilenameDefault());
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Get custom database path", "[Environment]"){
GIVEN("an environment instance"){
    EnvironmentTestFixture fixture;
    fixture.environment.setBasePath(Constants::dirToChange);

    INFO("current path: " << fixture.files.getCurrentDir());
WHEN("getting a data file passing some relative path and a filename"){
    string dataDir = Constants::someDataDir;
    string dataFilename = Constants::someDataFilename;
    string databasePath = fixture.environment.getDataFile(dataDir,dataFilename);
THEN    ("it should return a path relative to the current base path"){
    INFO("database path: " << databasePath);
    fixture.checkIsRelativeTo(databasePath, fixture.environment.getBasePath());
AND_THEN("the path should be relative to the informed dir"){
    fixture.checkIsRelativeTo(databasePath, fixture.environment.getPathFromBase(dataDir));
AND_THEN("the path should point to a file with the specified name"){
    CHECK(fixture.files.isFilePath(databasePath));
    fixture.checkFilename(databasePath, Constants::someDataFilename);
AND_THEN("the file does not need to exist"){
    CHECK(fixture.files.exists(databasePath) == false);
}//THEN
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Creating base path from current dir", "[Environment]"){

GIVEN("that environment is instantiated from some directory"){
    FileUtils &files = FileUtils::instance();
    string initialPath = files.absolutePath(files.getCurrentDir());

    ScopedDirectoryChange directoryChange(Constants::dirToChange);

    EnvironmentTestFixture fixture;
    Environment environment;

    const string instantiatedPath = FileUtils::instance().getCurrentDir();
    const string expectedPath = files.canonicalPath(files.joinPaths(initialPath, Constants::dirToChange));
    INFO("Initial path: " << files.canonicalPath(instantiatedPath));
    INFO("Expected initial path: " << expectedPath);
    CHECK(fixture.pathsAreEquals(expectedPath, instantiatedPath));

WHEN("Getting the base path"){
    string basePath = environment.getBasePath();
THEN("it should be equals to the current directory"){
    INFO("Environment base path: " << basePath);
    CHECK(!basePath.empty());
    CHECK(fixture.pathsAreEquals(basePath, instantiatedPath));
}//THEN
}//WHEN

}//GIVEN

}//SCENARIO

SCENARIO("Changing the current dir","[Environment]"){

GIVEN("an environment instance"){
    EnvironmentTestFixture fixture;
    Environment environment;
WHEN("changing the base path to a relative path"){
    environment.setBasePath(Constants::relativeDirToChange);
THEN("the new base path should be created relative to the current dir"){
    string basePath = fixture.files.canonicalPath(environment.getBasePath());
    string expectedBasePath = fixture.files.joinPaths(fixture.files.getCurrentDir(), Constants::relativeDirToChange);

    INFO("base path: " << basePath);
    INFO("expected base path: " << expectedBasePath);

    CHECK(fixture.pathsAreEquals(basePath, expectedBasePath));
AND_THEN("the base path should be absolute"){
    CHECK(fixture.files.isAbsolute(environment.getBasePath()));

    AND_WHEN("changing the current directory"){
        string dirToChange = Constants::dirToChange;
        INFO("Change to dir: " << dirToChange << " from: " << fixture.files.getCurrentDir());
        ScopedDirectoryChange changeDirectory(dirToChange);

        string currentDir = FileUtils::instance().getCurrentDir();
        INFO("Current path: " << currentDir);
        CHECK(!fixture.pathsAreEquals(currentDir, basePath));
    THEN("it should not change the base path"){
        INFO("Environment base path: " << fixture.files.canonicalPath(environment.getBasePath()));
        INFO("Expected base path: " << fixture.files.canonicalPath(basePath));
        CHECK(fixture.pathsAreEquals(basePath, environment.getBasePath()));
    }//THEN
    }
}//THEN
}//THEN
}//WHEN

}//GIVEN
}//SCENARIO
