#include "catch.hpp"

#include "PhotoFrameService.h"
#include "Configurations.h"
#include "Options.h"

#include "services/PhotoContextManager.h"

#include "utils/FileUtils.h"

#include "utils/ScopedDatabase.h"
#include "utils/ScopedFile.h"

using namespace calmframe;
using namespace calmframe::utils;

class PhotoFrameServiceFixture{
public:
    ScopedFile scopedImageDir;
    ScopedSqliteDatabase scopedDb{ScopedSqliteDatabase::DbType::InMemory};
    Configurations serviceConfigurations;
    PhotoFrameService service;
    PhotoContextManager contextManager;

    PhotoFrameServiceFixture()
        : scopedImageDir(Files().generateTmpFilepath(), true)
    {
        Files().createDirIfNotExists(scopedImageDir.getFilepath());
        serviceConfigurations.put(Options::IMAGE_DIR, scopedImageDir.getFilepath());
        serviceConfigurations.put(Options::DATA_DIR, scopedDb.getFilepath());
    }

    void initializeService(){
        service.startService(serviceConfigurations);
    }

    PhotoContextManager & getContextManager(){
        return contextManager;
    }
};

#define TEST_TAG "[PhotoFrameService]"
#define SCENARIO_TEST(msg, ...) SCENARIO_METHOD(PhotoFrameServiceFixture, msg, TEST_TAG __VA_ARGS__)

SCENARIO_TEST("When initializing PhotoFrameService, it should ensure that exist a default PhotoContext"){

    GIVEN("a not initialized PhotoFrameService with an empty database"){
    WHEN("initializing the PhotoFrameService"){
        initializeService();
    
    THEN("it should create a default PhotoContext"){
        PhotoContext defaultContext = getContextManager().getDefaultContext();
        CHECK(getContextManager().existContext(defaultContext.getId()));
    
    }//THEN
    }//WHEN
    }//GIVEN

}//SCENARIO

