#include "TestMacros.h"
#include <fakeit.hpp>

#include "PhotoFrameApplication.h"
#include "Configurations.h"
#include "Environment.h"
#include "Options.h"

#include "services/PhotoManager.h"

#include "data/Data.h"

#include "utils/ScopedDatabase.h"
#include "utils/FileUtils.h"
#include "utils/NullPointerException.hpp"
#include "utils/IllegalStateException.h"

#include <string>
#include <fstream>
#include <memory>

#include "json/json.h"

#include "helpers/ScopedDirectoryChange.h"
#include "helpers/ScopedTempDir.h"
#include "mocks/DataBuilderMockup.h"

#include "steps/DatabaseMockupSteps.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;
using namespace CommonExceptions;

using namespace fakeit;

namespace Constants {
    static const string ApplicationDirName = "applicationDir";
    static const int OtherPortNumber = 30000;
    static const string OtherLogDir = "./other_log_dir";
    static const string OtherLogName = "OtherLogName.log";
    static const string OtherDataDir = "./otherDataDir";
}

class PhotoFrameApplicationFixture{
public:
    void testApplicationStartDirectory();
    void testDefaultInitialization();
    void testInitializationWithJsonConfiguration();    
    
public:
    ~PhotoFrameApplicationFixture();

    void setup();
    void teardown();

    void initializeApplication();

    const string & getInitialPath();
    const string & getApplicationDir();
    const string getImageDir();
    string getApplicationBasePath();

    PhotoFrameApplication & getApplication();

    void checkDatabaseFile();
    void checkStartDir(const string & expectedDir);
    
    Configurations & then_the_configurations_should_load_correctly(Json::Value confAttributes);
    void then_application_attributes_should_change_correctly();
    Json::Value given_a_json_file_with_configurations();
    void check_image_dir_is_an_application_dir_subpath();

protected:
    void setupDb();
    
protected:
    void createJsonConfigurationAttributes(Json::Value &confAttributes);
    void saveJsonAttributes(const Json::Value & confAttributes, const string & jsonFilePath);

    void assertAppNotNull();

private:    
    ScopedTempDir appDir;
    std::shared_ptr<PhotoFrameApplication> app;
    std::shared_ptr<Mock<PhotoFrameApplication>> appSpy;
    std::shared_ptr<ScopedDirectoryChange> scopedDirChange;
    std::shared_ptr<ScopedSqliteDatabase> scopedDb;
};

#define TEST_TAG "[Application],[PhotoFrameApplication]"
#define FIXTURE PhotoFrameApplicationFixture

TEST_SCENARIO("Application should capture its start directory")     IS_DEFINED_BY(testApplicationStartDirectory)
TEST_SCENARIO("Application default initialization")                 IS_DEFINED_BY(testDefaultInitialization)
TEST_SCENARIO("Application initialization with json configuration") IS_DEFINED_BY(testInitializationWithJsonConfiguration)

/******************************************************************************************************/


void PhotoFrameApplicationFixture::testApplicationStartDirectory(){
    GIVEN("a start working directory where the PhotoFrameApplication is created"){
        CHECK(FileUtils::instance().exists(getApplicationDir()));
            
    WHEN("the application starts"){
        this->setup();
            
    THEN("the application start dir should be set to this directory"){
        checkStartDir(Files().getCurrentDir());
            
    }//WHEN
    }//THEN
    }//GIVEN
    
}

inline void PhotoFrameApplicationFixture::testDefaultInitialization(){
    GIVEN("a started up application"){
        this->setup();
        
    WHEN("changing the current directory"){
        REQUIRE(Files().exists(getInitialPath()));
        ScopedDirectoryChange changeDir(this->getInitialPath());
        INFO("Changed to dir: "  << changeDir.getChangePath());
        INFO("Current path is: " << Files().getCurrentDir() << "\n");

    AND_WHEN("initializing the PhotoFrameApplication"){
        this->initializeApplication();
        
    AND_WHEN("accessing Data::instance for the first time"){
        calmframe::data::Data::instance();
        
    THEN("the database file should be created on the application start directory with the default name"){
        this->checkDatabaseFile();

    AND_THEN("the application image dir should be a subpath of this directory"){
        check_image_dir_is_an_application_dir_subpath();
    }//THEN
    }//THEN
    }//WHEN
    }//WHEN
    }//WHEN
    }//GIVEN
}

void PhotoFrameApplicationFixture::testInitializationWithJsonConfiguration(){
    GIVEN("a started up application"){
        setup();
        
    GIVEN("an json file with default configuration name on application initialization path"){
        Json::Value confAttributes = given_a_json_file_with_configurations();
        
    WHEN("changing the current dir"){
        REQUIRE(Files().exists(getInitialPath()));
        ScopedDirectoryChange changeDir(this->getInitialPath());
        INFO("changed to dir: " << changeDir.getChangePath() << "\n");
        
    AND_WHEN("initializing the PhotoFrameApplication"){
        this->initializeApplication();

    THEN("the configurations should be loaded correctly"){
        then_the_configurations_should_load_correctly(confAttributes);

    THEN("the attributes should also be changed correctly"){
        then_application_attributes_should_change_correctly();
    }//THEN
    }//THEN
    }//WHEN
    }//WHEN
    }//GIVEN
    }//GIVEN
}


/*****************************************************************************************************/

void PhotoFrameApplicationFixture::setupDb(){
    scopedDb = std::make_shared<ScopedSqliteDatabase>(ScopedSqliteDatabase::DbType::InMemory);
}

Json::Value PhotoFrameApplicationFixture::given_a_json_file_with_configurations(){
    string jsonFilePath = Files().joinPaths(getApplicationDir(), Options::Defaults::CONFIGURATION_FILENAME);

    INFO("Current path: " <<Files().getCurrentDir());
    INFO("Application dir: " <<getApplicationDir());
    INFO("Configuration filepath: " << jsonFilePath);
    //Create json attributes
    Json::Value confAttributes;
    createJsonConfigurationAttributes(confAttributes);

    saveJsonAttributes(confAttributes, jsonFilePath);
    REQUIRE(Files().exists(jsonFilePath));

    return confAttributes;
}

Configurations & PhotoFrameApplicationFixture::then_the_configurations_should_load_correctly(Json::Value confAttributes){
    Configurations & conf = Configurations::instance();
    CHECK(conf.get(Options::BASE_DIR) == confAttributes[Options::BASE_DIR].asString());
    CHECK(conf.getInteger(Options::PORT) == confAttributes[Options::PORT].asInt());
    CHECK(conf.get(Options::LOG_DIR) == confAttributes[Options::LOG_DIR].asString());
    CHECK(conf.get(Options::LOG_FILE) == confAttributes[Options::LOG_FILE].asString());
    CHECK(conf.get(Options::DATA_DIR) == confAttributes[Options::DATA_DIR].asString());

    return conf;
}

void PhotoFrameApplicationFixture::then_application_attributes_should_change_correctly(){
    Configurations & conf = Configurations::instance();
    
    CHECK(getApplicationBasePath() == conf.get(Options::BASE_DIR));

    Environment & environment = Environment::instance();

    string logDirConf = environment.getPathFromBase(conf.get(Options::LOG_DIR));
    string logFileConf = Files().joinPaths(logDirConf, conf.get(Options::LOG_FILE));
    string dataDirConf = environment.getPathFromBase(conf.get(Options::DATA_DIR));
    CHECK(Files().pathEquals(environment.getLogDir(),  logDirConf));
    CHECK(Files().pathEquals(environment.getLogFile(), logFileConf));
    CHECK(Files().pathEquals(environment.getDataDir(), dataDirConf));    
}


void PhotoFrameApplicationFixture::check_image_dir_is_an_application_dir_subpath(){
    INFO("Image dir: " << getImageDir());
    INFO("Application base path: " << getApplicationBasePath());
    CHECK(Files().isSubpath(this->getImageDir(), this->getApplicationBasePath()));    
}

void PhotoFrameApplicationFixture::checkStartDir(const std::string & expectedDir)
{
    string appStartDir = app->getStartDirectory();
    INFO("Start dir: " << appStartDir << ". Canonical: " << Files().canonicalPath(appStartDir));
    INFO("Expect: " << expectedDir << ". Canonical: " << Files().canonicalPath(expectedDir));

    CHECK(Files().pathEquals(appStartDir, expectedDir));
}

/*****************************************************************************************************/

PhotoFrameApplicationFixture::~PhotoFrameApplicationFixture(){
    try{
        teardown();
    }
    catch(exception & ex){
        logError("PhotoFrameApplicationFixture::destructor:\t" "Failed due to exception: %s", ex.what());
    }
}

void PhotoFrameApplicationFixture::setup(){
    Configurations::instance().put(Options::LOG_LEVEL, LogLevel::toString(LogLevel::Error));

    if(app != NULL || scopedDirChange != NULL){
        throw IllegalStateException("Setup application twice.");
    }
    scopedDirChange = std::make_shared<ScopedDirectoryChange>(appDir.getFilepath());
    INFO("dir after scopedDirChange: " << Files().getCurrentDir());
    CHECK(Files().pathEquals(appDir.getFilepath(), Files().getCurrentDir()));
    
    app = std::make_shared<PhotoFrameApplication>();
    appSpy = std::make_shared<Mock<PhotoFrameApplication>>(*app);
    
    auto & mock = *appSpy;
    When(Method(mock, setupData)).Do(
        [this](Configurations &){
            this->setupDb();
        }
    );
}

void PhotoFrameApplicationFixture::teardown(){
    if(app){
        getApplication().uninitialize();
        app.reset();
    }
    scopedDirChange.reset();
    
    MainLog().resetSetup();
}

void PhotoFrameApplicationFixture::initializeApplication(){
    getApplication().initialize(getApplication());
}

const string &PhotoFrameApplicationFixture::getInitialPath(){
    return scopedDirChange->getInitialPath();
}

const string &PhotoFrameApplicationFixture::getApplicationDir(){
    return appDir.getFilepath();
}

const string PhotoFrameApplicationFixture::getImageDir(){
    return getApplication().getPhotoFrameService().getPhotoFrame().getImagesDir();
}

string PhotoFrameApplicationFixture::getApplicationBasePath(){
    return Environment::instance().getBasePath();
}

PhotoFrameApplication &PhotoFrameApplicationFixture::getApplication(){
    assertAppNotNull();
    return appSpy->get();
}

void PhotoFrameApplicationFixture::checkDatabaseFile(){
    const string & expectedDataDir = appDir.getFilepath();
    
//    string expectedDatabaseFile = FileUtils::instance()
//            .joinPaths(dataDir, Environment::instance().getDataFilenameDefault());
    
    INFO("Environment dataDir: " << Environment::instance().getDataDir());

    auto appDataDir = getApplication().getDataDir(Configurations::instance());
    
    CAPTURE(expectedDataDir);
    CAPTURE(appDataDir);
    CHECK(Files().pathEquals(expectedDataDir, appDataDir));
}

void PhotoFrameApplicationFixture::createJsonConfigurationAttributes(Json::Value & confAttributes){
    string basePath = Files().joinPaths(getApplicationDir(), "baseDir");
    INFO("using path " << basePath << " as new base path (should not exist yet)");
    REQUIRE(Files().exists(basePath) == false);
    
    Files().createDirIfNotExists(basePath);
    
    REQUIRE(Files().exists(basePath));
    confAttributes[Options::BASE_DIR] = basePath;

    confAttributes[Options::PORT] = Constants::OtherPortNumber;
    confAttributes[Options::LOG_DIR] = Constants::OtherLogDir;
    confAttributes[Options::LOG_FILE] = Constants::OtherLogName;
    confAttributes[Options::DATA_DIR] = Constants::OtherDataDir;
}

void PhotoFrameApplicationFixture::saveJsonAttributes(const Json::Value &confAttributes, const string &jsonFilePath){
    ofstream jsonFile;
    jsonFile.open(jsonFilePath.c_str());
    Json::StyledStreamWriter jsonWriter;
    jsonWriter.write(jsonFile, confAttributes);

    jsonFile.close();
}

void PhotoFrameApplicationFixture::assertAppNotNull(){
    CommonExceptions::assertNotNull(app.get(), "Photo Frame Application is NULL");
}
