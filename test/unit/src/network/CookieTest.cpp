#include "catch.hpp"

#include "network/Header.h"
#include "network/Headers.h"
#include "network/Cookie.h"

#include "utils/TimeUtils.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::network;

namespace Constants{
    static const char * cookieName  = "session";
    static const char * cookieValue = "token";
}

SCENARIO("Cookie creation","[Cookie]"){
GIVEN("an cookie name and a cookie value"){
    string cookieName = Constants::cookieName
         , cookieValue = Constants::cookieValue;
WHEN("creating a cookie"){
    Cookie cookie(cookieName, cookieValue);
    string headerValue = cookieName + "=" + cookieValue;
    THEN("it should have '=' as valueSeparator and ';' as parameterSeparator"){
        CHECK(cookie.getValueSeparator() == '=');
        CHECK(cookie.getParameterSeparator() == ';');
    THEN("it should format the header value as <CookieName>=<CookieValue>"){
        CHECK(cookie.getValue() == headerValue);
    AND_THEN("the cookie header name should be \"Set-Cookie\""){
        CHECK(cookie.getName() == Headers::SetCookie);
    }//AND_THEN
    }//THEN
    }

    WHEN("Creating an header from a cookie"){
        Header headerCookie(cookie);
    THEN("it should keep the correct name and value"){
        CHECK(headerCookie.getName() == Headers::SetCookie);
        CHECK(headerCookie.getValue() == headerValue);

    AND_WHEN("Creating an cookie from an cookie-header"){
        Cookie cookieFromHeader(headerCookie);
    THEN("it should parse correctly the cookie name and value"){
        CHECK(cookieFromHeader.getName()        == Headers::SetCookie);
        CHECK(cookieFromHeader.getCookieName()  == cookieName);
        CHECK(cookieFromHeader.getCookieValue() == cookieValue);
    }
    }

    }
    }

}//WHEN
}//GIVEN
}//SCENARIO

SCENARIO("Cookie creation with parameters","[Cookie]"){
GIVEN("a cookie value string with parameters"){
    string cookieValue = "name=value; Expires=Thu, 01-Jan-1970 00:00:01 GMT; Secure; Path=/; Domain=.example.com; HttpOnly";
WHEN("creating a cookie"){
    Cookie cookie;
AND_WHEN("setting the cookie header value to that string"){
    cookie.setValue(cookieValue);
THEN("it should parse correctly the parameters on string"){
    CHECK(cookie.getParameter(CookieParameters::Expires)  == "Thu, 01-Jan-1970 00:00:01 GMT");
    CHECK(cookie.getParameter(CookieParameters::Path)     == "/");
    CHECK(cookie.getParameter(CookieParameters::Domain)   == ".example.com");
AND_THEN("the parameters without value should also be stored"){
    CHECK(cookie.getParameter(CookieParameters::HttpOnly) == "");
    CHECK(cookie.getParameter(CookieParameters::Secure)   == "");
    CHECK(cookie.containsParameter(CookieParameters::HttpOnly) == true);
    CHECK(cookie.containsParameter(CookieParameters::Secure) == true);
AND_THEN("it should parse also the cookie name and cookie value"){
    CHECK(cookie.getCookieName() == "name");
    CHECK(cookie.getCookieValue() == "value");
}
}
}
}
}
}
}//SCENARIO

SCENARIO("Cookie to string empty parameters","[Cookie]"){
GIVEN("a cookie name, a cookie value and an parameter without value"){
    string cookieName = "name", cookieValue = "value", parameter = "Secure";
WHEN("building a cookie with that values"){
    Cookie cookie(cookieName, cookieValue);
    cookie.putParameter(parameter,"");
AND_WHEN("getting the cookie header value"){
    string cookieHeaderValue = cookie.getValue();
THEN("it should be formatted with no \";\" on the end"){
AND_THEN("should exist  1 space between each parameter"){
AND_THEN("parameters without value should not have '=' symbol"){
    string cookieExpectedValue = "name=value; Secure";
    CHECK(cookieHeaderValue == cookieExpectedValue);
}
}
}
}
}
}
}//SCENARIO
