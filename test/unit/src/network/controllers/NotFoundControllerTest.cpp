#include "catch.hpp"

#include "network/controller/NotFoundController.h"

#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"

#include <string>
#include <vector>

using namespace std;
using namespace calmframe::network;

class NotFoundControllerFixture{
public:
    MockRequest request;
    MockResponse response;
    NotFoundController notFoundController;

    void setupRequest(const string & method){
        request.setMethod(method);
    }

    vector<string> getRequestMethods(){
        const char * methodsArr[] = {"GET", "POST", "PUT", "DELETE", "OPTIONS"};
        return vector<string>(methodsArr + 0, methodsArr + 5);
    }
};


SCENARIO("NotFoundController respond with 404 for any request", "[NotFoundController]"){
    NotFoundControllerFixture fixture;
GIVEN("any type of request"){
    vector<string> requestMethods = fixture.getRequestMethods();
    vector<string>::iterator iter;
    for(iter = requestMethods.begin(); iter != requestMethods.end(); ++iter){
        fixture.setupRequest(*iter);

        WHEN("the request is intercepted by an NotFoundController instance"){
            CHECK(fixture.notFoundController.matchUrl(fixture.request.getUrl()));
            bool handled = fixture.notFoundController.onRequest(fixture.request, fixture.response);

        THEN("it should respond with an 404 message"){
            CHECK(handled == true);
            CHECK(fixture.response.statusLine().getStatusCode() == 404);
            CHECK(fixture.response.hasSentHeaders());

        }//THEN
        }//WHEN

    }//for
}//GIVEN
}//SCENARIO
