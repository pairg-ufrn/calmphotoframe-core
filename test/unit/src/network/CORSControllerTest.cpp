#include "catch.hpp"

#include "network/controller/CORSController.h"
#include "network/StatusResponses.h"

#include "utils/StringUtils.h"

#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"
#include "mocks/MockController.h"

#include <algorithm>
#include <iostream>

using namespace calmframe;
using namespace calmframe::network;
using namespace calmframe::utils;
using namespace std;

class CORSFixture{
public:
    MockRequest request;
    MockResponse response;
    CORSController corsController;

    void setupRequest(){
        request.setMethod("POST");
        request.setUrl("/");
        request.setContentType(ContentTypes::JSON);
        request.putHeader(Header(Headers::Origin, "http://other.domain.example.com"));
    }
    void setupPreflightRequest(){
        setupRequest();
        request.setMethod("OPTIONS");
        request.putHeader(Header(Headers::AccessControlRequestMethod, "PUT"));

        const char * headers[] = {Headers::ContentType, Headers::Cookie};
        string requestedHeaders = StringUtils::instance().join(headers + 0, headers + 2, ", ");

        request.putHeader(Header(Headers::AccessControlRequestHeaders, requestedHeaders));
    }

    void checkHeaderExist(const Response & response, const std::string & headerName)
    {
        CHECK_FALSE(response.getHeader(headerName).empty());
    }

    void checkAllowOriginResponseHeader(){
        string originRequestHeaderValue = request.getHeaderValue(Headers::Origin);
        string allowOriginHeaderValue   = response.getHeader(Headers::AccessControlAllowOrigin).getValue();
        CHECK(allowOriginHeaderValue.empty() == false);
        CHECK(originRequestHeaderValue == allowOriginHeaderValue);
    }

    void checkAllowMethodsHeader(){
        const Header & allowMethodsHeader = response.getHeader(Headers::AccessControlAllowMethods);
        CHECK(allowMethodsHeader.getName() == Headers::AccessControlAllowMethods);

        INFO("Access-Control-Allow-Methods: " << allowMethodsHeader.getValue());

        vector<string> allowedMethods = StringUtils::instance().splitAndTrim(allowMethodsHeader.getValue(), ',');
        CHECK(existItem(allowedMethods, "GET"));
        CHECK(existItem(allowedMethods, "POST"));
        CHECK(existItem(allowedMethods, "PUT"));
        CHECK(existItem(allowedMethods, "DELETE"));
        CHECK(existItem(allowedMethods, "OPTIONS"));
    }

    void checkAllowHeadersHeader(){
        checkHeaderExist(response, Headers::AccessControlAllowHeaders);

        const Header & allowHeadersHeader = response.getHeader(Headers::AccessControlAllowHeaders);

        INFO("Access-Control-Allow-Headers: " << allowHeadersHeader.getValue());

        CHECK(allowHeadersHeader.getName() == Headers::AccessControlAllowHeaders);

        compareWithRequestHeaders(allowHeadersHeader);
    }
    void compareWithRequestHeaders(const Header& allowHeadersHeader)
    {
        string requestedHeadersValue = request.getHeaderValue(Headers::AccessControlRequestHeaders);

        INFO("Access-Control-Request-Headers: " << requestedHeadersValue);

        vector<string> allowedHeaders = StringUtils::instance().splitAndTrim(allowHeadersHeader.getValue(), ',');
        vector<string> requestedHeaders = StringUtils::instance().splitAndTrim(requestedHeadersValue, ',');

        CHECK(isSetEquals(allowedHeaders, requestedHeaders));
    }

    bool isSetEquals(const vector<string> & vec1, const vector<string> & vec2){
        set<string> set1(vec1.begin(), vec1.end());
        set<string> set2(vec2.begin(), vec2.end());

        return set1 == set2;
    }

    bool existItem(const vector<string> & list, const string & item){
        vector<string>::const_iterator iter = std::find(list.begin(), list.end(), item);
        return iter != list.end();
    }
};

//Access-Control-Allow-Origin
//Access-Control-Allow-Credentials
//Access-Control-Allow-Methods
//(?)Access-Control-Allow-Headers
//(?)Access-Control-Expose-Headers
//(?)Access-Control-Max-Age

SCENARIO("CORSController should add CORS parameters on normal requests", "[CORS],[network]"){

GIVEN("an request with an Origin header"){
    CORSFixture fixture;
    fixture.setupRequest();
    CHECK(fixture.request.containsHeader(Headers::Origin));
WHEN("the request is intercepted by an CORSController instance"){
    bool handled = fixture.corsController.onRequest(fixture.request, fixture.response);
THEN("the CORS headers should be added to response"){
    fixture.checkHeaderExist(fixture.response, Headers::AccessControlAllowOrigin);
    fixture.checkHeaderExist(fixture.response, Headers::AccessControlAllowCredentials);
AND_THEN("the Access-Control-Allow-Origin header should be equals to the Origin request header"){
    fixture.checkAllowOriginResponseHeader();
AND_THEN("the Access-Control-Allow-Credentials should be true"){
    CHECK("true" == fixture.response.getHeader(Headers::AccessControlAllowCredentials).getValue());
AND_THEN("the request should not be handled, to allow further controllers deal with it"){
    CHECK(handled == false);
}//THEN
}//THEN
}
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO

SCENARIO("CORSController should handle preflight requests", "[CORS],[network]"){
    CORSFixture fixture;
GIVEN("an request made to some resource"){
    fixture.setupPreflightRequest();

GIVEN("that the request has an Origin header"){
    CHECK(fixture.request.containsHeader(Headers::Origin));

GIVEN("that the method request is OPTIONS"){
    CHECK(fixture.request.getMethod() == "OPTIONS");

GIVEN("that the request contains an Access-Control-Request-Method header"){
    CHECK(fixture.request.containsHeader(Headers::AccessControlRequestMethod));

GIVEN("that the request contains an non empty Access-Control-Request-Headers header"){
    CHECK(fixture.request.containsHeader(Headers::AccessControlRequestHeaders));
    CHECK_FALSE(fixture.request.getHeaderValue(Headers::AccessControlRequestHeaders).empty());

WHEN("the request is intercepted by an CORSController instance"){
    bool handled = fixture.corsController.onRequest(fixture.request, fixture.response);

THEN("the request response should be 200"){
    CHECK(handled == true);
    CHECK(fixture.response.hasSentHeaders());
    CHECK(fixture.response.statusLine() == StatusResponses::OK);

THEN("the response should contain an Access-Control-Allow-Origin header equals to the Origin request header"){
    fixture.checkAllowOriginResponseHeader();

THEN("the response should contain an 'Access-Control-Allow-Methods' header with all allowed HTTP methods"){
    fixture.checkAllowMethodsHeader();

THEN("by default, the response should contain an 'Access-Control-Allow-Headers' header "
        "with all headers from 'Access-Control-Request-Headers'"){
    fixture.checkAllowHeadersHeader();

}//THEN
}//THEN
}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN
}//GIVEN
}//GIVEN
}//GIVEN
}//SCENARIO
