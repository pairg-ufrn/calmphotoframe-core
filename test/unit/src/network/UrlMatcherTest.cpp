#include "catch.hpp"

#include "network/controller/UrlMatcher.h"

using namespace calmframe;

#define TEST_TAG "[UrlMatcher]"

TEST_CASE("UrlMatcher should match subpaths of a url", TEST_TAG){
    UrlMatcher matcher;

    CHECK(matcher.match("/root", "/root/path"));
    CHECK(matcher.match("/some", "/some"));
    CHECK(matcher.match("/", "/any/subpath"));

    CHECK_FALSE(matcher.match("/root/path", "/other"));
    CHECK_FALSE(matcher.match("/root/path", "/path"));
    CHECK_FALSE(matcher.match("/path", "/"));
}


TEST_CASE("UrlMatcher should consider parameterized paths when matching urls", TEST_TAG){
    UrlMatcher matcher;

    CHECK(matcher.match("/:parameter", "/any/path"));
    CHECK(matcher.match("/path/:parameter", "/path/anything"));
    CHECK(matcher.match("/path/:param1/:param2", "/path/anything/other"));
    CHECK(matcher.match("/:param/required/subpath", "/any/required/subpath"));

    CHECK_FALSE(matcher.match("/path/:parameter", "/path"));
    CHECK_FALSE(matcher.match("/root/:any", "/other"));
    CHECK_FALSE(matcher.match("/:param/required/subpath", "/missing/required"));
    CHECK_FALSE(matcher.match("/path", "/"));
}
