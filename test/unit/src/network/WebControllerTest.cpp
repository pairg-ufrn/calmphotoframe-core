#include "catch.hpp"

#include "mocks/MockController.h"

#include "network/controller/ControllerRouter.h"

using namespace std;
using namespace calmframe::network;

#define SOME_PATH "/alguma_url"
#define OTHER_PATH "/outra_url"
#define RANDOM_PATH "/randomurl"

#define CONTROLLER_URL          SOME_PATH
#define CONTROLLER_SUBURL       CONTROLLER_URL OTHER_PATH
#define CONTROLLER_UNRELATEDURL RANDOM_PATH CONTROLLER_URL

#define TEST_TAG "[WebController]"

class WebControllerFixture{
public:
    MockController controller;

    void checkNotMatchUrl(const std::string & controllerUrl, const std::string & otherUrl){
        checkMatchUrl(controllerUrl, otherUrl, false);
    }
    void checkMatchUrl(const std::string & controllerUrl, const std::string & otherUrl, bool shouldMatch=true){
        INFO("Controller Url: " << controllerUrl << "; url: " << otherUrl);

        controller.setBaseUrl(controllerUrl);
        if(shouldMatch){
            CHECK(controller.matchUrl(otherUrl));
        }
        else{
            CHECK(!controller.matchUrl(otherUrl));
        }
    }
};
typedef WebControllerFixture Fixture;

TEST_CASE("Match url", TEST_TAG){
    MockController controller(CONTROLLER_URL);

    CHECK(controller.matchUrl(CONTROLLER_URL));
    CHECK(controller.matchUrl(CONTROLLER_SUBURL));
    CHECK(!controller.matchUrl(CONTROLLER_UNRELATEDURL));
}

TEST_CASE_METHOD(Fixture, "Match wildcard url", TEST_TAG){
    //specify any path followed by 'SOME_PATH'
    string startWildcard = "/*" SOME_PATH;

    checkMatchUrl(startWildcard, OTHER_PATH SOME_PATH);
    checkMatchUrl(startWildcard, OTHER_PATH SOME_PATH OTHER_PATH);
    checkNotMatchUrl(startWildcard, SOME_PATH);
    checkNotMatchUrl(startWildcard, OTHER_PATH OTHER_PATH SOME_PATH);

    string finishWildcard = SOME_PATH "/*";

    checkMatchUrl(finishWildcard, SOME_PATH OTHER_PATH);
    checkMatchUrl(finishWildcard, SOME_PATH OTHER_PATH OTHER_PATH);
    checkNotMatchUrl(finishWildcard, SOME_PATH); //Need to have a second path
    checkNotMatchUrl(finishWildcard, OTHER_PATH SOME_PATH); //Does not start with SOME_PATH

    string middleWildcard = SOME_PATH "/*" OTHER_PATH;

    checkMatchUrl(middleWildcard, SOME_PATH RANDOM_PATH OTHER_PATH);
    checkMatchUrl(middleWildcard, SOME_PATH RANDOM_PATH OTHER_PATH RANDOM_PATH);
    checkNotMatchUrl(middleWildcard, SOME_PATH OTHER_PATH); //Miss path between SOME_PATH and OTHER_PATH
}



TEST_CASE_METHOD(Fixture, "Match double wildcard url", TEST_TAG){
    //specify any sequence of paths followed by 'SOME_PATH'
    string startWildcard = "**" SOME_PATH; //Can use "**" or "/**" at start

    checkMatchUrl(startWildcard, OTHER_PATH SOME_PATH);
    checkMatchUrl(startWildcard, OTHER_PATH RANDOM_PATH SOME_PATH);
    checkMatchUrl(startWildcard, SOME_PATH); //accepts also empty sequences
    checkMatchUrl(startWildcard, SOME_PATH OTHER_PATH); //match because ** can expand to empty

    //Requires finishing with SOME_PATH
    checkNotMatchUrl(startWildcard, "");
    checkNotMatchUrl(startWildcard, OTHER_PATH);

    //Matches any url that start with SOME_PATH and is followed by OTHER_PATH
    string middleWildcard = SOME_PATH "/**" OTHER_PATH;
    checkMatchUrl(middleWildcard, SOME_PATH OTHER_PATH);
    checkMatchUrl(middleWildcard, SOME_PATH OTHER_PATH RANDOM_PATH); //matches because is subUrl
    checkMatchUrl(middleWildcard, SOME_PATH RANDOM_PATH OTHER_PATH);
    checkMatchUrl(middleWildcard, SOME_PATH RANDOM_PATH RANDOM_PATH OTHER_PATH);
    checkMatchUrl(middleWildcard, SOME_PATH RANDOM_PATH RANDOM_PATH OTHER_PATH);

    checkNotMatchUrl(middleWildcard, "");
    checkNotMatchUrl(middleWildcard, SOME_PATH RANDOM_PATH); //missing OTHER_PATH
    checkNotMatchUrl(middleWildcard, SOME_PATH RANDOM_PATH RANDOM_PATH); //missing OTHER_PATH
    checkNotMatchUrl(middleWildcard, OTHER_PATH); //missing SOME_PATH on start
    checkNotMatchUrl(middleWildcard, RANDOM_PATH SOME_PATH OTHER_PATH); //does not start with SOME_PATH

    string finishWildcard = SOME_PATH "/**"; //The wildcard does not make difference on finish
    checkMatchUrl(finishWildcard, SOME_PATH);
    checkMatchUrl(finishWildcard, SOME_PATH RANDOM_PATH);
    checkNotMatchUrl(finishWildcard, OTHER_PATH SOME_PATH);
}
