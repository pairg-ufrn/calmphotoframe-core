#include "catch.hpp"

#include <string>
#include "network/ContentType.h"
#include "network/ContentTypes.hpp"

using namespace std;
using namespace calmframe::network;

SCENARIO("Constructor","[ContentType]"){
    GIVEN("a default constructed contentType instance"){
        ContentType contentType;
        THEN("it name should be \"Content-Type\" "){
            CHECK(contentType.getName()=="Content-Type");
        AND_THEN("it value should be \"text/plain\" "){
            CHECK(contentType.getValue()=="text/plain");
        AND_THEN("it type should be \"text\""){
            CHECK(contentType.getType() == "text");
        AND_THEN("it subtype should be \"plain\""){
            CHECK(contentType.getSubtype() == "plain");
        }
        }
        }
        }

        WHEN("call setName it should not change the header name"){
            contentType.setName("anyName");
            CHECK(contentType.getName() == "Content-Type");
        }
    }
    GIVEN("a contentType constructed with content type string"){
        string contentTypeStr = string(Types::IMAGE) + string("/") + string(SubTypes::PNG)
                + string("; parameter=example");
        ContentType contentType(contentTypeStr);
        THEN("it name should be \"Content-Type\" "){
            CHECK(contentType.getName()=="Content-Type");
        AND_THEN("it value should be the same of content type string (if it is well formatted)"){
            CHECK(contentType.getValue()==contentTypeStr);
        AND_THEN("it type correct split the type and subtype "){
            CHECK(contentType.getType() == Types::IMAGE);
            CHECK(contentType.getSubtype() == SubTypes::PNG);
        AND_THEN("it subtype should correct split the parameters"){
            CHECK(contentType.getParameters().size() == 1);
            CHECK(contentType.getParameters().find("parameter") != contentType.getParameters().end());
        }
        }
        }
        }
    }
}

#include "utils/StringUtils.h"
#include <iostream>
using namespace calmframe::utils;

#define EXPECTED_VALUE "application/json; encoding=UTF-8"
#define TYPE "application"
#define SUBTYPE "json"
#define ENCODING_PARAM "encoding=UTF-8"
SCENARIO("Constructor with parameters","[ContentType]"){
    GIVEN("a ContentType built with type \"" TYPE "\", subtype \"" SUBTYPE "\" and param: \"" ENCODING_PARAM "\"")
    {
        ContentType contentType(Types::APPLICATION,SubTypes::JSON, ENCODING_PARAM);
        contentType.setParameters(ENCODING_PARAM);
        THEN("it type should be \"application\" "){
            CHECK(contentType.getType()=="application");
        AND_THEN("it subtype should be \"json\" "){
            CHECK(contentType.getSubtype() == "json");
        AND_THEN("it should contain an encoding parameter with UTF-8 value"){
            CHECK(contentType.getParameter("encoding") == "UTF-8");
        AND_THEN("it value should be \"" EXPECTED_VALUE "\""){
            CHECK(contentType.getValue() == EXPECTED_VALUE);
        }
        }
        }
        }
    }
}
