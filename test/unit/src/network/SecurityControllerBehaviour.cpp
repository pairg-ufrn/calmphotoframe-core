#include "catch.hpp"

#include "network/controller/SecurityController.h"
#include "network/StatusResponses.h"

#include "mocks/MockController.h"
#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"

using namespace calmframe;
using namespace network;

namespace Constants {
    static const char * ControllerURL = "/controller";
    static const char * ControllerURL2 = "/controller2";
    static const char * InvalidURL     = "/invalidURL";
}

class MockupValidator : public RequestValidator{
public:
    bool validate;
    MockupValidator()
        : validate(false)
    {}

    virtual bool validateRequest(Request & request) const{
        return validate;
    }
};

class SecurityControllerFixture{
public:
    SecurityController securityController;
    MockController controller, controller2;
    MockupValidator validator;
    MockRequest request;
    MockResponse response;

    SecurityControllerFixture(){
        controller.setBaseUrl(Constants::ControllerURL);
        controller2.setBaseUrl(Constants::ControllerURL2);
    }

    void registerController(bool check=true){
        registerController(&controller, check);
    }
    void registerController(WebController * controller, bool check=true){
        securityController.addController(controller);
        if(check){
            CHECK(securityController.containsController(controller));
        }
    }
    void setupControllers(){
        registerController(&controller);
        registerController(&controller2);
    }

    void setValidator(bool validate=false, bool check=true){
        securityController.setValidator(&validator);
        validator.validate = validate;
        if(check){
            CHECK(securityController.getValidator() == &validator);
        }
    }

    void setupRequest(const std::string & url){
        request.setUrl(url);
    }


};

SCENARIO("SecurityController should reject invalid connections", "[SecurityController]"){
    SecurityControllerFixture fixture;
GIVEN("an controller registered on SecurityController"){
    fixture.setupControllers();
GIVEN("some validator is set on SecurityController"){
    MockupValidator validator;
    fixture.securityController.setValidator(&validator);
    CHECK(fixture.securityController.getValidator() == &validator);
WHEN ("a request is done to that controller"){
    MockRequest request;
    request.setUrl(Constants::ControllerURL);
    MockResponse response;

    CHECK(fixture.controller.matchUrl(request.getUrl()));
AND_WHEN("the validator rejects the request"){
    validator.validate = false;
    request.setMethod("GET");
    bool executed = fixture.securityController.onRequest(request, response);
    CHECK(executed == true);
THEN("the SecurityController should respond with an Unauthorized response"){
    CHECK(response.statusLine() == StatusResponses::UNAUTHORIZED);
}//THEN
}//AND_WHEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("SecurityController should redirect valid requests to controllers", "[SecurityController]"){
    SecurityControllerFixture fixture;
GIVEN("an controller registered on SecurityController"){
    fixture.registerController();
GIVEN("some validator is set on SecurityController"){
    fixture.setValidator();
WHEN ("a request is done to that controller"){
    fixture.setupRequest(Constants::ControllerURL);
AND_WHEN("the validator accepts the request"){
    fixture.validator.validate = true;
    fixture.request.setMethod("POST");
    CHECK(fixture.securityController.onRequest(fixture.request, fixture.response));
THEN("the correspondent method on controller should be executed"){
    CHECK(fixture.controller.getMethodCallsCount(HttpMethods::names::POST) == 1);
}//THEN
}//AND_WHEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

SCENARIO("SecurityController should not execute if there is none controller match the request", "[SecurityController]"){
    SecurityControllerFixture fixture;
GIVEN("some controllers registered on SecurityController"){
    fixture.setupControllers();
    fixture.setValidator(true);
WHEN ("a request is done to an invalid URL"){
    fixture.setupRequest(Constants::InvalidURL);
    CHECK(!fixture.securityController.matchUrl(Constants::InvalidURL));
THEN("the method should not execute"){
    CHECK(fixture.securityController.onPost(fixture.request, fixture.response) == false);
}//AND_WHEN
}//WHEN
}//GIVEN

}//SCENARIO
