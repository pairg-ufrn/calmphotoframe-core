#include "catch.hpp"

#include "network/Url.h"

using namespace calmframe::network;

#define SCHEME "http"
#define AUTHORITY "calmphotoframe.example.com"
#define PATH_0 "contexts"
#define PATH_1 "0"
#define PATH_2 "collections"
#define PATH "/" PATH_0 "/" PATH_1 "/" PATH_2
#define QUERY "arg=random&other=abc"
#define FRAGMENT "fragment"
#define FULL_URL        SCHEME "://" AUTHORITY PATH "?" QUERY "#" FRAGMENT
#define RELATIVE_URL    PATH "#" FRAGMENT

void checkPaths(const Url & url, int expectedNumPaths, const char * paths[]){
    CHECK(url.path().count() == expectedNumPaths);

    for(int i=0; i<url.path().count() && i<expectedNumPaths; ++i){
        CHECK(url.path().get(i) == paths[i]);
    }
}

#define TEST_TAG "[Url]"

TEST_CASE("Full url", TEST_TAG){
    Url url(FULL_URL);
    CHECK(url.getFragment() == FRAGMENT);
    CHECK(url.getScheme() == SCHEME);
    CHECK(url.getHost() == AUTHORITY);
    CHECK(url.query().get("other") == "abc");
    CHECK(url.query().get("arg") == "random");

    const char * paths[] = {PATH_0, PATH_1, PATH_2};
    checkPaths(url, 3, paths);
}

TEST_CASE("Relative url", TEST_TAG){
    Url url(RELATIVE_URL);

    CHECK(url.getFragment() == FRAGMENT);

    CHECK(url.path().count() == 3);

    const char * paths[] = {PATH_0, PATH_1, PATH_2};
    checkPaths(url, 3, paths);
}

TEST_CASE("Get Path from Url", TEST_TAG){
    Url fullUrl(FULL_URL);
    CHECK(fullUrl.path().get(0) == PATH_0);
    CHECK(fullUrl.path().get(1) == PATH_1);
    CHECK(fullUrl.path().get(2) == PATH_2);

    CHECK(fullUrl.path().get(3) == std::string());
    CHECK(fullUrl.path().get(-1) == std::string());
}
