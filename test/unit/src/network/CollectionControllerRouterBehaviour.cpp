#include "catch.hpp"

#include "network/controller/CollectionControllerRouter.h"
#include "network/StatusResponses.h"

#include "mocks/MockController.h"
#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"

#include "network/HttpMethods.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::network;

namespace Constants {
    static const char * ControllerURL = "/controller";
    static const char * ControllerURL2 = "/controller2";
    static const char * InvalidURL     = "/invalidURL";
}

class DoNotHandleMockController : public MockController{
    typedef MockController super;
public:
    DoNotHandleMockController(const string baseUrl=""){
        this->setBaseUrl(baseUrl);
        this->setResponseEnabled(false);
    }

    virtual bool onRequest(Request &request, Response &response){
        super::onRequest(request, response);
        return false;
    }
};

class CollectionControllerRouterFixture{
public:
    CollectionControllerRouter collectionControllerRouter;
    MockController controller, controller2;
    MockRequest request;
    MockResponse response;

    CollectionControllerRouterFixture(){
        controller.setBaseUrl(Constants::ControllerURL);
        controller2.setBaseUrl(Constants::ControllerURL2);
    }

    void registerController(bool check=true){
        registerController(&controller, check);
    }
    void registerController(WebController * controller, bool check=true){
        collectionControllerRouter.addController(controller);
        if(check){
            CHECK(collectionControllerRouter.containsController(controller));
        }
    }
    void setupControllers(){
        registerController(&controller);
        registerController(&controller2);
    }

    void setupRequest(const std::string & url){
        request.setUrl(url);
    }

    bool executeGetRequest(){
        request.setMethod("GET");
        return collectionControllerRouter.onRequest(request, response);
    }
};

SCENARIO("CollectionControllerRouter should redirect a request when some controller match"
         , "[CollectionControllerRouter]")
{
    CollectionControllerRouterFixture fixture;
GIVEN("some registered controllers on CollectionControllerRouter"){
    fixture.setupControllers();
WHEN ("a request is done to one controller"){
    fixture.setupRequest(fixture.controller2.getBaseUrl());
    CHECK(fixture.controller2.matchUrl(fixture.request.getUrl()));
    CHECK(!fixture.controller.matchUrl(fixture.request.getUrl()));
    CHECK(fixture.collectionControllerRouter.matchUrl(fixture.request.getUrl()));
    CHECK(fixture.executeGetRequest() == true);
THEN("the controller should be executed"){
    CHECK(fixture.controller.getMethodCallsCount(HttpMethods::names::GET) == 0);
    CHECK(fixture.controller2.getMethodCallsCount(HttpMethods::names::GET) == 1);
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO


SCENARIO("CollectionControllerRouter should not execute when none controller match"
         , "[CollectionControllerRouter]")
{
    CollectionControllerRouterFixture fixture;
GIVEN("some registered controllers on CollectionControllerRouter"){
    fixture.setupControllers();
WHEN ("a request is done to one controller"){
    fixture.setupRequest(Constants::InvalidURL);
    string requestUrl = fixture.request.getUrl();
    CHECK(!fixture.controller.matchUrl(requestUrl));
    CHECK(!fixture.controller2.matchUrl(requestUrl));
    CHECK(!fixture.collectionControllerRouter.matchUrl(fixture.request.getUrl()));
    CHECK(fixture.executeGetRequest() == false);
THEN("None controller should be executed"){
    CHECK(fixture.controller.getMethodCallsCount(HttpMethods::names::GET) == 0);
    CHECK(fixture.controller2.getMethodCallsCount(HttpMethods::names::GET) == 0);
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO


SCENARIO("Controller with empty URL should be executed when requesting '/'"
         , "[CollectionControllerRouter]")
{
    CollectionControllerRouterFixture fixture;
GIVEN("some registered controllers on CollectionControllerRouter"){
    fixture.setupControllers();
    MockController emptyUrlController;
    fixture.collectionControllerRouter.addController(&emptyUrlController);
WHEN ("a request is done to root '/' "){
    fixture.setupRequest("/");
    string requestUrl = fixture.request.getUrl();
    CHECK(!fixture.controller.matchUrl(requestUrl));
    CHECK(!fixture.controller2.matchUrl(requestUrl));
    CHECK(emptyUrlController.matchUrl(requestUrl));
    CHECK(fixture.collectionControllerRouter.matchUrl(requestUrl));
    CHECK(fixture.executeGetRequest() == true);
THEN("None controller should be executed"){
    CHECK(fixture.controller.getMethodCallsCount(HttpMethods::names::GET) == 0);
    CHECK(fixture.controller2.getMethodCallsCount(HttpMethods::names::GET) == 0);
    CHECK(emptyUrlController.getMethodCallsCount(HttpMethods::names::GET) == 1);
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO

SCENARIO("CollectionControllerRouter should call all controllers that match, "
            "until the request has been handled" , "[CollectionControllerRouter]")
{
    CollectionControllerRouterFixture fixture;

    GIVEN("more than one controller that match the same URL"){
        DoNotHandleMockController controller1(Constants::ControllerURL);
        DoNotHandleMockController controller2(Constants::ControllerURL);
        MockController controller3(Constants::ControllerURL2);
        MockController controller4(Constants::ControllerURL);
        DoNotHandleMockController controller5(Constants::ControllerURL);

        fixture.registerController(&controller1);
        fixture.registerController(&controller2);
        fixture.registerController(&controller3);
        fixture.registerController(&controller4);
        fixture.registerController(&controller5);
    WHEN("some request is made to that URL"){
        fixture.request.setUrl(Constants::ControllerURL);
        fixture.request.setMethod("GET");
        bool handled = fixture.executeGetRequest();
    THEN("all controllers that match the request will be called until it has been handled"){
        CHECK(controller1.getMethodCallsCount(HttpMethods::names::GET) == 1);
        CHECK(controller2.getMethodCallsCount(HttpMethods::names::GET) == 1);
        CHECK(controller3.getMethodCallsCount(HttpMethods::names::GET) == 0);//other URL
        CHECK(controller4.getMethodCallsCount(HttpMethods::names::GET) == 1);
        CHECK(controller5.getMethodCallsCount(HttpMethods::names::GET) == 0);//request was handled before
        CHECK(handled == true);
    }//THEN
    }//GIVEN
    }//GIVEN
}
