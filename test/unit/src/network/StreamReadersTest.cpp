#include "catch.hpp"

#include "civetweb.h"
#include "CivetServer.h"

#include <iostream>
#include <string>

#include "network/readers/StreamReader.hpp"
#include "helpers/StringStreamReader.h"
#include "network/Request.h"
#include "network/StatusResponses.h"
#include "network/civetweb/CivetRequest.h"
#include "network/civetweb/CivetWebReader.h"
#include "network/civetweb/CivetWebControllerAdapter.h"
#include "network/readers/BufferedStreamReader.h"
#include "network/readers/DelimitedStreamReader.h"

#include "mocks/MockController.h"
#include "mocks/MockResponse.h"
#include "mocks/MockRequest.hpp"

using namespace std;
using namespace calmframe::network;
using namespace calmframe;

SCENARIO("streambuf", "[NetworkStreamBuffer]"){
    WHEN("something"){
        string strToRead("Lorem_ipsum");
        BufferedStreamReader streamBuffer(std::make_shared<StringStreamReader>(&strToRead));
        istream inputStream(&streamBuffer);

        string str;
        inputStream >> str;

        CHECK(str.size() == strToRead.size());
        CHECK(str.compare(strToRead) ==0);
    }
}


SCENARIO("delimiterStreamReader", "[DelimiterStreamReader]"){
    GIVEN("some data to Read"){
        stringstream dataToReadStream{"Lorem ipsum dol dolar doolordolor sit amet."};
        auto dataToRead = dataToReadStream.str();
        
    GIVEN("an DelimiterStreamReader"){
        DelimitedStreamReader delimiterStreamReader{&dataToReadStream};

        GIVEN("some delimiter present on data"){
            string delimiter = "dolor";
            delimiterStreamReader.setDelimiter(delimiter);
            int delimiterStart = dataToRead.find(delimiter);
            REQUIRE(delimiterStart != string::npos);
            WHEN("reading the data with the delimiterStreamReader"){
                int bufferSize = dataToRead.size() + 5;
                char * buffer[bufferSize];
                size_t readedCount = delimiterStreamReader.read((char*)buffer, (streamsize)bufferSize);
            THEN("it should read until the last character before the delimiter"){
                REQUIRE(readedCount > 0);
                string readedData((const char *)buffer, readedCount);
                CHECK(readedData == dataToRead.substr(0, delimiterStart));
            }
            }
        }
        
        GIVEN("some delimiter not contained on data"){
            string delimiter = "inexistest delimiter";
            delimiterStreamReader.setDelimiter(delimiter);
            REQUIRE(dataToRead.find(delimiter) == string::npos);
            WHEN("reading the data with the delimiterStreamReader"){
                string readedData;

                auto readedCount = delimiterStreamReader.read(readedData);
            THEN("it should read all the data"){
                REQUIRE(readedCount > 0);
                CHECK(readedCount == dataToRead.size());
                CHECK(readedData == dataToRead);
            }
            }
        }
    }

    }

}

#define REQUEST_CONTENT \
    "GET /photo HTTP/1.1" "\r\n" \
     "User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3" "\r\n"\
     "Accept-Language: en, mi""\r\n" \
     "\r\n"

SCENARIO("CivetWebReader", "[NetworkStreamBuffer][slow]"){
    GIVEN("some CivetServer running locally"){
        const char * portOption = "8765";
        int portNumber = 8765;
        const char * options[] = { "listening_ports", portOption, 0
                                 };
        CivetServer civetServer(options);
        if(civetServer.getContext() == NULL){
            FAIL("Could not start server. Maybe this port is in use.");
        }
        const char * CONTROLLER_URL = "/";
    GIVEN("Some Web Controller attached to server"){
        const string responseMessage =
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                "Proin at urna vulputate, dictum nisi blandit, sollicitudin magna."
                "Nunc blandit eros eu ante semper, in dictum nunc pretium. "
                "Suspendisse vehicula urna quis feugiat posuere. Cras vel urna "
                "elementum, accumsan quam vitae, gravida ante. Interdum et malesuada "
                "tristique leo. ";
        MockController controller;
        controller.setStatusResponse(HttpMethods::names::GET,StatusResponses::BAD_REQUEST);
        controller.setResponseEnabled(true);
        controller.setResponse(HttpMethods::names::GET, responseMessage);

        CivetWebControllerAdapter controllerAdapter{&controller};
        civetServer.addHandler("/", &controllerAdapter);

    GIVEN("some connection to that controller"){
        int bufferSize = 70;
        char buffer[bufferSize+1];
        buffer[0] = 0;
        buffer[bufferSize] = 0;

        mg_connection * conn = NULL;
        const char * host = "127.0.0.1";

        WHEN("GET to that controller"){
            conn = mg_download(host, portNumber,0,buffer, bufferSize, REQUEST_CONTENT);


            string message = conn == NULL
                                        ? (string("error: ") + string(buffer))
                                        : "Successfull connection";
            INFO(message.c_str());

            REQUIRE(conn != NULL);

            AND_WHEN("reading the response using a CivetWebReader"){
                stringstream responseBodyStream;
                CivetWebReader reader(conn);

                int readed = -1;
                do{
                    readed = reader.read(buffer, bufferSize);
                    if(readed > 0){
                        responseBodyStream << std::string(buffer, readed);
                    }
                }while(readed > 0 && readed != streambuf::traits_type::eof());

                THEN("it should read the message body returned from the handler"){
                    INFO("responseBodyStream: " << responseBodyStream.str());
                    CHECK(responseBodyStream.str().empty() == false);
                    CHECK(responseBodyStream.str() == controller.getResponse(HttpMethods::names::GET));
                }
            }
            AND_WHEN("reading the response using a CivetWebReader embedded in a istream"){
                BufferedStreamReader buffer(std::make_shared<CivetWebReader>(conn));
                istream networkInputStream(&buffer);

                THEN("it should allow copy the reader stream to anoter"){
                    stringstream responseBodyStream;
                    responseBodyStream << networkInputStream.rdbuf();
                AND_THEN("the copy result should be equals to the message body"){
                    CHECK(responseBodyStream.str() == controller.getResponse(HttpMethods::names::GET));
                }
                }
            }
            AND_WHEN("reading the response embedded in a Request class"){
                CivetRequest request(conn);

                THEN("it should allow copy the reader stream to another"){
                    stringstream responseBodyStream;
                    responseBodyStream << request.getContent().rdbuf();

                    INFO(request.getContent().tellg());
                    CHECK(request.getContent().tellg() == controller.getResponse(HttpMethods::names::GET).size());
                    CHECK(!request.getContent().fail());
                    int responseSize = controller.getResponse(HttpMethods::names::GET).size();
                    int pos = request.getContent().tellg();
                    CHECK(pos == responseSize);
                AND_THEN("the copy result should be equals to the message body"){
                    CHECK(responseBodyStream.str() == controller.getResponse(HttpMethods::names::GET));
                }
                }
            }
            WHEN("read the connection using a BufferedConnection embed in a inputstream"){
                auto readerPtr = std::make_shared<CivetWebReader>(conn);
                BufferedStreamReader bufferedConnection(readerPtr, 100);
                istream stream(&bufferedConnection);
                WHEN("there is no delimiter"){
                    THEN("it should allow read all the message"){
                        ostringstream out;
                        out << stream.rdbuf();
                    AND_THEN("the copy result should be equals to the message body"){
                        CHECK(out.str() == controller.getResponse(HttpMethods::names::GET));
                    }
                    }
                }
                WHEN("there is an existent delimiter"){
                    string delimiter = "ipsum";
                    int pos = controller.getResponse(HttpMethods::names::GET).find(delimiter);
                    REQUIRE(pos != string::npos);

                    bufferedConnection.setDelimiter(delimiter);
                    THEN("it should stop reading before the delimiter"){
                        ostringstream out;
                        out << stream.rdbuf();
                    AND_THEN("the copy result should be equals to the message body part before the delimiter"){
                        CHECK(out.str() == controller.getResponse(HttpMethods::names::GET).substr(0,pos));
                    }
                    }
                }
                WHEN("there is an delimiter that does not exist on data"){
                    const string & controllerResponse = controller.getResponse(HttpMethods::names::GET);
                    //Forçando  falso positivo no fim da stream
                    string delimiter = controllerResponse.substr(controllerResponse.size() - 10);
                    delimiter.append("Does not exist");
                    REQUIRE(controllerResponse.find(delimiter) == string::npos);

                    bufferedConnection.setDelimiter(delimiter);
                    THEN("it should read all the content"){
                        ostringstream out;
                        out << stream.rdbuf();
                    AND_THEN("the copy result should be equals to the message body"){
                        CHECK(out.str() == controller.getResponse(HttpMethods::names::GET));
                    }
                    }
                }
                WHEN("the delimiter is equals to the message"){
                    string delimiter = controller.getResponse(HttpMethods::names::GET);
                    bufferedConnection.setDelimiter(delimiter);
                    THEN("it should read nothing"){
                        ostringstream out;
                        out << stream.rdbuf();
                        INFO("Readed data: " << out.str());
                        CHECK(out.str().empty());
                    }
                }

                WHEN("reading part of the data"){
                    int bytesToRead = 10;
                    AND_WHEN("exist an delimiter partially contained on that part"){
                        int delimiterStart = bytesToRead /2;
                        string delimiter = responseMessage.substr(delimiterStart, bytesToRead);
                        bufferedConnection.setDelimiter(delimiter);
                        INFO("delimiter: \"" << delimiter <<"\"");
                        REQUIRE(responseMessage.find(delimiter) != string::npos);

                        THEN("the bufferedConnection should not read the delimiter part"){
                            char buffer[bytesToRead];
                            stream.read(buffer, bytesToRead);
                            int readedBytes = stream.gcount();
                            REQUIRE(readedBytes > 0);
                            CHECK(string(buffer, readedBytes) == responseMessage.substr(0,delimiterStart));
                        }
                    }
                }

                WHEN("calling readUntilFind"){
                    string search;
                    string readedData;
                    AND_WHEN("the string to search exist on data"){
                        search = responseMessage.substr(responseMessage.size()/2, 10);
                        int foundPos = responseMessage.find(search);
                        REQUIRE(foundPos != string::npos);

                        bool found = bufferedConnection.readUntilFind(search,&readedData);
                    THEN("it should found the string"){
                        CHECK(found == true);
                    AND_THEN("the data readed should include all the content before the search string"){
                        CHECK(readedData == responseMessage.substr(0, foundPos));
                    }
                    }
                    }
                }

            }
        }
    }
    }
        civetServer.close();
    }
}

