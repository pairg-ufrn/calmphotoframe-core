#include "TestMacros.h"
#include "steps/MultipartRequestSteps.h"

#include "civetweb.h"
#include "CivetServer.h"

#include <iostream>
#include <string>
#include <memory>
#include <sstream>

#include "network/ContentTypes.hpp"
#include "network/civetweb/CivetRequest.h"
#include "network/civetweb/CivetWebControllerAdapter.h"

#include "utils/CommonExceptions.h"

#include "mocks/MockController.h"
#include "mocks/MockStreamController.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::network;
using namespace calmframe::utils;

#define REQUEST_CONTENT \
    "GET / HTTP/1.1" "\r\n" \
     "\r\n"

#define FIXTURE MultipartRequestFixture
#define TEST_TAG "[MultipartRequest],[network]"

class MultipartRequestFixture : public MultipartRequestSteps{
public:
    static constexpr const char * CONTROLLER_URL = "/";
        
public: //tests
    void testReadMultipartRequest();

public: //steps
    ~MultipartRequestFixture();

    void given_a_valid_request_to_a_multipart_content();

    void given_a_server_running_locally();
    void given_a_controller_attached_to_server();
    void given_that_this_controller_respond_with_a_multipart_message();
    void given_this_controller_receives_a_connection();
    
protected:
    CivetServer & getServer();
    
public:
    MockStreamController controller;
    shared_ptr<istream> multipartContent;
    shared_ptr<CivetServer> server;
    shared_ptr<CivetWebControllerAdapter> civetAdapter;
    
    int serverPort=-1;  
    
    vector<char> errorbuffer;
    mg_connection * conn = NULL;

    shared_ptr<CivetRequest> request;
};

TEST_SCENARIO("Handling MultipartRequests") IS_DEFINED_BY(testReadMultipartRequest)

/*******************************************************************************************************/

void MultipartRequestFixture::testReadMultipartRequest(){
    GIVEN("a request with a multipart content"){
        given_a_valid_request_to_a_multipart_content();
        check_request_has_multipart_content_type(request.get(), BOUNDARY);
    
    WHEN("starting the multipart request reader from that request"){
        when_starting_multipart_request_reader(request.get());

    WHEN("reading each part"){
        when_reading_all_parts();
    
    THEN("all parts should be read"){
        check_all_parts_readed();
    
    THEN("each part should have the correct headers and contents"){
        check_parts_headers();
        check_parts_bodys();
        
    }//THEN
    }//THEN
    }//WHEN
    }//WHEN
    }//GIVEN
}

/**************************************** steps ******************************************************/

void MultipartRequestFixture::given_a_valid_request_to_a_multipart_content(){
    given_a_server_running_locally();
    given_a_controller_attached_to_server();
    given_that_this_controller_respond_with_a_multipart_message();
    given_this_controller_receives_a_connection();  
    
    request = std::make_shared<CivetRequest>(conn);  
}

void MultipartRequestFixture::given_a_server_running_locally(){
    const char * portOption = "0";
    const char * options[] = { "listening_ports", portOption, 0
                             };


    server = std::make_shared<CivetServer>(options);
    
    if(server->getContext() == NULL){
        FAIL("Could not start server. Maybe this port is in use.");
    }

    int ports[1], sslPorts[1];
    int numberPorts = mg_get_ports(server->getContext(),1,ports, sslPorts);
    REQUIRE(numberPorts >= 1);
    
    int portNumber = ports[0];
    INFO("Listen to port: " << portNumber);
    
    this->serverPort = portNumber;
}

void MultipartRequestFixture::given_a_controller_attached_to_server(){
    civetAdapter = std::make_shared<CivetWebControllerAdapter>(&controller);
    getServer().addHandler(CONTROLLER_URL, civetAdapter.get());
}

void MultipartRequestFixture::given_that_this_controller_respond_with_a_multipart_message(){
    ContentType messageContentType = ContentTypes::MULTIPART_FORM;
    messageContentType.putParameter("boundary", BOUNDARY);

    controller.putHeader(messageContentType);
    
    multipartContent = std::make_shared<stringstream>(MULTIPART_CONTENT);
    REQUIRE(multipartContent->good());

    controller.setResponseStream(multipartContent.get());
}

void MultipartRequestFixture::given_this_controller_receives_a_connection(){
    errorbuffer.resize(100);
    errorbuffer[0] = 0;

    const char * host = "127.0.0.1";

    conn = mg_download(host, this->serverPort, 0, errorbuffer.data(), errorbuffer.size(), REQUEST_CONTENT);
    
    if(conn == NULL){
        INFO("error: " << (const char *)errorbuffer.data());
    }
    REQUIRE(conn != NULL);
}

/*****************************************************************************************************/

MultipartRequestFixture::~MultipartRequestFixture(){
    if(server){
        server->close();
    }
    
    if(conn != NULL){
        mg_close_connection(conn);
        conn = NULL;
    }
}

CivetServer &MultipartRequestFixture::getServer(){
    if(!server){
        throw CommonExceptions::NullPointerException("server is null");
    }
    return *server.get();
}
