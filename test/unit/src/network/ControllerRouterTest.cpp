#include "catch.hpp"

#include "network/controller/ControllerRouter.h"
#include "network/Url.h"

#include "mocks/MockController.h"
#include "mocks/MockRequest.hpp"
#include "mocks/MockResponse.h"

#include "helpers/WebControllerFixture.h"
#include "TestMacros.h"

#include <string>

using namespace std;
using namespace calmframe;
using namespace calmframe::network;

#define CONTROLLER_URL "/test"
#define CONTROLLER_SUBURL CONTROLLER_URL "/otherTest"

class ControllerRouterFixture : public WebControllerFixture{
public:
    MockController mockController;
    ControllerRouter controllerRouter;

    ControllerRouterFixture()
        :mockController(CONTROLLER_URL)
    {
        controllerRouter.setBaseUrl("/");
        setController(&controllerRouter);
    }

    void onRequest(const std::string & method, const std::string & url){
        request.setUrl(url);
        request.setMethod(method);

        controllerRouter.onRequest(request, response);
    }
};
typedef ControllerRouterFixture Fixture;

#define FIXTURE ControllerRouterFixture
#define TEST_TAG "[ControllerRouter]"

class CaptureUrlMock : public MockController{
public:
    string url;

    bool onRequest(Request &request, Response &response){
        url = request.getUrl();
        return MockController::onRequest(request, response);
    }
};

FIXTURE_SCENARIO("ControllerRouter redirection"){

GIVEN(" a controller router with some WebController as child"){
    controllerRouter.add(CONTROLLER_URL, &mockController);
    REQUIRE(mockController.getBaseUrl() == string(CONTROLLER_URL));
    REQUIRE(controllerRouter.countControllers() == 1);


WHEN("calling to WebController URl from router"){
//    request.setUrl(CONTROLLER_URL);
//    controllerRouter.onGet(request, response);
    onRequest("GET", CONTROLLER_URL);

    THEN("WebController should be called"){
        CHECK(mockController.getMethodCallsCount(HttpMethods::names::GET) == 1);
    }
    mockController.resetMethodCallsCount(HttpMethods::names::GET);
}//WHEN

AND_WHEN("calling router with webController 'subUrl''"){
    mockController.resetCallsCount();
    onRequest("GET", CONTROLLER_SUBURL);

    THEN("webController should be executed"){
        CHECK(mockController.getMethodCallsCount(HttpMethods::names::GET) == 1);
    }
    mockController.resetCallsCount();
}//WHEN
AND_WHEN("calling other http methods"){
    mockController.resetCallsCount();
    request.setUrl(CONTROLLER_SUBURL);
    onRequest("POST", CONTROLLER_SUBURL);
    onRequest("PUT", CONTROLLER_SUBURL);
    onRequest("DELETE", CONTROLLER_SUBURL);

    THEN("webController should be executed"){
        CHECK(mockController.getMethodCallsCount(HttpMethods::names::POST) == 1);
        CHECK(mockController.getMethodCallsCount(HttpMethods::names::PUT) == 1);
        CHECK(mockController.getMethodCallsCount(HttpMethods::names::DELETE) == 1);
    }
    mockController.resetCallsCount();
}//WHEN
}//GIVEN

}//SCENARIO

FIXTURE_SCENARIO("ControllerRouter should support 'template' routes"){

GIVEN("some route with 'parameter paths' (ex: /:parameter)"){
    string controllerRoute = "/path/:param1/otherpath/:param2";

GIVEN("some controller registered with that route"){
    controllerRouter.add(controllerRoute, &mockController);

WHEN("an request is made that matches this route"){
    string realParam1 = "realParam1", realParam2 = "other";

    executeGetRequest(route("path", realParam1, "otherpath", realParam2));

THEN("the controller will be called"){
    CHECK(mockController.getMethodCallsCount(HttpMethods::names::GET) == 1);

THEN("the real parameter values should be put as parameters on the request"){
    CHECK(request.getParameter("param1") == realParam1);
    CHECK(request.getParameter("param2") == realParam2);

}//THEN
}//THEN
}//WHEN
}//GIVEN
}//GIVEN

}//SCENARIO

FIXTURE_SCENARIO("Controller router should match controllers based on the order that they were added"){

GIVEN("some controllers registered with urls that contains the same prefix"){
    string prefix = "/some/path";
    string compl1 = "/specific/path";
    string compl2 = compl1 + "/more/specific/path";

    MockController ctrl1, ctrl2, ctrl3;

    controllerRouter.add(prefix, &ctrl1);
    controllerRouter.add(prefix + compl2, &ctrl2);
    controllerRouter.add(prefix + compl1, &ctrl3);

    ctrl1.setHandleMethod("GET", false);

WHEN("an request is made that starts with that prefix"){
    executeGetRequest(prefix + compl2);

THEN("the request should be redirected to controller added first until someone return true"){
    CHECK(ctrl1.getMethodCallsCount(HttpMethods::names::GET) == 1);
    CHECK(ctrl2.getMethodCallsCount(HttpMethods::names::GET) == 1);
    CHECK(ctrl3.getMethodCallsCount(HttpMethods::names::GET) == 0);
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO

FIXTURE_SCENARIO("Controller router should forward relative requests to controllers"){

GIVEN("some controller registered with a given route on a ControllerRouter"){
    string route = "/any/route";
    CaptureUrlMock mockController;
    controllerRouter.add(route, &mockController);

WHEN("a request is made that matches that route"){
    string subpath = "/some/subpath";
    executeGetRequest(route + subpath);

THEN("that controller should be called with a url relative to the registered route"){
    CHECK(mockController.getMethodCallsCount(HttpMethods::names::GET) == 1);
    CHECK(mockController.url == subpath);

THEN("after the request, the url should be back to the initial value"){
    CHECK(request.getUrl() == route + subpath);

}//THEN
}//THEN
}//WHEN
}//GIVEN

}//SCENARIO

FIXTURE_SCENARIO("ControllerRouter should match a url only when at least one of his controllers match"){

GIVEN("some controllers registered on a ControllerRouter"){
    MockController ctrl1, ctrl2;
    string url1 = "/path1", url2 = "/path2";

    controllerRouter.add(url1, &ctrl1);
    controllerRouter.add(url2, &ctrl2);

    WHEN("a request is made to this router that does not match any controller"){
    THEN("it should not match the url"){
        CHECK(controllerRouter.matchUrl("/otherpath") == false);
    }//THEN
    }//WHEN

    WHEN("a request is made that match one of router's children"){
    THEN("it should match the url"){
        CHECK(controllerRouter.matchUrl(url1) == true);
    }//THEN
    }//WHEN

}//GIVEN


}//SCENARIO
