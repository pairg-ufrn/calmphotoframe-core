#include "catch.hpp"

#include "network/Response.h"

using namespace calmframe::network;

SCENARIO("Response ContentType", "[Response][ContentType]"){

    GIVEN("Some Response instance"){
        Response response(NULL,NULL);
        WHEN("call getContentType"){
            THEN("it should be text/plain"){
                INFO("Name: " << response.getContentType().getName()
                        << "; Value: " << response.getContentType().getValue()
                        << "; Type: " << response.getContentType().getType()
                        << "; Subtype: " << response.getContentType().getSubtype());
                CHECK(response.getContentType().getType() == "text");
                CHECK(response.getContentType().getSubtype() == "plain");
            }

        AND_WHEN("call getHeader(\"Content-Type\") it should be the same as getContentType"){
            const Header & header = response.getHeader("Content-Type");
            CHECK(&response.getContentType() == &header);
            CHECK(response.getContentType().getValue() == header.getValue());
        }
        }

        WHEN("call putHeader(\"Content-Type\") it should change the contentType value"){
            response.putHeader(Header("Content-Type", "application/json"));
            CHECK(response.getContentType().is("application","json"));
        }
    }
}
