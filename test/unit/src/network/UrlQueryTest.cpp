#include "TestMacros.h"

#include "network/UrlQuery.h"

#define TEST_TAG "[UrlQuery]"
#define FIXTURE UrlQueryFixture

using namespace std;
using namespace calmframe;
using namespace calmframe::network;

class UrlQueryFixture{
public:
    static constexpr const char * KEY = "key";
    static constexpr const char * VALUE = "value";
};

TEST_SCENARIO("Add query parameters"){
    UrlQuery query;
    
    CHECK(query.empty());
    
    query.add(KEY, VALUE);
    
    CHECK_FALSE(query.empty());
    CHECK(query.contains(KEY));
}

TEST_SCENARIO("Remove query parameter"){
    UrlQuery query({{KEY, VALUE}});
    
    CHECK(query.contains(KEY));
    
    query.remove(KEY);
    
    CHECK_FALSE(query.contains(KEY));
}

TEST_SCENARIO("Get query parameter value"){
    UrlQuery query({{KEY, VALUE}});
    
    CHECK(query.get(KEY) == VALUE);
    CHECK(query.get("other", VALUE) == VALUE);
    CHECK(query.get("other") == string());
}

TEST_SCENARIO("Conversion to string"){
    UrlQuery query({{KEY, VALUE}});
    
    CHECK(UrlQuery({{KEY, VALUE}}).toString() == (string(KEY).append("=").append(VALUE)));
    CHECK(UrlQuery({{"a", "1"}, {"b", "2"}, {"c", "3"}}).toString() == "a=1&b=2&c=3");
}

TEST_SCENARIO("Conversion from string"){
    UrlQuery query;
    
    query.set("a=1&b=2&c=3");
    
    CHECK(query.get("a") == "1");
    CHECK(query.get("b") == "2");
    CHECK(query.get("c") == "3");
}

TEST_SCENARIO("Clear query"){
    UrlQuery query({{KEY, VALUE}});
    
    CHECK_FALSE(query.empty());
    CHECK(query.clear().empty());
}
