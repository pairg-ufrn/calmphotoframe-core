#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include "utils/Log.h"
#include "helpers/Timer.h"

using namespace calmframe;

class CatchLog : public LogChannel{
public:
    virtual void log(LogLevel::Level , const std::string & msg) override{
        INFO("Log: " << msg);
    }
};

void quiet(Catch::ConfigData &){
    MainLog().setStdoutEnabled(false);
}

int main( int argc, char* const argv[] ){
    CatchLog logChannel;
    Timer timer;

    //TODO: allow define log level from command line
    MainLog().setLogLevel(calmframe::LogLevel::Debug);
//    MainLog().setStdoutEnabled(false);
//    MainLog().addChannel(&logChannel);

    Catch::Session session; // There must be exactly once instance

    auto & cli = (Catch::Clara::CommandLine<Catch::ConfigData> &)session.cli();
    
    cli["-q"]["--quiet"]
        .describe("Disable application logs in tests")
        .bind(quiet);
        
    int returnCode = session.applyCommandLine( argc, argv );
    if( returnCode == 0 ){ // returnCode != 0 indicates a command line error
        returnCode = session.run();
    }

    std::cout << "Test time: " << timer.elapsed() << " s" << std::endl;

    return returnCode;
}
