#ifndef TESTMACROS_H
#define TESTMACROS_H

#include "catch.hpp"

using namespace std;

#define FIXTURE_SCENARIO(msg, ...) \
		SCENARIO_METHOD(FIXTURE, msg, TEST_TAG __VA_ARGS__)

#define FIXTURE_SCENARIO_METHOD(msg, method, ...) \
		FIXTURE_SCENARIO(msg, __VA_ARGS__){\
			method(); \
		}

#define F_SCENARIO_METHOD FIXTURE_SCENARIO_METHOD

#define TEST_METHOD FIXTURE_SCENARIO_METHOD

#define TEST_SCENARIO(...) FIXTURE_SCENARIO(__VA_ARGS__)

#define IS_DEFINED_BY(method) { method(); }

#endif // TESTMACROS_H

