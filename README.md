# Calm Photo Frame - Core

O Calm Photo Frame é um sistema open-source que permite criar um porta-retrato digital.

O sistema possui duas partes principais: o **Core**(núcleo) e a view.
Se você quer saber mais detalhes sobre o sistema e o projeto como um todo visite [a página do projeto][calmphotoframe-page].

[calmphotoframe-page]: https://code.google.com/p/calmphotoframe/ "Página do Calm Photo Frame"

## Sobre o Core

O Core do Calm Photo Frame provê os serviços para manipular fotos e as descrições
associadas a elas. Ele utiliza uma api RESTful para oferecer estes serviços à(s) view(s).

O **core** pode tanto ser embutido em uma aplicação junto à view (para gerar um porta-retrato) ou ser executado *standalone* (funcionando como um serviço).

## Compilando

Este projeto utiliza o sistema de build [waf](https://github.com/waf-project/waf). Este sistema permite escrever o script da build em python (ver arquivo [wscript](wscript)). Além disto, o executável pode ser distribuído junto ao projeto (ver arquivo [waf](waf)).

Para compilar o CalmPhotoFrame-Core utilizando o waf basta executar o comando:

    ./waf configure build

Para isto é preciso ter uma versão do python instalada e um compilador C++.
Waf deve reconhecer o compilador automaticamente (para maiores informações
ver a [documentação deles](https://waf.io/book/#_c_and_c_projects)). Atualmente o projeto foi testado apenas com o GCC.

Ao compilar o projeto da primeira vez ele irá compilar as dependências do projeto (diretório libs), o que pode levar um tempo.

Os arquivos compilados serão gerados no diretório de build (*build* por padrão). Para compilar também os testes adicione a opção **--with-tests** durante a compilação.

### Compilando para Android

É possível compilar o core para ser utilizado em uma aplicação Android, através do [Android NDK](https://developer.android.com/tools/sdk/ndk/index.html).

Para isto é preciso primeiro gerar uma toolchain do android (para isto veja a documentação embutida com a android NDK).

O comando a seguir irá compilar o *Core* utilizando a toolchain localizada em */path/to/android/toolchain*

    ./waf configure build --target=android --toolchain-path=/path/to/android/toolchain

Em seguida para gerar uma biblioteca no diretório *dist* é possível utilizar o seguinte comando:

    ./waf install --destdir=dist --target=android

### Executando Calm Photo

A compilação irá gerar um executável (*calmphotoframe-daemon*) no diretório de build.

Ele pode ser executado diretamente pela linha de comando. Para encerrá-lo utilize **Ctrl + C** (comando de interrupção).

Para executá-lo em background, utilize  opção **--daemon** na linha de comando. Neste caso, será preciso matar o processo iniciado para que o programa finalize.

Em ambos os casos o Calm Photo Frame utilizará o caminho em que foi iniciado no terminal como diretório raiz para execução. Caso nenhuma configuração seja informada, este diretório será utilizado para armazenar as imagens e dados do programa.

#### Configurações

Configurações podem ser definidas através de um arquivo (*conf.json*) no diretório raiz.
Os binários vem acompanhados de um arquivo com uma configurações básica. Abaixo estão listados os parâmetros configuráveis:

| *Parâmetro* | *Valor Padrão* | *Descrição* |
| --- | --- | --- |
| "data_dir"  |       "."      | Define o diretório em que serão armazenados os arquivos de dados da aplicação.                 |
| "image_dir" |       "."      | Define o diretório em que serão armazenadas as imagens inseridas no porta-retrato.             |
| "log_dir"   |       "."      | Diretório em que serão armazenados os arquivos de log do porta-retrato.                        |
| "log_file"  |"calmphotoframe.log"| Nome do arquivo de log do calm photo frame.|
| "tmp_dir"   | "/tmp" | Define localização do diretório temporário. |
| "port"      | 8081   | Porta TCP a ser utilizada pela aplicação.   |

Os diretórios podem ser caminhos absolutos ou relativos. Caminhos relativos serão definidos com base no *diretório raiz* (diretório em que o programa foi inicializado).

### Empacotando o projeto

O comando a seguir irá produzir pacotes (.zip) dos arquivos gerados no projeto, incluindo código-fonte.

    ./waf release

## Dependências e Bibliotecas Utilizadas

Para facilitar a distribuição e compilação do projeto, as principais dependências
são inclusas nele (ver pasta *libs*).
Isto é feito principalmente para facilitar a compilação do código Android.

São necessárias, no entanto, as seguintes dependências:

* **pthread**
* **dl**(*dynamic link library*)

Não é esperado que seja necessário a instalação de outras dependências. Se este não for o caso para você, entre em contato conosco, que atualizaremos nossas informações.

### Bibliotecas Utilizadas

As seguintes bibliotecas são utilizadas no projeto:

* [Poco Foundation][POCO] : provê suporte multiplataforma para tarefas diversas, como manipulação de arquivos, logging, hashing, threads e outras.
* [Poco Util][POCO]: uma biblioteca que faz parte do projeto POCO e provê suporte para criação de aplicações de linha de comando ou *daemons*.
* [civetweb](https://github.com/bel2125/civetweb): uma biblioteca *lightweight* que dá suporte à criação de servidores em C e C++.
* [FreeImage](http://freeimage.sourceforge.net/): uma biblioteca para manipulação de diversos formatos de imagens em C. O projeto utiliza o bind em C++ (FreeImagePlus) da biblioteca.
* [JsonCPP](https://github.com/open-source-parsers/jsoncpp): uma biblioteca em C++ para interagir com JSON.
* [Sqlite](https://www.sqlite.org/): uma biblioteca que implementa uma *engine* de banco de dados SQL auto-contida.
* [streamprintf](https://github.com/mmorearty/streamprintf): uma biblioteca simples (1 arquivo!) que permite utilizar o printf com strings e streams C++.
* [Catch](https://github.com/philsquared/Catch): um framework moderno e *header-only* em C++ para testes de unidade, TDD e BDD.

[POCO]: http://pocoproject.org/ "Poco Framework"

## Licença

O projeto é distribuído sobre a licença Apache 2.0.
Para mais detalhes consulte o arquivo LICENSE.txt ou o endereço: https://www.apache.org/licenses.
