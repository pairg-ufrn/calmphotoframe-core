#

TOP_PATH := $(call my-dir)

include $(TOP_PATH)/libs/PocoFoundation/Android.mk
include $(TOP_PATH)/libs/civetweb/Android.mk
include $(TOP_PATH)/libs/jsoncpp/Android.mk
include $(TOP_PATH)/libs/sqlite/Android.mk

LOCAL_PATH       := $(TOP_PATH)
include $(CLEAR_VARS)

# build app
include $(CLEAR_VARS)

INCLUDE_PATH := $(TOP_PATH)/src
SRC_DIR := $(TOP_PATH)/src

# Determine LOCAL_SOURCE_FILES
# Get all .cpp and .c files recursively
SRCS = $(shell find $(SRC_DIR) -name "*.cpp")
SRCS += $(shell find $(SRC_DIR) -name "*.c")

LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%, % , $(SRCS))
LOCAL_SRC_FILES += PhotoFrameControllerJNI.cpp


$(info "Local source files: $(LOCAL_SRC_FILES)")

LOCAL_LDLIBS := -llog
LOCAL_MODULE    := CalmPhotoFrame
LOCAL_C_INCLUDES := $(INCLUDE_PATH)
# LOCAL_SRC_FILES := PhotoFrameControllerJNI.cpp PhotoFrame.cpp Photo.cpp PhotoDescription.cpp PhotoHandler.cpp
LOCAL_STATIC_LIBRARIES := civetweb PocoFoundation jsoncpp sqlite3



include $(BUILD_SHARED_LIBRARY)
