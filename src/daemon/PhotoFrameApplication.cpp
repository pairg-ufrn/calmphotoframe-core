#include "PhotoFrameApplication.h"

#include "Configurations.h"
#include "Environment.h"
#include "JsonConfigurationLoader.h"
#include "Options.h"
#include "PhotoFrameService.h"

#include "data/Data.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"

#include "exceptions/DatabaseException.h"

#include "utils/Log.h"
#include "utils/FileUtils.h"
#include "utils/CommonExceptions.h"

#include <iostream>
#include <map>
#include <string>
#include <exception>

#include "Poco/Util/OptionException.h"
#include "Poco/Util/Validator.h"

EXCEPTION_CLASS(InitializationException);

namespace calmframe {

using namespace std;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace Poco;
using namespace Poco::Util;


namespace Constants {
    static const int STARTERROR_CODE   = 1;
    static const int RUNERROR_CODE = 2;

    static const char *  START_ERROR_MSG      = "Could not start application due to an unexpected error.";
    static const char *  RUN_ERROR_MSG        = "Application was finalized due to an unexpected error.";
    static const char *  CAUGHT_EXCEPTION_MSG = "Caught exception: ";
}

static
string getStartDir(const string & startDir){
    return startDir.empty() ? FileUtils::instance().getCurrentDir() : startDir;
}

class LogLevelValidator: public Poco::Util::Validator{
public:
    void validate(const Option& option, const std::string& value){
        LogLevel::Level logLevel = LogLevel::fromString(value);
        if(logLevel == LogLevel::Null){
            throw Poco::Util::OptionException("Could not parse log level option");
        }
    }
};

PhotoFrameApplication::PhotoFrameApplication(const string &startDir)
    : startDir(getStartDir(startDir))
{}

void PhotoFrameApplication::defineOptions(OptionSet &optionSet){
    super::defineOptions(optionSet);

    optionSet.addOption(Option().fullName(Options::LOG_LEVEL).required(false).argument("log-level")
                                .description("Specify the minimum log level that will be registered.")
                                .validator(new LogLevelValidator()));
    //TODO: definir opções suportadas via linha de comando
}

void PhotoFrameApplication::handleOption(const string &name, const string &value){
    super::handleOption(name, value);
    getConfigurations().put(name, value);
}

void PhotoFrameApplication::initialize(Application &self){
    try{
        setupConfigurations(getConfigurations());
        setupBaseDir();
        setupLog();
        MainLog().info("Initializing PhotoFrameApplication");

        setupData(getConfigurations());
        if(!getPhotoFrameService().startService(getConfigurations())){
            string port = getConfigurations().get(Options::PORT, Options::Defaults::PORT);
            string errMsg = string("Failed to initialize Photo Frame Service ")
                                .append("on port ").append(port).append(". ")
                                .append("Maybe this port is already in use.");
            throw InitializationException(errMsg);
        }
    }
    catch(const DatabaseException & ex){
        const char * err = "Failed to start application due to database error";
        logError(err);
        throw InitializationException(err, ex);    
    }
    catch(std::exception & ex){
        logError("%s", Constants::START_ERROR_MSG);
        logError("%s %s", Constants::CAUGHT_EXCEPTION_MSG, ex.what());
        throw;
    }
    catch(...){
        logError("%s", Constants::START_ERROR_MSG);
        throw;
    }
}


int PhotoFrameApplication::main(const std::vector<std::string> &args){
    waitForTerminationRequest();
    return EXIT_OK;
}

void PhotoFrameApplication::uninitialize(){
    MainLog().info("Finishing application");

    getPhotoFrameService().stopService();

    MainLog().info("Application finalized.");
    MainLog().info("Bye...");
}

Configurations &PhotoFrameApplication::getConfigurations(){
    return Configurations::instance();
}
PhotoFrameService &PhotoFrameApplication::getPhotoFrameService(){
    return this->photoFrameService;
}

void PhotoFrameApplication::setupConfigurations(Configurations & configurations){
    JsonConfigurationLoader jsonConfLoader;
    jsonConfLoader.setAllowOptionOverride(false);

    std::string baseDir = configurations.get(Options::BASE_DIR, startDir);
    std::string confFilename = configurations.get(Options::CONF_FILE, Options::Defaults::CONF_FILE);
    string confFilePath = Files().joinPaths(baseDir, confFilename);
    MainLog().debug("Loading configurations from file: " + confFilePath);
    jsonConfLoader.loadFromFile(confFilePath, configurations);
}

void PhotoFrameApplication::setupBaseDir(){
    string baseDir = getConfigurations().get(Options::BASE_DIR, startDir);
    if(!baseDir.empty()){
        Environment::instance().setBasePath(baseDir);
    }
    logInfo("PhotoFrameApplication:\t" "base dir = '%s'", Environment::instance().getBasePath().c_str());
}

const string &PhotoFrameApplication::getStartDirectory() const{
    return this->startDir;
}
void PhotoFrameApplication::setupLog(){
    //PocoApplication overrides changes on logger, so reset it
    MainLog().resetSetup();

    string logDir = Environment::instance().getLogDir();
    Files().createDirIfNotExists(logDir, true);

    string logFile = Environment::instance().getLogFile();
    MainLog().addLogFile("main", logFile);

    MainLog().setLogLevel(LogLevel::fromString(getConfigurations().get(Options::LOG_LEVEL), LogLevel::Information));
}

void PhotoFrameApplication::setupData(Configurations& configurations) {
    auto dataBuilder = std::make_shared<SqliteDataBuilder>(getDataDir(configurations),"",true);

    calmframe::data::Data::setup(dataBuilder);
}

std::string PhotoFrameApplication::getDataDir(Configurations & conf) const{
    string dataDir = Environment::instance().getDataDir();

    if(conf.exists(Options::DATA_DIR)){
        string dataDirConf = conf.get(Options::DATA_DIR, Options::Defaults::DATA_DIR);
        dataDir = Environment::instance().getPathFromBase(dataDirConf);
    }
    return dataDir;
}

}//namespace

