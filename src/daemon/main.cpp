#ifndef ANDROID

#include "PhotoFrameApplication.h"
#include "utils/Log.h"
using namespace calmframe;

int main(int argc, char ** argv){
    PhotoFrameApplication application;
    return application.run(argc, argv);
}

#endif
