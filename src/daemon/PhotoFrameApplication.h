#ifndef PHOTOFRAMEAPPLICATION_H
#define PHOTOFRAMEAPPLICATION_H

#include "Poco/Util/ServerApplication.h"
#include "PhotoFrameService.h"

#include <vector>
#include <string>

namespace Poco{
    class Channel;
}

namespace calmframe {

class Configurations;

using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::OptionSet;
using Poco::Channel;

class PhotoFrameApplication : public ServerApplication{
    typedef ServerApplication super;
private:
    PhotoFrameService photoFrameService;
    std::string startDir;
public:
    PhotoFrameApplication(const std::string & startDir="");

    virtual void initialize(Application &self);
    virtual void uninitialize();
    virtual void defineOptions(OptionSet & optionSet);
    virtual int main(const std::vector<std::string> &args);
    virtual void handleOption(const std::string & name, const std::string & value);

    virtual Configurations & getConfigurations();
    virtual PhotoFrameService & getPhotoFrameService();
    void setupLog();
    void setupBaseDir();
    const std::string & getStartDirectory() const;
    
public:    
    virtual void setupData(Configurations &configurations);
    virtual std::string getDataDir(Configurations &conf) const;

protected:
    virtual void setupConfigurations(Configurations &configurations);
};

}
#endif // PHOTOFRAMEAPPLICATION_H
