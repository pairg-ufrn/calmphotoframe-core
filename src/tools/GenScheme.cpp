#include "Poco/Util/Application.h"

#include "data/data-sqlite/SqliteData.h"
#include "data/data-sqlite/DatabaseReader.h"
#include "utils/ScopedDatabase.h"
#include "utils/FileUtils.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace Poco::Util;
using namespace calmframe;

class DumpReader : public DatabaseReader{
public:
    std::ostream * outStream;

    DumpReader(std::ostream * out) : outStream(out)
    {}

    void readRow(const QueryData & data) override{
        if(outStream != nullptr){
            const string sqlTxt = data.getString(0);
            if(sqlTxt.empty()){
                return;
            }

            *outStream << sqlTxt << ";" << endl;
        }
    }
};

class GenSchemeApplication : public Poco::Util::Application{
public:
    typedef Poco::Util::Application super;
public:
    GenSchemeApplication(int argc, char ** argv)
        : super(argc, argv)
    {}

    int main(const std::vector<string> &args){
        if(args.empty()){
            cout << "Usage: " << this->commandName() << " <outputfile>" <<endl;
            return EXIT_USAGE;
        }

        const std::string & dbFilename = args[0];

        fstream out(dbFilename.c_str(), ios_base::out);

        if(!out.good()){
            cerr << "Failed to open file " << dbFilename << endl;
            return EXIT_IOERR;
        }

        ScopedSqliteDatabase scopedDb;

        SqliteData * data = static_cast<SqliteData *>(scopedDb.getDataBuilder().buildData());

        DumpReader reader(&out);
        data->getDatabaseConnector().query(reader, "SELECT sql FROM sqlite_master;");

        return EXIT_OK;
    }
};

int main(int argc, char ** argv){
    GenSchemeApplication app(argc, argv);
    return app.run();
}
