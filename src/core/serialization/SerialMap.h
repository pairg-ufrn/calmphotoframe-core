#ifndef SERIALMAP_H
#define SERIALMAP_H

#include "Serializable.hpp"
#include "Serializer.h"

#include <map>

class SerialMap : public Serializable{
public:
    using Map = std::map<std::string, std::string>;
    Map properties;

    SerialMap(std::initializer_list<Map::value_type> values={})
        : properties(values)
    {}
    
    explicit SerialMap(const Map & properties) : properties(properties)
    {}

    virtual void serialize(Serializer & serializer) const{
        for (auto prop : properties) {
            serializer.put(prop.first, prop.second);
        }  
    }
};

#endif // SERIALMAP_H
