#ifndef GENERICFIELD_H
#define GENERICFIELD_H

#include <string>
#include "Serializable.hpp"
#include "Deserializable.hpp"
#include "Serializer.h"
#include "Deserializer.h"

template<typename T>
class GenericField : public Deserializable, public Serializable{
    std::string name; 
    T value;
    
public:
    GenericField(const std::string & fieldName) 
        : name(fieldName)
    {}
    GenericField(const std::string & fieldName, const T & value) 
        : name(fieldName)
        , value(value)
    {}

    std::string getName() const{
        return name;
    }
    void setName(const std::string & value){
        name = value;
    }

    const T & getValue() const{
        return value;
    }
    void setValue(const T & value){
        this->value = value;
    } 
    
    void deserialize(const GenericContainer & deserializer){
        if(deserializer.contains(name)){
            this->value = deserializer.get(name).as<T>();
        }
    }
    
    virtual void serialize(Serializer & serializer) const {
        serializer.put(name, value);
    }
};


#endif // GENERICFIELD_H
