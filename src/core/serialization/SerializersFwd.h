#ifndef SERIALIZERSFWD_H
#define SERIALIZERSFWD_H

class Deserializable;
class Serializable;
class Deserializer;
class Serializer;

#endif // SERIALIZERSFWD_H

