/*
 * Configurations.cpp
 *
 *  Created on: 19/11/2014
 *      Author: noface
 */

#include "Configurations.h"

#include "utils/StringCast.h"
#include "utils/ParseUtils.h"

using namespace std;

namespace calmframe {

using calmframe::utils::StringCast;

Configurations & Configurations::instance(){
	static Configurations singleton;
	return singleton;
}

Configurations::Configurations()
{}

Configurations::~Configurations()
{}

void Configurations::put(const std::string& key, const std::string& value) {
    this->configurations[key] = value;
}

void Configurations::put(const string & key, bool value){
    put(key, utils::StrCast().toString(value));
}

void Configurations::put(const string & key, int value){
    put(key, utils::StrCast().toString(value));
}

void Configurations::put(const string & key, long value){
    put(key, utils::StrCast().toString(value));
}

void Configurations::put(const string & key, double value){
    put(key, utils::StrCast().toString(value));
}

void Configurations::putIfNotExists(const string &key, const string &value){
    if(!exists(key)){
        put(key,value);
    }
}

const std::string & Configurations::get(const std::string& key, const string &defaultValue) const {
	map<string,string>::const_iterator it = this->configurations.find(key);
	if(it != configurations.end()){
		return it->second;
	}
    return defaultValue;
}

bool Configurations::getBoolean(const string &key, const bool &defaultValue) const{
    return ParseUtils::instance().toBoolean(get(key), defaultValue);
}

int Configurations::getInteger(const string &key, const int &defaultValue) const{
    return StringCast::instance().tryToInt(get(key), defaultValue);
}

long Configurations::getLong(const string &key, const long &defaultValue) const{
    return StringCast::instance().tryToLong(get(key), defaultValue);
}

double Configurations::getDouble(const string &key, const double &defaultValue) const{
    return StringCast::instance().tryToDouble(get(key), defaultValue);
}

bool Configurations::exists(const string &key) const {
    return this->configurations.find(key) != end();
}

Configurations::const_iterator Configurations::begin() const{
    return this->configurations.begin();
}
Configurations::const_iterator Configurations::end() const{
    return this->configurations.end();
}

Configurations::iterator Configurations::begin(){
    return this->configurations.begin();
}
Configurations::iterator Configurations::end(){
    return this->configurations.end();
}

} /* namespace calmframe */

