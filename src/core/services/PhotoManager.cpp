#include "PhotoManager.h"

#include "services/PhotoCollectionManager.h"
#include "exceptions/NotFoundMember.h"

#include "data/Data.h"
#include "data/DataTransaction.h"

#include "image/ImageHelper.h"
#include "model/image/TemporaryImage.h"
#include "model/image/ScopedOpen.h"

#include "services/EventsManager.h"

#include "utils/Exception.hpp"
#include "utils/IOException.h"
#include "utils/IllegalArgumentException.hpp"
#include "utils/FileUtils.h"
#include "utils/StringCast.h"
#include "utils/Log.h"

#include <fstream>
#include <string>
#include <set>
#include <istream>

using namespace std;
using namespace calmframe::data;
using namespace calmframe::utils;

namespace calmframe {

using namespace CommonExceptions;

const long PhotoManager::INVALID_COLLECTION_ID    = PhotoCollection::INVALID_ID;
const long PhotoManager::INVALID_USER_ID          = Event::InvalidUserId;

namespace Constants{
    static const string THUMBNAIL_DIR_NAME = "thumbnails";
}
namespace Errors {
    static const string InvalidImage = "Invalid image";
    static const string InvalidImageContent = "Image content is not valid";
    static const string FailedImageCreation = "Could not create image file.";

    static string nonExistentContext(long ctxId){
        return string("Context with id ")
                        .append(StrCast().toString(ctxId))
                        .append(" does not exist.");
    }
    static string nonExistentPhoto(long photoId){
        return string("Photo with id ")
                        .append(StrCast().toString(photoId))
                        .append(" does not exist.");
    }
}//namespace
PhotoManager::~PhotoManager()
{}

const std::string &PhotoManager::getImagesDir() const{
    return this->imagesDir;
}

void PhotoManager::setImagesDir(const std::string &imagesDir){
    this->imagesDir = imagesDir;
}

Photo PhotoManager::getPhoto(unsigned photoId){
    Photo photo;
    if(!Data::instance().photos().getPhoto(photoId, photo)){
        throw NotFoundMember(Errors::nonExistentPhoto(photoId));
    }

    return photo;
}

Image PhotoManager::getImage(unsigned photoId){
    Photo photo = getPhoto(photoId);
    return getImage(photo);
}

Image PhotoManager::getImage(const Photo &photo)
{
    return Image(Files().getPathFromBase(photo.getImagePath(), getImagesDir()));
}

bool PhotoManager::existPhoto(unsigned photoId) const{
    return Data::instance().photos().exist(photoId);
}

Image PhotoManager::getThumbnail(unsigned photoId, int size)
{
    Photo photo = getPhoto(photoId);

    if(photo.getId() == Photo::UNKNOWN_ID){
        throw CommonExceptions::IllegalArgumentException("Photo id is invalid!");
    }

    Image thumbnailImage = getPhotoThumbnail(photo, size);
    if(thumbnailImage.isValid()){
        return thumbnailImage;
    }
    return createThumbnail(photo, size);
}


Image PhotoManager::getPhotoThumbnail(const Photo &photo, const int &size){
    string filePath = generateThumbnailPath(photo, size);
    return FileUtils::instance().exists(filePath) ?
                Image(filePath) : Image();
}
Image PhotoManager::createThumbnail(const Photo &photo, const int &size){
    string thumbnailPath = generateThumbnailPath(photo, size);

    Image photoImage = getImage(photo);

    return ImageHelper::instance().makeThumbnail(photoImage.getImagePath(), size, thumbnailPath);
}

string PhotoManager::generateThumbnailPath(const Photo &photo, const int &size) const{
    string thumbnailFilename = generateThumbnailFilename(photo.getImagePath(), size);
    return FileUtils::instance().joinPaths(getThumbnailsDir(), thumbnailFilename);
}

string PhotoManager::generateThumbnailFilename(const string &filepath, const int &size) const
{
    string filename, extension;
    FileUtils::instance().splitFilenameAndExtension(filepath,filename, extension);
    return generateThumbnailPrefix(filename).append(StringCast::instance().toString(size)).append(".").append(extension);
}

string PhotoManager::generateThumbnailPrefix(const string &filename) const{
    return filename + string("_thumbnail_");
}

std::vector<Photo> PhotoManager::getPhotos(const std::string & query){
    return Data::instance().photos().listAll(query);
}

std::vector<Photo> PhotoManager::getPhotosIn(const std::set<long> & photoIds, const std::string & query){
    return Data::instance().photos().listIn(photoIds, query);
}

void PhotoManager::getPhotoIdsInCollectionTree(long collId, set<long> & outPhotoIds) const
{
    map<long, PhotoCollection> collections;
    Data::instance().collections().getCollectionTree(collId, collections);

    map<long, PhotoCollection>::const_iterator iter = collections.begin();
    while(iter != collections.end()){
        const set<long> & collPhotoIds = iter->second.getPhotos();

        set<long>::const_iterator photoIter = collPhotoIds.begin();
        while(photoIter != collPhotoIds.end()){
            outPhotoIds.insert(*photoIter);
            ++photoIter;
        }
        ++iter;
    }
}

std::vector<Photo> PhotoManager::getPhotosInCollection(long collId, bool recursively, const std::string & query)
{
    set<long> photoIds;
    if(!recursively){
        photoIds = Data::instance().collections().getCollectionPhotos(collId);
    }
    else{
        getPhotoIdsInCollectionTree(collId, photoIds);
    }

    return getPhotosIn(photoIds, query);
}


PhotoManager::PhotosList PhotoManager::getPhotosInContext(long ctxId, const string & query){
    return Data::instance().photos().listByContext(ctxId);
}

long PhotoManager::getRootPhotoCollectionId(long contextId)
{
    PhotoContext ctx = Data::instance().contexts().get(contextId);
    if(ctx.getId() != contextId){
        throw IllegalArgumentException(Errors::nonExistentContext(ctx.getId()));
    }
    return ctx.getRootCollection();
}

Photo PhotoManager::updatePhoto(const Photo &photoToUpdate, long userId){
    return updatePhotoImpl(photoToUpdate, userId, true);
}

Photo PhotoManager::updatePhotoImpl(const Photo &photoToUpdate, long userId, bool logEvent)
{
    Photo photo;
    DataTransaction transaction(Data::instance());

    assertPhotoExist(photoToUpdate);

    if(Data::instance().photos().update(photoToUpdate)) //FIXME: if cannot update should throw exception
    {
        Data::instance().photos().getPhoto(photoToUpdate.getId(), photo);

        if(logEvent){
            logPhotoEvent(EventType::PhotoUpdate, photo.getId(), userId);
        }

        transaction.commit();
    }

    return photo;
}

Photo PhotoManager::removePhotoAndImage(long photoId)
{
    Photo photo = getPhoto(photoId);
    if(photo.getId() != photoId){
        throw NotFoundMember(Errors::nonExistentPhoto(photoId));
    }
    if(!Data::instance().photos().remove(photo)){
        logError("Error! Could not delete photo %ld from db", photoId);
        throw IOException("Could not remove photo");
    }
    //Remove imagem
    tryDeleteImage(photo);

    return photo;
}

Photo PhotoManager::deletePhoto(long photoId, long userId){
    DataTransaction transaction(Data::instance());

    Photo photo = removePhotoAndImage(photoId);

    Data::instance().collections().removePhotoFromCollections(photo.getId());
    logPhotoEvent(EventType::PhotoRemove, photo.getId(), userId);

    transaction.commit();

    return photo;
}

void PhotoManager::tryDeleteImage(Photo photo){
    if(FileUtils::instance().exists(photo.getImagePath())
          && !FileUtils::instance().remove(photo.getImagePath())){

        logError("Error! Could not delete image %s", photo.getImagePath().c_str());

        //FIXME: isto pode levar o sistema a um estado inválido (o arquivo existe, mas o registro no banco não existe)
        throw IOException(std::string("Image ") + photo.getImagePath() + std::string(" could not be removed!"));
    }

    removeThumbnails(photo);
}

void PhotoManager::removeThumbnails(const Photo &photo){
    string filename_without_ext = FileUtils::instance().getFilenameWithoutExtension(photo.getImagePath());
    std::string thumbnailPrefix = generateThumbnailPrefix(filename_without_ext);
    thumbnailPrefix.append("*");
    std::string thumbnailsPattern = FileUtils::instance().joinPaths(getThumbnailsDir(), thumbnailPrefix);

    set<string> files = FileUtils::instance().findFiles(thumbnailsPattern);
    set<string>::const_iterator iter = files.begin();
    while(iter != files.end()){
        FileUtils::instance().remove(*iter);
        ++iter;
    }
}

Photo PhotoManager::createPhoto(Image &image, const PhotoDescription &photoDescription, const string &originalFilename, long collectionId, long userId)
{
    Photo photo{photoDescription, PhotoMetadata().setOriginalFilename(originalFilename)};
    return createPhotoImpl(photo, image, collectionId, userId);
}

Photo PhotoManager::createPhoto(Image &image, long collectiontId, long userId){
    Photo photo;
    return createPhotoImpl(photo, image, collectiontId, userId);
}


Photo & PhotoManager::createPhotoImpl(Photo & photo, Image & image, long collectionId, long userId){
    TemporaryImage savedImage = saveImage(image, photo);

    insertPhoto(photo, collectionId, userId);

    savedImage.keep();
    savedImage.close();

    return photo;
}

std::string PhotoManager::saveImage(Image & img, Photo & outPhoto){
    ScopedOpen openImg(img);

    if(!img.isValid() || !img.open()){
        throw CommonExceptions::IllegalArgumentException(Errors::InvalidImage);
    }

    istream& imageStream = img.getContent();
    if(!imageStream.good()){
        throw IllegalArgumentException(Errors::InvalidImageContent);
    }

    std::string imagePath = generateImagePath(img);
    this->photoCreator.create(&imageStream,imagePath, outPhoto);

    outPhoto.setImagePath(Files().extractFilename(imagePath));

    openImg.close();
    return imagePath;
}

long PhotoManager::getCollectionCtxId(long collectionId){
    return Data::instance().collections().get(collectionId).getParentContext();
}

Photo &PhotoManager::insertPhoto(Photo & photo, long collectionId, long userId)
{
    DataTransaction transaction(Data::instance(), TransactionType::Write);

    long ctxId = getCollectionCtxId(collectionId);
    photo.setContextId(ctxId);

    long photoId = Data::instance().photos().insert(photo);
    photo = getPhoto(photoId);

    if(collectionId != INVALID_COLLECTION_ID){
        Data::instance().collections().addPhoto(photoId, collectionId);
    }

    logPhotoEvent(EventType::PhotoCreation, photoId, userId);

    transaction.commit();

    return photo;
}

void PhotoManager::logPhotoEvent(const EventType& eventType, long photoId, long userId){
    auto event = Event{eventType, userId, photoId};
    EventsManager::instance().insert(event);
}

void PhotoManager::assertPhotoExist(const Photo &photoToUpdate)
{
    if(!existPhoto(photoToUpdate.getId())){
        throw NotFoundMember(Errors::nonExistentPhoto(photoToUpdate.getId()));
    }
}

std::string PhotoManager::generateImagePath(const Image &image) const{
    const string & imgDir = getImagesDir();
    std::string filepath = Files().generateUniqueFilepath(imgDir.empty() ? "." : imgDir);

    std::string extension = getImageExtension(image);
    return filepath + (extension.empty() ? "" : "." + getImageExtension(image));
}

string PhotoManager::getImageExtension(const Image &image) const
{
    //FIXME: should not use the type as the extension
    return image.getType();
}

string PhotoManager::getThumbnailsDir() const{
    string thumbnailDir = FileUtils::instance().joinPaths(getImagesDir(), Constants::THUMBNAIL_DIR_NAME);
    FileUtils::instance().createDirIfNotExists(thumbnailDir);

    return thumbnailDir;
}

} //namespace calmframe
