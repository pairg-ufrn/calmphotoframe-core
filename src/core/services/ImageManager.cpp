#include "ImageManager.h"

#include "model/image/ScopedOpen.h"
#include "image/PhotoCreator.h"
#include "image/ImageHelper.h"

#include "exceptions/NotFoundMember.h"
#include "utils/IOException.h"

#include "utils/StringCast.h"
#include "utils/FileUtils.h"

namespace calmframe {

using namespace utils;


ImageManager::ImageManager()
    : imageCreator(std::make_shared<ImageCreator>())
{}

const std::string &ImageManager::getImagesDir() const{
    return imagesDir;
}
void ImageManager::setImagesDir(const std::string & path){
    this->imagesDir = path;
    this->thumbnailsDir = Files().joinPaths(path, "thumbnails");
}

const std::string &ImageManager::getThumbnailsDir() const{
    return thumbnailsDir;
}

std::string ImageManager::saveImage(Image & image){
    Files().createDirIfNotExists(getImagesDir(), true);
    
    ScopedOpen openImage(image);
    Image result = imageCreator->createImage(&image.getContent(), genPath(image));
    
    openImage.close();
    
    return Files().extractFilename(result.getImagePath());
}

ImageManager::ImagePtr ImageManager::getImage(const std::string & filename){
    auto imagePath = getImagePath(filename);
    
    return getImageImpl(imagePath);
}

ImageManager::ImagePtr ImageManager::getThumbnail(const std::string & imageFilename, unsigned thumbnailSize){
    auto thumbPath = getThumbnailPath(imageFilename, thumbnailSize);
    
    if(Files().exists(thumbPath)){
        return getImageImpl(thumbPath);
    }
    
    return createThumbnail(imageFilename, thumbnailSize, thumbPath);
}

Size ImageManager::getSize(Image & image){
    return imageCreator->getSize(image);
}

bool ImageManager::removeImage(const std::string & imageKey){
    auto imgPath = getImagePath(imageKey);
    
    if(!Files().exists(imgPath)){
        return false;
    }
    
    removeThumbnails(imgPath);
    Files().remove(imgPath);
    
    return true;
}


void ImageManager::removeThumbnails(const std::string & imgPath)
{
    auto extension = Files().getFilenameExtension(imgPath);
    auto baseName = Files().getFilenameWithoutExtension(imgPath);
    auto basePath = Files().joinPaths(getThumbnailsDir(), baseName);
    
    auto thumbnailsPattern = basePath + "_*";
    
    for(const auto & thumbPath : Files().findFiles(thumbnailsPattern)){
        if(isThumbnailPath(thumbPath, basePath, extension)){
            Files().remove(thumbPath);
        }
    }    
}


ImageManager::ImagePtr 
ImageManager::createThumbnail(const std::string & imageFilename, unsigned thumbnailSize, const std::string & thumbnailPath){
    auto img = getImage(imageFilename);
    
    Files().createDirIfNotExists(getThumbnailsDir());
    
    Image imgResult = imageCreator->createThumbnail(*img, thumbnailSize, thumbnailPath);
    
    return std::make_shared<Image>(imgResult);
}


std::string ImageManager::getImagePath(const std::string& filename){
    return Files().joinPaths(getImagesDir(), filename);
}

std::string ImageManager::getThumbnailPath(const std::string& imageFilename, unsigned thumbnailSize){
    auto filename = Files().getFilenameWithoutExtension(imageFilename);
    auto extension = Files().getFilenameExtension(imageFilename);
    auto thumbnailFilename = filename + "_" + StrCast().toString(thumbnailSize) + "." + extension;
    
    return Files().joinPaths(getThumbnailsDir(), thumbnailFilename);    
}

bool ImageManager::isThumbnailPath(const std::string & thumbPath, const std::string & basePath, const std::string & extension){
    //TODO: use regular expressions when available

    //thumbPath starts with basePath
    if(thumbPath.compare(0, basePath.size(), basePath) != 0 || thumbPath.size() <= basePath.size()){
        return false;
    }
    
    //then is followed by a '_'
    size_t index = basePath.size();
    if(thumbPath[index] != '_'){
        return false;
    }
    
    //then is followed by 1 or more numbers
    int digitsCount = 0;
    ++index;
    while(index < thumbPath.size() && isdigit(thumbPath[index])){
        ++index;
        ++digitsCount;
    }
    
    if(digitsCount == 0 || index >= thumbPath.size()){
        return false;
    }
    
    //then is followed by a '.'
    if(thumbPath[index] != '.'){
        return false;
    }
    
    //then ends with extension
    return ++index < thumbPath.size()
           && thumbPath.compare(index, thumbPath.size(), extension) == 0;
}

std::string ImageManager::genPath(const Image &){
    return Files().generateTmpFilepath(getImagesDir());
}

ImageManager::ImagePtr ImageManager::getImageImpl(const std::string & path){
    if(!Files().exists(path)){
        throw NotFoundMember("Not found given image.");
    }
    
    return std::make_shared<Image>(path);
}

}//namespace
