#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include <memory>

#include "model/image/Image.h"
#include "model/image/Size.h"

namespace calmframe {

class ImageCreator;

class ImageManager{
public:
    using ImagePtr = std::shared_ptr<Image>;
    
public:
    ImageManager();
    virtual ~ImageManager() = default;

    const std::string & getImagesDir() const;
    void setImagesDir(const std::string & path);
    virtual const std::string & getThumbnailsDir() const;

    /** Save the given image on imagesDir.
     * @return a 'key' to get the saved image latter.
     * @throw IOException if image cannot be saved.
    */
    virtual std::string saveImage(Image & image);
    
    virtual ImagePtr getImage(const std::string & imageKey);
    
    /** Get a thumbnail for image 'imageFilename' and each dimension with 'thumbnailSize' pixels.
     *  If a thumbnail for this image with the given size already exist, then gets it.
     *  If the thumbnailSize is greater than or equals the image size, returns a square version of the image.
     * 
     *  @param imageKey - identifies an existing image on imagesDir
     *  @param thumbnailSize - the dimension in pixels for width and height of thumbnail.
     *  @throws NotFoundMember - if image 'imageFilename' does not exist
     *  @throws IOException - if cannot create thumbnail
    */
    virtual ImagePtr getThumbnail(const std::string & imageKey, unsigned thumbnailSize);

    /** Removes an image identified by the given key (and all related thumbnails).
     * @return true if image was removed and false if image was not found.
    */
    virtual bool removeImage(const std::string & imageKey);
    
    virtual Size getSize(Image & image);
        
    
protected:
    ImagePtr createThumbnail(const std::string & imageFilename, unsigned thumbnailSize, const std::string & thumbnailPath);
    
    std::string getImagePath(const std::string& filename);
    std::string getThumbnailPath(const std::string& imageFilename, unsigned thumbnailSize);
    bool isThumbnailPath(const std::string & thumbPath, const std::string & basePath, const std::string & extension);
    
    std::string genPath(const Image & img);
    ImagePtr getImageImpl(const std::string & path);
    
    void removeThumbnails(const std::string & imgPath);
    
private:
    std::string imagesDir, thumbnailsDir;
    std::shared_ptr<ImageCreator> imageCreator;
};

}//namespace

#endif // IMAGEMANAGER_H
