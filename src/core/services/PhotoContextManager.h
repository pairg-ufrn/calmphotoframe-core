#ifndef PHOTOCONTEXTMANAGER_H
#define PHOTOCONTEXTMANAGER_H

#include "../model/photo/PhotoContext.h"
#include "../model/permissions/ProtectedEntity.h"
#include "../model/permissions/Permission.h"
#include "../model/user/UserInfo.h"
#include "PermissionsVerifier.h"

#include <vector>
#include <unordered_map>

namespace calmframe {

class PhotoContextManager {
public:
    static const int InvalidUserId;

    typedef ProtectedEntity<PhotoContext> ContextWithPermission;
    typedef std::vector<ContextWithPermission> ListContextsWithPermission;
    typedef ProtectedEntity<UserInfo> Collaborator;
    typedef std::vector<Collaborator> CollaboratorsList;

private:
    PermissionsVerifier permissionsVerifier;
public:
    void initialize() const;

    std::vector<PhotoContext> listContexts() const;

    /** List all contexts that can be read.
     *  If @param{userId} is valid, include also the contexts that this user has (at least) a read permission level.*/
    std::vector<PhotoContext> listVisible(long userId=InvalidUserId) const;
    ListContextsWithPermission listWithPermission(long userId, PermissionLevel permissionLevel);

    /** Return the list of users that has a specific permission on the given context, with the respective permission levels.*/
    CollaboratorsList listCollaborators(long ctxId);


    bool existContext(long ctxId) const;
    PhotoContext getContext(long id) const;
    PhotoContext getDefaultContext(long userId=InvalidUserId) const;

    /** Creates a context and (optionally) assigns a owner to it.
        @param ctx - the context data used to create the context
        @param ownerUserId - if is a valid user id, assigns a admin permission to this user on the created context
    */
    PhotoContext createContext(const PhotoContext & ctx, long ownerUserId=InvalidUserId) const;
    PhotoContext updateContext(long ctxId, const PhotoContext & ctxToUpdate, long userId=InvalidUserId) const;
    PhotoContext updateContext(long ctxId, const PhotoContext & ctxToUpdate, const CollaboratorsList & collaborators
                                , long userId=InvalidUserId) const;
    PhotoContext deleteContext(long ctxId) const;

    void createDefaultContext() const;

    Permission getUserPermission(long ctxId, long userId);
    void assertUserHasPermission(long userId, long ctxId, PermissionLevel permissionLevel) const;
protected:
    /** Removes all collection of a given context (including the root collection)*/
    void removeAllContextCollections(long ctxId) const;

    using CtxMap = std::unordered_map<long, ContextWithPermission>;
    void listPermissionsFromVisibility(CtxMap & ctxToPermission, long userId, PermissionLevel permissionLevel);
    void listUserPermissions(CtxMap & ctxToPermission, long userId, PermissionLevel permissionLevel);
};

}//namespace

#endif // PHOTOCONTEXTMANAGER_H
