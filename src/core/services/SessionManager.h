#ifndef SESSIONMANAGER_H
#define SESSIONMANAGER_H

#include "../model/user/Session.h"
#include "../model/user/UserAccount.h"

namespace calmframe{

class SessionManager
{
public:
    static SessionManager & instance();

    static const long LOCALUSER_ID;
public:
    SessionManager();
    ~SessionManager();
public:
    virtual Session startSession(const UserAccount & userAccount, const std::string &userPassword, bool unlimited=false);
    virtual Session startLocalSession(const std::string & clientSecret, bool unlimited=false);
    virtual void finishSession(const Session & sessionId);
    virtual void finishSession(const std::string & token);
    virtual void updateSession(const Session & session) const;
    bool validateSession(const Session & session) const;
    bool validateSession(const std::string & token) const;
    bool validateAndUpdateSession(const std::string & token) const;
    bool validateAndUpdateSession(const std::string & token, Session * sessionOut) const;
    virtual bool validatePassword(const UserAccount & userAccount, const std::string &userPassword) const;
    virtual long getSessionDuration() const;

    Session & getSession(const std::string & sessionToken, Session & session) const;
    Session && getSession(const std::string & sessionToken, Session && session = Session()) const;
    int getSessionUser(const std::string & sessionToken) const;

    void createLocalUser();
protected:

    bool validateSession(const std::string & token, Session *outSession) const;
    bool isTimeToUpdate(Session session) const;
    virtual bool isValid(const std::string & token, Session *outSession) const;
    virtual void removeExpiredSessions() const;

    virtual Session createSession(const UserAccount & userAccount, bool unlimited=false);
    Session createSession(long userId, const std::string &token, bool unlimited=false);
    virtual long generateExpireTimestamp() const;
    virtual std::string generateToken(const UserAccount &userAccount) const;
    std::string generateLocalToken(const std::string & clientSecret);
};

}

#endif // SESSIONMANAGER_H
