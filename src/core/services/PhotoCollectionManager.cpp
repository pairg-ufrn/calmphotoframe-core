#include "PhotoCollectionManager.h"

#include "model/CollectionsOperation.h"
#include "data/Data.h"
#include "utils/IllegalArgumentException.hpp"
#include "utils/IllegalStateException.h"

#include "utils/CommonExceptions.h"
#include "utils/Log.h"
#include "exceptions/NotFoundMember.h"

#include <set>
#include <algorithm>

using namespace std;

namespace calmframe {

using namespace calmframe::data;

namespace Errors {
    static const char * RemoveRootError = "Forbidden to remove the root collection.";
    static const char * RemoveRootInBulkRemove = "Cannot remove collections, because root collection is among then.";
    static const char * CreateWithNoParentIdOrContextId = "Cannot create a PhotoCollection without a parent collection or a PhotoContext id";
}//namespace

PhotoCollectionManager::PhotoCollectionManager()
{}

bool PhotoCollectionManager::existCollection(long collectionId)
{
    return Data::instance().collections().exist(collectionId);
}

PhotoCollection PhotoCollectionManager::getCollection(long collectionId)
{
    PhotoCollection coll;
    if(!Data::instance().collections().get(collectionId, coll)){
        throw NotFoundMember();
    }
    return coll;
}

long PhotoCollectionManager::getContextRootId(long contextId) const
{
    return Data::instance().contexts().get(contextId).getRootCollection();
}

long PhotoCollectionManager::getDefaultRootId() const
{
    //FIXME: isso deveria ser responsabilidade do PhotoContextManager
    return Data::instance().contexts().getFirst().getRootCollection();
}

PhotoCollection PhotoCollectionManager::getContextRoot(long contextId)
{
    return Data::instance().collections().get(getContextRootId(contextId));
}

std::set<long> PhotoCollectionManager::getRootCollections() const{
    return Data::instance().contexts().listRootCollections();
}

long PhotoCollectionManager::getCollectionContext(long collectionId){
    return getCollection(collectionId).getParentContext();
}

std::vector<PhotoCollection> PhotoCollectionManager::listAll(){
    return Data::instance().collections().listAll();
}

std::vector<PhotoCollection> PhotoCollectionManager::listIn(const std::set<long> &collections)
{
    return Data::instance().collections().listIn(collections);
}

std::vector<PhotoCollection> PhotoCollectionManager::listFromContext(long ctxId)
{
    return Data::instance().collections().getByContext(ctxId);
}

std::map<long, PhotoCollection> PhotoCollectionManager::getCollectionTree(long collId)
{
    std::map<long, PhotoCollection> collTree;
    getCollectionTree(collId, collTree);
    return collTree;
}

std::map<long, PhotoCollection> &PhotoCollectionManager::getCollectionTree(long collId, std::map<long, PhotoCollection> &collTree)
{

    vector<PhotoCollection> allCollections = listAll();
    std::map<long, int> idToIndex;
    for(size_t i=0; i < allCollections.size(); ++i){
        idToIndex[allCollections[i].getId()] = i;
    }

    addToTree(collId, collTree, allCollections, idToIndex);

    return collTree;
}

std::set<long> PhotoCollectionManager::getCollectionsWithoutParent() const
{
    return Data::instance().collections().getCollectionsWithoutParent();
}

void PhotoCollectionManager::addToTree(long collId, std::map<long, PhotoCollection> &collTree
        , const vector<PhotoCollection> & allCollections
        , const std::map<long, int> &idToIndex )
{
    std::map<long, int>::const_iterator found = idToIndex.find(collId);
    if(found != idToIndex.end() && collTree.find(collId) == collTree.end()){
        const PhotoCollection & coll = allCollections[found->second];
        collTree[collId] = coll;

        const std::set<long> & subColls = coll.getSubCollections().getIds();
        std::set<long>::const_iterator iter = subColls.begin();
        while(iter != subColls.end()){
            addToTree(*iter, collTree, allCollections, idToIndex);
            ++iter;
        }
    }
}

void PhotoCollectionManager::executeCollectionOperation(const CollectionsOperation & collOp, long rootCollectionId){
    executeCollOperationImpl(collOp.getOperationType()
                                , collOp.getTargetCollectionIds()
                                , collOp.getPhotoIds()
                                , collOp.getSubCollectionsIds()
                                , rootCollectionId);
}

void PhotoCollectionManager::addItemsToCollections(const std::set<long> &collectionTargets
        , const std::set<long> &photoItems
        , const std::set<long> &subCollectionItems)
{
    executeCollOperationImpl(CollectionsOperation::ADD
                                    , collectionTargets
                                    , photoItems
                                    , subCollectionItems);
}

void PhotoCollectionManager::removeItemsFromCollections(
        const std::set<long> &collectionTargets
        , const std::set<long> &photoItems
        , const std::set<long> &subCollectionItems
        , long rootCollectionId)
{
    executeCollOperationImpl(CollectionsOperation::REMOVE
                                    , collectionTargets
                                    , photoItems
                                    , subCollectionItems
                                    , rootCollectionId);
}

void PhotoCollectionManager::executeCollOperationImpl(
        int operationType
        , const std::set<long>& collectionTargets
        , const std::set<long>& photoItems
        , const std::set<long>& subCollectionItems
        , long rootCollectionId)
{
    if(operationType == CollectionsOperation::ADD){
        Data::instance().collections().addItemsToCollections(collectionTargets, photoItems, subCollectionItems);
    }
    else if(operationType == CollectionsOperation::REMOVE){
        //FIXME: tornar operação transacional
        Data::instance().collections().removeItemsFromCollections(collectionTargets, photoItems, subCollectionItems);
        Data::instance().collections().removeCollectionsWithoutParent();

        moveOrphanPhotosToRoot(photoItems, rootCollectionId);
    }
    else{
        throw CommonExceptions::IllegalArgumentException("");
    }

    return;
}

void PhotoCollectionManager::moveOrphanPhotosToRoot(const std::set<long>& photoItems, long rootCollectionId)
{
    std::set<long> photoIds = Data::instance().collections().getPhotosWithoutParent(photoItems);

    if(rootCollectionId != PhotoCollection::INVALID_ID){
        set<long> rootCollection;
        rootCollection.insert(rootCollectionId);
        Data::instance().collections().addPhotos(rootCollection, photoIds);
    }
    else if(!photoIds.empty()){
//        logWarning("There are photos with no parent");
        throw CommonExceptions::IllegalStateException("There are photos with no parent");
    }
}

PhotoCollection PhotoCollectionManager::moveCollectionItems(long collectionId, const CollectionsOperation &collectionOperation)
{
    Data::instance().collections().moveCollectionItems(collectionId, collectionOperation.getTargetCollectionIds()
                                                        , collectionOperation.getPhotoIds()
                                                        , collectionOperation.getSubCollectionsIds());
    return Data::instance().collections().get(collectionId);
}

PhotoCollection PhotoCollectionManager::createCollectionAtContext(const PhotoCollection & collection, long contextId)
{
    return createCollection(collection, PhotoCollection::INVALID_ID, contextId);
}

PhotoCollection PhotoCollectionManager::createCollection(const PhotoCollection & collection, long parentId, long contextId)
{
    if(parentId == PhotoCollection::INVALID_ID && contextId == PhotoCollection::INVALID_CONTEXT_ID){
        throw CommonExceptions::IllegalArgumentException(Errors::CreateWithNoParentIdOrContextId);
    }
    if(parentId == PhotoCollection::INVALID_ID){
        parentId = getContextRootId(contextId);
    }

    long createdId = Data::instance().collections().insert(collection, parentId);

    return Data::instance().collections().get(createdId);
}

std::set<long> PhotoCollectionManager::insertCollectionPhotoIds(long memberId, const std::set<long> & photoIds)
{
    PhotoCollection coll = Data::instance().collections().get(memberId);

    if(!photoIds.empty()){
        set<long>::iterator iter = photoIds.begin();
        while(iter != photoIds.end()){
            coll.putPhoto(*iter);
            ++iter;
        }
        Data::instance().collections().update(coll);
    }

    return coll.getPhotos();
}

PhotoCollection PhotoCollectionManager::updateCollection(const PhotoCollection &photoCollection)
{
    PhotoCollection current = getCollection(photoCollection.getId());
    Data::instance().collections().update(photoCollection);

    long rootCollectionId = getContextRootId(photoCollection.getParentContext());
    if(current.getPhotos() != photoCollection.getPhotos()){
        moveOrphanPhotosToRoot(current.getPhotos(), rootCollectionId);
    }
    if(current.getSubCollections().getIds() != photoCollection.getSubCollections().getIds()){
        Data::instance().collections().removeCollectionsWithoutParent();
    }
    return getCollection(photoCollection.getId());
}

bool PhotoCollectionManager::removeCollection(long collectionId)
{
    return removeCollectionImpl(collectionId);
}

bool PhotoCollectionManager::removeCollection(long collectionId, PhotoCollection &outRemovedPhotoCollection)
{
    return removeCollectionImpl(collectionId, &outRemovedPhotoCollection);
}

bool PhotoCollectionManager::isRoot(long collectionId)
{
    return Data::instance().contexts().isRootCollection(collectionId);
}

bool PhotoCollectionManager::removeCollectionImpl(long collectionId, PhotoCollection* outCollection)
{
    if(isRoot(collectionId)){
        throw CommonExceptions::IllegalArgumentException(Errors::RemoveRootError);
    }

    PhotoCollection collToRemove;
    if(outCollection != NULL){
        collToRemove = Data::instance().collections().get(collectionId);
    }

    if(Data::instance().collections().remove(collToRemove.getId())){
        if(outCollection != NULL){
            *outCollection = collToRemove;
        }
        return true;
    }
    return false;
}

bool PhotoCollectionManager::containsRootCollection(const std::set<long> &collectionIds)
{
    set<long> rootIds = getRootCollections();
    set<long> intersection;
    std::set_intersection(rootIds.begin(), rootIds.end()
                        , collectionIds.begin(), collectionIds.end()
                        , std::inserter(intersection, intersection.end()));

    return !intersection.empty();
}

std::vector<PhotoCollection> PhotoCollectionManager::removeCollections(const std::set<long> &collectionIds)
{
    if(containsRootCollection(collectionIds)){
        throw CommonExceptions::IllegalArgumentException(Errors::RemoveRootInBulkRemove);
    }
    vector<PhotoCollection> toRemoveCollections = Data::instance().collections().listIn(collectionIds);
    Data::instance().collections().remove(collectionIds);

    return toRemoveCollections;
}

std::vector<Photo> PhotoCollectionManager::getPhotos(const std::set<long> &photoIds)
{
    return Data::instance().photos().listIn(photoIds);
}

std::vector<Photo> PhotoCollectionManager::getCollectionPhotos(long collectionId)
{
    SetId photoIds = Data::instance().collections().getCollectionPhotos(collectionId);
    return getPhotos(photoIds);
}

std::vector<PhotoCollection> PhotoCollectionManager::getSubCollections(long collectionId)
{
    return Data::instance().collections().getSubCollections(collectionId);
}

}//namespace
