#include "PhotoManagersLayer.h"

namespace calmframe {

PhotoManagersLayer::PhotoManagersLayer()
    : photoFrame()
{

}

PhotoManager &PhotoManagersLayer::getPhotoManager(){
    return photoFrame;
}
const PhotoManager & PhotoManagersLayer::getPhotoManager() const{
    return photoFrame;
}

PhotoCollectionManager &PhotoManagersLayer::getCollectionsManager(){
    return collectionsManager;
}
const PhotoCollectionManager & PhotoManagersLayer::getCollectionsManager() const{
    return collectionsManager;
}

PhotoContextManager &PhotoManagersLayer::getContextManager(){
    return contextManager;
}
const PhotoContextManager & PhotoManagersLayer::getContextManager() const{
    return contextManager;
}

void PhotoManagersLayer::initialize()
{
    contextManager.initialize();
}


}//namespace

