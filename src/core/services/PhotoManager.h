#ifndef PHOTOMANAGER_H
#define PHOTOMANAGER_H

#include <vector>
#include <set>

#include "model/photo/Photo.h"
#include "image/PhotoCreator.h"
#include "../model/image/Image.h"

namespace calmframe {

enum class EventType;

class PhotoManager{
public:
    static const long INVALID_COLLECTION_ID;
    static const long INVALID_USER_ID;

    typedef std::vector<Photo> PhotosList;
private:
    std::string imagesDir;
    ImageCreator photoCreator;
public:
    virtual ~PhotoManager();

    virtual bool existPhoto(unsigned photoId) const;
    virtual Photo getPhoto(unsigned photoId);
    virtual Image getImage(unsigned photoId);
    virtual Image getImage(const Photo & photo);
    virtual Image getThumbnail(unsigned photoId, int size);
    virtual PhotosList getPhotos(const std::string & query= std::string());
    virtual PhotosList getPhotosIn(const std::set<long> & photoIds, const std::string & query= std::string());
    virtual PhotosList getPhotosInCollection(long collId, bool recursively=false, const std::string & query= std::string());
    virtual PhotosList getPhotosInContext(long ctxId, const std::string & query={});

    virtual long getRootPhotoCollectionId(long contextId);
    void getPhotoIdsInCollectionTree(long collId, std::set<long> & outPhotoIds) const;

    virtual Photo updatePhoto(const Photo & photoToUpdate, long userId = INVALID_USER_ID);
    virtual Photo deletePhoto(long photoId, long userId=INVALID_USER_ID);
    virtual Photo createPhoto(Image &image, long collectiontId=INVALID_COLLECTION_ID, long userId=INVALID_USER_ID);
    virtual Photo createPhoto(Image & image
                              , const PhotoDescription & photoDescription
                              , const std::string & originalFilename=""
                              , long collectionId=INVALID_COLLECTION_ID
                              , long userId=INVALID_USER_ID);

    Photo removePhotoAndImage(long photoId);

    virtual const std::string & getImagesDir() const;
    virtual void setImagesDir(const std::string & imagesDir);

    long getCollectionCtxId(long collectionId);
protected:
    Photo updatePhotoImpl(const Photo &photoToUpdate, long userId, bool logEvent);

    Photo & createPhotoImpl(Photo & photo, Image & image, long collectionId, long userId);
    std::string saveImage(Image & img, Photo & outPhoto);
    Photo & insertPhoto(Photo &photo, long collectionId, long userId);

//    void addNewPhotoDescription(Photo & photo, const PhotoDescription &photoDescription
//            , const std::string &originalFilename);

    void logPhotoEvent(const EventType& eventType, long photoId, long userId);

    virtual std::string generateImagePath(const Image & image) const;
    virtual std::string getImageExtension(const Image & image) const;
    virtual void tryDeleteImage(Photo photo);

    std::string getThumbnailsDir() const;

    Image getPhotoThumbnail(const Photo & photo, const int &size);
    Image createThumbnail(const Photo & photo, const int & size);
    void removeThumbnails(const Photo & photo);
    std::string generateThumbnailPath(const Photo & photo, const int & size) const;
    std::string generateThumbnailFilename(const std::string & filepath, const int & size) const;
    std::string generateThumbnailPrefix(const std::string &filename) const;

    void assertPhotoExist(const Photo &photoToUpdate);
};

}

#endif // PHOTOMANAGER_H
