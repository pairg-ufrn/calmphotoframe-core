#include "PhotoContextManager.h"

#include "data/Data.h"
#include "data/DataTransaction.h"
#include "exceptions/NotFoundMember.h"
#include "exceptions/InsufficientPermissionException.h"
#include "utils/IllegalStateException.h"
#include "utils/StringCast.h"

#include "utils/Log.h"

namespace calmframe {

using namespace ::calmframe::data;

const int PhotoContextManager::InvalidUserId = User::INVALID_ID;

namespace Errors {
    static const char * const UsersAndPermissionsMismatch = "users and permission does not match.";
}//namespace

void PhotoContextManager::initialize() const{
    if(Data::instance().contexts().count() == 0){
        createDefaultContext();
    }
}

std::vector<PhotoContext> PhotoContextManager::listContexts() const
{
    return Data::instance().contexts().listAll();
}

std::vector<PhotoContext> PhotoContextManager::listVisible(long userId) const{
    auto visibleContexts = Data::instance().contexts().list(Visibility::VisibleByOthers);

    if(userId != InvalidUserId){
        auto readableCtxIds = Data::instance().permissions().listContextsWithPermission(userId, PermissionLevel::Read);
        for(const PhotoContext & ctx: visibleContexts){
            //removes already gotten contexts
            readableCtxIds.erase(ctx.getId());
        }
        if(!readableCtxIds.empty()){
            auto otherCtxs = Data::instance().contexts().listIn(readableCtxIds);
            visibleContexts.insert(visibleContexts.end(), otherCtxs.begin(), otherCtxs.end());
        }
    }

    return visibleContexts;
}

void PhotoContextManager::listUserPermissions(
    PhotoContextManager::CtxMap & ctxToPermission, long userId, PermissionLevel permissionLevel)
{
    auto permissions = Data::instance().permissions().list(userId, permissionLevel);
    for(const auto & p: permissions){
        long ctxId = p.getContextId();
        auto iter = ctxToPermission.find(ctxId);
        if(iter == ctxToPermission.end()){
            auto context = Data::instance().contexts().get(ctxId);
            ctxToPermission[ctxId] = ContextWithPermission{userId, p.getPermissionLevel(), context};
        }
        else if(p.getPermissionLevel() > iter->second.getPermissionLevel()){
            iter->second.setPermissionLevel(p.getPermissionLevel());
        }
    }
}

bool PhotoContextManager::existContext(long ctxId) const
{
    return Data::instance().contexts().exist(ctxId);
}

PhotoContext PhotoContextManager::getContext(long id) const
{
    PhotoContext gotContext = Data::instance().contexts().get(id);
    if(gotContext.getId() == PhotoContext::INVALID_ID){
        throw NotFoundMember("Not found context with id " + utils::StrCast().cast().toString(id));
    }

    return gotContext;
}

PhotoContext PhotoContextManager::getDefaultContext(long userId) const
{
    //TODO: evitar listar todos os contextos visiveis pelo usuário
    //TODO: utilizar
    auto visibleContexts = listVisible(userId);
    if(visibleContexts.empty()){
        throw InsufficientPermissionException("Not found any visible context");
    }
    return *std::min_element(visibleContexts.begin(), visibleContexts.end());
}

PhotoContext PhotoContextManager::createContext(const PhotoContext & ctx, long ownerUserId) const
{
    DataTransaction transaction(Data::instance());

    //Accepts only the context name and visibility on creation(ignore other parameters)
    const Visibility & visibility = (ctx.getVisibility() != Visibility::Unknown)
                                        ? ctx.getVisibility()
                                        : Visibility::EditableByOthers;

    PhotoContext ctxToInsert(ctx.getName(), visibility);
    long ctxId = Data::instance().contexts().insert(ctxToInsert);

    //Create root PhotoCollection
    PhotoCollection rootColl;
    rootColl.setParentContext(ctxId);
    long collId = Data::instance().collections().insert(rootColl);

    //Update context with the root collection id
    PhotoContext createdContext = Data::instance().contexts().get(ctxId);
    createdContext.setRootCollection(collId);

    Data::instance().contexts().update(createdContext);

    //Assigns the admin permission to context creator
    if(ownerUserId != InvalidUserId){
        Data::instance().permissions().putPermission(Permission{ctxId, ownerUserId, PermissionLevel::Admin});
    }

    transaction.commit();

    return createdContext;
}

PhotoContext PhotoContextManager::updateContext(long ctxId, const PhotoContext & ctxToUpdate, long userId) const{
    DataTransaction transaction(Data::instance());

    assertUserHasPermission(userId, ctxId, PermissionLevel::Admin);

    //Limit the fields allowed to update
    PhotoContext updateCtx = getContext(ctxId);
    updateCtx.setName(ctxToUpdate.getName());
    updateCtx.setVisibility(ctxToUpdate.getVisibility());

    Data::instance().contexts().update(updateCtx);

    transaction.commit();

    return updateCtx;
}

PhotoContext PhotoContextManager::updateContext(long ctxId, const PhotoContext & ctxToUpdate
    , const PhotoContextManager::CollaboratorsList & collaborators, long userId) const
{
    DataTransaction transaction(Data::instance());
    PhotoContext ctxUpdated = updateContext(ctxId, ctxToUpdate, userId);

    std::vector<Permission> ctxPermissions;
    for(auto collab : collaborators){
        ctxPermissions.push_back(Permission{ctxId, collab.getUserId(), collab.getPermissionLevel()});
    }
    Data::instance().permissions().replaceContextPermissions(ctxId, ctxPermissions);
    /** Restore the admin permission (is safe because #updateContext(long, PhotoContext, long userId)
     * require that the userId has admin permission over this context).*/
    Data::instance().permissions().putPermission(Permission{ctxId, userId, PermissionLevel::Admin});

    transaction.commit();
    return ctxUpdated;
}


PhotoContext PhotoContextManager::deleteContext(long ctxId) const
{
    DataTransaction transaction(Data::instance());

    //Get the context that will be removed (this also ensure that the context exist)
    PhotoContext ctx = getContext(ctxId);

    /* If there is only one context on database (the one that will be removed),
        then then database will be with no one context. So, insert a new default context.
        This is made before remove the context to prevent the sqlite to give the same
        id to the new context (clients could think that the context has not been removed).
    */
    if(Data::instance().contexts().count() == 1){
        createDefaultContext();
    }

    Data::instance().contexts().remove(ctxId);

    //When using an Sql Data instance, this should not be needed, because the
    //collections should be removed by cascade
    removeAllContextCollections(ctxId);

    transaction.commit();

    return ctx;
}

void PhotoContextManager::removeAllContextCollections(long ctxId) const
{
    SetId collIds = Data::instance().collections().listIdsByContext(ctxId);

    for(SetId::const_iterator iter = collIds.begin(); iter!=collIds.end(); ++iter){
        Data::instance().collections().remove(*iter);
    }
}

void PhotoContextManager::createDefaultContext() const{
    createContext(PhotoContext());
}

Permission PhotoContextManager::getUserPermission(long ctxId, long userId){
    return permissionsVerifier.getUserPermission(userId, ctxId);
}

void PhotoContextManager::assertUserHasPermission(long userId, long ctxId, PermissionLevel permissionLevel) const
{
    permissionsVerifier.assertUserHasPermission(userId, ctxId, permissionLevel);
}

static Visibility fromPermissionLevel(PermissionLevel permissionLevel){
    switch (permissionLevel) {
    case PermissionLevel::Write:
        return Visibility::EditableByOthers;
    case PermissionLevel::Read:
        return Visibility::VisibleByOthers;
    default:
        return Visibility::Unknown;
    }
}

void PhotoContextManager::listPermissionsFromVisibility(
PhotoContextManager::CtxMap & ctxToPermission, long userId, PermissionLevel permissionLevel)
{
    const Visibility visibility = fromPermissionLevel(permissionLevel);
    if(visibility != Visibility::Unknown){
        auto visibleContexts = Data::instance().contexts().list(visibility);
        for(auto ctx : visibleContexts){
            ctxToPermission[ctx.getId()] =
                ContextWithPermission{userId, Permission::fromVisibility(ctx.getVisibility()), ctx};
        }
    }
}

PhotoContextManager::ListContextsWithPermission
PhotoContextManager::listWithPermission(long userId, PermissionLevel permissionLevel){
    CtxMap ctxToPermission;

    listPermissionsFromVisibility(ctxToPermission, userId, permissionLevel);

    listUserPermissions(ctxToPermission, userId, permissionLevel);

    ListContextsWithPermission ctxPermissions;
    std::transform(ctxToPermission.begin(), ctxToPermission.end(), std::back_inserter(ctxPermissions),
        [](CtxMap::value_type & mapPair){
            return mapPair.second;
        });
    return ctxPermissions;
}

static constexpr auto orderByUserId = [](const Permission & p1, const Permission & p2){
    return (p1.getUserId() < p2.getUserId());
};
static constexpr auto orderById = [](const User & u1, const User & u2){
    return (u1.getId() < u2.getId());
};

PhotoContextManager::CollaboratorsList PhotoContextManager::listCollaborators(long ctxId){
    auto permissions = Data::instance().permissions().listByContext(ctxId);
    std::sort(permissions.begin(), permissions.end(), orderByUserId);

    SetId userIds;
    for(auto p: permissions){
        userIds.insert(p.getUserId());
    }

    auto users = Data::instance().users().listUsersIn(userIds);
    std::sort(users.begin(), users.end(), orderById);

    if(users.size() != permissions.size()){
        throw CommonExceptions::IllegalStateException(Errors::UsersAndPermissionsMismatch);
    }

    CollaboratorsList collaborators;
    for(size_t i=0; i<permissions.size(); ++i){
        const Permission & perm = permissions[i];
        const User & user = users[i];

        if(perm.getUserId() != user.getId()){
            throw CommonExceptions::IllegalStateException(Errors::UsersAndPermissionsMismatch);
        }

        collaborators.push_back(Collaborator{perm, UserInfo{user}});
    }

    return collaborators;
}


}//namespace
