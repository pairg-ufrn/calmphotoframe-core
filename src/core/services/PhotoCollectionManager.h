#ifndef PHOTOCOLLECTIONMANAGER_H
#define PHOTOCOLLECTIONMANAGER_H

#include "model/photo/PhotoCollection.h"
#include "model/photo/Photo.h"

#include <vector>
#include <set>

namespace calmframe {

class CollectionsOperation;

class PhotoCollectionManager
{
public:
    PhotoCollectionManager();

    bool existCollection(long collectionId);
    PhotoCollection getCollection(long collectionId);
    long getContextRootId(long contextId) const;
    /** Get the collection id of the default PhotoContext root*/
    long getDefaultRootId() const;

    PhotoCollection getContextRoot(long contextId);
    std::set<long> getRootCollections() const;

    /** Get the context id of the given collection.*/
    long getCollectionContext(long collectionId);

    std::vector<PhotoCollection> listAll();
    std::vector<PhotoCollection> listIn(const std::set<long>& collections);
    std::vector<PhotoCollection> listFromContext(long ctxId);
    std::map<long, PhotoCollection> getCollectionTree(long collId);
    std::map<long, PhotoCollection> & getCollectionTree(long collId, std::map<long, PhotoCollection> & collTree);

    std::set<long> getCollectionsWithoutParent() const;

    void executeCollectionOperation(const CollectionsOperation & collOp, long rootCollectionId);
    void addItemsToCollections(const std::set<long>& collectionTargets
                    , const std::set<long>& photoItems
                    , const std::set<long>& subCollectionItems
                    );
    void removeItemsFromCollections(
                     const std::set<long>& collectionTargets
                    , const std::set<long>& photoItems
                    , const std::set<long>& subCollectionItems
                    , long rootCollectionId);

    PhotoCollection moveCollectionItems(long collectionId, const CollectionsOperation & collectionOperation);

    PhotoCollection createCollection(const PhotoCollection & collection
                                    , long parentId
                                    , long contextId = PhotoCollection::INVALID_CONTEXT_ID);
    PhotoCollection createCollectionAtContext(const PhotoCollection & collection, long contextId);

    std::set<long> insertCollectionPhotoIds(long memberId, const std::set<long> &photoIds);
    PhotoCollection updateCollection(const PhotoCollection & photoCollection);

    bool removeCollection(long collectionId);
    bool removeCollection(long collectionId, PhotoCollection & outRemovedPhotoCollection);
    std::vector<PhotoCollection> removeCollections(const std::set<long> & collectionIds);

    std::vector<Photo> getPhotos(const std::set<long> & photoIds);
    std::vector<Photo> getCollectionPhotos(long collectionId);
    std::vector<PhotoCollection> getSubCollections(long collectionId);
    bool isRoot(long collectionId);
    bool containsRootCollection(const std::set<long> &collectionIds);
protected:
    void executeCollOperationImpl(int operationType
                    , const std::set<long>& collectionTargets
                    , const std::set<long>& photoItems
                    , const std::set<long>& subCollectionItems
                    , long rootCollectionId=PhotoCollection::INVALID_ID);
    void moveOrphanPhotosToRoot(const std::set<long> &photoItems, long rootCollectionId);
    bool removeCollectionImpl(long collectionId, PhotoCollection* outCollection=NULL);
    void addToTree(long collId, std::map<long, PhotoCollection> &collTree
                 , const std::vector<PhotoCollection> &allCollections, const std::map<long, int> & idToIndex);
};

}//namespace

#endif // PHOTOCOLLECTIONMANAGER_H
