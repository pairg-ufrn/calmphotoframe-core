#include "PhotoCreator.h"

#include "FreeImage.h"
#include "FreeImagePlus.h"
#include "FreeimageLibrary.h"
#include "FreeimageStreamHandler.h"
#include "ImageHelper.h"
#include "MetadataExtractor.h"

#include "model/image/ScopedOpen.h"

#include "utils/FileUtils.h"
#include "utils/IOException.h"
#include "utils/Log.h"

#include <fstream>
#include <string>


namespace calmframe {

using namespace std;
using namespace CommonExceptions;
using namespace utils;
using namespace photo;

ImageCreator::ImageCreator(){
    FreeimageLibrary::require();
}

std::string correctPath(std::string imagePath, const FREE_IMAGE_FORMAT & format){
    string extension = FileUtils::instance().getFilenameExtension(imagePath);
    if(extension.empty()){
        string formatExtension = ImageHelper::instance().getExtensionFromFormat(format);
        
        if(formatExtension.empty()){
            logWarning("Saving image \"%s\" with no extension!", imagePath.c_str());
        }
        
        imagePath.append(".").append(formatExtension);
    }
    
    return imagePath;
}

void ImageCreator::create(std::istream *imageContent, std::string &imagePath, Photo &photoOut){
    checkPathNotExist(imagePath);
    
    FreeImageIO imageHandler;
    FreeimageStreamHandler::createInputHandler(imageHandler);

    FREE_IMAGE_FORMAT format =fipImage::identifyFIFFromHandle(&imageHandler, imageContent);
    fipImage image;

    bool loaded = image.loadFromHandle(&imageHandler, imageContent);

    if(!loaded){
        throw IOException("Could not load image file from stream.");
    }

    PhotoMetadata metadata = MetadataExtractor::instance().extractMetadata(image);
    photoOut.setMetadata(metadata);

    imagePath = correctPath(imagePath, format);

    bool saved = image.save(imagePath.c_str());

    if(!saved || !FileUtils::instance().exists(imagePath)){
        logError("PhotoCreator::create:\t"  "error saving image");
        throw IOException("Could not save image file at path " + imagePath);
    }
    photoOut.getMetadata().setImageSize(FileUtils::instance().getFileSize(imagePath));
}

Image ImageCreator::createImage(istream * imageContent, const string & imagePath){
    checkPathNotExist(imagePath);

    fipImage image;
    int format = loadImageFromStream(imageContent, image);

    return saveImage(image, format, imagePath);
}

Image ImageCreator::createThumbnail(Image & img, int thumbnailSize, const string & thumbnailPath){
    fipImage image;
    openImage(img, image);
    
    return ImageHelper::instance().makeThumbnail(image, thumbnailSize, thumbnailPath);
}

Size ImageCreator::getSize(Image & img){
    fipImage image;
    openImage(img, image);
    
    return Size{image.getWidth(), image.getHeight()};
}

void ImageCreator::openImage(Image & img, fipImage & outImage, int * outFormat){
    bool loaded = false;
    int format = FIF_UNKNOWN;
    
    if(!img.getImagePath().empty()){
        format = fipImage::identifyFIF(img.getImagePath().c_str());
        loaded = outImage.load(img.getImagePath().c_str());
    }
    
    if(!loaded){
        ScopedOpen scopedOpenImg(img);
        loaded = loadImageFromStreamImpl(&img.getContent(), outImage, &format);
    }
    
    checkLoaded(loaded);
    
    if(outFormat){
        *outFormat = format;
    }
}

int ImageCreator::loadImageFromStream(istream * imageContent, fipImage & image){
    int format;
    bool loaded = loadImageFromStreamImpl(imageContent, image, &format);
    
    checkLoaded(loaded);
    
    return format;
}

bool ImageCreator::loadImageFromStreamImpl(istream* imageContent, fipImage& image, int * outFormat){
    FreeImageIO imageHandler;
    FreeimageStreamHandler::createInputHandler(imageHandler);

    FREE_IMAGE_FORMAT format = fipImage::identifyFIFFromHandle(&imageHandler, imageContent);

    if(outFormat){
        *outFormat = format;
    }

    return  image.loadFromHandle(&imageHandler, imageContent);
}

Image ImageCreator::saveImage(fipImage & image, int format, const string & imagePath){
    Image imgOut{correctPath(imagePath, (FREE_IMAGE_FORMAT)format)};
    
    setImageType(imgOut, format);
    
    bool saved = image.save(imgOut.getImagePath().c_str());
    checkSaved(saved, imgOut);    
    
    return imgOut;
}



void ImageCreator::checkLoaded(bool loaded){
    if(!loaded){
        throw IOException("Failed to load image.");
    }    
}

void ImageCreator::checkSaved(bool saved, const Image & img){
    if(!saved || !FileUtils::instance().exists(img.getImagePath())){
        throw IOException("Failed to save image at path " + img.getImagePath());
    }    
}


void ImageCreator::checkPathNotExist(const string & imagePath)
{
    if(FileUtils::instance().exists(imagePath)){
        logError("PhotoCreator::create:\t" "File at path %s already exists!", imagePath.c_str());
        string errMessage = string("Trying to create photo overriding existent image \"").append(imagePath).append("\"");
        throw IOException(errMessage);
    }    
}


void ImageCreator::setImageType(Image & out, int format){
    out.setType(getTypeFromFormat(format));
}

const char * ImageCreator::getTypeFromFormat(int format) const{
    FREE_IMAGE_FORMAT imgFormat = (FREE_IMAGE_FORMAT)format;
    
    switch (imgFormat) {
        case FIF_BMP:
            return "bmp";
        case FIF_JPEG:
            return "jpeg";
        case FIF_PNG:
            return "png";
        case FIF_TIFF:
            return "tiff";
        case FIF_GIF:
            return "gif";
        case FIF_JP2:
            return "jp2";
        case FIF_ICO:
            return "x-icon";  
        /* These types are not registered in iana       
        case FIF_CUT:
        case FIF_DDS:
        case FIF_EXR:
        case FIF_FAXG3:
        case FIF_HDR:
        case FIF_IFF:
        case FIF_J2K:
        case FIF_JNG:
        case FIF_JXR:
        case FIF_KOALA:
        case FIF_LBM:
        case FIF_MNG:
        case FIF_PBM:
        case FIF_PBMRAW:
        case FIF_PCD:
        case FIF_PCX:
        case FIF_PFM:
        case FIF_PGM:
        case FIF_PGMRAW	:
        case FIF_PICT:
        case FIF_PPM:
        case FIF_PPMRAW:
        case FIF_PSD:
        case FIF_RAS:
        case FIF_RAW:
        case FIF_SGI:
        case FIF_TARGA:
        case FIF_WBMP:
        case FIF_WEBP:
        case FIF_XBM:
        case FIF_XPM: */
        default:
            return "";
    }    
}

}//namespace
