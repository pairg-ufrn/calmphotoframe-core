/*
 * MetadataGenerator.cpp
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#include "MetadataExtractor.h"
#include "FreeimageLibrary.h"
#include "FreeImagePlus.h"
#include "utils/FileUtils.h"

#include <string>

using namespace std;

namespace calmframe {
namespace photo {

using namespace calmframe::utils;

namespace Constants{
    const char * GPSLatitudeKey = "GPSLatitude";
    const char * GPSLongitudeKey = "GPSLongitude";
    const char * GPSAltitudeKey = "GPSAltitude";
    const char * GPSLatitudeRefKey = "GPSLatitudeRef";
    const char * GPSLongitudeRefKey = "GPSLongitudeRef";
    const char * GPSAltitudeRefKey = "GPSAltitudeRef";
}

string getCameraMaker(const fipImage & image){
	return FreeimageLibrary::instance().getMetadata(image,"Make",FIMD_EXIF_MAIN);
}
string getCameraModel(const fipImage & image){
	return FreeimageLibrary::instance().getMetadata(image,"Model",FIMD_EXIF_MAIN);
}
string getOriginalDate(const fipImage & image){
    return FreeimageLibrary::instance().getMetadata(image,"DateTimeOriginal",FIMD_EXIF_EXIF);
}

int toInt(const fipTag & tag){
    switch (tag.getLength()) {
    case 1:
        return ((int8_t *)tag.getValue())[0];
    case 2:
        return ((int16_t *)tag.getValue())[0];
    case 4:
        return ((int32_t *)tag.getValue())[0];
    case 8:
        return ((int64_t *)tag.getValue())[0];
    }
    return 0;
}

int getOrientationFlag(const fipImage & image){
    fipTag tag;
    if(image.getMetadata(FIMD_EXIF_MAIN,"Orientation", tag)){
        return toInt(tag);
    }
    return 0;
}
float fromRational(UINT64 rational){
    LONG * rationalParts = (LONG *)&rational;
    LONG numerator = rationalParts[0];
    LONG denominator = rationalParts[1];

    return numerator / (float)denominator;
}

float getCoordinateValue(fipTag & tag, bool positive=true){
    /*Uma coordenada é feita de 3 valores (graus, minutos e segundos).
        Minutos e segundos (neste caso) são subunidades do grau, em que:
            1 grau = 60 minutos
            1 grau = 3600 segundos
        Cada um deles é representado como um valor "RATIONAL" (FIDT_RATIONAL).
        Isto é um valor de 8 bytes, cujos 4 primeiros correspondem a um numerador e os dois últimos a um denominador.*/
    if(tag.getType() != FIDT_RATIONAL && tag.getCount() != 3){
        return Location::UNKNOWN_COORD;
    }

    float degrees = 0;
    float values[3];
    for(DWORD i=0; i < tag.getCount(); ++i){
        UINT64 rationalValue = ((UINT64 *)tag.getValue())[i];
        values[i] = fromRational(rationalValue);
    }
    degrees = values[0]          //degrees
              + values[1]/60.0f    //minutes
              + values[2]/3600.0f; //seconds
    return positive ? degrees : -degrees;
}
float getAltitudeValue(fipTag & tag, bool positive=true){
    /* Altitude é expressa por um valor do tipo "RATIONAL" (FIDT_RATIONAL).
    */
    if(tag.getType() != FIDT_RATIONAL && tag.getCount() != 1){
        return Location::UNKNOWN_ALTITUDE;
    }

    UINT64 rationalValue = ((UINT64 *)tag.getValue())[0];
    float altitude = fromRational(rationalValue);
    return positive ? altitude : -altitude;
}

bool latitudeIsPositive(const fipImage & image){
    fipTag tag;
    if(image.getMetadata(FIMD_EXIF_GPS,Constants::GPSLatitudeRefKey, tag)){
        return string(tag.toString(FIMD_EXIF_GPS)) == "N";
    }
    return true;
}
bool longitudeIsPositive(const fipImage & image){
    fipTag tag;
    if(image.getMetadata(FIMD_EXIF_GPS,Constants::GPSLongitudeRefKey, tag)){
        return string(tag.toString(FIMD_EXIF_GPS)) == "E";
    }
    return true;
}
bool altitudeIsPositive(const fipImage & image){
    /* A altitude pode ser dada como um valor acima ou abaixo do nível do mar.
        Esta informação é armazenada na tag GPSAltitudeRef e codificada como um byte (FIDT_BYTE).
        O valor 0 indica que a altitude é acima do nível do mar e o valor 1 para abaixo do nível do mar. */
    fipTag tag;
    if(image.getMetadata(FIMD_EXIF_GPS,Constants::GPSAltitudeRefKey, tag)){
        return toInt(tag) != 1;
    }
    return true;
}

float getLatitude(const fipImage & image){
    fipTag tag;
    if(image.getMetadata(FIMD_EXIF_GPS,Constants::GPSLatitudeKey, tag)){
        return getCoordinateValue(tag, latitudeIsPositive(image));
    }
    return Location::UNKNOWN_COORD;
}
float getLongitude(const fipImage & image){
    fipTag tag;
    if(image.getMetadata(FIMD_EXIF_GPS,Constants::GPSLongitudeKey, tag)){
        return getCoordinateValue(tag, longitudeIsPositive(image));
    }
    return Location::UNKNOWN_COORD;
}
float getAltitude(const fipImage & image){
    fipTag tag;
    if(image.getMetadata(FIMD_EXIF_GPS,Constants::GPSAltitudeKey, tag)){
        return getAltitudeValue(tag, altitudeIsPositive(image));
    }
    return Location::UNKNOWN_COORD;
}

void fillMetadata(PhotoMetadata & metadata, const fipImage & image){
    metadata.setImageWidth(image.getWidth());
    metadata.setImageHeight(image.getHeight());

	metadata.setCameraMaker(getCameraMaker(image));
	metadata.setCameraModel(getCameraModel(image));
	metadata.setOriginalDate(getOriginalDate(image));
    metadata.setOrientation(getOrientationFlag(image));

    metadata.getLocation().setLatitude(getLatitude(image));
    metadata.getLocation().setLongitude(getLongitude(image));
    metadata.getLocation().setAltitude(getAltitude(image));
}
PhotoMetadata MetadataExtractor::extractMetadata(const std::string& imagePath) {
	FreeimageLibrary::require();
	PhotoMetadata metadata;

	fipImage image;
	if(image.load(imagePath.c_str())){
		fillMetadata(metadata, image);
    }
    metadata.setImageSize(FileUtils::instance().getFileSize(imagePath));

    return metadata;
}

PhotoMetadata MetadataExtractor::extractMetadata(const fipImage &image){
    FreeimageLibrary::require();
    PhotoMetadata metadata;

    fillMetadata(metadata, image);

    return metadata;
}

} /* namespace photo */
} /* namespace calmframe */

