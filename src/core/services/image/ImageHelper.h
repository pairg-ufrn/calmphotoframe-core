#ifndef IMAGEHELPER_H
#define IMAGEHELPER_H

#include <string>
#include "model/image/Image.h"

class fipImage;
using calmframe::Image;

class ImageHelper
{
public:
    static ImageHelper & instance();
public:
    ImageHelper();
    Image makeThumbnail(const std::string & filepath, int thumbnailSize
                        ,const std::string & thumbnailDstPath="", bool centerCrop=true);

    Image makeThumbnail(fipImage & image, int thumbnailSize
                        ,const std::string & thumbnailDstPath="", bool centerCrop=true);

    std::string getExtensionFromFormat(const int & format);
protected:
    bool canSaveImageExtension(const fipImage & img, const std::string & extension) const;
    std::string createThumbnailPath(const std::string & filepath);
};

#endif // IMAGEHELPER_H
