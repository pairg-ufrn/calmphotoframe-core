#include "FreeimageStreamHandler.h"
#include <cstdio>

#include "utils/Log.h"

using namespace std;

unsigned DLL_CALLCONV FreeimageStreamHandler::readInputStream(void *buffer, unsigned size, unsigned count, fi_handle handle) {
    istream * inputStream = (istream *)handle;
    if(inputStream == NULL){
        return EOF;
    }

    int readedBytes = 0;
    for(unsigned i=0; i < count && inputStream->good(); ++i){
        inputStream->read((char *)buffer + readedBytes, size);
        readedBytes += inputStream->gcount();
    }

    return readedBytes;
}

int FreeimageStreamHandler::seekInputStream(fi_handle handle, long offset, int origin) {
    istream * inputStream = (istream *)handle;
    if(inputStream == NULL){
        return EOF;
    }

    ios_base::seekdir seekReference = (origin == SEEK_SET ? ios_base::beg : (origin == SEEK_CUR ? ios_base::cur : ios_base::end) );
    inputStream->seekg(offset, seekReference);
    return inputStream->good() || inputStream->eof() ? 0 : -1;
}

long FreeimageStreamHandler::tellInputStream(fi_handle handle) {
    istream * inputStream = (istream *)handle;
    if(inputStream == NULL){
        return EOF;
    }

    long tellg = ((istream *)handle)->tellg();
    return tellg;
}

unsigned FreeimageStreamHandler::writeInputStream(void */*buffer*/, unsigned /*size*/, unsigned /*count*/, fi_handle /*handle*/) {
    return 0;
}

void FreeimageStreamHandler::createInputHandler(FreeImageIO &handler)
{
    handler.read_proc  = readInputStream;
    handler.write_proc = writeInputStream;
    handler.seek_proc  = seekInputStream;
    handler.tell_proc  = tellInputStream;
}

FreeimageStreamHandler::FreeimageStreamHandler()
{
}
