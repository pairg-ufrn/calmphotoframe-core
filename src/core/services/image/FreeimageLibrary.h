/*
 * FreeimageLibrary.h
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#ifndef PHOTO_IMAGE_FREEIMAGELIBRARY_H_
#define PHOTO_IMAGE_FREEIMAGELIBRARY_H_

#include <FreeImage.h>
#include <FreeImagePlus.h>
#include <string>

namespace calmframe {
namespace photo {
/** classe para manipular operações globais de freeimage e para garantir sua inicialização e
 * destruição.*/
class FreeimageLibrary {
public:
    static FreeimageLibrary & instance();
	/** Utilizado para garantir que será realizada inicialização à FreeImage*/
    static void require();
private: //Construtor privado para impedir mais de uma inicialização
    FreeimageLibrary();
public:
    virtual ~FreeimageLibrary();
public:
	std::string getMetadata(const fipImage & image, const std::string & metadataName, FREE_IMAGE_MDMODEL model);
};

} /* namespace photo */
} /* namespace calmframe */

#endif /* PHOTO_IMAGE_FREEIMAGELIBRARY_H_ */
