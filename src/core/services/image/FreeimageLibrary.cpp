/*
 * FreeimageLibrary.cpp
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#include "FreeimageLibrary.h"

#include "utils/Log.h"

using std::string;

namespace calmframe {
namespace photo {

FreeimageLibrary &FreeimageLibrary::instance(){
    //Este objeto será criado apenas uma vez (pois é marcado como estático)
    static FreeimageLibrary singleton;
    return singleton;
}

void FreeimageLibrary::require(){
    instance();
}

/**
FreeImage error handler
@param fif Format / Plugin responsible for the error
@param message Error message
*/
void FreeImageErrorHandler(FREE_IMAGE_FORMAT /*fif*/, const char *message) {
    logError("FreeImage error: '%s'", message);
}

FreeimageLibrary::FreeimageLibrary() {
#if defined(FREEIMAGE_LIB) || !defined(WIN32)
    FreeImage_Initialise();
#endif

    FreeImage_SetOutputMessage(FreeImageErrorHandler);
}

FreeimageLibrary::~FreeimageLibrary() {
#if defined(FREEIMAGE_LIB) || !defined(WIN32)
        FreeImage_DeInitialise();
    #endif
}

string FreeimageLibrary::getMetadata(const fipImage & image, const string & metadataName, FREE_IMAGE_MDMODEL model){
    fipTag tag;
	if(image.getMetadata(model, metadataName.c_str(), tag)){
		return string(tag.toString(model));
	}
	return string();
}
} /* namespace photo */
} /* namespace calmframe */

