#include "ImageHelper.h"

#include <algorithm>
#include <vector>

#include <FreeImage.h>
#include <FreeImagePlus.h>

#include "FreeimageLibrary.h"
#include "utils/FileUtils.h"
#include "utils/StringUtils.h"
#include "utils/Log.h"

using namespace std;
using namespace calmframe::photo;
using namespace calmframe::utils;

namespace Constants{
    static const char * DEFAULT_IMAGE_EXTENSION = "jpg";
#ifdef WIN32
    static const char * PATH_SEP = "\\";
#else
    static const char * PATH_SEP = "/";
#endif
}

ImageHelper &ImageHelper::instance()
{
    static ImageHelper singleton;
    return singleton;
}

ImageHelper::ImageHelper()
{
}


static bool makeSquare(fipImage & image)
{
    int minSize = min(image.getWidth(), image.getHeight());
    int maxSize = max(image.getWidth(), image.getHeight());
    int diff = maxSize - minSize;

    if(diff == 0){
        return true; //Já é um quadrado
    }

    int left=0, right=image.getWidth(), top=0, bottom=image.getHeight();

    if(maxSize == (int)image.getHeight()){
        top += diff/2;
        bottom -= diff/2;
    }
    else{
        left += diff/2;
        right -= diff/2;
    }
    return image.crop(left, top, right, bottom);
}

Image ImageHelper::makeThumbnail(const std::string &filepath, int thumbnailSize, const string &thumbnailDstPath, bool centerCrop){
    //Garante que freeImageLibrary terá sido iniciada
    FreeimageLibrary::instance().require();

    fipImage image;
    if(image.load(filepath.c_str())){
        string thumbnailPath = thumbnailDstPath;
        if(thumbnailPath.empty()){
            thumbnailPath = createThumbnailPath(filepath);
        }
        
        return makeThumbnail(image, thumbnailSize, thumbnailPath, centerCrop);
    }

    return Image();
}

Image ImageHelper::makeThumbnail(fipImage & image, int thumbnailSize, const string & thumbnailDstPath, bool centerCrop){
    int width = image.getWidth(), height = image.getHeight();
    int maxSize = std::max(width, height);
    int minSize = std::min(width, height);

    float ratio = maxSize/(float)minSize;

    int thumbMinSize = thumbnailSize;
    int thumbMaxSize = (int)(thumbMinSize * ratio);

    if(image.makeThumbnail(thumbMaxSize, false)){
        if(centerCrop){
            makeSquare(image);
        }
        
        string thumbnailPath = thumbnailDstPath;

        if(!canSaveImageExtension(image, Files().getFilenameExtension(thumbnailPath))){
            //Modifica extensão para salvar imagem como jpeg
            thumbnailPath = Files().getFilenameWithoutExtension(thumbnailPath)
                               + ".jpeg";
        }
        if(image.save(thumbnailPath.c_str())){
            return Image(thumbnailPath);
        }
        //TODO: lançar exceção
    }
    
    return Image{};
}

string ImageHelper::getExtensionFromFormat(const int &format)
{
    const char * extensionList = FreeImage_GetFIFExtensionList((FREE_IMAGE_FORMAT)format);
    vector<string> extensions = StringUtils::instance().splitAndTrim(extensionList,',');

    return extensions.size() == 0 ? "" : extensions[0];
}

bool ImageHelper::canSaveImageExtension(const fipImage &img, const string &extension) const
{
    string filename = "a." + extension;
    //Obs.:Code adapted from fipImage.cpp

    // Try to guess the file format from the file extension
    FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(filename.c_str());
    if(fif != FIF_UNKNOWN ) {
        // Check that the dib can be saved in this format
        FREE_IMAGE_TYPE image_type = img.getImageType();
        if(image_type == FIT_BITMAP) {
            // standard bitmap type
            WORD bpp = img.getBitsPerPixel();
            return (FreeImage_FIFSupportsWriting(fif) && FreeImage_FIFSupportsExportBPP(fif, bpp));
        } else {
            // special bitmap type
            return FreeImage_FIFSupportsExportType(fif, image_type);
        }
    }
    return false;
}

string ImageHelper::createThumbnailPath(const string &filepath){
    size_t filenameIndex = filepath.rfind(Constants::PATH_SEP);
    string filename = filenameIndex != string::npos ? filepath.substr(filenameIndex + 1)
                                                    : filepath;

    size_t extIndex = filename.rfind(".");
    string extension = (extIndex != string::npos) ? filename.substr(extIndex)
                                                  : string(".").append(Constants::DEFAULT_IMAGE_EXTENSION);

    return FileUtils::instance().generateTmpFilepath().append(extension);
}
