#ifndef PHOTOCREATOR_H
#define PHOTOCREATOR_H

#include <istream>
#include <string>

#include "model/photo/Photo.h"
#include "model/image/Image.h"
#include "model/image/Size.h"

class fipImage;

namespace calmframe {

class ImageCreator
{
public:
    ImageCreator();

    void create(std::istream * imageContent, std::string &imagePath, Photo & photoOut);
    
    Image createImage(std::istream * imageContent, const std::string & imagePath);
    Image createThumbnail(Image & img, int thumbnailSize, const std::string & thumbnailPath);
    
    Size getSize(Image & img);
    
    void checkLoaded(bool loaded);
    
    
protected:
    void openImage(Image & img, fipImage & outImage, int * outFormat=nullptr);

    int loadImageFromStream(std::istream * imageContent, fipImage & outImage);
    bool loadImageFromStreamImpl(std::istream* imageContent, fipImage& image, int * outFormat=nullptr);
    
    Image saveImage(fipImage & image, int format, const std::string & imagePath);
    const char * getTypeFromFormat(int format) const;
    
    void setImageType(Image & out, int format);
    
    void checkSaved(bool saved, const Image & img);
    void checkPathNotExist(const std::string& imagePath);
};

}

#endif // PHOTOCREATOR_H
