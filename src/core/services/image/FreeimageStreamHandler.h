#ifndef FREEIMAGESTREAMHANDLER_H
#define FREEIMAGESTREAMHANDLER_H

#include "FreeImage.h"
#include <istream>

class FreeimageStreamHandler
{
public:
    static unsigned DLL_CALLCONV readInputStream(void *buffer, unsigned size, unsigned count, fi_handle handle);
    static int      DLL_CALLCONV seekInputStream(fi_handle handle, long offset, int origin);
    static long     DLL_CALLCONV tellInputStream(fi_handle handle);
    static unsigned DLL_CALLCONV writeInputStream(void *buffer, unsigned size, unsigned count, fi_handle handle);

    static void createInputHandler(FreeImageIO & handler);
public:
    FreeimageStreamHandler();
};

#endif // FREEIMAGESTREAMHANDLER_H
