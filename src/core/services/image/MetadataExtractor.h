/*
 * MetadataGenerator.h
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#ifndef PHOTO_METADATAGENERATOR_H_
#define PHOTO_METADATAGENERATOR_H_

#include "model/photo/PhotoMetadata.h"

class fipImage;

namespace calmframe {
namespace photo {

class MetadataExtractor {
public:
	static MetadataExtractor & instance(){
		static MetadataExtractor singleton;
		return singleton;
	}
public:
	MetadataExtractor() {}
    virtual ~MetadataExtractor() {}
    PhotoMetadata extractMetadata(const std::string & imagePath);
    PhotoMetadata extractMetadata(const fipImage & image);
};

} /* namespace photo */
} /* namespace calmframe */

#endif /* PHOTO_METADATAGENERATOR_H_ */
