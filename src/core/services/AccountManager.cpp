#include "AccountManager.h"

#include "ImageManager.h"

#include "model/user/UserLoginInfo.h"
#include "model/user/Session.h"
#include "model/image/Image.h"

#include "exceptions/DatabaseException.h"
#include "exceptions/NotFoundMember.h"

#include "data/Data.h"

#include "utils/StringCast.h"
#include "utils/EncodingUtils.h"
#include "utils/IllegalStateException.h"
#include "utils/RandomUtils.h"
#include "utils/Log.h"

namespace calmframe {

using namespace std;
using namespace data;
using namespace CommonExceptions;

namespace Errors {

static string duplicateLoginError(const std::string & login){
    return string("Login '").append(login).append("' already exist");
}

const string emptyLogin = "Login is empty";
const string emptyPassword = "Password is empty";

}//namespace


const long AccountManager::ROOT_ID = 0;
const char * AccountManager::ROOT_LOGIN = "admin";
const char * AccountManager::ROOT_DEFAULT_PASS = "admin";
    
AccountManager::AccountManager()
    : _imageManager(std::make_shared<ImageManager>())
{}

void AccountManager::initialize(){
    if(!exist(ROOT_ID) && !Data::instance().users().exist(ROOT_LOGIN)){
        UserAccount account = makeAccount(ROOT_LOGIN, ROOT_DEFAULT_PASS);
        account.setIsAdmin(true);
        account.setActive(true);
        
        Data::instance().users().insertAt(ROOT_ID, account);
    }
}

AccountManager::UsersCollection AccountManager::listUsers()
{
    return Data::instance().users().listUsers();
}

bool AccountManager::exist(long userId)
{
    return Data::instance().users().exist(userId);
}

bool AccountManager::isAdmin(long userId){
    UserAccount account;

    if(userId != User::INVALID_ID && Data::instance().users().get(userId, account)){
        return account.isAdmin();
    }

    return false;
}

UserAccount AccountManager::create(const UserLoginInfo & loginInfo, bool startActivated){
    return create(loginInfo.getLogin(), loginInfo.getPassword(), startActivated);
}

UserAccount AccountManager::create(const std::string &userName, const std::string &userPassword, bool startActivated)
{
    UserAccount accountToCreate = makeAccount(userName, userPassword);
    accountToCreate.setActive(startActivated);

    if(userName.empty()){
        throw InvalidLoginException(Errors::emptyLogin);
    }
    if(userPassword.empty()){
        throw InvalidPasswordException(Errors::emptyPassword);
    }
    if(Data::instance().users().exist(userName)){
        throw DuplicateLoginException(Errors::duplicateLoginError(userName));
    }

    try{
        long insertedId = Data::instance().users().insert(accountToCreate);
        accountToCreate.setId(insertedId);
    }
    catch(std::exception & ex){
        throw DatabaseException("Could not create user account", ex);
    }

    return accountToCreate;
}

User AccountManager::getUser(long userId)
{
    User user;
    if(!Data::instance().users().getUser(userId, user)){
        throwNotFoundMemberError(userId);
    }
    return user;
}

User &AccountManager::updateUser(User & user)
{
    User originUser = getUser(user.getId());
    
    //avoid change these fields (maybe should throw exception)
    user.setActive(originUser.isActive());
    user.setIsAdmin(originUser.isAdmin());

    Data::instance().users().update(user);
    return user;
}

User AccountManager::removeUser(long userId)
{
    User user = getUser(userId);
    if(!Data::instance().users().remove(userId)){
        throw IllegalStateException("Failed to remove user with id "
                        + utils::StrCast().toString(userId));
    }

    return user;
}

void AccountManager::ensureUpdatePermission(long requestUser, const User & targetUser){
    if(isAdmin(requestUser)){
        return;
    }
    else if(requestUser != targetUser.getId()){//non admin user cannot change other user
        throw InsufficientPermissionException();
    }

    User currentUser = getUser(targetUser.getId());
    if(currentUser.isActive() != targetUser.isActive()
        || currentUser.isAdmin() != targetUser.isAdmin())
    {
        throw UpdateFieldsForbiddenException();
    }
}

UserAccount AccountManager::makeAccount(const string & userName, const string & userPassword){
    UserAccount accountToCreate;
    accountToCreate.setLogin(userName);
    setPasswordInfo(accountToCreate, userPassword);

    return accountToCreate;
}

void AccountManager::setPasswordInfo(UserAccount & accountToCreate, const string& userPassword){
    accountToCreate.setPassSalt(generateSalt(accountToCreate));
    accountToCreate.setPassHash(generatePassHash(accountToCreate, userPassword));    
}

ImageManager &AccountManager::getImageManager(){
    CHECK_NOT_NULL(_imageManager.get());
    return *_imageManager;
}

const ImageManager &AccountManager::getImageManager() const{
    CHECK_NOT_NULL(_imageManager.get());
    return *_imageManager;
}


std::string AccountManager::generatePassHash(const UserAccount &userAccount, const std::string &userPassword)
{
    string encodingData;
    encodingData.append(userAccount.getPassSalt());
    encodingData.append(userPassword);
    return EncodingUtils::instance().generateSHA1(encodingData);
}

string AccountManager::generateSalt(const UserAccount &userAccount){
    const int randNum = 4;
    int randoms[randNum];

    for(int i=0; i < randNum; ++i){
        randoms[i] = RandomUtils::instance().random();
    }

    string salt;
    const char * bytes = (char *)&randoms;
    for(size_t i=0; i < sizeof(int) * randNum; ++i){
        if(bytes[i] != 0){
            salt.push_back(bytes[i]);
        }
    }
    salt.append(userAccount.getLogin());

    return salt;
}

void AccountManager::throwNotFoundMemberError(long userId){
    throw NotFoundMember("Could not found user with id " + utils::StrCast().toString(userId));
}

AccountManager::ImageManagerPtr AccountManager::imageManager() const{
    return _imageManager;
}

AccountManager & AccountManager::imageManager(const ImageManagerPtr & imageManager){
    _imageManager = imageManager;
    return *this;
}

const string &AccountManager::getImagesDir(){
    return getImageManager().getImagesDir();
}

AccountManager &AccountManager::setImagesDir(const string & path){
    getImageManager().setImagesDir(path);
    return *this;
}

long AccountManager::getSessionUser(const std::string & sessionToken) const
{
    Session session;
    if(Data::instance().sessions().getByToken(sessionToken, session)){
        return session.getUserId();
    }
    return User::INVALID_ID;
}

void AccountManager::updatePassword(long userId, const std::string & password){
    UserAccount account;
    if(!Data::instance().users().get(userId, account)){
        throwNotFoundMemberError(userId);
    }
    
    setPasswordInfo(account, password);
    
    Data::instance().users().updateAccount(userId, account);
}

User AccountManager::updateProfileImage(long userId, Image & image){
    User user = getUser(userId);
    
    auto oldImage = user.profileImage();
    
    auto profileImageKey = _imageManager->saveImage(image);
    user.profileImage(profileImageKey);
    
    updateUser(user);
    
    if(!oldImage.empty()){
       _imageManager->removeImage(oldImage);
    }
    
    return user;
}

AccountManager::ImagePtr AccountManager::getProfileImage(long userId){
    User user = getUser(userId);
    
    try{
        return _imageManager->getThumbnail(user.profileImage(), THUMBNAIL_SIZE);
    }
    catch(const NotFoundMember &){
        return {};
    }
}

User AccountManager::setAccountEnabled(long userId, bool enabled){
    User user = getUser(userId);
    user.setActive(enabled);
    
    Data::instance().users().update(user);
    
    return getUser(userId);
}

}//namespace

