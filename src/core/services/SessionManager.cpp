#include "SessionManager.h"
#include "AccountManager.h"

#include "Configurations.h"
#include "Options.h"
#include "data/Data.h"

#include "exceptions/InvalidTokenException.h"

#include "utils/CommonExceptions.h"
#include "utils/TimeUtils.h"
#include "utils/EncodingUtils.h"
#include "utils/StringCast.h"
#include "utils/RandomUtils.h"

#include "utils/Log.h"

#include "Poco/Thread.h"
using Poco::Thread;

namespace calmframe{

using namespace std;
using namespace data;
using namespace utils;
using CommonExceptions::DataException;

namespace Constants{
    static const char * LOCAL_PASSHASH = "localhash";
    static const char * LOCAL_PASSSALT = "localsalt";

    static const float SessionUpdatePercent = 0.6;
}

const long SessionManager::LOCALUSER_ID = -1;

SessionManager &SessionManager::instance()
{
    static SessionManager singleton;
    return singleton;
}

SessionManager::SessionManager()
{}
SessionManager::~SessionManager()
{}

Session SessionManager::startSession(const UserAccount &userAccount, const std::string & userPassword, bool unlimited){
    if(validatePassword(userAccount,userPassword)){
        return createSession(userAccount, unlimited);
    }

    static const Session INVALID_SESSION;
    return INVALID_SESSION;
}

Session SessionManager::startLocalSession(const string &clientSecret, bool unlimited){
    createLocalUser();

    return createSession(LOCALUSER_ID, generateLocalToken(clientSecret), unlimited);
}

void SessionManager::finishSession(const string &token){
    Session session;
    if(Data::instance().sessions().getByToken(token, session)){
        this->finishSession(session);
    }
}

void SessionManager::updateSession(const Session &session) const{
    Session sessionToUpdate(session);
    sessionToUpdate.setExpireTimestamp(generateExpireTimestamp());
    if(!Data::instance().sessions().update(sessionToUpdate)){
        throw DataException("Failed to update session data.");
    }
}
void SessionManager::finishSession(const Session &session){
    Data::instance().sessions().remove(session);
}

bool SessionManager::validateSession(const Session &session) const{
    removeExpiredSessions();
    Session dbSession;
    if(Data::instance().sessions().getByToken(session.getToken(), dbSession)){
        return dbSession.getUserId() == session.getUserId()
                && !dbSession.expired();
    }
    return false;
}

bool SessionManager::validateSession(const string &token) const{
    return validateSession(token, NULL);
}
bool SessionManager::validateSession(const string &token, Session * outSession) const{
    removeExpiredSessions();
    return isValid(token, outSession);
}
bool SessionManager::isValid(const string &token, Session *outSession) const{
    Session session;
    if(Data::instance().sessions().getByToken(token, session)){
        bool expired = session.expired();
        if(!expired && outSession != NULL){
            *outSession = session;
        }
        return !expired;
    }

    return false;
}

void SessionManager::removeExpiredSessions() const{
    Data::instance().sessions().removeExpired();
}

bool SessionManager::validatePassword(const UserAccount &userAccount, const std::string &userPassword) const{
    const long userId = userAccount.getId();

    UserAccount existingAccount;
    if(userId != LOCALUSER_ID && Data::instance().users().get(userId, existingAccount))
    {
        string existingPassHash = existingAccount.getPassHash();
        string generatedPassHash = AccountManager::generatePassHash(userAccount,userPassword);
        return (existingAccount.isActive() && generatedPassHash == existingPassHash);
    }

    return false;
}

bool SessionManager::validateAndUpdateSession(const string &token) const{
    Session session;
    return validateAndUpdateSession(token, &session);
}

bool SessionManager::validateAndUpdateSession(const string & token, Session * sessionOut) const{
    if(validateSession(token, sessionOut)){
        if(isTimeToUpdate(*sessionOut)){
            updateSession(*sessionOut);
        }
        return true;
    }
    return false;
}

Session SessionManager::createSession(const UserAccount &userAccount, bool unlimited){
    return createSession(userAccount.getId(), generateToken(userAccount), unlimited);
}

Session SessionManager::createSession(long userId, const string & token, bool unlimited){
    long expireTimestamp = unlimited ? Session::unlimitedExpiration : generateExpireTimestamp();

    Session session{expireTimestamp, token, userId};

    long sessionId = Data::instance().sessions().insert(session);
    session.setId(sessionId);

    return session;
}

long SessionManager::generateExpireTimestamp() const{
    long currentTime = TimeUtils::instance().getTimestamp();
    long expireTime = currentTime + getSessionDuration();

    return expireTime;
}

bool SessionManager::isTimeToUpdate(Session session) const{
    long timeLeft = TimeUtils::instance().getTimestamp() - session.getExpireTimestamp();
    return timeLeft  < getSessionDuration() * Constants::SessionUpdatePercent;
}

long SessionManager::getSessionDuration() const{
    string sessionTimeOption = Configurations::instance().get(Options::SESSION_TIME, Options::Defaults::SESSION_TIME);
    long sessionTime;
    if(!StringCast::instance().tryToLong(sessionTime, sessionTimeOption)
            || sessionTime <= 0)
    {
        logWarning("SessionManager:\t" "Failed getting sessionTimeout option."
                   "Got value: \"%s\". Using default value.", sessionTimeOption.c_str());

        sessionTime = StringCast::instance().toLong(Options::Defaults::SESSION_TIME);
    }

    return sessionTime;
}

Session &SessionManager::getSession(const string & sessionToken, Session & session) const{
    if(!Data::instance().sessions().getByToken(sessionToken, session)){
        throw InvalidTokenException();
    }
    return session;
}

Session &&SessionManager::getSession(const string & sessionToken, Session && session) const{
    getSession(sessionToken, (Session &)session);
    return std::move(session);
}

int SessionManager::getSessionUser(const std::string & sessionToken) const{
    return getSession(sessionToken).getUserId();
}

void SessionManager::createLocalUser(){
    if(Data::instance().users().exist(LOCALUSER_ID)){
        return;
    }

    UserAccount localUser;
    localUser.getUser().setIsAdmin(true);

    Data::instance().users().insertAt(LOCALUSER_ID, localUser);
}

string SessionManager::generateToken(const UserAccount & userAccount) const{
    string hashBase(userAccount.getPassHash());
    hashBase.append(userAccount.getPassSalt());
    hashBase.append(StringCast::instance().toString(RandomUtils::instance().random()));

    return EncodingUtils::instance().generateSHA1(hashBase);
}

string SessionManager::generateLocalToken(const std::string & clientSecret){
    UserAccount localUser;
    localUser.setPassHash(string(Constants::LOCAL_PASSHASH).append(clientSecret));
    localUser.setPassSalt(string(Constants::LOCAL_PASSSALT).append(clientSecret));

    return generateToken(localUser);
}

}

