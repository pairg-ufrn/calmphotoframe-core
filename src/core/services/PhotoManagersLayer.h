#ifndef PHOTOMANAGERSLAYER_H
#define PHOTOMANAGERSLAYER_H

#include "PhotoManager.h"
#include "PhotoCollectionManager.h"
#include "PhotoContextManager.h"

namespace calmframe {

class PhotoManagersLayer
{
private:
    PhotoManager photoFrame;
    PhotoCollectionManager collectionsManager;
    PhotoContextManager contextManager;

public:
    PhotoManagersLayer();

    PhotoManager & getPhotoManager();
    const PhotoManager & getPhotoManager() const;
    PhotoCollectionManager & getCollectionsManager();
    const PhotoCollectionManager & getCollectionsManager() const;
    PhotoContextManager & getContextManager();
    const PhotoContextManager & getContextManager() const;

    void initialize();
};

}//namespace

#endif // PHOTOMANAGERSLAYER_H
