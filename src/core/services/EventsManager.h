#ifndef EVENTSMANAGER_H
#define EVENTSMANAGER_H

#include <vector>

#include "model/events/Event.h"
#include "model/events/ExtendedEvent.h"

namespace calmframe {

class EventsManager{
public:
    typedef std::vector<Event> EventsList;
    typedef std::vector<ExtendedEvent> ExtendedEventsList;
public:
    static EventsManager & instance();

public:
    bool existEvent(long evtId) const;
    EventsList listRecents(int limit) const;
    ExtendedEventsList listRecentEvents(int limit=-1) const;
    void insert(const Event & evt) const;
    ExtendedEventsList completeExtendedEvents(const EventsList& events) const;
};

}//namespace

#endif // EVENTSMANAGER_H
