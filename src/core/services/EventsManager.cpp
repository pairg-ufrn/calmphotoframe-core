#include "EventsManager.h"

#include "data/Data.h"

#include <map>

using namespace std;

namespace calmframe {

using calmframe::data::Data;

EventsManager &EventsManager::instance(){
    static EventsManager singleton;
    return singleton;
}

bool EventsManager::existEvent(long evtId) const{
    return Data::instance().events().exist(evtId);
}

EventsManager::EventsList EventsManager::listRecents(int limit) const{
    return Data::instance().events().listRecents(limit);
}

void EventsManager::insert(const Event & evt) const{
    Data::instance().events().insert(evt);
}

EventsManager::ExtendedEventsList EventsManager::listRecentEvents(int limit) const{
    const EventsList & events = listRecents(limit);

    return completeExtendedEvents(events);
}

EventsManager::ExtendedEventsList EventsManager::completeExtendedEvents(const EventsList& events) const{
    //TODO: melhorar performance

    auto users = Data::instance().users().listUsers();

    map<long, int> idToIndex;
    for(size_t i=0; i < users.size(); ++i){
        auto user = users[i];
        idToIndex[user.getId()] = i;
    }

    ExtendedEventsList extendedEvents;

    for(auto evt : events){
        auto iter = idToIndex.find(evt.getUserId());

        User user;
        if(iter != idToIndex.end()){
            user = users[iter->second];
        }

        extendedEvents.push_back({evt, user});
    }

    return extendedEvents;
}

}//namespace

