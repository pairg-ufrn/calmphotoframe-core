#include "PermissionsVerifier.h"

#include "data/Data.h"
#include "exceptions/InsufficientPermissionException.h"

namespace calmframe {

using namespace calmframe::data;

Permission PermissionsVerifier::getUserPermission(long userId, long ctxId) const
{
    PhotoContext ctx = Data::instance().contexts().get(ctxId);
    PermissionLevel defaultPermissionLevel = ctx.getVisibility().toPermissionLevel();

    Permission permission(ctxId, userId, defaultPermissionLevel);


    if(Data::instance().permissions().tryGetPermission(userId, ctxId, permission)
        && permission.getPermissionLevel() < defaultPermissionLevel)
    {
        permission.setPermissionLevel(defaultPermissionLevel);
    }

    return permission;
}

Permission PermissionsVerifier::assertUserHasPermission(long userId, long ctxId, PermissionLevel requiredPermissionLevel) const
{
    Permission permission = getUserPermission(userId, ctxId);

    if(permission.getPermissionLevel() < requiredPermissionLevel)
    {
        throw InsufficientPermissionException();
    }

    return permission;
}



}//namespace
