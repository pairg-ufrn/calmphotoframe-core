#ifndef ACCOUNTMANAGER_H
#define ACCOUNTMANAGER_H

#include "../model/user/UserAccount.h"
#include "model/permissions/PermissionLevel.h"

#include "utils/IllegalArgumentException.hpp"

#include <string>
#include <vector>
#include <memory>

namespace calmframe {

class UserLoginInfo;
class Image;
class ImageManager;

EXCEPTION_SUBCLASS(DuplicateLoginException, CommonExceptions::IllegalArgumentException);
EXCEPTION_SUBCLASS(InvalidLoginException, CommonExceptions::IllegalArgumentException);
EXCEPTION_SUBCLASS(InvalidPasswordException, CommonExceptions::IllegalArgumentException);
EXCEPTION_SUBCLASS(InsufficientPermissionException, CommonExceptions::IllegalArgumentException);
EXCEPTION_SUBCLASS(UpdateFieldsForbiddenException, InsufficientPermissionException);

class AccountManager{
public:

    typedef std::vector<User> UsersCollection;
    
    using ImageManagerPtr = std::shared_ptr<ImageManager>;
    using ImagePtr = std::shared_ptr<Image>;
    
public:
    static constexpr const int THUMBNAIL_SIZE = 72;
    static const long ROOT_ID;
    static const char * ROOT_LOGIN;
    static const char * ROOT_DEFAULT_PASS;
    
public:
    AccountManager();
    virtual ~AccountManager() = default;
    
    void initialize();
    
    ImageManagerPtr imageManager() const;
    AccountManager & imageManager(const ImageManagerPtr & imageManager);
    
    const std::string & getImagesDir();
    AccountManager & setImagesDir(const std::string & path);
    
public:
    UsersCollection listUsers();

    virtual bool exist(long userId);
    virtual bool isAdmin(long userId);

    UserAccount create(const UserLoginInfo & loginInfo, bool startActivated=true);
    UserAccount create(const std::string & userName, const std::string & userPassword, bool startActivated=true);
    virtual User getUser(long userId);
    
    /** Updates the user profile on database.
     *  Does not enable change the user admin or active status.
     * 
     * @param user - the user to change and its new state.
     * @throw NotFoundMember if user does not exist
    */
    virtual User & updateUser(User & user);
    
    virtual void updatePassword(long userId, const std::string & password);
    virtual User setAccountEnabled(long userId, bool enabled);

    virtual User updateProfileImage(long userId, Image & image);
    virtual ImagePtr getProfileImage(long userId);

    User removeUser(long userId);
    
    /** Verify if the user 'requestUser' has permission to update the 'targetUser' and throws exceptions if not.
        @throw InsufficientPermissionException - if the requestUser has no permission to change the targetUser
        @throw UpdateFieldsForbiddenException - if the requestUser can change the targetUser, but not some fields
    */
    void ensureUpdatePermission(long requestUser, const User & targetUser);

    /** Given a sessionToken, returns the user id related to this session*/
    virtual long getSessionUser(const std::string & sessionToken) const;

public:
    static UserAccount makeAccount(const std::string & userName, const std::string & userPassword);
    static std::string generatePassHash(const UserAccount & userAccount, const std::string &userPassword);
    static std::string generateSalt(const UserAccount & userAccount);

    
    static void setPasswordInfo(UserAccount & accountToCreate, const std::string& userPassword);
    
protected:
    ImageManager & getImageManager();
    const ImageManager & getImageManager() const;
    void throwNotFoundMemberError(long userId);
    
private:
    ImageManagerPtr _imageManager;
};

}

#endif // ACCOUNTMANAGER_H
