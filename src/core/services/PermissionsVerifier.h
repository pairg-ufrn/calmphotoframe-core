#ifndef PERMISSIONSVERIFIER_H
#define PERMISSIONSVERIFIER_H

#include "../model/permissions/Permission.h"

namespace calmframe {

class PermissionsVerifier
{
public:
    Permission assertUserHasPermission(long userId, long ctxId, PermissionLevel permissionLevel) const;
    Permission getUserPermission(long userId, long ctxId) const;
};

}//namespace

#endif // PERMISSIONSVERIFIER_H
