#ifndef JSONCONFIGURATIONLOADER_H
#define JSONCONFIGURATIONLOADER_H

#include <istream>
#include <string>
#include "Configurations.h"

namespace calmframe{

class JsonConfigurationLoader{
    bool allowOptionOverride;
public:
    JsonConfigurationLoader(const bool & allowOptionOverride=true);
public:
    bool getAllowOptionOverride() const;
    void setAllowOptionOverride(bool value);
public:
    virtual void loadFromFile(const std::string & filename, Configurations &configurations);
    virtual void load(std::istream & jsonInput, Configurations & configurations);
};

}
#endif // JSONCONFIGURATIONLOADER_H
