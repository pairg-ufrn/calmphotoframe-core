#ifndef CONFIGURATIONSDAO_H
#define CONFIGURATIONSDAO_H

#include <string>

namespace calmframe {

class ConfigurationsDAO{
public:
    virtual ~ConfigurationsDAO() = default;

    virtual std::string get(const std::string & key)=0;
    virtual void put(const std::string & key, const std::string & value)=0;
};

}//namespace

#endif // CONFIGURATIONSDAO_H
