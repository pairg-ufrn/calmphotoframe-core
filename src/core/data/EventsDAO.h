#ifndef EVENTSDAO_H
#define EVENTSDAO_H

#include "model/events/Event.h"

#include <vector>

namespace calmframe {

class EventsDAO{
public:
    typedef std::vector<Event> EventsList;
public:
    virtual ~EventsDAO(){}

    virtual Event get(long id)=0;
    virtual long insert(const Event & evt)=0;
    virtual bool exist(long eventId)=0;
    virtual EventsList listRecents(int limit=-1)=0;
};

}//namespace

#endif // EVENTSDAO_H

