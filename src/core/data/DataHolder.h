#ifndef DATAHOLDER_H
#define DATAHOLDER_H

#include "pthread.h"
#include "DataBuilder.hpp"

#include "utils/PthreadMutex.h"

#include <memory>

namespace calmframe {
namespace data {

class DataHolder{
public:
    using DataBuilderPtr = std::shared_ptr<DataBuilder>;
public:
    static void dataDestructor(void * data);
public:
    DataHolder();

    Data * getData();
    void setDataBuilder(DataBuilderPtr builder);
    const DataBuilder * getDataBuilder();

    void destroyData();
protected:
    void updateData(Data* newData);
    Data * buildData();
    bool changedBuilder();

protected:
    DataBuilderPtr dataBuilder;
    pthread_key_t dataKey, dataBuilderKey;
    PthreadMutex mutex;
};

}//namespace
}//namespace

#endif // DATAHOLDER_H
