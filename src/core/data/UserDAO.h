#ifndef USERDAO_H_
#define USERDAO_H_

#include <vector>
#include <string>

#include "model/user/User.h"
#include "SetId.h"
namespace calmframe{

class UserAccount;

typedef std::vector<User> UserCollection;

class UserDAO {
public:
    virtual ~UserDAO(){}
    virtual UserCollection listUsers()=0;
    virtual UserCollection listUsersIn(const data::SetId & userIds)=0;
    virtual bool get(long userId, UserAccount & userOut) =0;
    virtual bool getUser(long userId, User & user)=0;
    virtual bool getByLogin(const std::string & userName, UserAccount & userOut) =0;
    virtual bool exist(long userId) const=0;
    virtual bool exist(const std::string & userName) const=0;
    virtual long insert(const UserAccount & userAccount) =0;
    virtual long insertAt(long userId, const UserAccount & userAccount) =0;
    virtual bool update(const User & user) =0;
    
    /** Update account related info (not change user).
        Currently fields to update:
            - password (hash and salt)
    */
    virtual bool updateAccount(long id, const UserAccount & account)=0;
    
    virtual bool remove(long userId) =0;
//    virtual long insert(const UserAccount & userAccount, long userId) =0;
};

}

#endif /* USERDAO_H_ */

