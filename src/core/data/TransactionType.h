#ifndef TRANSACTIONTYPE_H
#define TRANSACTIONTYPE_H

namespace calmframe {

enum class TransactionType{
    Default,
    Write,
    Read
};

}//namespace

#endif // TRANSACTIONTYPE_H

