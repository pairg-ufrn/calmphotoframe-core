/*
 * Data.h
 *
 *  Created on: 29/09/2014
 *      Author: noface
 */

#ifndef DATA_H_
#define DATA_H_

#include "PhotoDAO.hpp"
#include "UserDAO.h"
#include "SessionDAO.h"
#include "PhotoCollectionDAO.h"
#include "PhotoContextDAO.h"
#include "EventsDAO.h"
#include "PermissionsDAO.h"
#include "ConfigurationsDAO.h"

#include "TransactionType.h"

#include <memory>

namespace calmframe {
namespace data {

class DataHolder;
class DataBuilder;

class Data {
public:
    using DataBuilderPtr = std::shared_ptr<DataBuilder>;

    static void setup(DataBuilderPtr dataBuilder);
    static Data & instance();
    static void destroyInstance();

protected:
    static DataHolder &getHolder();
public:
	virtual ~Data()
	{}
    virtual PhotoDAO        & photos()   = 0;
    virtual UserDAO         & users()    = 0;
    virtual SessionDAO      & sessions() = 0;
    virtual PhotoCollectionDAO & collections() = 0;
    virtual PhotoContextDAO & contexts() = 0;
    virtual EventsDAO       & events()   = 0;
    virtual PermissionsDAO  & permissions()=0;
    virtual ConfigurationsDAO & configs()=0;

    virtual bool allowTransactions() const=0;
    virtual bool isOnTransaction() const=0;
    virtual void beginTransaction(TransactionType type=TransactionType::Default) = 0;
    virtual void commit()=0;
    virtual void rollback()=0;
}; /* class Data*/

} /* namespace data */
} /* namespace calmframe */


#endif /* DATA_H_ */
