#ifndef SETID_H
#define SETID_H

#include <set>

namespace calmframe {
namespace data {
    typedef std::set<long> SetId;

}//namespace
}//namespace

#endif // SETID_H

