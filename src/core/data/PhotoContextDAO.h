#ifndef PHOTOCONTEXTDAO_H
#define PHOTOCONTEXTDAO_H

#include "model/photo/PhotoContext.h"
#include <set>
#include <vector>

namespace calmframe {
namespace data {

class PhotoContextDAO{
public:public:
    typedef std::set<long> SetId;
    typedef std::vector<PhotoContext> ContextList;
public:
    virtual ~PhotoContextDAO(){}

    /** Return the numbers of entities on database*/
    virtual long count()=0;
    virtual PhotoContext get(long contextId)=0;
    /** Return the oldest PhotoContext*/
    virtual PhotoContext getFirst()=0;
    virtual bool exist(long contextId)=0;
    virtual std::set<long> listRootCollections()=0;
    virtual long getRootCollectionId(long ctxId)=0;
    virtual bool isRootCollection(long collectionId)=0;
    virtual ContextList list(Visibility minVisibility = Visibility::Unknown)=0;
    virtual ContextList listAll()=0;
    virtual ContextList listIn(const SetId & collIds)=0;
    virtual long insert(const PhotoContext & photoContext)=0;
    //virtual long insertAt(long contextId, const PhotoContext & photoContext)=0;
    virtual bool update(const PhotoContext & photoContext)=0;
    virtual bool remove(long contextId) =0;
};

}//namespace
}//namespace
#endif // PHOTOCONTEXTDAO_H

