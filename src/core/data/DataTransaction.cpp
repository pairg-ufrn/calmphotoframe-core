#include "DataTransaction.h"

#include "data/Data.h"
#include <exception>
#include "utils/Log.h"

namespace calmframe {
namespace data {

DataTransaction::DataTransaction(Data & dataInstance, TransactionType type)
: data(dataInstance)
, finishedTransaction(false)
{
    data.beginTransaction(type);
}

DataTransaction::~DataTransaction()
{
    if(!hasFinishedTransaction()){
        logWarning("DataTransaction::destructor:\t" "Transaction not finished, rollback.");
        tryRollback();
    }
}

void DataTransaction::commit()
{
    data.commit();
    finishedTransaction = true;
}

void DataTransaction::rollback()
{
    data.rollback();
    finishedTransaction = true;
}


void DataTransaction::tryRollback()
{
    try{
        rollback();
    }
    catch(const std::exception & ex){
        MainLog().warning(std::string("Failed to finished transaction due to error: ").append(ex.what()));
    }
    catch(...){
        MainLog().warning("Failed to finished transaction due to unknown error.");
    }
}

bool DataTransaction::hasFinishedTransaction() const
{
    return finishedTransaction;
}

}//namespace
}//namespace

