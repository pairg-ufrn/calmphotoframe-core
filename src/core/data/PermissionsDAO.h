#ifndef PERMISSIONSDAO_H
#define PERMISSIONSDAO_H

#include "model/permissions/Permission.h"
#include "SetId.h"

#include <vector>

namespace calmframe {

class PermissionsDAO{
public:
    typedef std::vector<Permission> PermissionList;
public:
    virtual bool hasPermission(long userId, long ctxId, PermissionLevel minPermissionLevel = PermissionLevel::None)=0;
    /** List all permissions with at least the given permission level.*/
    virtual PermissionList list(long userId, PermissionLevel minPermissionLevel = PermissionLevel::None)=0;
    virtual PermissionList listByContext(long ctxId)=0;
    virtual void putPermission(const Permission & permission)=0;
    
    /** Get permission for user with given id at the context with given id.
      * @throws NotFoundMember exception with permission does not exist 
    */
    virtual Permission getPermission(long userId, long ctxId)=0;
    
    /** Same as #getPermission(long, long), but return false if Permission is not found. */
    virtual bool tryGetPermission(long userId, long ctxId, Permission & permission)=0;
    
    virtual calmframe::data::SetId listContextsWithPermission(long userId, PermissionLevel permissionLevel)=0;
    virtual void replaceContextPermissions(long ctxId, const PermissionList & ctxPermissions)=0;
};

}//namespace

#endif // PERMISSIONSDAO_H

