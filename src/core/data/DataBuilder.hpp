/*
 * DataBuilder.h
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#ifndef DATABUILDER_H_
#define DATABUILDER_H_


namespace calmframe {
namespace data {

//Forward-declaration
class Data;

class DataBuilder {
public:
	virtual ~DataBuilder()
	{}
	virtual Data * buildData() =0;
    virtual DataBuilder * clone() const = 0;
};

} /* namespace data */
} /* namespace calmframe */

#endif /* DATABUILDER_H_ */
