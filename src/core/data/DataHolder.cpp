#include "DataHolder.h"
#include "Data.h"

#include "utils/ScopedPthreadLock.h"

#include "exceptions/DatabaseException.h"

namespace calmframe {
namespace data {

#define SINCHRONIZE ScopedPthreadLock sinchronizeMutex(mutex)

DataHolder::DataHolder()
    :dataBuilder(NULL)
{
    pthread_key_create(&dataKey, dataDestructor);
    pthread_key_create(&dataBuilderKey, NULL);
}

void DataHolder::dataDestructor(void *data){
    if(data != NULL){
        delete static_cast<Data*>(data);
    }
}

void DataHolder::setDataBuilder(DataBuilderPtr builder){
    SINCHRONIZE;
    this->dataBuilder = builder;
}

const DataBuilder *DataHolder::getDataBuilder(){
    SINCHRONIZE;
    return this->dataBuilder.get();
}

Data *DataHolder::getData(){
    bool changed = changedBuilder();
    void * dataPointer = pthread_getspecific(dataKey);
    if(dataPointer == NULL || changed){
        SINCHRONIZE;
        if(changed){
            pthread_setspecific(dataBuilderKey, dataBuilder.get());
        }
        return buildData();
    }
    return static_cast<Data *>(dataPointer);
}

void DataHolder::destroyData(){
    SINCHRONIZE;
    updateData(nullptr);
}

bool DataHolder::changedBuilder(){
    SINCHRONIZE;
    DataBuilder * dataBuilderPtr = static_cast<DataBuilder *>(pthread_getspecific(dataBuilderKey));

    return (dataBuilderPtr != dataBuilder.get());
}

Data * DataHolder::buildData(){
    if(dataBuilder == NULL){
        throw DatabaseCreationException("Trying to get data, but DataBuilder is not set");
    }
    Data * newData = dataBuilder->buildData();
    updateData(newData);

    return newData;
}

void DataHolder::updateData(Data* newData)
{
    Data * oldData = static_cast<Data *>(pthread_getspecific(dataKey));

    pthread_setspecific(dataKey, newData);
    if(oldData != NULL){
        delete oldData;
    }
}

}//namespace
}//namespace

