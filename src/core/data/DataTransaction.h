#ifndef DATATRANSACTION_H
#define DATATRANSACTION_H

#include "TransactionType.h"

namespace calmframe {
namespace data {

class Data;

class DataTransaction
{
private:
    Data & data;
    bool finishedTransaction;
public:
    DataTransaction(Data & dataInstance, TransactionType type=TransactionType::Default);
    virtual ~DataTransaction();

    virtual void commit();
    virtual void rollback();
    bool hasFinishedTransaction() const;
    void tryRollback();
};

}//namespace
}//namespace

#endif // DATATRANSACTION_H
