#ifndef PHOTOCOLLECTIONDAO_H
#define PHOTOCOLLECTIONDAO_H

#include "SetId.h"

#include "model/photo/PhotoCollection.h"
#include "model/photo/Photo.h"
#include <vector>
#include <string>
#include <map>

namespace calmframe {
namespace data {

class PhotoCollectionDAO{
public:
    typedef std::vector<PhotoCollection> CollectionList;
    typedef std::map<long, PhotoCollection> CollectionTree;
public:
    virtual ~PhotoCollectionDAO(){}

    virtual PhotoCollection get(long collectionId){
        PhotoCollection coll;
        get(collectionId, coll);
        return coll;
    }

    virtual bool get(long collectionId, PhotoCollection & outCollection)=0;
    virtual bool exist(long collectionId)=0;
    virtual CollectionList listAll()=0;
    virtual CollectionList listIn(const SetId & collIds)=0;
    virtual CollectionList getByName(const std::string & name)=0;
    virtual CollectionList getByContext(long ctxId);
    virtual CollectionList & getByContext(long ctxId, CollectionList & list)=0;
    virtual SetId listIdsByContext(long ctxId)=0;

    virtual CollectionTree getCollectionTree(long collId){
        CollectionTree collTree;
        return getCollectionTree(collId, collTree);
    }

    virtual CollectionTree & getCollectionTree(
                                                long collId, CollectionTree & collTree)=0;

    virtual SetId getCollectionsWithoutParent() const=0;
    virtual long insert(const PhotoCollection & photoCollection)=0;
    virtual long insert(const PhotoCollection &photoCollection, long parentCollId)=0;
    virtual long insertAt(long collectionId, const PhotoCollection & photoCollection)=0;
    virtual bool update(const PhotoCollection & photoCollection)=0;
    virtual bool remove(long collectionId) =0;
    virtual int remove(const SetId & collectionIds)=0;
    virtual int removeCollectionsWithoutParent()=0;


    virtual std::set<long> getPhotosWithoutParent(const std::set<long> & photoIds)=0;
//    virtual std::vector<Photo> getPhotos(long collectionId)=0;
    virtual SetId getCollectionPhotos(long collectionId)=0;
    virtual CollectionList getSubCollections(long parentCollectionId)=0;

    virtual void addPhoto(long photoId, long collectionId) =0;
    virtual void addPhotos(const SetId & collectionIds, const SetId & photoIds)=0;
    /** Remove photo from all collections*/
    virtual void removePhotoFromCollections(long photoId)=0;
    virtual void removePhotosFromCollections(const SetId & collectionIds, const SetId & photoIds)=0;

    virtual void addItemsToCollections(const SetId & targetCollections
                                  , const SetId & photoIds, const SetId & subCollections)=0;
    virtual void removeItemsFromCollections(const SetId & targetCollections
                                  , const SetId & photoIds, const SetId & subCollections)=0;

    virtual void moveCollectionItems(long collectionId, const SetId & targetCollections
                                             , const SetId & photosToMove
                                             , const SetId & subCollectionsToMove) = 0;


};

inline PhotoCollectionDAO::CollectionList PhotoCollectionDAO::getByContext(long ctxId)
{
    CollectionList list;
    return this->getByContext(ctxId, list);
}

}//namespace
}//namespace

#endif // PHOTOCOLLECTIONDAO_H

