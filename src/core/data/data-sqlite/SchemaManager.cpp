#include "SchemaManager.h"

#include "Sql.h"
#include "SqliteConnector.h"
#include "ScopedTransaction.h"
#include "Query.h"
#include "dao/GenericDAO.hpp"
#include "parsers/ColumnsParser.h"
#include "parsers/ForeignKeysParser.h"

#include "utils/StringCast.h"
#include "utils/StringUtils.h"
#include "utils/CommonExceptions.h"

#include "utils/Log.h"

using namespace std;
using namespace calmframe::data;
using namespace calmframe::utils;
using namespace calmframe::Sql;

namespace calmframe {

SchemaManager::SchemaManager(data::SqliteConnector * connector)
    : connector(connector)
{}

bool SchemaManager::existTable(const string & tableName){
    return connector->existTable(tableName);
}

data::SqliteConnector &SchemaManager::getConnector(){
    CHECK_NOT_NULL(this->connector);
    return *connector;
}

data::Table SchemaManager::readTable(const std::string & tableName){
    Table table(tableName);

    auto columns = readColumns(tableName);
    for(const auto & c : columns){
        table.putColumn(c);
    }

    auto FKs = readFKs(tableName);
    for(const auto & fk : FKs){
        table.addForeignKey(fk);
    }

    return table;
}

std::vector<Column> SchemaManager::readColumns(const std::string & tableName){
    Query columnsQuery(getConnector(), PRAGMAFunction(Pragmas::tableInfo, tableName));

    std::vector<Column> columns;

    GenericDbReader<Column, ColumnsParser> reader(&columns);
    columnsQuery.read(reader);

    return columns;
}


SchemaManager::ForeignKeyList SchemaManager::readFKs(const std::string & tableName){
    Query FKsQuery(getConnector(), PRAGMAFunction(Pragmas::foreignKeyList, tableName));

    ForeignKeyList foreignKeys;

    GenericDbReader<ForeignKey, ForeignKeysParser> reader(&foreignKeys);
    FKsQuery.read(reader);

    return foreignKeys;
}

void SchemaManager::copyTable(const std::string & srcTable, const ColumnList & srcColumns, const std::string& dstTable, const ColumnList & dstColumns)
{
    auto copyTableStmt = Table::insertIntoTablePrefix(dstTable, dstColumns)
                         + " " +
                         Table::getQueryStatement(srcTable, srcColumns);
    getConnector().execute(copyTableStmt);
}

void SchemaManager::updateColumns(const string & tableName, const std::map<string, Column> & columnsToUpdate){
    updateTable(tableName, columnsToUpdate, {});
}

void SchemaManager::updateForeignKeys(const string & tableName, const std::map<string, ForeignKey> & FKsToUpdate){
    updateTable(tableName, {}, FKsToUpdate);
}

void SchemaManager::updateTable(const string& tableName, 
    const std::map<string, Column>& columnsToUpdate, 
    const std::map<std::string, ForeignKey> & FKsToUpdate)
{
    ScopedTransaction transaction(connector, TransactionType::Write);

    Table oldTable = readTable(tableName);
    auto oldColumns = oldTable.getColumnsList();

    //Change table
    Table changedTable = oldTable;
    changeColumns(changedTable, columnsToUpdate);
    changeForeignKeys(changedTable, FKsToUpdate);

    //Rename old table
    const string tmpName = tableName + "_tmp";
    renameTable(tableName, tmpName);
    oldTable.setName(tmpName);

    //Create new table - with old name
    getConnector().execute(changedTable.getCreateStatement());

    //Copy content from source table to target table
    auto newColumns = replaceColumnNames(oldColumns, columnsToUpdate);
    copyTable(tmpName, oldColumns, tableName, newColumns);
    
    //Drop old table
    dropTable(tmpName);

    transaction.commit();    
}


void SchemaManager::changeColumns(Table & changedTable, const std::map<string, Column>& columnsToUpdate){
    for(auto & pair: columnsToUpdate){
        changedTable.removeColumn(pair.first);
        changedTable.putColumn(pair.second);
    }    
}

void SchemaManager::changeForeignKeys(Table & changedTable, const std::map<std::string, ForeignKey>& FKsToUpdate){
    for(const auto & colToFK : FKsToUpdate){
        changedTable.removeForeignKey(colToFK.first);
        changedTable.addForeignKey(colToFK.second);
    }    
}

ColumnList SchemaManager::replaceColumnNames(const ColumnList & oldColumns,
                                            const std::map<string, Column>& columnsToUpdate)
{
    auto newColumns = oldColumns;
    for(auto & columnName : newColumns){
        auto columnToChange = columnsToUpdate.find(columnName);
        if(columnToChange != columnsToUpdate.end()){
            //Update current name to (possibly) new column name
            columnName.assign(columnToChange->second.getName());
        }
    }

    return newColumns;
}

void SchemaManager::renameTable(const string & tableName, const string & newName){
    Statement renameTableStmt(getConnector(), "ALTER TABLE " + tableName + " RENAME TO " + newName);
    renameTableStmt.execute();
}

void SchemaManager::dropTable(const string & tableName){
    Statement dropTableStmt(getConnector(), "DROP TABLE " + tableName);
    dropTableStmt.execute();
}

}//namespace

