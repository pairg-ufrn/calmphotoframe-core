#ifndef GENERICDBWRITER_H
#define GENERICDBWRITER_H

#include "data/data-sqlite/DatabaseWriter.h"
#include "data/data-sqlite/Statement.h"

#include "utils/IllegalArgumentException.hpp"

namespace calmframe {

template<typename T, typename DbParser>
class GenericDbWriter : public data::DatabaseWriter
{
private:
    const T * dataToWrite;
    int writeCount;
    DbParser dbParser;
public:
    GenericDbWriter(const T * dataToWrite);
    ~GenericDbWriter();

    virtual bool hasNext() const;
    virtual void writeRow(calmframe::data::Statement & statement);
};



namespace Errors {
    static const char * IllegalArgumentException_NullData = "Data pointer cannot be NULL.";
}

template<typename T, typename DbParser>
GenericDbWriter<T, DbParser>::GenericDbWriter(const T *_dataToWrite)
    : dataToWrite(_dataToWrite)
    , writeCount(1)
{
    if(dataToWrite== NULL){
        throw CommonExceptions::IllegalArgumentException(Errors::IllegalArgumentException_NullData);
    }
}

template<typename T, typename DbParser>
GenericDbWriter<T, DbParser>::~GenericDbWriter()
{}

template<typename T, typename DbParser>
bool GenericDbWriter<T, DbParser>::hasNext() const{
    return writeCount > 0;
}

template<typename T, typename DbParser>
void GenericDbWriter<T, DbParser>::writeRow(data::Statement &statement){
    this->dbParser.writeStatement(*dataToWrite, statement);
    --writeCount;
}

}//namespace

#endif // GENERICDBWRITER_H
