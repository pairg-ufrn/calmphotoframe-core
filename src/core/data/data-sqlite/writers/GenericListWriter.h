#ifndef GENERICLISTWRITER_H
#define GENERICLISTWRITER_H

#include "data/data-sqlite/DatabaseWriter.h"

namespace calmframe {
template<typename T, typename DbParser, typename Iterator>
class GenericListWriter: public data::DatabaseWriter
{
private:
    Iterator iter;
    const Iterator begin, end;
    DbParser dbParser;
public:
    GenericListWriter(const Iterator & begin, const Iterator & end);

    virtual bool hasNext() const;
    virtual void writeRow(calmframe::data::Statement & statement);
};

template<typename T, typename DbParser, typename Iterator>
GenericListWriter<T, DbParser, Iterator>::GenericListWriter(const Iterator &begin, const Iterator &end)
    : iter(begin)
    , begin(begin)
    , end(end)
{}

template<typename T, typename DbParser, typename Iterator>
bool GenericListWriter<T, DbParser, Iterator>::hasNext() const{
    return iter != end;
}

template<typename T, typename DbParser, typename Iterator>
void GenericListWriter<T, DbParser, Iterator>::writeRow(calmframe::data::Statement &statement){
    this->dbParser.writeStatement(*iter, statement);
    ++iter;
}

}//namespace

#endif // GENERICLISTWRITER_H
