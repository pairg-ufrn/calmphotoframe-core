#ifndef DBVERSIONREADER_H
#define DBVERSIONREADER_H

#include "data/data-sqlite/DatabaseReader.h"

namespace calmframe {

class DbVersionReader : public data::DatabaseReader{
public:
    DbVersionReader();

    void readRow(const QueryData & data);

    int getVersion() const;
private:
    int version;
};

}//namespace

#endif // DBVERSIONREADER_H
