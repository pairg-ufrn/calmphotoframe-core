#ifndef GENERICDBREADER_H
#define GENERICDBREADER_H

#include <vector>
#include "data/data-sqlite/DatabaseReader.h"
#include "utils/IllegalArgumentException.hpp"

namespace calmframe {

class QueryData;

template<typename T, typename DbParser>
class GenericDbReader : public data::DatabaseReader
{
private:
    int readLimit;
    int readCount;
    std::vector<T> * data;
    DbParser dbParser;
public:
    GenericDbReader(std::vector<T> * data, int readLimit = -1);

    bool hasFinished() const;

    virtual void readRow(const QueryData & data);
    void resetRead();
    int getReadLimit() const;
    void setReadLimit(int value);

protected:
    void assertDataNotNullArgument(std::vector<T>* data);
};

template<typename T, typename DbParser>
GenericDbReader<T, DbParser>::GenericDbReader(std::vector<T> *_data, int _readLimit)
    : readLimit(_readLimit)
    , readCount(0)
    , data(_data)
{
    assertDataNotNullArgument(data);
}

template<typename T, typename DbParser>
int GenericDbReader<T, DbParser>::getReadLimit() const{
    return readLimit;
}

template<typename T, typename DbParser>
void GenericDbReader<T, DbParser>::setReadLimit(int value){
    readLimit = value;
}

template<typename T, typename DbParser>
bool GenericDbReader<T, DbParser>::hasFinished() const{
    return readLimit > 0 && readCount >= readLimit;
}

template<typename T, typename DbParser>
void GenericDbReader<T, DbParser>::readRow(const QueryData &rowData){
    T data;
    dbParser.readRow(rowData, &data);
    this->data->push_back(data);
    ++readCount;
}

template<typename T, typename DbParser>
void GenericDbReader<T, DbParser>::resetRead(){
    readCount = 0;
}

template<typename T, typename DbParser>
void GenericDbReader<T, DbParser>::assertDataNotNullArgument(std::vector<T> *data){
    if(data == NULL){
        throw CommonExceptions::IllegalArgumentException("Vector pointer should not be NULL.");
    }
}

}

#endif // GENERICDBREADER_H
