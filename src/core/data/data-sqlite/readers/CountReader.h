#ifndef COUNTREADER_H
#define COUNTREADER_H

#include "data/data-sqlite/DatabaseReader.h"

namespace calmframe {

class CountReader : public data::DatabaseReader
{
private:
    int limiter;
    int counter;
public:
    CountReader(const int & limiter=-1);
    ~CountReader();

    virtual bool hasFinished() const;
    virtual void readRow(const QueryData & data);

    void reset();

    const int & getCounter() const;
    int getLimiter() const;
    void setLimiter(int value);
};

}

#endif // COUNTREADER_H
