#include "CountReader.h"

namespace calmframe {

int CountReader::getLimiter() const
{
    return limiter;
}

void CountReader::setLimiter(int value)
{
    limiter = value;
}
CountReader::CountReader(const int &_limiter)
    : limiter(_limiter)
    , counter(0)
{}

CountReader::~CountReader()
{

}

bool CountReader::hasFinished() const{
    if(limiter >= 0){
        return !(counter < limiter);
    }
    //Se não tiver limitador, conta até fim dos dados
    return false;
}

void CountReader::readRow(const QueryData &){
    ++counter;
}

void CountReader::reset(){
    counter = 0;
}

const int &CountReader::getCounter() const{
    return this->counter;
}

}
