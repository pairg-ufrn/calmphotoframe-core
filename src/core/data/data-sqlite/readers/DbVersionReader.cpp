#include "DbVersionReader.h"

namespace calmframe {

DbVersionReader::DbVersionReader() : version(0)
{}

void DbVersionReader::readRow(const QueryData & data){
    this->version = data.getInt(0);
}

int DbVersionReader::getVersion() const{
    return version;
}

}//namespace
