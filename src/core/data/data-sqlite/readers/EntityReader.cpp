#include "EntityReader.h"

#include "../QueryData.h"

namespace calmframe {

void EntityReader::readRow(const QueryData & data){
    Entity e;
    for(int i=0; i < data.getColumnsCount(); ++i){
        e[data.getColumnName(i)] = data.getString(i);
    }
    
    entities.push_back(e);
}


}//namespace
