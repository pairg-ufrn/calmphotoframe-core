#ifndef ENTITYREADER_H
#define ENTITYREADER_H

#include "../DatabaseReader.h"

#include <vector>

namespace calmframe {

class EntityReader : public data::DatabaseReader{
public:
    using Entity = std::map<std::string, std::string>;
    std::vector<Entity> entities;

    void reset(){
        entities.clear();
    }

    virtual void readRow(const QueryData & data);
};

}//namespace

#endif // ENTITYREADER_H
