#ifndef FOREIGNKEY_H
#define FOREIGNKEY_H

#include <string>

namespace ForeignKeyConstants{
namespace Constants {
    static const char * FOREIGN_KEY = "FOREIGN KEY";
    static const char * REFERENCES = "REFERENCES";
    static const char * COL_DELIM_START = "(";
    static const char * COL_DELIM_END   = ")";
    static const char * ON_UPDATE = " ON UPDATE ";
    static const char * ON_DELETE = " ON DELETE ";
}
namespace Actions {
    static const char * NO_ACTION   = "NO ACTION";
    static const char * RESTRICT    = "RESTRICT";
    static const char * SET_NULL    = "SET NULL";
    static const char * SET_DEFAULT = "SET DEFAULT";
    static const char * CASCADE     = "CASCADE";
}//namespace
}
class ForeignKey
{
private:
    std::string localColumn;
    std::string foreignTable;
    std::string foreignKey;
    std::string onUpdateAction;
    std::string onDeleteAction;
public:
    ForeignKey();
    ForeignKey(const std::string & localColumn,
               const std::string & foreignTable, const std::string & foreignKey);

    std::string toString() const;

    friend std::ostream & operator<<(std::ostream & outStream, const ForeignKey &  foreignKey);

    const std::string & getLocalColumn() const;
    void setLocalColumn(const std::string &value);
    const std::string & getForeignTable() const;
    void setForeignTable(const std::string &value);
    const std::string & getForeignKey() const;
    void setForeignKey(const std::string &value);
    const std::string & getOnUpdateAction() const;
    ForeignKey & setOnUpdateAction(const std::string &value);
    const std::string & getOnDeleteAction() const;
    ForeignKey & setOnDeleteAction(const std::string &value);
};

#endif // FOREIGNKEY_H
