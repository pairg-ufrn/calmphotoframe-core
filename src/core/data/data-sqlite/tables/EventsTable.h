#ifndef EVENTSTABLE_H
#define EVENTSTABLE_H

#include "CommonTable.h"

namespace Tables {
namespace EventsTable {
    void buildTable(calmframe::data::Table & table);

    const std::string NAME = "Events";
namespace Columns {
    const std::string id        = CommonTable::Columns::ID;
    const std::string type      = "type";
    const std::string entityId  = "entityId";
    const std::string userId    = "userId";
    const std::string createdAt = CommonTable::Columns::CREATEDAT;
}//namespace
}//namespace
}//namespace
#endif // EVENTSTABLE_H
