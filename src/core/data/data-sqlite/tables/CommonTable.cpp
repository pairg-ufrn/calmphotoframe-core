#include "CommonTable.h"

#include "data/data-sqlite/Table.h"

namespace Tables {

using namespace calmframe::data;
using namespace CommonTable::Columns;

void CommonTable::buildTable(Table &table, const std::string & tableName, bool timestampColumns)
{
    table.setName(tableName);

    Column idColumn(Columns::ID , ColumnTypes::INTEGER);
    idColumn.addConstraint(ColumnConstraints::PRIMARY_KEY());
    table.setIdColumn(Columns::ID);

    table.putColumn(idColumn);

    if(timestampColumns){
        putTimestampColumns(table);
    }
}

void CommonTable::putTimestampColumns(Table &table){
    table.putColumn(Column(Columns::UPDATEDAT, ColumnTypes::TIMESTAMP, DEFAULT_TIMESTAMP), false, true);
    table.putColumn(Column(Columns::CREATEDAT, ColumnTypes::TIMESTAMP, DEFAULT_TIMESTAMP));
}

}//namespace
