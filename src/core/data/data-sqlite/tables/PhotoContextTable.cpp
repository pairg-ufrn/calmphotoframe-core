#include "PhotoContextTable.h"
#include "PhotoCollectionTable.h"

#include "data/data-sqlite/Table.h"

namespace Tables {
namespace PhotoContextTable {

using calmframe::data::Table;

Table & buildTable(Table &table)
{
    CommonTable::buildTable(table, NAME);

    table.putColumn(Column(Columns::NAME, ColumnTypes::TEXT), true, true);
    table.putColumn(Column(Columns::VISIBILITY, ColumnTypes::INTEGER), true, true);
    table.putColumn(Column(Columns::ROOT_PHOTOCOLLECTION, ColumnTypes::INTEGER), true, true);

    table.setIdColumn(Columns::ID);
    table.addForeignKey(Columns::ROOT_PHOTOCOLLECTION
                                , Tables::PhotoCollectionTable::NAME
                        , Tables::PhotoCollectionTable::Columns::ID);

    return table;
}

Table && buildTable(calmframe::data::Table && table){
    buildTable((Table &)table);
    return std::move(table);
}

}//namespace
}//namespace
