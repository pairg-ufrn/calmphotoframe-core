#ifndef PERMISSIONSTABLE_H
#define PERMISSIONSTABLE_H

#include "../Table.h"

namespace Tables {
namespace PermissionsTable {
    void buildTable(calmframe::data::Table & table);
    calmframe::data::Table && buildTable(calmframe::data::Table && table);

    const std::string NAME = "Permissions";
namespace Columns {
    const char * const contextId        = "contextId";
    const char * const userId           = "userId";
    const char * const permissionLevel  = "permissionLevel";
}//namespace
}//namespace
}//namespace


#endif // PERMISSIONTABLE_H
