#include "EventsTable.h"

#include "CommonTable.h"
#include "UserTable.h"
#include "data/data-sqlite/Table.h"

namespace Tables {

namespace EventsTable {

using namespace calmframe::data;

void buildTable(Table & table){
    CommonTable::buildTable(table, NAME, false);

    table.putColumn({Columns::type, ColumnTypes::INTEGER}, true, false);
    table.putColumn({Columns::userId, ColumnTypes::INTEGER}, true, false);
    table.putColumn({Columns::entityId, ColumnTypes::INTEGER}, true, false);
    table.putColumn({Columns::createdAt, ColumnTypes::TIMESTAMP}, true, false);

    ForeignKey & userFk = table.addForeignKey(Columns::userId, UserTable::NAME, UserTable::Columns::ID);
    userFk.setOnDeleteAction(ForeignKeyConstants::Actions::SET_NULL);
    userFk.setOnUpdateAction(ForeignKeyConstants::Actions::CASCADE);
}

}//namespace
}//namespace
