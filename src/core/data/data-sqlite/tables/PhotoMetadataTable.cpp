#include "PhotoMetadataTable.h"

#include "PhotoTable.h"

#include "../Table.h"

namespace Tables {

void PhotoMetadataTable::buildTable(calmframe::data::Table & photoMetadataTable)
{
    //Not creating timestamp columns, because it is already associated with the Photo table (TODO: review that)
    CommonTable::buildTable(photoMetadataTable, NAME, false);

    photoMetadataTable.putColumn(Column(Columns::IMAGESIZE, ColumnTypes::INTEGER), true, true);
    photoMetadataTable.putColumn(Column(Columns::IMAGEWIDTH, ColumnTypes::INTEGER), true, true);
    photoMetadataTable.putColumn(Column(Columns::IMAGEHEIGHT, ColumnTypes::INTEGER), true, true);
    photoMetadataTable.putColumn(Column(Columns::CAMERAMAKER, ColumnTypes::TEXT), true, true);
    photoMetadataTable.putColumn(Column(Columns::CAMERAMODEL, ColumnTypes::TEXT), true, true);
    photoMetadataTable.putColumn(Column(Columns::ORIGINALDATE, ColumnTypes::TIMESTAMP), true, true);
    photoMetadataTable.putColumn(Column(Columns::ORIGINALFILENAME, ColumnTypes::TEXT), true, true);
    photoMetadataTable.putColumn(Column(Columns::LOCATION_LATITUDE, ColumnTypes::REAL), true, true);
    photoMetadataTable.putColumn(Column(Columns::LOCATION_LONGITUDE, ColumnTypes::REAL), true, true);
    photoMetadataTable.putColumn(Column(Columns::LOCATION_ALTITUDE, ColumnTypes::REAL), true, true);
    photoMetadataTable.putColumn(Column(Columns::ORIENTATION_FLAG, ColumnTypes::INTEGER), true, true);

    photoMetadataTable.addForeignKey(Columns::ID,
                                           PhotoTable::NAME, PhotoTable::Columns::ID);
}

}//namespace
