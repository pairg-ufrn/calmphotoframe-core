#include "MarkersTable.h"

#include "../Table.h"
#include "../Column.h"

#include "PhotoTable.h"

#include <string>

using namespace std;
using namespace calmframe::data;

namespace Tables {
namespace MarkersTable {

void buildTable(calmframe::data::Table & table){
    table.setName(NAME);

    Column photoIdColumn(Columns::PHOTO_ID, ColumnTypes::INTEGER);
    Column indexColumn(Columns::INDEX     , ColumnTypes::INTEGER);

    photoIdColumn.addConstraint(ColumnConstraints::NOT_NULL());
    indexColumn.addConstraint(ColumnConstraints::NOT_NULL());

    table.putColumn(photoIdColumn, true, true);
    table.putColumn(indexColumn, true, true);
    table.putColumn(Column(Columns::IDENTIFICATION, ColumnTypes::TEXT), true, true);
    table.putColumn(Column(Columns::RELATIVEX     , ColumnTypes::REAL), true, true);
    table.putColumn(Column(Columns::RELATIVEY     , ColumnTypes::REAL), true, true);

    CommonTable::putTimestampColumns(table);

    string keys[] = {Columns::PHOTO_ID, Columns::INDEX};
    table.addConstraint(ColumnConstraints::buildPrimaryKey(keys, 2));

    table.addForeignKey(Columns::PHOTO_ID, PhotoTable::NAME, PhotoTable::Columns::ID);
}


}//namespace
}//namespace
