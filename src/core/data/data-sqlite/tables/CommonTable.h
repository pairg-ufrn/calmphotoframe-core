#ifndef COMMONTABLE_H
#define COMMONTABLE_H

#include <string>

namespace calmframe {
namespace data {
    class Table;
}//namespace
}//namespace

namespace Tables {
namespace CommonTable {
    /** Create a base table with given name
        @param timestampColumns - set to false to not create columns 'createdAt' and 'updatedAt' */
    void buildTable(calmframe::data::Table & table, const std::string & tableName, bool timestampColumns=true);
    void putTimestampColumns(calmframe::data::Table &table);

    constexpr const char * const DEFAULT_TIMESTAMP = "CURRENT_TIMESTAMP";
namespace Columns {
    constexpr const char * const ID = "id";
    constexpr const char * const CREATEDAT   = "createdAt";
    constexpr const char * const UPDATEDAT   = "updatedAt";

}//namespace
}//namespace
}//namespace

#endif // COMMONTABLE_H
