#ifndef USERTABLE_H
#define USERTABLE_H

#include <vector>
#include <string>

#include "CommonTable.h"

namespace Tables{
    using calmframe::data::Table;

namespace UserTable {
    constexpr const char * const NAME = "User";
namespace Columns {
    constexpr const char * const ID         = CommonTable::Columns::ID;
    constexpr const char * const LOGIN      = "login";
    constexpr const char * const NAME       = "name";
    constexpr const char * const SALT       = "salt";
    constexpr const char * const PASS_HASH  = "passHash";
    constexpr const char * const ACTIVE     = "active";
    constexpr const char * const ISADMIN    = "isAdmin";
    constexpr const char * const PROFILE_IMAGE = "profileImage";
}
    const std::vector<std::string> & getUserColumns();
    const std::vector<std::string> & updateAccountOnlyColumns();
    void buildTable(Table & userTable);
}
}

#endif // USERTABLE_H

