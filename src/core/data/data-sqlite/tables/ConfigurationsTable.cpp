#include "ConfigurationsTable.h"

#include "data/data-sqlite/Column.h"

namespace Tables {
namespace ConfigurationsTable {

void buildTable(calmframe::data::Table & table){
    table.setName(NAME);

    Column keyColumn{Columns::key, ColumnTypes::TEXT};
    keyColumn.addConstraint(ColumnConstraints::UNIQUE());
    
    table.putColumn(keyColumn, true, true);
    table.putColumn({Columns::value, ColumnTypes::TEXT}, true, true);
}


}//namespace
}//namespace
