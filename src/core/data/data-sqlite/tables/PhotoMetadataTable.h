#ifndef PHOTOMETADATATABLE_H
#define PHOTOMETADATATABLE_H

#include "CommonTable.h"

namespace Tables {
namespace PhotoMetadataTable {

    void buildTable(calmframe::data::Table & table);

    const char * const NAME        = "PhotoMetadata";
namespace Columns {
    const char * const ID                   = "id";
    const char * const IMAGESIZE            = "imageSize";
    const char * const IMAGEWIDTH           = "imageWidth";
    const char * const IMAGEHEIGHT          = "imageHeight";
    const char * const CAMERAMODEL          = "cameraModel";
    const char * const CAMERAMAKER          = "cameraMaker";
    const char * const ORIGINALDATE         = "originalDate";
    const char * const ORIGINALFILENAME     = "originalFilename";
    const char * const LOCATION_LATITUDE    = "locationLatitude";
    const char * const LOCATION_LONGITUDE   = "locationLongitude";
    const char * const LOCATION_ALTITUDE    = "locationAltitude";
    const char * const ORIENTATION_FLAG     = "orientationFlag";
}
}
}

#endif // PHOTOMETADATATABLE_H
