#ifndef MARKERSTABLE_H
#define MARKERSTABLE_H

#include "CommonTable.h"

namespace Tables {
namespace MarkersTable {
    void buildTable(calmframe::data::Table & table);

    constexpr const char * const NAME = "PhotoMarker";
namespace Columns {
    constexpr const char * const PHOTO_ID         = "photoId";
    constexpr const char * const INDEX            = "photoIndex";
    constexpr const char * const IDENTIFICATION   = "name";
    constexpr const char * const RELATIVEX        = "relativeX";
    constexpr const char * const RELATIVEY        = "relativeY";
    constexpr const char * const CREATED_AT       = CommonTable::Columns::CREATEDAT;
    constexpr const char * const UPDATED_AT       = CommonTable::Columns::UPDATEDAT;
}//namespace
}//namespace
}//namespace

#endif // MARKERSTABLE_H
