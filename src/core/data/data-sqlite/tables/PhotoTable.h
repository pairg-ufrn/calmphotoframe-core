#ifndef PHOTOTABLE_H
#define PHOTOTABLE_H

#include "CommonTable.h"

namespace Tables {
namespace PhotoTable {

    void buildTable(calmframe::data::Table & table);

    const char * const NAME        = "Photo";
namespace Columns {
    constexpr const char * const ID          = "id";
    constexpr const char * const ContextId   = "contextId";
    constexpr const char * const IMAGEPATH   = "imagePath";
    constexpr const char * const DESCRIPTION = "description";
    constexpr const char * const MOMENT 	 = "moment";
    constexpr const char * const DATE        = "date";
    constexpr const char * const PLACE 	     = "place";
    constexpr const char * const CREDITS	 = "credits";
    constexpr const char * const CREATEDAT   = "createdAt";
    constexpr const char * const UPDATEDAT   = "updatedAt";
}
}
}

#endif // PHOTOTABLE_H

