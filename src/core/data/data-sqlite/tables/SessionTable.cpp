#include "SessionTable.h"

#include "UserTable.h"

namespace Tables{
namespace SessionTable {

    void buildTable(calmframe::data::Table & table){
            
        table.setName(SessionTable::NAME);
    
        Column idColumn(SessionTable::Columns::ID , ColumnTypes::INTEGER);
        idColumn.addConstraint(ColumnConstraints::PRIMARY_KEY());
    
        table.putColumn(idColumn);
        table.putColumn(Column(SessionTable::Columns::TOKEN           , ColumnTypes::TEXT));
        table.putColumn(Column(SessionTable::Columns::EXPIRE_TIMESTAMP, ColumnTypes::INTEGER));
        table.putColumn(Column(SessionTable::Columns::USER_ID, ColumnTypes::TEXT));
    
        table.addForeignKey(SessionTable::Columns::USER_ID, UserTable::NAME, UserTable::Columns::ID)
            .setOnUpdateAction(ForeignKeyConstants::Actions::CASCADE)
            .setOnDeleteAction(ForeignKeyConstants::Actions::CASCADE);
    }
}
}
