#include "UserTable.h"

#include "CommonTable.h"
#include "data/data-sqlite/Table.h"

using namespace std;

namespace Tables {
namespace UserTable {


const std::vector<std::string> &getUserColumns()
{
    static const std::vector<std::string> columns{
        Columns::ID, 
        Columns::LOGIN, 
        Columns::NAME, 
        Columns::ACTIVE, 
        Columns::ISADMIN,
        Columns::PROFILE_IMAGE};

    return columns;
}

const std::vector<string> & updateAccountOnlyColumns(){
    static const std::vector<string> colums{Columns::SALT, Columns::PASS_HASH};
    
    return colums;
}

void buildTable(Table & userTable)
{
    CommonTable::buildTable(userTable, UserTable::NAME);

    Column nameColumn(UserTable::Columns::LOGIN, ColumnTypes::TEXT);
    userTable.putColumn(nameColumn, true, true).addConstraint(ColumnConstraints::UNIQUE());

    userTable.putColumn(Column(UserTable::Columns::NAME     , ColumnTypes::TEXT), true, true);

    userTable.putColumn(Column(UserTable::Columns::SALT     , ColumnTypes::TEXT), true, true);
    userTable.putColumn(Column(UserTable::Columns::PASS_HASH, ColumnTypes::TEXT), true, true);

    userTable.putColumn(Column(UserTable::Columns::ACTIVE, ColumnTypes::INTEGER), true, true);
    userTable.putColumn(Column(UserTable::Columns::ISADMIN, ColumnTypes::INTEGER), true, true);
    
    userTable.putColumn(Column(UserTable::Columns::PROFILE_IMAGE, ColumnTypes::TEXT), true, true);
}

}//namespace
}//namespace
