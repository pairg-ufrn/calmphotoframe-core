#ifndef CONFIGURATIONSTABLE_H
#define CONFIGURATIONSTABLE_H

#include "../Table.h"

namespace Tables {
namespace ConfigurationsTable {
    void buildTable(calmframe::data::Table & table);

    const std::string NAME = "Configurations";
namespace Columns {
    const char * const key        = "key";
    const char * const value      = "value";
}//namespace
}//namespace
}//namespace

#endif // CONFIGURATIONSTABLE_H
