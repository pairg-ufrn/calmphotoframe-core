#include "PhotoTable.h"

#include "../Table.h"
#include "PhotoContextTable.h"

namespace Tables {
namespace PhotoTable {

void buildTable(calmframe::data::Table & photoTable){
    CommonTable::buildTable(photoTable, NAME);

    photoTable.putColumn(Column(Columns::ContextId  , ColumnTypes::INTEGER), true, true);
    photoTable.putColumn(Column(Columns::IMAGEPATH  , ColumnTypes::TEXT), true, true);
    photoTable.putColumn(Column(Columns::DESCRIPTION, ColumnTypes::TEXT), true, true);
    photoTable.putColumn(Column(Columns::MOMENT     , ColumnTypes::TEXT), true, true);
    photoTable.putColumn(Column(Columns::DATE       , ColumnTypes::TEXT), true, true);
    photoTable.putColumn(Column(Columns::PLACE      , ColumnTypes::TEXT), true, true);
    photoTable.putColumn(Column(Columns::CREDITS    , ColumnTypes::TEXT), true, true);

    photoTable.addForeignKey(Columns::ContextId, PhotoContextTable::NAME, PhotoContextTable::Columns::ID);
}

}//namespace
}//namespace

