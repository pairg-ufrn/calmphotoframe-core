#ifndef PHOTOCONTEXTTABLE_H
#define PHOTOCONTEXTTABLE_H

#include "CommonTable.h"

namespace calmframe{
namespace data{
    class Table;
}
}

namespace Tables {
    using calmframe::data::Table;

namespace PhotoContextTable
{
    Table & buildTable(Table & table);
    Table && buildTable(Table && table);

    constexpr const char * NAME        = "PhotoContext";
namespace Columns {
    constexpr const char * ID                   = CommonTable::Columns::ID;
    constexpr const char * NAME                 = "name";
    constexpr const char * VISIBILITY           = "visibility";
    constexpr const char * ROOT_PHOTOCOLLECTION = "rootCollection";
    constexpr const char * CREATEDAT            = CommonTable::Columns::CREATEDAT;
    constexpr const char * UPDATEDAT            = CommonTable::Columns::UPDATEDAT;
}//namespace
}//namespace

}//namespace

#endif // PHOTOCONTEXTTABLE_H
