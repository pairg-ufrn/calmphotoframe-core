#include "PhotoCollectionTable.h"

#include "data/data-sqlite/tables/PhotoContextTable.h"

using namespace calmframe::data;
using namespace std;

namespace Tables {

Table PhotoCollectionTable::buildTable(){
    Table table;
    table.setName(NAME);

    Column idColumn(Columns::ID , ColumnTypes::INTEGER);
    idColumn.addConstraint(ColumnConstraints::PRIMARY_KEY());

    table.putColumn(idColumn);
    table.putColumn(Column(Columns::NAME, ColumnTypes::TEXT), true, true);
    table.putColumn(Column(Columns::PARENT_CONTEXT, ColumnTypes::INTEGER), true, true);
    table.putColumn(Column(Columns::UPDATEDAT, ColumnTypes::TIMESTAMP, "CURRENT_TIMESTAMP"), false, true);
    table.putColumn(Column(Columns::CREATEDAT, ColumnTypes::TIMESTAMP, "CURRENT_TIMESTAMP"));

    table.addForeignKey(Columns::PARENT_CONTEXT
                         , PhotoContextTable::NAME, PhotoContextTable::Columns::ID)
                            .setOnDeleteAction(ForeignKeyConstants::Actions::CASCADE);

    return table;
}

Table PhotoCollectionItemsTable::buildTable(){
    Table table;
    table.setName(NAME);

    table.putColumn(Column(Columns::COLLECTIONID, ColumnTypes::INTEGER));
    table.putColumn(Column(Columns::ITEMID   , ColumnTypes::INTEGER));
    table.putColumn(Column(Columns::ITEMTYPE , ColumnTypes::INTEGER));
    table.putColumn(Column(Columns::CREATEDAT, ColumnTypes::TIMESTAMP, "CURRENT_TIMESTAMP"));
    table.putColumn(Column(Columns::CREATEDAT, ColumnTypes::TIMESTAMP, "CURRENT_TIMESTAMP"));

    std::string keys[] = {Columns::COLLECTIONID, Columns::ITEMID, Columns::ITEMTYPE};
    std::string pkConstraint = ColumnConstraints::buildPrimaryKey(keys, 3);
    table.addConstraint(pkConstraint);

    table.addForeignKey(Columns::COLLECTIONID
                            , PhotoCollectionTable::NAME
                            , PhotoCollectionTable::Columns::ID)
                            .setOnDeleteAction(ForeignKeyConstants::Actions::CASCADE);

    return table;
}

const std::vector<std::string> & PhotoCollectionItemsTable::getInsertColumns(){
    static vector<string> columns;
    if(columns.empty()){
        columns.push_back(Columns::COLLECTIONID);
        columns.push_back(Columns::ITEMID);
        columns.push_back(Columns::ITEMTYPE);
    }
    return columns;
}

}//namespace
