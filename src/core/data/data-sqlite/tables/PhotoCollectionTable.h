#ifndef PHOTOCOLLECTIONTABLE_H
#define PHOTOCOLLECTIONTABLE_H

#include "data/data-sqlite/Table.h"
#include <vector>
#include <string>

namespace Tables {
    using calmframe::data::Table;

namespace PhotoCollectionTable {
    Table buildTable();

    static const char * NAME         = "PhotoCollection";
namespace Columns {
    static const char * ID          = "id";
    static const char * NAME        = "name";
    static const char * PARENT_CONTEXT = "contextId";
    static const char * CREATEDAT   = "createdAt";
    static const char * UPDATEDAT   = "updatedAt";
}//namespace Columns
}//namespace PhotoCollectionTable

namespace PhotoCollectionItemsTable {
    Table buildTable();
    const std::vector<std::string> & getInsertColumns();

    static const char * NAME        = "PhotoCollectionItems";
namespace Columns {
    static const char * COLLECTIONID   = "collection_id";
    static const char * ITEMID      = "item_id";
    static const char * ITEMTYPE    = "item_type";
    static const char * CREATEDAT   = "createdAt";
    static const char * UPDATEDAT   = "updatedAt";
}
}//namespace

}//namespace Tables

#endif // PHOTOCOLLECTIONTABLE_H

