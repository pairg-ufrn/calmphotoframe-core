#include "PermissionsTable.h"

#include "PhotoContextTable.h"
#include "UserTable.h"

namespace Tables {
namespace PermissionsTable {

using namespace calmframe::data;

void buildTable(Table & table){
    table.setName(NAME);

    table.putColumn({Columns::contextId      , ColumnTypes::INTEGER}, true, false);
    table.putColumn({Columns::userId         , ColumnTypes::INTEGER}, true, false);
    table.putColumn({Columns::permissionLevel, ColumnTypes::INTEGER}, true, true);

    table.addForeignKey(Columns::contextId, PhotoContextTable::NAME, PhotoContextTable::Columns::ID)
         .setOnDeleteAction(ForeignKeyConstants::Actions::CASCADE)
         .setOnUpdateAction(ForeignKeyConstants::Actions::CASCADE);

    table.addForeignKey(Columns::userId, UserTable::NAME, UserTable::Columns::ID)
         .setOnDeleteAction(ForeignKeyConstants::Actions::CASCADE)
         .setOnUpdateAction(ForeignKeyConstants::Actions::CASCADE);

    std::string keys[] = {Columns::contextId, Columns::userId};
    table.addConstraint(ColumnConstraints::buildPrimaryKey(keys, 2));
}

Table &&buildTable(Table && table){
    buildTable(table);
    return std::move(table);
}

}//namespace
}//namespace
