#ifndef SESSIONTABLE_H
#define SESSIONTABLE_H

#include "data/data-sqlite/Table.h"

namespace Tables{
namespace SessionTable {
    void buildTable(calmframe::data::Table & table);
    
    //Table name
    static const char * NAME = "Session";
namespace Columns {
    static const char * ID               = "id";
    static const char * USER_ID          = "userId";
    static const char * TOKEN            = "token";
    static const char * EXPIRE_TIMESTAMP = "expireTimestamp";
}
}
}
#endif // SESSIONTABLE_H

