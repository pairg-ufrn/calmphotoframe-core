/*
 * SqliteData.h
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#ifndef SQLITEDATA_H_
#define SQLITEDATA_H_

#include "../Data.h"
#include "dao/SqlitePhotoDAO.h"
#include "dao/SqlitePhotoMarkerDAO.h"
#include "dao/SqlitePhotoMetadataDAO.h"
#include "dao/SqliteUserDAO.h"
#include "dao/SqliteSessionDAO.h"
#include "dao/SqlitePhotoCollectionDAO.h"
#include "dao/SqlitePhotoContextDAO.h"
#include "dao/SqliteEventsDAO.h"
#include "dao/SqlitePermissionDAO.h"
#include "dao/SqliteConfigurationsDAO.h"
#include "DatabaseMigration.h"

#include <string>

namespace calmframe {
namespace data {

class SqliteData : public Data{
public:
    static const int DB_VERSION;

    static std::string createDatabaseName(const std::string & databasePath, const std::string & databaseName
                                        , bool requireDirExist=true);

private: //TODO: use "Pointer to Implementation (PImpl)" idiom to hide dao attributes on implementation
    std::string databasePath;
	SqliteConnector database;
	SqlitePhotoMarkerDAO photoMarkerDAO;
    SqlitePhotoMetadataDAO photoMetadataDAO;
    SqlitePhotoDAO photoDAO;
    SqliteUserDAO userDAO;
    SqliteSessionDAO sessionDAO;
    SqlitePhotoCollectionDAO  collectionDAO;
    SqlitePhotoContextDAO contextDAO;
    SqliteEventsDAO eventsDAO;
    SqlitePermissionDAO permissionDAO;
    SqliteConfigurationsDAO configsDAO;

	bool databaseCreated;

    DatabaseMigration migration;
    std::vector<SqliteDAO *> daoCollection;
public:
    SqliteData();
    SqliteData(const SqliteConnector & sqliteConnector);
    SqliteData(const std::string & databaseDir, const std::string & databaseName);

    PhotoDAO & photos() override;
    UserDAO & users() override;
    SessionDAO & sessions() override;
    PhotoCollectionDAO & collections() override;
    PhotoContextDAO & contexts() override;
    EventsDAO & events() override;
    PermissionsDAO & permissions() override;
    ConfigurationsDAO & configs() override;

    virtual SqlitePhotoMetadataDAO & getPhotoMetadataDAO();
    virtual SqlitePhotoMarkerDAO & getPhotoMarkerDAO();
    virtual SqlitePhotoCollectionMappingDAO & getCollectionMappingDAO();

    std::string getDatabasePath() const;
    bool databaseExist() const;

    SqliteConnector & getDatabaseConnector();
    const SqliteConnector & getDatabaseConnector() const;

    bool allowTransactions() const override;
    bool isOnTransaction() const override;
    void beginTransaction(TransactionType type=TransactionType::Default) override;
    void commit() override;
    void rollback() override;

    void writeDatabaseVersion(long dbVersion);
    long readDatabaseVersion() const;

protected:
    SqliteData(const std::string & databasePath, int overloadHack);

private:
    void init();
    void createDatabase();
};

} /* namespace data */
} /* namespace calmframe */

#endif /* SQLITEDATA_H_ */
