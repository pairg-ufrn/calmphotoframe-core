#ifndef DATABASEWRITER_H
#define DATABASEWRITER_H

#include "Statement.h"

namespace calmframe {
namespace data {

class DatabaseWriter
{
public:
    virtual ~DatabaseWriter(){}
    virtual bool hasNext() const=0;
    virtual void writeRow(Statement & statement)=0;
};

}
}

#endif // DATABASEWRITER_H
