#ifndef QUERY_H
#define QUERY_H

#include "Statement.h"
#include "QueryData.h"

#include <string>
#include <map>
#include <vector>

namespace calmframe {
namespace data {
    class QueryArgs;
    class Table;
    class DatabaseReader;
}//namespace
}//namespace

namespace calmframe {

using calmframe::data::Table;

class Query : public data::Statement, public QueryData{
public:
    typedef data::Statement super;
    typedef std::map<std::string, int> ColumnToIndex;
    typedef std::vector<std::string> ColumnsList;
public:
    Query(const std::string & statement=std::string());
    Query(const Table & table);
    Query(const Table & table, const data::QueryArgs & args, const ColumnsList & columns = ColumnsList());
    Query(data::SqliteConnector & conn, const Table & table, const data::QueryArgs & args, const ColumnsList & columns = ColumnsList());
    Query(data::SqliteConnector & conn, const std::string & statement);

    /** Try to read a query row.
        @return true if there is data to be readed and false otherwise. */
    bool read();

    /** Read the query data using the reader, until all data be readed or the reader has finished. */
    void read(data::DatabaseReader & reader);

    std::string getString(const std::string & col, const std::string & defaultValue = std::string()) const override;
    std::string getString(int resultIndex, const std::string & defaultValue = std::string()) const override;

    long getLong(const std::string & col, long defaultValue = 0l) const override;
    long getLong(int index, long defaultValue = 0l) const override;

    int getInt(const std::string & col, int defaultValue = 0) const override;
    int getInt(int index, int defaultValue = 0) const override;

    float getFloat(const std::string & col, float defaultValue = 0.0f) const override;
    float getFloat(int index, float defaultValue = 0.0f) const override;

    double getDouble(const std::string & col, double defaultValue = 0.0f) const override;
    double getDouble(int index, double defaultValue = 0.0f) const override;

    bool hasData(const std::string & column) const override;
    bool hasData(int resultIndex) const override;

    bool columnIsNull(const std::string & column) const override;
    bool columnIsNull(int index)                  const override;

    int getColumnsCount() const override;
    std::string getColumnName(int index) const override;
public:
    bool indexIsValid(int resultIndex) const;
protected:
    int getColumnIndex(const std::string & col) const;
private:
    mutable ColumnToIndex columnToIndex;
};

}//namespace

#endif // QUERY_H
