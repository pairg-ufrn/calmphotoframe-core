#ifndef COLUMN_H
#define COLUMN_H

#include <string>
#include <vector>

namespace ColumnTypes {
    typedef std::string ColumnType;
    static const ColumnType UNKNOWN = "";
    static const ColumnType INTEGER = "INTEGER";
    static const ColumnType TEXT = "TEXT";
    static const ColumnType TIMESTAMP = "TIMESTAMP";
    static const ColumnType REAL = "REAL";
}
class ColumnConstraints {
public:
    class Constraint{
    private:
        std::string name;
        std::string value;
        std::string finalvalue;
    public:
        Constraint(const std::string & name="", const std::string & value = "")
            : name(name), value(value)
            , finalvalue(
                  name.empty() && value.empty() ? "" : std::string(name).append(" ").append(value))
        {}
        operator const std::string() const {return toString();}
        const std::string & toString() const {return finalvalue;}

        const std::string & getName()  const {return name;}
        const std::string & getValue() const {return value;}
    };

    static std::string buildPrimaryKey(const std::string * keys, int numberOfKeys);

    static Constraint DEFAULT(const std::string & defaultValue){
        return Constraint("DEFAULT", defaultValue);
    }

    static const Constraint & PRIMARY_KEY(){
        static const Constraint primaryKey("PRIMARY KEY");
        return primaryKey;
    }
    static const Constraint & NOT_NULL(){
        static const Constraint notNull("NOT NULL");
        return notNull;
    }
    static const Constraint & UNIQUE(){
        static const Constraint unique("UNIQUE");
        return unique;
    }
};

class Column
{
private:
    std::string name;
    ColumnTypes::ColumnType type;
    typedef std::vector<ColumnConstraints::Constraint> ConstraintsCollection;
    ConstraintsCollection constraints;
public:
    Column(const std::string & colName = ""
            , const ColumnTypes::ColumnType & colType = ColumnTypes::UNKNOWN);
    Column(const std::string & colName
            , const ColumnTypes::ColumnType & colType
            , const std::string & defaultValue);

    const std::string & getName() const;
    const ColumnTypes::ColumnType &getType() const;

    void setName(const std::string &value);
    void setType(const ColumnTypes::ColumnType &value);

    Column & addConstraint(const ColumnConstraints::Constraint & constraint);
    Column & removeConstraint(const ColumnConstraints::Constraint & constraint);
    Column & removeConstraint(const std::string & constraintName);
    bool hasConstraint(const std::string & constraintName) const;

    bool isPK() const;

    friend std::ostream & operator<<(std::ostream & outStream, const Column & col);

protected:
    ConstraintsCollection::iterator findConstraint(const std::string & constrName);
    ConstraintsCollection::const_iterator findConstraint(const std::string & constrName) const;
};

#endif // COLUMN_H
