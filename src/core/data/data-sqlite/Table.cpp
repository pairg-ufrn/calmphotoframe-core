#include "Table.h"

#include "QueryArgs.h"
#include "ForeignKey.h"
#include "utils/CommonExceptions.h"
#include "utils/StringUtils.h"

#include <sstream>
#include <algorithm>

#include <utils/Log.h>

using namespace std;
using namespace calmframe::utils;

namespace calmframe{
namespace data{

namespace Constants {
    static const char * CREATE_IF_NOT_EXISTS = "create table if not exists ";
    static const char * SEPARATOR = ",";
    static const char * TABLE_BEGIN = "(";
    static const char * TABLE_END = ");";
    static const char * PLACEHOLDER = "?";
    static const char * AND = " AND ";
    static const char * OR  = " OR ";
    static const char * NOT  = " NOT ";
    static const char * IN = " IN ";
    static const char * WHERE = "WHERE";
    static const char * EQUALS = "=";
    static const string AS = " AS ";

    static const char * COLS_LIST_BEGIN = "(";
    static const char * COLS_LIST_END   = ")";

    static const char * SELECT_ALL = "SELECT * FROM";
    static const char * SELECT_START = "SELECT ";

    static const char * FROM = "FROM";
    static const char * LIMIT = " LIMIT ";
    static const char * ORDER_BY = " ORDER BY ";

    static const char * INSERT = "INSERT ";
    static const char * INSERT_INTO = " INTO ";
    static const char * INSERT_VALUES = "VALUES";

    static const char * UPDATE_PREFIX = "UPDATE ";
    static const char * SET = "SET";

    static const char * DELETE_PREFIX = "DELETE FROM ";

    static const char * CONFLICT_OR = " OR ";
    static const char * IGNORE   = "IGNORE";
    static const char * REPLACE  = "REPLACE";
    static const char * ROLLBACK = "ROLLBACK";
    static const char * FAIL     = "FAIL";
    static const char * ABORT    = "ABORT";

    static const std::string UNION = " UNION ";

    static const string LPAREN = "(";
    static const string RPAREN = ")";
}

//static
string Table::getQueryStatement(const string &tableName
                              , const std::vector<string> &columns
                              , const string &whereConditionStr)
{
    return getQueryStatement(tableName, columns, QueryArgs(whereConditionStr));
}

//static
string Table::getQueryStatement(const string & tableName, const std::vector<string> & columns, const QueryArgs & queryArgs)
{
    return getQueryStmtImpl(tableName, columns, queryArgs);
}

//static
string Table::getQueryStmtImpl(const string& tableName, const ColumnList& columns, const QueryArgs& queryArgs, bool makeTemplate){
    stringstream out;
    if(!columns.empty()){
        out << Constants::SELECT_START;
        listColumns(columns, out);
        out << " " << Constants::FROM;
    }
    else{
        out << Constants::SELECT_ALL;
    }
    out << " " << tableName;

    //TODO: move this part to an operator related to QueryArgs

    if(!queryArgs.getWhereCondition().empty()){
        whereCondition(out, queryArgs.getWhereCondition());
    }

    //Order by should be placed before LIMIT clause
    if(!queryArgs.getOrderBy().empty()){
        out << Constants::ORDER_BY << queryArgs.getOrderBy();
        if(queryArgs.getOrderType() != QueryArgs::DEFAULT){
            out << " " << QueryArgs::orderTypeToString(queryArgs.getOrderType());
        }
    }

    if(makeTemplate){
        out << Constants::LIMIT << Constants::PLACEHOLDER;
    }
    else if(queryArgs.getLimit() > 0){
        out << Constants::LIMIT << queryArgs.getLimit();
    }

    return out.str();
}

//static
string Table::getInsertPrefix(OnConflict resolution)
{
    string insertPrefix(Constants::INSERT);

    if(resolution != OnConflict::DEFAULT){
        insertPrefix.append(Constants::CONFLICT_OR);
        switch (resolution) {
        case OnConflict::IGNORE :
            insertPrefix.append(Constants::IGNORE);
            break;
        case OnConflict::REPLACE :
            insertPrefix.append(Constants::REPLACE);
            break;
        case OnConflict::ROLLBACK:
            insertPrefix.append(Constants::ROLLBACK);
            break;
        case OnConflict::STOP:
            insertPrefix.append(Constants::FAIL);
            break;
        case OnConflict::ABORT_STMT :
            insertPrefix.append(Constants::ABORT);
            break;
        default:
            break;
        }
    }

    insertPrefix.append(Constants::INSERT_INTO);

    return insertPrefix;
}

//static
string Table::insertIntoTablePrefix(const string & table, const std::vector<string> & columns, OnConflict resolution)
{
    stringstream sstream;

    sstream << getInsertPrefix(resolution) << table << " ";

    listColumns(sstream, columns);

    return sstream.str();
}


//static
string Table::getInsertStatement(const string &table, const std::vector<string> &columns, OnConflict resolution)
{
    stringstream sstream;

    sstream << insertIntoTablePrefix(table, columns, resolution);
    sstream << " " << Constants::INSERT_VALUES << " " << Constants::LPAREN;
    outputPlaceholders(sstream, columns.size());
    sstream << Constants::RPAREN;

    return sstream.str();
}

//static
ostream & Table::listColumns(ostream & out, const std::vector<string> &columns){
    out << Constants::LPAREN;
//    for(size_t i=0; i  < columns.size(); ++i){
//        if(i > 0){
//            out << Constants::SEPARATOR << " ";
//        }
//        out << columns[i] << " ";
//    }

    out << StringUtils::instance().join(", ", columns);

    out << Constants::RPAREN;

    return out;
}

//static
string Table::getUpdateStatement(const string &table, const std::vector<string> &columns, const string &whereCondition){
    stringstream sstream;

    sstream << Constants::UPDATE_PREFIX << table << " " << Constants::SET << " ";

    for(size_t i=0; i  < columns.size(); ++i){
        if(i > 0){
            sstream << Constants::SEPARATOR << " ";
        }
        sstream << columns[i] << Constants::EQUALS << Constants::PLACEHOLDER;
    }

    if(!whereCondition.empty()){
        sstream << " " << Constants::WHERE << " " << whereCondition;
    }

    return sstream.str();
}

string Table::WhereIn(const string &columnName, const string &stmt)
{
    return string(columnName).append(Constants::IN).append("(").append(stmt).append(")");
}

//static
string Table::AND(const string &conditionOne, const string &conditionTwo){
    return string("(").append(conditionOne).append(Constants::AND).append(conditionTwo).append(")");
}

string Table::NOT(const string &condition)
{
    return string(Constants::NOT).append("(").append(condition).append(")");
}

string Table::AS(const string &column, const string &renameColumn)
{
    return string(column).append(Constants::AS).append(renameColumn);
}

string Table::UNION(const string &selection1, const string &selection2)
{
    return selection1 + Constants::UNION + selection2;
}

string Table::PAREN(const string &expr)
{
    return string("(").append(expr).append(")");
}

string Table::PAREN(const string & prefix, const string & expr){
    return prefix + PAREN(expr);
}

Table::Table(const string & tableName)
    : name(tableName)
{}

const std::string & Table::getIdColumn() const
{
    return idColumn;
}

void Table::setIdColumn(const std::string &value)
{
    idColumn = value;
}

string Table::whereIdEquals(long id) const
{
    return Table::WhereEquals(getIdColumn(), id);
}

//static
string Table::OR(const string &conditionOne, const string &conditionTwo){
    return string("(").append(conditionOne).append(Constants::OR).append(conditionTwo).append(")");
}

const std::string &Table::getName() const{
    return name;
}
void Table::setName(const std::string &value){
    name = value;
}

bool Table::hasMultipleKeys() const{
    return this->primaryKeys.size() > 1;
}

const ColumnList &Table::getInsertColumns() const
{
    return insertColumns;
}

void Table::addInsertColumn(const string &column)
{
    insertColumns.push_back(column);
}

void Table::removeInsertColumn(const string &column)
{
    insertColumns.erase(std::remove(insertColumns.begin(), insertColumns.end(), column), insertColumns.end());
}

const ColumnList &Table::getUpdateColumns() const
{
    return updateColumns;
}

void Table::addUpdateColumn(const string &column)
{
    updateColumns.push_back(column);
}

void Table::removeUpdateColumn(const string &column)
{
    updateColumns.erase(std::remove(updateColumns.begin(), updateColumns.end(), column), updateColumns.end());
}

bool Table::hasColumn(const string & columnName){
    return columns.find(columnName) != columns.end();    
}

template<typename ColumnType, typename ColumnMap>
static ColumnType getColumnImpl(ColumnMap&& columns, const std::string & columnName, ColumnType&& defaultColumn){
    auto&& columnIter = columns.find(columnName);
    return columnIter == columns.end() ? defaultColumn : columnIter->second;
}

Column &Table::getColumn(const string & columnName, Column && defaultValue){
    return getColumnImpl(this->columns, columnName, defaultValue);
}

const Column &Table::getColumn(const string & columnName, const Column && defaultValue) const{
    return getColumnImpl(this->columns, columnName, defaultValue);
}

Column & Table::putColumn(const Column &col, bool isInsertColumn, bool isUpdateColumn){
    Column & column = columns[col.getName()] = col;

    if(idColumn.empty() && col.hasConstraint(ColumnConstraints::PRIMARY_KEY().getName())){
        idColumn = col.getName();
    }
    if(isInsertColumn){
        this->addInsertColumn(col.getName());
    }
    if(isUpdateColumn){
        this->addUpdateColumn(col.getName());
    }

    //WARNING: primary key is only added when putting column
    if(column.isPK()){
        this->primaryKeys.insert(column.getName());
    }

    return column;
}

void Table::removeColumn(const string & columnName){
    this->columns.erase(columnName);
    this->primaryKeys.erase(columnName);
}

const ColumnsSet &Table::getPKs() const{
    return this->primaryKeys;
}

void Table::updatePKs(){
    NOT_IMPLEMENTED(Table::updatePKs);
}

void Table::addConstraint(const string &constraint)
{
    this->otherConstraints.push_back(constraint);
}

ForeignKey & Table::addForeignKey(const string &localColumn, const string &foreignTable, const string &foreignKey){
    return addForeignKey(ForeignKey(localColumn, foreignTable, foreignKey));
}

ForeignKey &calmframe::data::Table::addForeignKey(const ForeignKey & fk){
    this->foreignKeys[fk.getLocalColumn()] = fk;
    
    return foreignKeys[fk.getLocalColumn()];
}

ForeignKey Table::getForeignKey(const string & localColumn) const{
    auto iter = foreignKeys.find(localColumn);
    return iter == foreignKeys.end()? ForeignKey() : iter->second;
}

void Table::removeForeignKey(const string & localColumn){
    foreignKeys.erase(localColumn);
}

std::string Table::getCreateStatement() const{
    stringstream sstream;
    sstream << Constants::CREATE_IF_NOT_EXISTS << getName() << Constants::TABLE_BEGIN;

    outputColumns(sstream);
    outputTableConstraints(sstream, false);

    sstream << Constants::TABLE_END;

    return sstream.str();
}

string Table::getInsertStatement(OnConflict resolution) const{
    stringstream sstream;


    sstream << getInsertPrefix(resolution) << getName() << " ";
    int colCount = outputColumnsList(sstream);
    sstream << " " << Constants::INSERT_VALUES << " " << Constants::LPAREN;
    outputPlaceholders(sstream, colCount);
    sstream << Constants::RPAREN;

    return sstream.str();
}


string Table::getRemoveStatement(const string &whereCondition) const{
    stringstream sstream;
    sstream << Constants::DELETE_PREFIX << getName() << " " << Constants::WHERE << " " << whereCondition;
    return sstream.str();
}

int Table::outputColumnsList(ostream &out) const
{
    int colCount = 0;

    out << Constants::LPAREN;

    ColumnsMap::const_iterator iterator = columns.begin();
    while(iterator != columns.end()){
        if(colCount > 0){
            out << Constants::SEPARATOR << " ";
        }
        out << iterator->second.getName();

        ++colCount;
        ++iterator;
    }

    out << Constants::RPAREN;

    return colCount;
}

void Table::outputPlaceholders(ostream &out, int count)
{
    for(int i=0; i < count; ++i){
        if(i > 0){
            out << Constants::SEPARATOR;
        }
        out << Constants::PLACEHOLDER;
    }
}

void Table::outputColumns(ostream & out) const{
    bool first = true;
    ColumnsMap::const_iterator iterator = columns.begin();

    while(iterator != columns.end()){
        out << "\n\t";
        if(!first){
            out << Constants::SEPARATOR << " ";
        }
        else{
            first = false;
        }
        const Column & col = iterator->second;
        if(!hasMultipleKeys() || !col.isPK()){
            out << col;
        }
        else{
            out << Column(col).removeConstraint(ColumnConstraints::PRIMARY_KEY().getName());
        }
        ++iterator;
    }
}

void Table::outputTableConstraints(ostream &out, bool first) const{
    for(const auto & fkPair : foreignKeys){
        out << "\n\t";
        if(!first){
            out << Constants::SEPARATOR << " ";
        }
        else{
            first = false;
        }
        out << fkPair.second;
    }

    ConstraintsList::const_iterator otherConstraintsIterator = otherConstraints.begin();
    while(otherConstraintsIterator != otherConstraints.end()){
        out << "\n\t";
        if(!first){
            out << Constants::SEPARATOR << " ";
        }
        else{
            first = false;
        }
        out << *otherConstraintsIterator;
        ++otherConstraintsIterator;
    }

    if(hasMultipleKeys()){
        out << "\n\t, ";
        out << ColumnConstraints::PRIMARY_KEY().getName() << "(";
        out << StringUtils::instance().join(", ", getPKs());
        out << ")";
    }
}


ColumnList calmframe::data::Table::getColumnsList() const{
    vector<string> columnsList;

    ColumnsMap::const_iterator iterator = columns.begin();
    while(iterator != columns.end()){
        columnsList.push_back(iterator->first);

        ++iterator;
    }

    return columnsList;
}

//string Table::queryCondition(const string & queryString) const{
//    stringstream sstream;

//    queryColumnsImpl(sstream, queryString, ColumnList(), false);

//    return sstream.str();
//}

string Table::queryTemplate(const string & templateParam) const{
    return queryTemplate(templateParam, ColumnList());
}

string Table::queryTemplate(const string & templateParam, const ColumnList & columns) const{
    stringstream sstream;

    queryColumnsImpl(sstream, templateParam, columns, false);

    return sstream.str();
}

ostream & Table::queryColumnsImpl(ostream & out,
                                  const string& paramName,
                                  const ColumnList & columns,
                                  bool paramIsStrLiteral) const
{
    bool first = true;

    const ColumnList queryColumns = columns.empty() ? this->getColumnsList() : columns;

    out << Constants::LPAREN;
    for(const auto & columnName : queryColumns){
        if(!first){
            out << Constants::OR;
        }

        out << columnName << " like ";

        //"||" symbol is used to concatenation

        out << "'%' || ";
        joinParamName(out, paramName, paramIsStrLiteral);
        out << " || '%'";

        first = false;
    }
    out << Constants::RPAREN;

    return out;
}

void Table::joinParamName(ostream& out, const string& paramName, bool isStrLiteral) const
{
    if(isStrLiteral){
        out << "'" << paramName << "'";
    }
    else{
        out << paramName;
    }
}

string Table::getQueryStatement(const string &whereConditionStr) const{
    return Table::getQueryStatement(getName(), std::vector<string>(), whereConditionStr);
}
std::string Table::getQueryStatement(const std::vector<std::string> &columns, const std::string &whereConditionStr) const{
    return Table::getQueryStatement(getName(), columns, whereConditionStr);
}

std::string Table::getQueryStatement(const QueryArgs & queryArgs) const
{
    return Table::getQueryStatement(getName(), std::vector<string>(), queryArgs);
}

string Table::getQueryTemplate(const QueryArgs & queryArgs) const{
    return Table::getQueryStmtImpl(getName(), ColumnList{}, queryArgs, true);
}
std::string Table::getQueryStatement(const ColumnList & columns, const QueryArgs & queryArgs) const
{
    return Table::getQueryStatement(getName(), columns, queryArgs);
}

void Table::listColumns(const std::vector<std::string> &columns, std::ostream & out)
{
    for(size_t i=0; i<columns.size(); ++i){
        if(i!=0){
            out << Constants::SEPARATOR;
        }
        out << columns[i];
    }
}

std::ostream & Table::whereCondition(std::ostream & out, const std::string &whereCondition){
    if(!whereCondition.empty()){
        out << " " << Constants::WHERE << " " << whereCondition;
    }
    return out;
}

}
}
