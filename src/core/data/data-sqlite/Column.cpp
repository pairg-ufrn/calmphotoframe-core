#include "Column.h"

#include "utils/NullPointerException.hpp"
#include "utils/Log.h"

#include <algorithm>

using namespace std;

namespace Constants{
    static const char * PK_START      = "(";
    static const char * PK_END        = ")";
    static const char * PK_SEPARATOR  = ", ";
}

Column::Column(const std::string &colName, const ColumnTypes::ColumnType &colType)
    : name(colName)
    , type(colType)
{}

Column::Column(const std::string &colName, const ColumnTypes::ColumnType &colType
             , const string &defaultValue)
    : name(colName)
    , type(colType)
{
    this->addConstraint(ColumnConstraints::DEFAULT(defaultValue));
}

const std::string &Column::getName() const{
    return name;
}
const ColumnTypes::ColumnType &Column::getType() const{
    return type;
}
void Column::setName(const std::string &value){
    name = value;
}
void Column::setType(const ColumnTypes::ColumnType &value){
    type = value;
}

Column & Column::addConstraint(const ColumnConstraints::Constraint &constraint){
    this->constraints.push_back(constraint);
    return *this;
}

Column &Column::removeConstraint(const ColumnConstraints::Constraint & constraint){
    return removeConstraint(constraint.getName());
}

Column &Column::removeConstraint(const string & constraintName){
    auto iter = findConstraint(constraintName);
    constraints.erase(iter);

    return *this;
}

bool Column::hasConstraint(const std::string & constraintName) const{
    auto iter = findConstraint(constraintName);
    return iter != constraints.end();
}

template<typename Iter, typename Coll>
static Iter findConstraintImpl(Coll&& collection, const std::string & constrName){
    auto hasName = [&constrName](const ColumnConstraints::Constraint& constr){
        return constr.getName() == constrName;
    };

    return std::find_if(collection.begin(), collection.end(), hasName);
}

Column::ConstraintsCollection::iterator Column::findConstraint(const string & constrName){
    return findConstraintImpl<ConstraintsCollection::iterator>(this->constraints, constrName);
}

Column::ConstraintsCollection::const_iterator Column::findConstraint(const string & constrName) const{
    return findConstraintImpl<ConstraintsCollection::const_iterator>(this->constraints, constrName);
}

bool Column::isPK() const{
    //TODO: optimize this (use a field instead of a constraint)
    return hasConstraint(ColumnConstraints::PRIMARY_KEY().getName());
}
std::ostream & operator<<(std::ostream &outStream, const Column &col)
{
    outStream << col.getName() << string(" ") << col.getType();
    Column::ConstraintsCollection::const_iterator iter = col.constraints.begin();
    while(iter != col.constraints.end()){
        outStream << string(" ") << iter->toString();

        ++iter;
    }

    return outStream;
}


string ColumnConstraints::buildPrimaryKey(const string *keys, int numberOfKeys){
    CommonExceptions::assertNotNull(keys, "ColumnConstraints::Constraint::buildPrimaryKey: \t"
                                   "Parameter keys should not be NULL!");

    std::string primaryKeyConstraint(ColumnConstraints::PRIMARY_KEY());
    primaryKeyConstraint.append(Constants::PK_START);
    for(int i=0; i < numberOfKeys; ++i){
        if(i > 0){
            primaryKeyConstraint.append(Constants::PK_SEPARATOR);
        }
        primaryKeyConstraint.append(keys[i]);
    }
    primaryKeyConstraint.append(Constants::PK_END);

    return primaryKeyConstraint;
}
