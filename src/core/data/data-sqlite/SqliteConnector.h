/*
 * SqliteDatabase.h
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#ifndef SQLITEDATABASE_H_
#define SQLITEDATABASE_H_

#include <string>
#include <map>

#include "Statement.h"
#include "QueryHandler.hpp"

#include "Table.h"

#include "../TransactionType.h"

class sqlite3;

namespace calmframe {
namespace data {

class SqliteExecuteHandler{
public:
    virtual ~SqliteExecuteHandler(){}
	virtual int onExecuteSql(int colNumber, char ** colNames, char ** colData) =0;
};

class DatabaseWriter;
class DatabaseReader;
class QueryArgs;

class SqliteConnector {
private:
	static int executeCallback(void * data, int collumnCount, char **collumnData, char **collumnName);

	sqlite3 * database;
    std::string databasePath;
	int transactionCount;
public:
    static SqliteConnector createInMemoryDb();
protected:
    SqliteConnector(const std::string &databasePath, bool allowInMemory);
public:
    SqliteConnector(const std::string & databasePath);
    //Move constructor
    SqliteConnector(SqliteConnector && connector);
    virtual ~SqliteConnector();

    const std::string & getDatabasePath() const;

    void createStatement(Statement &statement, const std::string & statementStr) const;
    bool existTable(const std::string & tableName);

	bool isOpen() const;
    void open(const std::string & databasePath);
	void close();

    bool execute(SqliteExecuteHandler * handler, const std::string & statement);
    bool execute(const std::string & statement);

    void query(DatabaseReader & reader,  const Table & table, const std::string & whereCondition="") const;
    void query(DatabaseReader & reader,  const Table & table, const QueryArgs & args, const std::vector<std::string> & columns = {}) const;
    void query(DatabaseReader & reader, const std::string & statement) const;

    /** @return the inserted row id*/
	long insert(Statement & insertStatement);
    /** @return the number of inserted rows*/
    int insert(DatabaseWriter & writer, const Table & table, bool useTransaction=true);
    /** @return the number of inserted rows*/
    int insert(const std::string & table, const std::vector<std::string> & columns, DatabaseWriter & writer
               , bool useTransaction=true, OnConflict resolution = OnConflict::DEFAULT);
    int update(Statement & updateStatement);
    int update(DatabaseWriter & writer, const Table & table, const std::string & whereCondition=""
            , bool useTransaction=true);
    int update(DatabaseWriter & writer, const std::string & tableName, const std::vector<std::string> & columns
               , const std::string & whereCondition="", bool useTransaction=true);

    int remove(const Table & table, const std::string & whereCondition);
    int remove(Statement & deleteStatement);

    int executeWriteStatement(DatabaseWriter &writer, Statement & statement, bool useTransaction=true);

    long getLastInsertedId();

    void beginTransaction(TransactionType type=TransactionType::Default);
	void endTransaction();
	void commit();
	void rollback();
    bool isOnTransaction() const;

    /** Sets the max time that the database will wait in BUSY state.
        If this time is reached and the database is still busy, it will throw an exception.
    */
    void setBusyTimeout(int milliseconds);
    void tryFinalizeStatement(Statement& statement);
protected:
    void configDatabase();
    std::string beginTransactionStmt(TransactionType type);
    int executeWriteStatementImpl(DatabaseWriter &writer, Statement& statement) const;
private:
    /** Execute statement and return the number of changed rows*/
    int executeStatement(Statement & statement) const;
    bool execute(int (*callback)(void*,int,char**,char**), const std::string& statement, void * data = NULL)const;
};

} /* namespace data */
} /* namespace calmframe */

#endif /* SQLITEDATABASE_H_ */
