#ifndef DAOFORWARD_H
#define DAOFORWARD_H

namespace calmframe{
namespace data{

class SqlitePhotoDAO;
class SqlitePhotoMarkerDAO;
class SqlitePhotoMetadataDAO;

}
}

#endif // DAOFORWARD_H
