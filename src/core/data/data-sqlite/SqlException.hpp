/*
 * SqlException.h
 *
 *  Created on: 02/10/2014
 *      Author: noface
 */

#ifndef SQLEXCEPTION_H_
#define SQLEXCEPTION_H_

#include "utils/Exception.hpp"

namespace calmframe {
namespace data {

using CommonExceptions::Exception;

class SqlException : public Exception{
public:
    static const int UNKNOWN_ERROR = -1;
private:
    int errorNumber;
public:
    SqlException(const std::string & cause, int errorNumber = UNKNOWN_ERROR)
        : Exception("SqlException", cause)
        , errorNumber(errorNumber)
    {}
    SqlException(const std::string & cause, const std::exception & origin, int errorNumber = UNKNOWN_ERROR)
        : Exception("SqlException", cause, origin)
        , errorNumber(errorNumber)
    {}

    int getErrorNumber() const{
        return this->errorNumber;
    }
};

} /* namespace data */
} /* namespace calmframe */

#endif /* SQLEXCEPTION_H_ */
