#ifndef DATABASEREADER_H
#define DATABASEREADER_H

#include "QueryData.h"
#include "QueryHandler.hpp"
namespace calmframe {
namespace data {

class DatabaseReader : public QueryHandler
{
public:
    virtual ~DatabaseReader(){}
    virtual QueryResult onQueryData(const QueryData & data);

    /** Method to be implemented by subclasses that allow stop reading rows before all data is sent.
        By default, always return false. I.e. all rows will be sent to readRow.
    */
    virtual bool hasFinished() const{
        return false;
    }
    /** Proccess the data readed on the current row, passed as a QueryData. */
    virtual void readRow(const QueryData & data)=0;
};

}
}

#endif // DATABASEREADER_H
