#ifndef SCHEMAMANAGER_H
#define SCHEMAMANAGER_H

#include "Table.h"

#include <map>

namespace calmframe {

namespace data {
    class SqliteConnector;
}//namespace

/** Responsible by manipulate the database schema to allow migrations.*/
class SchemaManager{
public:
    typedef std::vector<ForeignKey> ForeignKeyList;
public:
    SchemaManager(data::SqliteConnector * connector);

    bool existTable(const std::string & tableName);
    data::Table readTable(const std::string & tableName);
    std::vector<Column> readColumns(const std::string & tableName);
    ForeignKeyList readFKs(const std::string & tableName);

    void updateColumns(const std::string & tableName,
                       const std::map<std::string, Column> & columnsToUpdate);

    void updateForeignKeys(
        const std::string & tableName, 
        const std::map<std::string, ForeignKey> & FKsToUpdate);

    void updateTable(const std::string& tableName, 
        const std::map<std::string, Column>& columnsToUpdate, 
        const std::map<std::string, ForeignKey> & FKsToUpdate);
    
    void renameTable(const std::string & tableName, const std::string & newName);
    void dropTable(const std::string & tableName);

    void copyTable(const std::string& srcTable, const data::ColumnList & srcColumns,
                   const std::string& dstTable, const data::ColumnList & dstColumns);
public://getters
    data::SqliteConnector & getConnector();
    
    
    
protected:
    /** Substitute in oldColumns the names mapped in columnsToUpdate, but keep the relative order between them.
       For each item in oldColumns that exist a pair (name, column) in columnsToUpdate,
       uses the column name to update the value on oldColumns.
    */
    data::ColumnList replaceColumnNames(const data::ColumnList & oldColumns,
                                       const std::map<std::string, Column>& columnsToUpdate);
    
    void changeColumns(data::Table & changedTable, const std::map<std::string, Column>& columnsToUpdate);
    void changeForeignKeys(data::Table & changedTable, const std::map<std::string, ForeignKey>& FKsToUpdate);

private:
    data::SqliteConnector * connector;
};


}//namespace

#endif // SCHEMAMANAGER_H
