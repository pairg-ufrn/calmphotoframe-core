/*
 * Statement.cpp
 *
 *  Created on: 02/10/2014
 *      Author: noface
 */

#include "Statement.h"
#include "SqlException.hpp"
#include "SqliteConnector.h"

#include "utils/IllegalStateException.h"
#include "utils/Log.h"

#include "sqlite3.h"
#include <algorithm>

using namespace std;

namespace calmframe {
namespace data {

Statement::Statement(const std::string &  templateStatement, sqlite3_stmt* sqliteStatement)
    : templateStatement(templateStatement)
    , sqliteStatement(sqliteStatement)
    , bindedParamCount(0)
{}

Statement::Statement(SqliteConnector & connector, const string & statement, const ColumnsList & columns)
    : Statement()
{
    this->columns = columns;
    connector.createStatement(*this, statement);
}

Statement::~Statement() {
    try{
        finalize();
    }
    catch(std::exception & ex){
        logError("Failed to finalize statement, due to error:\n%s", ex.what());
    }
}

const Statement::ColumnsList &Statement::getColumns() const{
    return columns;
}

void Statement::setColumns(const ColumnsList &value){
    this->columns = value;
}

const string &Statement::getStatementTemplate() const{
    return this->templateStatement;
}

void Statement::initialize(const SqliteConnector & connector){
    connector.createStatement(*this, getStatementTemplate());
}

void Statement::initialize(const string &templateStatementStr, sqlite3_stmt *sqliteStatement){
    if(this->sqliteStatement != NULL){
        throw CommonExceptions::IllegalStateException("Initializing Statement already initialized");
    }
    this->templateStatement = templateStatementStr;
    this->sqliteStatement = sqliteStatement;
    this->bindedParamCount = 0;
}

Statement &Statement::appendTextValue(const std::string &text, const std::string &columnName){
    int index = indexOf(columnName);
    if(index >= 0){
        appendTextValue(text, index);
    }
    return *this;
}

Statement &Statement::appendValue(int number, const std::string &columnName){
    int index = indexOf(columnName);
    if(index >= 0){
        appendValue(number, index);
    }
    return *this;
}
Statement &Statement::appendValue(long number, const std::string &columnName){
    int index = indexOf(columnName);
    if(index >= 0){
        appendValue(number, index);
    }
    return *this;
}
Statement &Statement::appendRealValue(double number, const std::string &columnName){
    int index = indexOf(columnName);
    if(index >= 0){
        appendRealValue(number, index);
    }
    return *this;
}

Statement &Statement::appendNullValue(const string &columnName)
{
    int index = indexOf(columnName);
    if(index >= 0){
        appendNullValue(index);
    }
    return *this;
}


static void verifyError(int resultCode){
    const char * errMsg = sqlite3_errstr(resultCode);

    string errMessage = string("Failed to bind value due to database error: ").append(errMsg);
    Statement::assertNotErrorCode(resultCode, errMessage);
}

template <typename T>
class Binder{
public:
    //Declare type BindFunction
    typedef int (*BindFunction)(sqlite3_stmt * stmtPointer, int argumentNumber, T value);

    typedef void (*DestructorFunction)(void *);
    typedef int (*TextBindFunction)(sqlite3_stmt * stmtPointer
                                    , int argumentNumber, T value, int dataSize, DestructorFunction destructor);
private:
    sqlite3_stmt * statement;
    int * bindedParamCounter;
public:
    Binder(sqlite3_stmt * stmt, int * paramCounter=NULL)
        : statement(stmt), bindedParamCounter(paramCounter)
    {}

    void bind(BindFunction bindFunction, int argumentNumber, const T & value){
        incrementCounter();
        int resultCode = bindFunction(statement, argumentNumber, value);
        verifyError(resultCode);
    }
    void bind(TextBindFunction bindFunction, int argumentNumber, const T & value, int size=-1
              , DestructorFunction destructor=SQLITE_TRANSIENT)
    {
        incrementCounter();
        int resultCode = bindFunction(statement, argumentNumber, value, size, destructor);
        verifyError(resultCode);
    }

    void incrementCounter() {
        if(bindedParamCounter != NULL){
            (*bindedParamCounter) += 1;
        }
    }
};


//TODO: handle error codes
Statement& Statement::appendTextValue(const std::string& text, int valueNumber) {
    assertNotFinalized(string("Statement::appendTextValue \"").append(text).append("\"") );

    Binder<const char *> binder(sqliteStatement, &bindedParamCount);
    binder.bind(sqlite3_bind_text, getArgumentNumber(valueNumber), text.c_str());

    return *this;
}

Statement& Statement::appendValue(int number, int valueNumber) {
    assertNotFinalized("Statement::appendIntValue");

    Binder<int> binder(sqliteStatement, &bindedParamCount);
    binder.bind(sqlite3_bind_int, getArgumentNumber(valueNumber), number);

	return *this;
}

Statement& Statement::appendValue(long number, int argumentNumber) {
    assertNotFinalized("Statement::appendLongValue");

    Binder<sqlite_int64> binder(sqliteStatement, &bindedParamCount);
    binder.bind(sqlite3_bind_int64, getArgumentNumber(argumentNumber), number);

    return *this;
}

Statement& Statement::appendRealValue(double number, int argumentNumber) {
    assertNotFinalized("Statement::appendRealValue");

    Binder<double> binder(sqliteStatement, &bindedParamCount);
    binder.bind(sqlite3_bind_double, getArgumentNumber(argumentNumber), number);

    return *this;
}

Statement &Statement::appendNullValue(int columnNumber)
{
    assertNotFinalized("Statement::appendNullValue");
    int errCode = sqlite3_bind_null(sqliteStatement, getArgumentNumber(columnNumber));
    verifyError(errCode);

    return *this;
}

int Statement::getArgumentNumber(int argumentNumber){
    return getArgumentNumber(bindedParamCount, argumentNumber);
}
int Statement::getArgumentNumber(int currentCount, int argumentNumber) {
    return (argumentNumber < 0 ? currentCount : argumentNumber) + 1;
}

bool Statement::isErrorCode(int resultCode) {
	return !((resultCode == SQLITE_OK)
			|| (resultCode == SQLITE_DONE) || (resultCode == SQLITE_ROW));
}

void Statement::execute() {
    executeImpl();
}

int Statement::executeImpl(){
    assertNotFinalized("Statement::execute");
    int resultCode = sqlite3_step(this->sqliteStatement);

    assertNotErrorCode(resultCode,errorMessage("Could not execute statement. ", resultCode));

    return resultCode;
}

sqlite3_stmt *Statement::getStmtPointer() const{
    return sqliteStatement;
}

Statement & Statement::reset() {
    assertNotFinalized("Statement::reset");

    int resultCode = sqlite3_reset(sqliteStatement);
    assertNotErrorCode(resultCode, errorMessage("Failed to reset statement.", resultCode));
	bindedParamCount = 0;

    return *this;
}

void Statement::finalize() {
	if(!isFinalized()){
        int resultCode = sqlite3_finalize(sqliteStatement);
        sqliteStatement = NULL;

        if(isErrorCode(resultCode)){
            const string errMessage = "Statement::finalize:\t"
                                   "Found error when finalizing statement "
                                   "\"" + templateStatement + "\".";

            assertNotErrorCode(resultCode, errorMessage(errMessage, resultCode));
        }
	}
}
bool Statement::isFinalized() const {
    return !isInitialized();
}

bool Statement::isInitialized() const{
    return sqliteStatement != NULL;
}

void Statement::assertNotFinalized(const std::string & where) const{
    if(isFinalized()){
        string errMessage = "Trying to use finalized Statement!";
        if(!where.empty()){
            errMessage.append("In: ").append(where);
        }
        throw CommonExceptions::IllegalStateException(errMessage);
    }
}


void Statement::assertNotErrorCode(int resultCode, const string &errMessage, bool appendErrorMsg){
    if(isErrorCode(resultCode)){
        const char * sqliteErrMsg = sqlite3_errstr(resultCode);

        string finalErrMsg = string(errMessage).append(";");

        if(appendErrorMsg){
            finalErrMsg.append(" Error message: \"").append(sqliteErrMsg).append("\"");
        }

        throw SqlException(finalErrMsg, resultCode);
    }
}

string Statement::errorMessage(const string & err, int resultCode){
    const char * errStr = sqlite3_errstr(resultCode);
    return string(err).append(" Database error: '").append(errStr).append("'.");
}

int Statement::indexOf(const string &colName) const
{
    ColumnsList::const_iterator found = std::find(this->columns.begin(), this->columns.end(), colName);
    if(found == this->columns.end()){
        return -1;
    }
    int distance= std::distance(columns.begin(), found);
    return distance;
}


} /* namespace data */
} /* namespace calmframe */

