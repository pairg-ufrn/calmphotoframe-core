#ifndef QUERYARGS_H
#define QUERYARGS_H

#include <string>

namespace calmframe {
namespace data {

class QueryArgs{
public:
    enum OrderType{DEFAULT, ASC, DESC};

    static std::string orderTypeToString(OrderType orderType);

    static const int UNLIMITED = 0;
private:
    //TODO: allow ordering by more than one column
    std::string whereCondition, orderBy;
    int limit;
    OrderType orderType;

public:
    QueryArgs(const std::string & whereCondition=std::string()
            , int limit=UNLIMITED
            , const std::string & orderBy=std::string()
            , QueryArgs::OrderType orderType= DEFAULT);

    const std::string & getWhereCondition() const;
    QueryArgs & setWhereCondition(const std::string & value);

    const std::string & getOrderBy() const;
    QueryArgs & setOrderBy(const std::string & value);

    int getLimit() const;
    QueryArgs & setLimit(int value);

    OrderType getOrderType() const;
    QueryArgs & setOrderType(const OrderType & value);
};

}//namespace
}//namespace

#endif // QUERYARGS_H
