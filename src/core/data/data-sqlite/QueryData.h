#ifndef QUERYDATA_H
#define QUERYDATA_H

#include <string>

namespace calmframe {

class QueryData{
public:
    virtual ~QueryData(){}

    virtual std::string getString(const std::string & col, const std::string & defaultValue = std::string()) const=0;
    virtual std::string getString(int index              , const std::string & defaultValue = std::string()) const=0;

    virtual long getLong(const std::string & col, long defaultValue = 0l) const=0;
    virtual long getLong(int index              , long defaultValue = 0l) const=0;

    virtual int getInt(const std::string & col, int defaultValue = 0) const=0;
    virtual int getInt(int index              , int defaultValue = 0) const=0;

    virtual float getFloat(const std::string & col, float defaultValue = 0.0f) const=0;
    virtual float getFloat(int index              , float defaultValue = 0.0f) const=0;

    virtual double getDouble(const std::string & col, double defaultValue = 0.0f) const=0;
    virtual double getDouble(int index              , double defaultValue = 0.0f) const=0;

    virtual bool hasData(const std::string & column) const=0;
    virtual bool hasData(int resultIndex)            const=0;

    virtual bool columnIsNull(const std::string & column) const=0;
    virtual bool columnIsNull(int index)                  const=0;

    virtual int getColumnsCount() const=0;
    virtual std::string getColumnName(int index) const=0;
};

}//namespace
#endif // QUERYDATA_H
