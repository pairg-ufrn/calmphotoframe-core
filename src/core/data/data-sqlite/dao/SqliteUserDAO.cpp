#include "SqliteUserDAO.h"

#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/readers/CountReader.h"
#include "data/data-sqlite/tables/UserTable.h"
#include "data/data-sqlite/SqlException.hpp"
#include "data/data-sqlite/parsers/UserDbParser.h"

#include "model/user/UserAccount.h"
#include "model/user/Session.h"

#include "utils/CommonExceptions.h"
#include "utils/Log.h"

#include <string>
#include <vector>
#include <sstream>

namespace calmframe{

using namespace data;
using namespace Tables;
using namespace std;

SqliteUserDAO::SqliteUserDAO(SqliteConnector *database)
    : database(database)
    , accountDAO(&userTable, database)
    , userDao(&userTable, database)
{
    UserTable::buildTable(userTable);
}

SqliteUserDAO::~SqliteUserDAO()
{
}

bool SqliteUserDAO::createTableIfNotExist(SqliteConnector &database){
    return database.execute(userTable.getCreateStatement());
}

UserCollection SqliteUserDAO::listUsers()
{
    return userDao.listAll();
}

UserCollection SqliteUserDAO::listUsersIn(const SetId & userIds){
    return userDao.listAll(Table::WhereIn(UserTable::Columns::ID, userIds.begin(), userIds.end()));
}

bool SqliteUserDAO::get(long userId, UserAccount &userOut){
    return getUser(userOut, whereIdEquals(userId));
}

bool SqliteUserDAO::getUser(long userId, User & userOut)
{
    return userDao.get(userOut, whereIdEquals(userId));
}

bool SqliteUserDAO::getUser(UserAccount &userOut, const string &whereCondition){
    return accountDAO.get(userOut, whereCondition);
}

bool SqliteUserDAO::getByLogin(const std::string &userName, UserAccount &userOut){
    bool gotUser = getUser(userOut, whereNameEquals(userName));
    return gotUser;
}


bool SqliteUserDAO::exist(long userId) const{
    return existUser(whereIdEquals(userId));
}

bool SqliteUserDAO::exist(const string &userName) const{
    return existUser(whereNameEquals(userName));
}

bool SqliteUserDAO::existUser(const std::string & whereCondition) const{
    return accountDAO.exist(UserTable::Columns::ID, whereCondition);
}

string SqliteUserDAO::whereIdEquals(long userId) const{
    return Table::WhereEquals(UserTable::Columns::ID, userId);
}

string SqliteUserDAO::whereNameEquals(const std::string &userName) const{
    return Table::WhereEquals(UserTable::Columns::LOGIN, userName, true);
}

long SqliteUserDAO::insert(const UserAccount &userAccount){
    long inserted = accountDAO.insert(userAccount);
    return inserted;
}

bool SqliteUserDAO::remove(long userId)
{
    return accountDAO.remove(userId) == 1;
}

bool SqliteUserDAO::update(const User & user)
{
    return userDao.update(user.getId(), user, Tables::UserTable::getUserColumns());
}

bool SqliteUserDAO::updateAccount(long id, const UserAccount & account){
    return accountDAO.update(id, account, Tables::UserTable::updateAccountOnlyColumns());
}

long SqliteUserDAO::insertAt(long userId, const calmframe::UserAccount & userAccount)
{
    std::vector<std::string> columns = userTable.getInsertColumns();
    columns.push_back(Tables::CommonTable::Columns::ID);

    UserAccount accountWithId = userAccount;
    accountWithId.setId(userId);

    return accountDAO.insert(accountWithId, columns);
}

}
