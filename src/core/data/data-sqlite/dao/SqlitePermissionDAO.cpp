#include "SqlitePermissionDAO.h"

#include "../ScopedTransaction.h"
#include "../parsers/IdDbReader.h"
#include "../tables/PermissionsTable.h"
#include "utils/CommonExceptions.h"

namespace calmframe {

using namespace std;
using namespace data;
using namespace Tables;
using namespace Tables::PermissionsTable;

static std::string buildListContextsWithPermissionStmt(Table & table){
    const string whereCond = Table::AND(
        Table::WhereEquals(Columns::userId, "?")
      , Table::WhereCompare(Columns::permissionLevel, ">=", "?"));

    return table.getQueryStatement(QueryArgs{whereCond, });
}

SqlitePermissionDAO::SqlitePermissionDAO(data::SqliteConnector * connector)
    : table(PermissionsTable::buildTable(Table()))
    , dao(&table, connector)
{
}

bool SqlitePermissionDAO::createTableIfNotExist(data::SqliteConnector & connector)
{
    return dao.createTableIfNotExist(connector);
}

bool SqlitePermissionDAO::hasPermission(long userId, long ctxId, PermissionLevel minPermissionLevel){
    Permission permission;
    if(tryGetPermission(userId, ctxId, permission)){
        return permission.getPermissionLevel() >= minPermissionLevel;
    }
    return false;
}

void SqlitePermissionDAO::putPermission(const Permission & permission){
    dao.insertOrReplace(permission);
}

Permission SqlitePermissionDAO::getPermission(long userId, long ctxId){
    Query getPermissionStmt;
    prepareGetPermissionStmt(userId, ctxId, getPermissionStmt);

    return dao.get(getPermissionStmt);
}

bool SqlitePermissionDAO::tryGetPermission(long userId, long ctxId, Permission & permission){
    Query getPermissionStmt;
    prepareGetPermissionStmt(userId, ctxId, getPermissionStmt);

    return dao.get(getPermissionStmt, permission);
}

PermissionsDAO::PermissionList SqlitePermissionDAO::list(long userId, PermissionLevel minPermissionLevel){
    Query queryUserPermissions;
    prepareUserPermissionsQuery(userId, minPermissionLevel, queryUserPermissions);
    return dao.list(queryUserPermissions);
}

PermissionsDAO::PermissionList SqlitePermissionDAO::listByContext(long ctxId){
    static const std::string stmt = table.getQueryStatement(Table::WhereEquals(Columns::contextId, "?"));
    Query query{getConnector(), stmt};
    query.appendValue(ctxId);

    return dao.list(query);
}

SetId SqlitePermissionDAO::listContextsWithPermission(long userId, PermissionLevel permissionLevel){
    Query queryUserPermissions;
    prepareUserPermissionsQuery(userId, permissionLevel, queryUserPermissions);

    IdDbReader reader(Columns::contextId);
    queryUserPermissions.read(reader);
    return reader.getIds();
}

void SqlitePermissionDAO::replaceContextPermissions(long ctxId, const PermissionsDAO::PermissionList & ctxPermissions){
    ScopedTransaction transaction(&getConnector());

    dao.remove(Table::WhereEquals(Columns::contextId, ctxId));
    dao.insertAll(ctxPermissions.begin(), ctxPermissions.end(), table.getInsertColumns());

    transaction.commit();
}


void SqlitePermissionDAO::prepareGetPermissionStmt(long userId, long ctxId, Query & getPermissionStmt)
{
    if(!getPermissionStmt.isInitialized()){
        const std::string equalsContextAndUser = Table::AND(
            Table::WhereEquals(Columns::contextId, "?")
          , Table::WhereEquals(Columns::userId, "?"));
        getConnector().createStatement(getPermissionStmt, table.getQueryStatement(equalsContextAndUser));
    }

    getPermissionStmt.reset().appendValue(ctxId).appendValue(userId);
}

void SqlitePermissionDAO::prepareUserPermissionsQuery(long userId, PermissionLevel permissionLevel, Query & queryUserPermissions)
{
    if(!queryUserPermissions.isInitialized()){
        getConnector().createStatement(queryUserPermissions, buildListContextsWithPermissionStmt(table));
    }
    queryUserPermissions.reset();
    queryUserPermissions.appendValue(userId).appendValue((int)permissionLevel);
}


SqliteConnector &SqlitePermissionDAO::getConnector(){
    CHECK_NOT_NULL(dao.getDatabase());
    return *dao.getDatabase();
}


}//namespace
