#ifndef SQLITEPERMISSIONDAO_H
#define SQLITEPERMISSIONDAO_H

#include "../../PermissionsDAO.h"

#include "GenericDAO.hpp"
#include "SqliteDAO.hpp"

#include "model/permissions/Permission.h"
#include "../parsers/PermissionParser.h"
#include "../Query.h"
#include "../../SetId.h"

namespace calmframe {

class SqlitePermissionDAO : public data::SqliteDAO, public PermissionsDAO{
public:
    SqlitePermissionDAO(data::SqliteConnector * connector);
    bool createTableIfNotExist(data::SqliteConnector &connector) override;

    bool hasPermission(long userId, long ctxId, PermissionLevel minPermissionLevel = PermissionLevel::None) override;
    PermissionList list(long userId, PermissionLevel minPermissionLevel = PermissionLevel::None) override;
    PermissionList listByContext(long ctxId) override;
    void putPermission(const Permission & permission) override;
    Permission getPermission(long userId, long ctxId) override;
    virtual bool tryGetPermission(long userId, long ctxId, Permission & permission) override;
    calmframe::data::SetId listContextsWithPermission(long userId, PermissionLevel permissionLevel) override;
    void replaceContextPermissions(long ctxId, const PermissionList & ctxPermissions) override;
    
protected:
    void prepareGetPermissionStmt(long userId, long ctxId, Query & query);
    void prepareUserPermissionsQuery(long userId, PermissionLevel permissionLevel, Query & query);
    data::SqliteConnector & getConnector();

private:
    data::Table table;
    GenericDAO<Permission, PermissionParser> dao;
};

}//namespace

#endif // SQLITEPERMISSIONDAO_H
