#ifndef GENERICDAO_HPP
#define GENERICDAO_HPP

#include "SqliteDAO.hpp"
#include "data/data-sqlite/QueryArgs.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/readers/CountReader.h"
#include "data/data-sqlite/readers/GenericDbReader.h"
#include "data/data-sqlite/writers/GenericDbWriter.h"
#include "data/data-sqlite/writers/GenericListWriter.h"
#include "data/data-sqlite/SqlException.hpp"
#include "data/data-sqlite/parsers/IdDbReader.h"
#include "../Query.h"
#include "exceptions/NotFoundMember.h"

#include "utils/IllegalStateException.h"
#include <string>
#include <vector>

#include "utils/Log.h"

namespace calmframe {

template<typename T, typename DbParser>
class GenericDAO : public data::SqliteDAO{
private:
    const data::Table * table;
    data::SqliteConnector * database;
public:
    GenericDAO(const data::Table * table, data::SqliteConnector * _database);
    data::SqliteConnector * getDatabase() const;
    bool createTableIfNotExist(data::SqliteConnector &database);

    int count();
    bool get(T & dataOut, const std::string & whereCondition) const;
    T get(Query & query) const;
    bool get(Query & query, T & out) const;
    bool exist(const std::string & idColumn, long id) const;
    bool exist(const std::string & idColumn, const std::string & whereCondition) const;

    /** Updates the row that has 'idToUpdate' as primaryKey with the value of data.
        This method requires that the attribute #Table::idColumn be not empty.

        @param idToUpdate - the id of the row to be changed
        @param data - the value used to update the row
        @param updateColumns - the columns that will be updated. If not set, use the updateColumns from table
    */
    int  update(long idToUpdate, const T & data
                              , const std::vector<std::string> & updateColumns=std::vector<std::string>());
    int  update(const T & data, const std::string &whereCondition
                              , const std::vector<std::string> & updateColumns=std::vector<std::string>());
    int remove(const std::string &whereCondition);

    /** Removes the row that has 'rowId' as primaryKey.
        Requires that the attribute #Table::idColumn be not empty.*/
    int remove(long rowId);

    std::vector<T> list(Query & query);
    std::vector<T> listAll(const std::string & whereCondition="");
    std::vector<T> & listAll(std::vector<T> & output, const std::string & whereCondition);
    std::vector<T> & listAll(std::vector<T> & output, const data::QueryArgs & args);
    std::vector<T> && listAll(const data::QueryArgs & args, std::vector<T> && output = std::vector<T>());

    long insert(const T & data);

    long insert(const T & data, const std::vector<std::string> & insertColumns
                                       , data::OnConflict resolution = data::OnConflict::DEFAULT);
    long insertOrReplace(const T &data);

    template<typename Iterator>
    int insertAll(const Iterator &begin, const Iterator &end, const std::vector<std::string> &insertColumns
                                       , data::OnConflict resolution = data::OnConflict::DEFAULT);
protected:
    std::vector<T> queryImpl(Query & query, int limit=-1) const;
    std::string getWhereIdEquals(long id) const;
};

template<typename T, typename DbParser>
GenericDAO<T, DbParser>::GenericDAO(const data::Table *_table, data::SqliteConnector * _database)
    : table(_table)
    , database(_database)
{
    if(_table == NULL){
        throw CommonExceptions::IllegalArgumentException("GenericDAO(Constructor):\t""Table pointer cannot be NULL!");
    }
    if(_database == NULL){
        throw CommonExceptions::IllegalArgumentException("GenericDAO(Constructor):\t""SqliteDatabase pointer cannot be NULL!");
    }
}

template<typename T, typename DbParser>
data::SqliteConnector * GenericDAO<T, DbParser>::getDatabase() const{
    return this->database;
}

template<typename T, typename DbParser>
bool GenericDAO<T, DbParser>::createTableIfNotExist(data::SqliteConnector &database){
    std::string createStatement = table->getCreateStatement();
    return database.execute(createStatement);
}

template<typename T, typename DbParser>
int GenericDAO<T, DbParser>::count(){
    IdDbReader idReader("count");

    getDatabase()->query(idReader, *table, {}, {"Count(1) as count"});

    return idReader.getIds().empty() ? 0 : *idReader.getIds().begin();
}

template<typename T, typename DbParser>
bool GenericDAO<T, DbParser>::get(T &dataOut, const std::string &whereCondition) const{
    std::vector<T> rows;
    GenericDbReader<T, DbParser> reader(&rows, 1);

    this->database->query(reader, *table, whereCondition);

    if(rows.size() > 0){
        dataOut = rows[0];
        return true;
    }
    return false;
}

template<typename T, typename DbParser>
T GenericDAO<T, DbParser>::get(Query & query) const{
    T item;
    if(!get(query, item)){
        throw NotFoundMember();
    }
    return item;
}

template<typename T, typename DbParser>
bool GenericDAO<T, DbParser>::get(Query & query, T & out) const{
    auto rows = queryImpl(query, 1);
    if(!rows.empty()){
        out = rows[0];
        return true;
    }
    return false;
}


template<typename T, typename DbParser>
bool GenericDAO<T, DbParser>::exist(const std::string &idColumn, long id) const{
    return exist(idColumn, data::Table::WhereEquals(idColumn, id));
}
template<typename T, typename DbParser>
bool GenericDAO<T, DbParser>::exist(const std::string &idColumn, const std::string &whereCondition) const{
    CountReader reader(1);
    std::vector<std::string> columnsToRead;
    columnsToRead.push_back(idColumn);

    database->query(reader, *table, whereCondition, columnsToRead);

    return reader.getCounter() > 0;
}

template<typename T, typename DbParser>
long GenericDAO<T, DbParser>::insert(const T &data)
{
    return this->insert(data, this->table->getInsertColumns());
}
template<typename T, typename DbParser>
long GenericDAO<T, DbParser>::insertOrReplace(const T &data)
{
    return this->insert(data, this->table->getInsertColumns(), data::OnConflict::REPLACE);
}

template<typename T, typename DbParser>
long GenericDAO<T, DbParser>::insert(const T &data, const std::vector<std::string> &insertColumns
                                       , data::OnConflict resolution)
{
    GenericDbWriter<T, DbParser> writer(&data);
    int insertCount = this->database->insert(table->getName(), insertColumns,writer,true, resolution);

    if(insertCount != 1){
        std::stringstream sstream;
        sstream << "GenericDAO::insert:\t" "Tried to insert 1 row, but inserted " << insertCount;
        throw data::SqlException(sstream.str());
    }

    return database->getLastInsertedId();
}

template<typename T, typename DbParser>
template<typename Iterator>
int GenericDAO<T, DbParser>::insertAll(const Iterator & begin, const Iterator & end
        , const std::vector<std::string> &insertColumns
        , data::OnConflict resolution)
{
    GenericListWriter<T, DbParser, Iterator> writer(begin, end);

    return this->database->insert(table->getName(), insertColumns,writer,true, resolution);
}

template<typename T, typename DbParser>
int GenericDAO<T, DbParser>
::update(long idToUpdate, const T &data, const std::vector<std::string> &updateColumns)
{
    return update(data, getWhereIdEquals(idToUpdate), updateColumns);
}

template<typename T, typename DbParser>
int GenericDAO<T, DbParser>
::update(const T &data, const std::string & whereCondition, const std::vector<std::string> &updateColumns)
{
    GenericDbWriter<T, DbParser> writer(&data);
    const std::vector<std::string> & colsToUpdate = !updateColumns.empty()
                                                            ? updateColumns
                                                            : table->getUpdateColumns();

    return this->database->update(writer, table->getName(), colsToUpdate, whereCondition);
}

template<typename T, typename DbParser>
int GenericDAO<T, DbParser>::remove(const std::string & whereCondition){
    return this->database->remove(*table, whereCondition);
}

template<typename T, typename DbParser>
int GenericDAO<T, DbParser>::remove(long rowId)
{
    return remove(getWhereIdEquals(rowId));
}

template<typename T, typename DbParser>
std::vector<T> GenericDAO<T, DbParser>::list(Query & query){
    return queryImpl(query);
}

template<typename T, typename DbParser>
std::vector<T> GenericDAO<T, DbParser>::listAll(const std::string & whereCondition){
    std::vector<T> rows;
    return listAll(rows, whereCondition);
}

template<typename T, typename DbParser>
std::vector<T> & GenericDAO<T, DbParser>::listAll(std::vector<T> & output, const std::string & whereCondition){
    GenericDbReader<T, DbParser> reader(&output);
    this->database->query(reader,*this->table, whereCondition);

    return output;
}

template<typename T, typename DbParser>
std::vector<T> & GenericDAO<T, DbParser>
::listAll(std::vector<T> & output, const data::QueryArgs & args)
{
    GenericDbReader<T, DbParser> reader(&output);
    this->database->query(reader,*this->table, args);
    return output;
}

template<typename T, typename DbParser>
std::vector<T> && GenericDAO<T, DbParser>
::listAll(const data::QueryArgs & args, std::vector<T> && output)
{
    listAll(output, args);
    return std::move(output);
}

template<typename T, typename DbParser>
std::vector<T> GenericDAO<T, DbParser>::queryImpl(Query & query, int limit) const{
    std::vector<T> rows;
    GenericDbReader<T, DbParser> reader(&rows, limit);

    query.read(reader);

    return rows;
}

template<typename T, typename DbParser>
std::string GenericDAO<T, DbParser>::getWhereIdEquals(long id) const
{
    if(table->getIdColumn().empty()){
        throw CommonExceptions::IllegalStateException(COMMON_EXC_HERE ": table does not have idColumn");
    }
    return table->whereIdEquals(id);
}

}//namespace


#endif // GENERICDAO_HPP

