#ifndef SQLITEEVENTSDAO_H
#define SQLITEEVENTSDAO_H

#include "GenericDAO.hpp"
#include "SqliteDAO.hpp"
#include "../parsers/EventsParser.h"
#include "data/EventsDAO.h"

#include "model/events/Event.h"

namespace calmframe {

using namespace data;

class SqliteEventsDAO : public EventsDAO, public data::SqliteDAO{
public:
    SqliteEventsDAO(SqliteConnector * connector);
    bool createTableIfNotExist(SqliteConnector &connector) override;

    Event get(long id) override;
    long insert(const Event & evt) override;
    bool exist(long eventId) override;
    EventsList listRecents(int limit=-1) override;

private:
    Table table;
    GenericDAO<Event, EventsParser> dao;
};

}//namespace

#endif // SQLITEEVENTSDAO_H
