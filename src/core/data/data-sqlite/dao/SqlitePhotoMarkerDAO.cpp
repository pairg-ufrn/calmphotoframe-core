/*
 * SqlitePhotoMarkerDAO.cpp
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#include "SqlitePhotoMarkerDAO.h"
#include "../DatabaseWriter.h"
#include "../DatabaseReader.h"
#include "../SqlException.hpp"
#include "../ScopedTransaction.h"
#include "../tables/PhotoTable.h"
#include "../tables/MarkersTable.h"

#include "../../../exceptions/DatabaseException.h"

#include "model/photo/Photo.h"
#include "utils/StringCast.h"
#include "utils/TimeUtils.h"
#include "utils/IllegalArgumentException.hpp"
#include "utils/NullPointerException.hpp"
#include "utils/IllegalStateException.h"
#include "utils/Log.h"

#include <vector>

namespace calmframe {
namespace data {

using namespace std;
using namespace Tables;

using ::calmframe::utils::StringCast;

SqlitePhotoMarkerDAO::SqlitePhotoMarkerDAO(SqliteConnector * connector)
    :connector(connector)
    , dao(&photoMarkerTable, connector)
{
    MarkersTable::buildTable(photoMarkerTable);
}

SqlitePhotoMarkerDAO::~SqlitePhotoMarkerDAO() {
	// TODO Auto-generated destructor stub
}

bool SqlitePhotoMarkerDAO::createTableIfNotExist(SqliteConnector& database) {
    return dao.createTableIfNotExist(database);
}

std::vector<PhotoMarker> SqlitePhotoMarkerDAO::listAll(const Photo& photo) {
	return this->listAll(&photo);
}
std::vector<PhotoMarker> SqlitePhotoMarkerDAO::listAll(const Photo* photo) {
    return listAll(photo != NULL ? photo->getId() : -1);
}
std::vector<PhotoMarker> SqlitePhotoMarkerDAO::listAll(const long & photoId) {
    string whereCondition = (photoId < 0   ? ""
                                           : Table::WhereEquals(MarkersTable::Columns::PHOTO_ID, photoId));
    return dao.listAll(whereCondition);
}

SetId SqlitePhotoMarkerDAO::queryPhotos(const string & queryStr){
    string where = photoMarkerTable.queryTemplate("?1", ColumnList{MarkersTable::Columns::IDENTIFICATION});
    Query query(*dao.getDatabase(), photoMarkerTable, where, ColumnList{MarkersTable::Columns::PHOTO_ID});
    query.appendTextValue(queryStr);

    IdDbReader reader(MarkersTable::Columns::PHOTO_ID);
    query.read(reader);
    return reader.getIds();
}

long SqlitePhotoMarkerDAO::insert(long photoId, const PhotoMarker& marker){
    PhotoMarker markerCopy(marker);
    markerCopy.setPhotoId(photoId);

    return dao.insert(markerCopy);
}
void SqlitePhotoMarkerDAO::insert(long photoId, const std::vector<PhotoMarker>& photoMarkers){
    ScopedTransaction transaction(dao.getDatabase(), TransactionType::Write);

    for(auto const & marker : photoMarkers){
        insert(photoId, marker);
    }

    transaction.commit();
}

void SqlitePhotoMarkerDAO::update(const PhotoMarker& marker) {
    ScopedTransaction transaction(dao.getDatabase(), TransactionType::Write);

    string whereCondition = Table::AND(Table::WhereEquals(MarkersTable::Columns::PHOTO_ID, marker.getPhotoId())
                                       , Table::WhereEquals(MarkersTable::Columns::INDEX, marker.getIndex()));

    int updatedRows = dao.update(marker, whereCondition);
    if(updatedRows != 1){
        const char * cause = updatedRows < 1
                    ? "No one marker updated."
                    : "More than one marker updated.";

        throw DatabaseException(string("Failed to update marker. ").append(cause));
    }

    transaction.commit();
}

bool SqlitePhotoMarkerDAO::removeMarkersFromPhoto(long photoId) {
    const std::string condition = Table::WhereEquals(MarkersTable::Columns::PHOTO_ID, photoId);
    return dao.remove(condition) >= 0;
}

bool SqlitePhotoMarkerDAO::remove(const PhotoMarker& photoMarker) {
    const std::string condition =
        Table::AND(
            Table::WhereEquals(MarkersTable::Columns::PHOTO_ID, photoMarker.getPhotoId()),
            Table::WhereEquals(MarkersTable::Columns::INDEX, photoMarker.getIndex()));

    return dao.remove(condition) > 0;
}

} /* namespace data */
} /* namespace calmframe */

