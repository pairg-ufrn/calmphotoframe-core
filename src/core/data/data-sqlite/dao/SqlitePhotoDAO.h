#ifndef SQLITEPHOTODAO_H_
#define SQLITEPHOTODAO_H_

#include "model/photo/Photo.h"
#include <vector>
#include <map>

#include "SqliteDAO.hpp"
#include "GenericDAO.hpp"
#include "data/PhotoDAO.hpp"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/QueryHandler.hpp"
#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/parsers/PhotoDbParser.h"
#include "data/SetId.h"

#include "data/data-sqlite/DAOForward.h"

namespace calmframe{

class Query;

namespace data{

class PhotoQueryArgs;

class SqlitePhotoDAO : public PhotoDAO, public SqliteDAO{
public:
    SqlitePhotoDAO(SqliteConnector * sqlite
                   , SqlitePhotoMarkerDAO * photoMarkerDAO
                   , SqlitePhotoMetadataDAO * photoMetadataDAO);

	virtual bool createTableIfNotExist(SqliteConnector & database);

    PhotoList listAll(const std::string & query= std::string());
    PhotoList listIn(const std::set<long> & photoIds, const std::string & query=std::string());
    PhotoList listByContext(long contextId);
    bool getPhoto(long photoId, Photo & photoOut);
    long insert(const Photo & photo);
    long insert(const Photo & photo, long photoId);
	bool update(const Photo & photo);
	bool remove(const Photo & photo);
    bool exist(long photoId);

    bool updatePhotoDescription(long photoId, const PhotoDescription & photoDescription);

protected:
    PhotoList listPhotos(const std::string &whereCondition="");
    PhotoList listPhotos(const std::string & queryString, const SetId * photoIds, long ctxId=-1);
    std::string whereMatchQuery(const PhotoQueryArgs & args);
    void completePhotosData(std::vector<Photo> & photos);

    long insertImplementation(const Photo & photo, long * photoId = NULL);

    std::vector<std::string> getInsertColumns(long photoIdValue);
    std::vector<std::string> getUpdateColumns(bool updateImagePath);

    bool updateImplementation(const Photo & photo, bool updatedImagePath);
	bool tryUpdatePhotoMarkers(long photoId, const std::vector<PhotoMarker> & newPhotoMarkers);

	std::map<long, std::vector<PhotoMarker> > listAllMarkersByPhotoId();    
    void listAllMetadataByPhoto(std::map<long, std::vector<PhotoMarker> > & metadataByPhotoId);

    Photo rowToPhoto(const std::map<std::string, std::string> & rowData);
//	void handleListAllQuery(const QueryData& data);

protected:
    SqlitePhotoMarkerDAO & markerDAO();

private:
    std::vector<Photo> photos;
    SqliteConnector * database;

    SqlitePhotoMarkerDAO * photoMarkerDAO;
    SqlitePhotoMetadataDAO * photoMetadataDAO;
    Table photoTable;

    GenericDAO<Photo, PhotoDbParser> photoGenericDAO;
};

} /* namespace data */
} /* namespace calmframe */

#endif /* SQLITEPHOTODAO_H_ */
