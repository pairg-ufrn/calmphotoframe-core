#ifndef SQLITEPHOTOCOLLECTIONDAO_H
#define SQLITEPHOTOCOLLECTIONDAO_H

#include "SqliteDAO.hpp"
#include "GenericDAO.hpp"
#include "SqlitePhotoCollectionMappingDAO.h"

#include "data/PhotoCollectionDAO.h"
#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/parsers/PhotoCollectionParser.h"
#include "model/photo/PhotoCollection.h"

#include <vector>

class PhotoDAO;

namespace calmframe {
namespace data {
class SqliteConnector;
class ScopedTransaction;

class SqlitePhotoCollectionDAO : public SqliteDAO, public PhotoCollectionDAO
{
private:
    Table photoCollectionTable;
    GenericDAO<PhotoCollection, PhotoCollectionParser> genericDAO;
    SqlitePhotoCollectionMappingDAO collMappingDAO;
public:
    SqlitePhotoCollectionDAO(SqliteConnector * database);
    virtual ~SqlitePhotoCollectionDAO(){}

    /** @override */
    virtual bool createTableIfNotExist(SqliteConnector & database);

    virtual bool get(long collectionId, PhotoCollection & outCollection);
    virtual bool exist(long collectionId);
    virtual std::vector<PhotoCollection> listAll();
    virtual std::vector<PhotoCollection> listIn(const std::set<long> & collIds);
    virtual std::vector<PhotoCollection> getByName(const std::string & name);
    virtual CollectionList & getByContext(long ctxId, CollectionList &list);
    virtual SetId listIdsByContext(long ctxId);
    virtual SetId getCollectionsWithoutParent() const;

    virtual CollectionTree & getCollectionTree(
                                                long collId, CollectionTree & collTree);

    virtual long insert(const PhotoCollection & photoCollection);
    virtual long insert(const PhotoCollection &photoCollection, long parentCollId);
    virtual long insertAt(long collectionId, const PhotoCollection & photoCollection);
    virtual bool update(const PhotoCollection & photoCollection);
    virtual bool remove(long collectionId);
    virtual int remove(const std::set<long> & collectionIds);
    virtual int removeCollectionsWithoutParent();

    virtual std::set<long> getPhotosWithoutParent(const std::set<long> & photoIds);
    virtual SetId getCollectionPhotos(long collectionId);
    virtual void addPhoto(long photoId, long collectionId);
    virtual void addPhotos(const std::set<long> & collectionIds, const std::set<long> & photoIds);
    virtual void removePhotoFromCollections(long photoId);
    virtual void removePhotosFromCollections(const std::set<long> & collectionIds, const std::set<long> & photoIds);

    virtual void addItemsToCollections(const std::set<long> & targetCollections
                                  , const std::set<long> & photoIds, const std::set<long> & subCollections);
    virtual void removeItemsFromCollections(const std::set<long> & targetCollections
                                  , const std::set<long> & photoIds, const std::set<long> & subCollections);

    virtual void moveCollectionItems(long collectionId, const SetId & targetCollections
                                             , const SetId & photosToMove
                                             , const SetId & subCollectionsToMove);

    virtual std::vector<PhotoCollection> getSubCollections(long parentCollectionId);

    SqlitePhotoCollectionMappingDAO & collectionMappingDAO();
public:
    void updateCollectionIds(const PhotoCollection &photoCollection, ScopedTransaction &transaction);
    void updateCollection(const PhotoCollection &photoCollection, ScopedTransaction & transaction);
    long insertImpl(const PhotoCollection &photoCollection, long collectionId=-1);
    void listRootCollections();
    SetId listIds(const std::string & whereCondition) const;
protected:
    CollectionList listCollections(const std::string &whereCondition="");
    CollectionList & listCollections(CollectionList & list,const std::string &whereCondition="");
    static const std::string whereCollectionHasNoParent();

    void addToTree(long collId, CollectionTree &collTree
        , const std::vector<PhotoCollection> & allCollections
        , const std::map<long, int> &idToIndex );
};

}//data
}//calmframe


#endif // SQLITEPHOTOCOLLECTIONDAO_H
