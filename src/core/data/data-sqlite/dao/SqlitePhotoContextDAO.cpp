#include "SqlitePhotoContextDAO.h"

#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/tables/PhotoContextTable.h"
#include "data/data-sqlite/parsers/IdDbReader.h"
#include "data/data-sqlite/QueryArgs.h"

#include "utils/CommonExceptions.h"
#include "utils/Log.h"

namespace calmframe {

using namespace std;

using namespace data;
using namespace Tables;
using namespace Tables::PhotoContextTable;

SqlitePhotoContextDAO::SqlitePhotoContextDAO(data::SqliteConnector *database)
    : table(PhotoContextTable::buildTable(Table()))
    , genericDao(&table, database)
{
}

bool SqlitePhotoContextDAO::createTableIfNotExist(data::SqliteConnector &database){
    return genericDao.createTableIfNotExist(database);
}

long SqlitePhotoContextDAO::count()
{
    return genericDao.count();
}

PhotoContext SqlitePhotoContextDAO::get(long contextId)
{
    PhotoContext ctx;
    return (genericDao.get(ctx, table.whereIdEquals(contextId)) ? ctx : PhotoContext());
}

PhotoContext SqlitePhotoContextDAO::getFirst()
{
    static const QueryArgs args(string(), 1, PhotoContextTable::Columns::CREATEDAT, QueryArgs::ASC);

    std::vector<PhotoContext> firstContext;
    firstContext.reserve(1); //Avoid allocating more than 1 PhotoContext when inserting the first item
    genericDao.listAll(firstContext, args);

    return firstContext.empty() ? PhotoContext() : *firstContext.begin();
}

bool SqlitePhotoContextDAO::exist(long contextId)
{
    return genericDao.exist(Tables::PhotoContextTable::Columns::ID, contextId);
}

PhotoContextDAO::ContextList SqlitePhotoContextDAO::list(Visibility minVisibility){
    Query listByVisibilityStmt(getConnector(), table, Table::WhereCompare(Columns::VISIBILITY, ">=", "?"));
    listByVisibilityStmt.appendValue((int)minVisibility);

    return genericDao.list(listByVisibilityStmt);
}

PhotoContextDAO::ContextList SqlitePhotoContextDAO::listAll()
{
    return genericDao.listAll();
}

PhotoContextDAO::ContextList SqlitePhotoContextDAO::listIn(const data::PhotoContextDAO::SetId &collIds)
{
    return genericDao.listAll(Table::WhereIn(Columns::ID, collIds.begin(), collIds.end()));
}

long SqlitePhotoContextDAO::getRootCollectionId(long ctxId)
{
    string whereEquals = Table::WhereEquals(PhotoContextTable::Columns::ID,ctxId);
    set<long> ids = queryRootCollectionsImpl(whereEquals);
    return ids.empty() ? PhotoContext::INVALID_COLLECTION_ID : *ids.begin();
}

std::set<long> SqlitePhotoContextDAO::listRootCollections()
{
    return queryRootCollectionsImpl();
}

bool SqlitePhotoContextDAO::isRootCollection(long collectionId)
{
    string whereEquals = Table::WhereEquals(PhotoContextTable::Columns::ROOT_PHOTOCOLLECTION
                                          , collectionId);
    return !queryRootCollectionsImpl(whereEquals).empty();
}

long SqlitePhotoContextDAO::insert(const PhotoContext &photoContext)
{
    return genericDao.insert(photoContext);
}

bool SqlitePhotoContextDAO::update(const PhotoContext &photoContext)
{
    string where = table.whereIdEquals(photoContext.getId());
    return genericDao.update(photoContext, where);
}

bool SqlitePhotoContextDAO::remove(long contextId)
{
    return genericDao.remove(contextId);
}

set<long> SqlitePhotoContextDAO::queryRootCollectionsImpl(const std::string & whereCondition)
{
    IdDbReader idReader(PhotoContextTable::Columns::ROOT_PHOTOCOLLECTION);
    idReader.setDefault(PhotoContext::INVALID_COLLECTION_ID);

    genericDao.getDatabase()->query(idReader, this->table, whereCondition, {PhotoContextTable::Columns::ROOT_PHOTOCOLLECTION});
    return idReader.getIds();
}

SqliteConnector &SqlitePhotoContextDAO::getConnector(){
    CHECK_NOT_NULL(genericDao.getDatabase());
    return *genericDao.getDatabase();
}

}//namespace
