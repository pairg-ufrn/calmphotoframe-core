#include "SqliteConfigurationsDAO.h"

#include "data/data-sqlite/tables/ConfigurationsTable.h"

using namespace Tables::ConfigurationsTable;

namespace calmframe {

SqliteConfigurationsDAO::SqliteConfigurationsDAO(data::SqliteConnector * database)
    : table()
    , database(database)
    , dao{&table, database}
{
    Tables::ConfigurationsTable::buildTable(table);
}

bool SqliteConfigurationsDAO::createTableIfNotExist(data::SqliteConnector & connector){
    return dao.createTableIfNotExist(connector);   
}

std::string SqliteConfigurationsDAO::get(const std::string & key){
    ConfigValue value;
    if(dao.get(value, Table::WhereEquals(Columns::key, key, true))){
        return value.getValue();
    }
    
    return std::string();
}

void SqliteConfigurationsDAO::put(const std::string & key, const std::string & value){
    dao.insertOrReplace(ConfigValue{key, value});
}


}//namespace
