#ifndef SQLITECONFIGURATIONSDAO_H
#define SQLITECONFIGURATIONSDAO_H

#include "data/ConfigurationsDAO.h"

#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/dao/SqliteDAO.hpp"
#include "data/data-sqlite/dao/GenericDAO.hpp"
#include "data/data-sqlite/parsers/ConfigParser.h"

#include "model/ConfigValue.h"

namespace calmframe {

class SqliteConfigurationsDAO : public data::SqliteDAO, public ConfigurationsDAO{

    data::Table table;
    data::SqliteConnector * database;
    GenericDAO<ConfigValue, ConfigParser> dao;
    
public:    
    SqliteConfigurationsDAO(data::SqliteConnector * database);

    bool createTableIfNotExist(data::SqliteConnector &connector) override;
    
    std::string get(const std::string & key) override;
    void put(const std::string & key, const std::string & value) override;
};

}//namespace

#endif // CONFIGURATIONSDAO_H
