#ifndef SQLITESESSIONDAO_H
#define SQLITESESSIONDAO_H

#include "SqliteDAO.hpp"
#include "GenericDAO.hpp"
#include "data/SessionDAO.h"
#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/parsers/SessionDbParser.h"

#include <string>

namespace calmframe {

class SqliteSessionDAO : public SessionDAO, public data::SqliteDAO
{
private:
    data::Table sessionTable;
    data::SqliteConnector * database;
    GenericDAO<Session, SessionDbParser> genericDAO;
public:
    SqliteSessionDAO(data::SqliteConnector * database);
    ~SqliteSessionDAO();

    virtual bool createTableIfNotExist(data::SqliteConnector & database) override;

    virtual bool getUserSession(long userId, Session & sessionOut) const override;
    virtual bool get(long sessionId, Session & sessionOut) const override;
    virtual bool getByToken(const std::string & token, Session & sessionOut) const override;
    virtual bool exist(long sessionId) const override;
    virtual long insert(const Session & session) override;
    virtual std::vector<Session> listAll() override;
    virtual int update(const Session & session) override;
    virtual bool remove(const Session & session) override;
    virtual int removeExpired() override;
    
protected:
    bool get(Session &userOut, const std::string &whereCondition) const;
    const std::vector<std::string> & getInsertColumns() const;
    const std::vector<std::string> & getUpdateColumns() const;
};

}

#endif // SQLITESESSIONDAO_H
