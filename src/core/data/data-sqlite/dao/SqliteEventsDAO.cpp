#include "SqliteEventsDAO.h"

#include "../tables/EventsTable.h"
#include "exceptions/NotFoundMember.h"

namespace calmframe {

using namespace std;

SqliteEventsDAO::SqliteEventsDAO(SqliteConnector * connector)
    : dao(&table, connector)
{
    Tables::EventsTable::buildTable(table);
}

bool SqliteEventsDAO::createTableIfNotExist(SqliteConnector & connector){
    return dao.createTableIfNotExist(connector);
}

Event SqliteEventsDAO::get(long id){
    Event evt;
    if(dao.get(evt, table.whereIdEquals(id))){
       return evt;
    }

    throw NotFoundMember();
}

long SqliteEventsDAO::insert(const Event & evt){
    return dao.insert(evt);
}

bool SqliteEventsDAO::exist(long eventId){
    return dao.exist(table.getIdColumn(), eventId);
}

EventsDAO::EventsList SqliteEventsDAO::listRecents(int limit){
    return dao.listAll(QueryArgs{std::string(), limit
                , Table::PAREN("datetime", Tables::EventsTable::Columns::createdAt) //convert colum to datetime
                , QueryArgs::DESC});
}

}//namespace
