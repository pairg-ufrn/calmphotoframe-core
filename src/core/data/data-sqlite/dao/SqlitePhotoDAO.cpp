#include "SqlitePhotoDAO.h"
#include "SqlitePhotoMetadataDAO.h"
#include "SqlitePhotoMarkerDAO.h"

#include "data/data-sqlite/Sql.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/SqlException.hpp"
#include "data/data-sqlite/tables/PhotoTable.h"
#include "data/data-sqlite/ScopedTransaction.h"
#include "data/data-sqlite/Query.h"

#include "utils/IllegalArgumentException.hpp"
#include "utils/CommonExceptions.h"
#include "utils/StringCast.h"
#include "utils/Log.h"

#include <vector>
#include <algorithm>

using namespace std;
using namespace calmframe::utils;
using namespace Tables;

namespace calmframe{
namespace data{

class PhotoQueryArgs{
public:
    PhotoQueryArgs(const std::string * queryString, const SetId * photoIds, long ctxId)
        : queryStr(queryString)
        , photosFilter(photoIds)
        , contextId(ctxId)
    {}

    int ctxId() const{
        return contextId;
    }

    bool hasCtxId() const{
        return contextId >= 0;
    }

    bool hasQuery() const{
        return queryStr!= NULL && !queryStr->empty();
    }
    const std::string & query(){
        CHECK_NOT_NULL(queryStr);
        return *queryStr;
    }

    bool hasIdsFilter() const{
        return photosFilter != NULL && !photosFilter->empty();
    }
    const SetId & idsFilter() const{
        CHECK_NOT_NULL(photosFilter);
        return *photosFilter;
    }

    void includePhotos(const SetId & photoIds){
        includedPhotoIds.insert(photoIds.begin(), photoIds.end());
    }

    const SetId & includedPhotos() const{
        return includedPhotoIds;
    }

//private:
    const std::string * queryStr;
    const SetId * photosFilter;
    SetId includedPhotoIds;
    long contextId;
};

SqlitePhotoDAO::SqlitePhotoDAO(SqliteConnector* sqliteDb, SqlitePhotoMarkerDAO * photoMarkerDAO, SqlitePhotoMetadataDAO *photoMetadataDAO)
	:database(sqliteDb)
    , photoMarkerDAO(photoMarkerDAO)
    , photoMetadataDAO(photoMetadataDAO)
    , photoGenericDAO(&photoTable, sqliteDb)
{
    PhotoTable::buildTable(photoTable);
}


SqlitePhotoMarkerDAO &SqlitePhotoDAO::markerDAO(){
    CHECK_NOT_NULL(this->photoMarkerDAO);
    return *photoMarkerDAO;
}

bool SqlitePhotoDAO::createTableIfNotExist(SqliteConnector& database) {
    std::string createStatement = photoTable.getCreateStatement();
    return database.execute(createStatement);
}

PhotoDAO::PhotoList SqlitePhotoDAO::listAll(const string & queryString) {
    return listPhotos(queryString, NULL);
}

PhotoDAO::PhotoList SqlitePhotoDAO::listIn(const std::set<long> &photoIds, const string & query){
    if(photoIds.empty()){
        return PhotoList();
    }
    return listPhotos(query, &photoIds);
}

PhotoDAO::PhotoList SqlitePhotoDAO::listByContext(long contextId){
    return listPhotos("", NULL, contextId);
}

PhotoDAO::PhotoList SqlitePhotoDAO::listPhotos(const std::string & whereCondition){
    vector<Photo> photos = whereCondition.empty()
                                                  ? photoGenericDAO.listAll()
                                                  : photoGenericDAO.listAll(whereCondition);
    completePhotosData(photos);

    return photos;
}

PhotoDAO::PhotoList SqlitePhotoDAO::listPhotos(const std::string & queryString, const SetId * photoIds
    , long ctxId)
{
    PhotoQueryArgs args(&queryString, photoIds, ctxId);

    if(args.hasQuery()){
        SetId matchPhotoIds = this->markerDAO().queryPhotos(args.query());
        args.includePhotos(matchPhotoIds);
    }

    string whereCond = whereMatchQuery(args);
    Query query(*database, photoTable, whereCond);
    if(args.hasQuery()){
        query.appendTextValue(args.query());
    }

    vector<Photo> photos;

    GenericDbReader<Photo, PhotoDbParser> reader(&photos);
    query.read(reader);

    completePhotosData(photos);

    return photos;
}

string SqlitePhotoDAO::whereMatchQuery(const PhotoQueryArgs & args)
{
    string whereCond;

    if(args.hasQuery()){
        whereCond = photoTable.queryTemplate("?1");
     }

    const auto & photoIds = args.includedPhotos();
    if(photoIds.size() > 0){
        whereCond = Sql::OR(Table::WhereIn(PhotoTable::Columns::ID,
                                photoIds.begin(),
                                photoIds.end()),
                            whereCond);
    }

    if(args.hasIdsFilter()){
        auto inFilterIds = Table::WhereIn(
                                PhotoTable::Columns::ID,
                                args.idsFilter().begin(),
                                args.idsFilter().end());

        whereCond = Sql::AND(whereCond, inFilterIds);
    }

    if(args.hasCtxId()){
        string ctxEquals = Table::WhereEquals(PhotoTable::Columns::ContextId, args.ctxId());
        whereCond = Sql::AND(whereCond, ctxEquals);
    }

    return whereCond;
}

void SqlitePhotoDAO::completePhotosData(vector<Photo>& photos)
{
    if(!photos.empty()){
        map<long, vector<PhotoMarker> > markersByPhotoId = listAllMarkersByPhotoId();

        for(size_t i=0; i < photos.size(); ++i){
            Photo & photo = photos[i];
            photo.getDescription().setMarkers(markersByPhotoId[photo.getId()]);
        }
        //FIXME: utilizar JOIN para obter metadados de cada foto
        this->photoMetadataDAO->listAll(photos);
    }
}

std::map<long, std::vector<PhotoMarker> > SqlitePhotoDAO::listAllMarkersByPhotoId() {
	map<long, vector<PhotoMarker> > markersByPhotoId;
	vector<PhotoMarker> allMarkers = this->photoMarkerDAO->listAll();

	for(size_t i=0; i < allMarkers.size(); ++i){
		PhotoMarker & marker = allMarkers.at(i);

		long photoId = marker.getPhotoId();
		markersByPhotoId[photoId].push_back(marker);
	}

    return markersByPhotoId;
}


long SqlitePhotoDAO::insert(const Photo& photo) {
   return this->insertImplementation(photo);
}
long SqlitePhotoDAO::insert(const Photo &photo, long photoId){
    return this->insertImplementation(photo, &photoId);
}
long SqlitePhotoDAO::insertImplementation(const Photo &photo, long *photoId){
    long photoIdValue = (photoId == NULL ? 0 : *photoId);

    ScopedTransaction transaction(database);

    Photo photoToInsert(photo);
    photoToInsert.setId(photoIdValue);

    long insertedId = photoGenericDAO.insert(photoToInsert,getInsertColumns(photoIdValue));

    photoMarkerDAO->insert(insertedId, photo.getDescription().getMarkers());
    photoMetadataDAO->insert(insertedId, photo.getMetadata());

    transaction.commit();

    return insertedId;

}

bool SqlitePhotoDAO::update(const Photo& photo) {
	return updateImplementation(photo, true);
}

bool SqlitePhotoDAO::updatePhotoDescription(long photoId, const PhotoDescription& photoDescription) {
    return updateImplementation(Photo(photoId, "", photoDescription), false);
}

bool SqlitePhotoDAO::updateImplementation(const Photo& photo, bool updateImagePath) {
    int updatedCount = 0;

    ScopedTransaction transaction(database);

    bool updatedPhotoMarkers = tryUpdatePhotoMarkers(photo.getId(), photo.getDescription().getMarkers());
    if(!updatedPhotoMarkers ||
            !photoMetadataDAO->update(photo.getId(), photo.getMetadata()))
    {
        logError("Could not update photo markers from photo = %ld", photo.getId());
        database->rollback();
        return false;
    }
    updatedCount = this->photoGenericDAO.update(photo.getId(), photo, getUpdateColumns(updateImagePath));

    if(updatedCount != 1){
        logError("Error updating photo. Updated %d when expected %d", updatedCount, 1);
        transaction.rollback();
        return false;
    }
    transaction.commit();

    return updatedCount > 0;
}

bool SqlitePhotoDAO::tryUpdatePhotoMarkers(long photoId, const vector<PhotoMarker> & newPhotoMarkers) {
	bool success = false;
	this->database->beginTransaction();
	try{
		if(this->photoMarkerDAO->removeMarkersFromPhoto(photoId)){
			success = true;
            if(newPhotoMarkers.size()> 0u){
                photoMarkerDAO->insert(photoId, newPhotoMarkers);
			}
		}
	}
	catch(SqlException & exception){
		logError("Could not update markers on photo because of: %s", exception.what());
		success = false;
	}
	if(success){
		database->commit();
	}
	else{
		database->rollback();
	}
	return success;
}


bool SqlitePhotoDAO::remove(const Photo& photo) {
    ScopedTransaction transaction(this->database);

    bool removed = false;

    if(this->photoMarkerDAO->removeMarkersFromPhoto(photo.getId())
            && this->photoMetadataDAO->remove(photo.getId()))
    {
        int removeCount = photoGenericDAO.remove(photo.getId());
        //FIXME: delegar remoção de imagem para outra classe
        removed = (removeCount == 1);
    }

    transaction.commit();

    return removed;
}

bool SqlitePhotoDAO::exist(long photoId){
    return photoGenericDAO.exist(PhotoTable::Columns::ID
                                 , Table::WhereEquals(PhotoTable::Columns::ID, photoId));
}

bool SqlitePhotoDAO::getPhoto(long photoId, Photo& photoOut) {
    Photo tempPhoto;
    if(photoGenericDAO.get(tempPhoto, Table::WhereEquals(PhotoTable::Columns::ID, photoId))){
        photoOut = tempPhoto;
        vector<PhotoMarker> markers =  this->photoMarkerDAO->listAll(tempPhoto);
        photoOut.getDescription().setMarkers(markers);
        photoOut.setMetadata(this->photoMetadataDAO->get(photoId));
        return true;
    }
    return false;
}

vector<string> SqlitePhotoDAO::getInsertColumns(long photoIdValue){
    vector<string> columns = photoTable.getInsertColumns();
    if(photoIdValue > 0){
        columns.push_back(PhotoTable::Columns::ID);
    }

    return columns;
}
vector<string> SqlitePhotoDAO::getUpdateColumns(bool updateImagePath){
    vector<string> columns = photoTable.getUpdateColumns();

    if(!updateImagePath){
        auto iter = std::remove(columns.begin(), columns.end(), PhotoTable::Columns::IMAGEPATH);
        columns.erase(iter, columns.end());
    }

    return columns;
}

} /* namespace data */
} /* namespace calmframe */


