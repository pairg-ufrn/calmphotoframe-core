#include "SqlitePhotoCollectionDAO.h"

#include "data/PhotoDAO.hpp"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/tables/PhotoCollectionTable.h"
#include "data/data-sqlite/tables/PhotoContextTable.h"
#include "data/data-sqlite/ScopedTransaction.h"
#include "data/data-sqlite/SqlException.hpp"
#include "data/data-sqlite/parsers/IdDbReader.h"

#include "utils/CommonExceptions.h"
#include "utils/NullPointerException.hpp"
#include "utils/StringCast.h"
#include "model/photo/PhotoCollection.h"

#include "utils/Log.h"

#include <string>

namespace calmframe {
namespace data {

using namespace std;
using namespace Tables;


SqlitePhotoCollectionDAO::SqlitePhotoCollectionDAO(SqliteConnector *database)
    : photoCollectionTable(Tables::PhotoCollectionTable::buildTable())
    , genericDAO(&photoCollectionTable, database)
    , collMappingDAO(database)
{
}

SqlitePhotoCollectionMappingDAO &SqlitePhotoCollectionDAO::collectionMappingDAO(){
    return this->collMappingDAO;
}

bool SqlitePhotoCollectionDAO::createTableIfNotExist(SqliteConnector &database){
    return this->genericDAO.createTableIfNotExist(database)
            && this->collectionMappingDAO().createTableIfNotExist(database);
}

bool SqlitePhotoCollectionDAO::get(long collectionId, PhotoCollection & outCollection){

    if(genericDAO.get(outCollection, this->photoCollectionTable.whereIdEquals(collectionId))){
        outCollection.setPhotos(collectionMappingDAO().getCollectionPhotos(collectionId));
        outCollection.setSubCollections(collectionMappingDAO().getSubCollections(collectionId));

        return true;
    }

    return false;
}

SetId SqlitePhotoCollectionDAO::getCollectionPhotos(long collectionId){
    return collectionMappingDAO().getCollectionPhotos(collectionId);
}

bool SqlitePhotoCollectionDAO::exist(long id){
    return genericDAO.exist(PhotoCollectionTable::Columns::ID
                                  , Table::WhereEquals(PhotoCollectionTable::Columns::ID, id));
}

static map<long, int> mapCollectionIndexes(const std::vector<PhotoCollection> & collections){
    map<long, int> collIndexes;
    for(size_t i=0; i < collections.size(); ++i){
        const PhotoCollection & coll = collections[i];
        collIndexes[coll.getId()] = i;
    }
    return collIndexes;
}

std::vector<PhotoCollection> SqlitePhotoCollectionDAO::listAll(){
    return listCollections();
}

std::vector<PhotoCollection> SqlitePhotoCollectionDAO::listIn(const std::set<long> &collIds)
{
    const string where = Table::WhereIn(Tables::PhotoCollectionTable::Columns::ID, collIds.begin(), collIds.end());
    return listCollections(where);
}


std::vector<PhotoCollection> SqlitePhotoCollectionDAO::getByName(const std::string &name){
    return listCollections(Table::WhereEquals(PhotoCollectionTable::Columns::NAME, name, true));
}

PhotoCollectionDAO::CollectionList &SqlitePhotoCollectionDAO::getByContext(long ctxId, CollectionList &list)
{
    return listCollections(list, Table::WhereEquals(PhotoCollectionTable::Columns::PARENT_CONTEXT, ctxId));
}

PhotoCollectionDAO::CollectionList SqlitePhotoCollectionDAO::listCollections(const std::string & whereCondition)
{
    vector<PhotoCollection> collections;
    return listCollections(collections, whereCondition);
}

PhotoCollectionDAO::CollectionList &
SqlitePhotoCollectionDAO::listCollections(CollectionList & collections, const string & whereCondition)
{
    collections =  this->genericDAO.listAll(whereCondition);

    map<long, int> collIndexes = mapCollectionIndexes(collections);

    for(const auto & map : collectionMappingDAO().listAll()){

        auto found = collIndexes.find(map.getCollectionId());
        if(found != collIndexes.end()){
            int index = found->second;
            PhotoCollection & coll = collections[index];
            if(map.getType() == PhotoCollectionMap::PhotoMapping){
                coll.putPhoto(map.getItemId());
            }
            else if(map.getType() == PhotoCollectionMap::SubCollectionMapping){
                coll.getSubCollections().put(map.getItemId());
            }
        }
    }

    return collections;
}


long SqlitePhotoCollectionDAO::insert(const PhotoCollection &photoCollection, long parentCollId)
{
    ScopedTransaction transaction(genericDAO.getDatabase());

    long createdCollId;

    if(photoCollection.hasPhotoContext()){
        createdCollId = this->insert(photoCollection);
    }
    else{
        PhotoCollection parent = PhotoCollectionDAO::get(parentCollId);

        long parentCtx = parent.getParentContext();
        PhotoCollection cpy = photoCollection;
        cpy.setParentContext(parentCtx);

        createdCollId = insert(cpy);
    }

    set<long> createdIdSet;
    createdIdSet.insert(createdCollId);

    collectionMappingDAO().addSubCollections(parentCollId, createdIdSet);

    transaction.commit();
    return createdCollId;
}

long SqlitePhotoCollectionDAO::insert(const PhotoCollection &photoCollection){
    return insertImpl(photoCollection);
}

long SqlitePhotoCollectionDAO::insertAt(long collectionId, const PhotoCollection &photoCollection)
{
    return insertImpl(photoCollection, collectionId);
}

long SqlitePhotoCollectionDAO::insertImpl(const PhotoCollection &photoCollection, long idToInsert)
{
    ScopedTransaction scopedTransaction(genericDAO.getDatabase());

    long collectionId;

    if(idToInsert < 0){
        collectionId = genericDAO.insert(photoCollection);
    }
    else{
        PhotoCollection collWithId = photoCollection;
        collWithId.setId(idToInsert);

        vector<string> insertColumns = photoCollectionTable.getInsertColumns();
        insertColumns.push_back(PhotoCollectionTable::Columns::ID);
        collectionId = genericDAO.insert(collWithId, insertColumns);
    }

    if(!photoCollection.getPhotos().empty()){
        collMappingDAO.insertCollectionPhotos(collectionId, photoCollection.getPhotos());
    }
    if(!photoCollection.getSubCollections().empty()){
        collMappingDAO.insertSubCollections(collectionId, photoCollection.getSubCollections().getIds());
    }
    scopedTransaction.commit();

    return collectionId;
}

bool SqlitePhotoCollectionDAO::update(const PhotoCollection &photoCollection){
    ScopedTransaction transaction(genericDAO.getDatabase());

    updateCollection(photoCollection   , transaction);
    updateCollectionIds(photoCollection, transaction);

    transaction.commit();
    return true;
}

bool SqlitePhotoCollectionDAO::remove(long collectionId)
{
    ScopedTransaction transaction(genericDAO.getDatabase());
    if(this->genericDAO.remove(Table::WhereEquals(PhotoCollectionTable::Columns::ID, collectionId)) > 0){
        collectionMappingDAO().removeCollection(collectionId);

        transaction.commit();
        return true;
    }
    return false;
}

int SqlitePhotoCollectionDAO::remove(const std::set<long> &collectionIds)
{
    ScopedTransaction transaction(genericDAO.getDatabase());

    collectionMappingDAO().removeCollections(collectionIds);
    string whereCond = Table::WhereIn(PhotoCollectionTable::Columns::ID, collectionIds.begin(), collectionIds.end());
    int removedCollections = this->genericDAO.remove(whereCond);

    transaction.commit();
    return removedCollections;
}

int SqlitePhotoCollectionDAO::removeCollectionsWithoutParent()
{
    static const string whereCond = whereCollectionHasNoParent();
    return genericDAO.remove(whereCond);
}

std::set<long> SqlitePhotoCollectionDAO::getPhotosWithoutParent(const std::set<long> &photoIds)
{
    set<long> relatedPhotoIds;
    collectionMappingDAO().listPhotoItemsInto(relatedPhotoIds);

    set<long> photosWithoutParent;
    std::set_difference(photoIds.begin(), photoIds.end()
                        , relatedPhotoIds.begin(), relatedPhotoIds.end()
                        ,std::inserter(photosWithoutParent, photosWithoutParent.end()));

    return photosWithoutParent;
}


void SqlitePhotoCollectionDAO::updateCollection(const PhotoCollection &photoCollection, ScopedTransaction & transaction)
{
    long collectionId = photoCollection.getId();

    const string where = Table::WhereEquals(PhotoCollectionTable::Columns::ID, collectionId);
    int updatedCount = this->genericDAO.update(photoCollection, where);
    if(updatedCount != 1){
        transaction.rollback();
        string errMsg = string("Error updating photoCollection with id ")
                              .append(utils::StringCast::instance().toString(collectionId))
                              .append(". Updated ")
                              .append(utils::StringCast::instance().toString(updatedCount))
                              .append(" rows when expected update 1");
        throw SqlException(errMsg);
    }
}
void SqlitePhotoCollectionDAO::updateCollectionIds(const PhotoCollection &photoCollection, ScopedTransaction & transaction)
{
    long collectionId = photoCollection.getId();

    const set<long> & photoIds = photoCollection.getPhotos();
    const set<long> & subcollectionIds = photoCollection.getSubCollections().getIds();

    size_t updatedItems = collectionMappingDAO().updateCollectionItems(collectionId, photoIds, subcollectionIds);
    if(updatedItems != (photoIds.size() + subcollectionIds.size())){
        transaction.rollback();
        string errMsg = string("Failed to update items of PhotoCollection ")
                              .append(utils::StringCast::instance().toString(collectionId))
                              .append(". Expected ")
                              .append(utils::StringCast::instance().toString((long)photoIds.size()))
                              .append(" photos and ")
                              .append(utils::StringCast::instance().toString((long)subcollectionIds.size()))
                              .append(" subCollections, but found ")
                              .append(utils::StringCast::instance().toString((long)updatedItems));
        throw SqlException(errMsg);
    }
}

SetId SqlitePhotoCollectionDAO::listIdsByContext(long ctxId)
{
    return listIds(Table::WhereEquals(PhotoCollectionTable::Columns::PARENT_CONTEXT, ctxId));
}

SetId SqlitePhotoCollectionDAO::listIds(const string & whereCondition) const
{
    IdDbReader idReader(PhotoCollectionTable::Columns::ID);
    idReader.setDefault(PhotoCollection::INVALID_ID);

    genericDAO.getDatabase()->query(idReader, photoCollectionTable, whereCondition, {string(PhotoCollectionTable::Columns::ID)});

    return idReader.getIds();
}

SetId SqlitePhotoCollectionDAO::getCollectionsWithoutParent() const
{
    static const string whereCondition = whereCollectionHasNoParent();

    return listIds(whereCondition);
}

PhotoCollectionDAO::CollectionTree &
SqlitePhotoCollectionDAO::getCollectionTree(long collId, PhotoCollectionDAO::CollectionTree & collTree)
{
    //TODO: optimize
    vector<PhotoCollection> allCollections = listAll();
    std::map<long, int> idToIndex;
    for(size_t i=0; i < allCollections.size(); ++i){
        idToIndex[allCollections[i].getId()] = i;
    }

    addToTree(collId, collTree, allCollections, idToIndex);

    return collTree;
}

void SqlitePhotoCollectionDAO::addToTree(long collId, PhotoCollectionDAO::CollectionTree & collTree, const std::vector<PhotoCollection> & allCollections, const std::map<long, int> & idToIndex)
{
    std::map<long, int>::const_iterator found = idToIndex.find(collId);
    if(found != idToIndex.end() && collTree.find(collId) == collTree.end()){
        const PhotoCollection & coll = allCollections[found->second];
        collTree[collId] = coll;

        const SetId & subColls = coll.getSubCollections().getIds();
        SetId::const_iterator iter = subColls.begin();
        while(iter != subColls.end()){
            addToTree(*iter, collTree, allCollections, idToIndex);
            ++iter;
        }
    }
}

void SqlitePhotoCollectionDAO::addPhotos(const std::set<long> &collIds
                                              , const std::set<long> &photoIds)
{
    ScopedTransaction scopedTransaction(genericDAO.getDatabase());

    for(std::set<long>::const_iterator i = collIds.begin(); i != collIds.end(); ++i){
        long collId = *i;
        collectionMappingDAO().addCollectionPhotos(collId, photoIds);
    }

    scopedTransaction.commit();
}

void SqlitePhotoCollectionDAO::removePhotoFromCollections(long photoId)
{
    collectionMappingDAO().removePhotoFromCollections(photoId);
}

void SqlitePhotoCollectionDAO::removePhotosFromCollections(const std::set<long> &collIds, const std::set<long> &photoIds)
{
    ScopedTransaction scopedTransaction(genericDAO.getDatabase());

    for(std::set<long>::const_iterator i = collIds.begin(); i != collIds.end(); ++i){
        long collId = *i;
        collectionMappingDAO().removeCollectionPhotos(collId, photoIds);
    }

    scopedTransaction.commit();
}

void SqlitePhotoCollectionDAO::addItemsToCollections(const std::set<long> &targetCollections, const std::set<long> &photoIds, const std::set<long> &subCollections)
{
    collectionMappingDAO().addItemsToCollections(targetCollections, photoIds, subCollections);
}

void SqlitePhotoCollectionDAO::removeItemsFromCollections(const std::set<long> &targetCollections, const std::set<long> &photoIds, const std::set<long> &subCollections)
{
    collectionMappingDAO().removeItemsFromCollections(targetCollections, photoIds, subCollections);
}

void SqlitePhotoCollectionDAO::moveCollectionItems(long collectionId, const SetId & targetCollections
                                             , const SetId & photosToMove
                                             , const SetId & subCollectionsToMove)
{
    ScopedTransaction transaction(genericDAO.getDatabase());
    collectionMappingDAO().removeCollectionItems(collectionId, photosToMove, PhotoCollectionMap::PhotoMapping);
    collectionMappingDAO().removeCollectionItems(collectionId, subCollectionsToMove, PhotoCollectionMap::SubCollectionMapping);
    this->addItemsToCollections(targetCollections, photosToMove, subCollectionsToMove);
    transaction.commit();
}

void SqlitePhotoCollectionDAO::addPhoto(long photoId, long collectionId)
{
    collectionMappingDAO().addPhotoToCollection(photoId, collectionId);
}

std::vector<PhotoCollection> SqlitePhotoCollectionDAO::getSubCollections(long parentCollectionId)
{
    set<long> subcollectionIds = collectionMappingDAO().getSubCollections(parentCollectionId);
    return listIn(subcollectionIds);
}

const string SqlitePhotoCollectionDAO::whereCollectionHasNoParent()
{
    static const string itemIdIsCollection = Table::WhereEquals(PhotoCollectionItemsTable::Columns::ITEMTYPE, PhotoCollectionMap::SubCollectionMapping);

    static const string collId = "collection_id";

    static const vector<string> itensColumns(1, Table::AS(PhotoCollectionItemsTable::Columns::ITEMID, collId));

    static const string selectSubcollections = Table::getQueryStatement(PhotoCollectionItemsTable::NAME
                                                   , itensColumns
                                                   , itemIdIsCollection);

    static const vector<string> rootPhotoCollColumn(1, Table::AS(PhotoContextTable::Columns::ROOT_PHOTOCOLLECTION, collId));
    static const string selectRootCollections = Table::getQueryStatement(PhotoContextTable::NAME, rootPhotoCollColumn);

    static const string unionSubcollectionsAndRoot = Table::UNION(selectSubcollections, selectRootCollections);
    static const string isSubcollectionOrRoot = Table::WhereIn(PhotoCollectionTable::Columns::ID
                                                                , unionSubcollectionsAndRoot);

    return Table::NOT(isSubcollectionOrRoot);
}

}//data
}//calmframe

