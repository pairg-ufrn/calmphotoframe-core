#ifndef SQLITEUSERDAO_H
#define SQLITEUSERDAO_H

#include "SqliteDAO.hpp"
#include "GenericDAO.hpp"
#include "data/UserDAO.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/tables/UserTable.h"

#include "data/data-sqlite/parsers/UserAccountParser.h"
#include "data/data-sqlite/parsers/UserDbParser.h"

namespace calmframe{

using ::calmframe::data::SqliteConnector;

class SqliteUserDAO : public UserDAO, public data::SqliteDAO
{
private:

    data::Table userTable;
    SqliteConnector * database;
    GenericDAO<UserAccount, UserAccountParser> accountDAO;
    GenericDAO<User, UserDbParser> userDao;
public:
    SqliteUserDAO(SqliteConnector * database);
    ~SqliteUserDAO();

    virtual bool createTableIfNotExist(SqliteConnector & database);

    virtual UserCollection listUsers();
    UserCollection listUsersIn(const data::SetId & userIds) override;

    virtual bool get(long userId, UserAccount & userOut);
    virtual bool getUser(long userId, User & userOut);
    virtual bool getByLogin(const std::string & userName, UserAccount & userOut);
    virtual bool exist(long userId) const;
    virtual bool exist(const std::string & userName) const;
    virtual long insert(const UserAccount & userAccount);
    virtual long insertAt(long userId, const UserAccount & userAccount);
    virtual bool remove(long userId);
    virtual bool update(const User & user);
    virtual bool updateAccount(long id, const UserAccount & account);
    
//    virtual long insert(const UserAccount & userAccount, long userId) =0;
    std::string whereNameEquals(const std::string &userName) const;
protected:
    std::string whereIdEquals(long userId) const;
    bool getUser(UserAccount & userOut, const std::string &whereCondition);
    bool existUser(const std::string &whereCondition) const;
};

}//namespace

#endif // SQLITEUSERDAO_H
