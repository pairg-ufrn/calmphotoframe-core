#ifndef SQLITEPHOTOCONTEXTDAO_H
#define SQLITEPHOTOCONTEXTDAO_H

#include "GenericDAO.hpp"
#include "data/data-sqlite/Table.h"

#include "model/photo/PhotoContext.h"
#include "data/PhotoContextDAO.h"
#include "data/data-sqlite/dao/SqliteDAO.hpp"
#include "data/data-sqlite/parsers/PhotoContextParser.h"
#include "../Query.h"

namespace calmframe {

namespace data {
    class Statement;
    class SqliteConnector;
}

class SqlitePhotoContextDAO : public data::PhotoContextDAO, public data::SqliteDAO
{
private:
    data::Table table;
    GenericDAO<PhotoContext, PhotoContextParser> genericDao;

public:
    SqlitePhotoContextDAO(data::SqliteConnector * database);
public:
    virtual bool createTableIfNotExist(data::SqliteConnector & database) override;

    long count() override;
    PhotoContext get(long contextId) override;
    PhotoContext getFirst() override;
    bool exist(long contextId) override;

    ContextList list(Visibility minVisibility = Visibility::Unknown) override;
    ContextList listAll() override;
    ContextList listIn(const SetId & collIds) override;

    long getRootCollectionId(long ctxId) override;
    std::set<long> listRootCollections() override;
    bool isRootCollection(long collectionId) override;
    long insert(const PhotoContext & photoContext) override;
    bool update(const PhotoContext & photoContext) override;
    bool remove(long contextId) override;

protected:
    std::set<long> queryRootCollectionsImpl(const std::string &whereCondition=std::string());
    data::SqliteConnector & getConnector();
};

}//namespace


#endif // SQLITEPHOTOCONTEXTDAO_H
