#include "SqlitePhotoMetadataDAO.h"

#include "SqlitePhotoDAO.h"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/Column.h"
#include "data/data-sqlite/DatabaseWriter.h"
#include "data/data-sqlite/DatabaseReader.h"
#include "data/data-sqlite/tables/PhotoTable.h"
#include "data/data-sqlite/tables/PhotoMetadataTable.h"

#include "model/photo/PhotoMetadata.h"
#include "utils/Log.h"

#include <vector>
#include <sstream>

using namespace calmframe;

namespace calmframe{
namespace data{

using namespace std;
using namespace Tables;
using namespace Tables::PhotoMetadataTable;

class MetadataWriter : public DatabaseWriter{
    long photoId;
    const PhotoMetadata * metadata;
    int writeCount;
public:
    MetadataWriter(long photoId, const PhotoMetadata * photoMetadata)
        : photoId(photoId)
        , metadata(photoMetadata)
        , writeCount(0)
    {}

    bool hasNext() const{
        return writeCount < 1;
    }

    void writeRow(Statement & statement){
        statement.appendValue(photoId, Columns::ID);
        statement.appendValue(metadata->getImageSize(), Columns::IMAGESIZE);
        statement.appendValue(metadata->getImageWidth(), Columns::IMAGEWIDTH);
        statement.appendValue(metadata->getImageHeight(), Columns::IMAGEHEIGHT);
        statement.appendTextValue(metadata->getCameraMaker(), Columns::CAMERAMAKER);
        statement.appendTextValue(metadata->getCameraModel(), Columns::CAMERAMODEL);
        statement.appendTextValue(metadata->getOriginalDate(), Columns::ORIGINALDATE);
        statement.appendTextValue(metadata->getOriginalFilename(), Columns::ORIGINALFILENAME);
        statement.appendRealValue(metadata->getLocation().getAltitude(), Columns::LOCATION_ALTITUDE);
        statement.appendRealValue(metadata->getLocation().getLatitude(), Columns::LOCATION_LATITUDE);
        statement.appendRealValue(metadata->getLocation().getLongitude(), Columns::LOCATION_LONGITUDE);
        statement.appendRealValue(metadata->getOrientation().getFlag(), Columns::ORIENTATION_FLAG);

        ++writeCount;
    }
};

class MetadataReader : public DatabaseReader{
    vector<PhotoMetadata> * metadataList;
    int readLimit;
    int readCount;
public:
    MetadataReader(vector<PhotoMetadata> * photoMetadataList, int readLimit = -1)
        : metadataList(photoMetadataList)
        , readLimit(readLimit)
        , readCount(0)
    {}

    bool hasFinished() const{
        return readLimit > 0 && readCount >= readLimit;
    }

    virtual void readRow(const QueryData & data){
        PhotoMetadata photoMetadata;

        photoMetadata.setPhotoId     (data.getInt  (Columns::ID, PhotoMetadata::UNKNOWN_PHOTO_ID));
        photoMetadata.setCameraMaker (data.getString(Columns::CAMERAMAKER));
        photoMetadata.setCameraModel (data.getString(Columns::CAMERAMODEL));
        photoMetadata.setImageSize   (data.getInt(Columns::IMAGESIZE, PhotoMetadata::UNKNOWN_SIZE));
        photoMetadata.setImageWidth  (data.getInt(Columns::IMAGEWIDTH, PhotoMetadata::UNKNOWN_DIMENSION));
        photoMetadata.setImageHeight (data.getInt(Columns::IMAGEHEIGHT, PhotoMetadata::UNKNOWN_DIMENSION));
        photoMetadata.setOriginalDate(data.getString(Columns::ORIGINALDATE));
        photoMetadata.setOriginalFilename(data.getString(Columns::ORIGINALFILENAME));
        Location & location = photoMetadata.getLocation();
        location.setAltitude (data.getFloat(Columns::LOCATION_ALTITUDE, Location::UNKNOWN_ALTITUDE));
        location.setLatitude (data.getFloat(Columns::LOCATION_LATITUDE, Location::UNKNOWN_COORD));
        location.setLongitude(data.getFloat(Columns::LOCATION_LONGITUDE, Location::UNKNOWN_COORD));
        int orientationValue = data.getInt(Columns::ORIENTATION_FLAG, Orientation::UNKNOWN);
        photoMetadata.getOrientation().setFlag(Orientation::isValid(orientationValue)
                                               ? (Orientation::OrientationFlag)orientationValue
                                               : Orientation::UNKNOWN);

        this->metadataList->push_back(photoMetadata);
    }
};


SqlitePhotoMetadataDAO::SqlitePhotoMetadataDAO(SqliteConnector *database)
    : database(database)
{
    PhotoMetadataTable::buildTable(photoMetadataTable);
}

bool SqlitePhotoMetadataDAO::createTableIfNotExist(SqliteConnector &database){
    string createStatement = photoMetadataTable.getCreateStatement();
    return database.execute(createStatement);
}

PhotoMetadata SqlitePhotoMetadataDAO::get(long photoId){
    vector<PhotoMetadata> metadataList;
    MetadataReader reader(&metadataList, 1);

    stringstream whereCondition;
    whereCondition << Columns::ID << "=" << photoId;

    this->database->query(reader,this->photoMetadataTable, whereCondition.str());

    return metadataList.size() > 0 ? metadataList[0] : PhotoMetadata();
}

bool SqlitePhotoMetadataDAO::exist(long photoId){
    //TODO: otimizar (evitar carregar PhotoMetadata do banco de dados)
    return get(photoId).getPhotoId() != PhotoMetadata::UNKNOWN_PHOTO_ID;
}
void SqlitePhotoMetadataDAO::listAll(std::vector<Photo> &photos){
    MetadataMap metadataById;
    listAllById(metadataById);

    for(size_t i=0; i < photos.size(); ++i){
        Photo & photo = photos[i];
        MetadataMap::const_iterator found = metadataById.find(photo.getId());
        if(found != metadataById.end()){
            photo.setMetadata(found->second);
        }
    }
}
std::vector<PhotoMetadata> SqlitePhotoMetadataDAO::listAll(){
    vector<PhotoMetadata> metadataList;
    this->listAll(metadataList);
    return metadataList;
}

void SqlitePhotoMetadataDAO::listAllById(MetadataMap &metadataMap){
    std::vector<PhotoMetadata> metadataList;
    listAll(metadataList);

    std::vector<PhotoMetadata>::iterator iterator = metadataList.begin();
    while(iterator != metadataList.end()){
        metadataMap[iterator->getPhotoId()] = *iterator;

        ++iterator;
    }
}
void SqlitePhotoMetadataDAO::listAll(std::vector<PhotoMetadata> &metadataListOut){
    MetadataReader reader(&metadataListOut);
    this->database->query(reader,this->photoMetadataTable);
}

void SqlitePhotoMetadataDAO::insert(long photoId, const PhotoMetadata &metadata){
    MetadataWriter writer(photoId, &metadata);
    this->database->insert(writer, photoMetadataTable);
}

bool SqlitePhotoMetadataDAO::update(long photoId, const PhotoMetadata &metadata){
    MetadataWriter writer(photoId, &metadata);
    int updateCount = this->database->update(writer, photoMetadataTable, Table::WhereEquals(Columns::ID, photoId));
    bool updated = exist(photoId) ? updateCount == 1 : true;
    if(!updated){
        logWarning("SqlitePhotoMetadataDAO::update: \t" "Error when trying to update metadata %ld. Updated %d rows."
                , photoId, updateCount);
    }

    return updated;
}

bool SqlitePhotoMetadataDAO::remove(long photoId){
    int removedCount = this->database->remove(photoMetadataTable, Table::WhereEquals(Columns::ID, photoId));
    return removedCount > 0;
}

}
}
