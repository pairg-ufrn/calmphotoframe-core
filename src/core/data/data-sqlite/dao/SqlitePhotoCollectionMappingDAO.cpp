#include "SqlitePhotoCollectionMappingDAO.h"

#include "data/data-sqlite/tables/PhotoCollectionTable.h"
#include "data/data-sqlite/ScopedTransaction.h"
#include "data/data-sqlite/parsers/IdDbReader.h"
#include "utils/Log.h"

#include "utils/IllegalArgumentException.hpp"

namespace calmframe {

using namespace std;
using namespace data;
using namespace Tables;

SqlitePhotoCollectionMappingDAO::SqlitePhotoCollectionMappingDAO(data::SqliteConnector *database)
    : photoCollectionMappingTable(Tables::PhotoCollectionItemsTable::buildTable())
    , photoCollectionMapDAO(&photoCollectionMappingTable, database)
{}

bool SqlitePhotoCollectionMappingDAO::createTableIfNotExist(::calmframe::data::SqliteConnector &database)
{
    return photoCollectionMapDAO.createTableIfNotExist(database);
}

std::vector<PhotoCollectionMap> SqlitePhotoCollectionMappingDAO::listAll()
{
    return photoCollectionMapDAO.listAll();
}

void SqlitePhotoCollectionMappingDAO::listPhotoItemsInto(std::set<long> &outPhotoItems)
{
    IdDbReader idReader(PhotoCollectionItemsTable::Columns::ITEMID);

    static const std::string isPhotoItem = Table::WhereEquals(PhotoCollectionItemsTable::Columns::ITEMTYPE
                                                            , PhotoCollectionMap::PhotoMapping);
    photoCollectionMapDAO.getDatabase()->query(idReader
                                            , photoCollectionMappingTable
                                            , isPhotoItem, {PhotoCollectionItemsTable::Columns::ITEMID});
    outPhotoItems = idReader.getIds();
}

PhotoCollectionMap SqlitePhotoCollectionMappingDAO::insert(long collectionId, long photoId)
{
    PhotoCollectionMap map(collectionId, photoId);
    photoCollectionMapDAO.insert(map, PhotoCollectionItemsTable::getInsertColumns());
    return map;
}

int SqlitePhotoCollectionMappingDAO::insertCollectionPhotos(long collectionId, const std::set<long> &photoIds)
{
    return insertCollectionItemsImpl(collectionId, photoIds);
}

int SqlitePhotoCollectionMappingDAO::insertSubCollections(long collectionId, const std::set<long> &subCollectionIds)
{
    if(subCollectionIds.find(collectionId) != subCollectionIds.end()){
        throw CommonExceptions::IllegalArgumentException("Trying to put photoCollection as its own subCollection.");
    }
    return this->insertCollectionItemsImpl(collectionId, subCollectionIds, false, PhotoCollectionMap::SubCollectionMapping);
}

int SqlitePhotoCollectionMappingDAO::addSubCollections(long collectionId, const std::set<long> &subCollectionIds)
{
    if(subCollectionIds.find(collectionId) != subCollectionIds.end()){
        throw CommonExceptions::IllegalArgumentException("Trying to put photoCollection as its own subCollection.");
    }
    return this->insertCollectionItemsImpl(collectionId, subCollectionIds, true, PhotoCollectionMap::SubCollectionMapping);
}

int SqlitePhotoCollectionMappingDAO::addCollectionPhotos(long collectionId, const std::set<long> &photoIds)
{
    return this->insertCollectionItemsImpl(collectionId, photoIds, true);
}

void SqlitePhotoCollectionMappingDAO::addPhotoToCollection(long photoId, long collectionId)
{
    PhotoCollectionMap photocollectionMap(collectionId, photoId, PhotoCollectionMap::PhotoMapping);

    photoCollectionMapDAO.insert(photocollectionMap, PhotoCollectionItemsTable::getInsertColumns(), OnConflict::IGNORE);
}

int SqlitePhotoCollectionMappingDAO::addItemsToCollections(const std::set<long> &targetCollections, const std::set<long> &photoIds, const std::set<long> &subCollections)
{
    ScopedTransaction transaction(photoCollectionMapDAO.getDatabase());

    int changeCount = 0;
    for(std::set<long>::iterator i = targetCollections.begin();
            i != targetCollections.end(); ++i)
    {
        changeCount += this->insertCollectionItemsImpl(*i, photoIds, true, PhotoCollectionMap::PhotoMapping);
        changeCount += this->insertCollectionItemsImpl(*i, subCollections, true, PhotoCollectionMap::SubCollectionMapping);
    }

    transaction.commit();

    return changeCount;
}

int SqlitePhotoCollectionMappingDAO::insertCollectionItemsImpl(long collectionId, const std::set<long> &itemIds
            , bool ignoreConflict, PhotoCollectionMap::MappingType mappingType)
{
    std::vector<PhotoCollectionMap> collectionMaps = mapFromIds(itemIds, collectionId, mappingType);

    OnConflict resolution = ignoreConflict ? OnConflict::IGNORE : OnConflict::DEFAULT;

    return photoCollectionMapDAO.insertAll(collectionMaps.begin(), collectionMaps.end()
                                            , PhotoCollectionItemsTable::getInsertColumns(), resolution);
}

int SqlitePhotoCollectionMappingDAO::removeItemsFromCollections(const std::set<long> &targetCollections
            , const std::set<long> &photoIds , const std::set<long> &subCollections)
{
    ScopedTransaction transaction(photoCollectionMapDAO.getDatabase());

    int changeCount = 0;
    for(std::set<long>::iterator i = targetCollections.begin();
            i != targetCollections.end(); ++i)
    {
        long collId = *i;
        changeCount += this->removeCollectionItems(collId, photoIds, PhotoCollectionMap::PhotoMapping);
        changeCount += this->removeCollectionItems(collId, subCollections, PhotoCollectionMap::SubCollectionMapping);
    }

    transaction.commit();

    return changeCount;
}

int SqlitePhotoCollectionMappingDAO::removePhotoFromCollections(long photoId)
{
    set<long> photoIdSet;
    photoIdSet.insert(photoId);
    return removeItems(photoIdSet, PhotoCollectionMap::PhotoMapping, NULL);
}

int SqlitePhotoCollectionMappingDAO::removeCollectionPhotos(long collectionId, const std::set<long> &photoIds)
{
    return removeCollectionItems(collectionId, photoIds, PhotoCollectionMap::PhotoMapping);
}

int SqlitePhotoCollectionMappingDAO::removeCollectionItems(long collectionId, const std::set<long> &itemsIds
        , PhotoCollectionMap::MappingType type)
{
    return removeItems(itemsIds, type, &collectionId);
}

int SqlitePhotoCollectionMappingDAO::removeItems(const std::set<long> &itemsIds, PhotoCollectionMap::MappingType type, const long * collectionId)
{
    std::string IdInPhotoIds = Table::WhereIn(PhotoCollectionItemsTable::Columns::ITEMID
                                                , itemsIds.begin(), itemsIds.end());
    string equalsType    = Table::WhereEquals(PhotoCollectionItemsTable::Columns::ITEMTYPE , type);

    string equalsItems = Table::AND(equalsType, IdInPhotoIds);

    std::string whereCond;
    if(collectionId != NULL){
        whereCond = Table::AND(equalsItems
                 , Table::WhereEquals(PhotoCollectionItemsTable::Columns::COLLECTIONID, *collectionId));
    }
    else{
        whereCond = equalsItems;
    }

    return photoCollectionMapDAO.remove(whereCond);
}

int SqlitePhotoCollectionMappingDAO::removeCollection(long collectionId)
{
    set<long> idsSet;
    idsSet.insert(collectionId);

    return removeCollections(idsSet);
}

int SqlitePhotoCollectionMappingDAO::removeCollections(const std::set<long> &collectionIds)
{
    if(collectionIds.empty()){
        return 0;
    }

    //Remove mappings where collectionId is the containing collection
    string collectionIn = Table::WhereIn(PhotoCollectionItemsTable::Columns::COLLECTIONID
                                           , collectionIds.begin(), collectionIds.end());

    //Remove mapping where collectionId is a subCollection
    string isSubCollection = Table::WhereEquals(PhotoCollectionItemsTable::Columns::ITEMTYPE , PhotoCollectionMap::SubCollectionMapping);
    string itensIn = Table::WhereIn(PhotoCollectionItemsTable::Columns::ITEMID
                                                , collectionIds.begin()
                                                , collectionIds.end());
    string collectionIdIsSubCollection = Table::AND(isSubCollection, itensIn);

    string whereCond = Table::OR(collectionIn, collectionIdIsSubCollection);

    return photoCollectionMapDAO.remove(whereCond);
}

std::set<long> SqlitePhotoCollectionMappingDAO::getCollectionPhotos(long collectionId)
{
    return getCollectionItems(collectionId, PhotoCollectionMap::PhotoMapping);
}

std::set<long> SqlitePhotoCollectionMappingDAO::getSubCollections(long collectionId)
{
    return getCollectionItems(collectionId, PhotoCollectionMap::SubCollectionMapping);
}

std::set<long> SqlitePhotoCollectionMappingDAO::getCollectionItems(long collectionId, PhotoCollectionMap::MappingType type)
{
    set<long> photoIds;
    string equalsCollection = Table::WhereEquals(PhotoCollectionItemsTable::Columns::COLLECTIONID, collectionId);
    string equalsType    = Table::WhereEquals(PhotoCollectionItemsTable::Columns::ITEMTYPE, type);
    string whereCond     = Table::AND(equalsCollection, equalsType);
    vector<PhotoCollectionMap> collectionMaps = photoCollectionMapDAO.listAll(whereCond);

    for(size_t i=0; i < collectionMaps.size(); ++i){
        photoIds.insert(collectionMaps[i].getItemId());
    }

    return photoIds;
}

int SqlitePhotoCollectionMappingDAO::updateCollectionItems(long collectionId,
        const std::set<long> &photoIds, const std::set<long> & subcollectionIds)
{
    ScopedTransaction scopedTransaction(this->photoCollectionMapDAO.getDatabase());

    int collectionItemsCount = updateCollectionItems(collectionId, photoIds, PhotoCollectionMap::PhotoMapping);
    collectionItemsCount += updateCollectionItems(collectionId, subcollectionIds, PhotoCollectionMap::SubCollectionMapping);

    scopedTransaction.commit();
    return collectionItemsCount;
}

int SqlitePhotoCollectionMappingDAO::updateCollectionItems(long collectionId, const std::set<long> &itensIds, PhotoCollectionMap::MappingType type)
{
    ScopedTransaction scopedTransaction(this->photoCollectionMapDAO.getDatabase());

    //Apaga todos os registros atuais e insere os novos
    string equalsCollection = Table::WhereEquals(PhotoCollectionItemsTable::Columns::COLLECTIONID, collectionId);
    string equalsType    = Table::WhereEquals(PhotoCollectionItemsTable::Columns::ITEMTYPE, type);
    string whereCond     = Table::AND(equalsCollection, equalsType);

    photoCollectionMapDAO.remove(whereCond);
    int collectionItemsCount = 0;
    if(!itensIds.empty()){
        collectionItemsCount = this->insertCollectionItemsImpl(collectionId, itensIds, false, type);
    }
    scopedTransaction.commit();
    return collectionItemsCount;
}



std::vector<PhotoCollectionMap> SqlitePhotoCollectionMappingDAO
    ::mapFromIds(const std::set<long> &photoIds, long collectionId, PhotoCollectionMap::MappingType mappingType)
{
    std::vector<PhotoCollectionMap> collectionMaps;
    collectionMaps.reserve(photoIds.size());

    std::set<long>::const_iterator iter = photoIds.begin();
    while(iter != photoIds.end()){
        collectionMaps.push_back(PhotoCollectionMap(collectionId, *iter, mappingType));
        ++iter;
    }

    return collectionMaps;
}

}//namespace



