#ifndef SQLITEPHOTOCOLLECTIONMAPPINGDAO_H
#define SQLITEPHOTOCOLLECTIONMAPPINGDAO_H

#include "SqliteDAO.hpp"
#include "GenericDAO.hpp"
#include "data/data-sqlite/Table.h"
#include "data/data-sqlite/parsers/PhotoCollectionMapDbParser.h"
#include <set>

namespace calmframe {

namespace data {
    class SqliteConnector;
}//namespace

class SqlitePhotoCollectionMappingDAO : public data::SqliteDAO
{
private:
    data::Table photoCollectionMappingTable;
    GenericDAO<PhotoCollectionMap, PhotoCollectionMapDbParser> photoCollectionMapDAO;
public:
    SqlitePhotoCollectionMappingDAO(data::SqliteConnector * database);
    virtual bool createTableIfNotExist(data::SqliteConnector & database);

    std::vector<PhotoCollectionMap> listAll();
    void listPhotoItemsInto(std::set<long> & outPhotoItems);

    PhotoCollectionMap insert(long collectionId, long photoId);
    int insertCollectionPhotos(long collectionId, const std::set<long> & photoIds);
    int insertSubCollections(long collectionId, const std::set<long> & subCollectionIds);
    int addSubCollections(long collectionId, const std::set<long> & subCollectionIds);
    /** Adiciona a coleção com id {@code collectionId} as fotos com id em {@code photoIds} que ele já não contém.
    */
    int addCollectionPhotos(long collectionId, const std::set<long> & photoIds);
    void addPhotoToCollection(long photoId, long collectionId);
    int addItemsToCollections(const std::set<long> & targetCollections
                            , const std::set<long> & photoIds, const std::set<long> & subCollections);
    int removeItemsFromCollections(const std::set<long> & targetCollections
                                  , const std::set<long> & photoIds, const std::set<long> & subCollections);

    int removePhotoFromCollections(long photoId);
    int removeCollectionPhotos(long collectionId, const std::set<long> & photoIds);
    int removeCollectionItems(long collectionId, const std::set<long> &itemsIds, PhotoCollectionMap::MappingType type);

    /** Remove all mappings with this collectionId*/
    int removeCollection(long collectionId);
    /** Remove all mappings with some of these collectionIds*/
    int removeCollections(const std::set<long> & collectionIds);

    std::set<long> getCollectionPhotos(long collectionId);
    std::set<long> getSubCollections(long collectionId);
    int updateCollectionItems(long collectionId, const std::set<long> & photoIds, const std::set<long> &subcollectionIds);
    int updateCollectionItems(long collectionId, const std::set<long> & itensIds, PhotoCollectionMap::MappingType type = PhotoCollectionMap::PhotoMapping);
    std::set<long> getCollectionItems(long collectionId, PhotoCollectionMap::MappingType type = PhotoCollectionMap::PhotoMapping);
    int removeItems(const std::set<long> &itemsIds, PhotoCollectionMap::MappingType type, const long * collectionId=NULL);
protected:
    std::vector<PhotoCollectionMap> mapFromIds(const std::set<long> &photoIds, long collectionId, PhotoCollectionMap::MappingType mappingType);
    int insertCollectionItemsImpl(long collectionId, const std::set<long> &itemIds, bool ignoreConflict=false
                                  , PhotoCollectionMap::MappingType mappingType = PhotoCollectionMap::PhotoMapping);
};

}//namespace

#endif // SQLITEPHOTOCOLLECTIONMAPPINGDAO_H
