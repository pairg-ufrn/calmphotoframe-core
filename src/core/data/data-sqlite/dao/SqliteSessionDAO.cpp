#include "SqliteSessionDAO.h"
#include "model/user/Session.h"

#include "GenericDAO.hpp"
#include "data/data-sqlite/SqliteConnector.h"
#include "data/data-sqlite/ForeignKey.h"
#include "data/data-sqlite/tables/SessionTable.h"
#include "data/data-sqlite/tables/UserTable.h"
#include "data/data-sqlite/parsers/SessionDbParser.h"

#include "data/data-sqlite/SqlException.hpp"

#include "utils/TimeUtils.h"
#include "utils/Log.h"

namespace calmframe {

using namespace std;
using namespace data;
using namespace Tables;

SqliteSessionDAO::SqliteSessionDAO(SqliteConnector * _database)
    : database(_database)
    , genericDAO(&sessionTable, _database)
{
    SessionTable::buildTable(sessionTable);
}

SqliteSessionDAO::~SqliteSessionDAO()
{}

bool SqliteSessionDAO::createTableIfNotExist(data::SqliteConnector &database){
    string createStatement = sessionTable.getCreateStatement();
    return database.execute(createStatement);
}

bool SqliteSessionDAO::getUserSession(long userId, Session &sessionOut) const{
    return get(sessionOut, Table::WhereEquals(SessionTable::Columns::USER_ID, userId));
}

bool SqliteSessionDAO::get(long sessionId, Session &sessionOut) const{
    return get(sessionOut, Table::WhereEquals(SessionTable::Columns::ID, sessionId));
}

bool SqliteSessionDAO::getByToken(const string & token, Session &sessionOut) const{
    return get(sessionOut, Table::WhereEquals(SessionTable::Columns::TOKEN, token, true));
}

bool SqliteSessionDAO::get(Session &sessionOut, const string &whereCondition) const{
    return genericDAO.get(sessionOut, whereCondition);
}

bool SqliteSessionDAO::exist(long sessionId) const{
    return genericDAO.exist(SessionTable::Columns::ID
                                  , Table::WhereEquals(SessionTable::Columns::ID, sessionId));
}

long SqliteSessionDAO::insert(const Session &session){
    return genericDAO.insert(session, getInsertColumns());
}

bool SqliteSessionDAO::remove(const Session &session){
    return genericDAO.remove(Table::WhereEquals(SessionTable::Columns::ID, session.getId())) == 1;
}

int SqliteSessionDAO::removeExpired(){
    long now = TimeUtils::instance().getTimestamp();
    const std::string & expireColumn = SessionTable::Columns::EXPIRE_TIMESTAMP;
    string whereTimeExpired = Table::WhereCompare(expireColumn, SqlComparator::LESS_THAN, now);
    string whereNotUnlimited = Table::WhereCompare(expireColumn, SqlComparator::NOT_EQUALS, Session::unlimitedExpiration);

    return genericDAO.remove(Table::AND(whereTimeExpired, whereNotUnlimited));
}

std::vector<Session> SqliteSessionDAO::listAll(){
    return genericDAO.listAll();
}

int SqliteSessionDAO::update(const Session &session){
    string whereEquals = Table::WhereEquals(SessionTable::Columns::ID, session.getId());
    int updatedRows =  genericDAO.update(session, whereEquals, getUpdateColumns());

    return updatedRows;
}


const std::vector<string> &SqliteSessionDAO::getInsertColumns() const{
    static vector<string> columns;
    //FIXME: this is not thread-safe
    if(columns.empty()){//Se primeira vez que executou, preenche
        columns.push_back(SessionTable::Columns::TOKEN);
        columns.push_back(SessionTable::Columns::USER_ID);
        columns.push_back(SessionTable::Columns::EXPIRE_TIMESTAMP);
    }
    return columns;
}

const std::vector<string> &SqliteSessionDAO::getUpdateColumns() const{
    return getInsertColumns();
}
}
