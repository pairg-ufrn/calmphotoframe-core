/*
 * SqlitePhotoMarkerDAO.h
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#ifndef SQLITEPHOTOMARKERDAO_H_
#define SQLITEPHOTOMARKERDAO_H_

#include "SqliteDAO.hpp"
#include "GenericDAO.hpp"
#include "../parsers/PhotoMarkerParser.h"
#include "../Table.h"
#include "../../SetId.h"

#include "model/photo/Photo.h"

#include <vector>

namespace calmframe {
namespace data {

class SqlitePhotoMarkerDAO : public SqliteDAO{
public:
    typedef std::vector<PhotoMarker> MarkersList;

private:
    SqliteConnector * connector;
    Table photoMarkerTable;
    GenericDAO<PhotoMarker, PhotoMarkerParser> dao;
public:
    SqlitePhotoMarkerDAO(SqliteConnector * connector);
	virtual ~SqlitePhotoMarkerDAO();
    virtual bool createTableIfNotExist(SqliteConnector & connector);

    std::vector<PhotoMarker> listAll(const Photo * photo = NULL);
    std::vector<PhotoMarker> listAll(const Photo & photo);
    std::vector<PhotoMarker> listAll(const long & photoId);
    SetId queryPhotos(const std::string & queryStr);
	long insert(long photoId, const PhotoMarker & photoMarker);
    void insert(long photoId, const std::vector<PhotoMarker> & photoMarker);
    void update(const PhotoMarker & photoMarker);
	bool removeMarkersFromPhoto(long photoId);
	bool remove(const PhotoMarker & photoMarker);
};

} /* namespace data */
} /* namespace calmframe */

#endif /* SQLITEPHOTOMARKERDAO_H_ */
