/*
 * SqliteDAO.h
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#ifndef SQLITEDAO_H_
#define SQLITEDAO_H_

namespace calmframe {
namespace data {

class SqliteConnector;

class SqliteDAO {
public:
	virtual ~SqliteDAO()
	{}
	virtual bool createTableIfNotExist(SqliteConnector & database)=0;
};

} /* namespace data */
} /* namespace calmframe */

#endif /* SQLITEDAO_H_ */
