#ifndef SQLITEPHOTOMETADATADAO_H
#define SQLITEPHOTOMETADATADAO_H

#include "SqliteDAO.hpp"
#include "data/data-sqlite/DAOForward.h"
#include "data/data-sqlite/Table.h"
#include "model/photo/Photo.h"
#include "model/photo/PhotoMetadata.h"

#include <vector>
#include <map>

namespace calmframe{

namespace data{
    class SqliteConnector;

class SqlitePhotoMetadataDAO : public SqliteDAO
{
private:
    Table photoMetadataTable;
    SqliteConnector * database;
public:
    typedef std::map<long, PhotoMetadata> MetadataMap;
public:
    SqlitePhotoMetadataDAO(SqliteConnector * database);
    virtual ~SqlitePhotoMetadataDAO(){}

    virtual bool createTableIfNotExist(SqliteConnector & database);
    virtual PhotoMetadata get(long photoId);

    virtual bool exist(long photoId);
    virtual void insert(long photoId, const PhotoMetadata & metadata);
    virtual bool update(long photoId, const PhotoMetadata & metadata);
    virtual bool remove(long photoId);
    virtual void listAll(std::vector<Photo> & photos);
    virtual void listAll(std::vector<PhotoMetadata> & metadataListOut);
    virtual std::vector<PhotoMetadata> listAll();

    virtual void listAllById(MetadataMap &metadataMap);
};

}
}

#endif // SQLITEPHOTOMETADATADAO_H
