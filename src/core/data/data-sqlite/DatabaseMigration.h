#ifndef DATABASEMIGRATION_H
#define DATABASEMIGRATION_H

#include "utils/Exception.hpp"

#include "Table.h"

#include <vector>
#include <functional>

namespace calmframe {
namespace data {
    class SqliteConnector;
}//namespace
}//namespace

namespace calmframe {

EXCEPTION_CLASS(InvalidMigrationVersion);

class Migration;

class DatabaseMigration{
public:
    static const int MAX_VERSION; //max supported version for migration

    using EntityValues = std::map<std::string, std::string>;
public:
    DatabaseMigration();

    void migrate(data::SqliteConnector & connector, unsigned targetVersion);

    int queryDatabaseVersion(const data::SqliteConnector & connector) const;
    void updateDbVersion(data::SqliteConnector& connector, int targetVersion);

    bool updateEntity(
        data::SqliteConnector& connector,
        const std::string & table, 
        const std::string & whereCondition,
        const EntityValues & values);
        
    void verifyForeignKeys(data::SqliteConnector& connector);
    void verifyIntegrity(data::SqliteConnector& connector);
protected:
    void addColumn(data::SqliteConnector& connector, const std::string & table, const Column & col);
    
    void executeMigrations(data::SqliteConnector& connector, int targetVersion);

    void setForeignKeysEnabled(data::SqliteConnector& connector, bool enabled);

    std::vector<std::function<void (DatabaseMigration * , data::SqliteConnector &)>> migrations;


    static void upgrade_toV2(DatabaseMigration * migration, data::SqliteConnector& connector);
    static void upgrade_toV3(DatabaseMigration * migration, data::SqliteConnector& connector);
    static void upgrade_toV4(DatabaseMigration * migration, data::SqliteConnector& connector);
    static void upgrade_toV5(DatabaseMigration * migration, data::SqliteConnector& connector);
    static void upgrade_toV6(DatabaseMigration * migration, data::SqliteConnector& connector);
    static void upgrade_toV7(DatabaseMigration * migration, data::SqliteConnector& connector);
    static void upgrade_toV8(DatabaseMigration * migration, data::SqliteConnector& connector);

    static std::string makeStmt_MigratePhotosFromV3ToV4();
};

}//namespace

#endif // DATABASEMIGRATION_H
