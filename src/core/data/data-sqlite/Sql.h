#ifndef SQL_H
#define SQL_H

#include <string>

namespace calmframe {

namespace Sql{
    std::string PRAGMAFunction(const std::string & name, const std::string & parameter);
    std::string PRAGMA(const std::string & name, const std::string & value="");

    std::string AND(const std::string & cond1, const std::string & cond2);
    std::string OR(const std::string & cond1, const std::string & cond2);
namespace Pragmas{
    typedef const char * const ConstCstr;

    static constexpr ConstCstr tableInfo        = "table_info";
    static constexpr ConstCstr integrityCheck   = "integrity_check";
    static constexpr ConstCstr schemaVersion    = "schema_version";
    static constexpr ConstCstr writableSchema   = "writable_schema";
    static constexpr ConstCstr foreignKeyList   = "foreign_key_list";

}//namespace
}//namespace
}//namespace
#endif // SQL_H
