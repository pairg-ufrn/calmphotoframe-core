#include "DatabaseMigration.h"

#include "Sql.h"
#include "SqliteConnector.h"
#include "SchemaManager.h"
#include "ScopedTransaction.h"
#include "SqliteData.h"
#include "readers/GenericDbReader.h"
#include "readers/DbVersionReader.h"
#include "readers/CountReader.h"
#include "readers/GenericDbReader.h"
#include "readers/EntityReader.h"
#include "parsers/ColumnsParser.h"
#include "migration/ViolationParser.h"

#include "exceptions/DatabaseException.h"
#include "utils/StringCast.h"

#include <sstream>
#include <iostream>

namespace calmframe {

const int DatabaseMigration::MAX_VERSION = 8;



DatabaseMigration::DatabaseMigration()
    : migrations{
        upgrade_toV2, 
        upgrade_toV3, 
        upgrade_toV4, 
        upgrade_toV5, 
        upgrade_toV6, 
        upgrade_toV7, 
        upgrade_toV8}
{}

int DatabaseMigration::queryDatabaseVersion(const data::SqliteConnector & connector) const{

    DbVersionReader versionReader;
    connector.query(versionReader, "PRAGMA user_version;");

    return versionReader.getVersion();
}

void DatabaseMigration::updateDbVersion(data::SqliteConnector& connector, int targetVersion){
    connector.execute("PRAGMA user_version=" + utils::StrCast().toString(targetVersion));
}

bool DatabaseMigration::updateEntity(data::SqliteConnector& connector, const std::string & table, 
    const std::string & whereCondition, 
    const EntityValues & valuesEntity)
{
    std::vector<std::string> columns;
    std::vector<std::string> values;
    
    for(auto & keyValue : valuesEntity){
        columns.push_back(keyValue.first);
        values.push_back(keyValue.second);
    }
    
    auto stmt = Table::getUpdateStatement(table, columns, whereCondition);
    
    std::string placeholder = "?";
    
    size_t startPos = 0;
    for (const auto & v : values) {
        startPos = stmt.find(placeholder, startPos);
        if(startPos == std::string::npos){
            break;
        }
        
        stmt.replace(startPos, placeholder.size(), v);
    }
    
    return connector.execute(stmt);
}

void DatabaseMigration::executeMigrations(data::SqliteConnector& connector, int targetVersion)
{
    int currentVersion = queryDatabaseVersion(connector);
    if(currentVersion < 1){
        throw InvalidMigrationVersion("Current version is smaller than minimum.");
    }

    if(currentVersion < targetVersion && targetVersion >= 2){
        int initIndex = currentVersion - 1;
        int finalIndex = targetVersion - 2;

        for(int migrationIndex = initIndex; migrationIndex <= finalIndex; ++migrationIndex){
            migrations[migrationIndex](this, connector);
        }
        updateDbVersion(connector, targetVersion);
    }
}

void DatabaseMigration::verifyForeignKeys(data::SqliteConnector& connector)
{
    Query verifyForeignKeys(connector, "PRAGMA foreign_key_check;");
    
    std::vector<ForeignKeyViolation> violations;
    GenericDbReader<ForeignKeyViolation, ViolationParser> violationsReader{&violations};
    verifyForeignKeys.read(violationsReader);

    if(violations.size() > 0){
        std::set<std::string> tables;
        for(const ForeignKeyViolation & v : violations){
            tables.insert(v.getViolatedTable());
        }
        
        std::stringstream errMessage;
        
        errMessage << "Failed to migrate database, found " 
            << violations.size() << " foreign key violations ";

        errMessage << "at tables: ";
        bool first = true;
        for(const auto & table : tables){
            if(!first){
                errMessage << ", ";
            }
            errMessage << table;
            first = false;
        }            

//        for (const ForeignKeyViolation & v : violations) {
//            errMessage << "\n\t" "at " << v.getViolatedTable()
//                        << " to " << v.getReferencedTable() 
//                        << ": ";
            
//            std::string stmt = std::string("SELECT * FROM ") + v.getViolatedTable()
//                            + " Where ROWID=" + std::to_string(v.getRowId());            
//            Query query{connector, stmt};
//            EntityReader reader;
//            query.read(reader);
            
//            for(const auto & e : reader.entities){
//                errMessage << "{";
                
//                for(const auto & p : e){
//                    errMessage << p.first << ": " << p.second << ", ";
//                }
                
//                errMessage << "}";
//            }            
//        }
            
        throw DatabaseException(errMessage.str());
    }
}

void DatabaseMigration::verifyIntegrity(SqliteConnector& connector)
{
    Query integrityCheckStmt(connector, Sql::PRAGMA(Sql::Pragmas::integrityCheck));
    integrityCheckStmt.read();
    if(integrityCheckStmt.getString(0) != "ok"){
        //TODO: log errors returned by query
        throw DatabaseException("Failed to migrate database due to integrity violations.");
    }
}

void DatabaseMigration::setForeignKeysEnabled(data::SqliteConnector& connector, bool enabled)
{
    std::string stmt("PRAGMA foreign_keys=");
    stmt.append(enabled ? "ON" : "OFF");
    stmt.append(";");

    connector.execute(stmt);
}

void DatabaseMigration::migrate(data::SqliteConnector & connector, unsigned targetVersion){
    if(targetVersion <= 0 || targetVersion > MAX_VERSION){
        throw InvalidMigrationVersion();
    }
    if(connector.isOnTransaction()){
        throw DatabaseException("Cannot migrate database while on transaction");
    }

    //Disable foreign keys to allow changes on schema (cannot be made inside transaction)
    setForeignKeysEnabled(connector, false);

    data::ScopedTransaction transaction(&connector);

    executeMigrations(connector, targetVersion);
    verifyForeignKeys(connector);
    verifyIntegrity(connector);

    transaction.commit();

    setForeignKeysEnabled(connector, true);
}

void DatabaseMigration::addColumn(SqliteConnector& connector, const std::string & table, const Column & col)
{
    //TODO: (?) support constraints?
    auto addColumnStmt = 
        std::string("ALTER TABLE ") + table + 
        " ADD COLUMN " + col.getName() + " " + col.getType() + ";";
        
    connector.execute(addColumnStmt);    
}

void DatabaseMigration::upgrade_toV2(DatabaseMigration * , data::SqliteConnector& connector){
    const char * const createEventsTable =
            "CREATE TABLE Events("
                "createdAt TIMESTAMP"
                ", entityId INTEGER"
                ", id INTEGER PRIMARY KEY "
                ", type INTEGER"
                ", userId INTEGER"
                ", FOREIGN KEY (userId) REFERENCES User(id) ON UPDATE CASCADE ON DELETE SET NULL);";

    connector.execute(createEventsTable);
}

void DatabaseMigration::upgrade_toV3(DatabaseMigration * migration, SqliteConnector & connector){
    int defaultVisibility = static_cast<int>(Visibility::EditableByOthers);
    
    migration->addColumn(connector, "PhotoContext", Column("visibility", ColumnTypes::INTEGER));
    
    const std::string updateContextVisibility =
            "Update PhotoContext SET visibility=" + utils::StrCast().toString(defaultVisibility) +" ;";
    const char * const createPermissionsTable =
            "CREATE TABLE Permissions("
                "contextId INTEGER"
                ", permissionLevel INTEGER"
                ", userId INTEGER"
                ", FOREIGN KEY (contextId) REFERENCES PhotoContext(id) ON UPDATE CASCADE ON DELETE CASCADE"
                ", FOREIGN KEY (userId) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE"
                ", PRIMARY KEY (contextId, userId));";

    connector.execute(updateContextVisibility);
    connector.execute(createPermissionsTable);
}

void DatabaseMigration::upgrade_toV4(DatabaseMigration *, SqliteConnector & connector){
    constexpr const char * const createNewTable = //need recreate table, because Sqlite does not support add foreign key
            "CREATE TABLE NewPhoto("
                "contextId INTEGER"
                ", createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
                ", credits TEXT"
                ", date TEXT"
                ", description TEXT"
                ", id INTEGER PRIMARY KEY "
                ", imagePath TEXT"
                ", moment TEXT"
                ", place TEXT"
                ", updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
                ", FOREIGN KEY (contextId) REFERENCES PhotoContext(id));";

    const std::string migratePhotoDataStmt = makeStmt_MigratePhotosFromV3ToV4();

    constexpr const char * const dropOldTable = "DROP Table Photo;";

    constexpr const char * const renameNewPhotoTable =
            "ALTER TABLE NewPhoto RENAME TO Photo;";

    connector.execute(createNewTable);
    connector.execute(migratePhotoDataStmt);
    connector.execute(dropOldTable);
    connector.execute(renameNewPhotoTable);
}

void DatabaseMigration::upgrade_toV5(DatabaseMigration *, SqliteConnector & connector){
    SchemaManager schemaManager(&connector);

    Table table = schemaManager.readTable("PhotoMarker");
    Column & createdAt = table.getColumn("createdAt");
    Column & updatedAt = table.getColumn("updatedAt");

    createdAt.addConstraint(ColumnConstraints::DEFAULT(Tables::CommonTable::DEFAULT_TIMESTAMP));
    updatedAt.addConstraint(ColumnConstraints::DEFAULT(Tables::CommonTable::DEFAULT_TIMESTAMP));

    schemaManager.updateColumns(table.getName(), {
        {"createdAt", createdAt},
        {"updatedAt", updatedAt}
                                });
}

void DatabaseMigration::upgrade_toV6(DatabaseMigration * migration, SqliteConnector & connector){
    migration->addColumn(connector, "User", Column("name", ColumnTypes::TEXT));
    migration->addColumn(connector, "User", Column("profileImage", ColumnTypes::TEXT));
}

void DatabaseMigration::upgrade_toV7(DatabaseMigration *, SqliteConnector & connector){
    Table configurationsTable{"Configurations"};
    configurationsTable.putColumn(Column("key", ColumnTypes::TEXT).addConstraint(ColumnConstraints::UNIQUE()));
    configurationsTable.putColumn(Column("value", ColumnTypes::TEXT));
    
    connector.execute(configurationsTable.getCreateStatement());
}

void DatabaseMigration::upgrade_toV8(DatabaseMigration * migration, SqliteConnector & connector){
    SchemaManager schemaManager{&connector};
    
    auto updatedUserFK = ForeignKey("userId", "User", "id")
                            .setOnUpdateAction(ForeignKeyConstants::Actions::CASCADE)
                            .setOnDeleteAction(ForeignKeyConstants::Actions::CASCADE);
    
    schemaManager.updateForeignKeys("Session", {{"userId", updatedUserFK}});
    
    std::string condition = "userId=0", newId = "-1";    
    migration->updateEntity(connector, "User"       , "id=0"   , {{"id", newId}});
    migration->updateEntity(connector, "Session"    , condition, {{"userId", newId}});
    migration->updateEntity(connector, "Events"     , condition, {{"userId", newId}});
    migration->updateEntity(connector, "Permissions", condition, {{"userId", newId}});
}

std::string DatabaseMigration::makeStmt_MigratePhotosFromV3ToV4()
{
    //Joins Collections items and PhotoCollection to get contextId
    constexpr const char * const collectionItemsWithContextId =
                "select item_id , contextId FROM PhotoCollectionItems "
                "JOIN PhotoCollection ON item_type=1 AND collection_id = PhotoCollection.id";

    //Join Photos With previous query to add context into Photos
    constexpr const char * const join_PhotoTemp_And_PhotosItemsWithContext =
                "select * from Photo JOIN PhotoItems_WithContext ON Photo.id = item_id";

    constexpr const char * photoColumns =
                "contextId, createdAt, credits, date, description, id, imagePath, moment, place, updatedAt";

    std::stringstream migratePhotoData;
    migratePhotoData
            << "WITH PhotoItems_WithContext AS (" << collectionItemsWithContextId << ")"
            << ", Photos_WithContext AS (" << join_PhotoTemp_And_PhotosItemsWithContext << ")"
            << "INSERT OR REPLACE INTO NewPhoto "
                << "SELECT " << photoColumns << " FROM Photos_WithContext"
            << ";";

    return migratePhotoData.str();
}

}//namespace
