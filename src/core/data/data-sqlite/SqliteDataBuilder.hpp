/*
 * SqliteDataBuilder.h
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#include "data/Data.h"
#include "data/DataBuilder.hpp"
#include <string>

#ifndef SQLITEDATABUILDER_H_
#define SQLITEDATABUILDER_H_

namespace calmframe {
namespace data {

class SqliteDataBuilder : public DataBuilder {
private:
    std::string dbDir;
    std::string dbName;
    bool createHierarchy;
public:
    SqliteDataBuilder(const std::string & databaseDirectory
                    , const std::string & dbName = std::string()
                    , bool createHierarchy=false);

    virtual Data * buildData();
    virtual DataBuilder * clone() const;
public:
    bool getCreateHierarchy() const;
};

} /* namespace data */
} /* namespace calmframe */

#endif /* SQLITEDATABUILDER_H_ */
