#include "Sql.h"

using namespace std;

namespace calmframe {

std::string Sql::PRAGMAFunction(const std::string & name, const std::string & parameter){
    return string("PRAGMA ").append(name).append("(").append(parameter).append(")");
}

string Sql::PRAGMA(const string & name, const string & value){
    string stmt = "PRAGMA " + name;
    if(!value.empty()){
        stmt.append("=").append(value);
    }

    return stmt;
}

string binaryOp(const std::string & op, const string& cond1, const string& cond2)
{
    string result;
    if(!cond1.empty() && !cond2.empty()){
        result =  "(" + cond1 + " " + op +" " + cond2 + ")";
    }
    else if(!cond1.empty()){
        result =  cond1;
    }
    else{
        result =  cond2;
    }

    return result;
}

string Sql::AND(const string & cond1, const string & cond2){
    return binaryOp("AND", cond1, cond2);
}

string Sql::OR(const string & cond1, const string & cond2){
    return binaryOp("OR", cond1, cond2);
}

}//namespace
