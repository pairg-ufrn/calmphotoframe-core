#include "ForeignKey.h"
#include <sstream>

using namespace std;
using namespace ForeignKeyConstants::Constants;

ForeignKey::ForeignKey()
    : ForeignKey(std::string(), std::string(), std::string())
{}

ForeignKey::ForeignKey(const std::string &localColumn, const std::string &foreignTable, const std::string &foreignKey)
    : localColumn(localColumn)
    , foreignTable(foreignTable)
    , foreignKey(foreignKey)
{}

std::string ForeignKey::toString() const
{
    stringstream sstream;
    sstream << *this;
    return sstream.str();
}

std::ostream & operator<<(std::ostream &outStream, const ForeignKey & foreignKey)
{
    outStream<< FOREIGN_KEY << " "<< COL_DELIM_START << foreignKey.localColumn << COL_DELIM_END
                << " " << REFERENCES << " "
                << foreignKey.foreignTable << COL_DELIM_START << foreignKey.foreignKey << COL_DELIM_END;
    if(!foreignKey.getOnUpdateAction().empty()){
        outStream << ON_UPDATE << foreignKey.getOnUpdateAction();
    }
    if(!foreignKey.getOnDeleteAction().empty()){
        outStream << ON_DELETE << foreignKey.getOnDeleteAction();
    }
    return outStream;
}

const string &ForeignKey::getLocalColumn() const{
    return localColumn;
}
void ForeignKey::setLocalColumn(const std::string &value){
    localColumn = value;
}

const string &ForeignKey::getForeignTable() const{
    return foreignTable;
}
void ForeignKey::setForeignTable(const std::string &value){
    foreignTable = value;
}

const string &ForeignKey::getForeignKey() const{
    return foreignKey;
}
void ForeignKey::setForeignKey(const std::string &value){
    foreignKey = value;
}

const string &ForeignKey::getOnUpdateAction() const{
    return onUpdateAction;
}
ForeignKey & ForeignKey::setOnUpdateAction(const std::string &value){
    onUpdateAction = value;
    return *this;
}

const string &ForeignKey::getOnDeleteAction() const{
    return onDeleteAction;
}
ForeignKey & ForeignKey::setOnDeleteAction(const std::string &value){
    onDeleteAction = value;
    return *this;
}
