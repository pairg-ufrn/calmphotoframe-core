#ifndef PHOTODBPARSER_H
#define PHOTODBPARSER_H

#include "model/photo/Photo.h"
#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/QueryData.h"

namespace calmframe {

class PhotoDbParser{
public:
    void writeStatement(const Photo & photo, data::Statement &statement);
    void readRow(const QueryData & data, Photo * photo);
};

}
#endif // PHOTODBPARSER_H
