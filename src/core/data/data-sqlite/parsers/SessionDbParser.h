#ifndef SESSIONDBPARSER_H
#define SESSIONDBPARSER_H

#include "model/user/Session.h"
#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/QueryData.h"

namespace calmframe {

class SessionDbParser{
public:
    void writeStatement(const Session & session, data::Statement &statement);
    void readRow(const QueryData & data, Session * session);
};

}

#endif // SESSIONDBPARSER_H

