#include "ConfigParser.h"

#include "data/data-sqlite/QueryData.h"
#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/tables/ConfigurationsTable.h"

namespace calmframe {

using namespace Tables;

void ConfigParser::writeStatement(const ConfigValue & value, data::Statement & statement){
    statement.appendTextValue(value.getKey(), ConfigurationsTable::Columns::key);
    statement.appendTextValue(value.getValue(), ConfigurationsTable::Columns::value);
}

void ConfigParser::readRow(const QueryData & data, ConfigValue * outValue){
    if(outValue != nullptr){
        outValue->setKey(data.getString(ConfigurationsTable::Columns::key));
        outValue->setValue(data.getString(ConfigurationsTable::Columns::value));
    }
}

}//namespace
