#ifndef COLUMNSPARSER_H
#define COLUMNSPARSER_H

#include "ParserFwdDef.h"
#include "../Column.h"

namespace calmframe{

class ColumnsParser
{
public:
    void writeStatement(const Column & column, data::Statement &statement);
    void readRow(const QueryData & data, Column * outColumn);
};

}

#endif // COLUMNSPARSER_H
