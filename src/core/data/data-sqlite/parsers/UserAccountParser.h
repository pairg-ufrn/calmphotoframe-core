#ifndef USERACCOUNTPARSER_H
#define USERACCOUNTPARSER_H

#include "model/user/UserAccount.h"
#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/QueryData.h"

namespace calmframe {

class UserAccountParser{
public:
    void writeStatement(const UserAccount & userAccount, data::Statement &statement);
    void readRow(const QueryData & data, UserAccount * userAccount);
};

}

#endif // USERACCOUNTPARSER_H
