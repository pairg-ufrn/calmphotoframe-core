#include "PhotoDbParser.h"

#include "data/data-sqlite/tables/PhotoTable.h"

namespace calmframe {

using namespace std;
using namespace Tables;
using namespace PhotoTable;

void PhotoDbParser::writeStatement(const Photo &photo, data::Statement &statement){
    statement.appendValue(photo.getId(),PhotoTable::Columns::ID);

    if(photo.getContextId() != Photo::UnknownContextId){
        statement.appendValue(photo.getContextId(), PhotoTable::Columns::ContextId);
    }
    else{
        statement.appendNullValue(PhotoTable::Columns::ContextId);
    }

    statement.appendTextValue(photo.getImagePath(), PhotoTable::Columns::IMAGEPATH)
            .appendTextValue(photo.getDescription().getDescription(), PhotoTable::Columns::DESCRIPTION)
            .appendTextValue(photo.getDescription().getMoment(), PhotoTable::Columns::MOMENT)
            .appendTextValue(photo.getDescription().getDate(), PhotoTable::Columns::DATE)
            .appendTextValue(photo.getDescription().getPlace(), PhotoTable::Columns::PLACE)
            .appendTextValue(photo.getDescription().getCredits(), PhotoTable::Columns::CREDITS);
}

void PhotoDbParser::readRow(const QueryData &data, Photo *photo)
{
    PhotoDescription & description = photo->getDescription();
    description.setDate(data.getString(Columns::DATE));
    description.setDescription(data.getString(Columns::DESCRIPTION));
    description.setMoment(data.getString(Columns::MOMENT));
    description.setPlace(data.getString(Columns::PLACE));
    description.setCredits(data.getString(Columns::CREDITS));

    photo->setId(data.getLong(Columns::ID));
    photo->setContextId(data.getLong(Columns::ContextId, Photo::UnknownContextId));
    photo->setImagePath(data.getString(Columns::IMAGEPATH));
}

}
