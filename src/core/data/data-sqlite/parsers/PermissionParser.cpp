#include "PermissionParser.h"

#include "../QueryData.h"
#include "../Statement.h"
#include "../tables/PermissionsTable.h"

using namespace Tables::PermissionsTable;

namespace calmframe {

void PermissionParser::writeStatement(const calmframe::Permission & permission, calmframe::data::Statement & statement){
    statement.appendValue(permission.getContextId(), Columns::contextId);
    statement.appendValue(permission.getUserId()   , Columns::userId);
    statement.appendValue((int)permission.getPermissionLevel(), Columns::permissionLevel);
}

void PermissionParser::readRow(const QueryData & data, Permission * outPermission){
    outPermission->setContextId(data.getLong(Columns::contextId, Permission::InvalidId));
    outPermission->setUserId(data.getLong(Columns::userId, Permission::InvalidId));
    outPermission->setPermissionLevel((PermissionLevel)data.getInt(Columns::permissionLevel, (int)PermissionLevel::None));
}

}//namespace
