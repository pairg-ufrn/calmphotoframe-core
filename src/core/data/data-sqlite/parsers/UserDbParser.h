#ifndef USERDBPARSER_H
#define USERDBPARSER_H

#include "model/user/UserAccount.h"
#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/QueryData.h"

namespace calmframe {

class UserDbParser{
public:
    void writeStatement(const User & user, data::Statement &statement);
    void readRow(const QueryData & data, User * userAccount);
};

}//namespace

#endif // USERDBPARSER_H
