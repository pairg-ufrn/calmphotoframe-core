#include "PhotoCollectionMapDbParser.h"

#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/QueryData.h"
#include "data/data-sqlite/tables/PhotoCollectionTable.h"

namespace calmframe {

using namespace Tables;

void PhotoCollectionMapDbParser::writeStatement(const PhotoCollectionMap &photoCollectionMap, data::Statement &statement)
{
    statement.appendValue(photoCollectionMap.getCollectionId(), PhotoCollectionItemsTable::Columns::COLLECTIONID);
    statement.appendValue(photoCollectionMap.getItemId()  , PhotoCollectionItemsTable::Columns::ITEMID);
    statement.appendValue(photoCollectionMap.getType()     , PhotoCollectionItemsTable::Columns::ITEMTYPE);
    statement.appendTextValue("CURRENT_TIMESTAMP"           , PhotoCollectionItemsTable::Columns::UPDATEDAT);
}

void PhotoCollectionMapDbParser::readRow(const QueryData &data, PhotoCollectionMap *photoCollectionMap)
{
    photoCollectionMap->setCollectionId(data.getLong(PhotoCollectionItemsTable::Columns::COLLECTIONID));
    photoCollectionMap->setItemId(data.getLong(PhotoCollectionItemsTable::Columns::ITEMID));
    photoCollectionMap->setType((PhotoCollectionMap::MappingType)data.getInt(PhotoCollectionItemsTable::Columns::ITEMTYPE));
}


PhotoCollectionMap::MappingType PhotoCollectionMap::getType() const
{
    return type;
}

void PhotoCollectionMap::setType(const MappingType &value)
{
    type = value;
}
PhotoCollectionMap::PhotoCollectionMap(long collectionId, long photoId, MappingType type)
    : collectionId(collectionId), itemId(photoId), type(type)
{}

long PhotoCollectionMap::getCollectionId() const{
    return collectionId;
}
void PhotoCollectionMap::setCollectionId(long value){
    collectionId = value;
}
long PhotoCollectionMap::getItemId() const{
    return itemId;
}
void PhotoCollectionMap::setItemId(long value)
{
    itemId = value;
}

}//namespace
