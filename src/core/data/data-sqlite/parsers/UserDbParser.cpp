#include "UserDbParser.h"
#include "data/data-sqlite/tables/UserTable.h"

#include "utils/CommonExceptions.h"

namespace calmframe {

using namespace Tables;

void UserDbParser::writeStatement(const User & user, data::Statement & statement)
{
    statement.appendTextValue(user.getLogin(), UserTable::Columns::LOGIN)
             .appendTextValue(user.name()    , UserTable::Columns::NAME)
             .appendTextValue(user.profileImage(), UserTable::Columns::PROFILE_IMAGE);
             
    statement.appendValue(user.isActive()? 1 : 0, UserTable::Columns::ACTIVE)
             .appendValue(user.isAdmin() ? 1 : 0, UserTable::Columns::ISADMIN)
             .appendValue(user.getId(), UserTable::Columns::ID);
}

void UserDbParser::readRow(const QueryData & data, User * user){
    user->setId(data.getLong(UserTable::Columns::ID, UserAccount::INVALID_ID));
    user->setLogin(data.getString(UserTable::Columns::LOGIN));
    user->name(data.getString(UserTable::Columns::NAME));
    user->setActive(data.getInt(UserTable::Columns::ACTIVE) == 1);
    user->setIsAdmin(data.getInt(UserTable::Columns::ISADMIN) == 1);
    user->profileImage(data.getString(UserTable::Columns::PROFILE_IMAGE));
}


}//namespace


