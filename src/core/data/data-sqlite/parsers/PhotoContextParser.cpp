#include "PhotoContextParser.h"

#include "model/photo/PhotoContext.h"

#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/QueryData.h"
#include "data/data-sqlite/tables/PhotoContextTable.h"

namespace calmframe {

using namespace Tables::PhotoContextTable;

void PhotoContextParser::writeStatement(const PhotoContext &photoContext, data::Statement &statement)
{
    statement.appendValue(photoContext.getId()  , Columns::ID);
    statement.appendTextValue(photoContext.getName(), Columns::NAME);
    statement.appendValue(static_cast<int>(photoContext.getVisibility()), Columns::VISIBILITY);
    statement.appendTextValue("CURRENT_TIMESTAMP", Columns::UPDATEDAT);

    if(photoContext.getRootCollection() > PhotoContext::INVALID_COLLECTION_ID){
        statement.appendValue(photoContext.getRootCollection(), Columns::ROOT_PHOTOCOLLECTION);
    }
    else{
        statement.appendNullValue(Columns::ROOT_PHOTOCOLLECTION);
    }
}

void PhotoContextParser::readRow(const QueryData &data, PhotoContext *photoContext)
{
    if(photoContext != NULL){
        photoContext->setId(data.getLong(Columns::ID, PhotoContext::INVALID_ID));
        photoContext->setName(data.getString(Columns::NAME));

        int defaultVisibility = static_cast<int>(Visibility::Unknown);
        Visibility contextVisibility = static_cast<Visibility>(data.getInt(Columns::VISIBILITY, defaultVisibility));
        photoContext->setVisibility(contextVisibility);
        photoContext->setRootCollection(data.getLong(Columns::ROOT_PHOTOCOLLECTION
                                                    , PhotoContext::INVALID_COLLECTION_ID));
    }
}

}//namespace

