#include "ColumnsParser.h"

#include "utils/CommonExceptions.h"

#include "../QueryData.h"

namespace calmframe {

void ColumnsParser::writeStatement(const Column & column, data::Statement & statement)
{
    NOT_IMPLEMENTED(ColumnsParser::writeStatement);
}

void ColumnsParser::readRow(const QueryData & data, Column * outColumn){
    outColumn->setName(data.getString("name"));
    outColumn->setType(ColumnTypes::ColumnType(data.getString("type")));

    if(data.getInt("notnull", 0)){
        outColumn->addConstraint(ColumnConstraints::NOT_NULL());
    }
    if(data.getInt("pk", 0)){
        outColumn->addConstraint(ColumnConstraints::PRIMARY_KEY());
    }
    if(data.hasData("dflt_value")){
        std::string defaultValue = data.getString("dflt_value");
        outColumn->addConstraint(ColumnConstraints::DEFAULT(defaultValue));
    }
}

}//namespace

