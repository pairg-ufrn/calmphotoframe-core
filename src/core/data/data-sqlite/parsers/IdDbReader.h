#ifndef IDDBREADER_H
#define IDDBREADER_H

#include <string>
#include <set>

#include "data/data-sqlite/DatabaseReader.h"

namespace calmframe {

class QueryData;

class IdDbReader : public calmframe::data::DatabaseReader
{
    std::string idColumn;
    std::set<long> ids;
    long defaultValue;
public:
    IdDbReader(const std::string & idColumn=std::string());

    virtual void readRow(const QueryData & data);

    const std::string & getIdColumn() const;
    void setIdColumn(const std::string &value);

    long getDefault() const;
    void setDefault(long defaultValue);

    const std::set<long> & getIds() const;

    /** Erases the data readed from an previous query*/
    void clearIds();
};

}//namespace
#endif // IDDBREADER_H
