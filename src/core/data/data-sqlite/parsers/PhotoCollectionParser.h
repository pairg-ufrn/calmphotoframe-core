#ifndef PHOTOCOLLECTIONPARSER_H
#define PHOTOCOLLECTIONPARSER_H

namespace calmframe {

class QueryData;
class PhotoCollection;
namespace data {
    class Statement;
}

class PhotoCollectionParser
{
public:
    void writeStatement(const PhotoCollection & photoCollection, data::Statement &statement);
    void readRow(const QueryData & data, PhotoCollection * photoCollection);
};

}//namespace

#endif // PHOTOCOLLECTIONPARSER_H
