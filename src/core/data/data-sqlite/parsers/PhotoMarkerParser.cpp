#include "PhotoMarkerParser.h"

#include "../QueryData.h"
#include "../Statement.h"
#include "../tables/MarkersTable.h"

#include "../../../model/photo/Photo.h"

#include "utils/TimeUtils.h"

namespace calmframe {

using namespace Tables;

void PhotoMarkerParser::writeStatement(const PhotoMarker & marker, data::Statement & statement){
    statement.appendValue(marker.getPhotoId()      , MarkersTable::Columns::PHOTO_ID)
             .appendValue ((int)marker.getIndex()  , MarkersTable::Columns::INDEX)
             .appendTextValue(marker.getName()     , MarkersTable::Columns::IDENTIFICATION)
             .appendRealValue(marker.getRelativeX(), MarkersTable::Columns::RELATIVEX)
             .appendRealValue(marker.getRelativeY(), MarkersTable::Columns::RELATIVEY)
             .appendValue(TimeUtils::instance().getTimestamp(), MarkersTable::Columns::UPDATED_AT);
}

void PhotoMarkerParser::readRow(const QueryData & data, PhotoMarker * outMarker){
    outMarker->setPhotoId  (data.getLong(MarkersTable::Columns::PHOTO_ID, Photo::UNKNOWN_ID));
    outMarker->setIndex    (data.getInt(MarkersTable::Columns::INDEX, 0));
    outMarker->setName     (data.getString(MarkersTable::Columns::IDENTIFICATION));
    outMarker->setRelativeX(data.getFloat(MarkersTable::Columns::RELATIVEX, 0.0f));
    outMarker->setRelativeY(data.getFloat(MarkersTable::Columns::RELATIVEY, 0.0f));
}

}//namespace
