#include "EventsParser.h"
#include "../QueryData.h"
#include "../Statement.h"
#include "../tables/EventsTable.h"

namespace calmframe {

using namespace Tables::EventsTable;

void EventsParser::writeStatement(const Event & event, data::Statement & statement){
    statement.appendValue(event.getId()                    , Columns::id);
    statement.appendValue (static_cast<int>(event.getType()), Columns::type);
    statement.appendTextValue(event.getCreation().toString()   , Columns::createdAt);

    if(event.getUserId() == Event::InvalidUserId){
        statement.appendNullValue(Columns::userId);
    }
    else{
        statement.appendValue(event.getUserId(), Columns::userId);
    }

    if(event.getEntityId() != Event::InvalidUserId){
        statement.appendValue(event.getEntityId(), Columns::entityId);
    }
    else{
        statement.appendNullValue(Columns::entityId);
    }
}

void EventsParser::readRow(const QueryData & data, Event * outEvent){
    outEvent->setId(data.getLong(Columns::id, Event::InvalidId));
    outEvent->setUserId(data.getLong(Columns::userId, Event::InvalidUserId));
    outEvent->setEntityId(data.getLong(Columns::entityId, Event::InvalidId));
    outEvent->setType(static_cast<EventType>(data.getInt(Columns::type, (int)EventType::None)));
    if(data.hasData(Columns::createdAt)){
        outEvent->setCreation(Date(data.getString(Columns::createdAt)));
    }
}


}//namespace
