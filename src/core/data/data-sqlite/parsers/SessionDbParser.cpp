#include "SessionDbParser.h"

#include "data/data-sqlite/tables/SessionTable.h"
#include "model/user/UserAccount.h"

namespace calmframe {
using namespace data;
using namespace Tables;
using namespace Tables::SessionTable;

void SessionDbParser::writeStatement(const Session &session, Statement & statement){
    statement.appendValue(session.getId()             , SessionTable::Columns::ID);
    statement.appendTextValue(session.getToken()          , SessionTable::Columns::TOKEN);
    if(session.getUserId() != UserAccount::INVALID_ID){
        statement.appendValue(session.getUserId()         , SessionTable::Columns::USER_ID);
    }
    else{
        statement.appendNullValue(SessionTable::Columns::USER_ID);
    }
    statement.appendValue(session.getExpireTimestamp(), SessionTable::Columns::EXPIRE_TIMESTAMP);
}

void SessionDbParser::readRow(const QueryData &data, Session *session){
    if(session != NULL){
        session->setId(data.getLong(Columns::ID, Session::INVALID_ID));
        session->setToken(data.getString(Columns::TOKEN));
        session->setUserId(data.getLong(Columns::USER_ID, UserAccount::INVALID_ID));
        session->setExpireTimestamp(data.getLong(Columns::EXPIRE_TIMESTAMP));
    }
}

}//namespace
