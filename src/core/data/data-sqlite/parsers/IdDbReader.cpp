#include "IdDbReader.h"

#include "data/data-sqlite/QueryData.h"

namespace calmframe {
IdDbReader::IdDbReader(const std::string &idColumn)
    : idColumn(idColumn)
    , defaultValue(0)
{}

void IdDbReader::readRow(const QueryData &data)
{
    if(data.hasData(getIdColumn())){
        long id = data.getLong(getIdColumn(), defaultValue);
        this->ids.insert(id);
    }
}

const std::string &IdDbReader::getIdColumn() const{
    return idColumn;
}
void IdDbReader::setIdColumn(const std::string &value){
    idColumn = value;
}

long IdDbReader::getDefault() const{
    return defaultValue;
}

void IdDbReader::setDefault(long defaultValue){
    this->defaultValue = defaultValue;
}

const std::set<long> &IdDbReader::getIds() const{
    return ids;
}

void IdDbReader::clearIds()
{
    this->ids.clear();
}


}//namespace
