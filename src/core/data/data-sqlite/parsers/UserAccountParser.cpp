#include "UserAccountParser.h"
#include "data/data-sqlite/tables/UserTable.h"

#include "UserDbParser.h"

namespace calmframe {

using namespace Tables;

void UserAccountParser::writeStatement(const UserAccount &userToWrite, data::Statement &statement)
{
    UserDbParser parser;
    parser.writeStatement(userToWrite.getUser(), statement);

    statement.appendTextValue(userToWrite.getPassSalt(), UserTable::Columns::SALT)
             .appendTextValue(userToWrite.getPassHash(), UserTable::Columns::PASS_HASH);
}

void UserAccountParser::readRow(const QueryData &data, UserAccount *userAccount)
{
    UserDbParser userParser;
    userParser.readRow(data, &userAccount->getUser());

    userAccount->setPassHash(data.getString(UserTable::Columns::PASS_HASH));
    userAccount->setPassSalt(data.getString(UserTable::Columns::SALT));
}


}
