#include "ForeignKeysParser.h"

#include "../QueryData.h"
#include "utils/CommonExceptions.h"

namespace calmframe {

void ForeignKeysParser::writeStatement(const ForeignKey &, data::Statement &){
    NOT_IMPLEMENTED(ForeignKeysParser::writeStatement);
}

void ForeignKeysParser::readRow(const QueryData & data, ForeignKey * outForeignKey){
    outForeignKey->setForeignTable(data.getString("table"));
    outForeignKey->setLocalColumn(data.getString("from"));
    outForeignKey->setForeignKey(data.getString("to"));

    const auto&& onDelete = data.getString("on_delete");
    const auto&& onUpdate = data.getString("on_delete");

    if(onDelete != ForeignKeyConstants::Actions::NO_ACTION){
        outForeignKey->setOnDeleteAction(onDelete);
    }
    if(onUpdate != ForeignKeyConstants::Actions::NO_ACTION){
        outForeignKey->setOnUpdateAction(onUpdate);
    }
}

}//namespace
