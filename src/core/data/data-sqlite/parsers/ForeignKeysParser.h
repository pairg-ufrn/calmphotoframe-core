#ifndef FOREIGNKEYSPARSER_H
#define FOREIGNKEYSPARSER_H

#include "./ParserFwdDef.h"
#include "../ForeignKey.h"

namespace calmframe{

class ForeignKeysParser{
public:
    void writeStatement(const ForeignKey & foreignKey, data::Statement &statement);
    void readRow(const QueryData & data, ForeignKey * outForeignKey);
};

}//namespace

#endif // FOREIGNKEYSPARSER_H
