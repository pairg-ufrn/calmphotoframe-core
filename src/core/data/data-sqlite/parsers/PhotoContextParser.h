#ifndef PHOTOCONTEXTPARSER_H
#define PHOTOCONTEXTPARSER_H


namespace calmframe {

class PhotoContext;
class QueryData;
namespace data {
    class Statement;
}//namespace

class PhotoContextParser
{
public:
    void writeStatement(const PhotoContext & photoContext, data::Statement &statement);
    void readRow(const QueryData & data, PhotoContext * photoContext);
};

}//namespace

#endif // PHOTOCONTEXTPARSER_H
