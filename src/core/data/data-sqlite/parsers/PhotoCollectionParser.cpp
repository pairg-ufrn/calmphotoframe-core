#include "PhotoCollectionParser.h"

#include "model/photo/PhotoCollection.h"
#include "model/photo/PhotoContext.h"
#include "data/data-sqlite/Statement.h"
#include "data/data-sqlite/QueryData.h"
#include "data/data-sqlite/tables/PhotoCollectionTable.h"

namespace calmframe {

using namespace data;
using namespace Tables;

void PhotoCollectionParser::writeStatement(const PhotoCollection &photoCollection, data::Statement &statement){
    statement.appendValue(photoCollection.getId()  , PhotoCollectionTable::Columns::ID);
    statement.appendTextValue(photoCollection.getName(), PhotoCollectionTable::Columns::NAME);
    if(photoCollection.getParentContext() > PhotoContext::INVALID_ID){
        statement.appendValue(photoCollection.getParentContext(), PhotoCollectionTable::Columns::PARENT_CONTEXT);
    }
    else{
        statement.appendNullValue(PhotoCollectionTable::Columns::PARENT_CONTEXT);
    }

    statement.appendTextValue("CURRENT_TIMESTAMP", PhotoCollectionTable::Columns::UPDATEDAT);
}

void PhotoCollectionParser::readRow(const QueryData &data, PhotoCollection *photoCollection){
    if(photoCollection != NULL){
        photoCollection->setId(data.getLong(PhotoCollectionTable::Columns::ID));
        photoCollection->setParentContext(data.getLong(PhotoCollectionTable::Columns::PARENT_CONTEXT));
        photoCollection->setName(data.getString(PhotoCollectionTable::Columns::NAME));

    }
}

}

