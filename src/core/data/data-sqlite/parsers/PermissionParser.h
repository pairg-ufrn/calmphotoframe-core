#ifndef PERMISSIONPARSER_H
#define PERMISSIONPARSER_H

#include "ParserFwdDef.h"
#include "model/permissions/Permission.h"

namespace calmframe{

class PermissionParser
{
public:
    void writeStatement(const Permission & permission, data::Statement &statement);
    void readRow(const QueryData & data, Permission * outPermission);
};

}

#endif // PERMISSIONPARSER_H
