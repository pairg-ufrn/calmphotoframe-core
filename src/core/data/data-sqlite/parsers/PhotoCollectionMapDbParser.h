#ifndef PHOTOCOLLECTIONMAPDBPARSER_H
#define PHOTOCOLLECTIONMAPDBPARSER_H

namespace calmframe {
    class QueryData;
namespace data {
    class Statement;
}//namespace

class PhotoCollectionMap{
public:
    enum MappingType{PhotoMapping=1, SubCollectionMapping=2};
private:
    long collectionId;
    long itemId;
    MappingType type;
public:
    PhotoCollectionMap(long collectionId=0, long itemId=0, MappingType type=PhotoMapping);

    long getCollectionId() const;
    void setCollectionId(long value);
    long getItemId() const;
    void setItemId(long value);

    MappingType getType() const;
    void setType(const MappingType &value);
};

class PhotoCollectionMapDbParser
{
public:
    void writeStatement(const PhotoCollectionMap & photoCollectionMap, data::Statement &statement);
    void readRow(const QueryData & data, PhotoCollectionMap * photoCollectionMap);
};

}//namespace

#endif // PHOTOCOLLECTIONMAPDBPARSER_H
