#ifndef EVENTSPARSER_H
#define EVENTSPARSER_H

#include "ParserFwdDef.h"
#include "model/events/Event.h"

namespace calmframe{

class EventsParser
{
public:
    void writeStatement(const Event & event, data::Statement &statement);
    void readRow(const QueryData & data, Event * outEvent);
};

}

#endif // EVENTSPARSER_H
