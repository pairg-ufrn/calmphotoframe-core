#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include "ParserFwdDef.h"
#include "model/ConfigValue.h"

namespace calmframe{

class ConfigParser{
public:
    void writeStatement(const ConfigValue & value, data::Statement &statement);
    void readRow(const QueryData & data, ConfigValue * outValue);
};

}//namespace

#endif // CONFIGPARSER_H
