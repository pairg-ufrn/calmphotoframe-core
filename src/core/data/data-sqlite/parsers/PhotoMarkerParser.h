#ifndef PHOTOMARKERPARSER_H
#define PHOTOMARKERPARSER_H

#include "ParserFwdDef.h"
#include "../../../model/photo/PhotoMarker.h"

namespace calmframe {

class PhotoMarkerParser
{
public:
    void writeStatement(const PhotoMarker & marker, data::Statement &statement);
    void readRow(const QueryData & data, PhotoMarker * outMarker);
};

}//namespace

#endif // PHOTOMARKERPARSER_H
