#include "ForeignKeyViolation.h"

namespace calmframe {

std::string ForeignKeyViolation::getViolatedTable() const
{
    return violatedTable;
}

void ForeignKeyViolation::setViolatedTable(const std::string & value)
{
    violatedTable = value;
}

std::string ForeignKeyViolation::getReferencedTable() const
{
    return referencedTable;
}

void ForeignKeyViolation::setReferencedTable(const std::string & value)
{
    referencedTable = value;
}

long ForeignKeyViolation::getRowId() const
{
    return rowId;
}

void ForeignKeyViolation::setRowId(long value)
{
    rowId = value;
}

int ForeignKeyViolation::getForeignKeyIndex() const
{
    return foreignKeyIndex;
}

void ForeignKeyViolation::setForeignKeyIndex(int value)
{
    foreignKeyIndex = value;
}

}//namespace
