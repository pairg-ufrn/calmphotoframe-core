#include "ViolationParser.h"

#include "data/data-sqlite/QueryData.h"

namespace calmframe {

void ViolationParser::readRow(const calmframe::QueryData & data, ForeignKeyViolation * out){
    out->setViolatedTable(data.getString(0));
    out->setRowId(data.getLong(1));
    out->setReferencedTable(data.getString(2));
    out->setForeignKeyIndex(data.getInt(3));
}

}//namespace

