#ifndef VIOLATIONREADER_H
#define VIOLATIONREADER_H

#include "../parsers/ParserFwdDef.h"
#include "ForeignKeyViolation.h"

namespace calmframe {

class ViolationParser{
public:
    void readRow(const QueryData & data, ForeignKeyViolation * out);
};

}

#endif // VIOLATIONREADER_H
