#ifndef VIOLATION_H
#define VIOLATION_H

#include <string>

namespace calmframe {

class ForeignKeyViolation{
public:
    std::string getViolatedTable() const;
    void setViolatedTable(const std::string & value);
    
    std::string getReferencedTable() const;
    void setReferencedTable(const std::string & value);
    
    long getRowId() const;
    void setRowId(long value);
    
    int getForeignKeyIndex() const;
    void setForeignKeyIndex(int value);
    
private:
    std::string violatedTable, referencedTable;
    long rowId;
    int foreignKeyIndex;
};

}//namespace

#endif // VIOLATION_H
