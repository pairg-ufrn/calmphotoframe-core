/*
 * SqliteDatabase.cpp
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#include "Configurations.h"
#include "Options.h"

#include "SqliteConnector.h"
#include "SqlException.hpp"
#include "DatabaseWriter.h"
#include "DatabaseReader.h"
#include "Table.h"
#include "ScopedTransaction.h"
#include "Statement.h"
#include "Query.h"
#include "QueryArgs.h"

#include "../../exceptions/BusyDatabaseException.h"

#include "utils/IllegalStateException.h"
#include "utils/Log.h"

extern "C" {
    #include <sqlite3.h>
}

#include <string>
#include <map>
#include <iostream>
using namespace std;

namespace calmframe {
namespace data {

#define BEGIN_TRANSACTION_STATEMENT "BEGIN TRANSACTION;"
#define COMMIT_STATEMENT "COMMIT;"
#define ROLLBACK_STATEMENT "ROLLBACK;"

namespace Errors {
    namespace Constants {
        static const char * WRITE_STATEMENT = "Failed to execute write statement.";
        static const char * OPEN_DATABASE   = "Failed to open database";
        static const char * CANNOT_ENABLE_FOREIGNKEYS = "Failed to enable foreign keys constraints.";
    }

    string getWriteStatementError(Statement& statement)
    {
        string errMsg(Constants::WRITE_STATEMENT);
        errMsg.append(" When trying to execute statement: '")
              .append(statement.getStatementTemplate()).append("'.");
        return errMsg;
    }

    string getCreateStatementError(sqlite3 * sqliteDb, const std::string & stmtTemplate)
    {
        return string("Trying to create statement: '")
                        .append(stmtTemplate)
                        .append("'. But, ocurred sqlite error: '")
                        .append(sqlite3_errmsg(sqliteDb))
                        .append("'.");
    }


    string openDbMsg(const std::string& databaseName, sqlite3 * database)
    {
        return string(Errors::Constants::OPEN_DATABASE)
                                    .append(" '").append(databaseName).append("'.")
                                    .append(" Due to database error: ").append(sqlite3_errmsg(database));
    }
}

namespace ConstStatements {
    //Sqlite does not enable foreign keys constraints by default
    static const string ENABLE_FOREIGNKEYS    = "PRAGMA foreign_keys = ON";
    static const string STORE_TEMPS_ON_MEMORY = "PRAGMA temp_store = memory";
}//namespace

class SqliteInitializer{
public:
    SqliteInitializer(){
        //Must be called before initialize sqlite, i.e. before any call to sqlite.
        // Calls made after sqlite initialization will not make any change.

        //Setup function to log/display sqlite errors
        sqlite3_config(SQLITE_CONFIG_LOG, errorLogCallback, NULL);
        //enable multithread mode
        sqlite3_config(SQLITE_CONFIG_MULTITHREAD);
    }

    static void errorLogCallback(void * /*pArg*/, int iErrCode, const char *zMsg){
        logWarning("Sqlite error(%d): %s", iErrCode, zMsg);
    }
};
static const SqliteInitializer initializer;

//static
SqliteConnector SqliteConnector::createInMemoryDb(){
    return SqliteConnector(std::string(), true);
}

SqliteConnector::SqliteConnector(const std::string& databasePath)
    : SqliteConnector(databasePath, false)
{}

SqliteConnector::SqliteConnector(const string & databasePath, bool allowInMemory)
    : database(NULL)
    , databasePath(databasePath)
    , transactionCount(0)
{
    if(allowInMemory || !databasePath.empty()){
        this->open(databasePath);
    }
}

SqliteConnector::SqliteConnector(SqliteConnector && other)
    : database(other.database)
    , databasePath(other.databasePath)
    , transactionCount(other.transactionCount)
{
    other.database = NULL;
    other.databasePath = "";
    other.transactionCount = 0;
}

SqliteConnector::~SqliteConnector(){
    if(isOpen()){
        close();
    }
}

const string &SqliteConnector::getDatabasePath() const{
    return databasePath;
}

bool SqliteConnector::isOpen() const {
	return database != NULL;
}

void SqliteConnector::open(const std::string& dbName) {
	if(isOpen()){
		close();
    }

    int rc = sqlite3_open(dbName.c_str(), &database);

    if( rc != SQLITE_OK){
        close();
        throw SqlException(Errors::openDbMsg(dbName, database), rc);
    }

    configDatabase();
}
void SqliteConnector::close() {
    int result = sqlite3_close_v2(this->database);
    if(result != SQLITE_OK){
        const char * sqliteErrMsg = sqlite3_errstr(result);
        logWarning("Error trying to close database: '%s'", sqliteErrMsg);
    }
	database = NULL;
}

void SqliteConnector::configDatabase()
{
    setBusyTimeout(Configurations::instance().getInteger(Options::DATABASE_TIMEOUT, Options::Defaults::DATABASE_TIMEOUT));

    if(!execute(ConstStatements::ENABLE_FOREIGNKEYS)){
        throw SqlException(Errors::Constants::CANNOT_ENABLE_FOREIGNKEYS);
    }
    if(!execute(ConstStatements::STORE_TEMPS_ON_MEMORY)){
        throw SqlException("Failed enable store on memory");
    }
}

void SqliteConnector::setBusyTimeout(int milliseconds)
{
    if(!isOpen()){
        throw CommonExceptions::IllegalStateException("Trying to set busy timeout on closed database.");
    }
    //Allows database to wait until BUSY_TIMEOUT milliseconds to database unlock (exit from busy state).
    //If the max time is over, it will return SQLITE_BUSY error.
    sqlite3_busy_timeout(database, milliseconds);
}

void SqliteConnector::query(DatabaseReader & reader, const Table & table, const string & whereCondition) const{
    query(reader, table, QueryArgs{whereCondition});
}

void SqliteConnector::query(DatabaseReader & reader, const Table & table, const QueryArgs & args, const std::vector<string> & columns) const
{
    this->query(reader, table.getQueryStatement(columns, args));
}

void SqliteConnector::query(DatabaseReader & reader, const string & statement) const
{
    Query queryStatement(statement);
    queryStatement.initialize(*this);
    queryStatement.read(reader);
}

long SqliteConnector::insert(Statement& insertStatement) {
	insertStatement.execute();
	//TODO: verificar se de fato comando foi executado e dados inseridos
	return sqlite3_last_insert_rowid(this->database);
}

int SqliteConnector::insert(DatabaseWriter &writer, const Table &table, bool useTransaction){
    return this->insert(table.getName(), table.getColumnsList(), writer, useTransaction);
}

int SqliteConnector::insert(const string &table, const std::vector<string> &columns, DatabaseWriter &writer, bool useTransaction
                                       , OnConflict resolution){
    Statement statement;
    std::string insertStatementStr = Table::getInsertStatement(table, columns, resolution);

    this->createStatement(statement, insertStatementStr);

    statement.setColumns(columns);

    return executeWriteStatement(writer, statement,useTransaction);
}

int SqliteConnector::update(Statement& updateStatement) {
    return executeStatement(updateStatement);
}

int SqliteConnector::update(DatabaseWriter &writer, const Table &table, const string &whereCondition, bool useTransaction){
    return this->update(writer, table.getName(), table.getColumnsList(), whereCondition, useTransaction);
}

int SqliteConnector::update(DatabaseWriter &writer, const string &tableName, const std::vector<string> &columns, const string &whereCondition, bool useTransaction){
    Statement statement;
    this->createStatement(statement, Table::getUpdateStatement(tableName, columns, whereCondition));
    statement.setColumns(columns);

    return executeWriteStatement(writer, statement, useTransaction);
}

int SqliteConnector::remove(const Table &table, const string &whereCondition){
    //FIXME: tratar "whereCondition" antes de criar statement
    string removeStatementStr = table.getRemoveStatement(whereCondition);
    Statement removeStatement;
    this->createStatement(removeStatement, removeStatementStr);

    return remove(removeStatement);
}

int SqliteConnector::remove(Statement& deleteStatement) {
    return executeStatement(deleteStatement);
}

void SqliteConnector::beginTransaction(TransactionType type) {
    if(transactionCount == 0){
        try{
            execute(beginTransactionStmt(type));
        }
        catch(const SqlException & ex){
            if(ex.getErrorNumber() == SQLITE_BUSY){
                throw BusyDatabaseException("", ex);
            }
            throw;
        }
    }
    ++transactionCount;
}

void SqliteConnector::endTransaction() {
    commit();
}

void SqliteConnector::commit() {
    --transactionCount;
    if(transactionCount == 0){
        this->execute(COMMIT_STATEMENT);
    }
}
void SqliteConnector::rollback() {
    if(transactionCount > 0){
        this->execute(ROLLBACK_STATEMENT);
        transactionCount = 0;
    }
}

string SqliteConnector::beginTransactionStmt(TransactionType type){
    stringstream stmt;
    stmt << "BEGIN ";

    if(type == TransactionType::Write){
        stmt << "IMMEDIATE ";
    }

    stmt << "TRANSACTION;";
    return stmt.str();
}

int SqliteConnector::executeWriteStatement(DatabaseWriter &writer, Statement & statement, bool useTransaction){
    try{
        if(!useTransaction){
            return executeWriteStatementImpl(writer, statement);
        }
        else{
            ScopedTransaction transaction(this);
            int changedRows = executeWriteStatementImpl(writer, statement);
            transaction.commit();
            return changedRows;
        }
    }
    catch(const exception & ex){
        tryFinalizeStatement(statement);
        throw SqlException(Errors::getWriteStatementError(statement), ex);
    }
}

long SqliteConnector::getLastInsertedId(){
    return sqlite3_last_insert_rowid(this->database);
}

bool SqliteConnector::execute(const std::string& statement) {
    return this->execute((SqliteExecuteHandler *) NULL, statement);
}

bool SqliteConnector::execute(SqliteExecuteHandler * handler, const std::string& statement){
    return this->execute((handler == NULL ? NULL : executeCallback), statement, handler);
}
bool SqliteConnector::execute(int (*callback)(void*, int, char**, char** ), const std::string& statement, void * data) const{
        if(database == NULL){
            string errorExecute = string("Trying to execute statement with NULL database.")
                                        .append(" When trying to execute statement: ")
                                        .append("'").append(statement).append("'");
            throw CommonExceptions::IllegalStateException(errorExecute);
        }

        char * errMsg;

        int rc = sqlite3_exec(this->database, statement.c_str(), callback, data, &errMsg);
        if( rc != SQLITE_OK){
            logError("Sqlite Error(%d): %s\n", rc, errMsg);
            string error(errMsg);
            sqlite3_free(errMsg);

            string errMsg = string("Could not execute statement: ")
                                .append("'").append(statement).append("'. Due to database error: ").append(error);
            throw SqlException(errMsg, rc);
        }
        return true;
}
int SqliteConnector::executeCallback(void* data, int collumnCount, char** collumnData, char** collumnNames) {
    SqliteExecuteHandler* handler = static_cast<SqliteExecuteHandler *>(data);
    return handler->onExecuteSql(collumnCount, collumnNames, collumnData);
}

void SqliteConnector::createStatement(Statement & statementOut, const std::string& statementStr) const{
	  sqlite3_stmt *statement;
	  const char  *pzTail = NULL;  /* OUT: Pointer to unused portion of zSql */

	  if( sqlite3_prepare_v2(this->database, statementStr.c_str(),statementStr.length(), &statement, &pzTail)
			  != SQLITE_OK )
      {
          throw SqlException(Errors::getCreateStatementError(database, statementStr));
	  };
      statementOut.initialize(statementStr, statement);
}

class ExistTableDbReader : public DatabaseReader{
private:
    bool exist;
public:
    ExistTableDbReader() : exist(false)
    {}

    void readRow(const QueryData & rowData){
        exist = (rowData.getInt("count") != 0);
    }
    bool tableExist() const{
        return this->exist;
    }
};

bool calmframe::data::SqliteConnector::existTable(const std::string &tableName){
    ExistTableDbReader reader;
    const string stmt = string("select count(type) as count from sqlite_master "
                               "where type='table' and name=")
                              .append("'").append(tableName).append("'");
    this->query(reader, stmt);
    return reader.tableExist();
}

bool SqliteConnector::isOnTransaction() const {
    return transactionCount > 0;
}

int SqliteConnector::executeStatement(Statement &statement) const{
    statement.execute();
    return sqlite3_changes(this->database);
}


int SqliteConnector::executeWriteStatementImpl(DatabaseWriter &writer, Statement& statement) const
{
    int changedRows = 0;
    while(writer.hasNext()){
        writer.writeRow(statement);
        statement.execute();
        changedRows += sqlite3_changes(database);
        statement.reset();
    }

    statement.finalize();

    return changedRows;
}

void SqliteConnector::tryFinalizeStatement(Statement& statement)
{
    try{
        if(!statement.isFinalized()){
            statement.finalize();
        }
    }
    catch(const SqlException & otherEx){
        logError("Error when trying to finalize statement: \n%s",otherEx.getMessage().c_str());
    }
}

} /* namespace data */
} /* namespace calmframe */

