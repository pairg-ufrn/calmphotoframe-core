/*
 * Statement.h
 *
 *  Created on: 02/10/2014
 *      Author: noface
 */

#ifndef STATEMENT_H_
#define STATEMENT_H_

#include <string>
//#include "sqlite3.h"

#include <vector>

class sqlite3_stmt;

namespace calmframe {
namespace data {

class SqliteConnector;

class Statement {
private:
    std::string templateStatement;
	sqlite3_stmt* sqliteStatement;
	int bindedParamCount;
    typedef std::vector<std::string> ColumnsList;
    ColumnsList columns;
public:
    Statement(const std::string & templateStatement=std::string(), sqlite3_stmt* sqliteStatement=NULL);
    Statement(SqliteConnector & connector,
              const std::string & statement,
              const ColumnsList & columns=ColumnsList());

	virtual ~Statement();

    const std::string & getStatementTemplate() const;

    void initialize(const SqliteConnector & connector);
    void initialize(const std::string & templateStatement, sqlite3_stmt* sqliteStatement);

    Statement & appendTextValue(const std::string & text, const std::string & columnName);
    Statement & appendValue(int number, const std::string & columnName);
    Statement & appendValue(long number, const std::string & columnName);
    Statement & appendRealValue(double number, const std::string & columnName);
    Statement & appendNullValue(const std::string & columnName);

    Statement & appendTextValue(const std::string & text, int argumentNumber = -1);
    Statement & appendValue(int number, int argumentNumber = -1);
    Statement & appendValue(long number, int argumentNumber = -1);
    Statement & appendRealValue(double number, int argumentNumber = -1);
    Statement & appendNullValue(int columnNumber);

    Statement & reset();
	void execute();
	void finalize();
	bool isFinalized() const;
    bool isInitialized() const;

    int indexOf(const std::string & colName) const;

    const ColumnsList & getColumns() const;
    void setColumns(const ColumnsList &value);
public:
    static void assertNotErrorCode(int resultCode, const std::string & errMessage, bool appendErrorMsg=true);
    static bool isErrorCode(int resultCode);
    static std::string errorMessage(const std::string &err, int resultCode);
protected:
    void assertNotFinalized(const std::string &where) const;
    int executeImpl();
    sqlite3_stmt * getStmtPointer() const;
private:
    int getArgumentNumber(int argumentNumber);
    int getArgumentNumber(int currentCount, int argumentNumber);
};

} /* namespace data */
} /* namespace calmframe */

#endif /* STATEMENT_H_ */
