#ifndef TABLE_H
#define TABLE_H

#include "Column.h"
#include "ForeignKey.h"

#include <string>
#include <map>
#include <set>
#include <vector>
#include <ostream>
#include <sstream>

namespace calmframe {
namespace data{

namespace SqlComparator{
    static const char * EQUALS    = "=";
    static const char * NOT_EQUALS    = "!=";
    static const char * LESS_THAN = "< ";
}

enum class OnConflict{DEFAULT, REPLACE, ROLLBACK, ABORT_STMT, STOP /* FAIL */, IGNORE};

class QueryArgs;

typedef std::vector<std::string> ColumnList;
typedef std::set<std::string> ColumnsSet;

class Table
{
public: //static members
    static std::string getQueryStatement(const std::string & tableName
                                        , const std::vector<std::string> & columns
                                        , const std::string & whereCondition="");
    static std::string getQueryStatement(const std::string & tableName
                                        , const std::vector<std::string> & columns
                                        , const QueryArgs & queryArgs);

    static std::string getInsertPrefix(OnConflict resolution = OnConflict::DEFAULT );
    static std::string insertIntoTablePrefix(const std::string & table, const std::vector<std::string> & columns
                                          , OnConflict resolution = OnConflict::DEFAULT);
    static std::string getInsertStatement(const std::string & table, const std::vector<std::string> & columns
                                          , OnConflict resolution = OnConflict::DEFAULT );
    static std::string getUpdateStatement(const std::string & table, const std::vector<std::string> & columns
                                                 , const std::string & whereCondition);
    template<typename T>
    static std::string WhereEquals(const std::string & columnName, const T & value, bool text=false);
    template<typename T> static
    std::string WhereCompare(const std::string & columnName, const std::string & sqlComparator
                             , const T & value, bool text=false);
    template<typename Iterator>
    static std::string WhereIn(const std::string & columnName, const Iterator & begin, const Iterator & end);
    static std::string WhereIn(const std::string & columnName, const std::string & stmt);

    static std::string OR(const std::string &conditionOne, const std::string &conditionTwo);
    static std::string AND(const std::string & conditionOne, const std::string & conditionTwo);
    static std::string NOT(const std::string & condition);
    static std::string AS(const std::string & column, const std::string & renameColumn);
    static std::string UNION(const std::string & selection1, const std::string & selection2);

    static std::string PAREN(const std::string & expr);

    /** Produces a string with @code{expr} surrounded by parenthesis and preceded by @code{prefix} */
    static std::string PAREN(const std::string & prefix, const std::string & expr);
private:
    using ColumnsMap      = std::map<std::string, Column>;
    //maps the local user column to the foreignKey data
    using ForeignKeyMap   = std::map<std::string, ForeignKey>; 
    using ConstraintsList = std::vector<std::string>;

    std::string name;

    ColumnsMap columns;
    ColumnsSet primaryKeys;

    ForeignKeyMap foreignKeys;
    ConstraintsList otherConstraints;

    std::string idColumn;
    ColumnList insertColumns;
    ColumnList updateColumns;
public:
    Table(const std::string & tableName=std::string());
    virtual ~Table(){}

    const std::string & getName() const;
    void setName(const std::string &value);

    bool hasMultipleKeys() const;

    const ColumnList & getInsertColumns() const;
    void addInsertColumn(const std::string & column);
    void removeInsertColumn(const std::string & column);
    const ColumnList & getUpdateColumns() const;
    void addUpdateColumn(const std::string & column);
    void removeUpdateColumn(const std::string & column);

    bool hasColumn(const std::string & columnName);
    Column & getColumn(const std::string & columnName, Column && defaultValue=Column());
    const Column & getColumn(const std::string & columnName, const Column && defaultValue=Column()) const;
    Column & putColumn(const Column & col, bool isInsertColumn=false, bool isUpdateColumn=false);
    void removeColumn(const std::string & columnName);

    const ColumnsSet & getPKs() const;
    void updatePKs();

    void addConstraint(const std::string & constraint);

    ForeignKey &addForeignKey(const std::string & localColumn,
                                           const std::string & foreignTable, const std::string & foreignKey);
    ForeignKey &addForeignKey(const ForeignKey & fk);
    ForeignKey getForeignKey(const std::string & localColumn) const;
    void removeForeignKey(const std::string & localColumn);

    ColumnList getColumnsList() const;

//    std::string queryCondition(const std::string & queryString) const;
    std::string queryTemplate(const std::string & templateParam) const;
    std::string queryTemplate(const std::string & templateParam, const ColumnList & columns) const;

    std::string getQueryStatement(const std::string & whereCondition="") const;
    std::string getQueryStatement(const QueryArgs & queryArgs) const;
    std::string getQueryTemplate(const QueryArgs & queryArgs) const;
    std::string getQueryStatement(const ColumnList & columns, const std::string & whereCondition="") const;
    std::string getQueryStatement(const ColumnList & columns
                                        , const QueryArgs & queryArgs) const;

    std::string getCreateStatement() const;
    std::string getInsertStatement(OnConflict resolution = OnConflict::DEFAULT) const;
    std::string getRemoveStatement(const std::string & whereCondition) const;
    static void listColumns(const std::vector<std::string> &columns, std::ostream & out);
    static std::ostream & whereCondition(std::ostream & out, const std::string &whereCondition);

    const std::string &getIdColumn() const;
    void setIdColumn(const std::string &value);

    std::string whereIdEquals(long id) const;

    static std::ostream & listColumns(std::ostream & sstream, const std::vector<std::string> &columns);

protected:
    std::ostream & queryColumnsImpl(std::ostream & out,
                                    const std::string& queryString,
                                    const ColumnList & columns,
                                    bool templateStr) const;

    static std::string getQueryStmtImpl(const std::string& tableName, const ColumnList& columns
                , const QueryArgs& queryArgs, bool makeTemplate=false);
    void joinParamName(std::ostream& out, const std::string& paramName, bool isStrLiteral) const;

    int outputColumnsList(std::ostream & out) const;
    static void outputPlaceholders(std::ostream & out, int count);
    void outputColumns(std::ostream & out) const;
    void outputTableConstraints(std::ostream & out, bool first=true) const;
};


template<typename T>
std::string Table::WhereEquals(const std::string &columnName, const T &value, bool text){
    return WhereCompare(columnName, SqlComparator::EQUALS, value, text);
}

template<typename T>
std::string Table::WhereCompare(const std::string &columnName, const std::string & sqlComparator
                                , const T &value, bool text)
{
    std::stringstream sstream;
    sstream << columnName << " "<< sqlComparator <<" ";
    if(!text){
        sstream<< value;
    }
    else{

        sstream << "'" << value << "'";
    }
    return sstream.str();
}

template<typename Iterator>
std::string Table::WhereIn(const std::string &columnName, const Iterator &begin, const Iterator &end)
{
    std::stringstream itemsList;

    bool first = true;
    Iterator iter  = begin;
    while(iter != end){
        if(!first){
            itemsList << ", ";
        }
        first = false;

        itemsList << *iter;
        ++iter;
    }

    return WhereIn(columnName, itemsList.str());
}


}
}


#endif // TABLE_H
