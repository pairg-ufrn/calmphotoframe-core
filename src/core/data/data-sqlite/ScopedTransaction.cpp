#include "ScopedTransaction.h"
#include "SqlException.hpp"

namespace calmframe {
namespace data {

namespace Constants {
    static const char * ALREADY_FINISHED_ERROR = "Trying to finish already finished transaction.";
}//namespace

ScopedTransaction::ScopedTransaction(SqliteConnector * db, TransactionType transactionType)
    : dbConnector(db)
    , finished(false)
{
    this->dbConnector->beginTransaction(transactionType);
}

ScopedTransaction::~ScopedTransaction()
{
    if(!finished){
        rollback();
    }
}

void ScopedTransaction::commit()
{
    assertNotFinished();
    this->finished = true;
    this->dbConnector->commit();
}

void ScopedTransaction::rollback()
{
    assertNotFinished();
    this->finished = true;
    this->dbConnector->rollback();
}

void ScopedTransaction::assertNotFinished()
{
    if(finished){
        throw SqlException(Constants::ALREADY_FINISHED_ERROR);
    }
}

}//namespace
}//namespace

