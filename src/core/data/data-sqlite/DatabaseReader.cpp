#include "DatabaseReader.h"
#include "utils/Log.h"

namespace calmframe {
namespace data {

QueryHandler::QueryResult DatabaseReader::onQueryData(const QueryData &data){
    if(!this->hasFinished()){
        this->readRow(data);
    }
    return QueryOk;
}

}
}
