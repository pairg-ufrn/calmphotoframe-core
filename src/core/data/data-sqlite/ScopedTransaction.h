#ifndef SCOPEDTRANSACTION_H
#define SCOPEDTRANSACTION_H

#include "SqliteConnector.h"

namespace calmframe {
namespace data {

class ScopedTransaction
{
private:
    SqliteConnector * dbConnector;
    bool finished;
public:
    ScopedTransaction(SqliteConnector * db, TransactionType transactionType = TransactionType::Default);
    ~ScopedTransaction();

    void commit();
    void rollback();
    void assertNotFinished();
};

}//namespace
}//namespace

#endif // SCOPEDTRANSACTION_H
