#include "QueryArgs.h"

namespace calmframe {
namespace data {

std::string QueryArgs::orderTypeToString(QueryArgs::OrderType orderType)
{
    switch (orderType) {
    case QueryArgs::ASC:
        return "ASC";
    case QueryArgs::DESC:
        return "DESC";
    default:
        return std::string();
    }
}

QueryArgs::QueryArgs(
    const std::string & whereCondition
    , int limit
    , const std::string & orderBy
    , OrderType orderType)
: whereCondition(whereCondition)
    , orderBy(orderBy)
    , limit(limit)
    , orderType(orderType)
{}

const std::string & QueryArgs::getOrderBy() const{
    return orderBy;
}
QueryArgs & QueryArgs::setOrderBy(const std::string & value){
    orderBy = value;
    return *this;
}

int QueryArgs::getLimit() const{
    return limit;
}
QueryArgs & QueryArgs::setLimit(int value){
    limit = value;
    return *this;
}

const std::string & QueryArgs::getWhereCondition() const{
    return whereCondition;
}
QueryArgs & QueryArgs::setWhereCondition(const std::string & value){
    whereCondition = value;
    return *this;
}

QueryArgs::OrderType QueryArgs::getOrderType() const{
    return orderType;
}

QueryArgs & QueryArgs::setOrderType(const OrderType & value){
    orderType = value;
    return *this;
}

}//namespace
}//namespace

