/*
 * QueryHandler.h
 *
 *  Created on: 03/10/2014
 *      Author: noface
 */

#ifndef QUERYHANDLER_H_
#define QUERYHANDLER_H_

#include <map>
#include "QueryData.h"

namespace calmframe {
namespace data {

class QueryHandler {
public:
    enum QueryResult{QueryOk = 0, QueryCancel};

    virtual ~QueryHandler(){}
    virtual QueryResult onQueryData(const QueryData & data) =0;
};

} /* namespace data */
} /* namespace calmframe */

#endif /* QUERYHANDLER_H_ */
