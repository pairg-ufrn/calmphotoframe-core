/*
 * SqliteDataBuilder.cpp
 *
 *  Created on: 25/12/2014
 *      Author: noface
 */

#include "SqliteDataBuilder.hpp"
#include "SqliteData.h"
#include "utils/FileUtils.h"

namespace calmframe {
namespace data {

SqliteDataBuilder::SqliteDataBuilder(const std::string & databaseDirectory, const std::string &databaseName
                                     , bool createHierarchy)
    : dbDir(databaseDirectory)
    , dbName(databaseName)
    , createHierarchy(createHierarchy)
{}
DataBuilder *SqliteDataBuilder::clone() const{
    return new SqliteDataBuilder(this->dbDir, this->dbName, getCreateHierarchy());
}

Data * SqliteDataBuilder::buildData(){
    if(getCreateHierarchy() == true){
        utils::FileUtils::instance().createDirIfNotExists(dbDir, true);
    }

    return new SqliteData(this->dbDir, dbName);
}

bool SqliteDataBuilder::getCreateHierarchy() const{
    return createHierarchy;
}

} /* namespace data */
} /* namespace calmframe */



