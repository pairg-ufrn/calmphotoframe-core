#include "Query.h"

#include "Table.h"
#include "QueryArgs.h"
#include "DatabaseReader.h"

#include "sqlite3.h"

namespace calmframe {

using namespace data;

Query::Query(const std::string & statement)
    : super(statement)
{}

Query::Query(const Table & table)
    : Query(table, QueryArgs())
{}

Query::Query(const data::Table & table, const data::QueryArgs & args, const ColumnsList & columns)
    : Query(table.getQueryStatement(columns, args))
{}

Query::Query(SqliteConnector & conn, const Table & table, const QueryArgs & args, const ColumnsList & columns)
    : Query(conn, table.getQueryStatement(columns, args))
{}

Query::Query(SqliteConnector & conn, const std::string & statement)
    : super(conn, statement)
{}

bool Query::read(){
    return executeImpl() == SQLITE_ROW;
}

void Query::read(data::DatabaseReader & reader)
{
    while(!reader.hasFinished() && read()){
        reader.readRow(*this);
    }
}

std::string Query::getString(const std::string & col, const std::string & defaultValue) const{
    return getString(getColumnIndex(col), defaultValue);
}

int Query::getColumnsCount() const{
    return sqlite3_data_count(getStmtPointer());
}

std::string Query::getColumnName(int index) const{
    if(indexIsValid(index)){
        const char * const name = sqlite3_column_name(getStmtPointer(), index);
        if(name != NULL){
            return std::string(name);
        }
    }
    return std::string();
}

bool Query::indexIsValid(int resultIndex) const{
    int columnsCount = getColumnsCount();
    return resultIndex >= 0 && resultIndex < columnsCount;
}

std::string Query::getString(int resultIndex, const std::string & defaultValue) const{
    if(!hasData(resultIndex)){
        return defaultValue;
    }
    const unsigned char * text = sqlite3_column_text(getStmtPointer(), resultIndex);
    return text  != NULL ? std::string((const char *)text) : defaultValue;
}

long Query::getLong(const std::string & col, long defaultValue) const{
    return getLong(getColumnIndex(col), defaultValue);
}

long Query::getLong(int index, long defaultValue) const{
    return ! hasData(index) ? defaultValue : sqlite3_column_int64(getStmtPointer(), index);
}

int Query::getInt(const std::string & col, int defaultValue) const{
    return getInt(getColumnIndex(col), defaultValue);
}

int Query::getInt(int index, int defaultValue) const{
    return !hasData(index) ? defaultValue : sqlite3_column_int(getStmtPointer(), index);
}

float Query::getFloat(const std::string & col, float defaultValue) const{
    return getFloat(getColumnIndex(col), defaultValue);
}

float Query::getFloat(int index, float defaultValue) const{
    //sqlite has no method to get float
    return (float)getDouble(index, defaultValue);
}

double Query::getDouble(const std::string & col, double defaultValue) const{
    return getDouble(getColumnIndex(col), defaultValue);
}

double Query::getDouble(int index, double defaultValue) const{
    return !hasData(index) ? defaultValue : sqlite3_column_double(getStmtPointer(), index);
}

bool Query::hasData(const std::string & column) const{
    return hasData(getColumnIndex(column));
}

bool Query::hasData(int resultIndex) const{
    return indexIsValid(resultIndex) && !columnIsNull(resultIndex);
}

bool Query::columnIsNull(const std::string & column) const{
    return columnIsNull(getColumnIndex(column));
}

bool Query::columnIsNull(int index) const{
    return indexIsValid(index) && sqlite3_column_type(getStmtPointer(), index) == SQLITE_NULL;
}

int Query::getColumnIndex(const std::string & col) const{
    if(columnToIndex.empty()){
        int numColumns = getColumnsCount();
        for(int i=0; i < numColumns; ++i){
            const char * const name = sqlite3_column_name(getStmtPointer(), i);
            if(name != NULL){
                columnToIndex[name] = i;
            }
        }
    }

    auto iter = columnToIndex.find(col);
    return iter == columnToIndex.end() ? -1 : iter->second;
}

}//namespace

