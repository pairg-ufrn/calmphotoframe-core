/*
 * SqliteData.cpp
 *
 *  Created on: 01/10/2014
 *      Author: noface
 */

#include "SqliteData.h"
#include "SqlException.hpp"
#include "parsers/IdDbReader.h"
#include "Table.h"
#include "Environment.h"

#include "utils/IllegalArgumentException.hpp"
#include "utils/FileUtils.h"
#include "utils/StringCast.h"
#include "utils/Log.h"

#include <exception>
#include <string>

using namespace std;
using namespace calmframe::utils;

namespace calmframe {
namespace data {

const int SqliteData::DB_VERSION = DatabaseMigration::MAX_VERSION;


SqliteData::SqliteData()
    : SqliteData("", "")
{}

SqliteData::SqliteData(const SqliteConnector & sqliteConnector)
    : SqliteData(sqliteConnector.getDatabasePath(), 0)
{}

SqliteData::SqliteData(const std::string& databaseDir, const std::string& databaseName)
    : SqliteData(createDatabaseName(databaseDir, databaseName), 0)
{}

SqliteData::SqliteData(const string & databasePath, int /* Param used to avoid ambiguous override*/)
    : databasePath(databasePath)
    , database(databasePath.empty()? SqliteConnector::createInMemoryDb() : databasePath)
    , photoMarkerDAO(&database)
    , photoMetadataDAO(&database)
    , photoDAO(&database, &photoMarkerDAO, &photoMetadataDAO)
    , userDAO(&database)
    , sessionDAO(&database)
    , collectionDAO(&database)
    , contextDAO(&database)
    , eventsDAO(&database)
    , permissionDAO(&database)
    , configsDAO(&database)
    , databaseCreated(false)
    , daoCollection{
              &photoDAO, &photoMarkerDAO, &photoMetadataDAO
            , &userDAO, &sessionDAO, &collectionDAO, &contextDAO
            , &eventsDAO, &permissionDAO, &configsDAO}
{
    init();
}

void SqliteData::init(){
    if(databaseExist()){
        migration.migrate(getDatabaseConnector(), DB_VERSION);
        databaseCreated = true;
        return;
    }

    createDatabase();
}

std::string SqliteData::getDatabasePath() const{
    return databasePath;
}

PhotoDAO &SqliteData::photos(){
    return photoDAO;
}

UserDAO &SqliteData::users(){
    return userDAO;
}

SessionDAO &SqliteData::sessions(){
    return sessionDAO;
}

PhotoCollectionDAO &SqliteData::collections(){
    return collectionDAO;
}

PhotoContextDAO &SqliteData::contexts(){
    return contextDAO;
}

EventsDAO &SqliteData::events(){
    return eventsDAO;
}

PermissionsDAO & SqliteData::permissions(){
    return permissionDAO;
}

ConfigurationsDAO &SqliteData::configs(){
    return configsDAO;
}

SqlitePhotoMetadataDAO &SqliteData::getPhotoMetadataDAO(){
    return photoMetadataDAO;
}

SqlitePhotoMarkerDAO &SqliteData::getPhotoMarkerDAO(){
    return photoMarkerDAO;
}

SqlitePhotoCollectionMappingDAO &SqliteData::getCollectionMappingDAO(){
    return collectionDAO.collectionMappingDAO();
}

std::string SqliteData::createDatabaseName(
        const std::string& databaseDir,
        const std::string& databaseName, bool requireDirExist)
{
    if(requireDirExist){
        if(!Files().exists(databaseDir) || !Files().isDirectory(databaseDir)){
            string errMessage = string("Database dir '")
                                            .append(databaseDir)
                                            .append("' is not a valid directory.");
            throw CommonExceptions::IllegalArgumentException(errMessage);
        }
    }

    string dbName = databaseName.empty() ? Environment::instance().getDataFilenameDefault() : databaseName;
    string dataFilePath = Files().canonicalPath(Files().joinPaths(databaseDir, dbName));

    return dataFilePath;
}

bool SqliteData::databaseExist() const{
    if(databaseCreated){
        return true;
    }

    return readDatabaseVersion() > 0;
}


long SqliteData::readDatabaseVersion() const
{
    return migration.queryDatabaseVersion(getDatabaseConnector());
}

void SqliteData::writeDatabaseVersion(long dbVersion)
{
    migration.updateDbVersion(getDatabaseConnector(), dbVersion);
}

void SqliteData::createDatabase()
{
    try{
        database.beginTransaction();
        for(size_t i=0; i < daoCollection.size(); ++i){
            daoCollection[i]->createTableIfNotExist(this->database);
        }

        writeDatabaseVersion(DB_VERSION);

        database.commit();
        databaseCreated = true;
    }
    catch (const SqlException & e) {
        logError("Could not create database because of SqlError:\n" "%s", e.what());
        database.rollback();
        databaseCreated = false;
    }
}

SqliteConnector &SqliteData::getDatabaseConnector(){
    return this->database;
}
const SqliteConnector &SqliteData::getDatabaseConnector() const{
    return this->database;
}

bool SqliteData::allowTransactions() const
{
    return true;
}

bool SqliteData::isOnTransaction() const{
    return getDatabaseConnector().isOnTransaction();
}

void SqliteData::beginTransaction(TransactionType type)
{
    getDatabaseConnector().beginTransaction(type);
}

void SqliteData::commit()
{
    getDatabaseConnector().commit();
}

void SqliteData::rollback()
{
    getDatabaseConnector().rollback();
}

} /* namespace data */
} /* namespace calmframe */

