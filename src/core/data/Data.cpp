/*
 * Data.cpp
 *
 *  Created on: 29/09/2014
 *      Author: noface
 */

#include <data/Data.h>

#include "utils/IllegalStateException.h"
#include "utils/Log.h"

#include "DataHolder.h"

namespace calmframe {
namespace data {


DataHolder & Data::getHolder(){
    static DataHolder dataHolder;
    return dataHolder;
}

void Data::setup(DataBuilderPtr dataBuilder) {
    getHolder().setDataBuilder(dataBuilder);
}
Data & Data::instance(){
    return * getHolder().getData();
}

void Data::destroyInstance(){
    getHolder().destroyData();
}

} /* namespace data */
} /* namespace calmframe */

