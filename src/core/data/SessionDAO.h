#ifndef SESSIONDAO_H
#define SESSIONDAO_H

#include <vector>
#include <string>

namespace calmframe{
    class Session;
}

using calmframe::Session;

class SessionDAO {
public:
    virtual ~SessionDAO(){}
    virtual std::vector<Session> listAll()=0;
    virtual bool get(long sessionId, Session & sessionOut) const =0;
    virtual bool getByToken(const std::string & token, Session & sessionOut) const =0;
    virtual bool getUserSession(long userId, Session & sessionOut) const =0;
    virtual bool exist(long sessionId) const=0;
    virtual long insert(const Session & session) =0;
//    virtual long insert(const Session & session, long sessionId) =0;
    virtual int update(const Session & session) =0;
    virtual bool remove(const Session & session) =0;
    virtual int removeExpired() =0;
};

#endif // SESSIONDAO_H

