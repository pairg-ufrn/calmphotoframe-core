/*
 * PhotoDAO.h
 *
 *  Created on: 03/08/2014
 *      Author: noface
 */

#ifndef PHOTODAO_H_
#define PHOTODAO_H_

#include "model/photo/Photo.h"
#include <vector>
#include <set>
#include <string>

using calmframe::Photo;

class PhotoDAO {
public:
    typedef std::vector<Photo> PhotoList;

    virtual ~PhotoDAO(){}
    virtual PhotoList listAll(const std::string & query= std::string())=0;
    virtual PhotoList listIn(const std::set<long> & photoIds, const std::string & query=std::string())=0;
    virtual PhotoList listByContext(long contextId)=0;
	virtual bool getPhoto(long photoId, Photo & photoOut) =0;
    virtual bool exist(long photoId)=0;
    virtual long insert(const Photo & photo) =0;
    virtual long insert(const Photo & photo, long photoId) =0;
	virtual bool update(const Photo & photo) =0;
	virtual bool remove(const Photo & photo) =0;
	//TODO: rever método updatePhotoDescription
    virtual bool updatePhotoDescription(long photoId, const PhotoDescription & photoDescription) =0;
};

#endif /* PHOTODAO_H_ */
