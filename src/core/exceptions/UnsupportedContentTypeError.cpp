#include "UnsupportedContentTypeError.h"

#include "network/ContentType.h"

namespace calmframe {

using network::ContentType;

std::string UnsupportedContentTypeError::buildErrorMessage(
    const ContentType & content, const std::string & expectedType, const std::string & expectedSubtype)
{
    std::string message = 
        "Found content type '" + content.toString() + "' but expected: ";
        
    message += (expectedType.empty() ? std::string("*") : expectedType);
    message += "/";
    message += (expectedSubtype.empty() ? std::string("*") : expectedSubtype);
    
    return message;
}

void UnsupportedContentTypeError::checkType(const ContentType & contentType, const ContentType & expected){
    if(!expected.getType().empty() 
        && expected.getType() != contentType.getType()){
        throw UnsupportedContentTypeError(contentType, expected.getType(), expected.getSubtype());
    }
    if(!expected.getSubtype().empty() 
        && expected.getSubtype() != contentType.getSubtype())
    {
        throw UnsupportedContentTypeError(contentType, expected.getType(), expected.getSubtype());
    }
}

UnsupportedContentTypeError::UnsupportedContentTypeError(
    const network::ContentType & foundContent, const std::string & expectedType, const std::string & expectedSubtype)
    : super(buildErrorMessage(foundContent, expectedType, expectedSubtype))
{}

}//namespace
