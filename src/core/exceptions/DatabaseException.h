#ifndef DATABASEEXCEPTION_H
#define DATABASEEXCEPTION_H

#include "utils/Exception.hpp"

namespace calmframe {

EXCEPTION_CLASS(DatabaseException);
EXCEPTION_SUBCLASS(DatabaseCreationException, DatabaseException);

}//namespace

#endif // DATABASEEXCEPTION_H

