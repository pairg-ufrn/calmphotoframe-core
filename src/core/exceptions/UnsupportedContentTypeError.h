#ifndef INVALIDCONTENTTYPEEXCEPTION_H
#define INVALIDCONTENTTYPEEXCEPTION_H

#include "ApiExceptions.h"

namespace calmframe {
namespace network {
    class ContentType;
}//namespace
}//namespace

namespace calmframe {
    
    using network::ContentType;
    
    class UnsupportedContentTypeError : public ClientError{
    public:
        using super = ClientError;
        
        static std::string buildErrorMessage(const calmframe::network::ContentType & content, const std::string & expectedType, const std::string & expectedSubtype);
        
        static void checkType(const ContentType & contentType, const ContentType & expected);
        
        UnsupportedContentTypeError(
            const ContentType & foundContent, 
            const std::string & expectedType=std::string(), const std::string & expectedSubtype=std::string());
    };
}//namespace
#endif // INVALIDCONTENTTYPEEXCEPTION_H
