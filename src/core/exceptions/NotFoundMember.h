#ifndef NOTFOUNDMEMBER_H
#define NOTFOUNDMEMBER_H

#include "exceptions/ApiExceptions.h"

namespace calmframe {

EXCEPTION_SUBCLASS(NotFoundMember, ClientError);

}//namespace

#endif // NOTFOUNDMEMBER_H

