#ifndef APIEXCEPTIONS_H
#define APIEXCEPTIONS_H

#include "utils/Exception.hpp"

namespace calmframe {

EXCEPTION_CLASS(ClientError);
    
}//namespace

#endif // APIEXCEPTIONS_H
