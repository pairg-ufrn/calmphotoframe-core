#ifndef LOCKEDDATABASEEXCEPTION_H
#define LOCKEDDATABASEEXCEPTION_H

#include "DatabaseException.h"

namespace calmframe {

EXCEPTION_SUBCLASS(BusyDatabaseException, DatabaseException);

}//namespace

#endif // LOCKEDDATABASEEXCEPTION_H

