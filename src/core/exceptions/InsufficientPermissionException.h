#ifndef INSUFFICIENTPERMISSIONEXCEPTION
#define INSUFFICIENTPERMISSIONEXCEPTION

#include "utils/Exception.hpp"

namespace calmframe {

EXCEPTION_CLASS(InsufficientPermissionException);

}//namespace

#endif // INSUFFICIENTPERMISSIONEXCEPTION

