#ifndef INVALIDTOKENEXCEPTION_H
#define INVALIDTOKENEXCEPTION_H

#include "utils/Exception.hpp"

namespace calmframe {

EXCEPTION_CLASS(InvalidTokenException);

}//namespace

#endif // INVALIDTOKENEXCEPTION_H

