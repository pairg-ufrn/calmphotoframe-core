#include "JsonConfigurationLoader.h"

#include "json/json.h"
#include <fstream>

namespace calmframe{
using namespace std;

JsonConfigurationLoader::JsonConfigurationLoader(const bool &_overrideOption)
    : allowOptionOverride(_overrideOption)
{}

bool JsonConfigurationLoader::getAllowOptionOverride() const{
    return allowOptionOverride;
}
void JsonConfigurationLoader::setAllowOptionOverride(bool value){
    allowOptionOverride = value;
}

void JsonConfigurationLoader::loadFromFile(const std::string &filename, Configurations &configurations){
    ifstream inputFile;
    inputFile.open(filename.c_str(), ios_base::in);
    if(inputFile.good()){
        load(inputFile, configurations);
    }
}

void JsonConfigurationLoader::load(std::istream & jsonInput, Configurations &configurations){
    Json::Reader reader;
    Json::Value configs;
    if(reader.parse(jsonInput, configs)){
        Json::Value::iterator iter = configs.begin();
        while(iter != configs.end()){
            if(getAllowOptionOverride() == true){
                configurations.put(iter.name(), (*iter).asString());
            }
            else{
                configurations.putIfNotExists(iter.name(), (*iter).asString());
            }
            ++iter;
        }
    }
}

} //namespace calmframe
