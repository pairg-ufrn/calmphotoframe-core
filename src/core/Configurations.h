/*
 * Configurations.h
 *
 *  Created on: 19/11/2014
 *      Author: noface
 */

#ifndef CONFIGURATIONS_H_
#define CONFIGURATIONS_H_

#include <map>
#include <string>

namespace calmframe {

class Configurations {
public:
	static Configurations & instance();
    typedef std::map<std::string, std::string> ConfigurationsMap;
    typedef ConfigurationsMap::const_iterator const_iterator;
    typedef ConfigurationsMap::iterator       iterator;
private:
    ConfigurationsMap configurations;
public:
    Configurations();
    virtual ~Configurations();
public:
    void put(const std::string & key, const std::string & value);
    void put(const std::string & key, bool value);
    void put(const std::string & key, int value);
    void put(const std::string & key, long value);
    void put(const std::string & key, double value);
    
    void putIfNotExists(const std::string & key, const std::string & value);
    const std::string & get(const std::string & key, const std::string & defaultValue=std::string()) const;
    bool   getBoolean(const std::string & key, const bool   & defaultValue=false) const;
    int    getInteger(const std::string & key, const int    & defaultValue=0) const;
    long   getLong   (const std::string & key, const long   & defaultValue=0l) const;
    double getDouble (const std::string & key, const double & defaultValue=0.0) const;

    bool exists(const std::string & key) const;
public:
    const_iterator begin() const;
    const_iterator end() const;
    iterator begin();
    iterator end();
};

} /* namespace calmframe */

#endif /* CONFIGURATIONS_H_ */
