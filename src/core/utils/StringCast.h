/*
 * NumberUtils.h
 *
 *  Created on: 07/10/2014
 *      Author: noface
 */

#ifndef STRCASTUTILS_H_
#define STRCASTUTILS_H_

#include <string>
#include <stdint.h>

namespace calmframe {
namespace utils {

class StringCast {
public:
    static const StringCast & instance(){
        static StringCast singleton;
        return singleton;
    }
    static const StringCast & cast(){
        return instance();
    }
    StringCast() {}
    virtual ~StringCast() {}

    bool isNumber(const std::string & str) const;

    std::string toString(long number) const;
    std::string toString(int number) const;
    std::string toString(unsigned number) const;
    std::string toString(double number) const;
    const std::string & toString(const std::string & str) const;

    int   toInt(const std::string & string) const;
    long  toLong(const std::string & string) const;
    float toFloat(const std::string & string) const;
    bool  toBoolean(const std::string & str, bool useBoolAlpha=true) const;

    int     tryToInt   (const std::string & str, int     defaultValue=0    ) const;
    int64_t tryToLong  (const std::string & str, int64_t defaultValue=0l   ) const;
    float   tryToFloat (const std::string & str, float   defaultValue=0.0f ) const;
    double  tryToDouble(const std::string & str, double  defaultValue=0.0  ) const;
    bool    tryToBool  (const std::string & str, bool    defaultValue=false, bool useBoolAlpha=true) const;

    bool tryToLong (long & value, const std::string & str) const;
};

const StringCast & StrCast();

} /* namespace utils */
} /* namespace calmframe */

#endif /* STRCASTUTILS_H_ */
