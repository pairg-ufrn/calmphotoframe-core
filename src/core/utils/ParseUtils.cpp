#include "ParseUtils.h"

#include <sstream>

namespace calmframe {

using namespace std;

ParseUtils &ParseUtils::instance()
{
    static ParseUtils singleton;
    return singleton;
}

ParseUtils::ParseUtils()
{

}

ParseUtils::~ParseUtils()
{

}

bool ParseUtils::toBoolean(const std::string &str, const bool &defaulValue) const{
    bool value;
    stringstream sstream;
    sstream << str;
    sstream >> std::boolalpha >> value;

    if(sstream.fail()){
        value = defaulValue;
    }

    return value;
}

}//namespace

