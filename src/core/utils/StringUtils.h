/*
 * StringUtils.h
 *
 *  Created on: 18/09/2014
 *      Author: noface
 */

#ifndef STRINGUTILS_H_
#define STRINGUTILS_H_

#include <string>
#include <vector>
#include <sstream>

namespace calmframe {
namespace utils {

class StringUtils {
public:
	static const StringUtils & instance(){
		static StringUtils singleton;
		return singleton;
    }
	static const unsigned SPLIT_IGNORE_EMPTY;
	static const unsigned SPLIT_TRIM;
public:
	StringUtils(){}
	virtual ~StringUtils(){}

    bool stringStartsWith(std::string str, std::string part) const;
    bool stringEndsWith(std::string str, std::string part) const;

    template<typename Iterator>
    std::string join(const Iterator & begin, const Iterator & end, const std::string & separator) const;

    std::vector<std::string> split(const std::string & str, char separator, unsigned options = 0) const;
	bool split(const std::string & str, char separator
			, std::string & partOne, std::string & partTwo, unsigned options = 0) const;
	bool split(const std::string & str, char separator
			, std::string * partOne, std::string * partTwo, unsigned options = 0) const;


    std::vector<std::string> splitAndTrim(const std::string & str, char separator, unsigned options = 0) const;

    bool splitAndTrim(const std::string & str, char separator
            , std::string & partOne, std::string & partTwo) const;
    bool splitAndTrim(const std::string & str, char separator
            , std::string * partOne, std::string * partTwo) const;

	std::string & trim (std::string & str) const;
	std::string & ltrim(std::string & str) const;
    std::string & rtrim(std::string & str) const;

    bool caseInsensitiveEquals(const std::string & str1, const std::string & str2) const;

    int countOcurrences(const std::string & str, const std::string & substring) const;

    template<typename Collection>
    std::string join(const std::string & separator, Collection&& coll) const;
};

template<typename Collection>
std::string StringUtils::join(const std::string & separator, Collection && coll) const{
    std::stringstream out;
    bool first = true;

    for(auto&& item : coll){
        if(!first){
            out << separator;
        }

        out << item;

        first = false;
    }

    return out.str();
}

template<typename Iterator>
std::string StringUtils::join(const Iterator &begin, const Iterator &end, const std::string &separator) const
{
    std::string result;
    for(Iterator iter = begin; iter != end; ++iter){
        if(iter != begin){
            result += separator;
        }
        result += *iter;
    }
    return result;
}

} /* namespace utils */
} /* namespace calmframe */

#endif /* STRINGUTILS_H_ */
