#include "Log.h"
#include "Environment.h"
#include "utils/NullPointerException.hpp"

#include "StringUtils.h"

#include "Poco/Logger.h"
#include "Poco/Channel.h"
#include "Poco/AutoPtr.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/PatternFormatter.h"
#include "Poco/FormattingChannel.h"
//#include "Poco/SimpleFileChannel.h"
#include "Poco/FileChannel.h"
#include "Poco/SplitterChannel.h"

#include <iostream>
#include <map>
#include <memory>

using namespace std;
using namespace Poco;
using namespace calmframe::utils;

namespace calmframe {


static LogLevel::Level convertFromPoco(int logLevel){
    switch (logLevel) {
    case Message::PRIO_FATAL :
        return LogLevel::Quiet;
    case Message::PRIO_CRITICAL :
    case Message::PRIO_ERROR :
        return LogLevel::Error;
    case Message::PRIO_WARNING :
        return LogLevel::Warning;
    case Message::PRIO_NOTICE :
    case Message::PRIO_INFORMATION :
        return LogLevel::Debug;
    case Message::PRIO_DEBUG :
    case Message::PRIO_TRACE :
    default:
        return LogLevel::Debug;
    }
}

static int convertToPocoLevel(const LogLevel::Level & logLevel){
    switch (logLevel) {
    case LogLevel::Quiet:
        return Message::PRIO_FATAL;
        
    case LogLevel::Error:
        return Message::PRIO_ERROR;

    case LogLevel::Warning:
        return Message::PRIO_WARNING;

    case LogLevel::Information:
        return Message::PRIO_INFORMATION;

    case LogLevel::Debug:
    default:
        return Message::PRIO_DEBUG;
    }
}

namespace Impl {

/** Pattern: <msg priority> [<hora>:<minuto>:<segundo>]: <texto>
    Ex.: I [12:34:56]: Log de exemplo
*/
const char * logPattern = "%q [%H:%M:%S]: %t";

class ChannelWrapper : public Poco::Channel{
public:
    LogChannel * delegate;
    
    ChannelWrapper(LogChannel * delegate)
        : delegate(delegate)
    {}
    
    virtual void log(const Message& msg){
        LogLevel::Level level = convertFromPoco((int)msg.getPriority());
        
        delegate->log(level, msg.getText());
    }
};

class ChannelHolder{
private:
    //TODO: use shared_ptr instead of AutoPtr
    
    AutoPtr<ConsoleChannel> consoleChannel;
    AutoPtr<SplitterChannel> splitterChannel;
    AutoPtr<PatternFormatter> patternFormatter;
    AutoPtr<FormattingChannel> formattingChannel;
    AutoPtr<Channel> channel;
    typedef map<string, AutoPtr<Channel> > ChannelsMap;
    ChannelsMap fileChannels;
    
    using CustomChannelsMap = map<LogChannel *, std::shared_ptr<Channel>>;
    CustomChannelsMap customChannels;
    
    bool consoleEnabled;
public:
    ChannelHolder()
        : consoleEnabled(true)
    {
        reset();
    }

    void reset(){
        consoleChannel.assign(new ConsoleChannel());
        splitterChannel.assign(new SplitterChannel());
        
        if(consoleEnabled){
            splitterChannel->addChannel(consoleChannel.get());
        }
        
        patternFormatter.assign(new PatternFormatter(logPattern));
        patternFormatter->setProperty("times", "local");
        formattingChannel.assign(new FormattingChannel(patternFormatter, splitterChannel));
        channel.assign(formattingChannel);

        this->fileChannels.clear();
    }

    void addLogFile(const string & key, const string & filename){
//        fileChannels[key] = new SimpleFileChannel(filename);
        FileChannel * channel = new FileChannel(filename);
        channel->setProperty("rotation", "daily");

        fileChannels[key] = channel;
        splitterChannel->addChannel(fileChannels[key]);
    }
    void removeLogFile(const string & key){
        ChannelsMap::iterator found = fileChannels.find(key);
        if(found != fileChannels.end()){
            AutoPtr<Channel>  & ptr = found->second;
            ptr.assign(NULL);
            fileChannels.erase(found);
        }
    }

    AutoPtr<Channel> & getChannel(){
        return channel;
    }
    
    void setStdoutEnabled(bool enabled){
        this->consoleEnabled = enabled;
        
        this->splitterChannel->removeChannel(consoleChannel.get());
        if(enabled){
            this->splitterChannel->addChannel(consoleChannel.get());
        }
    }
    
    void addChannel(LogChannel * channel){
        auto iter = this->customChannels.find(channel);
        if(iter == customChannels.end()){
            auto channelWrapper = std::make_shared<ChannelWrapper>(channel);
            this->customChannels[channel] = channelWrapper;
            
            this->splitterChannel->addChannel(channelWrapper.get());
        }
    }

    void removeChannel(LogChannel * channel){
        auto iter = this->customChannels.find(channel);
        if(iter != customChannels.end()){
            this->splitterChannel->removeChannel(iter->second.get());
            
            this->customChannels.erase(iter);
        }
    }
};

}//namespace

Log &MainLog(){
    return Log::instance();
}

Log &Log::instance(){
    static Log singleton;
    return singleton;
}

Log::Log(const std::string &name)
    : name(name)
    , channelHolder(new Impl::ChannelHolder())
{
    getLogger().setChannel(channelHolder->getChannel());
}

Log::~Log()
{
    if(channelHolder != NULL){
        delete channelHolder;
        channelHolder = NULL;
    }
}

LogLevel::Level Log::getLogLevel() const{
    return convertFromPoco(getLogger().getLevel());
}

void Log::setLogLevel(const LogLevel::Level &logLevel){
    getLogger().setLevel(convertToPocoLevel(logLevel));
}

std::string &Log::getName(){
    return name;
}
const std::string &Log::getName() const{
    return name;
}

Logger &Log::getLogger(){
    return Logger::get(getName());
}
const Logger &Log::getLogger() const{
    return Logger::get(getName());
}

void Log::debug(const string &text){
    getLogger().debug(text);
}

void Log::info(const std::string &text){
    getLogger().information(text);
}

void Log::warning(const std::string &text){
    getLogger().warning(text);
}

void Log::error(const std::string &text){
    getLogger().error(text);
}

void Log::resetSetup()
{
    assertChannelHolderNotNull();
    channelHolder->reset();
    this->getLogger().setChannel(channelHolder->getChannel());
}

void Log::addLogFile(const string &name, const string &filepath){
    assertChannelHolderNotNull();
    channelHolder->addLogFile(name, filepath);
}

void Log::removeLogFile(const string &name){
    assertChannelHolderNotNull();
    channelHolder->removeLogFile(name);
}

void Log::addChannel(LogChannel * channel)
{
    channelHolder->addChannel(channel);
}

void Log::removeChannel(LogChannel * channel)
{
    channelHolder->removeChannel(channel);
}

void Log::setStdoutEnabled(bool enabled){
    channelHolder->setStdoutEnabled(enabled);
}

void Log::assertChannelHolderNotNull(){
    if(this->channelHolder == NULL){
        throw NullPointerException("calmframe::Log : Channel Holder is NULL.");
    }
}

static bool iEquals(const std::string & str1, const std::string & str2){
    return StringUtils::instance().caseInsensitiveEquals(str1, str2);
}

namespace LogLevelStr {
    constexpr const char * const Null        = "Null"; 
    constexpr const char * const Debug       = "Debug";
    constexpr const char * const Information = "Information";
    constexpr const char * const Warning     = "Warning";
    constexpr const char * const Error       = "Error";
    constexpr const char * const Quiet       = "Quiet";
}//namespace

LogLevel::Level LogLevel::fromString(const string & str, LogLevel::Level defaultLevel){

    if(iEquals(str, LogLevelStr::Debug)){
        return LogLevel::Debug;
    }
    else if(iEquals(str, LogLevelStr::Information)){
        return LogLevel::Information;
    }
    else if(iEquals(str, LogLevelStr::Warning)){
        return LogLevel::Warning;
    }
    else if(iEquals(str, LogLevelStr::Error)){
        return LogLevel::Error;
    }
    else if(iEquals(str, LogLevelStr::Quiet)){
        return LogLevel::Quiet;
    }

    return defaultLevel;
}

string LogLevel::toString(LogLevel::Level logLevel){
    switch (logLevel) {
    case calmframe::LogLevel::Null:
        return LogLevelStr::Null;
    case calmframe::LogLevel::Debug:
        return LogLevelStr::Debug;
    case calmframe::LogLevel::Information:
        return LogLevelStr::Information;
    case calmframe::LogLevel::Warning:
        return LogLevelStr::Warning;
    case calmframe::LogLevel::Error:
        return LogLevelStr::Error;
    case calmframe::LogLevel::Quiet:
        return LogLevelStr::Quiet;
    default:
        return std::string();
    }
}

}//namespace
