#ifndef IOEXCEPTION_H
#define IOEXCEPTION_H

#include "Exception.hpp"

namespace CommonExceptions {
    EXCEPTION_CLASS(IOException);
}//namespace

#endif // IOEXCEPTION_H
