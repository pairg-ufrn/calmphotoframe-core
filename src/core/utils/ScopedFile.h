#ifndef SCOPEDFILE_H
#define SCOPEDFILE_H

#include <string>
#include "utils/FileUtils.h"
#include "utils/Log.h"

class ScopedFile{
private:
    std::string filepath;
    bool recursive;
public:
    ScopedFile()
        : filepath()
        , recursive(false)
    {}
    ScopedFile(const std::string & filePath, bool recursiveRemove=false)
        : filepath(filePath)
        , recursive(recursiveRemove)
    {}
    ~ScopedFile(){
        if(!filepath.empty() && calmframe::utils::Files().exists(filepath)){
            if(!calmframe::utils::Files().remove(filepath, recursive)){
                logWarning("ScopedFile::destructor:\t"
                           "failed to remove path '%s'", filepath.c_str());
            }
        }
    }

    const std::string & getFilepath() const{
        return filepath;
    }
};

#endif // SCOPEDFILE_H

