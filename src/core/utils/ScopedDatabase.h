#ifndef TEMPORARYDATABASE_H
#define TEMPORARYDATABASE_H

#include "data/Data.h"
#include "data/data-sqlite/SqliteDataBuilder.hpp"
#include "utils/ScopedFile.h"
#include "utils/FileUtils.h"

#include <string>
#include <memory>

class ScopedSqliteDatabase{
public:
    enum class DbType{InFile, InMemory};
private:
    std::shared_ptr<calmframe::data::DataBuilder> dataBuilder;
    ScopedFile scopedDbFile;
    DbType dbType;
public:
    ScopedSqliteDatabase(const std::string & dbDir=calmframe::utils::Files().getDefaultTmpDir(),
                    const std::string & dbName="", bool requireDirExist=true);
    /** Create a sqlite db of given type and on specified filepath (ignored if is a in memory database).*/
    ScopedSqliteDatabase(DbType dbType, const std::string & filepath = std::string());

    ~ScopedSqliteDatabase();

    const std::string & getFilepath() const;
    calmframe::data::DataBuilder & getDataBuilder();
    DbType getDbType();
};

#endif // TEMPORARYDATABASE_H
