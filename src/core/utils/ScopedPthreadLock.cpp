#include "ScopedPthreadLock.h"

#include "PthreadMutex.h"


calmframe::ScopedPthreadLock::ScopedPthreadLock(calmframe::PthreadMutex & mutex)
    : mutex(&mutex)
{
    this->mutex->lock();
}

calmframe::ScopedPthreadLock::~ScopedPthreadLock(){
    this->mutex->unlock();
}
