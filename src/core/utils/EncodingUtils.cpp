#include "EncodingUtils.h"

#include "Poco/MD5Engine.h"
#include "Poco/SHA1Engine.h"

using Poco::MD5Engine;
using Poco::SHA1Engine;
using Poco::DigestEngine;

EncodingUtils &EncodingUtils::instance(){
    static EncodingUtils singleton;
    return singleton;
}

EncodingUtils::EncodingUtils()
{

}

EncodingUtils::~EncodingUtils()
{

}

template<typename EngineType>
std::string generateHash(const std::string &str){
    EngineType engine;
    engine.update(str);

    return DigestEngine::digestToHex(engine.digest());
}

std::string EncodingUtils::generateMD5(const std::string &str) const{
    return generateHash<MD5Engine>(str);
}
std::string EncodingUtils::generateSHA1(const std::string &str) const{
    return generateHash<SHA1Engine>(str);
}

