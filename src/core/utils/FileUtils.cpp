/*
 * FileUtils.cpp
 *
 *  Created on: 14/10/2014
 *      Author: noface
 */

#include "FileUtils.h"

#include "Poco/Path.h"
#include "Poco/File.h"
#include "Poco/Glob.h"
#include "Poco/TemporaryFile.h"
#include "utils/Exception.hpp"
#include "utils/IOException.h"
#include "utils/StringUtils.h"
#include "utils/Log.h"

#include <string>
#include <unistd.h>


namespace calmframe {
namespace utils {

using namespace Poco;
using namespace std;
using namespace CommonExceptions;

FileUtils & Files(){
    return FileUtils::instance();
}

std::string FileUtils::getDefaultTmpDir() const{
    return defaultTmpDir;
}
void FileUtils::setDefaultTmpDir(const std::string &tmpDir){
    defaultTmpDir = tmpDir;
}

string FileUtils::getCurrentDir() const{
    return Path::current();
}

bool FileUtils::changeCurrentDir(const string &path) const{
    return chdir(path.c_str()) == 0;
}

char FileUtils::getPathSeparator(){
    return Path::separator();
}

FileUtils::FileUtils()
    :defaultTmpDir(Path::temp())
{}

bool FileUtils::isDirectory(const string &path) const{
    if(isAValidPath(path)){
        File dir(path);
        return dir.exists() && dir.isDirectory();
    }
    return false;
}
bool FileUtils::isFile(const string &path) const{
    if(isAValidPath(path)){
        File file(path);
        return file.exists() && file.isFile();
    }
    return false;
}

bool FileUtils::isFilePath(const string &path) const{
    return Path(path).isFile();
}

bool FileUtils::isAbsolute(const string &path) const{
    return Path(path).isAbsolute();
}

bool FileUtils::isRelative(const string &path) const
{
    return Path(path).isRelative();
}

bool FileUtils::isSubpath(const string &subpath, const string &parentPath) const{
    string canonicalSubPath    = canonicalPath(subpath);
    string canonicalParentPath = canonicalPath(parentPath);
    return StringUtils::instance().stringStartsWith(canonicalSubPath,canonicalParentPath);
}

void FileUtils::createDirIfNotExists(const string &dirPath, bool createParents){
    createDirImpl(dirPath, createParents, false);
}

void FileUtils::createDir(const string & dirPath, bool createParents){
    createDirImpl(dirPath, createParents, true);
}

void FileUtils::createDirImpl(const string & dirPath, bool createParents, bool throwIfExist){
    bool created = false;
    
    try{
        Path path{dirPath};
        File dir{path};
        
        if(createParents){
            File parentDir{path.parent()};
            parentDir.createDirectories();
        }
        
        created = dir.createDirectory();
    }
    catch(...){
        throw IOException("In FileUtils::createDirIfNotExists, could not create directory \"" + dirPath + "\"");
    }
    
    if(!created && throwIfExist){
        throw IOException("Directory already exist");
    }
}

string FileUtils::extractFilename(const string &filepath) const{
    size_t lastPathSepPos = filepath.rfind(Poco::Path::separator());
    return lastPathSepPos != string ::npos
                                           ? filepath.substr(lastPathSepPos + 1)
                                           : filepath;
}

string FileUtils::getFilenameExtension(const string &filepath) const{
    if(filepath.empty()){
        return string();
    }
    
    size_t lastSeparator = filepath.rfind(Path::separator());

    string filename = filepath;
    if(lastSeparator < filepath.size() - 1){
        filename = filepath.substr(lastSeparator + 1);
    }

    size_t dotPos  = filename.rfind(".");
    if(dotPos >= filename.size()){
        return string();
    }
    else{
        return filename.substr(dotPos + 1);
    }
}

string FileUtils::getFilenameWithoutExtension(const string &filepath) const
{
    string filenameWithoutExtension;
    splitFilenameAndExtensionImpl(filepath, &filenameWithoutExtension, NULL);

    return filenameWithoutExtension;
}
void FileUtils::splitFilenameAndExtension(const string &filepath, string & filenameOut, string & extensionOut) const{
    splitFilenameAndExtensionImpl(filepath, &filenameOut, &extensionOut);
}
void FileUtils::splitFilenameAndExtensionImpl(const string &filepath, string *filenameOut, string *extensionOut) const{
    string filenameWithoutExt = extractFilename(filepath);
    if(extensionOut != NULL){
        *extensionOut = getFilenameExtension(filepath);
    }
    //Remove extensão de filenameOut
    string::size_type dotPos = filenameWithoutExt.rfind('.');
    if(dotPos != string::npos){
        filenameWithoutExt.erase(dotPos);
    }
    if(filenameOut != NULL){
        *filenameOut = filenameWithoutExt;
    }
}

string FileUtils::joinPaths(const string &parentPath, const string &subpath) const{
    return Poco::Path(parentPath,subpath).toString();
}

std::string FileUtils::absolutePath(const std::string& relativePath) const{
    Path path(relativePath);
    return path.makeAbsolute().toString();
}

string FileUtils::absolutePath(const string &relativePath, const string &parent) const
{
    Path path(relativePath, parent);
    return path.makeAbsolute().toString();
}

//string FileUtils::relativePath(const string &pathStr, const string &basePathStr) const
//{
//    Poco::Path path    (Path::expand(pathStr));
//    Poco::Path basePath(Path::expand(basePathStr));

//    path.makeAbsolute();
//    basePath.makeAbsolute();

//    bool hasSameRoot = path.getDevice() == basePath.getDevice()
//                        && path.getNode() == basePath.getNode();
//    if(hasSameRoot){
//        //Caminhos que não terminam com '/' consideram o último nó como o nome do arquivo
//        //mesmo que ele seja um diretório. Assim, acrescenta mais 1 para estes casos
//        int pathLength      = !path.getFileName().empty() ? path.depth() +1 : path.depth();
//        int basePathLength  = !basePath.getFileName().empty() ? basePath.depth() + 1 : basePath.depth();

//        int commonCount = 0;
//        for(int i=0; i < pathLength && i < basePathLength; ++i){
//            if(path[i] != basePath[i]){
//                break;
//            }
//            ++commonCount;
//        }

//        Path relativePath;
//        relativePath.setDevice(path.getDevice());
//        relativePath.setNode(path.getNode());

//        int notCommon = basePathLength - commonCount;

//        for(int i=0; i < notCommon; ++i){
//            relativePath.append("..");
//        }
//        for(int i=commonCount; i < pathLength; ++i){
//            relativePath.append(path[i]);
//        }
//        relativePath.setFileName(path.getFileName());

//        return relativePath.toString();
//    }

//    return path.toString();
//}

string FileUtils::canonicalPath(const string& strPath) const{
    Path path(Path::expand(strPath));
    path.makeAbsolute();
    
    Path normalized{true};
    
    for(int i=0; i <= path.depth(); ++i){
        const auto & segment = path[i];
        if(segment == ".."){
            normalized.popDirectory();
        }
        else if(!segment.empty() && segment != "."){
            normalized.pushDirectory(segment);
        }
    }

    auto pathStr = normalized.toString();   
    
    if(pathStr.size() > 1 && pathStr.back() == Path::separator()){
        pathStr.pop_back();
    } 
    
    return pathStr;
}

string FileUtils::getPathFromBase(const string &path, const string &basePath) const{
    if(isAbsolute(path)){
        return path;
    }
    return joinPaths(basePath, path);
}

bool FileUtils::pathEquals(const string & strPath1, const string &strPath2) const{
    return canonicalPath(strPath1) == canonicalPath(strPath2);
}
bool FileUtils::isAValidPath(const string &filePath) const{
    if(filePath.empty()){
        return false;
    }

    Path path;
    return path.tryParse(filePath);
}

bool FileUtils::exists(const std::string& filePath) const {
    if(isAValidPath(filePath)){
        return File(filePath).exists();
    }
    return false;
}

bool FileUtils::remove(const std::string& filePath, bool recursiveRemove) const {
	File file(filePath);
    if(file.exists()){
        file.remove(recursiveRemove);
    }
    return !file.exists();
}

bool FileUtils::copyFile(const string &originPath, const string &dstPath) const{
    File file(originPath);
    if(file.exists()){
        file.copyTo(dstPath);
        return exists(dstPath);
    }
    return false;
}

long FileUtils::getFileSize(const string &filepath) const{
    File file(filepath);
    if(file.exists()){
        return file.getSize();
    }
    return -1;
}

std::set<string> FileUtils::findFiles(const string &globPattern) const{
    std::set<string> files;
    Glob::glob(globPattern, files);

    return files;
}

void FileUtils::listFiles(const string &path, std::vector<string> &files) const{
    File dir(path);
    dir.list(files);
}

string FileUtils::copyTmpFile(const string &pathToCopy, const string &dir) const{
    File fileToCopy(pathToCopy);
    if(!fileToCopy.exists() || fileToCopy.isDirectory()){
        return string();
    }

    TemporaryFile tmpFile;
    if(!dir.empty()){
       tmpFile = TemporaryFile(dir);
    }
    tmpFile.keepUntilExit();
    tmpFile.createFile();

    fileToCopy.copyTo(tmpFile.path());

    return tmpFile.path();
}

string FileUtils::generateUniqueFilepath(const string &dir) const{
    return genUniqueFilepathImpl(dir);
}

string FileUtils::generateTmpFilepath(const string &dir) const{
    return genUniqueFilepathImpl(dir);
}

string FileUtils::genUniqueFilepathImpl(const string &dir) const
{
    string dirPath = (dir.empty()
                            ? this->getDefaultTmpDir()
                            : dir);
    if(dirPath.empty() ==false && dirPath.at(dirPath.size() - 1) != '/'){
        dirPath.append("/");
    }
    return TemporaryFile::tempName(dirPath);
}

} /* namespace utils */
} /* namespace calmframe */

