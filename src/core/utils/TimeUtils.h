#ifndef TIMEUTILS_H
#define TIMEUTILS_H

#include <string>
#include <ctime>

#define HTTP_TIME_FORMAT "%a, %d %b %Y %T %Z"

namespace TimeUtilsConstants{
    static const int defaultMaxDatetimeLenght = 80;
    static const char * defaultFormat = "YYYY-MM-DD HH:MM:SS";
}

class TimeUtils{
public:
    static TimeUtils & instance(){
        static TimeUtils singleton;
        return singleton;
    }

    long getTimestamp() const{
        return std::time(0);
    }
    std::string getCurrentHttpDateTime(){
        time_t rawtime;
        struct tm * timeinfo;
        const int bufferSize = 80;
        char buffer [bufferSize];

        time (&rawtime);
        timeinfo = gmtime (&rawtime);

        strftime (buffer,bufferSize,HTTP_TIME_FORMAT,timeinfo);
        std::string timeStr((const char *)&buffer);

        return timeStr;
    }
    std::string getCurrentDateTime(const std::string & format = TimeUtilsConstants::defaultFormat,
                                   int maxStringSize = TimeUtilsConstants::defaultMaxDatetimeLenght){
        time_t rawtime;
        struct tm * timeinfo;
        const int bufferSize = maxStringSize;
        char buffer [bufferSize];

        time (&rawtime);
        timeinfo = gmtime (&rawtime);

        strftime (buffer,bufferSize,format.c_str(),timeinfo);
        std::string timeStr((const char *)&buffer);

        return timeStr;
    }
};

#endif // TIMEUTILS_H
