/*
 * StringUtils.cpp
 *
 *  Created on: 18/09/2014
 *      Author: noface
 */

#include "utils/StringUtils.h"
#include "Poco/StringTokenizer.h"
#include "Poco/String.h"
#include <vector>
#include <cctype>

#include "streamprintf.h"

using namespace std;
using Poco::StringTokenizer;

namespace calmframe {
namespace utils {

const unsigned StringUtils::SPLIT_IGNORE_EMPTY = StringTokenizer::TOK_IGNORE_EMPTY;
const unsigned StringUtils::SPLIT_TRIM = StringTokenizer::TOK_TRIM;

bool StringUtils::stringStartsWith(std::string str, std::string part) const {
	//return (other.compare(0, other.size(),str) == 0);
    return (str.compare(0, part.size(),part) == 0);
}

bool StringUtils::stringEndsWith(string str, string part) const
{
    return str.size() >= part.size() &&
            str.compare(str.size() - part.size(),part.size(), part) == 0;
}

std::vector<std::string> calmframe::utils::StringUtils::split(const std::string& str, char separator, unsigned options) const{
	vector<string> tokens;
	string strSeparator;
	strSeparator.push_back(separator);
	StringTokenizer tokenizer(str, strSeparator, options);

	for(size_t i =0; i < tokenizer.count(); ++i){
		tokens.push_back(tokenizer[i]);
	}
	return tokens;
}

bool StringUtils::split(const std::string& str, char separator
		, std::string & partOne, std::string & partTwo, unsigned options) const
{
	return split(str,separator,&partOne,&partTwo,options);
}
bool StringUtils::split(const std::string& str, char separator
		, std::string* partOne, std::string* partTwo, unsigned options) const
{
	std::size_t separatorIndex = str.find(separator);

	//Faz isso para tratar o caso em que partOne ou partTwo é igual a str
	std::string leftSize, rightSize;

	if (separatorIndex==std::string::npos){
		//Se não encontrou, atribui tudo à esquerda
		leftSize = str;
	}
	else{
		//Faz isso para tratar o caso em que partOne ou partTwo é igual a str
		leftSize.assign(str.substr(0,separatorIndex));
		rightSize.assign(str.substr(separatorIndex+1, string::npos));
	}

	if(partOne != NULL){
		//Atribui à string todos os caracteres anteriores à 'separatorIndex'
		partOne->assign(leftSize);
		if(options & SPLIT_TRIM){
			trim(*partOne);
		}
	}
	if(partTwo != NULL){
		//Atribui à segunda string todos os caracteres após 'separatorIndex'
		partTwo->assign(rightSize);
		if(options & SPLIT_TRIM){
			trim(*partTwo);
		}
	}
	return separatorIndex != std::string::npos;
}

std::vector<string> StringUtils::splitAndTrim(const string &str, char separator, unsigned options) const
{
    return split(str,separator, SPLIT_IGNORE_EMPTY | SPLIT_TRIM | options);
}

bool StringUtils::splitAndTrim(const string &str, char separator, string &partOne, string &partTwo) const
{
    return split(str,separator,partOne, partTwo, SPLIT_IGNORE_EMPTY | SPLIT_TRIM);
}


std::string& StringUtils::trim(std::string& str) const{
    return rtrim(ltrim(str));
}

std::string& StringUtils::ltrim(std::string& str) const{
    size_t countToErase = 0;
    while(countToErase < str.length() && isspace(str[countToErase])){
		++countToErase;
	}
	if(countToErase > 0){
		str.erase(0,countToErase);
	}
	return str;
}

std::string& StringUtils::rtrim(std::string& str) const{
	size_t countToErase = 0;
	size_t len = str.length();
	int index = len-1;
	while(index >= 0 && isspace(str[index])){
		++countToErase;
		--index;
	}
	if(countToErase > 0){
		str.erase(len - countToErase,countToErase);
	}
    return str;
}

bool StringUtils::caseInsensitiveEquals(const string &str1, const string &str2) const
{
    return Poco::icompare(str1, str2) == 0;
}

int StringUtils::countOcurrences(const string & str, const string & substr) const{
    //Code adapted from: https://dzone.com/articles/c-string-count-substring

    int count=0;
    string::size_type pos = str.find(substr);

    while(pos!=string::npos){
        count++;
        pos = str.find(substr,pos+1);
    }

    return count;
}

bool calmframe::utils::StringUtils::splitAndTrim(const std::string &str, char separator, std::string *partOne, std::string *partTwo) const
{
    return split(str,separator,partOne, partTwo, SPLIT_IGNORE_EMPTY | SPLIT_TRIM);
}

} /* namespace utils */
} /* namespace calmframe */
