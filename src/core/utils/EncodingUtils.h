#ifndef ENCODINGUTILS_H
#define ENCODINGUTILS_H

#include <string>

class EncodingUtils
{
public:
    static EncodingUtils & instance();
public:
    EncodingUtils();
    ~EncodingUtils();

    std::string generateMD5(const std::string & str) const;
    std::string generateSHA1(const std::string &str) const;
};

#endif // ENCODINGUTILS_H
