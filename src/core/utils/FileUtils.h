/*
 * FileUtils.h
 *
 *  Created on: 14/10/2014
 *      Author: noface
 */

#ifndef FILEUTILS_H_
#define FILEUTILS_H_

#include <string>
#include <set>
#include <vector>

namespace calmframe {
namespace utils {
class FileUtils;

FileUtils & Files();

class FileUtils {
public:
	static FileUtils & instance(){
		static FileUtils singleton;
		return singleton;
	}
    static char getPathSeparator();
private:
    std::string defaultTmpDir;
public:
    FileUtils();
    virtual ~FileUtils() {}
public:
    bool isDirectory(const std::string & path) const;
    bool isFile(const std::string & path) const;
    bool isFilePath(const std::string & path) const;
    bool isAbsolute(const std::string & path) const;
    bool isRelative(const std::string & path) const;
    bool isSubpath(const std::string & path, const std::string & parentPath) const;

    std::string absolutePath(const std::string & relativePath) const;
    std::string absolutePath(const std::string & relativePath, const std::string & parent) const;
    std::string canonicalPath(const std::string& path) const;
    std::string getPathFromBase(const std::string & relativePath, const std::string & basePath) const;

    bool pathEquals(const std::string & path1, const std::string & path2) const;
    bool isAValidPath(const std::string & filePath) const;

    long getFileSize(const std::string & filepath) const;
	bool exists(const std::string & filePath) const;
    bool remove(const std::string & filePath, bool recursiveRemove = false) const;
    bool copyFile(const std::string & originPath, const std::string & dstPath) const;

    std::set<std::string> findFiles(const std::string & globPattern) const;
    void listFiles(const std::string & path, std::vector<std::string> & files) const;

    /** @returns the copied file path or std::string("") if fails.*/
    std::string copyTmpFile(const std::string & fileToCopy, const std::string & dir="") const;
    std::string generateUniqueFilepath(const std::string & dir="") const;
    std::string generateTmpFilepath(const std::string & dir="") const;

    std::string getDefaultTmpDir() const;
    void setDefaultTmpDir(const std::string &tmpDir);

    std::string getCurrentDir() const;
    bool changeCurrentDir(const std::string & path) const;

    /** Create directory dirPath if not exist.
     * @param dirPath the path to the directory to be created.
     * @param createParents if set to true, try create the directory parents.
     * @throw IOException if fail in create directory
    */
    void createDirIfNotExists(const std::string & dirPath, bool createParents=false);
    
    /** Create directory dirPath and throws exception if it already exists.
     * @param dirPath the path to the directory to be created.
     * @param createParents if set to true, try create the directory parents.
     * @throw IOException if fail in create directory
    */
    void createDir(const std::string & dirPath, bool createParents=false);

    std::string extractFilename(const std::string & filepath) const;
    std::string getFilenameExtension(const std::string & filepath) const;
    std::string getFilenameWithoutExtension(const std::string & filepath) const;
    void splitFilenameAndExtension(const std::string & filepath, std::string & filenameOut, std::string & extensionOut) const;
    std::string joinPaths(const std::string & parentPath, const std::string & subpath) const;
    
private:
    void createDirImpl(const std::string & dirPath, bool createParents, bool throwIfExist);
    
    std::string genUniqueFilepathImpl(const std::string &dir) const;
    void splitFilenameAndExtensionImpl(const std::string & filepath, std::string * filenameOut, std::string * extensionOut) const;
};

} /* namespace utils */
} /* namespace calmframe */

#endif /* FILEUTILS_H_ */
