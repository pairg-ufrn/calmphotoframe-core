#include "PthreadMutex.h"

namespace calmframe {

PthreadMutex::PthreadMutex()
{
    setupMutexAttributes(mutex_attr);
    pthread_mutex_init(&mutex,&mutex_attr);
}

PthreadMutex::~PthreadMutex()
{
}

pthread_mutex_t &PthreadMutex::getMutex(){
    return mutex;
}

const pthread_mutex_t & PthreadMutex::getMutex() const{
    return mutex;
}

void PthreadMutex::setMutex(const pthread_mutex_t & value){
    mutex = value;
}

void PthreadMutex::lock()
{
    pthread_mutex_lock(&mutex);
}

void PthreadMutex::unlock()
{
    pthread_mutex_unlock(&mutex);
}

void PthreadMutex::setupMutexAttributes(pthread_mutexattr_t & mutexAttr)
{
    pthread_mutexattr_settype(&mutexAttr,PTHREAD_MUTEX_RECURSIVE);
    pthread_mutexattr_setpshared(&mutexAttr, PTHREAD_PROCESS_PRIVATE);
#ifdef HAS_ROBUST_MUTEX
    pthread_mutexattr_setrobust(&mutexAttr, PTHREAD_MUTEX_ROBUST);
#endif
}

}//namespace
