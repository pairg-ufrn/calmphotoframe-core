#ifndef LOG__H
#define LOG__H

#include <string>

namespace Poco {
    class Logger;
}

//TODO: finalizar implementação
namespace calmframe {

namespace Impl {
    class ChannelHolder;
}

namespace LogLevel {
    enum Level{Null, Debug, Information, Warning, Error, Quiet};

    Level fromString(const std::string & str, Level defaultLevel=Null);
    std::string toString(Level logLevel);
}

/** Represents an output to log messages*/
class LogChannel{
public:
    virtual void log(LogLevel::Level level, const std::string & msg)=0;
};

class Log{
public:
    static Log & instance();
    
private:
    //Prevenindo cópias
    Log(const Log & copy);
    Log(Log & copy);
public:
    Log(const std::string & name="");
    virtual ~Log();

    std::string & getName();
    const std::string & getName() const;

    LogLevel::Level getLogLevel() const;
    void setLogLevel(const LogLevel::Level & logLevel);

    void debug(const std::string & text);
    void info(const std::string & text);
    void warning(const std::string & text);
    void error(const std::string & text);

    void resetSetup();

    void addLogFile(const std::string & name, const std::string & filepath);
    void removeLogFile(const std::string & name);
    
    void addChannel(LogChannel * channel);
    void removeChannel(LogChannel * channel);
    
    void setStdoutEnabled(bool enabled);
    
protected:
    void logImpl(const std::string &text, LogLevel::Level logLevel);
    
    void assertChannelHolderNotNull();
    Poco::Logger & getLogger();
    const Poco::Logger & getLogger() const;
    
private:
    std::string name;
    Impl::ChannelHolder * channelHolder;
};

Log & MainLog();

}//namespace


#ifdef ANDROID

#include <android/log.h>
#define logDebug(...) __android_log_print(ANDROID_LOG_DEBUG, __FILE__, __VA_ARGS__)
#define logInfo(...) __android_log_print(ANDROID_LOG_INFO, __FILE__, __VA_ARGS__)
#define logWarning(...) __android_log_print(ANDROID_LOG_WARN, __FILE__, __VA_ARGS__)
#define logError(...) __android_log_print(ANDROID_LOG_ERROR, __FILE__, __VA_ARGS__)

#else

#include "streamprintf.h"

#define _logToStr(...)  strprintf(__VA_ARGS__)
#define logDebug(...)   calmframe::MainLog().debug(_logToStr(__VA_ARGS__));
#define logInfo(...)    calmframe::MainLog().info(_logToStr(__VA_ARGS__));
#define logWarning(...) calmframe::MainLog().warning(_logToStr(__VA_ARGS__));
#define logError(...)   calmframe::MainLog().error(_logToStr(__VA_ARGS__));

#endif

#endif //LOG_H
