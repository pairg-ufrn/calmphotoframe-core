#ifndef PTHREADMUTEX_H
#define PTHREADMUTEX_H

#include "pthread.h"

namespace calmframe {

class PthreadMutex
{
private:
    pthread_mutex_t mutex;
    pthread_mutexattr_t mutex_attr;
public:
    PthreadMutex();
    ~PthreadMutex();

    pthread_mutex_t & getMutex();
    const pthread_mutex_t & getMutex() const;
    void setMutex(const pthread_mutex_t & value);

    void lock();
    void unlock();
protected:
    void setupMutexAttributes(pthread_mutexattr_t & mutexAttr);
};


}//namespace
#endif // PTHREADMUTEX_H
