#ifndef SCOPEDPTHREADLOCK_H
#define SCOPEDPTHREADLOCK_H


namespace calmframe {

class PthreadMutex;

class ScopedPthreadLock{
private:
    PthreadMutex * mutex;
public:
    ScopedPthreadLock(PthreadMutex & mutex);
    ~ScopedPthreadLock();
};

}//namespace
#endif // SCOPEDPTHREADLOCK_H
