/*
 * IllegalArgumentException.hpp
 *
 *  Created on: 10/10/2014
 *      Author: noface
 */

#ifndef ILLEGALARGUMENTEXCEPTION_HPP_
#define ILLEGALARGUMENTEXCEPTION_HPP_

#include "Exception.hpp"
#include <string>

namespace CommonExceptions {

    EXCEPTION_CLASS(IllegalArgumentException);

}//namespace

#endif /* ILLEGALARGUMENTEXCEPTION_HPP_ */
