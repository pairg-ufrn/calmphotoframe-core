/*
 * NumberUtils.cpp
 *
 *  Created on: 07/10/2014
 *      Author: noface
 */


#include <Poco/NumberFormatter.h>
#include <Poco/NumberParser.h>
#include <Poco/Exception.h>

#include <sstream>

#include "utils/CommonExceptions.h"
#include "utils/StringCast.h"
#include "utils/StringUtils.h"

using namespace Poco;
using namespace std;

namespace calmframe {
namespace utils {

bool StringCast::isNumber(const string & str) const
{
    long outTmp;
    return tryToLong(outTmp, str);
}

    std::string StringCast::toString(long number)  const{
        return NumberFormatter::format(number);
    }

    std::string StringCast::toString(unsigned number)  const{
        return toString((int)number);
    }
    
    string StringCast::toString(double number) const{
        return NumberFormatter::format(number);
    }
    
    std::string StringCast::toString(int number)  const{
        try{
            return NumberFormatter::format(number);
        }
        catch(const Poco::Exception & ex){
            throw CommonExceptions::ParseException(ex.displayText());
        }
    }

    int StringCast::toInt(const std::string & string) const{
        try{
            return NumberParser::parse(string);
        }
        catch(const Poco::Exception & ex){
            throw CommonExceptions::ParseException(ex.displayText());
        }
    }
    long StringCast::toLong(const std::string& str)  const{
        try{
            return NumberParser::parse64(str);
        }
        catch(const Poco::Exception & ex){
            throw CommonExceptions::ParseException(ex.displayText());
        }
    }
    float StringCast::toFloat(const std::string& str) const {
        try{
            return NumberParser::parseFloat(str);
        }
        catch(const Poco::Exception & ex){
            throw CommonExceptions::ParseException(ex.displayText());
        }
    }

    bool StringCast::toBoolean(const std::string &str, bool useBoolAlpha) const {
        stringstream sstream(str);
        bool value;
        if(useBoolAlpha){
            sstream >> std::boolalpha >> value;
        }
        else{
            sstream >> value;
        }
        return value;
    }

    int StringCast::tryToInt(const std::string &string, int defaultValue) const{
        int response;
        if(NumberParser::tryParse(string, response)){
            return response;
        }
        else{
            return defaultValue;
        }
    }

    int64_t StringCast::tryToLong(const std::string &str, int64_t defaultValue) const{
        int64_t response;
        if(!str.empty() && NumberParser::tryParse64(str, response)){
            return response;
        }
        else{
            return defaultValue;
        }

    }

    float StringCast::tryToFloat(const std::string &string, float defaultValue) const{
        double response;
        if(NumberParser::tryParseFloat(string, response)){
            return (float)response;
        }
        else{
            return defaultValue;
        }

    }

    double StringCast::tryToDouble(const std::string &str, double defaultValue) const{
        //NumberParser não tem suporte à double
        return tryToFloat(str, (float)defaultValue);
    }

    bool StringCast::tryToBool(const std::string &str, bool defaultValue, bool useBoolAlpha) const
    {
        bool value = this->toBoolean(str, useBoolAlpha);
        string expectedStr;
        stringstream sstream;
        if(useBoolAlpha){
            sstream << std::boolalpha << value;
            sstream >> std::boolalpha >> expectedStr;
        }
        else{
            sstream <<  value;
            sstream >> expectedStr;
        }

        string trimStr(str);
        if(expectedStr == StringUtils::instance().trim(trimStr)){
            return value;
        }
        return defaultValue;
    }


    bool StringCast::tryToLong(long &value, const std::string &str) const
    {
        Int64 tmpInt64;
        bool parsed = NumberParser::tryParse64(str, tmpInt64);
        if(parsed){
            value = tmpInt64;
        }
        return parsed;
    }

    const StringCast &StrCast(){
        return StringCast::instance();
    }
    
    const std::string &calmframe::utils::StringCast::toString(const std::string & str) const{
        return str;
    }

} /* namespace utils */
} /* namespace calmframe */

