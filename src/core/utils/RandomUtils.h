#ifndef RANDOMUTILS_H
#define RANDOMUTILS_H


class RandomUtils
{
public:
    static RandomUtils & instance();
protected:
    RandomUtils();
public:
    int random() const;
};

#endif // RANDOMUTILS_H
