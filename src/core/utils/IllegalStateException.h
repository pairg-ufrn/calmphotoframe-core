#ifndef ILLEGALSTATEEXCEPTION_H
#define ILLEGALSTATEEXCEPTION_H

#include "Exception.hpp"

namespace CommonExceptions {
    EXCEPTION_CLASS(IllegalStateException);
}//namespace

#endif // ILLEGALSTATEEXCEPTION_H
