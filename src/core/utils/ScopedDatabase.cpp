#include "ScopedDatabase.h"
#include "data/Data.h"
#include "data/data-sqlite/SqliteData.h"

using namespace std;
using namespace calmframe::utils;
using namespace calmframe::data;

class InMemoryDataBuilder : public DataBuilder{
public:
    virtual Data * buildData(){
        return new SqliteData(SqliteConnector::createInMemoryDb());
    }

    virtual DataBuilder * clone() const{
        return new InMemoryDataBuilder();
    }
};

ScopedSqliteDatabase::ScopedSqliteDatabase(const std::string & dbDir, const std::string &dbName, bool requireDirExist)
    : dataBuilder(new SqliteDataBuilder{dbDir, dbName})
    , scopedDbFile(SqliteData::createDatabaseName(dbDir, dbName, requireDirExist))
    , dbType(DbType::InFile)
{
    Data::setup(dataBuilder);
}

static DataBuilder * makeBuilder(ScopedSqliteDatabase::DbType dbType, const string & filepath){
    if(dbType == ScopedSqliteDatabase::DbType::InMemory){
        return new InMemoryDataBuilder();
    }
    else{
        return new SqliteDataBuilder{filepath};
    }
}

ScopedSqliteDatabase::ScopedSqliteDatabase(ScopedSqliteDatabase::DbType dbType, const string & filepath)
  : dataBuilder(makeBuilder(dbType, filepath))
  , scopedDbFile(dbType == DbType::InMemory ? std::string() : filepath)
  , dbType(dbType)
{
    Data::setup(dataBuilder);
}

ScopedSqliteDatabase::~ScopedSqliteDatabase(){
    Data::destroyInstance();
}

DataBuilder & ScopedSqliteDatabase::getDataBuilder(){
    return *dataBuilder;
}

const string & ScopedSqliteDatabase::getFilepath() const
{
    return scopedDbFile.getFilepath();
}

ScopedSqliteDatabase::DbType ScopedSqliteDatabase::getDbType(){
    return dbType;
}
