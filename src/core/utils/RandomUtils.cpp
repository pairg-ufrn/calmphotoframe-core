#include "RandomUtils.h"
#include <cstdlib>
#include <ctime>

using namespace std;

RandomUtils &RandomUtils::instance()
{
    static RandomUtils singleton;
    return singleton;
}

RandomUtils::RandomUtils()
{
    srand(time(NULL));
}

int RandomUtils::random() const
{
    return rand();
}
