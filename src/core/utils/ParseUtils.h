#ifndef PARSEUTILS_H
#define PARSEUTILS_H

#include <string>

namespace calmframe {

class ParseUtils
{
public:
    static ParseUtils & instance();
public:
    ParseUtils();
    ~ParseUtils();
public:
    bool toBoolean(const std::string & str, const bool & defaulValue=false) const;
};

}

#endif // PARSEUTILS_H
