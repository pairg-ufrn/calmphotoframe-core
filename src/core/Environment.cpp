#include "Environment.h"

#include "utils/FileUtils.h"
#include "Configurations.h"
#include "Options.h"

#ifndef VERSION_MAJOR
#define VERSION_MAJOR -1
#endif
#ifndef VERSION_MINOR
#define VERSION_MINOR -1
#endif
#ifndef VERSION_PATCH
#define VERSION_PATCH -1
#endif

namespace calmframe {

using namespace std;
using namespace calmframe::utils;

Environment &Environment::instance(){
    static Environment singleton;
    return singleton;
}

Environment::Environment()
    : basePath(FileUtils::instance().getCurrentDir())
{}

Environment::~Environment(){

}

string Environment::getBasePath() const{
    return basePath;
}
void Environment::setBasePath(const string &path){
    this->basePath = FileUtils::instance().canonicalPath(path);
}

string Environment::getImageDir() const{
    return getImageDir(Configurations::instance());
}

string Environment::getImageDir(const Configurations &configurations) const{
    string baseImageDir = imageDir;
    if(baseImageDir.empty()){
        baseImageDir = configurations.get(Options::IMAGE_DIR, Options::Defaults::IMAGE_DIR);
    }
    return getPathFromBase(baseImageDir);
}
void Environment::setImageDir(const std::string & imageDir){
    this->imageDir = imageDir;
}

string Environment::getImagePath(const string &imageName) const{
    return getImagePath(Configurations::instance(), imageName);
}

string Environment::getImagePath(const Configurations &configurations, const string &imageName) const{
    return FileUtils::instance().joinPaths(getImageDir(configurations), imageName);
}

string Environment::getDataDir() const{
    string dataDir = Configurations::instance().get(Options::DATA_DIR, Options::Defaults::DATA_DIR);
    return getPathFromBase(dataDir);
}

static string strOrDefault(const string & str, const string & defaultStr){
    if(str.empty()){
        return defaultStr;
    }
    return str;
}

string Environment::getDataFile(const string &dataDir, const string &fileName) const{
    string dataDirPath = strOrDefault(dataDir, getDataDir());
    string file = strOrDefault(fileName, getDataFilenameDefault());
    return getPathFromBase(FileUtils::instance().joinPaths(dataDirPath, file));
}

string Environment::getDataFilenameDefault() const{
    return Defaults::dataFileName;
}

string Environment::getDefaultLogFile() const{
    return Configurations::instance().get(Options::LOG_FILE,Options::Defaults::LOG_FILE);
}

std::string Environment::getLogFile(const std::string &filename) const{
    return FileUtils::instance().joinPaths(getLogDir()
                                           , filename.empty() ? getDefaultLogFile() : filename);
}

std::string Environment::getLogDir() const{
    string logDir = Configurations::instance().get(Options::LOG_DIR, Options::Defaults::LOG_DIR);
    return getPathFromBase(logDir);
}

string Environment::getPathFromBase(const string &path) const{
    return Files().getPathFromBase(path, basePath);
}

calmframe::VersionNumbers calmframe::Environment::getAppVersion() const{
    return {VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH};
}

}//namespace
