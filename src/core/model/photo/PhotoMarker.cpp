#include "PhotoMarker.h"

PhotoMarker::PhotoMarker(std::string markerIdentification, float x, float y, long photoId, int index)
    :
      index(index)
    , name(markerIdentification)
    , relativeX(x)
    , relativeY(y)
    , photoId(photoId)
{}

const std::string &PhotoMarker::getName() const { return name;}

const float &PhotoMarker::getRelativeX() const { return relativeX;}

const float &PhotoMarker::getRelativeY() const { return relativeY;}

const long &PhotoMarker::getPhotoId() const { return photoId;}

const unsigned &PhotoMarker::getIndex() const { return index;}

void PhotoMarker::setName(std::string identification) {
    this->name = identification;
}

void PhotoMarker::setRelativeX(float relativeX) {
    this->relativeX = relativeX;
}

void PhotoMarker::setRelativeY(float relativeY) {
    this->relativeY = relativeY;
}

void PhotoMarker::setPhotoId(long photoId) {
    this->photoId = photoId;
}

void PhotoMarker::setIndex(const unsigned & value){
    index = value;
}

PhotoMarker &PhotoMarker::operator=(const PhotoMarker & other){
    this->set(other);
    return *this;
}

void PhotoMarker::set(const PhotoMarker & other){
    this->setName(other.getName());
    this->setPhotoId(other.getPhotoId());
    this->setRelativeX(other.getRelativeX());
    this->setRelativeY(other.getRelativeY());
    this->setIndex(other.getIndex());
}

bool PhotoMarker::operator==(const PhotoMarker & other) const{
    return
            this->getIndex() == other.getIndex()
            && this->getName() == other.getName()
            && this->getRelativeX() == other.getRelativeX()
            && this->getRelativeY() == other.getRelativeY();
}

bool PhotoMarker::operator<(const PhotoMarker & other) const{
    //FIXME: ordenação não exata (ver parenteses)
    return this->getPhotoId() < other.getPhotoId()
            || (this->getPhotoId()   == other.getPhotoId()   && this->getIndex()     < other.getIndex())
            || (this->getIndex()     == other.getIndex()     && this->getRelativeX() < other.getRelativeX())
            || (this->getRelativeX() == other.getRelativeX() && this->getRelativeY() < other.getRelativeY())
            || (this->getRelativeY() == other.getRelativeY() && this->getName()      < other.getName());
}

void PhotoMarker::serialize(Serializer & serializer) const {
    serializer.put(PhotoMarkerFields::X, getRelativeX());
    serializer.put(PhotoMarkerFields::Y, getRelativeY());
    serializer.put(PhotoMarkerFields::IDENTIFICATION, getName());
    serializer.put(PhotoMarkerFields::INDEX, (int)getIndex());
}

void PhotoMarker::deserialize(const GenericContainer & container){
    this->setRelativeX(container.get(PhotoMarkerFields::X, 0.0f).asNumber<float>());
    this->setRelativeY(container.get(PhotoMarkerFields::Y, 0.0f).asNumber<float>());
    this->setName(container.get(PhotoMarkerFields::IDENTIFICATION,std::string()).asString());
    this->setIndex(container.get(PhotoMarkerFields::INDEX, 0u).asNumber<unsigned>());
}
