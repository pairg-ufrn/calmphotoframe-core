/*
 * PhotoDescription.h
 *
 *  Created on: 03/08/2014
 *      Author: noface
 */

#ifndef PHOTO_PHOTODESCRIPTION_H_
#define PHOTO_PHOTODESCRIPTION_H_

#include <string>
#include <vector>
#include "PhotoMarker.h"
#include <iostream>
#include <algorithm>
class PhotoDescription {
private:
	std::string description;
	std::string moment;
	//TODO: utilizar um formato de data (talvez outra classe) ou manter como um long (tempo unix)
	std::string date;
	std::string place;
    std::string credits;

    typedef std::vector<PhotoMarker> PhotoMarkersList;
    PhotoMarkersList markers;
public:
    PhotoDescription(
            const std::string & description = ""
          , const std::string & moment = ""
          , const std::string & date = ""
          , const std::string & place = ""
          , const std::vector<PhotoMarker> & photoMarkers = std::vector<PhotoMarker>() );
    virtual ~PhotoDescription(){}

    bool operator== (const PhotoDescription & other) const;
    void set(const PhotoDescription & other);

	const std::string & getDate()        	const { return date; }
	const std::string & getDescription() 	const { return description;}
    const std::string & getMoment() 	 	const { return moment;}
    const std::string & getPlace()       	const { return place;}
    const std::string & getCredits()       	const { return credits;}

    const std::vector<PhotoMarker>
                     & getMarkers()  		const { return this->markers;}

    void setDate         (const std::string &  date)          { this->date 		      = date;}
    void setDescription  (const std::string &  description)   { this->description     = description;}
    void setMoment       (const std::string &  moment)        { this->moment 		  = moment;}
    void setPlace        (const std::string &  place)         { this->place 		  = place;}
    void setCredits      (const std::string &  credits)       { this->credits 		  = credits;}

    PhotoDescription & setMarkers(const std::vector<PhotoMarker> & someMarkers);

    /** Get PhotoMarker in position markerPos.
        @throw std::out_of_range exception if markerPos does not exist.
        @param markerPos is 0 based index of a marker contained in this PhotoDescription.
    */
    const PhotoMarker & getMarker(const unsigned & markerPos) const;
    /** Add a PhotoMarker to the end of the markers list on this PhotoDescription.
     * Updates the marker index and to the correct value.
    */
    void addMarker(const PhotoMarker & marker);
    /** Insert a PhotoMarker in the markerPos position of the markers list on this PhotoDescription.
     * If markerPos is greater than or equals this list size, insert it on the end.
     * @param marker some PhotoMarker
     * @param markerPos is 0 based index of a marker contained in this PhotoDescription.
    */
    void addMarker(const PhotoMarker & marker, int markerPos);
    /** Remove the marker on position markerPos, if it exist.*/
    void removeMarker(const unsigned & markerPos);
    /** Return the number of PhotoMarkers contained on this PhotoDescription*/
    unsigned getMarkerCount() const;

    /** Change PhotoMarkers to ensure that they have correct indexes and are in right order.*/
    void fixPhotoMarkers();
};

#endif /* PHOTO_PHOTODESCRIPTION_H_ */
