#ifndef ORIENTATIONMETADATA_H
#define ORIENTATIONMETADATA_H

#include "serialization/Serializable.hpp"

namespace Orientation{
    enum OrientationFlag{//Flags defined by EXIF standard
        UNKNOWN      = 0,
        TOP_LEFT     = 1,
        TOP_RIGHT    = 2, //Flipped
        BOTTOM_RIGHT = 3,
        BOTTOM_LEFT  = 4, //Flipped
        LEFT_TOP     = 5, //Flipped
        RIGHT_TOP    = 6,
        RIGHT_BOTTOM = 7, //Flipped
        LEFT_BOTTOM  = 8
    };
    bool isValid(int value);

    namespace Constants{
        const bool isFlipped_hashTable[] = {
              false //UNKNOWN
            , false //TOP_LEFT
            , true //TOP_RIGHT
            , false //BOTTOM_RIGHT
            , true //BOTTOM_LEFT
            , true //LEFT_TOP
            , false //RIGHT_TOP
            , true //RIGHT_BOTTOM
            , false //LEFT_BOTTOM
        };
        /* The rotation in degrees at which the image is rotated in clockwise direction*/
        const unsigned clockwiseRotatedAngle_hashTable[] = {
              0 //UNKNOWN
            , 0 //TOP_LEFT
            , 0 //TOP_RIGHT
            , 180 //BOTTOM_RIGHT
            , 180 //BOTTOM_LEFT
            , 270 //LEFT_TOP
            , 270 //RIGHT_TOP
            , 90 //RIGHT_BOTTOM
            , 90 //LEFT_BOTTOM
        };
    }
}

namespace OrientationMetadataFields{
    static const char * FLIPPED = "flipped";
    static const char * CLOCKWISE_ROTATION = "clockwiseRotation";
    static const char * ORIENTATION_CODE = "orientationCode";
}
class OrientationMetadata : public Serializable {
private:
    Orientation::OrientationFlag flag;
public:
    OrientationMetadata(const Orientation::OrientationFlag & orientationFlag = Orientation::UNKNOWN);
    OrientationMetadata(const unsigned &orientationFlag);

    bool isFlipped() const;
    unsigned getRotatedAngle() const;
    const Orientation::OrientationFlag & getFlag() const;
    void setFlag(const Orientation::OrientationFlag & flag);

    bool isUnknown() const;

    virtual void serialize(Serializer &serializer) const;
};

#endif // ORIENTATIONMETADATA_H
