#ifndef VISIBILITY_H
#define VISIBILITY_H

#include "utils/CommonExceptions.h"
#include "../permissions/PermissionLevel.h"

namespace calmframe {

class Visibility{
public:
    static Visibility fromString(const std::string & name);

public:
    Visibility(int enumValue);
    operator int() const;

    const char * toString() const;
    PermissionLevel toPermissionLevel() const;
public:
    static const Visibility
        Unknown         ,
        Private         ,
        VisibleByOthers ,
        EditableByOthers;

private:
    static Visibility putValue(const char * name);

private:
    Visibility(int enumValue, const char * name);
    int enumValue;
    const char * name;
    void assertValueIsValid(int value);
};

}//namespace

#endif // VISIBILITY_H

