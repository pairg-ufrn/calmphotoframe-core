#ifndef LOCATION_H
#define LOCATION_H

#include "serialization/Serializable.hpp"

namespace calmframe {

namespace LocationFields{
    static const char * LATITUDE = "latitude";
    static const char * LONGITUDE = "longitude";
    static const char * ALTITUDE = "altitude";
}

class Location : public Serializable{
public:
    static const float UNKNOWN_COORD;
    static const float UNKNOWN_ALTITUDE;
private:
    float latitude;
    float longitude;
    float altitude;
public:
    Location()
        : latitude(UNKNOWN_COORD)
        , longitude(UNKNOWN_COORD)
        , altitude(UNKNOWN_ALTITUDE)
    {}
    Location(float latitude, float longitude, float altitude = UNKNOWN_ALTITUDE)
        : latitude(latitude)
        , longitude(longitude)
        , altitude(altitude)
    {}

    float getLatitude() const  { return latitude;}
    float getLongitude() const { return longitude;}
    float getAltitude() const  { return altitude;}
    void setLatitude(float value){
        latitude = value;
    }
    void setLongitude(float value){
        longitude = value;
    }
    void setAltitude(float value){
        altitude = value;
    }
public:
    virtual void serialize(Serializer & serializer) const;
public:
    bool isUnknown() const;
    bool operator==(const Location & other) const;
};

}//namespace

#endif // LOCATION_H
