#include "PhotoCollection.h"
#include "PhotoContext.h"

#include "serialization/GenericContainer.h"
#include "serialization/Serializer.h"
#include "serialization/GenericIterator.h"
#include <algorithm>
#include <iterator>

#include "Photo.h"

namespace calmframe {

const long PhotoCollection::INVALID_ID = -1;
const long PhotoCollection::INVALID_CONTEXT_ID = PhotoContext::INVALID_ID;

PhotoCollection::PhotoCollection(const std::string & name)
    : id(INVALID_ID)
    , name(name)
    , parentContext(INVALID_CONTEXT_ID)
    , photoCoverId(Photo::UNKNOWN_ID)
{}

PhotoCollection::~PhotoCollection()
{}

long PhotoCollection::getId() const{
    return id;
}
const std::string &PhotoCollection::getName() const{
    return name;
}
long PhotoCollection::getParentContext() const{
    return parentContext;
}
const std::set<long> &PhotoCollection::getPhotos() const{
    return this->photos;
}

const IdCollection &PhotoCollection::getSubCollections() const
{
    return subcollections;
}

IdCollection &PhotoCollection::getSubCollections()
{
    return subcollections;
}
bool PhotoCollection::hasPhotoContext() const{
    return getParentContext() != INVALID_CONTEXT_ID;
}

void PhotoCollection::setId(long value){
    id = value;
}
void PhotoCollection::setName(const std::string &value){
    name = value;
}
void PhotoCollection::setParentContext(long id){
    parentContext = id;
}

void calmframe::PhotoCollection::setPhotos(const std::set<long> &photoIds){
    this->photos = photoIds;
}

void PhotoCollection::setSubCollections(const IdCollection &subcollectionIds)
{
    this->subcollections = subcollectionIds;
}

void PhotoCollection::clearPhotos(){
    this->photos.clear();
}

void PhotoCollection::putPhoto(long photoId){
    this->photos.insert(photoId);
}

void PhotoCollection::removePhoto(long photoId){
    this->photos.erase(photoId);
}

bool PhotoCollection::containsPhoto(long photoId) const{
    return this->photos.find(photoId) != photos.end();
}

long PhotoCollection::getPhotoCover() const{
    if(photoIsValid(photoCoverId)){
        return photoCoverId;
    }
    else if(getPhotos().size() > 0){
        return *getPhotos().begin();
    }
    else{
        return Photo::UNKNOWN_ID;
    }
}

void PhotoCollection::setPhotoCover(long photoId){
    this->photoCoverId = photoId;
}

void PhotoCollection::clearPhotoCover(){
    this->setPhotoCover(Photo::UNKNOWN_ID);
}

bool PhotoCollection::photoIsValid(long photoId) const{
    return photoId > 0;
}

void PhotoCollection::serialize(Serializer &serializer) const{
    if(getId() != INVALID_ID){
        serializer.put(PhotoCollectionFields::ID, this->getId());
    }

    serializer.put(PhotoCollectionFields::NAME, this->getName());

    long coverId = getPhotoCover();
    if(photoIsValid(coverId)){
        serializer.put(PhotoCollectionFields::PHOTO_COVER_ID, coverId);
    }
    if(getParentContext() != INVALID_ID){
        serializer.put(PhotoCollectionFields::CONTAINER_CONTEXT, getParentContext());
    }

    serializer.putCollection(PhotoCollectionFields::PHOTO_IDS, getPhotos().begin(), getPhotos().end());
    serializer.putCollection(PhotoCollectionFields::SUBCOLLECTIONS_IDS, getSubCollections().getIds().begin()
                                                                , getSubCollections().getIds().end());
}

void PhotoCollection::deserialize(const GenericContainer &deserializer){
    this->setId(deserializer.get(PhotoCollectionFields::ID).asNumber<long>(this->getId()));
    this->setName(deserializer.get(PhotoCollectionFields::NAME).asString(this->getName()));

    if(deserializer.contains(PhotoCollectionFields::PHOTO_IDS)){
        this->clearPhotos();
        GenericIterator iterator = deserializer.get(PhotoCollectionFields::PHOTO_IDS).as<GenericIterator>();
        while(iterator.next()){
            this->putPhoto(iterator.getValue().asNumber<long>());
        }
    }
    if(deserializer.contains(PhotoCollectionFields::SUBCOLLECTIONS_IDS)){
        this->getSubCollections().clear();
        GenericIterator iterator = deserializer.get(PhotoCollectionFields::SUBCOLLECTIONS_IDS).as<GenericIterator>();
        while(iterator.next()){
            this->getSubCollections().put(iterator.getValue().asNumber<long>());
        }
    }

    long coverId = deserializer.get(PhotoCollectionFields::PHOTO_COVER_ID).asNumber<long>(this->photoCoverId);
    setPhotoCover(coverId);

    long ctxId = deserializer.get(PhotoCollectionFields::CONTAINER_CONTEXT).asNumber<long>(getParentContext());
    setParentContext(ctxId);
}

bool PhotoCollection::equals(const PhotoCollection &other) const{
    return this->getId()                 == other.getId()
            && this->getName()           == other.getName()
            && this->getParentContext()  == other.getParentContext()
            && this->getPhotos()         == other.getPhotos()
    && this->getSubCollections() == other.getSubCollections();
}

bool operator==(const PhotoCollection &coll1, const PhotoCollection &coll2){
    return coll1.equals(coll2);
}
bool operator!=(const PhotoCollection &coll1, const PhotoCollection &coll2){
    return !(coll1 == coll2);
}

}//namespace

