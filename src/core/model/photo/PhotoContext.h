#ifndef PHOTOCONTEXT_H
#define PHOTOCONTEXT_H

#include <string>
#include "serialization/Deserializable.hpp"
#include "serialization/Serializable.hpp"
#include "Visibility.h"

namespace calmframe {

namespace PhotoContextFields {
    const std::string id                = "id";
    const std::string name              = "name";
    const std::string visibility        = "visibility";
    const std::string rootCollectionId  = "rootCollectionId";
}//namespace

class PhotoContext : public Deserializable, public Serializable
{
public:
    static const long INVALID_ID;
    static const long INVALID_COLLECTION_ID;
private:
    long id;
    long rootCollection;
    std::string name;
    Visibility visibility;
public:
    PhotoContext(long id, const std::string & name=std::string(), Visibility visibility = Visibility::Unknown);
    PhotoContext(const std::string & name=std::string(), Visibility visibility = Visibility::Unknown);
    ~PhotoContext();

    bool hasValidId() const;

    long getId() const;
    long getRootCollection() const;
    const std::string & getName() const;
    const Visibility & getVisibility() const;

    void setId(long value);
    void setRootCollection(long value);
    void setName(const std::string &value);
    void setVisibility(const Visibility & value);

    virtual void serialize(Serializer & serializer) const;
    virtual void deserialize(const GenericContainer & deserializer);

    bool operator!=(const PhotoContext & other)const;
    bool operator==(const PhotoContext & other)const;
    bool operator<(const PhotoContext & other) const;
};

}//namespace

#endif // PHOTOCONTEXT_H
