#include "OrientationMetadata.h"

#include "serialization/Serializer.h"

bool Orientation::isValid(int value){
    return value >= UNKNOWN && value <= LEFT_BOTTOM;
}

OrientationMetadata::OrientationMetadata(const unsigned &orientationFlag)
    : flag((Orientation::OrientationFlag) (orientationFlag > 8 ? 0 : orientationFlag) )
{}


OrientationMetadata::OrientationMetadata(const Orientation::OrientationFlag &orientationFlag)
    : flag(orientationFlag)
{}

const Orientation::OrientationFlag &OrientationMetadata::getFlag() const{
    return this->flag;
}
void OrientationMetadata::setFlag(const Orientation::OrientationFlag &flag){
    this->flag = flag;
}

bool OrientationMetadata::isUnknown() const
{
    return this->getFlag() == Orientation::UNKNOWN;
}

bool OrientationMetadata::isFlipped() const
{
    return Orientation::Constants::isFlipped_hashTable[(int)this->flag];
}

unsigned OrientationMetadata::getRotatedAngle() const
{
    return Orientation::Constants::clockwiseRotatedAngle_hashTable[(int)this->flag];
}


void OrientationMetadata::serialize(Serializer &serializer) const{
    if(!this->isUnknown()){
        serializer.put(OrientationMetadataFields::ORIENTATION_CODE  , getFlag());
        serializer.put(OrientationMetadataFields::FLIPPED           , isFlipped());
        serializer.put(OrientationMetadataFields::CLOCKWISE_ROTATION, (int)getRotatedAngle());
    }
}
