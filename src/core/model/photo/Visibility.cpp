#include "Visibility.h"

#include <vector>

#include "utils/StringUtils.h"

namespace calmframe {

static std::vector<Visibility> values;

Visibility Visibility::putValue(const char * name){
    Visibility newVisibility{values.size() + 1, name};
    values.push_back(newVisibility);

    return newVisibility;
}

#define ADD_ENUM(Name) \
    const Visibility Visibility::Name = Visibility::putValue(#Name)

ADD_ENUM(Unknown);
ADD_ENUM(Private);
ADD_ENUM(VisibleByOthers);
ADD_ENUM(EditableByOthers);

Visibility::operator int() const{
    return enumValue;
}

Visibility Visibility::fromString(const std::string & name){
    for(auto visibility : values){
        if(utils::StringUtils::instance().caseInsensitiveEquals(visibility.toString(), name)){
            return visibility;
        }
    }
    return Visibility::Unknown;
}

Visibility::Visibility(int enumValue){
    assertValueIsValid(enumValue);
    *this = values[enumValue - 1];
}
Visibility::Visibility(int enumValue, const char * name)
    : enumValue(enumValue)
    , name(name)
{
}

void Visibility::assertValueIsValid(int value){
    if(value <=0 || value > values.size()){
        throw CommonExceptions::IllegalArgumentException("Integer value is not a valid Visibility instance");
    }
}

const char *Visibility::toString() const{
    return name;
}

PermissionLevel Visibility::toPermissionLevel() const{
    if(*this == EditableByOthers){
        return PermissionLevel::Write;
    }
    else if(*this == VisibleByOthers){
        return PermissionLevel::Read;
    }
    else{
        return PermissionLevel::None;
    }
}

}//namespace
