#include "PhotoContext.h"
#include "PhotoCollection.h"
#include "serialization/Serializer.h"

namespace calmframe {

const long PhotoContext::INVALID_ID = 0;
const long PhotoContext::INVALID_COLLECTION_ID = PhotoCollection::INVALID_ID;


PhotoContext::PhotoContext(const std::string &name, Visibility visibility)
    : PhotoContext(INVALID_ID, name, visibility)
{}

PhotoContext::PhotoContext(long id, const std::string & name, Visibility visibility)
    : id(id)
    , rootCollection(INVALID_COLLECTION_ID)
    , name(name)
    , visibility(visibility)
{}

PhotoContext::~PhotoContext()
{}

bool PhotoContext::hasValidId() const{
    return getId() != INVALID_ID;
}

long PhotoContext::getId() const{
    return id;
}
void PhotoContext::setId(long value){
    id = value;
}

const std::string &PhotoContext::getName() const{
    return name;
}
void PhotoContext::setName(const std::string &value){
    name = value;
}

long PhotoContext::getRootCollection() const{
    return rootCollection;
}

void PhotoContext::setRootCollection(long value){
    rootCollection = value;
}

const Visibility & PhotoContext::getVisibility() const{
    return visibility;
}

void PhotoContext::setVisibility(const Visibility & value){
    visibility = value;
}

void PhotoContext::serialize(Serializer &serializer) const{
    serializer.put(PhotoContextFields::name, getName());

    if(getId() != INVALID_ID){
        serializer.put(PhotoContextFields::id, getId());
    }
    if(getRootCollection() != INVALID_COLLECTION_ID){
        serializer.put(PhotoContextFields::rootCollectionId, getRootCollection());
    }
    if(getVisibility() != Visibility::Unknown){
        serializer.put(PhotoContextFields::visibility, getVisibility().toString());
    }
}
void PhotoContext::deserialize(const GenericContainer &deserializer){
    setId(deserializer.get(PhotoContextFields::id, getId()).asNumber<long>());
    setName(deserializer.get(PhotoContextFields::name, getName()).asString());
    setRootCollection(deserializer.get(PhotoContextFields::rootCollectionId
                                     , getRootCollection()).asNumber<long>());
    setVisibility(Visibility::fromString(deserializer.get(PhotoContextFields::visibility).asString()));
}

bool PhotoContext::operator!=(const PhotoContext & other) const
{
    return !(*this == other);
}

bool PhotoContext::operator==(const PhotoContext & other) const
{
    return getId() == other.getId()
            && getName() == other.getName()
            && getRootCollection() == other.getRootCollection()
            && getVisibility() == other.getVisibility();
}

bool PhotoContext::operator<(const PhotoContext & other) const{
    return getId()          < other.getId()         || (getId()   == other.getId() && (
           getName()        < other.getName()       || (getName() == other.getName() && (
           getVisibility()  < other.getVisibility() || (getVisibility() == other.getVisibility() && (
           getRootCollection() < other.getRootCollection()
           ))
           ))
           ));
}

}//namespace

