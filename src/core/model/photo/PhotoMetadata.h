/*
 * PhotoMetadata.h
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#ifndef PHOTO_PHOTOMETADATA_H_
#define PHOTO_PHOTOMETADATA_H_

#include <string>
#include "OrientationMetadata.h"
#include "Location.h"

#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"

namespace calmframe {

namespace PhotoMetadataFields{
    static const char * IMAGESIZE        = "imageSize";
    static const char * IMAGEWIDTH       = "imageWidth";
    static const char * IMAGEHEIGHT      = "imageHeight";
    static const char * CAMERAMODEL      = "cameraModel";
    static const char * CAMERAMAKER      = "cameraMaker";
    static const char * ORIGINALDATE     = "originalDate";
    static const char * ORIGINALFILENAME = "originalFilename";
    static const char * LOCATION         = "location";
    static const char * ORIENTATION      = "orientation";
}
class PhotoMetadata : public Serializable{
public:
    const static long UNKNOWN_PHOTO_ID;
    const static long UNKNOWN_SIZE;
    const static int UNKNOWN_DIMENSION;
private:
    long photoId;
    long imageSize;
    int imageWidth, imageHeight;
    std::string originalFilename;
    std::string cameraModel;
    std::string cameraMaker;
    std::string originalDate;
    Location location;
    OrientationMetadata orientation;
public:
    PhotoMetadata(long photoId      = UNKNOWN_PHOTO_ID
                 , long imageSize   = UNKNOWN_SIZE
                 , int imageWidth   = UNKNOWN_DIMENSION
                 , int imageHeight  = UNKNOWN_DIMENSION
                 , const std::string & originalFilename     = std::string()
                 , const std::string & cameraModel          = std::string()
                 , const std::string & cameraMaker          = std::string()
                 , const std::string & originalDate         = std::string()
                 , const Location & location                = Location()
                 , const OrientationMetadata & orientation  = OrientationMetadata());

public: //Getters and Setters
    const long &        getPhotoId()            const;
    const std::string&  getCameraMaker()        const;
    const std::string&  getCameraModel()        const;
    const std::string&  getOriginalDate()       const;
    const std::string&  getOriginalFilename()   const;
    const long &        getImageSize()          const;
    const int  &        getImageWidth()         const;
    const int  &        getImageHeight()        const;

    Location &          getLocation();
    const Location &    getLocation() const;

    OrientationMetadata &       getOrientation();
    const OrientationMetadata & getOrientation() const;

    PhotoMetadata & setPhotoId(long value);
    PhotoMetadata & setCameraMaker(const std::string& cameraMaker);
    PhotoMetadata & setCameraModel(const std::string& cameraModel);
    PhotoMetadata & setOriginalDate(const std::string& originalDate);
    PhotoMetadata & setOriginalFilename(const std::string& originalFilename);
    PhotoMetadata & setImageSize(long value);
    PhotoMetadata & setImageWidth(int value);
    PhotoMetadata & setImageHeight(int value);
    PhotoMetadata & setOrientation(const OrientationMetadata &value);
    PhotoMetadata & setLocation(const Location &value);
public:
    virtual void serialize(Serializer &serializer) const;
//    virtual void deserialize(const GenericContainer & deserializer);
public:
    bool operator==(const PhotoMetadata & other) const;

    /* Function to compare two PhotoMetadata instances based on their photoId.
       This function can be used in std::sort.
       @return true if metadata.getPhotoId() < otherMetadata.getPhotoId()
    */
    static bool compareByPhotoId(const PhotoMetadata & metadata, const PhotoMetadata & otherMetadata);
};

} /* namespace calmframe */

#endif /* PHOTO_PHOTOMETADATA_H_ */
