#include "Location.h"

#include <cfloat>
#include <cmath>

#include "serialization/Serializer.h"

namespace calmframe {
const float Location::UNKNOWN_COORD    = FLT_MAX;
const float Location::UNKNOWN_ALTITUDE = FLT_MAX;

namespace Constants{
    static const float DEGREE_EPSILON = 1.0f/3600.0f; //Valor de 1 segundo em graus
    static const float METERS_EPSILON = 1.0f/1000.0f; //Valor de 1 milímetro em metros
}

void Location::serialize(Serializer &serializer) const{
    if(getLatitude() != UNKNOWN_COORD){
        serializer.put(LocationFields::LATITUDE, getLatitude());
    }
    if(getLongitude() != UNKNOWN_COORD){
        serializer.put(LocationFields::LONGITUDE, getLongitude());
    }if(getAltitude() != UNKNOWN_ALTITUDE){
        serializer.put(LocationFields::ALTITUDE, getAltitude());
    }
}

bool Location::isUnknown() const
{
    return getLatitude() == UNKNOWN_COORD
            && getLongitude() == UNKNOWN_COORD
            && getAltitude() == UNKNOWN_COORD;
}

bool floatEquals(const float & degreeA, const float & degreeB, const float & epsilon){
    float diff = fabs(degreeA - degreeB);
    return diff < epsilon;
}
bool degreesEquals(const float & degreeA, const float & degreeB){
    return floatEquals(degreeA, degreeB, Constants::DEGREE_EPSILON);
}

bool Location::operator==(const Location &other) const{
    return degreesEquals(this->getLatitude(), other.getLatitude())
            && degreesEquals(this->getLongitude(), other.getLongitude())
            && floatEquals(this->getAltitude(), other.getAltitude(), Constants::METERS_EPSILON);
}

}//namespace
