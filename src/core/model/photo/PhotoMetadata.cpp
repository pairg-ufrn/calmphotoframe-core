/*
 * PhotoMetadata.cpp
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#include "PhotoMetadata.h"
#include "serialization/Serializer.h"

namespace calmframe {

const long PhotoMetadata::UNKNOWN_PHOTO_ID = 0;
const long PhotoMetadata::UNKNOWN_SIZE = -1;
const int PhotoMetadata::UNKNOWN_DIMENSION = -1;

PhotoMetadata::PhotoMetadata(
    long photoId
    , long imageSize
    , int imageWidth
    , int imageHeight
    , const std::string & originalFilename
    , const std::string & cameraModel
    , const std::string & cameraMaker
    , const std::string & originalDate
    , const Location & location
    , const OrientationMetadata & orientation)

    : photoId(photoId)
    , imageSize(imageSize)
    , imageWidth(imageWidth)
    , imageHeight(imageHeight)
    , originalFilename(originalFilename)
    , cameraModel(cameraModel)
    , cameraMaker(cameraMaker)
    , originalDate(originalDate)
    , location(location)
    , orientation(orientation)
{

}

const long &PhotoMetadata::getPhotoId() const {
    return photoId;
}

const std::string &PhotoMetadata::getCameraMaker() const {
    return cameraMaker;
}

const std::string &PhotoMetadata::getCameraModel() const {
    return cameraModel;
}

const std::string &PhotoMetadata::getOriginalDate() const {
    return originalDate;
}

const std::string &PhotoMetadata::getOriginalFilename() const {
    return originalFilename;
}

const long &PhotoMetadata::getImageSize() const {
    return imageSize;
}

const int &PhotoMetadata::getImageWidth() const {
    return imageWidth;
}

const int &PhotoMetadata::getImageHeight() const {
    return imageHeight;
}

OrientationMetadata &PhotoMetadata::getOrientation() {
    return orientation;
}

const OrientationMetadata &PhotoMetadata::getOrientation() const {
    return orientation;
}

Location &PhotoMetadata::getLocation() {
    return location;
}

const Location &PhotoMetadata::getLocation() const {
    return location;
}

PhotoMetadata & PhotoMetadata::setPhotoId(long value) {
    photoId = value;
    return * this;
}

PhotoMetadata & PhotoMetadata::setCameraMaker(const std::string & cameraMaker) {
    this->cameraMaker = cameraMaker;
    return * this;
}

PhotoMetadata & PhotoMetadata::setCameraModel(const std::string & cameraModel) {
    this->cameraModel = cameraModel;
    return * this;
}

PhotoMetadata & PhotoMetadata::setOriginalDate(const std::string & originalDate) {
    this->originalDate = originalDate;
    return * this;
}

PhotoMetadata & PhotoMetadata::setOriginalFilename(const std::string & originalFilename) {
    this->originalFilename = originalFilename;
    return * this;
}

PhotoMetadata & PhotoMetadata::setImageSize(long value){
    imageSize = value;
    return * this;
}

PhotoMetadata & PhotoMetadata::setImageWidth(int value){
    imageWidth = value;
    return * this;
}

PhotoMetadata & PhotoMetadata::setImageHeight(int value){
    imageHeight = value;
    return * this;
}

PhotoMetadata & PhotoMetadata::setOrientation(const OrientationMetadata & value){
    orientation = value;
    return * this;
}

PhotoMetadata & PhotoMetadata::setLocation(const Location & value){
    location = value;
    return * this;
}

void PhotoMetadata::serialize(Serializer &serializer) const{
    if(getImageSize() != UNKNOWN_SIZE){
        serializer.put(PhotoMetadataFields::IMAGESIZE, getImageSize());
    }
    if(getImageWidth() != UNKNOWN_DIMENSION){
        serializer.put(PhotoMetadataFields::IMAGEWIDTH, getImageWidth());
    }
    if(getImageHeight() != UNKNOWN_DIMENSION){
        serializer.put(PhotoMetadataFields::IMAGEHEIGHT, getImageHeight());
    }
    if(getCameraMaker().empty() == false){
        serializer.put(PhotoMetadataFields::CAMERAMAKER, getCameraMaker());
    }
    if(getCameraModel().empty() == false){
        serializer.put(PhotoMetadataFields::CAMERAMODEL, getCameraModel());
    }
    if(getOriginalDate().empty() == false){
        serializer.put(PhotoMetadataFields::ORIGINALDATE, getOriginalDate());
    }
    if(getOriginalFilename().empty() == false){
        serializer.put(PhotoMetadataFields::ORIGINALFILENAME, getOriginalFilename());
    }
    if(!getLocation().isUnknown()){
        serializer.putChild(PhotoMetadataFields::LOCATION, getLocation());
    }
    if(!getOrientation().isUnknown()){
        serializer.putChild(PhotoMetadataFields::ORIENTATION, getOrientation());
    }
}

bool PhotoMetadata::operator==(const PhotoMetadata &other) const{
    return this->getPhotoId() == other.getPhotoId()
            && this->getImageSize() == other.getImageSize()
            && this->getImageHeight() == other.getImageHeight()
            && this->getImageWidth() == other.getImageWidth()
            && this->getCameraMaker() == other.getCameraMaker()
            && this->getCameraModel() == other.getCameraModel()
            && this->getOriginalDate() == other.getOriginalDate()
            && this->getOrientation().getFlag() == other.getOrientation().getFlag()
            && this->getLocation() == other.getLocation();
}

bool PhotoMetadata::compareByPhotoId(const PhotoMetadata &metadata, const PhotoMetadata &otherMetadata){
    return metadata.getPhotoId() < otherMetadata.getPhotoId();
}

} /* namespace calmframe */
