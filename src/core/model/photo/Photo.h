/*
 * Photo.h
 *
 *  Created on: 03/08/2014
 *      Author: noface
 */

#ifndef PHOTO_PHOTO_H_
#define PHOTO_PHOTO_H_

#include "PhotoDescription.h"
#include "PhotoMetadata.h"
#include <string>
#include <iostream>
#include "serialization/Serializable.hpp"
#include "serialization/Serializer.h"
#include "serialization/Deserializable.hpp"
#include "serialization/Deserializer.h"
#include "serialization/GenericContainer.h"

namespace calmframe{

namespace PhotoFields{
    constexpr const char * ID          = "id";
    constexpr const char * ContextId   = "contextId";
    constexpr const char * DATE        = "date";
    constexpr const char * MOMENT      = "moment";
    constexpr const char * PLACE       = "place";
    constexpr const char * DESCRIPTION = "description";
    constexpr const char * METADATA    = "metadata";
    constexpr const char * MARKERS     = "markers";
    constexpr const char * CREDITS     = "credits";
}

class Photo : public Serializable
            , public Deserializable{
private:
    long id;
    long contextId;
    std::string imagePath;
    PhotoDescription photoDescription;
    PhotoMetadata metadata;
public:
    static constexpr const long UNKNOWN_ID=0;
    static const long UnknownContextId;

public:
    Photo(const std::string & filename = "", const PhotoDescription & photoDescription = PhotoDescription());
    Photo(long id, const std::string & path = "", const PhotoDescription & photoDescription = PhotoDescription());
    Photo(const PhotoDescription & photoDescription, const PhotoMetadata & metadata = {}, long contextId=UnknownContextId);

    virtual ~Photo(){}

    bool operator<(const Photo & other) const;
    bool operator>(const Photo & other) const;
    bool operator==(const Photo & other) const;
    bool operator!=(const Photo & other) const;

    int compare(const Photo & other)const;

    long getId() const 								 { return id;}
    const std::string & getImagePath() const 		 { return imagePath;}
    const PhotoDescription  & getDescription() const { return photoDescription; }
    PhotoDescription  	    & getDescription()       { return photoDescription; }
    const PhotoMetadata     & getMetadata() const    { return metadata; }
    PhotoMetadata  	        & getMetadata()          { return metadata; }
    long getContextId() const;

    void setId(const long & id) 										   { this->id = id;}
    void setImagePath  (const std::string & filePath)              { this->imagePath = filePath; }
    void setDescription(const PhotoDescription & photoDescription) { this->photoDescription.set(photoDescription);}
    void setMetadata(const PhotoMetadata & metadata)               {this->metadata = metadata;}
    void setContextId(long id);

    virtual void serialize(Serializer &serializer) const;
    virtual void deserialize(const GenericContainer & deserializer);
    void deserializerMarkers(const GenericContainer& container);
};

}

#endif /* PHOTO_PHOTO_H_ */
