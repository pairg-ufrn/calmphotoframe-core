#ifndef PHOTOCOLLECTION_H
#define PHOTOCOLLECTION_H

#include <string>
#include <map>
#include <set>

#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"

#include "../IdCollection.h"

namespace calmframe {

namespace PhotoCollectionFields{
    static const char * ID              = "id";
    static const char * NAME            = "name";
    static const char * PHOTO_IDS       = "photoIds";
    static const char * SUBCOLLECTIONS_IDS = "subcollectionsIds";
    static const char * PHOTO_COVER_ID  = "photoCoverId";
    static const char * CONTAINER_CONTEXT = "context";
}

class PhotoCollection : public Deserializable, public Serializable
{
public:
    static const long INVALID_ID;
    static const long INVALID_CONTEXT_ID;
public:
    template<typename Iterator>
    static std::map<long, PhotoCollection> mapById(const Iterator & begin, const Iterator & end);

private:
    long id;
    std::string name;
    std::set<long> photos;
    IdCollection subcollections;

    /** The context id to which this collection belongs*/
    long parentContext;
    /** The id of the photo which represents this collection*/
    long photoCoverId;
public:
    PhotoCollection(const std::string & name=std::string());
    ~PhotoCollection();

    long getId() const;
    void setId(long value);
    const std::string & getName() const;
    void setName(const std::string &value);

    const std::set<long> & getPhotos() const;
    const IdCollection & getSubCollections() const;
    IdCollection & getSubCollections();

    void setPhotos(const std::set<long> & photoIds);
    template<typename Iterator>
    void setPhotos(const Iterator & begin, const Iterator & end);
    void setSubCollections(const IdCollection & subcollectionIds);

    void clearPhotos();
    void putPhoto(long photoId);
    void removePhoto(long photoId);
    bool containsPhoto(long photoId) const;

    long getPhotoCover() const;
    void setPhotoCover(long photoId);
    void clearPhotoCover();

    bool hasPhotoContext() const;
    long getParentContext() const;
    void setParentContext(long id);

    virtual void serialize(Serializer & serializer) const;
    virtual void deserialize(const GenericContainer & deserializer);

    virtual bool equals(const PhotoCollection & other) const;
    friend bool operator==(const PhotoCollection & coll1, const PhotoCollection & coll2);
    friend bool operator!=(const PhotoCollection & coll1, const PhotoCollection & coll2);

protected:
    bool photoIsValid(long photoId) const;
};

template<typename Iterator>
std::map<long, PhotoCollection> PhotoCollection::mapById(const Iterator &begin, const Iterator & end)
{
    std::map<long, PhotoCollection> photocollectionMap;
    Iterator iter = begin;
    while(iter != end){
        const PhotoCollection & photoCollection = *iter;
        photocollectionMap[photoCollection.getId()] = photoCollection;

        ++iter;
    }
    return photocollectionMap;
}

template<typename PhotoIterator>
void PhotoCollection::setPhotos(const PhotoIterator &begin, const PhotoIterator &end){
    PhotoIterator iter = begin;
    while(iter != end){
        this->putPhoto(iter->getId());
        ++iter;
    }
}

}

#endif // PHOTOCOLLECTION_H
