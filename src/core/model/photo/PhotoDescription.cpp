/*
 * PhotoDescription.cpp
 *
 *  Created on: 03/08/2014
 *      Author: noface
 */

#include "PhotoDescription.h"


PhotoDescription::PhotoDescription(const std::string &description, const std::string &moment, const std::string &date, const std::string &place, const std::vector<PhotoMarker> &photoMarkers)
    : description (description)
    , moment (moment)
    , date (date)
    , place (place)
    , markers(photoMarkers)
{}

void PhotoDescription::fixPhotoMarkers(){
    if(markers.size() > 0){
        //Ordena marcadores (isto é feito para permitir comparação)
        sort(markers.begin(),markers.end());
        for(size_t i=0; i < markers.size(); ++i){
            markers[i].setIndex(i+1);
        }
    }
}

bool PhotoDescription::operator==(const PhotoDescription &other) const
{
    return this->description    == other.description
            && this->moment     == other.moment
            && this->date       == other.date
            && this->place      == other.place
            && this->markers    == other.markers
            && this->credits    == other.credits;
}

void PhotoDescription::set(const PhotoDescription &other){
    this->description = other.getDescription();
    this->moment = other.getMoment();
    this->date = other.getDate();
    this->place = other.getPlace();
    this->credits = other.getCredits();
    this->setMarkers(other.getMarkers());
}

PhotoDescription & PhotoDescription::setMarkers(const std::vector<PhotoMarker> &someMarkers) {
    markers.erase(markers.begin(), markers.end());
    //Substitui elementos de marker pelos elementos de someMarkers
    this->markers.insert(markers.begin(), someMarkers.begin(),someMarkers.end());
    this->fixPhotoMarkers();

    return *this;
}

const PhotoMarker &PhotoDescription::getMarker(const unsigned &markerPos) const{
    return this->markers.at(markerPos);
}

void PhotoDescription::addMarker(const PhotoMarker &marker){
    this->addMarker(marker, getMarkerCount());
}

void PhotoDescription::addMarker(const PhotoMarker &marker, int markerPos){
    if(markerPos >= (int)getMarkerCount()){
        this->markers.push_back(marker);
        this->markers.back().setIndex(getMarkerCount());
    }
    else{
        this->markers.insert(markers.begin() + markerPos, marker);
    }
}

void PhotoDescription::removeMarker(const unsigned &markerPos){
    if(markerPos < getMarkerCount()){
        this->markers.erase(this->markers.begin() + markerPos);
        this->fixPhotoMarkers();
    }
}

unsigned PhotoDescription::getMarkerCount() const{
    return this->markers.size();
}
