/*
 * Photo.cpp
 *
 *  Created on: 03/08/2014
 *      Author: noface
 */

#include "Photo.h"
#include "utils/Log.h"
#include "PhotoContext.h"

namespace calmframe {

const long Photo::UnknownContextId= PhotoContext::INVALID_ID;

Photo::Photo(const std::string & filename, const PhotoDescription & photoDescription)
    : Photo(UNKNOWN_ID, filename, photoDescription)
{}

Photo::Photo(long id, const std::string & path, const PhotoDescription & photoDescription)
    : id(id)
    , contextId(UnknownContextId)
    , imagePath(path)
    , photoDescription(photoDescription)
{}

Photo::Photo(const PhotoDescription & photoDescription, const PhotoMetadata & metadata, long contextId)
    : id(UNKNOWN_ID)
    , contextId(contextId)
    , imagePath()
    , photoDescription(photoDescription)
    , metadata(metadata)

{

}

long Photo::getContextId() const{
    return contextId;
}

void Photo::setContextId(long id){
    contextId = id;
}

bool Photo::operator<(const Photo &other) const
{
    //Considera apenas o id para tornar mais ágil ordenações
    return this->getId() < other.getId();
}

bool Photo::operator>(const Photo &other) const
{
    //Considera apenas o id para tornar mais ágil ordenações
    return this->getId() > other.getId();
}

bool Photo::operator==(const Photo &other) const
{
    if(this == &other){
        return true;
    }
    return other.getId() == this->getId()
            && other.getContextId() == this->getContextId()
            && other.getDescription() == this->getDescription()
            && other.getImagePath() == this->getImagePath();
}

bool Photo::operator!=(const Photo &other) const
{
    return !(*this == other);
}

void Photo::serialize(Serializer &serializer) const{
    if(getId() != UNKNOWN_ID){
        serializer.put(PhotoFields::ID         , this->getId());
    }

    serializer.put(PhotoFields::DATE       , this->getDescription().getDate());
    serializer.put(PhotoFields::MOMENT     , this->getDescription().getMoment());
    serializer.put(PhotoFields::PLACE      , this->getDescription().getPlace());
    serializer.put(PhotoFields::DESCRIPTION, this->getDescription().getDescription());
    serializer.put(PhotoFields::CREDITS    , this->getDescription().getCredits());

    if(getContextId() != UnknownContextId){
        serializer.put(PhotoFields::ContextId, this->getContextId());
    }

    serializer.putChild(PhotoFields::METADATA, this->getMetadata());
    serializer.putCollection(PhotoFields::MARKERS, this->getDescription().getMarkers().begin(), this->getDescription().getMarkers().end());
}


void Photo::deserialize(const GenericContainer & container){
    long id = container.get(PhotoFields::ID, this->getId()).asNumber<long>();
    if(id > UNKNOWN_ID){
        this->setId(id);
    }

    this->setContextId(container.get(PhotoFields::ContextId, getContextId()).asNumber<long>());

    PhotoDescription & description = this->getDescription();
    description.setDate       (container.get(PhotoFields::DATE       , description.getDate()       ).asString());
    description.setMoment     (container.get(PhotoFields::MOMENT     , description.getMoment()     ).asString());
    description.setDescription(container.get(PhotoFields::DESCRIPTION, description.getDescription()).asString());
    description.setPlace      (container.get(PhotoFields::PLACE      , description.getPlace()      ).asString());
    description.setCredits    (container.get(PhotoFields::CREDITS    , description.getCredits()    ).asString());
//    this->metadata.deserialize(container.get(PhotoFields::METADATA, GenericContainer()).as<GenericContainer>());



    deserializerMarkers(container);
}

void Photo::deserializerMarkers(const GenericContainer& container){
    if(container.contains(PhotoFields::MARKERS)){
        GenericIterator iterator = container.get(PhotoFields::MARKERS).as<GenericIterator>();
        std::vector<PhotoMarker> markers;

        int index =0;

        while(iterator.next()){
            PhotoMarker newPhotoMarker;
            newPhotoMarker.deserialize(iterator.getValue().as<GenericContainer>());
            newPhotoMarker.setPhotoId(this->getId());
            newPhotoMarker.setIndex(index + 1);
            ++index;

            markers.push_back(newPhotoMarker);
        }
        this->getDescription().setMarkers(markers);
    }
}
}

