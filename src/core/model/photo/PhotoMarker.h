/*
 * PhotoMark.h
 *
 *  Created on: 29/08/2014
 *      Author: noface
 */

#ifndef PHOTOMARK_H_
#define PHOTOMARK_H_

#include <string>
#include <iostream>
#include "serialization/Serializable.hpp"
#include "serialization/Serializer.h"
#include "serialization/Deserializable.hpp"

namespace PhotoMarkerFields{
    constexpr const char * const X              = "x";
    constexpr const char * const Y              = "y";
    constexpr const char * const IDENTIFICATION = "identification";
    constexpr const char * const INDEX          = "index";
}

class PhotoMarker : public Serializable
                  , public Deserializable{
private:
    /**Índice do marcador na foto*/
    unsigned index;
	std::string name;
	float relativeX;
	float relativeY;
	//Identificador da foto que contém este marcador
	long photoId;
public:
    PhotoMarker(
            std::string markerIdentification = "",
            float x = 0.0f,
            float y = 0.0f,
            long photoId = 0,
            int index=0);
	virtual ~PhotoMarker(){}

    const std::string & getName()      const;
    const float       & getRelativeX() const;
    const float       & getRelativeY() const;
    const long        & getPhotoId()   const;
    const unsigned    & getIndex()     const;

    void setName(std::string identification);
    void setRelativeX(float relativeX);
    void setRelativeY(float relativeY);
    void setPhotoId(long photoId);
    void setIndex(const unsigned &value);

    PhotoMarker & operator=(const PhotoMarker & other);

    void set(const PhotoMarker & other);

    bool operator==(const PhotoMarker & other) const;
    bool operator<(const PhotoMarker & other) const;

    virtual void serialize(Serializer & serializer) const;
    void deserialize(const GenericContainer & container);
};

#endif /* PHOTOMARK_H_ */




