#include "ConfigValue.h"

namespace calmframe {

ConfigValue::ConfigValue(const std::string & key, const std::string & value)
    : key(key), value(value)
{}

std::string ConfigValue::getKey() const
{
    return key;
}

void ConfigValue::setKey(const std::string & value)
{
    key = value;
}

std::string ConfigValue::getValue() const
{
    return value;
}

void ConfigValue::setValue(const std::string & value)
{
    this->value = value;
}

}//namespace
