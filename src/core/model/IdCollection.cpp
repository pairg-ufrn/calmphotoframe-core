#include "IdCollection.h"

namespace calmframe {


IdCollection::IdCollection()
{

}

IdCollection::IdCollection(const std::set<long> &ids)
    : ids(ids)
{}

const std::set<long> &IdCollection::getIds() const
{
    return ids;
}

void IdCollection::setIds(const std::set<long> &value)
{
    ids = value;
}

void IdCollection::clear()
{
    this->ids.clear();
}

void IdCollection::put(long id)
{
    this->ids.insert(id);
}

void IdCollection::remove(long id)
{
    this->ids.erase(id);
}

bool IdCollection::contains(long id) const
{
    return ids.find(id) != ids.end();
}

bool IdCollection::empty() const
{
    return ids.empty();
}

std::size_t IdCollection::size() const
{
    return this->ids.size();
}

bool IdCollection::operator==(const IdCollection &other) const{
    return this->getIds() == other.getIds();
}

bool IdCollection::operator!=(const IdCollection &other) const{
    return !(*this == other);
}


}//namespace

