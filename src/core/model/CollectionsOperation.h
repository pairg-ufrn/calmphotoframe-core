#ifndef COLLECTIONSOPERATION_H
#define COLLECTIONSOPERATION_H

#include "serialization/Deserializable.hpp"
#include "serialization/Serializable.hpp"

#include <set>
#include <string>

namespace calmframe {
namespace CollectionsOperationFields {
    static const char * OPERATION_TYPE  = "operation";
    static const char * PHOTO_ITEMS_IDS = "photoIds";
    static const char * SUBCOLLECTIONS_IDS = "subCollectionsIds";
    static const char * COLLECTIONS_TARGETS_IDS = "targetCollectionIds";
}//namespace
class CollectionsOperation : public Deserializable, public Serializable{
public:
    enum OperationTypes{UNKNOWN=0, ADD=1, REMOVE=2, MOVE=3};
private:
    std::set<long> photoIds;
    std::set<long> subCollectionsIds;
    std::set<long> targetCollectionsIds;

    OperationTypes operationType;
public:
    static const char *operationTypeToString(OperationTypes operationType);
    static OperationTypes operationTypeFromString(const std::string & str);
public:
    CollectionsOperation();

    void deserialize(const GenericContainer & container);
    void serialize(Serializer & serializer) const;

    const std::set<long> & getPhotoIds() const;
    void setPhotoIds(const std::set<long> &value);

    std::set<long> getSubCollectionsIds() const;
    void setSubCollectionsIds(const std::set<long> &value);

    const std::set<long> & getTargetCollectionIds() const;
    void setTargetCollectionIds(const std::set<long> &value);

    OperationTypes getOperationType() const;
    void setOperationType(const OperationTypes &value);

protected:
    void getIds(const GenericContainer& container, const std::string & member, std::set<long> & ids);
};

}//namespace

#endif // COLLECTIONSOPERATION_H
