#include "ServerInfo.h"

#include "serialization/Serializer.h"
#include "serialization/Deserializer.h"

#include <sstream>

#include "utils/Log.h"

namespace calmframe{

const ServerInfo::VersionNumbers & ServerInfo::getAppVersion() const{
    return appVersion;
}

void ServerInfo::setAppVersion(const VersionNumbers & value){
    appVersion = value;
}

const std::string & ServerInfo::getServerName() const
{
    return serverName;
}

void ServerInfo::setServerName(const std::string & value)
{
    serverName = value;
}

std::string ServerInfo::getVersionString() const{
    std::stringstream out;
    out << appVersion[0] << "." << appVersion[1] << "." << appVersion[2];
    return out.str();
}

void ServerInfo::serialize(Serializer & serializer) const{
    serializer.put(Fields::serverName, getServerName());
    serializer.put(Fields::version, getVersionString());
    serializer.putCollection(Fields::versionNumbers, getAppVersion().begin(), getAppVersion().end());
}

void ServerInfo::deserialize(const GenericContainer & deserializer){
    setServerName(deserializer.get(Fields::serverName, getServerName()).asString());
    
    auto iter = deserializer.get(Fields::versionNumbers, GenericIterator()).as<GenericIterator>();
    if(!iter.empty()){
        iter.toBegin();
        int i =0;
        while(iter.next() && i < 3){
            Value v = iter.getValue();
            if(!v.isEmpty()){
                this->appVersion[i] = iter.getValue().asNumber(-1);
            }
            ++i;
        }
    }
}

bool ServerInfo::operator==(const ServerInfo & serverInfo) const{
    return getServerName() == serverInfo.getServerName()
        && getAppVersion() == serverInfo.getAppVersion();
}

}//namespace
