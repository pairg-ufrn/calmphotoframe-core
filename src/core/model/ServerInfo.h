#ifndef CALMFRAME_SERVER_INFO_H
#define CALMFRAME_SERVER_INFO_H

#include <array>

#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"

namespace calmframe {
class ServerInfo : public Serializable, public Deserializable{
public:
    struct Fields{
        static constexpr const char * const version        = "version";
        static constexpr const char * const versionNumbers = "versionNumbers";
        static constexpr const char * const serverName     = "serverName";
    };
public:
    using VersionNumbers = std::array<int, 3>;
    
public:
    const VersionNumbers & getAppVersion() const;
    void setAppVersion(const VersionNumbers & value);
    
    const std::string & getServerName() const;
    void setServerName(const std::string & value);
    
    std::string getVersionString() const;
    
    void serialize(Serializer & serializer) const override;
    void deserialize(const GenericContainer & deserializer) override;
    
    bool operator==(const ServerInfo & serverInfo) const;
private:
    std::string serverName;
    VersionNumbers appVersion;
};

}//namespace

#endif // CALMFRAME_SERVER_INFO_H
