#ifndef IDCOLLECTION_H
#define IDCOLLECTION_H

#include <set>

namespace calmframe {

class IdCollection
{
public:
    typedef std::set<long> Collection;

private:
    Collection ids;
public:
    IdCollection();
    IdCollection(const Collection & ids);

    const Collection & getIds() const;
    void setIds(const Collection &value);

    void clear();
    void put(long id);
    void remove(long id);

    bool contains(long id) const;
    bool empty() const;
    std::size_t size() const;

    bool operator==(const IdCollection & other) const;
    bool operator!=(const IdCollection & other) const;
};

}//namespace

#endif // IDCOLLECTION_H
