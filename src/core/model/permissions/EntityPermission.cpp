#include "EntityPermission.h"

#include "serialization/Serializer.h"

namespace calmframe {

EntityPermission::EntityPermission(long userId, PermissionLevel permissionLevel)
    : userId(userId)
    , permissionLevel(permissionLevel)
{}

long EntityPermission::getUserId() const {
    return userId;
}
void EntityPermission::setUserId(long userId){
    this->userId = userId;
}

PermissionLevel EntityPermission::getPermissionLevel() const {
    return permissionLevel;
}
void EntityPermission::setPermissionLevel(PermissionLevel level){
    this->permissionLevel = level;
}

void EntityPermission::serialize(Serializer & serializer) const{
    serializer.put(Fields::permissionLevel, calmframe::toString(getPermissionLevel()));
    serializer.put(Fields::userId, getUserId());
}

void EntityPermission::deserialize(const GenericContainer & deserializer){
    setUserId(deserializer.get(Fields::userId).asNumber<long>(getUserId()));

    PermissionLevel level = calmframe::fromString(deserializer.get(Fields::permissionLevel).asString());
    if(level != PermissionLevel::None){
        setPermissionLevel(level);
    }
}

}//namespace
