#ifndef PERMISSIONLEVEL_H
#define PERMISSIONLEVEL_H

#include <ostream>

namespace calmframe {

enum class PermissionLevel{
    None  = 0,
    Read  = 1,
    Write = 2,
    Admin = 3
};

PermissionLevel fromString(std::string permissionStr);
const char * toString(PermissionLevel level);

/** Operator to convert permissionLevel on string.*/
std::ostream & operator<<(std::ostream & out, PermissionLevel permissionLevel);

}//namespace

#endif // PERMISSIONLEVEL_H

