#ifndef PERMISSION_H
#define PERMISSION_H

#include "../photo/Visibility.h"
#include "PermissionLevel.h"

namespace calmframe {

class Permission{
public:
    static const int InvalidId;
    static PermissionLevel fromVisibility(const Visibility & visibility);
public:
    Permission(long ctxId=InvalidId, long userId=InvalidId, PermissionLevel permLevel = PermissionLevel::None);

    long getContextId() const;
    void setContextId(long value);

    long getUserId() const;
    void setUserId(long value);

    PermissionLevel getPermissionLevel() const;
    void setPermissionLevel(const PermissionLevel & value);

    bool operator<(const Permission & other) const;
    bool operator==(const Permission & other) const;
private:
    long contextId, userId;
    PermissionLevel permissionLevel;
};

}//namespace

#endif // PERMISSION_H
