#include "Permission.h"

namespace calmframe {

const int Permission::InvalidId = -1;

PermissionLevel Permission::fromVisibility(const Visibility & visibility){
    if(visibility == Visibility::EditableByOthers){
        return PermissionLevel::Write;
    }
    else if(visibility == Visibility::VisibleByOthers){
        return PermissionLevel::Read;
    }
    else{
        return PermissionLevel::None;
    }
}

Permission::Permission(long ctxId, long userId, PermissionLevel permLevel)
    : contextId(ctxId)
    , userId(userId)
    , permissionLevel(permLevel)
{}

long Permission::getContextId() const{
    return contextId;
}
void Permission::setContextId(long value){
    contextId = value;
}

long Permission::getUserId() const{
    return userId;
}
void Permission::setUserId(long value){
    userId = value;
}

PermissionLevel Permission::getPermissionLevel() const{
    return permissionLevel;
}
void Permission::setPermissionLevel(const PermissionLevel & value){
    permissionLevel = value;
}

bool Permission::operator<(const Permission & other) const
{
    return getContextId() < other.getContextId()
            || (getContextId() == other.getContextId() && getUserId() < other.getUserId())
            || getPermissionLevel() < other.getPermissionLevel();
}

bool Permission::operator==(const Permission & other) const{
    return getContextId() == other.getContextId()
            && getUserId() == other.getUserId()
            && getPermissionLevel() == other.getPermissionLevel();
}

}//namespace

