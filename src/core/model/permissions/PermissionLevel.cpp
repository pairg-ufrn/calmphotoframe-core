#include "PermissionLevel.h"

#include <algorithm>
#include <cstring>

namespace calmframe {

namespace PermissionLevelNames {
    typedef const char * const ConstantCStr;
    constexpr ConstantCStr Admin = "Admin";
    constexpr ConstantCStr Read="Read";
    constexpr ConstantCStr Write="Write";
}//namespace


const char * toString(PermissionLevel permissionLevel){
    switch (permissionLevel) {
    case PermissionLevel::Admin:
        return PermissionLevelNames::Admin;
    case PermissionLevel::Write:
        return PermissionLevelNames::Write;
    case PermissionLevel::Read:
        return PermissionLevelNames::Read;
    default:
        return "";
    }
}

PermissionLevel fromString(std::string permissionStr){
    if(permissionStr.empty()){
        return PermissionLevel::None;
    }

    permissionStr[0] = toupper(permissionStr[0]);
    std::transform(permissionStr.begin() + 1, permissionStr.end(), permissionStr.begin() + 1, ::tolower);

    if(strcmp(permissionStr.c_str(), PermissionLevelNames::Read) == 0){
        return PermissionLevel::Read;
    }
    else if(strcmp(permissionStr.c_str(), PermissionLevelNames::Write) == 0){
        return PermissionLevel::Write;
    }
    else if(strcmp(permissionStr.c_str(), PermissionLevelNames::Admin) == 0){
        return PermissionLevel::Admin;
    }

    return PermissionLevel::None;
}

std::ostream &operator<<(std::ostream & out, PermissionLevel permissionLevel){
    out << toString(permissionLevel);
    return out;
}

}//namespace
