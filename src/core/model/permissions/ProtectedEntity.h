#ifndef PROTECTEDENTITY_H
#define PROTECTEDENTITY_H

#include "EntityPermission.h"
#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"
#include "serialization/Serializer.h"

namespace calmframe {

namespace ProtectedEntityFields {
    const char * const permission = "permission";
}//namespace


/** Encapsulates some entity with a given permission level*/
template<typename T>
class ProtectedEntity : public Serializable, public Deserializable{
private:
    EntityPermission permission;
    T entity;
public:
    ProtectedEntity()
        : ProtectedEntity(-1, PermissionLevel::None, T{})
    {}

    ProtectedEntity(const Permission & permission, const T & entity)
        : ProtectedEntity(permission.getUserId(), permission.getPermissionLevel(), entity)
    {}

    ProtectedEntity(long userId, PermissionLevel permissionLevel, const T & entity)
        : permission(userId, permissionLevel)
        , entity(entity)
    {}

    long            getUserId()          const { return permission.getUserId();}
    PermissionLevel getPermissionLevel() const { return permission.getPermissionLevel();}
    const T &       getEntity()          const { return entity; }

    void setUserId          (long userId)          { permission.setUserId(userId);}
    void setPermissionLevel (PermissionLevel level){ permission.setPermissionLevel(level);}
    void setEntity          (const T & entity)     { this->entity = entity; }

    operator const T&() const{
        return getEntity();
    }

    operator const Serializable&() const{
        return *this;
    }

    void serialize(Serializer & serializer) const override{
        getEntity().serialize(serializer);
        serializer.putChild(ProtectedEntityFields::permission, permission);
    }

    void deserialize(const GenericContainer & deserializer) override{
        this->entity.deserialize(deserializer);
        if(deserializer.contains(ProtectedEntityFields::permission)){
            permission.deserialize(deserializer.get(ProtectedEntityFields::permission, GenericContainer()).as<GenericContainer>());
        }
    }
};

}//namespace

#endif // PROTECTEDENTITY_H

