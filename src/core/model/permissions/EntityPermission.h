#ifndef ENTITYPERMISSION_H
#define ENTITYPERMISSION_H

#include "Permission.h"
#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"

namespace calmframe {

class EntityPermission : public Serializable, public Deserializable{
public:
    class Fields{
    public:
        static constexpr const char * const permissionLevel = "level";
        static constexpr const char * const userId = "userId";
    };

    EntityPermission(long userId, PermissionLevel permissionLevel);

    long            getUserId()          const;
    PermissionLevel getPermissionLevel() const;

    void setUserId          (long userId);
    void setPermissionLevel (PermissionLevel level);

    void serialize(Serializer & serializer) const override;
    void deserialize(const GenericContainer & deserializer) override;
private:
    long userId;
    PermissionLevel permissionLevel;
};


}//namespace
#endif // ENTITYPERMISSION_H
