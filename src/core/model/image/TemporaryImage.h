#ifndef TEMPORARYIMAGE_H
#define TEMPORARYIMAGE_H

#include "Image.h"

namespace calmframe {

class TemporaryImage : public Image
{
    typedef Image super;
public:
    TemporaryImage(std::string imagePath = std::string());
    TemporaryImage(const Image & other);
    TemporaryImage(Image && other);

    virtual ~TemporaryImage();

    /** Avoid deletion of image*/
    void keep();

    /** Closes the content stream (if open) and clean the image variables */
    void clean();

    /** Closes the image stream and destroy the file pointed by the imagePath.*/
    virtual void destroy();

private:
    bool keepImage;
};
}//namespace

#endif // TEMPORARYIMAGE_H
