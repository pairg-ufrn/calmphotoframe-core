#ifndef SIZE_H
#define SIZE_H

namespace calmframe {

class Size{
public:
    Size(unsigned width=0, unsigned height=0);
    
    unsigned width() const;
    Size & width(unsigned width);
    
    unsigned height() const;
    Size & height(unsigned height);
    
private:
    unsigned _width, _height;
};

}//namespace

#endif // SIZE_H
