#include "TemporaryImage.h"
#include "utils/FileUtils.h"

using namespace calmframe::utils;

namespace calmframe {

TemporaryImage::TemporaryImage(std::string imagePath)
    : super(imagePath)
    , keepImage(false)
{}

TemporaryImage::TemporaryImage(const Image &other)
    : super(other)
    , keepImage(false)
{}

TemporaryImage::TemporaryImage(Image && other)
    : super(other)
    , keepImage(false)
{}

TemporaryImage::~TemporaryImage(){
    this->destroy();
}

void TemporaryImage::keep(){
    this->keepImage = true;
}

void TemporaryImage::clean(){
    this->close();
    this->setImagePath(std::string());
    this->setType(std::string());
}

void TemporaryImage::destroy(){
    if(isOpen()){
        close();
    }
    if(!keepImage && Files().exists(this->getImagePath())){
        Files().remove(getImagePath());
    }
}

}//namespace
