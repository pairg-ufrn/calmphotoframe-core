#ifndef IMAGE_H
#define IMAGE_H

#include <string>
#include <istream>

namespace calmframe {

class Image{
private:
    std::string imagePath;
    std::string imageType;
    std::istream * imageStream;
public:
    Image(std::string imagePath = std::string());
    Image(const Image & other);
    Image(Image && other);
    Image & operator=(const Image & image);
    Image & operator=(Image && image);
    virtual void set(const Image & other);
    virtual void move(Image && other);

    virtual ~Image();
public: //Getters and Setters
    const std::string & getImagePath() const    { return imagePath; }
    std::string getImagePath()                  { return imagePath; }
    std::string getType() const                 { return imageType; }

    void setImagePath(const std::string &value) { imagePath = value; }
    void setType(const std::string &value)      { imageType = value; }
public:
    virtual bool isValid() const;
    virtual bool isOpen() const;
    virtual bool open();
    virtual std::istream & getContent();
    virtual void close();
protected:
    virtual bool isValidPath(const std::string & path) const;
    virtual std::istream * createStream(const std::string & path);
    virtual void destroyStream(std::istream * stream);
};

}//namespace

#endif // IMAGE_H
