#include "Size.h"

namespace calmframe {

Size::Size(unsigned width, unsigned height)
    : _width(width)
    , _height(height)
{}

unsigned Size::width() const{
    return _width;
}

Size & Size::width(unsigned width){
    _width = width;
    return *this;
}

unsigned Size::height() const{
    return _height;
}

Size & Size::height(unsigned height){
    _height = height;
    return *this;
}

}//namespace
