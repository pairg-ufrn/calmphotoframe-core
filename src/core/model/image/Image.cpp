#include "Image.h"

#include <fstream>
#include <exception>
#include "utils/IllegalStateException.h"
#include "utils/FileUtils.h"
#include "utils/Log.h"

using namespace calmframe::utils;
using namespace CommonExceptions;

namespace calmframe {

Image::Image(std::string imagePath)
    : imagePath(imagePath)
    , imageStream(NULL)
{}

Image::Image(const Image &other)
    : imagePath(std::string())
    , imageStream(NULL)
{
    this->set(other);
}

Image::Image(Image && other)
    : Image()
{
    this->move(std::move(other));
}

Image &Image::operator=(const Image &image){
    this->set(image);
    return *this;
}

Image & Image::operator=(Image && image){
    this->move(std::move(image));
    return *this;
}

void Image::set(const Image &other){
    this->imagePath = other.getImagePath();
    this->imageType = other.getType();
    this->close();
}

void Image::move(Image && other){
    this->imagePath = std::move(other.imagePath);
    this->imageStream = other.imageStream;
    other.imageStream = NULL;
    this->imageType = std::move(other.imageType);
}

Image::~Image(){
    try{
        this->close();
    }
    catch(std::exception & ex){
        logError("In Image::~Image:\t"
                 "Ocurred an exception: \"%s\"", ex.what());
    }
    catch(...){
        logError("In Image::~Image:\t"
                 "Ocurred an unknown error.");
    }
}

bool Image::isValid() const {
    return this->isValidPath(this->getImagePath());
}

bool Image::isOpen() const{
    return imageStream != NULL;
}

bool Image::open(){
    if(!isOpen()){
        imageStream = this->createStream(this->getImagePath());
    }
    return isOpen();
}

std::istream &Image::getContent(){
    if(!isOpen()){
        throw IllegalStateException("Should not getContent on closed image.");
    }
    return *this->imageStream;
}

void Image::close(){
    if(isOpen()){
        this->destroyStream(imageStream);
        imageStream = NULL;
    }
}

bool Image::isValidPath(const std::string &path) const{
    return !path.empty() && FileUtils::instance().isFile(path);
}
std::istream *Image::createStream(const std::string &path){
    return new std::ifstream(path.c_str());
}
void Image::destroyStream(std::istream *stream){
    if(stream != NULL){
        delete stream;
    }
}

}//namespace
