#ifndef SCOPEDOPEN_H
#define SCOPEDOPEN_H

#include "Image.h"

namespace calmframe {

/** Opens image on creation and closes it on destruction. */
class ScopedOpen
{
private:
    Image & img;
public:
    ScopedOpen(Image & img);
    ~ScopedOpen();

    void close();
};

}//namespace

#endif // SCOPEDOPEN_H
