#include "ImageExternalStream.h"

#include "utils/NullPointerException.hpp"

namespace calmframe {

ImageExternalStream::ImageExternalStream(std::istream *inputStream)
    : imageStream(inputStream)
{
    CommonExceptions::assertNotNull(inputStream, "In ImageExternalStream::ImageExternalStream:\t"
                                   "Parameter inputStream should not be NULL.");
}

bool ImageExternalStream::isValid() const
{
    return this->imageStream->good();
}

std::istream *ImageExternalStream::createStream(const std::string &)
{
    return this->imageStream;
}

void ImageExternalStream::destroyStream(std::istream *)
{
}

}//namespace
