#ifndef IMAGEEXTERNALSTREAM_H
#define IMAGEEXTERNALSTREAM_H

#include <istream>
#include "Image.h"

namespace calmframe {

class ImageExternalStream : public Image
{
private:
    std::istream * imageStream;
public:
    ImageExternalStream(std::istream * inputStream);

/* **************************Overriden methods************************************ */
public:
    virtual bool isValid() const;
protected:
    virtual std::istream * createStream(const std::string & path);
    virtual void destroyStream(std::istream * stream);

};
}//namespace

#endif // IMAGEEXTERNALSTREAM_H
