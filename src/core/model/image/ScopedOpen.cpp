#include "ScopedOpen.h"

namespace calmframe {

ScopedOpen::ScopedOpen(Image & img)
    : img(img)
{
    img.open();
}

ScopedOpen::~ScopedOpen(){
    close();
}

void ScopedOpen::close(){
    if(img.isOpen()){
        img.close();
    }
}

}//namespace

