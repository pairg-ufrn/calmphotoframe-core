#include "CollectionsOperation.h"

#include "serialization/Serializer.h"
#include "serialization/GenericContainer.h"
#include "serialization/GenericIterator.h"

#include "utils/StringUtils.h"

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;

namespace calmframe {

namespace Constants {
    static const char * AddOperationType     = "add";
    static const char * RemoveOperationType  = "remove";
    static const char * MoveOperationType    = "move";
    static const char * UnknownOperationType = "unknown";
}//namespace
const char * CollectionsOperation::operationTypeToString(CollectionsOperation::OperationTypes operationType)
{
    switch (operationType) {
    case ADD:
        return Constants::AddOperationType;
    case REMOVE:
        return Constants::RemoveOperationType;
    case MOVE:
        return Constants::MoveOperationType;
    default:
        return Constants::UnknownOperationType;
    }
}

CollectionsOperation::OperationTypes CollectionsOperation::operationTypeFromString(const string &str)
{
    if(StringUtils::instance().caseInsensitiveEquals(str, Constants::AddOperationType)){
        return ADD;
    }
    else if(StringUtils::instance().caseInsensitiveEquals(str, Constants::RemoveOperationType) ){
        return REMOVE;
    }
    else if(StringUtils::instance().caseInsensitiveEquals(str, Constants::MoveOperationType) ){
        return MOVE;
    }
    else{
        return UNKNOWN;
    }
}

CollectionsOperation::CollectionsOperation()
: operationType(UNKNOWN)
{}

std::set<long> CollectionsOperation::getSubCollectionsIds() const{
    return subCollectionsIds;
}
void CollectionsOperation::setSubCollectionsIds(const std::set<long> &value){
    subCollectionsIds = value;
}
const std::set<long> &CollectionsOperation::getTargetCollectionIds() const{
    return targetCollectionsIds;
}
void CollectionsOperation::setTargetCollectionIds(const std::set<long> &value){
    targetCollectionsIds = value;
}
const std::set<long> &CollectionsOperation::getPhotoIds() const{
    return photoIds;
}
void CollectionsOperation::setPhotoIds(const std::set<long> &value){
    photoIds = value;
}

CollectionsOperation::OperationTypes CollectionsOperation::getOperationType() const{
    return operationType;
}

void CollectionsOperation::setOperationType(const OperationTypes &value){
    operationType = value;
}

void CollectionsOperation::deserialize(const GenericContainer &container){

    if(container.contains(CollectionsOperationFields::OPERATION_TYPE)){
        string operationStr = container.get(CollectionsOperationFields::OPERATION_TYPE,string())
                                       .asString();
        this->operationType = operationTypeFromString(operationStr);
    }
    getIds(container, CollectionsOperationFields::PHOTO_ITEMS_IDS, photoIds);
    getIds(container, CollectionsOperationFields::SUBCOLLECTIONS_IDS, subCollectionsIds);
    getIds(container, CollectionsOperationFields::COLLECTIONS_TARGETS_IDS, targetCollectionsIds);
}

void CollectionsOperation::serialize(Serializer &serializer) const
{
    serializer.put(CollectionsOperationFields::OPERATION_TYPE, operationTypeToString(this->getOperationType()));
    serializer.putCollection(CollectionsOperationFields::COLLECTIONS_TARGETS_IDS, targetCollectionsIds.begin(), targetCollectionsIds.end());
    serializer.putCollection(CollectionsOperationFields::PHOTO_ITEMS_IDS, photoIds.begin(), photoIds.end());
    serializer.putCollection(CollectionsOperationFields::SUBCOLLECTIONS_IDS, subCollectionsIds.begin(), subCollectionsIds.end());

}

void CollectionsOperation::getIds(const GenericContainer &container, const std::string &member, std::set<long> &ids)
{
    if(container.get(member).isTypeOf<GenericIterator>()){
        GenericIterator idsIter = container.get(member).as<GenericIterator>();

        std::insert_iterator<set<long> > iter = std::inserter< set<long> >(ids, ids.end());
        idsIter.asCollection(iter, GenericIterator::NumberConverter<long>());
    }
}

}//namespace
