#ifndef CONFIGVALUE_H
#define CONFIGVALUE_H

#include <string>

namespace calmframe {

class ConfigValue{
public:
    ConfigValue(const std::string & key = std::string(), const std::string & value = std::string());

    std::string getKey() const;
    void setKey(const std::string & value);
    
    std::string getValue() const;
    void setValue(const std::string & value);
    
private:
    std::string key, value;
};

}//namespace

#endif // CONFIGVALUE_H
