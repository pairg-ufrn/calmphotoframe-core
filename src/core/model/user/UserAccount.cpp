#include "UserAccount.h"

namespace calmframe {

const long UserAccount::INVALID_ID = User::INVALID_ID;

UserAccount::UserAccount()
    : user(INVALID_ID)
{}
UserAccount::~UserAccount()
{}

User &UserAccount::getUser(){
    return user;
}

const User &UserAccount::getUser() const{
    return user;
}

const std::string & UserAccount::getPassSalt() const{
    return passSalt;
}
UserAccount& UserAccount::setPassSalt(const std::string &value){
    passSalt = value;
    return *this;
}

long UserAccount::getId() const{
    return user.getId();
}

bool UserAccount::isActive() const{
    return getUser().isActive();
}
bool UserAccount::isAdmin() const{
    return getUser().isAdmin();
}

const std::string & UserAccount::getPassHash() const{
    return passHash;
}
const std::string & UserAccount::getLogin() const{
    return user.getLogin();
}

UserAccount& UserAccount::setId(long id){
    user.setId(id);
    return *this;
}

UserAccount& UserAccount::setActive(bool active){
    getUser().setActive(active);
    return *this;
}

UserAccount& UserAccount::setIsAdmin(bool isAdmin){
    getUser().setIsAdmin(isAdmin);
    return *this;
}
UserAccount& UserAccount::setPassHash(const std::string &value){
    passHash = value;
    return *this;
}
UserAccount& UserAccount::setLogin(const std::string &value){
    user.setLogin(value);
    return *this;
}

} //namespace


