#ifndef USERINFO_H
#define USERINFO_H

#include <string>

#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"

namespace calmframe {

class User;

//TODO: unify User and UserInfo

class UserInfo : public Serializable, public Deserializable{
public:
    UserInfo();
    UserInfo(long id, const std::string & login = std::string());
    UserInfo(const User & user);

    long getUserId() const;
    void setUserId(long value);

    const std::string & getLogin() const;
    void setLogin(const std::string & value);
    
    const std::string & getName() const;
    void setName(const std::string & value);
    
    const std::string & getProfileImage() const;
    void setProfileImage(const std::string & value);

    bool hasProfileImage() const;

    void serialize(Serializer &serializer) const override;
    void deserialize(const GenericContainer &deserializer) override;

    bool operator==(const UserInfo & other) const;
    
private:
    long userId;
    std::string login, name, profileImage;
};

}//namespace

#endif // USERINFO_H
