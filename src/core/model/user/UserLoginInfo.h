#ifndef USERLOGININFO_H
#define USERLOGININFO_H

#include <string>

#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"

namespace Fields{
namespace UserLoginInfo {
    static const char * login = "login";
    static const char * password = "password";
}
}

namespace calmframe {

class UserLoginInfo : public Serializable, public Deserializable
{
private:
    std::string login;
    std::string password;
public:
    UserLoginInfo(const std::string & login="", const std::string & password="");
    virtual ~UserLoginInfo();

    std::string getLogin   () const;
    std::string getPassword() const;
    void setLogin   (const std::string &value);
    void setPassword(const std::string &value);

    virtual void serialize(Serializer & serializer) const;
    virtual void deserialize(const GenericContainer & deserializer);
};

}//namespace

#endif // USERLOGININFO_H
