#include "UserInfo.h"

#include "serialization/Serializer.h"
#include "../user/User.h"

namespace calmframe {

UserInfo::UserInfo()
    : userId(User::INVALID_ID)
{}

UserInfo::UserInfo(long id, const std::string & login)
    : userId(id)
    , login(login)
{}

UserInfo::UserInfo(const User & user)
    : userId(user.getId())
    , login(user.getLogin())
    , name(user.name())
    , profileImage(user.profileImage())
{}

long UserInfo::getUserId() const{
    return userId;
}

void UserInfo::setUserId(long value){
    userId = value;
}

const std::string & UserInfo::getLogin() const{
    return login;
}

void UserInfo::setLogin(const std::string & value){
    login = value;
}

const std::string & UserInfo::getName() const{
    return name;
}

void UserInfo::setName(const std::string & value){
    name = value;
}

const std::string & UserInfo::getProfileImage() const{
    return profileImage;
}

void UserInfo::setProfileImage(const std::string & value){
    profileImage = value;
}

bool UserInfo::hasProfileImage() const{
    return !getProfileImage().empty();
}

void UserInfo::serialize(Serializer & serializer) const{
    serializer.put(User::Fields::id, getUserId());
    serializer.put(User::Fields::login, getLogin());
    serializer.put(User::Fields::name, getName());
    serializer.put(User::Fields::hasProfileImage, hasProfileImage());
}

void UserInfo::deserialize(const GenericContainer & deserializer){
    setUserId(deserializer.get(User::Fields::id).asNumber<long>(getUserId()));
    setLogin(deserializer.get(User::Fields::login).asString(getLogin()));
    setName(deserializer.get(User::Fields::name).asString(getName()));
}

bool UserInfo::operator==(const UserInfo & other) const{
    return getUserId() == other.getUserId()
            && getLogin() == other.getLogin()
            && getName() == other.getName();
}

}//namespace
