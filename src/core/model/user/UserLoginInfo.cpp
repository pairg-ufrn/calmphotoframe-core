#include "UserLoginInfo.h"
#include "serialization/Serializer.h"

namespace calmframe {

UserLoginInfo::UserLoginInfo(const std::string &_login, const std::string & _password)
    : login(_login)
    , password(_password)
{}
UserLoginInfo::~UserLoginInfo()
{}

std::string UserLoginInfo::getLogin() const{
    return login;
}
std::string UserLoginInfo::getPassword() const{
    return password;
}

void UserLoginInfo::setLogin(const std::string &value){
    login = value;
}
void UserLoginInfo::setPassword(const std::string &value){
    password = value;
}

void UserLoginInfo::serialize(Serializer &serializer) const{
    serializer.put(Fields::UserLoginInfo::login, this->getLogin());
    serializer.put(Fields::UserLoginInfo::password, this->getPassword());
}
void UserLoginInfo::deserialize(const GenericContainer &deserializer){
    this->login    = deserializer.get(Fields::UserLoginInfo::login   ,"").asString();
    this->password = deserializer.get(Fields::UserLoginInfo::password,"").asString();
}

}//namespace
