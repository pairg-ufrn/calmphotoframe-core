#include "Session.h"
#include "utils/TimeUtils.h"

#include "UserAccount.h"

namespace calmframe {

const long Session::INVALID_ID = 0;
const long Session::INVALID_USERID = User::INVALID_ID;
const long Session::unlimitedExpiration = -1;

Session::Session()
    : Session(0)
{}

Session::Session(long expireTime, const std::string & token, long userId, long id)
    : token(token)
    , expireTimestamp(expireTime)
    , userId(userId)
    , id(id)
{}

Session::~Session()
{}

long Session::getId() const
{
    return id;
}

void Session::setId(long value)
{
    id = value;
}

std::string Session::getToken() const{
    return token;
}

void Session::setToken(const std::string &value){
    token = value;
}

long Session::getExpireTimestamp() const{
    return expireTimestamp;
}

void Session::setExpireTimestamp(long value)
{
    expireTimestamp = value;
}

long Session::getUserId() const
{
    return userId;
}

void Session::setUserId(long value)
{
    userId = value;
}

bool Session::expired() const
{
    return !isUnlimited()
            && getExpireTimestamp() <= TimeUtils::instance().getTimestamp();
}

bool Session::isUnlimited() const{
    return getExpireTimestamp() == unlimitedExpiration;
}

bool Session::operator==(const Session &other) const
{
    return other.getUserId() == this->getUserId()
            && other.getExpireTimestamp() == this->getExpireTimestamp()
            && other.getToken() == this->getToken();
}

}//namespace

