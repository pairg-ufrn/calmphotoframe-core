#ifndef USERPROFILE_H
#define USERPROFILE_H

#include <string>

#include "serialization/Serializable.hpp"
#include "serialization/Deserializable.hpp"

namespace calmframe {

class User : public Serializable, public Deserializable{
public:
    struct Fields{
        static constexpr const char * id = "id";
        static constexpr const char * login  = "login";
        static constexpr const char * name   = "name";
        static constexpr const char * active = "active";
        static constexpr const char * isAdmin  = "isAdmin";
        static constexpr const char * hasProfileImage = "hasProfileImage";
    };

public:
    static const long INVALID_ID;

    User(long id, const std::string & login=std::string());
    User(const std::string & login=std::string(), 
         const std::string & name=std::string(), 
         bool active=true, 
         bool admin=false);

    long getId() const;
    void setId(long value);

    const std::string & getLogin() const;
    void setLogin(const std::string & value);
    
    const std::string & name() const;
    User & name(const std::string & value);
    
    std::string profileImage() const;
    User & profileImage(const std::string & profileImage);

    bool isActive() const;
    void setActive(bool value);

    bool isAdmin() const;
    void setIsAdmin(bool value);

    virtual void serialize(Serializer & serializer) const;
    virtual void deserialize(const GenericContainer & deserializer);

    bool operator==(const User & other) const;
    
private:
    long id;
    std::string login, _name, _profileImage;
    bool active;
    bool admin;
};

}//namespace

#endif // USERPROFILE_H
