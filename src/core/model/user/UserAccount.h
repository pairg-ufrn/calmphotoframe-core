#ifndef USER_ACCOUNT_H
#define USER_ACCOUNT_H

#include <string>

#include "User.h"

namespace calmframe{
class UserAccount
{
public:
    static const long INVALID_ID;
private:
    User user;

    std::string passHash;
    std::string passSalt;
public:
    UserAccount();
    ~UserAccount();

public:
    User & getUser();
    const User & getUser() const;

    long                getId()       const;
    bool                isActive()    const;
    bool                isAdmin()     const;
    const std::string & getPassHash() const;
    const std::string & getLogin()    const;
    const std::string & getPassSalt() const;

    UserAccount& setId      (long id);
    UserAccount& setActive  (bool active);
    UserAccount& setIsAdmin (bool isAdmin);
    UserAccount& setPassHash(const std::string &value);
    UserAccount& setLogin    (const std::string &value);
    UserAccount& setPassSalt(const std::string &value);
};
}

#endif // USER_ACCOUNT_H
