#ifndef SESSION_H
#define SESSION_H

#include <string>

namespace calmframe {

namespace FieldNames{
namespace Session{
    static const char * EXPIRE_TIMESTAMP = "expire";
}
}

class Session
{
public:
    static const long INVALID_ID;
    static const long INVALID_USERID;

    static const long unlimitedExpiration;
private:
    std::string token;
    long expireTimestamp;
    long userId;
    long id;
public:
    Session();
    Session(long expireTime,
            const std::string & token=std::string(),
            long userId=INVALID_USERID,
            long id=INVALID_ID);
    ~Session();

public:
    std::string getToken() const;
    void setToken(const std::string &value);
    long getExpireTimestamp() const;
    void setExpireTimestamp(long value);
    long getUserId() const;
    void setUserId(long value);
public:
    bool expired() const;
    bool isUnlimited() const;
public:
    bool operator==(const Session & other) const;
    long getId() const;
    void setId(long value);
};

}

#endif // SESSION_H
