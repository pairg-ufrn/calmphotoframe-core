#include "User.h"

#include "serialization/Serializer.h"
#include "serialization/GenericContainer.h"

#include <limits>

namespace calmframe {

const long User::INVALID_ID = INT32_MIN;

User::User(long id, const std::string & login)
    : User(login)
{
    this->id = id;
}

User::User(const std::string & login, const std::string & name, bool active, bool admin)
    : id(INVALID_ID)
    , login(login)
    , _name(name)
    , active(active)
    , admin(admin)
{
    
}

long User::getId() const{
    return id;
}

void User::setId(long value){
    id = value;
}

const std::string & User::getLogin() const{
    return login;
}

void User::setLogin(const std::string & value){
    login = value;
}

const std::string &User::name() const{
    return _name;
}

User &User::name(const std::string & value){
    _name = value;
    return *this;
}

bool User::isActive() const{
    return active;
}

void User::setActive(bool value){
    active = value;
}

bool User::isAdmin() const{
    return admin;
}

void User::setIsAdmin(bool value){
    admin = value;
}

std::string User::profileImage() const{
    return _profileImage;
}

User & User::profileImage(const std::string & profileImage){
    _profileImage = profileImage;
    return *this;
}

void User::serialize(Serializer & serializer) const
{
    serializer.put(User::Fields::id, getId());
    serializer.put(User::Fields::login, getLogin());
    serializer.put(User::Fields::active, isActive());
    serializer.put(User::Fields::isAdmin, isAdmin());
    serializer.put(User::Fields::name, name());
    serializer.put(Fields::hasProfileImage, !profileImage().empty());
}

void User::deserialize(const GenericContainer & deserializer)
{
    setId(deserializer.get(User::Fields::id, getId()).asNumber<long>());
    setLogin(deserializer.get(User::Fields::login, getLogin()).asString());
    setActive(deserializer.get(User::Fields::active, isActive()).as<bool>());
    setIsAdmin(deserializer.get(User::Fields::isAdmin, isAdmin()).as<bool>());
    name(deserializer.get(User::Fields::name, name()).asString());
}

bool User::operator==(const User & other) const
{
    return other.getId() == getId()
            && other.isActive() == isActive()
            && other.isAdmin()  == isAdmin()
            && other.getLogin() == getLogin()
            && other.name() == name()
            && other.profileImage() == profileImage();
}

}//namespace

