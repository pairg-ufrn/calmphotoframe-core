#ifndef EXTENDEDEVENT_H
#define EXTENDEDEVENT_H

#include "Event.h"
#include "../user/UserInfo.h"

namespace calmframe {

class User;

namespace Fields {
namespace Event {
    static const std::string user = "user";
}//namespace
}//namespace

class ExtendedEvent : public Event{
public:
    typedef Event super;
public:
    ExtendedEvent(const EventType & type = EventType::None
        , long userId            = InvalidUserId
        , long entityId          = InvalidId
        , const Date & createdAt = Date()
    );

    ExtendedEvent(const User & user
        , const EventType & type = EventType::None
        , long userId            = InvalidUserId
        , long entityId          = InvalidId
        , const Date & createdAt = Date()
    );

    ExtendedEvent(const Event & evt, const User & user);

    bool hasUser() const;

    UserInfo       & getUserInfo();
    const UserInfo & getUserInfo() const;

    void setUserInfo(const UserInfo & value);
    void setUserInfo(const User& user);

    void serialize(Serializer &serializer) const override;
    void deserialize(const GenericContainer &container) override;

    bool operator==(const Event & other) const override;
    bool operator==(const ExtendedEvent & other) const;
private:
    UserInfo userInfo;
};

}//namespace

#endif // EXTENDEDEVENT_H
