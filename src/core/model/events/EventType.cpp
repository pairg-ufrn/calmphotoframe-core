#include "EventType.h"

#include "utils/StringUtils.h"
#include "utils/CommonExceptions.h"

namespace calmframe {

EventType to_EventType(const std::string & eventTypeName){
    for(size_t i=0; i < EventTypeNames.size(); ++i){
        auto name = EventTypeNames[i];
        if(utils::StringUtils::instance().caseInsensitiveEquals(eventTypeName,name)){
            return static_cast<EventType>(i);
        }
    }

    throw CommonExceptions::ParseException(std::string("Could not convert string '" + eventTypeName + "' to EventType"));
}

const std::string &to_string(const EventType & type){
    return EventTypeNames[(int)type];
}

}//namespace

