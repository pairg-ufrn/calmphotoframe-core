#include "Date.h"

#include <memory>

#include "Poco/LocalDateTime.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeParser.h"
#include "Poco/DateTimeFormat.h"

#include "utils/Log.h"

#include <climits>

namespace calmframe {

const int Date::KEEP_CURRENT = INT_MIN;

static int keepOrUpdate(int newValue, int current){
    return newValue != Date::KEEP_CURRENT ? newValue : current;
}

struct Date::DateImpl {
    Poco::LocalDateTime dateTime;
};

Date::Date()
    : impl(new DateImpl())
{}

Date::Date(time_t epochTime) : Date(){
    impl->dateTime = Poco::LocalDateTime(Poco::Timestamp::fromEpochTime(epochTime));
}

Date::Date(const std::string & dateStr)
    : Date()
{
    int timeZoneDifferencial;
    Poco::DateTime dateTime = Poco::DateTimeParser::parse(dateStr, timeZoneDifferencial);
    this->impl->dateTime = Poco::LocalDateTime(timeZoneDifferencial, dateTime, false);
}

Date::Date(int year, int month, int day, int hour, int minute, int second, int zoneOffsetHours)
    : Date()
{
    year    = keepOrUpdate(year, getYear());
    month   = keepOrUpdate(month, getMonth());
    day     = keepOrUpdate(day, getDay());
    hour    = keepOrUpdate(hour, getHour());
    minute  = keepOrUpdate(minute, getMinute());
    second  = keepOrUpdate(second, getSecond());
    zoneOffsetHours = keepOrUpdate(zoneOffsetHours * 3600, getZoneOffsetInSeconds());

    getDateTime().assign(zoneOffsetHours, year, month, day, hour, minute, second, 0, 0);
}

Date::~Date(){}

Date::Date(Date&& other) : impl(std::move(other.impl))
{}

Date& Date::operator=(Date&& other){
    this->impl = std::move(other.impl);
    return *this;
}

Date::Date(const Date& rhs)
: impl(new DateImpl(*rhs.impl))
{}

Date& Date::operator=(const Date& rhs){
  *impl = *rhs.impl;
    return *this;
}

bool Date::operator==(const Date & other) const{
    int diffSecs = (getDateTime() - other.getDateTime()).totalSeconds();
    return diffSecs == 0;
}

bool Date::operator!=(const Date & other) const{
    return !operator==(other);
}

bool Date::operator<(const Date & other) const{
    return getDateTime() < other.getDateTime();
}

int Date::getYear() const{
    return getDateTime().year();
}

int Date::getMonth() const{
    return getDateTime().month();
}

int Date::getDay() const{
    return getDateTime().day();
}

int Date::getHour() const{
    return getDateTime().hour();
}

int Date::getMinute() const{
    return getDateTime().minute();
}

int Date::getSecond() const{
    return getDateTime().second();
}

int Date::getZoneOffset() const{
    //LocalDateTime#tzd is in seconds
    int tzdInHours = getZoneOffsetInSeconds()/ (60*60);
    return tzdInHours;
}

int Date::getZoneOffsetInSeconds() const{
    return getDateTime().tzd();
}

std::string Date::toString() const{
    return Poco::DateTimeFormatter::format(getDateTime(), Poco::DateTimeFormat::ISO8601_FORMAT);
}

const Poco::LocalDateTime &Date::getDateTime() const{
    return impl.get()->dateTime;
}
Poco::LocalDateTime &Date::getDateTime(){
    return impl.get()->dateTime;
}

}//namespace
