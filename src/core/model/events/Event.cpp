#include "Event.h"
#include "../user/User.h"

#include "serialization/Serializer.h"
#include "serialization/Deserializer.h"

namespace calmframe {

const long Event::InvalidId     = -1;
const long Event::InvalidUserId = User::INVALID_ID;

Event::Event(const EventType & type, long userId, long entityId, const Date & createdAt)
    : Event(InvalidId, type, userId, entityId, createdAt)
{}

Event::Event(long id, const EventType & type, long userId, long entityId, const Date & createdAt)
    : id(id)
    , type(type)
    , userId(userId)
    , entityId(entityId)
    , creation(createdAt)
{}

long Event::getId() const{
    return id;
}

void Event::setId(long value){
    id = value;
}

EventType Event::getType() const{
    return type;
}

void Event::setType(const EventType & value){
    type = value;
}

long Event::getUserId() const{
    return userId;
}

void Event::setUserId(long value){
    userId = value;
}

long Event::getEntityId() const{
    return entityId;
}

void Event::setEntityId(long value){
    entityId = value;
}

Date Event::getCreation() const{
    return creation;
}

void Event::setCreation(const Date & value){
    creation = value;
}

void Event::serialize(Serializer & serializer) const{
    using namespace Fields::Event;

    serializer.put(Fields::Event::id, this->getId());
    serializer.put(Fields::Event::type, to_string(getType()));
    serializer.put(Fields::Event::createdAt, getCreation().toString());

    if(getUserId() != InvalidUserId){
        serializer.put(Fields::Event::userId, getUserId());
    }
    if(getEntityId() != InvalidId){
        serializer.put(Fields::Event::entityId, getEntityId());
    }

}

void Event::deserialize(const GenericContainer & container){
    long id = container.get(Fields::Event::id, this->getId()).asNumber<long>();

    this->setId       (id);
    setEntityId (container.get(Fields::Event::entityId   , getEntityId()).asNumber<long>());
    setUserId   (container.get(Fields::Event::userId     , getUserId()).asNumber<long>());

    if(container.contains(Fields::Event::type)){
        setType(to_EventType(container.get(Fields::Event::type).asString()));
    }

    if(container.contains(Fields::Event::createdAt)){
        setCreation (Date(container.get(Fields::Event::createdAt).asString()));
    }
}

bool Event::operator==(const Event & other) const{
    return getId() == other.getId()
            && getUserId() == other.getUserId()
            && getType() == other.getType()
            && getCreation() == other.getCreation();
}

bool Event::operator<(const Event & other) const{
    //FIXME: ordenação não exata (ver parenteses)
    return getCreation() < other.getCreation()
            || (getCreation() == other.getCreation() && getId()     < other.getId())
            || (getId()       == other.getId()       && getType()   < other.getType())
            || (getType()     == other.getType()     && getUserId() < other.getUserId());
}

bool Event::operator>(const Event & other) const{
    return other < *this;
}


}//namespace

