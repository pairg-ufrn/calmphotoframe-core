#ifndef EVENT_H
#define EVENT_H

#include "Date.h"
#include "EventType.h"
#include "serialization/Deserializable.hpp"
#include "serialization/Serializable.hpp"

namespace calmframe {

namespace Fields {
namespace Event {
    static const std::string id         = "id";
    static const std::string type       = "type";
    static const std::string userId     = "userId";
    static const std::string entityId   = "entityId";
    static const std::string createdAt  = "createdAt";
}//namespace
}//namespace

class Event : public Deserializable, public Serializable{
public:
    static const long InvalidId;
    static const long InvalidUserId;

public:
    Event(const EventType & type
        , long userId            = InvalidUserId
        , long entityId          = InvalidId
        , const Date & createdAt = Date()
    );

    Event(long id                = InvalidId
        , const EventType & type = EventType::None
        , long userId            = InvalidUserId
        , long entityId          = InvalidId
        , const Date & createdAt = Date());

    long getId() const;
    void setId(long value);

    EventType getType() const;
    void setType(const EventType & value);

    long getUserId() const;
    void setUserId(long value);

    long getEntityId() const;
    void setEntityId(long value);

    Date getCreation() const;
    void setCreation(const Date & value);

    void serialize(Serializer & serializer) const override;
    void deserialize(const GenericContainer & deserializer) override;
public:
    virtual bool operator==(const Event & other) const;
    bool operator<(const Event & other) const;
    bool operator>(const Event & other) const;

private:
    long id;
    EventType type;
    long userId;
    long entityId;
    Date creation;
};

}//namespace

#endif // EVENT_H
