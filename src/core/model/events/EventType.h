#ifndef EVENTTYPE_H
#define EVENTTYPE_H

#include <string>
#include <vector>

#define TO_STR(value) #value

namespace calmframe {

enum class EventType {
    None = 0

    /* Photo events*/
    , PhotoCreation
    , PhotoUpdate
    , PhotoRemove
};

static const std::vector<std::string> EventTypeNames =
    {TO_STR(None), TO_STR(PhotoCreation), TO_STR(PhotoUpdate), TO_STR(PhotoRemove)};

//FIXME: avoid free functions
const std::string & to_string(const EventType & type);
EventType to_EventType(const std::string & eventTypeName);

}//namespace

#endif // EVENTTYPE_H

