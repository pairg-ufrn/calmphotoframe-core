#ifndef DATE_H
#define DATE_H

#include <memory>
#include <ctime>

namespace Poco {
    class LocalDateTime;
}//namespace

namespace calmframe {

class Date{
public:
    static const int KEEP_CURRENT;

public:
    Date();
    Date(std::time_t epochTime);
    Date(const std::string & dateStr);
    Date(int year, int month, int day
        , int hour=KEEP_CURRENT, int minute=KEEP_CURRENT, int second=KEEP_CURRENT
        , int zoneOffsetHours=KEEP_CURRENT);

    ~Date();

    //Move operators
    Date(Date&& rhs);
    Date& operator=(Date&& rhs);

    //Copy operators
    Date(const Date& rhs);
    Date& operator=(const Date& rhs);

    //Comparison
    bool operator==(const Date & other) const;
    bool operator!=(const Date & other) const;
    bool operator<(const Date & other) const;
public:
    int getYear() const;
    int getMonth() const;
    int getDay() const;
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    /** Return the zone offset in hours for the given date (can vary due to saving daylight time)*/
    int getZoneOffset() const;
    int getZoneOffsetInSeconds() const;

    std::string toString() const;
protected:
    //Pointer to Implementation Idiom (allow put fields on source file)
    class DateImpl;
    std::unique_ptr<DateImpl> impl;

    Poco::LocalDateTime & getDateTime();
    const Poco::LocalDateTime & getDateTime() const;
};

}//namespace

#endif // DATE_H
