#include "ExtendedEvent.h"

#include "../user/User.h"
#include "serialization/Serializer.h"

namespace calmframe{

ExtendedEvent::ExtendedEvent(const EventType & type, long entityId, long userId, const Date & createdAt)
    : ExtendedEvent({}, type, userId, entityId, createdAt)
{}

ExtendedEvent::ExtendedEvent(const User & user, const EventType & type, long userId, long entityId, const Date & createdAt)
    : super(type, userId, entityId, createdAt)
{
    setUserInfo(user);
}

ExtendedEvent::ExtendedEvent(const Event & evt, const User & user)
    : super(evt)
{
    setUserInfo(user);
}

bool ExtendedEvent::hasUser() const{
    return getUserInfo().getUserId() != User::INVALID_ID;
}

UserInfo &ExtendedEvent::getUserInfo(){
    return userInfo;
}

const UserInfo & ExtendedEvent::getUserInfo() const{
    return userInfo;
}

void ExtendedEvent::setUserInfo(const UserInfo & value){
    userInfo = value;
    setUserId(value.getUserId());
}

void ExtendedEvent::setUserInfo(const User& user){
    setUserInfo(UserInfo{user});
}

void ExtendedEvent::serialize(Serializer & serializer) const{
    super::serialize(serializer);
    if(hasUser()){
        serializer.putChild(Fields::Event::user, getUserInfo());
    }
}

void ExtendedEvent::deserialize(const GenericContainer & container){
    super::deserialize(container);
    if(container.contains(Fields::Event::user)){
        getUserInfo().deserialize(container.get(Fields::Event::user, GenericContainer()).as<GenericContainer>());
    }
}

bool ExtendedEvent::operator==(const Event & other) const{
    const ExtendedEvent * otherExtended = dynamic_cast<const ExtendedEvent *>(&other);
    if(otherExtended != nullptr){
        return operator==(*otherExtended);
    }
    return super::operator ==(other);
}

bool ExtendedEvent::operator==(const ExtendedEvent & other) const{
    return super::operator==(other) && getUserInfo() == other.getUserInfo();
}

}//namespace
