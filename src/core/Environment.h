#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <string>
#include <array>

namespace calmframe {

class Configurations;

namespace Defaults {
    static const char * dataFileName = "calmphotoframe.db";
}

using VersionNumbers = std::array<int, 3>;
    
class Environment{
public:
    static Environment & instance();
private:
    std::string basePath;
    std::string imageDir;
public:
    Environment();
    ~Environment();

    std::string getBasePath() const;
    std::string getPathFromBase(const std::string & path) const;

    std::string getLogFile(const std::string & filename="") const;
    std::string getLogDir() const;

    std::string getImageDir() const;
    std::string getImageDir(const Configurations &configurations) const;
    std::string getImagePath(const std::string & imageName) const;
    std::string getImagePath(const Configurations &configurations, const std::string & imageName) const;

    std::string getDataDir() const;
    std::string getDataFile(const std::string & dataDir="", const std::string & fileName="") const;
    std::string getDataFilenameDefault() const;

    VersionNumbers getAppVersion() const;

    void setBasePath(const std::string & path);
    void setImageDir(const std::string &imageDir);
protected:
    std::string getDefaultLogFile() const;
};

}//namespace

#endif // ENVIRONMENT_H
