/*
 * RequestPart.h
 *
 *  Created on: 19/09/2014
 *      Author: noface
 */

#ifndef REQUESTPART_H_
#define REQUESTPART_H_

#include "Request.h"
#include "ContentType.h"

#include <string>
#include <map>
#include <memory>

namespace calmframe {
namespace network {

class BufferedStreamReader;
class DelimitedStreamReader;
class InputFilter;

class RequestPart : public Request{
public:
    std::shared_ptr<BufferedStreamReader> reader;
    std::shared_ptr<DelimitedStreamReader> delimitedReader;
    
	std::map<std::string, std::string> headers;
	std::string boundary;
	ContentType contentType;
	
	std::stringstream content;
public:
	RequestPart();

	bool isPart(){return true;}
	const ContentType & getContentType() const {return contentType;}
	const std::map<std::string, std::string> & getHeaders() const { return headers;}

    std::istream & getContent() override;
    const std::istream & getContent() const override;

	//@Override
	bool containsHeader(const std::string & headerName) const;
	//@Override
    std::string getHeaderValue(
            const std::string & headerName, 
            const std::string & defaultValue=std::string()) const;

    virtual bool hasContent() override;
    virtual bool hasData() override;
    virtual int ignoreData(int maxBytes = -1) override;
    virtual int readData(std::string * output, int maxBytes = -1) override;
    virtual int readData(std::ostream * output, int maxBytes = -1) override;
    
	bool readUntilFind(const std::string & delimiter, std::string * output=NULL, int maxBytes=-1) override;
public:    
	RequestPart & start(Request & request);
    bool readHeaders();
    bool readBody();
    
    
    int readData(int maxBytes, InputFilter * stringOut);
private:
	
    std::string extractDelimiter(const Request& request);
	bool foundDelimiter() const;
	void extractHeaders(const std::string & partHeadersString);
	void setHeadersFromString(const std::string& partHeadersString);
	void setContentTypeFromHeaders();
};

} /* namespace network */
} /* namespace calmframe */

#endif /* REQUESTPART_H_ */
