#ifndef PARAMETERIZEDHEADER_H
#define PARAMETERIZEDHEADER_H

#include "Header.h"
#include <string>
#include <map>

namespace calmframe {
namespace network{

class ParameterizedHeader : public Header{
public:
    typedef Header super;
    typedef std::map<std::string, std::string> ParametersType;
public:
    static const char DEFAULT_PARAM_SEPARATOR;
    static const char DEFAULT_VALUE_SEPARATOR;
private:
    ParametersType parameters;
    char parameterSeparator, valueSeparator;
    bool omitEmptyValues;
public:
    ParameterizedHeader()
        : parameterSeparator(DEFAULT_PARAM_SEPARATOR)
        , valueSeparator(DEFAULT_VALUE_SEPARATOR)
        , omitEmptyValues(false)
    {}
    ParameterizedHeader(const std::string & name, const std::string & value
                        , const char & paramSeparator=DEFAULT_PARAM_SEPARATOR
                        , const char & valueSeparator=DEFAULT_VALUE_SEPARATOR)
        : super(name, value)
        , parameterSeparator(paramSeparator)
        , valueSeparator(valueSeparator)
    {
        decodeHeaderValue(value);
    }
public:
    virtual void clearParameters();
    /** Returns the value of parameter paramName if found. Otherwise returns defaultValue.*/
    virtual std::string getParameter(const std::string & paramName, const std::string & defaultValue=std::string()) const;
    virtual void putParameter(const std::string & paramName, const std::string & paramValue, const bool & updateValue=true);
    virtual void removeParameter(const std::string & paramName);
    virtual bool containsParameter(const std::string & paramName) const;
public:
    virtual char getParameterSeparator() const;
    virtual char getValueSeparator() const;

    virtual void setParameterSeparator(char value);
    virtual void setValueSeparator(char value);

protected:
    bool getOmitEmptyValuesOnToString() const;
    void setOmitEmptyValuesOnToString(bool value);

    virtual void updateValue();
    virtual void decodeHeaderValue(const std::string & value);
};

}//namespace
}//namespace

#endif // PARAMETERIZEDHEADER_H
