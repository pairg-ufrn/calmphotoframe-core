#ifndef FILTERINPUTSTREAMBUFFER_H
#define FILTERINPUTSTREAMBUFFER_H

#include "InputFilter.hpp"
#include "DelegatorStreamBuffer.h"

namespace calmframe {
namespace network{

class FilterInputStreamBuffer : public DelegatorStreamBuffer
{
    typedef DelegatorStreamBuffer super;
    InputFilter * inputFilter;
public:
    FilterInputStreamBuffer(std::streambuf * delegate=NULL, InputFilter * filter = NULL)
        : super(delegate)
        , inputFilter(filter)
    {}

    virtual int_type underflow();
    InputFilter *getInputFilter() const;
    void setInputFilter(InputFilter *value);
};

}
}


#endif // FILTERINPUTSTREAMBUFFER_H
