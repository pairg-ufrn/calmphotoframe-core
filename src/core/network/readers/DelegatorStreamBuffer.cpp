#include "DelegatorStreamBuffer.h"

#include "utils/NullPointerException.hpp"

using namespace std;
typedef std::basic_streambuf<char>::int_type int_type;

int_type DelegatorStreamBuffer::underflow(){
    assertDelegateNotNull();
    int_type readedChar = this->delegate->sgetc();

    return readedChar;
}

int_type DelegatorStreamBuffer::pbackfail(int_type ch){
    assertDelegateNotNull();
    return this->delegate->sputbackc(ch);
}

streamsize DelegatorStreamBuffer::showmanyc(){
    assertDelegateNotNull();
    return this->delegate->in_avail();
}

int DelegatorStreamBuffer::sync(){
    assertDelegateNotNull();
    return this->delegate->pubsync();
}

int DelegatorStreamBuffer::overflow(int c){
    return this->delegate->sputc(c);
}

void DelegatorStreamBuffer::assertDelegateNotNull(){
    CommonExceptions::assertNotNull(delegate, "streambuf delegate should not be null.");
}
