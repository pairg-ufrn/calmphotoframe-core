#ifndef CHARBUFFEROUTPUT_H
#define CHARBUFFEROUTPUT_H

#include "network/readers/InputFilter.hpp"

namespace calmframe {
namespace network {

class CharBufferOutput : public InputFilter{
private:
    char * buffer;
    int _count;
    int maxLimit;
public:
    CharBufferOutput()
        : buffer(nullptr)
        , _count(0)
        , maxLimit(0)
    {}
    
    CharBufferOutput(char * reader, int maxLimit)
    {
        reset(reader, maxLimit);
    }
    
    void reset(char * buffer, int limit){
        this->buffer = buffer;
        this->maxLimit = limit;
        this->_count = 0;
    }

    bool onRead(int byte){
        if(!buffer || !hasSpace()){
            return false;
        }
        
        buffer[_count] = byte;
        ++_count;
        return true;
    }
    
    int count() const{
        return this->_count;
    }
    
protected:
    /** Return true if has space on buffer*/
    bool hasSpace(){
        return maxLimit > _count;
    }
};

}//namespace
}//namespace

#endif // CHARBUFFEROUTPUT_H
