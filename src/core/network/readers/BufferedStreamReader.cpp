#include "BufferedStreamReader.h"

#include <sstream>
#include "utils/Log.h"
#include <assert.h>
#include "network/readers/DelimitedBufferReader.h"

#include "CharBufferOutput.h"
#include "StringBufferOutput.h"
#include "StreamOutput.h"

using namespace std;

namespace calmframe {
namespace network {

const unsigned BufferedStreamReader::DEFAULT_SIZE_BUFFER = 4096;


/* **************************** public read methods ********************************/

BufferedStreamReader::BufferedStreamReader()
    : BufferedStreamReader({}, BufferedStreamReader::DEFAULT_SIZE_BUFFER)
{}

BufferedStreamReader::BufferedStreamReader(std::shared_ptr<StreamReader> reader, unsigned bufferSize)
    : reader(reader)
    , maxSizeBuffer(bufferSize)
    , readedCount(0)
    , buffer(new std::stringbuf())
    , matchCount(0)
    , foundDelimiter(false)
{}
BufferedStreamReader::BufferedStreamReader(const BufferedStreamReader &other)
    : reader(other.reader)
    , maxSizeBuffer(other.maxSizeBuffer)
//    , readedCount(other.getReadedCount())
    , readedCount(0)
    , buffer(other.buffer)
    , externalBuffer()
    , matchCount(other.matchCount)
    , foundDelimiter(other.foundDelimiter)
{
    //Do not copy data on other.externalBuffer because could not copy input and output
    //pointers location (because BufferedStreamReader is const)
    //this->externalBuffer.str(other.externalBuffer.str());
}

BufferedStreamReader::~BufferedStreamReader()
{}

/* **************************** public read methods ********************************/
bool BufferedStreamReader::hasDataAvailable() {
    return externalBuffer.in_avail() > 0 || canReadData();
}
streamsize BufferedStreamReader::read(){
    return readByteImpl(true);
}
int BufferedStreamReader::read(char * buffer, int numBytes){
//    CharBufferReader reader(buffer);
//	return read(reader, numBytes);
    streamsize readed = this->readManyBytes(buffer, numBytes);
    return readed == EOF ? 0 : readed;
}
int BufferedStreamReader::read(std::string * buffer, int numBytes){
	StringBufferOutput reader(buffer);
	return read(reader, numBytes);
}
int BufferedStreamReader::read(std::ostream * output, int numBytes){
	StreamOutput reader(output);
    return read(reader, numBytes);
}

bool BufferedStreamReader::readUntilFind(const std::string& terminator, std::string* output, int maxBytes) {  
    StringBufferOutput reader(output);
	bool found;
	DelimitedBufferReader delimitedReader(reader, terminator, NULL, &found);
    read(delimitedReader, maxBytes);

	return found;
}

/**
 * @brief Read until numBytes using some @class{InputFilter}
 *
 * @param reader: an object that receives each caracter readed and can redirect it to some output
 * and indicate if the reading is over.
 * @param numBytes: the max number of bytes to read.
 * @return the number of bytes readed. Return 0 if there is no more bytes to read.
 */
int BufferedStreamReader::read(InputFilter & reader, int numBytes){
    if(hasFoundDelimiter() || numBytes == 0){
        return 0;
    }

    bool unlimited = (numBytes < 0);
    int totalReaded = 0;


    bool hasData = false;

    streamsize byte;
    do{
        byte = this->readByteImpl(true);
        hasData = byte != EOF;
        if(hasData){
            ++totalReaded;
            if(reader.onRead(byte) == false){
                break;
            }
        }
    }
    while(hasData && (unlimited || totalReaded < numBytes));
    reader.onStop();

    return totalReaded;
}


/* **************************** std::streambuf methods ********************************/
streambuf::int_type BufferedStreamReader::underflow(){
    return readByteImpl(false);
}
streambuf::int_type BufferedStreamReader::uflow(){
    return read();
}
streamsize BufferedStreamReader::xsgetn(char *bufferOut, streamsize n){
    return this->readManyBytes(bufferOut, n);
}
streamsize BufferedStreamReader::showmanyc(){
    //TODO: rever esta implementação (talvez buffer->in_avail em de externalBuffer)
    return this->externalBuffer.in_avail();
}
int BufferedStreamReader::pbackfail(streambuf::int_type ){
    return this->externalBuffer.sungetc();
}
streampos BufferedStreamReader::seekoff(streamoff off, ios_base::seekdir way, ios_base::openmode which){
    return this->externalBuffer.pubseekoff(off, way, which);
}

/* **************************** Internal read methods ********************************/
/** When advance is true, the method should read a byte and advance the buffer to the
    next position. Otherwise it should return the current character.
    This method also fill the external buffer if needed.
*/
streamsize BufferedStreamReader::readByteImpl(bool advance){
    if(hasFoundDelimiter()){
        return EOF;
    }

    streamsize byte = EOF;
    if(externalBuffer.in_avail() <= 0){
        fillExternalBuffer();
    }
    if(externalBuffer.in_avail() > 0){
        if(advance){
            byte = externalBuffer.sbumpc();
        }
        else{
            byte = externalBuffer.sgetc();
        }
    }

    return byte;
}
streamsize BufferedStreamReader::readManyBytes(char *bufferOut, streamsize n){
    int readCount = 0;
    while(!hasFoundDelimiter() && readCount < n){
        streamsize byte = this->readByteImpl(true);
        if(byte == EOF){
            break;
        }
    
        bufferOut[readCount] = byte;
        ++readCount;
    }

    return readCount > 0 ? readCount : EOF;
}

void BufferedStreamReader::fillExternalBuffer(){
    if(hasDataAvailable()){
        if(delimiter.empty()){
            externalBuffer.sputc(this->buffer->sbumpc());
        }
        else{
            fillBufferUntilFindDelimiter();
        }
    }
}

void BufferedStreamReader::fillBufferUntilFindDelimiter()
{
    string tmpBuffer;
    do{
        if(buffer->in_avail() == 0){ //Não há mais bytes no buffer
            //Tenta ler mais bytes
            this->readToBuffer();
            if(buffer->in_avail() == 0){ //Não há mais o que ler
                matchCount = 0;
                //Encerra leitura
                break;
            }
        }

        streamsize byte = this->buffer->sbumpc();
        tmpBuffer.push_back((char)byte);
        if(delimiter[matchCount] != byte){
            matchCount = 0;
        }
        else{
            ++matchCount;

            if(matchCount == (int)delimiter.length()){
                this->foundDelimiter = true;
                //Descarta dados do delimitador
                tmpBuffer.clear();
            }
        }
    }while(matchCount > 0 && !hasFoundDelimiter());

    if(tmpBuffer.size() > 0){
        externalBuffer.sputn(tmpBuffer.data(), tmpBuffer.size());
    }
}

bool BufferedStreamReader::canReadData(){
    if(hasFoundDelimiter()){
        return false;
    }

    if(buffer.get()->in_avail() > 0){
        return true;
    }

    return readToBuffer() > 0;
}


int BufferedStreamReader::getBytesToRead(int n) {
    int bytesToRead = (n > 0 ? n : maxSizeBuffer);
    int spaceInBuffer = (maxSizeBuffer - buffer->in_avail());
    if(bytesToRead >  spaceInBuffer){
        bytesToRead = maxSizeBuffer - buffer->in_avail();
    }
    return bytesToRead;
}

int BufferedStreamReader::readToBuffer(int n){
    int bytesToRead = getBytesToRead(n);

    if(bytesToRead > 0 && reader){
        char tmpBuffer[bytesToRead];
        int readed = reader->read(tmpBuffer, bytesToRead);
        if(readed > 0){
            //Copia bytes do tmpBuffer para buffer
            buffer->sputn(tmpBuffer,readed);
        }
        //TODO: registrar caso erro tenha ocorrido (p/ não realizar mais leituras)

        return readed;
    }
    return 0;
}

} /* namespace network */
} /* namespace calmframe */

