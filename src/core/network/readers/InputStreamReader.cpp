#include "InputStreamReader.h"

#include "network/readers/InputFilter.hpp"

namespace calmframe {
namespace network {


InputStreamReader::InputStreamReader(std::istream * input) : stream(input)
{}

InputStreamReader &InputStreamReader::filter(InputFilter * filter){
    this->_filter = filter;
    return *this;
}

std::streamsize InputStreamReader::read(const std::streamsize & readLimit){
    if(!stream->good() || readLimit == 0){
        return 0;
    }

    std::streamsize count = 0;    
    while(stream->good() && count < readLimit){
        int readedByte = stream->get();
        if(readedByte == std::char_traits<char>::eof()){
            break;
        }
        
        if(_filter && !_filter->onRead(readedByte)){
            break;
        }
        
        ++count;
    }
    
    return count;
}

}//namespace
}//namespace

