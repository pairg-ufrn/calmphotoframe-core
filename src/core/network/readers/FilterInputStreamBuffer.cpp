#include "FilterInputStreamBuffer.h"

#include <streambuf>

using namespace calmframe::network;

InputFilter *FilterInputStreamBuffer::getInputFilter() const
{
    return inputFilter;
}

void FilterInputStreamBuffer::setInputFilter(InputFilter *value)
{
    inputFilter = value;
}
std::streambuf::int_type calmframe::network::FilterInputStreamBuffer::underflow(){
    int_type value = super::underflow();
    if(this->inputFilter != NULL){
        if(value != EOF){
            if(!this->inputFilter->onRead(value)){
                return EOF;
            }
        }
        else{
            this->inputFilter->onStop();
        }
    }

    return value;
}
