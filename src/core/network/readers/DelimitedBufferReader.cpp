#include "DelimitedBufferReader.h"
#include <cassert>
#include "utils/Log.h"


#include <streambuf>
#include <cstdlib>

using namespace std;

namespace calmframe{
namespace network {


DelimitedBufferReader::DelimitedBufferReader(InputFilter &reader, string delimiter, streambuf *externalBuffer, bool *found)
    : delegateReader(reader)
    , delimiter(delimiter)
    , matchCount(0)
    , outBuffer(externalBuffer)
    , found(found)
{
    if(found != NULL){
        *found = false;
    }
}

bool DelimitedBufferReader::onRead(int byte){
    localBuffer.sputc(byte);

    if(delimiter.empty() || delimiter[matchCount] != byte){
        matchCount = 0;
        return copyToReader();
    }
    else{
        ++matchCount;
        if(matchCount == (int)delimiter.length()){
            //Encontrou o delimitador
            //Encerra leitura
            discardBytesFromBuffer(matchCount);

            if(found!=NULL){
                *found = true;
            }
            return false;
        }
    }
    return true;
}

void DelimitedBufferReader::onStop(){
    if(localBuffer.in_avail() > 0){
        copyToReader();
    }
}

bool DelimitedBufferReader::copyToReader(){
    bool delegateStopped = false;
    //Tenta copiar todos os bytes do buffer local para o delegate
    while(localBuffer.in_avail() > 0 && !delegateStopped)
    {
        delegateStopped = !delegateReader.onRead(localBuffer.sbumpc());
    }

    if(delegateStopped && outBuffer != NULL){
        //Tenta devolver bytes restantes para outBuffer
        // "sungetc" faz com que ponteiro "get" do buffer retorne uma posição
        while(localBuffer.snextc() != EOF &&
              outBuffer->sungetc() != EOF)
        {
          //Do nothing
        }
    }

    return !delegateStopped;
}

void DelimitedBufferReader::clearLocalBuffer(){
    //Move buffer para posição final
    localBuffer.pubseekoff(0,ios::end);
    assert(localBuffer.in_avail() <= 0);
}

void DelimitedBufferReader::discardBytesFromBuffer(int numBytes){
    for(int i=0; i < numBytes && localBuffer.in_avail(); ++i){
        localBuffer.snextc();
    }
}


}
}


