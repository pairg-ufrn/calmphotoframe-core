#ifndef INPUTSTREAMREADER_H_
#define INPUTSTREAMREADER_H_

#include <istream>

namespace calmframe{
namespace network{

class InputFilter;

class InputStreamReader{
public:
    InputFilter * _filter = nullptr;
    std::istream * stream;

    InputStreamReader(std::istream * input);
    
    InputStreamReader & filter(InputFilter * filter);

    std::streamsize read(const std::streamsize & readLimit) ;
};

}
}
#endif 
