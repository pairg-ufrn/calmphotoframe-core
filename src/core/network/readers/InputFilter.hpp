/*
 * ConnectionReader.h
 *
 *  Created on: 25/09/2014
 *      Author: noface
 */

#ifndef INPUTFILTER_H_
#define INPUTFILTER_H_

namespace calmframe {
namespace network {

class InputFilter {
public:
    virtual ~InputFilter(){}
	/** @return true to continue reading or false to interrupt.*/
	virtual bool onRead(int byte) =0;
    virtual void onStop(){}
};

} /* namespace network */
} /* namespace calmframe */

#endif /* INPUTFILTER_H_ */
