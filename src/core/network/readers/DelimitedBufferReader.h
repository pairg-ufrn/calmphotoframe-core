#ifndef DELIMITEDBUFFERREADER_H
#define DELIMITEDBUFFERREADER_H

#include "InputFilter.hpp"
#include <sstream>
#include <string>

namespace calmframe{
namespace network {

class DelimitedBufferReader : public InputFilter{
private:
    InputFilter & delegateReader;
    std::string delimiter;
    int matchCount;
    std::stringbuf localBuffer;
    std::streambuf * outBuffer;
    bool * found;
public:
    DelimitedBufferReader(InputFilter & reader, std::string delimiter, std::streambuf * externalBuffer=NULL, bool * found = NULL);

    bool onRead(int byte);
    void onStop();

    const std::string& getDelimiter() const { return delimiter;}
    void setDelimiter(const std::string& delimiter) {
        this->delimiter = delimiter;
    }

    int getMatchCount() const {return this->matchCount;}

    bool copyToReader();
    void clearLocalBuffer();
private:
    void discardBytesFromBuffer(int numBytes);
};

}
}
#endif // DELIMITEDBUFFERREADER_H
