#ifndef STREAMOUTPUT_H
#define STREAMOUTPUT_H

#include <ostream>
#include "network/readers/InputFilter.hpp"

namespace calmframe {
namespace network {

class StreamOutput : public InputFilter{
private:
    std::ostream * stream;
public:
    StreamOutput(std::ostream * stream)
        : stream(stream)
    {}

    bool onRead(int byte){
        if(stream != NULL){
            stream->put(byte);
            //Continua lendo se stream não contiver erros
            return stream->good();
        }
        else{//Se stream é nula, lê até o fim dos dados (ignora eles)
            return true;
        }
    }
};

}//namespace
}//namespace

#endif // STREAMOUTPUT_H
