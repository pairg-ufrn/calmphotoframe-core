#ifndef DELEGATORSTREAMBUFFER_H
#define DELEGATORSTREAMBUFFER_H

#include <streambuf>
#include <cstdio> //EOF
class DelegatorStreamBuffer : public std::streambuf
{
private:
    std::streambuf * delegate;
public:
    DelegatorStreamBuffer(std::streambuf * delegate=NULL)
        : delegate(delegate)
    {}
    virtual ~DelegatorStreamBuffer(){}
public: //Override methods from streambuf
    virtual int_type underflow();
    virtual int_type pbackfail(int_type ch);
    virtual std::streamsize showmanyc();
//    virtual std::streamsize xsgetn (char* bufferOut, streamsize n);
    virtual int sync();
    virtual int overflow (int c = EOF);
public: //Getters & Setters
    virtual std::streambuf *getDelegate() const{ return delegate; }
    virtual void setDelegate(std::streambuf *value){delegate = value;}
private:
    void assertDelegateNotNull();
};

#endif // DELEGATORSTREAMBUFFER_H
