#include "StreamReader.hpp"

#include <iostream>
#include <string>
#include <sstream>

namespace calmframe{
namespace network{

std::ostream &operator<<(std::ostream &out, calmframe::network::StreamReader &reader)
{
    int bufferSize = reader.getTmpBufferSize();
    char buffer[bufferSize];
    streamsize readed = reader.read((char *)buffer, bufferSize);
    while(readed != std::char_traits<char>::eof() && readed > 0){
        out.write(buffer, readed);
        readed = reader.read((char *)buffer, bufferSize);
    }
    return out;
}


streamsize StreamReader::read(string &strBuffer){
    stringstream tmpStream;
    tmpStream << *this;
    strBuffer.assign(tmpStream.str());

    return tmpStream.tellp();
}

int StreamReader::getTmpBufferSize()
{
    return 100;
}

} //namespace network
} //namespace calmframe
