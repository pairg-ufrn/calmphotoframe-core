#ifndef STRINGBUFFEROUTPUT_H
#define STRINGBUFFEROUTPUT_H

#include <string>
#include "network/readers/InputFilter.hpp"

namespace calmframe {
namespace network {

class StringBufferOutput : public InputFilter{
private:
    std::string * buffer;
public:
    StringBufferOutput(std::string * buff)
        : buffer(buff)
    {}

    bool onRead(int byte){
        if(buffer != NULL){
            buffer->push_back(byte);
        }
        return true;
    }
};

}//namespace
}//namespace


#endif // STRINGBUFFEROUTPUT_H
