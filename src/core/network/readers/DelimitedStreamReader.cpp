#include "DelimitedStreamReader.h"

#include <cstdio>

namespace calmframe {
namespace network {

DelimitedStreamReader::DelimitedStreamReader(std::istream * input, const std::string & delimiter) 
    : DelimitedStreamReader(input->rdbuf(), delimiter)
{}

streamsize DelimitedStreamReader::read(string & strBuffer){
    return StreamReader::read(strBuffer);
}

DelimitedStreamReader::DelimitedStreamReader(std::streambuf * input, const string & delimiter)
    : streambuf(input)
    , bufferOutput()
    , delimiterReader(bufferOutput, delimiter , NULL, &_found)
{
}

streamsize DelimitedStreamReader::read(char * buffer, const streamsize & readLimit){
    if(_found){
        return 0;
    }
    
    bufferOutput.reset(buffer, readLimit);
    
    while(streambuf->sgetc() != EOF){
//        int readedByte = stream->get();
        int readedByte = streambuf->sbumpc();
        if(readedByte == EOF){
            break;
        }
        
        char c = (char)readedByte;
        
        if(!delimiterReader.onRead(readedByte)){
            break;
        }
    }
    
    return bufferOutput.count() > 0 ? bufferOutput.count() : EOF;
}

void DelimitedStreamReader::setDelimiter(const string & delimiter){
    this->delimiterReader.setDelimiter(delimiter);
}

bool DelimitedStreamReader::found() const{
    return _found;
}

}//namespace
}//namespace

