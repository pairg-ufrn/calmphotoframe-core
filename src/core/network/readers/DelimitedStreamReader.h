#ifndef DELIMITEDSTREAMREADER_H
#define DELIMITEDSTREAMREADER_H

#include <istream>

#include "network/readers/StreamReader.hpp"
#include "network/readers/DelimitedBufferReader.h"
#include "CharBufferOutput.h"

namespace calmframe {
namespace network {

class DelimitedStreamReader : public StreamReader{
public:
    
    DelimitedStreamReader(std::streambuf * input, const std::string & delimiter= std::string());
    DelimitedStreamReader(std::istream * input, const std::string & delimiter= std::string());

    streamsize read(std::string & strBuffer) override;
    streamsize read(char * buffer, const streamsize & readLimit) override;

    void setDelimiter(const std::string & delimiter);

    bool found() const;
private:
    bool _found = false;
//    std::istream * stream;
    std::streambuf * streambuf;
    CharBufferOutput bufferOutput;
    DelimitedBufferReader delimiterReader;
};

}//namespace
}//namespace

#endif // DELIMITEDSTREAMREADER_H
