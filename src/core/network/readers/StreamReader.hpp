#ifndef NETWORK_STREAMREADER_H_
#define NETWORK_STREAMREADER_H_

#include <streambuf>
#include <string>

namespace calmframe{
namespace network{


using namespace std;

class StreamReader{
public:
    virtual ~StreamReader(){}
    virtual streamsize read(std::string & strBuffer);
    virtual streamsize read(char * buffer, const streamsize & readLimit)=0;

    friend std::ostream & operator<<(std::ostream & out, StreamReader & reader);
protected:
    virtual int getTmpBufferSize();
};

}
}
#endif /* NETWORK_STREAMREADER_H_ */
