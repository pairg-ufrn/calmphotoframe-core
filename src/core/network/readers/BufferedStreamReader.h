#ifndef BUFFEREDSTREAMREADER_H
#define BUFFEREDSTREAMREADER_H

#include <streambuf>
#include <sstream>
#include <memory>
#include "network/readers/InputFilter.hpp"
#include "network/readers/StreamReader.hpp"

namespace calmframe {
namespace network {


class BufferedStreamReader : public std::streambuf{
private:
    static const unsigned DEFAULT_SIZE_BUFFER;
protected:
	unsigned maxSizeBuffer;

    int readedCount;

    std::shared_ptr<std::stringbuf> buffer;
    std::shared_ptr<StreamReader> reader;
    
    //Caso haja um delimitador, este buffer será utilizado para armazenar os caracteres
    //que estão prontos para leitura
public:
    std::stringbuf externalBuffer;
protected:
	std::string delimiter;
    int matchCount;

    bool foundDelimiter;
public:

    BufferedStreamReader();
    BufferedStreamReader(std::shared_ptr<StreamReader> reader, unsigned bufferSize = DEFAULT_SIZE_BUFFER);
    //Copy constructor
    BufferedStreamReader(const BufferedStreamReader & other);
    virtual ~BufferedStreamReader();

	virtual bool hasDataAvailable();
//	virtual int getReadedCount() const
//	{ return readedCount;}
//	virtual void clearReadedCount()
//	{ readedCount = 0;}

	void setDelimiter(const std::string & delimiter){
		this->delimiter = delimiter;
	}
	const std::string& getDelimiter() const {
		return delimiter;
	}
	bool hasFoundDelimiter() const{
		return foundDelimiter;
	}

	virtual int read(char * buffer, int numBytes = -1);
    virtual int read(std::string * buffer, int numBytes = -1);
    virtual int read(std::ostream * output, int numBytes = -1);
    virtual streamsize read();

    bool readUntilFind(const std::string& terminator, std::string * output, int maxBytes=-1);
public:
    int read(InputFilter & reader, int numBytes=-1);
protected:
    /** @brief Determines if there is data to be read (in buffer or from network).
        If there is no data on buffer, try to read from network and store readed data on buffer.*/
    virtual bool canReadData();
    virtual streamsize readByteImpl(bool advance = true);
    virtual streamsize readManyBytes(char *bufferOut, streamsize n);
	virtual int readToBuffer(int n=-1);
    int getBytesToRead(int n);

    void fillExternalBuffer();

    void fillBufferUntilFindDelimiter();
protected: //Override methods from streambuf
    virtual int_type underflow();
    virtual int_type uflow();
    virtual std::streamsize xsgetn (char* bufferOut, streamsize n);
    virtual std::streamsize showmanyc();
    virtual int pbackfail(int_type ch);

    virtual streampos seekoff (streamoff off, ios_base::seekdir way,
                       ios_base::openmode which = ios_base::in | ios_base::out);
// Not overrided virtual methods:
//    streambuf* setbuf (char* s, streamsize n);
//    streampos seekpos (streampos sp, ios_base::openmode which = ios_base::in | ios_base::out);
//    virtual int sync();

//    virtual int overflow (int c = EOF);
//    virtual streamsize xsputn (const char* s, streamsize n);
};

} /* namespace network */
} /* namespace calmframe */


#endif // BUFFEREDSTREAMREADER_H
