#ifndef STATUSRESPONSES_H
#define STATUSRESPONSES_H

#include "StatusResponse.h"

namespace calmframe {
namespace network {

namespace StatusResponses{
    const StatusResponse OK                     (200,"OK");
    
    const StatusResponse BAD_REQUEST            (400, "Bad Request");
    const StatusResponse UNAUTHORIZED           (401, "Unauthorized");
    const StatusResponse FORBIDDEN              (403, "Forbidden");
    const StatusResponse NOT_FOUND              (404, "Not Found");
    const StatusResponse UNSUPPORTED_MEDIA_TYPE (415, "Unsupported Media Type");
    
    const StatusResponse SERVER_ERROR    (500,"Internal Server Error");
}

}//namespace
}//namespace

#endif // STATUSRESPONSES_H

