#ifndef URLQUERY_H
#define URLQUERY_H

#include <string>
#include <unordered_map>
#include <map>

namespace calmframe {
namespace network {

class UrlQuery{
public:
    using Key = std::string;
    using Value = std::string;
    using ParamMap = std::map<Key, Value>;
    
public:
    UrlQuery() = default;
    UrlQuery(std::initializer_list<ParamMap::value_type> values);
    UrlQuery(const ParamMap & params);

    bool empty() const;
    
    bool contains(const Key & key) const;
    const Value & get(const Key  & key, const Value & defaultvalue = Value()) const;
    
    UrlQuery & add(const Key & key, const Key & value);
    UrlQuery & put(const Key & key, const Key & value);
    
    UrlQuery & remove(const Key & key);
    UrlQuery & clear();

    UrlQuery & set(const std::string & str);

    const ParamMap & params() const; 
    
    std::string toString() const;   
private:
    ParamMap _params;
};

}//namespace
}//namespace

std::ostream & operator<<(std::ostream & out, const calmframe::network::UrlQuery & query);

#endif // URLQUERY_H
