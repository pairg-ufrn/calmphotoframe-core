/*
 * StatusResponse.h
 *
 *  Created on: 27/11/2014
 *      Author: noface
 */

#ifndef NETWORK_STATUSRESPONSE_H_
#define NETWORK_STATUSRESPONSE_H_

#include <string>

namespace calmframe {
namespace network{

class StatusResponse {
private:
	int statusCode;
	std::string message;
public:
    StatusResponse();
    StatusResponse(int code, const std::string & message);
    virtual ~StatusResponse();

    const std::string& getMessage() const { return message; }
    int getStatusCode()             const { return statusCode; }

    void setMessage   (const std::string& message) { this->message = message; }
    void setStatusCode(int statusCode)             { this->statusCode = statusCode; }
    void set(const StatusResponse & otherStatus);
    void set(int code, const std::string & message);
public:
    bool operator==(const StatusResponse & other) const;
};

}

} /* namespace calmframe */

#endif /* NETWORK_STATUSRESPONSE_H_ */
