/*
 * ContentType.h
 *
 *  Created on: 19/09/2014
 *      Author: noface
 */

#ifndef CONTENTTYPE_H_
#define CONTENTTYPE_H_

#include <string>
#include <map>
#include "Header.h"
#include "Headers.h"

namespace calmframe {
namespace network {

class ContentType : public Header{
    typedef Header super;
private:
	std::string type;
	std::string subtype;
	std::map<std::string, std::string> parameters;
public: //types
    typedef std::map<std::string, std::string> ParametersType;
public: //Constructors e Destructor
    ContentType();
	ContentType(const std::string & contentType);
	ContentType(const std::string & type, const std::string & subtype, const std::string & parametersString="");
	virtual ~ContentType()
	{}

public: //Getters and Setters
	std::string getType   () const { return type;}
	std::string getSubtype() const { return subtype;}
    const std::map<std::string, std::string> & getParameters() const
    {	return parameters;}

    /** @Override*/
    virtual void setName(const std::string & name);
    /** @Override*/
    virtual void setValue(const std::string & value){
        this->setContentType(value);
    }
    void setContentType(const std::string & contentType);

    void setType(std::string type){
        this->type = type;
        updateValue();
    }
    void setSubtype(std::string subtype) {
        this->subtype = subtype;
        updateValue();
    }
	std::string getParameter(const std::string & param) const;
    void putParameter(const std::string & paramName, const std::string & paramValue){
        this->parameters[paramName] = paramValue;
        updateValue();
    }
public:
    bool is(const ContentType & other) const;
    bool is(std::string type, std::string subtype) const;
protected:
    virtual void updateValue();
//private:
public:
	void setParameters(const std::string & stringParameters);
private:
	void extractParameter();

public: //Operators
	operator std::string() const;
    ContentType & operator=(const Header & otherHeader);
};

} /* namespace network */
} /* namespace calmframe */

#endif /* CONTENTTYPE_H_ */
