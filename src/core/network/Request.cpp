/*
 * Request.cpp
 *
 *  Created on: 18/09/2014
 *      Author: noface
 */

#include "network/Request.h"

#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;

namespace calmframe {
namespace network {

//Copy constructor
Request::Request(const Request& request)
    : method(request.method)
    , contentType(request.getContentType())
	, parameters(request.parameters)
    , _url(request._url)
    , urlCache(request.urlCache)
{
}

const Url &Request::url() const{
    return this->_url;
}

void Request::url(const Url & url){
    this->_url = url;
    this->urlCache.clear();
}

const std::string & Request::getUrl() const {
    if(this->urlCache.empty()){
        this->urlCache = this->url().toString();
    }
    return urlCache;
}

void Request::setUrl(const string & url)
{
    this->urlCache = url;
    this->_url.set(url);
}


const calmframe::network::ContentType & Request::getContentType() const{
    return this->contentType;
}

std::string Request::getMethod() const{
    return this->method;
}

bool Request::containsHeader(const std::string& headerName) const {
    string headerValue = this->getHeaderValue(headerName);

    return !headerValue.empty();
}

const istream &Request::getContent() const{
    return (const istream &)((Request *)this)->getContent();
}

bool Request::hasQueryParameter(const string &param) const{
    return url().query().contains(param);
}

string Request::getQueryParameter(const string &paramName, const string &defaultValue) const{
    return url().query().get(paramName, defaultValue);
}

const string &Request::getParameter(const string &paramName, const string &defaultValue) const{
    ConstParamIter iter = parameters.find(paramName);
    if(iter == parameters.end()){
        return defaultValue;
    }
    else{
        return iter->second;
    }
}

string Request::getParameter(const string & paramName) const{
    return getParameter(paramName, string());
}

bool Request::hasParam(const string & paramName) const{
    return parameters.find(paramName) != parameters.end();
}

void Request::putParameter(const string &paramName, const string &value){
    this->parameters[paramName] = value;
}

bool Request::saveData(const std::string& filename) {
	ofstream file (filename.c_str(), ios::out | ios::binary);
	bool saved = this->saveData(&file);
	file.close();
	return saved;
}

bool Request::saveData(std::ostream * output){
	return this->readData(output) > 0;
}

} /* namespace network */
} /* namespace calmframe */
