#ifndef MULTIPARTREQUESTREADER_H
#define MULTIPARTREQUESTREADER_H

#include <string>
#include <memory>

#include "Request.h"

namespace calmframe {
namespace network {

class MultipartRequestReader{
public:
    using PartPtr = std::shared_ptr<Request>;

    virtual ~MultipartRequestReader() = default;

    virtual bool start(Request * request);
    virtual PartPtr next();
    
    virtual void clear();
    
protected:    
    bool skipToStartData();
    bool skipToNextPart();
    
    bool readUntilFind(const std::string & str, std::string * out=NULL, int maxBytes=-1);
private:
    Request * request = nullptr;
    std::string boundary;    
};

}//namespace
}//namespace

#endif // MULTIPARTREQUESTREADER_H
