#ifndef URLPATH_H
#define URLPATH_H

#include <vector>
#include <string>
#include <sstream>

class UrlPath{
public:
    using Segment = std::string;
    using PathList = std::vector<Segment>;

public:
    UrlPath() = default;
    UrlPath(const std::string & pathStr);
    UrlPath(const PathList & pathList);

    UrlPath & add(const Segment & pathSegment);
    UrlPath & add(const char * pathSegment);

    template<typename T>
    UrlPath & add(const T & segment){
        std::stringstream out;
        out << segment;
        
        return add(out.str());
    }

    template<typename T, typename ...Segments>
    UrlPath & add(const T & firstSegment, const Segments& ... segments){
        add(firstSegment);
        return add(segments...);
    }

    size_t count() const;
    bool empty() const;
    
    const Segment & get(size_t index, const Segment & defaultValue=Segment()) const;
    UrlPath & set(size_t index, const Segment & value);
    
    Segment pop();
    Segment remove(size_t index);
    void clear();

    const PathList & list() const;
    UrlPath & set(const UrlPath & urlPath);
    UrlPath & set(const PathList & pathList);

    std::string toString() const;

protected:
    void checkIndex(size_t index) const;
    
private:
    PathList path;
};

#endif // URLPATH_H
