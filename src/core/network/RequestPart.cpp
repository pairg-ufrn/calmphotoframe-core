/*
 * RequestPart.cpp
 *
 *  Created on: 19/09/2014
 *      Author: noface
 */

#include "RequestPart.h"

#include "readers/BufferedStreamReader.h"
#include "readers/DelimitedStreamReader.h"
#include "readers/InputStreamReader.h"
#include "readers/StringBufferOutput.h"
#include "readers/StreamOutput.h"
#include "network/readers/DelimitedBufferReader.h"

#include "utils/StringUtils.h"
#include "utils/Log.h"
#include "utils/IllegalStateException.h"

#include <string>
#include <vector>


using namespace calmframe::utils;
using namespace std;
using namespace CommonExceptions;

namespace calmframe {
namespace network {

namespace Constants {
    static const char * CRLF = "\r\n";
}//namespace

#define HEADER_SEPARATOR ':'
#define CONTENT_TYPE_HEADER "Content-Type"

#include "utils/IllegalStateException.h"

RequestPart::RequestPart()
{
}

RequestPart &RequestPart::start(Request & request){
    auto delimiter = extractDelimiter(request);
    
    this->delimitedReader = std::make_shared<DelimitedStreamReader>(&request.getContent(), delimiter);
    this->reader = std::make_shared<BufferedStreamReader>(delimitedReader);
    
    return *this;
}

string RequestPart::extractDelimiter(const Request& request)
{
    this->boundary = request.getContentType().getParameter("boundary");
    string delimiter = string(Constants::CRLF).append("--").append(boundary);
    
    if(boundary.empty()){
        throw IllegalStateException("When building RequestPart, the request ContentType should have a boundary parameter.");
    }

    return delimiter;
}

bool RequestPart::foundDelimiter() const{
    return delimitedReader->found();
}

bool RequestPart::containsHeader(const std::string& headerName) const {
	return headers.find(headerName) != headers.end();
}

string RequestPart::getHeaderValue(const string & headerName, const string & defaultValue) const{
	auto found =headers.find(headerName);
	if(found != headers.end()){
		return found->second;
	}
	
    return defaultValue;
}

bool RequestPart::hasContent(){
    return !content.str().empty();
}

bool RequestPart::hasData(){
    return getContent().good();
}

int RequestPart::ignoreData(int maxBytes){
    return getContent().ignore(maxBytes).gcount();
}

int RequestPart::readData(string * output, int maxBytes){
    StringBufferOutput stringOut(output);
    return readData(maxBytes, &stringOut);   
}

int RequestPart::readData(ostream * output, int maxBytes){
    StreamOutput streamOut(output);
    return readData(maxBytes, &streamOut);   
}

bool RequestPart::readUntilFind(const string & delimiter, string * output, int maxBytes){
    StringBufferOutput strOutput{output};
    DelimitedBufferReader delimiterReader{strOutput, delimiter};
    
    //FIXME: DelimitedBufferReader may not return the correct amount of readed bytes (some can still be buffered)
    return readData(maxBytes, &delimiterReader);
}

int RequestPart::readData(int maxBytes, InputFilter * output)
{
    InputStreamReader inputReader(&getContent());
    inputReader.filter(output);
    
    return inputReader.read(maxBytes);    
}

bool RequestPart::readHeaders() {
	string headersData;
    if(this->reader->readUntilFind(string(Constants::CRLF).append(Constants::CRLF), &headersData)){
        extractHeaders(headersData);
		return true;
    }

    return false;
}

bool RequestPart::readBody(){
    string body;
    if(reader->readUntilFind(string(Constants::CRLF).append(boundary),&body) || foundDelimiter()){
        this->content.str(std::move(body));
        
        return true;
    }
    
    return false;
}

istream &RequestPart::getContent(){
    return this->content;
}

const istream &RequestPart::getContent() const{
    return this->content;
}

void RequestPart::extractHeaders(const std::string& partHeadersString) {
	setHeadersFromString(partHeadersString);
    setContentTypeFromHeaders();
}

void RequestPart::setHeadersFromString(const std::string& partHeadersString) {
	this->headers.clear();
	/*FIXME: o certo seria fazer a divisão com base na string "\r\n", mas método split não permite isso(por hora)
	 porém, 'SPLIT_TRIM' e 'SPLI_IGNORE_EMPTY' eliminam os caracteres que são espaço (\r ?)
	 */
	vector<string> headersLines = StringUtils::instance().splitAndTrim(partHeadersString, '\n');
	for (size_t i = 0; i < headersLines.size(); ++i) {
		string headerName;
		string headerValue;
		StringUtils::instance().splitAndTrim(headersLines[i], HEADER_SEPARATOR, headerName, headerValue);
		headers[headerName] = headerValue;
	}
}

void RequestPart::setContentTypeFromHeaders() {
	std::map<string, string>::const_iterator contentHeaderPos = headers.find(
			CONTENT_TYPE_HEADER);
	this->contentType =
			contentHeaderPos != headers.end() ?
					ContentType(contentHeaderPos->second) :
					ContentType("text", "plain");
}

} /* namespace network */
} /* namespace calmframe */

