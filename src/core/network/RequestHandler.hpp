/*
 * RequestHandler.hpp
 *
 *  Created on: 18/09/2014
 *      Author: noface
 */

#ifndef REQUESTHANDLER_HPP_
#define REQUESTHANDLER_HPP_

#include "network/Request.h"
#include "network/Response.h"

namespace calmframe {
namespace network {

class RequestHandler {
public:
	virtual ~RequestHandler()
	{}
	virtual void onCreate(Request & request, Response & response)=0;
};

} /* namespace network */
} /* namespace calmframe */

#endif /* REQUESTHANDLER_HPP_ */
