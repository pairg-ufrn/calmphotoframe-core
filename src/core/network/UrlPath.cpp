#include "UrlPath.h"

#include "utils/StringUtils.h"
#include <stdexcept>

using namespace std;

using calmframe::utils::StringUtils;

UrlPath::UrlPath(const string & pathStr)
    : path(StringUtils::instance().split(pathStr, '/', StringUtils::SPLIT_IGNORE_EMPTY))
{}

UrlPath::UrlPath(const UrlPath::PathList & pathList)
    : path(pathList)
{}

UrlPath & UrlPath::add(const UrlPath::Segment & pathSegment){
    path.push_back(pathSegment);
    return *this;
}

UrlPath &UrlPath::add(const char * pathSegment){
    path.push_back(pathSegment);
    return *this;
}

UrlPath::Segment UrlPath::pop(){
    auto back = path.back();
    path.pop_back();
    return back;
}

UrlPath::Segment UrlPath::remove(size_t index){
    auto item = get(index);
    this->path.erase(path.begin() + index);
    
    return item;
}

const UrlPath::Segment & UrlPath::get(size_t index, const Segment & defaultValue) const{
    if(index >= path.size()){
        return defaultValue;
    }
    return this->path[index];
}

UrlPath &UrlPath::set(size_t index, const UrlPath::Segment & value){
    checkIndex(index);
    this->path[index] = value;
    
    return *this;
}

size_t UrlPath::count() const{
    return this->path.size();
}

bool UrlPath::empty() const{
    return path.empty();
}

void UrlPath::clear(){
    this->path.clear();
}

UrlPath &UrlPath::set(const UrlPath & urlPath){
    return set(urlPath.list());
}

UrlPath &UrlPath::set(const UrlPath::PathList & pathList){
    this->path = pathList;
    return *this;
}

const UrlPath::PathList &UrlPath::list() const{
    return this->path;
}

string UrlPath::toString() const{
    if(empty()){
        return string();
    }

    stringstream out;
    for(size_t i=0; i < count(); ++i){
        out << '/' << get(i);
    }
    return out.str();
}

void UrlPath::checkIndex(size_t index) const
{
    if(index >= path.size()){
        throw std::out_of_range("UrlPath: Trying to access invalid path segment");
    }    
}
