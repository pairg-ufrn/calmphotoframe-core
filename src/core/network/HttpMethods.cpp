#include "HttpMethods.h"

#include <map>

using namespace std;

namespace calmframe {

typedef map<string, HttpMethods::Method> StrMethodMap;

static const StrMethodMap & strToMethod(){
    static StrMethodMap mapStrToMethods;
    if(mapStrToMethods.empty()){
        mapStrToMethods[HttpMethods::names::GET]     = HttpMethods::GET;
        mapStrToMethods[HttpMethods::names::POST]    = HttpMethods::POST;
        mapStrToMethods[HttpMethods::names::PUT]     = HttpMethods::PUT;
        mapStrToMethods[HttpMethods::names::DELETE]  = HttpMethods::DELETE;
        mapStrToMethods[HttpMethods::names::OPTIONS] = HttpMethods::OPTIONS;
    }

    return mapStrToMethods;
}

HttpMethods::Method HttpMethods::fromString(const std::string & methodStr){
    StrMethodMap::const_iterator iter = strToMethod().find(methodStr);
    if(iter == strToMethod().end()){
        return UNKOWN;
    }
    return iter->second;
}

}//namespace
