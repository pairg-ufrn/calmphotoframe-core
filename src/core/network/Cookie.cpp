#include "Cookie.h"
#include "utils/StringUtils.h"
#include "Headers.h"

#include "utils/Log.h"

namespace calmframe {
namespace network{

using namespace std;
using namespace utils;

Cookie::Cookie()
    : super(Headers::SetCookie, "")
{
    setOmitEmptyValuesOnToString(true);
}
Cookie::Cookie(const std::string &cookieName, const std::string &cookieValue)
    : super(Headers::SetCookie, "")
    , cookieName(cookieName)
    , value(cookieValue)
{
    setOmitEmptyValuesOnToString(true);
    updateValue();
}
Cookie::Cookie(const Header &header)
    : super(header.getName(), header.getValue())
{
    setOmitEmptyValuesOnToString(true);
    decodeHeaderValue(header.getValue());
}
Cookie::~Cookie()
{}

const std::string Cookie::getCookieName() const{
    return this->cookieName;
}
const std::string Cookie::getCookieValue() const{
    return value;
}
const std::string Cookie::getCookiePath() const{
    return getParameter(CookieParameters::Path);
}

const string &Cookie::getValue() const{
    return this->headerValue;
}
void Cookie::setValue(const string &value){
    this->decodeHeaderValue(value);
}

void Cookie::setCookieName(const std::string &cookieName){
    this->cookieName = cookieName;
}
void Cookie::setCookieValue(const string &cookieValue){
    this->value = cookieValue;
}
void Cookie::setCookiePath(const string &cookiePath){
    this->putParameter(CookieParameters::Path, cookiePath);
}

void Cookie::decodeHeaderValue(const string &value){
    clearParameters();
    if(!value.empty()){
        vector<string> params = StringUtils::instance().splitAndTrim(value, getParameterSeparator());
        for(size_t i=0; i < params.size(); ++i){
            string paramName, paramValue;
            StringUtils::instance().splitAndTrim(params[i],getValueSeparator(),paramName, paramValue);
            if(i!=0){
                this->putParameter(paramName, paramValue);
            }
            else{
                this->setCookieName(paramName);
                this->setCookieValue(paramValue);
            }
        }
    }
    this->updateValue();
}

void Cookie::updateValue(){
    super::updateValue();
    this->headerValue = getCookieName() + "=" + getCookieValue();
    string parametersValue = super::getValue();
    if(!parametersValue.empty()){
        headerValue.append("; ").append(parametersValue);
    }
}

} //namespace
} //namespace
