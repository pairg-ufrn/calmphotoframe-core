#include "ParameterizedHeader.h"

#include "Cookie.h"
#include "utils/StringUtils.h"

namespace calmframe {
namespace network{

using namespace std;
using namespace utils;

const char ParameterizedHeader::DEFAULT_PARAM_SEPARATOR=';';
const char ParameterizedHeader::DEFAULT_VALUE_SEPARATOR='=';

char ParameterizedHeader::getParameterSeparator() const{
    return parameterSeparator;
}
void ParameterizedHeader::setParameterSeparator(char value){
    parameterSeparator = value;
}
char ParameterizedHeader::getValueSeparator() const{
    return valueSeparator;
}
void ParameterizedHeader::setValueSeparator(char value){
    valueSeparator = value;
}


bool ParameterizedHeader::getOmitEmptyValuesOnToString() const
{
    return omitEmptyValues;
}

void ParameterizedHeader::setOmitEmptyValuesOnToString(bool value)
{
    omitEmptyValues = value;
}
void ParameterizedHeader::clearParameters(){
    this->parameters.clear();
}

string ParameterizedHeader::getParameter(const string &paramName, const string &defaultValue) const{
    map<string,string>::const_iterator paramPair = this->parameters.find(paramName);
    if(paramPair != parameters.end()){
        return string(paramPair->second);
    }
    return defaultValue;
}

void ParameterizedHeader::putParameter(const string &paramName, const string &paramValue, const bool &updateHeaderValue){
    this->parameters[paramName] = paramValue;
    if(updateHeaderValue){
        updateValue();
    }
}
void ParameterizedHeader::removeParameter(const string &paramName){
    parameters.erase(paramName);
}

bool ParameterizedHeader::containsParameter(const string &paramName) const{
    return this->parameters.find(paramName) != parameters.end();
}

void ParameterizedHeader::updateValue(){
    string newValue;

    map<string,string>::const_iterator it = this->parameters.begin();
    for(; it != parameters.end(); ++it){
        if(it!=parameters.begin()){
            newValue.push_back(parameterSeparator);
            newValue.push_back(' ');
        }

        newValue.append(it->first);
        if(!omitEmptyValues || !it->second.empty()){
            newValue.push_back(valueSeparator);
            newValue.append(it->second);
        }
    }
    super::setValue(newValue);
}

void ParameterizedHeader::decodeHeaderValue(const std::string &value){
    this->parameters.clear();
    if(!value.empty()){
        vector<string> params = StringUtils::instance().splitAndTrim(value, parameterSeparator);
        for(size_t i=0; i < params.size(); ++i){
            string paramName, paramValue;
            StringUtils::instance().splitAndTrim(params[i],valueSeparator,paramName, paramValue);
            this->putParameter(paramName, paramValue);
        }
    }
    this->updateValue();
}

} //namespace
} //namespace


