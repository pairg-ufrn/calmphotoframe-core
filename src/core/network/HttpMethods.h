#ifndef HTTPMETHOD_H
#define HTTPMETHOD_H

#include <string>

namespace calmframe {

namespace HttpMethods {
    enum Method{UNKOWN, GET, POST, PUT, DELETE, OPTIONS};

    Method fromString(const std::string & methodStr);

namespace names{
    const std::string GET       = "GET";
    const std::string POST      = "POST";
    const std::string PUT       = "PUT";
    const std::string DELETE    = "DELETE";
    const std::string OPTIONS   = "OPTIONS";
}
}

}//namespace

#endif // HTTPMETHOD_H

