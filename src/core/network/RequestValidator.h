#ifndef REQUESTVALIDATOR_H
#define REQUESTVALIDATOR_H

namespace calmframe {
namespace network {

class Request;
class RequestValidator{
public:
    virtual ~RequestValidator(){}
    virtual bool validateRequest(Request & request) const =0;
};

}
}

#endif // REQUESTVALIDATOR_H

