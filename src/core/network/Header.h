/*
 * Header.h
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#ifndef NETWORK_HEADER_H_
#define NETWORK_HEADER_H_

#include <string>
#include <ostream>
namespace calmframe {
namespace network {

class Header {
private:
    std::string name, value;
public:
    Header()
    {}
    Header(const std::string & name, const std::string & value)
        : name(name), value(value)
    {}
    Header(const Header & other)
        : name(other.getName()), value(other.getValue())
    {}
	virtual ~Header(){}

    Header& operator=(const Header & other);

    virtual const std::string& getName()  const { return name;  }
    virtual const std::string& getValue() const { return value; }

    virtual void setName(const std::string& name) {
		this->name = name;
	}
    virtual void setValue(const std::string& value) {
        this->value = value;
	}

    virtual bool empty() const;

    virtual void toStream(std::ostream &out) const;
    virtual std::string toString() const;
    operator std::string() const;
	friend std::ostream& operator<< (std::ostream &out, const Header & header);
};

} /* namespace network */
} /* namespace calmframe */

#endif /* NETWORK_HEADER_H_ */
