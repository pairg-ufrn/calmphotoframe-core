#include "Url.h"

#include "utils/StringUtils.h"
#include "utils/StringCast.h"
#include "utils/IndexOutOfBoundsExceptions.h"

#include <ctype.h>
#include <sstream>

namespace calmframe {
namespace network {

namespace Constants {
    static const char PATH_SEP = '/';
    static const char QUERY_SEP = '?';
    static const char FRAG_SEP = '#';
    static const char SCHEME_SEP = ':';
    static const char PORT_SEP = ':';
    static const char PARAM_SEP = '&';
    static const char PAIR_SEP = '=';
}//namespace

using namespace std;
using namespace utils;

Url::PathsList Url::splitPaths(const std::string & pathsStr){
    return StringUtils::instance().splitAndTrim(pathsStr, Constants::PATH_SEP);
}

Url::PathsList Url::splitUrl(const std::string &url
                            , std::string *outPath
                            , std::string *outQuery
                            , std::string *outFragment
                            , std::string *outHost
                            , std::string *outPort
                            , std::string *outScheme)
{
    string part1;

    StringUtils::instance().splitAndTrim(url, Constants::FRAG_SEP, &part1, outFragment);
    StringUtils::instance().splitAndTrim(part1, Constants::QUERY_SEP, &part1, outQuery);

    if(isalpha(part1[0])){//If start with letter, should have a scheme
        StringUtils::instance().splitAndTrim(part1, Constants::SCHEME_SEP,outScheme, &part1);

        //Removes "//"
        size_t index = part1.find("//");
        if(index != string::npos){
            part1.erase(part1.begin() + index, part1.begin() + index + 2);
        }
    }

    if(part1.empty()){
        return Url::PathsList();
    }

    string authority, pathsStr;
    StringUtils::instance().split(part1, Constants::PATH_SEP, authority, pathsStr);

    if(outPath != NULL){
        *outPath = pathsStr;
    }

    if(!authority.empty()){
        StringUtils::instance().split(authority, Constants::PORT_SEP, outHost, outPort);
    }
    return splitPaths(pathsStr);
}

int Url::getPort() const
{
    return port;
}

void Url::setPort(int value)
{
    port = value;
}


Url::Url()
    : port(0)
{}

Url::Url(const char * url)
{
    set(url);
}

Url::Url(const string &url)
{
    this->set(url);
}



Url::~Url()
{

}

void Url::set(const std::string &url)
{
    string query;
    string portStr;
    auto paths = splitUrl(url, NULL, &query, &fragment, &host, &portStr, &scheme);
    this->path().set(paths);
    
    port = StringCast::cast().tryToInt(portStr, 0);

    this->query().clear();
    this->query().set(query);
//    decodeQueryArgs(query, _query);
}

const UrlPath &Url::path() const{
    return _path;
}

UrlPath &Url::path(){
    return _path;
}

Url &Url::path(const UrlPath & path){
    this->_path = path;
    return *this;
}

const UrlQuery &Url::query() const{
    return _query;
}

UrlQuery &Url::query(){
    return _query;
}

Url &Url::query(const UrlQuery & query){
    this->_query = query;
    return *this;
}

const std::string & Url::getScheme() const{
    return scheme;
}

void Url::setScheme(const std::string &value){
    scheme = value;
}

const std::string &Url::getFragment() const{
    return fragment;
}

const string &Url::getHost() const{
    return host;
}

void Url::setHost(const std::string &value){
    host = value;
}

string Url::toString() const
{
    stringstream sstream;
    if(!getScheme().empty()){
        sstream << getScheme() << Constants::SCHEME_SEP;
    }

    if(!getHost().empty()){
        //TODO: nem toda url contém // ?
        sstream << "//";
        sstream << getHost();

        if(getPort() != 0){
            sstream << Constants::PORT_SEP << getPort();
        }
    }

    joinPaths(sstream, path().list());

    if(!query().empty()){
        sstream << Constants::QUERY_SEP << query();
//        for(ConstParamIter iter = queryParams.begin(); iter != queryParams.end(); ++iter){
//            if(iter != queryParams.begin()){
//                sstream << Constants::PARAM_SEP;
//            }
//            sstream << iter->first << Constants::PAIR_SEP << iter->second;
//        }
    }

    if(!getFragment().empty()){
        sstream << Constants::FRAG_SEP << getFragment();
    }

    return sstream.str();
}

void Url::joinPaths(stringstream & sstream, const PathsList& paths)
{
    for(size_t i =0; i < paths.size(); ++i){
        sstream << Constants::PATH_SEP << paths[i];
    }
}

string Url::joinPaths(const Url::PathsList & paths){
    stringstream sstream;
    joinPaths(sstream, paths);
    return sstream.str();
}

}//namespace
}//namespace
