/*
 * Response.cpp
 *
 *  Created on: 18/09/2014
 *      Author: noface
 */

#include "network/Response.h"
#include "network/Headers.h"
#include "network/StatusResponses.h"
#include "utils/TimeUtils.h"
#include <sstream>

using std::stringstream;

namespace calmframe {
namespace network {

namespace Constants {
    static const char * HTTP_HEADER = "HTTP/1.1";
    static const char * SPACE = " ";
    static const char * LINE_END = "\r\n";
}

Response::Response(CivetServer *server, mg_connection *conn)
    : server(server)
    , connection(conn)
    , statusResponse(StatusResponses::OK)
    , sentHeaders(false)
{
    this->putHeader(Headers::Date, TimeUtils::instance().getCurrentHttpDateTime());
}

Response::~Response(){}

const ContentType &Response::getContentType() const{
    return contentType;
}
ContentType &Response::getContentType(){
    return contentType;
}

void Response::setContentType(const ContentType &value){
    contentType = value;
}

Header &Response::getHeader(const std::string &name){
    if(contentType.getName() == name){
        return contentType;
    }
    static Header nullHeader;
    HeadersMap::iterator iter = this->headers.find(name);
    if(iter != headers.end()){
        return iter->second;
    }
    else{
        return nullHeader;
    }
}

const Header &Response::getHeader(const std::string &name) const{
    if(contentType.getName() == name){
        return contentType;
    }
    static Header nullHeader;
    HeadersMap::const_iterator iter = this->headers.find(name);
    return iter != headers.end() ? iter->second : nullHeader;
}

bool Response::hasSentHeaders() const{
    return this->sentHeaders;
}

void Response::putHeader(const std::string &name, const std::string &value){
    this->putHeader(Header(name, value));
}

void Response::putHeader(const Header &header){
    if(header.getName() != contentType.getName()){
        this->headers[header.getName()] = header;
    }
    else{
        this->contentType = header;
    }
}

void Response::clearHeaders(){
    headers.clear();
}

void Response::removeHeader(const Header &header){
    this->removeHeader(header.getName());
}

void Response::removeHeader(const std::string &headerName){
    this->headers.erase(headerName);
}

StatusResponse &Response::statusLine(){
    return statusResponse;
}

const StatusResponse &Response::statusLine() const{
    return statusResponse;
}

void Response::setStatusLine(const StatusResponse &statusLine){
    this->statusResponse = statusLine;
}

void Response::sendStatusLine(){
    sendStatus(this->statusResponse);
}

void Response::finishHeaders(){
    write(Constants::LINE_END);
    this->sentHeaders = true;
}

void Response::sendStatus(const StatusResponse &statusResponse){
    localBuffer << Constants::HTTP_HEADER         << Constants::SPACE
                << statusResponse.getStatusCode() << Constants::SPACE
                << statusResponse.getMessage()    << Constants::LINE_END;
    this->write(localBuffer.str());
    this->clearLocalBuffer();
}

void Response::sendHeader(const Header &header){
    localBuffer << header.toString() << Constants::LINE_END;
    this->write(localBuffer.str());
    this->clearLocalBuffer();
}

void Response::clearLocalBuffer(){
    localBuffer.str("");
    localBuffer.clear();
}

void Response::sendHeaders(){
    sendStatusLine();
    sendHeader(getContentType());
    for(HeadersMap::const_iterator it = headers.begin();
        it != headers.end(); ++it)
    {
        const Header & header = it->second;
        sendHeader(header);
    }
    finishHeaders();
}

void Response::send(const std::string &data){
    write(data.c_str());
}

void Response::sendBinary(const void *data, size_t len){
    mg_write(connection, data, len);
}

void Response::sendFile(const std::string &filepath){
    this->sendFile(filepath.c_str());
}
void Response::sendFile(const char *filepath){
    mg_send_file(this->connection, filepath);
}

void Response::write(const std::string &data){
    write(data.c_str());
}
void Response::write(const char *data){
    mg_printf(connection, "%s",data);
}

Response & operator<<(Response & response, const std::string & str){
    response.send(str);
    return response;
}
Response & operator<<(Response & response, const Serializer & serializer){
    stringstream  strStream;
    strStream << serializer;
    /*FIXME: converter a stream para string é um problema de performance!
        Response deveria ser capaz de lidar diretamente com streams.*/
    response.send(strStream.str());

    return response;
}


} /* namespace network */
} /* namespace calmframe */
