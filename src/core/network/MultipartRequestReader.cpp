#include "MultipartRequestReader.h"

#include "network/Request.h"
#include "network/RequestPart.h"

#include "utils/Log.h"

using std::string;

namespace calmframe {
namespace network {

namespace Constants {
    static const char * CR = "\r";
    static const char * LF = "\n";
    static const char * CRLF = "\r\n";
    static const char * DOUBLE_HYPHEN = "--";
}//namespace

bool MultipartRequestReader::start(Request * request){
    //TODO: throw exception if boundary not exist
    this->boundary = request->getContentType().getParameter("boundary");
    this->request = (!boundary.empty() ? request : nullptr);
    
    if(this->request && !skipToStartData()){
        clear();
        return false;
    }
    return this->request;
}

MultipartRequestReader::PartPtr MultipartRequestReader::next(){
    if(!request){
        return {};
    }
    
    auto part = std::make_shared<RequestPart>();
    part->start(*request);

    if(!part->readHeaders()){
        clear();
        part.reset();
        return part;
    }
    part->readBody();
    if(!skipToNextPart()){
        clear();
    }

    return part;
}

void MultipartRequestReader::clear(){
    this->request = nullptr;
}

bool MultipartRequestReader::skipToStartData(){
    if(boundary.empty()){
        logError("Has not boundary!");
        return false;
    }
    
    string delimiter = string(Constants::DOUBLE_HYPHEN).append(boundary);
    
    bool foundDelimiter = readUntilFind(delimiter);
    if(foundDelimiter){
        return readUntilFind(Constants::CRLF);
    }
    else{
        logError("RequestBackup::skipToStartData: \t"
                 "Could not found initial Multipart delimiter");
    }
    
    return  false;
}

bool MultipartRequestReader::skipToNextPart(){
    string readedData;
    return (readUntilFind(Constants::CRLF,&readedData,2) && readedData != Constants::DOUBLE_HYPHEN);
}

bool MultipartRequestReader::readUntilFind(const string & str, std::string * out, int maxBytes)
{
    return request->readUntilFind(str, out, maxBytes);
}


}//namespace
}//namespace
