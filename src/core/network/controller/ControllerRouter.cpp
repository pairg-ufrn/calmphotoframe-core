#include "ControllerRouter.h"

#include "UrlMatcher.h"

#include "network/Url.h"
#include "utils/NullPointerException.hpp"
#include "utils/StringUtils.h"
#include "utils/Log.h"

#include <string>
#include <map>

#include "network/Request.h"
#include "network/Response.h"

using namespace calmframe::utils;
using namespace std;

namespace calmframe {
namespace network{

void ControllerRouter::add(const string &baseUrl, WebController *controller){
    CommonExceptions::assertNotNull(controller, "Parameter controller should not be null");

    this->controllers.push_back(RouteItem(baseUrl, controller));
}

size_t ControllerRouter::countControllers() const
{
    return controllers.size();
}


network::Url ControllerRouter::makeRelativeUrl(const Url & baseUrl, Url url)
{
    for(size_t i=0; i < baseUrl.path().count() && url.path().count() > 0; ++i){
        url.path().remove(0);
    }

    return url;
}

bool ControllerRouter::onRequest(Request & request, Response & response){
    Url requestUrl(request.getUrl());
    return forward(request, response, requestUrl);
}
bool ControllerRouter::forward(Request & request, Response & response, const network::Url & url){
    bool handled = false;
    ConstControllerIter iter = controllers.begin();
    do{
        iter = findController(url, request, iter);
        if(iter == controllers.end()){
            break;
        }

        Url relativeUrl = makeRelativeUrl(iter->route, url);
        handled = forwardRequest(iter->controller, request, response
                                , relativeUrl);
    }while(!handled && ++iter != controllers.end());

    return handled;
}

bool ControllerRouter::matchUrl(const string & url) const
{
    ConstControllerIter begin = controllers.begin();
    return findController(url, NULL, begin) != controllers.end();
}

ControllerRouter::ConstControllerIter ControllerRouter::findController(const Url & requestUrl, Request & request,
    ControllerRouter::ConstControllerIter & begin) const
{
    return findController(requestUrl, &request, begin);
}


ControllerRouter::ConstControllerIter ControllerRouter::findController(const Url & requestUrl, Request * request
    , ControllerRouter::ConstControllerIter & begin) const
{
    UrlMatcher matcher;

    if(begin == controllers.end()){
        return controllers.end();
    }

    ConstControllerIter iter = begin;
    for(; iter != controllers.end(); ++iter)
    {
        const Url & controllerUrl = iter->route;
        if(matcher.match(controllerUrl, requestUrl)){
            if(request != NULL){
                const Url::ParamMap & params = matcher.getParams();
                for(Url::ConstParamIter pIter = params.begin(); pIter != params.end(); ++pIter)
                {
                    if (!request->hasParam(pIter->first)) {
                        request->putParameter(pIter->first, pIter->second);
                    }
                }
            }

            break;
        }
    }

    return iter;
}

bool ControllerRouter::forwardRequest(WebController * controller, Request & request, Response & response, const Url & url)
{
    CHECK_NOT_NULL(controller);
    Url initUrl = request.url();
    request.url(url);
    bool handled = controller->onRequest(request, response);
    request.url(initUrl);

    return handled;
}

}
}
