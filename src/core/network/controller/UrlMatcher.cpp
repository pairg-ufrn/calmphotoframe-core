#include "UrlMatcher.h"

namespace calmframe {

bool UrlMatcher::isParameterPath(const std::string & path){
    return !path.empty() && path.at(0) == ':';
}

bool UrlMatcher::match(const calmframe::network::Url & baseUrl, const calmframe::network::Url & urlToMatch){
    clearParams();

    if(baseUrl.path().count() <= urlToMatch.path().count()){
        for(size_t i=0; i < baseUrl.path().count(); ++i){
            const std::string & path = baseUrl.path().get(i);
            if(isParameterPath(path)){
                parameters[path.substr(1)] = urlToMatch.path().get(i);
            }
            else if(baseUrl.path().get(i) != urlToMatch.path().get(i)){
                return false;
            }
        }
        return true;
    }

    return false;
}

}//namespace

