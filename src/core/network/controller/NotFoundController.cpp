#include "NotFoundController.h"
#include "network/StatusResponses.h"

#include "network/Request.h"
#include "network/Response.h"

namespace calmframe {
namespace network {

bool NotFoundController::onRequest(Request &, Response &response){
    response.setStatusLine(StatusResponses::NOT_FOUND);
    response.sendHeaders();
    response.send("");
    return true;
}

}//namespace
}//namespace
