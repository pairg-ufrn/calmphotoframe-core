#ifndef SECURITYCONTROLLER_H
#define SECURITYCONTROLLER_H

#include "CollectionControllerRouter.h"
#include "WebController.h"
#include "network/RequestValidator.h"

#include <vector>

namespace calmframe {
namespace network {

class SecurityController : public CollectionControllerRouter
{
private:
    RequestValidator * validator;
public:
    SecurityController();
    ~SecurityController();
public:
    RequestValidator *getValidator() const;
    void setValidator(RequestValidator *value);

    virtual bool onRequest(Request & request, Response & response);
protected:
    virtual bool validateRequest(Request & request) const;
    virtual void sendUnauthorizedResponse(Response & response);
};

}//namespace
}//namespace
#endif // SECURITYCONTROLLER_H
