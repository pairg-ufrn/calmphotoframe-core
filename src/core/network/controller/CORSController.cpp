#include "CORSController.h"

#include "network/Headers.h"
#include "utils/StringUtils.h"
#include "utils/Log.h"

#include "network/Request.h"
#include "network/Response.h"

using std::string;

namespace calmframe {
namespace network {

using calmframe::utils::StringUtils;

CORSController::CORSController()
{
    const char * defaultAllowedMethods[] = {"GET", "POST", "PUT", "DELETE", "OPTIONS"};
    allowedMethods.insert(defaultAllowedMethods, defaultAllowedMethods + 5);
}

std::set<std::string> CORSController::getAllowedMethods() const{
    return allowedMethods;
}

void CORSController::setAllowedMethods(const std::set<std::string> &value){
    allowedMethods = value;
}

void CORSController::putAllowedMethod(const string &httpMethod){
    this->allowedMethods.insert(httpMethod);
}

void CORSController::removeAllowedMethod(const string &httpMethod){
    this->allowedMethods.erase(httpMethod);
}

bool CORSController::matchUrl(const string &) const
{
    //Accepts all requests (independent of url)
    return true;
}

bool CORSController::onRequest(Request &request, Response &response){
    if(request.containsHeader(Headers::Origin)){
        response.putHeader(Headers::AccessControlAllowOrigin     , request.getHeaderValue(Headers::Origin));
        response.putHeader(Headers::AccessControlAllowCredentials, "true");

        if(isPreflightRequest(request)){
            handlePreflightRequest(request, response);
            return true;
        }
    }
    return super::onRequest(request, response);
}

bool CORSController::isPreflightRequest(Request &request) const
{
    return request.getMethod() == "OPTIONS"
            && request.containsHeader(Headers::Origin)
            && request.containsHeader(Headers::AccessControlRequestMethod);
}

void CORSController::handlePreflightRequest(Request &request, Response &response)
{
    string allowedMethodsStr = StringUtils::instance().join(this->allowedMethods.begin(), allowedMethods.end(), ", ");
    response.putHeader(Headers::AccessControlAllowMethods, allowedMethodsStr);

    if(request.containsHeader(Headers::AccessControlRequestHeaders)){
        response.putHeader(Headers::AccessControlAllowHeaders
                           , request.getHeaderValue(Headers::AccessControlRequestHeaders));
    }

    response.sendHeaders();
    response.send("");
}


}//calmframe
}//network
