#ifndef CORSCONTROLLER_H
#define CORSCONTROLLER_H

#include "WebController.h"
#include <set>
#include <string>

namespace calmframe {
namespace network {

class CORSController : public WebController
{
    typedef WebController super;

    std::set<std::string> allowedMethods;
public:
    CORSController();

    std::set<std::string> getAllowedMethods() const;
    void setAllowedMethods(const std::set<std::string> &value);
    void putAllowedMethod(const std::string & httpMethod);
    void removeAllowedMethod(const std::string & httpMethod);

    virtual bool matchUrl(const std::string & url) const;

    virtual bool onRequest(Request &request, Response &response);

protected:
    bool isPreflightRequest(Request &request) const;
    void handlePreflightRequest(Request &request, Response &response);
};

}//calmframe
}//network


#endif // CORSCONTROLLER_H
