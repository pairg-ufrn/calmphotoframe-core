#ifndef NOTFOUNDCONTROLLER_H
#define NOTFOUNDCONTROLLER_H

#include "WebController.h"

namespace calmframe {
namespace network {

class NotFoundController : public WebController
{
public:
    virtual bool onRequest(Request &request, Response &response);
};

}//namespace
}//namespace

#endif // NOTFOUNDCONTROLLER_H
