#ifndef URLMATCHER_H
#define URLMATCHER_H

#include "network/Url.h"

namespace calmframe {

using network::Url;

class UrlMatcher{
public:
private:
    Url::ParamMap parameters;

public:
    const Url::ParamMap & getParams() const{
        return parameters;
    }
    void clearParams(){
        parameters.clear();
    }

    bool isParameterPath(const std::string & path);

    bool match(const Url & baseUrl, const Url & urlToMatch);
};

}//namespace

#endif // URLMATCHER_H
