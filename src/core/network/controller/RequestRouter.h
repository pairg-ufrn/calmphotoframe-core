#ifndef REQUESTROUTER_H
#define REQUESTROUTER_H

namespace calmframe {

namespace network {
    class Request;
    class Response;
    class Url;
}//namespace

/** An entity capable of deliver requests to other controllers (based on a Url)*/
class RequestRouter{
public:
    /** Forwards a request to some Url*/
    virtual bool forward(network::Request & request, network::Response & response, const network::Url & url)=0;
};

}//namespace

#endif // REQUESTROUTER_H

