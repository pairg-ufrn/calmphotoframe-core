#include "WebController.h"

#include "network/Url.h"
#include "network/civetweb/CivetRequest.h"

#include "utils/StringUtils.h"
#include "utils/Macros.h"

#include <string>

using namespace calmframe::utils;
using namespace std;

namespace calmframe {
namespace network {

namespace Constants {
    static const char URL_SEPARATOR = '/';
}

const char * WebController::WILDCARD_PATH = "*";
const char * WebController::WILDCARD_PATHSEQ = "**";

string WebController::normalizeUrlPath(string urlPath) const
{
    if(urlPath.empty() || urlPath[0] != '/'){
        urlPath.insert(0, "/");
    }

    return urlPath;
}

bool WebController::matchUrl(const std::string &urlStr) const{
    return matchUrlImpl(normalizeUrlPath(getBaseUrl()), urlStr);
}

bool WebController::matchPaths(const string &controllerPath, const string &urlPath) const
{
    if(controllerPath == WILDCARD_PATH){
        return true;
    }
    return controllerPath == urlPath;
}

static void putInt(int intValue, int * ptrOut){
    if(ptrOut != NULL){
        *ptrOut = intValue;
    }
}

bool WebController::matchUrlImpl(const string &controllerUrl, const string &urlStr, int *outMatchCount) const
{
    Url baseUrl(controllerUrl);
    Url url(urlStr);

    if(baseUrl.path().count() == 0){
        putInt(0, outMatchCount);
        return true;
    }

    size_t pathIndex = 0;
    size_t matchCount = 0;
    for(matchCount=0; matchCount < url.path().count() && pathIndex < baseUrl.path().count(); ++matchCount){
        const string & urlPath = url.path().get(matchCount);
        const string & baseUrlPath = baseUrl.path().get(pathIndex);
        if(baseUrlPath == WILDCARD_PATHSEQ){
            if(pathIndex == baseUrl.path().count() - 1){
                //The last path on base Url is WILDCARD_PATHSEQ, so accepts anything thereafter
                putInt(matchCount, outMatchCount);
                return true;
            }

            //There is a next path on baseUrl, so forwards until find someone that matchs it
            ++pathIndex;
            const std::string & nextPath = baseUrl.path().get(pathIndex);
            while(matchCount < url.path().count() && !matchPaths(url.path().get(matchCount), nextPath)){
                ++matchCount;
            }
            if(matchCount >= url.path().count()){//Passed by all paths in url, and not found a match
                putInt(-1, outMatchCount);
                return false;
            }
        }
        else if(!matchPaths(baseUrlPath, urlPath)){
            putInt(-1, outMatchCount);
            return false;
        }
        ++pathIndex;
    }

    putInt(matchCount, outMatchCount);

    //There is still some paths not matched on baseUrl
    if(pathIndex < baseUrl.path().count()){
        if(pathIndex == baseUrl.path().count() - 1 &&
           baseUrl.path().get(pathIndex) == WILDCARD_PATHSEQ)
        {//WILDCARD_PATHSEQ is the last path on baseUrl
            return true;
        }
        putInt(-1, outMatchCount);
        return false;
    }

    return true;
}

/**
 * Estes métodos podem ser implementados por subclasses para executar responder aos métodos http*/

bool WebController::onRequest(Request &request, Response &response){
//    if(!matchUrl(request.getUrl())){
//        return false;
//    }
    
    string method = request.getMethod();
    if(method == "GET"){
        return onGet(request, response);
    }
    else if(method == "POST"){
        return onPost(request, response);
    }
    else if(method == "PUT"){
        return onPut(request, response);
    }
    else if(method == "DELETE"){
        return onDelete(request, response);
    }
    else if(method == "OPTIONS"){
        return onOptions(request, response);
    }
    return false;
}

bool WebController::onGet(Request&, Response&) {
	return false;
}

bool WebController::onPost(Request&, Response&) {
	return false;
}

bool WebController::onPut(Request&, Response&) {
	return false;
}

bool WebController::onDelete(Request&, Response&) {
    return false;
}

bool WebController::onOptions(Request&, Response&){
    return false;
}

} /* namespace network */
} /* namespace calmframe */
