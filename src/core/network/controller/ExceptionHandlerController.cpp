#include "ExceptionHandlerController.h"

#include "network/StatusResponses.h"
#include "network/Request.h"
#include "network/Response.h"

#include "utils/NullPointerException.hpp"
#include "utils/Log.h"

#include "exceptions/ApiExceptions.h"
#include "exceptions/UnsupportedContentTypeError.h"
#include "exceptions/NotFoundMember.h"

namespace calmframe {

using namespace network;

static const char * internalErrorMsg = "An internal error occurred and the request could not be completed.";

ExceptionHandlerController::ExceptionHandlerController(WebController * delegateController)
    : delegateController(delegateController)
{}

network::WebController * ExceptionHandlerController::getDelegateController() const
{
    return delegateController;
}

void ExceptionHandlerController::setDelegateController(WebController * value)
{
    delegateController = value;
}

bool ExceptionHandlerController::onRequest(network::Request & request, network::Response & response){
    CHECK_NOT_NULL(delegateController);
    
    std::string errMessage;
    StatusResponse errResponse;
    
    try{
        return getDelegateController()->onRequest(request, response);
    }
    catch(const NotFoundMember & ex){
        errResponse = StatusResponses::NOT_FOUND;
        errMessage = ex.getMessage();
    }
    catch(const UnsupportedContentTypeError & ex){
        errResponse = StatusResponses::UNSUPPORTED_MEDIA_TYPE;
        errMessage = ex.getMessage();
    }
    catch(const ClientError & ex){
        errResponse = StatusResponses::BAD_REQUEST;
        errMessage = ex.getMessage();
    }
    catch(const std::exception & ex){
        logError("Server error caught when trying to handle request '%s':\n%s"
                    , request.getUrl().c_str(), ex.what());
                    
        errResponse = StatusResponses::SERVER_ERROR;
        errMessage = internalErrorMsg;
    }
    
    response.statusLine().set(errResponse);
    response.sendHeaders();
    response.send(errMessage);
    
    return true;
}



}//namespace

