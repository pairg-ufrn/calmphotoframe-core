#ifndef EXCEPTIONHANDLERCONTROLLER_H
#define EXCEPTIONHANDLERCONTROLLER_H

#include "WebController.h"

namespace calmframe {

class ExceptionHandlerController : public network::WebController
{
private:
    WebController * delegateController;
public:
    ExceptionHandlerController(WebController * delegateController=NULL);

    WebController * getDelegateController() const;
    void setDelegateController(WebController * value);

    virtual bool onRequest(network::Request &request, network::Response &response);
};

}//namespace

#endif // EXCEPTIONHANDLERCONTROLLER_H
