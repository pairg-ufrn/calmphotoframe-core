#include "SecurityController.h"

#include "network/StatusResponses.h"
#include "network/Headers.h"
#include "network/Request.h"
#include "network/Response.h"

#include "utils/Log.h"

#include <algorithm>

namespace calmframe {
namespace network {

SecurityController::SecurityController()
    :validator(NULL)
{}
SecurityController::~SecurityController()
{}

bool SecurityController::onRequest(Request &request, Response &response){
//    MainLog().info("SecurityController::onRequest");
    
    WebController * controller = matchController(request);
    if(controller != NULL){
        if(validateRequest(request)){
            return controller->onRequest(request,response);
        }
        else{
            sendUnauthorizedResponse(response);
            return true;
        }
    }
    return false;
}


bool SecurityController::validateRequest(Request &request) const{
    if(this->validator != NULL){
        return validator->validateRequest(request);
    }
    return true;
}

void SecurityController::sendUnauthorizedResponse(Response &response){
    response.setStatusLine(StatusResponses::UNAUTHORIZED);
    response.putHeader(Header(Headers::ContentLength, "0"));
    response.sendHeaders();
    response.send("");
}

RequestValidator *SecurityController::getValidator() const{
    return validator;
}

void SecurityController::setValidator(RequestValidator *value){
    validator = value;
}

}//namespace
}//namespace

