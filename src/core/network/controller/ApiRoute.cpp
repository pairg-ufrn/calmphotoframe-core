#include "ApiRoute.h"

namespace calmframe {

ApiRoute::ApiRoute(const std::string & route, const calmframe::HttpMethods::Method & method)
: method(method)
{
    this->url.path().set(route);
}

HttpMethods::Method ApiRoute::getMethod() const{
    return method;
}

void ApiRoute::setMethod(const HttpMethods::Method & value){
    method = value;
}

const network::Url::PathsList &ApiRoute::getRoutePaths() const{
    return url.path().list();
}

void ApiRoute::setRoutePaths(const network::Url::PathsList & paths){
    this->url.path().set(paths);
}

bool ApiRoute::operator==(const ApiRoute & other) const{
    return method == other.method
    && url.path().list() == other.url.path().list();
}

bool ApiRoute::operator<(const ApiRoute & other) const{
    return (method < other.getMethod())
        || (method == other.getMethod() && getRoutePaths() < other.getRoutePaths());
}

}//namespace

