#ifndef CONTROLLERROUTER_H
#define CONTROLLERROUTER_H

#include "WebController.h"
#include "RequestRouter.h"

#include <string>
#include <vector>

#include "network/Url.h"

namespace calmframe{
namespace network {

class ControllerRouter : public WebController, public RequestRouter{
public:
    class RouteItem{
    public:
        Url route;
        WebController * controller;

        RouteItem()
            : route() , controller(NULL)
        {}
        RouteItem(const std::string & route, WebController * controller)
            : route(route) , controller(controller)
        {}
    };

    typedef WebController super;
    typedef std::vector<RouteItem> ControllersCollection;
    typedef ControllersCollection::const_iterator ConstControllerIter;
    typedef ControllersCollection::iterator ControllerIter;
private:
    ControllersCollection controllers;
public:
    void add(const std::string & baseUrl, WebController * controller);
    size_t countControllers() const;
    virtual bool onRequest(Request &request, Response &response);
    bool forward(Request & request, Response & response, const Url & url);

    bool matchUrl(const std::string &url) const;
protected:
    bool controllerMatches(WebController* controller
                                , const Url & controllerUrl
                                , const Url & requestUrl) const;
    network::Url makeRelativeUrl(const Url & baseUrl, Url url);
    ConstControllerIter findController(const Url & requestUrl, Request & request, ConstControllerIter & begin) const;
    ConstControllerIter findController(const Url & requestUrl, Request * request, ConstControllerIter & begin) const;
    bool forwardRequest(WebController * controller, Request & request, Response & response, const Url & url);
};

}
}



#endif // CONTROLLERROUTER_H
