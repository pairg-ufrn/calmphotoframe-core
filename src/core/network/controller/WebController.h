/*
 * WebController.h
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#ifndef NETWORK_WEBCONTROLLER_H_
#define NETWORK_WEBCONTROLLER_H_

#include <string>

#include "network/NetworkFwd.h"

namespace calmframe {
namespace network {

class WebController{
public:
    /** Pattern to match a path in a Url with any path*/
    static const char * WILDCARD_PATH;
    /** Pattern to match a sequence of any paths*/
    static const char * WILDCARD_PATHSEQ;

private:
	std::string baseUrl;
public:
    WebController(const std::string & baseUrl="")
		:baseUrl(baseUrl)
	{}
	virtual ~WebController() = default;

public: /* * Getters and Setters**/
    /** Controller should not be responsible by checking/matching urls.**/

    /** @deprecated */
    const std::string& getBaseUrl() const { return baseUrl; }
    /** @deprecated */
    void setBaseUrl(const std::string& baseUrl) { this->baseUrl = baseUrl; }

    /** @deprecated
     * Check if the given url match with this controller baseUrl
     * @param url - the url to match
    */
    virtual bool matchUrl(const std::string & url) const;
    /** @deprecated*/
    virtual bool matchPaths(const std::string & controllerPath, const std::string & urlPath) const;

protected:
    std::string normalizeUrlPath(std::string urlPath) const;
    bool matchUrlImpl(const std::string & controllerUrl, const std::string & url, int *outMatchCount=NULL) const;

public: /* * WebController interface**/
    /** Callback called when an request is made to this controller.
     *  This callback calls the appropriated "on<Request Method>", so
     *  is recommended to call the base implementation if it is overwrited.
     *
     *  @return true if the request was handled or false, otherwise.*/
    virtual bool onRequest(Request & request, Response & response);

    /** Callback called when a GET request is made to this controller.
     *  @see #onRequest(Request, Response)*/
    virtual bool onGet   (Request & request, Response & response);

    /** Callback called when a POST request is made to this controller.
     *  @see #onRequest(Request, Response)*/
    virtual bool onPost  (Request & request, Response & response);

    /** Callback called when a PUT request is made to this controller.
     *  @see #onRequest(Request, Response)*/
    virtual bool onPut   (Request & request, Response & response);

    /** Callback called when a DELETE request is made to this controller.
     *  @see #onRequest(Request, Response)*/
    virtual bool onDelete(Request & request, Response & response);

    /** Callback called when a OPTION request is made to this controller.
     *  @see #onRequest(Request, Response)*/
    virtual bool onOptions(Request & request, Response & response);
};

} /* namespace network */
} /* namespace calmframe */

#endif /* NETWORK_WEBCONTROLLER_H_ */
