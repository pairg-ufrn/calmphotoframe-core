#ifndef APIROUTE_H
#define APIROUTE_H

#include <string>
#include "network/HttpMethods.h"
#include "network/Url.h"

namespace calmframe {
class ApiRoute
{
private:
    HttpMethods::Method method;
    network::Url url;
public:
    ApiRoute(const std::string & route = std::string(), const HttpMethods::Method & method = HttpMethods::UNKOWN);

    HttpMethods::Method getMethod() const;
    void setMethod(const HttpMethods::Method & value);

    const network::Url::PathsList & getRoutePaths() const;
    void setRoutePaths(const network::Url::PathsList & paths);

    bool operator==(const ApiRoute & other)const;
    bool operator<(const ApiRoute & other) const;
};

}//namespace

#endif // APIROUTE_H
