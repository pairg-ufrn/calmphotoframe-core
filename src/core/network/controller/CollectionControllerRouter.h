#ifndef COLLECTIONCONTROLLERROUTER_H
#define COLLECTIONCONTROLLERROUTER_H

#include "network/controller/WebController.h"

#include <vector>

namespace calmframe {
namespace network {

class CollectionControllerRouter : public WebController
{
private:
    typedef WebController super;

    typedef std::vector<WebController *> ControllerCollection;
    ControllerCollection controllers;
public:
    /** @Override*/
    virtual bool onRequest(Request & request, Response & response);
protected:
    virtual WebController * matchController(const Request & request, unsigned startIndex=0) const;
    virtual WebController * matchController(const std::string & url, unsigned startIndex=0) const;
    bool routeRequest(Request &request, Response &response);
public:
    virtual bool matchUrl(const std::string & url) const;
public:
    void addController(WebController * controller);
    void removeController(WebController * controller);
    bool containsController(WebController * controller);
private:
    ControllerCollection::iterator findController(WebController * controller);
};

}//namespace
}//namespace

#endif // COLLECTIONCONTROLLERROUTER_H
