#include "CollectionControllerRouter.h"

#include "network/Request.h"
#include "network/Response.h"

#include "utils/Log.h"

#include <algorithm>

using std::string;

namespace calmframe {
namespace network {

bool CollectionControllerRouter::onRequest(Request &request, Response &response){
    MainLog().info(request.getMethod() + " " + request.getUrl());
    if(routeRequest(request, response)){
        return true;
    }
    return super::onRequest(request, response);
}

bool CollectionControllerRouter::routeRequest(Request &request, Response &response){
    string url = request.getUrl();

    ControllerCollection::const_iterator iter = controllers.begin();
    for(; iter != controllers.end(); ++iter){
        WebController * controller = *iter;

        if(controller->matchUrl(url)){
            if(controller->onRequest(request, response)){
                return true;
            }
        }
    }

    return false;
}

WebController *CollectionControllerRouter::matchController(const Request &request, unsigned startIndex) const{
    return matchController(request.getUrl(), startIndex);
}
WebController *CollectionControllerRouter::matchController(const string &url, unsigned startIndex) const{
    if(startIndex >= controllers.size()){
        return NULL;
    }

    ControllerCollection::const_iterator iter = controllers.begin() + startIndex;
    for(; iter != controllers.end(); ++iter){
        WebController * controller = *iter;

        if(controller->matchUrl(url)){
            return controller;
        }
    }
    return NULL;
}

bool CollectionControllerRouter::matchUrl(const string &url) const{
    return matchController(url) != NULL;
}

void CollectionControllerRouter::addController(WebController *controller){
    if(!containsController(controller)){
        this->controllers.push_back(controller);
    }
}

void CollectionControllerRouter::removeController(WebController *controller){
    ControllerCollection::iterator found = findController(controller);
    if(found != controllers.end()){
        controllers.erase(found);
    }
}

bool CollectionControllerRouter::containsController(WebController *controller){
    return findController(controller) != controllers.end();
}

CollectionControllerRouter::ControllerCollection::iterator
CollectionControllerRouter::findController(WebController *controller){
    return std::find(controllers.begin(), controllers.end(),controller);
}


}//namespace
}//namespace

