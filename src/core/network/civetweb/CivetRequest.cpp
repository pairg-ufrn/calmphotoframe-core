#include "CivetRequest.h"

#include "civetweb.h"
#include "CivetServer.h"
#include "CivetWebReader.h"
#include "network/readers/BufferedStreamReader.h"

#include "utils/StringUtils.h"
#include "utils/CommonExceptions.h"

#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>

#include "utils/Log.h"

using namespace std;
using namespace calmframe::utils;

namespace calmframe {
namespace network {

CivetRequest::CivetRequest(struct mg_connection *conn)
    : connection(conn)
    , connectionReader(std::make_shared<CivetWebReader>(conn))
    , bufferedConnection(std::make_shared<BufferedStreamReader>(connectionReader))
    , stream(bufferedConnection.get())
{
    extractRequestData(conn);
}

//Copy constructor
CivetRequest::CivetRequest(const CivetRequest& request)
    : super(request)
    , connection(request.connection)
    , connectionReader(request.connectionReader)
	, bufferedConnection(request.bufferedConnection)
    , stream(bufferedConnection.get())
{
}

void CivetRequest::extractRequestData(struct mg_connection * connection)
{
    if(connection != NULL){
        const char * contentTypeHeader = CivetServer::getHeader(connection,"Content-Type");
        this->contentType = ContentType(contentTypeHeader != NULL ? contentTypeHeader : "");

        const struct mg_request_info * req_info = mg_get_request_info(connection);
        
        Url urlTmp{req_info->request_uri};
        if(req_info->query_string != NULL){
            urlTmp.query().set(req_info->query_string);
        }
        
        this->url(urlTmp);
        
        super::method = req_info->request_method;
    }
}

mg_connection *CivetRequest::getConnection() const{
    return connection;
}

std::string CivetRequest::getHeaderValue(const std::string& headerName, const std::string & defaultValue) const {
    if(connection == NULL){
        return string();
    }
    const char * value = CivetServer::getHeader(this->connection,headerName);
    return value != NULL ? string(value) : defaultValue;
}

istream &CivetRequest::getContent(){
    return this->stream;
}
const istream &CivetRequest::getContent() const{
    return this->stream;
}
bool CivetRequest::hasContent()
{
    return getContent().peek() != EOF;
}

int CivetRequest::ignoreData(int maxBytes) {
    return this->bufferedConnection->read((ostream *)NULL, maxBytes);
}
int CivetRequest::readData(std::string * str, int maxBytes) {
	return this->bufferedConnection->read(str,maxBytes);
}
int CivetRequest::readData(std::ostream * output, int maxBytes){
	return this->bufferedConnection->read(output, maxBytes);
}
bool CivetRequest::readUntilFind(const std::string& terminator, std::string * output, int maxBytes) {
	return this->bufferedConnection->readUntilFind(terminator,output,maxBytes);
}

bool CivetRequest::hasData() {
	return this->bufferedConnection->hasDataAvailable();
}

} /* namespace network */
} /* namespace calmframe */
