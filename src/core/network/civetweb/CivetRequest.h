#ifndef CIVETREQUEST_H
#define CIVETREQUEST_H

#include "network/Request.h"

#include <memory>

struct mg_connection;

namespace calmframe {
namespace network {

class BufferedStreamReader;
class CivetWebReader;

class CivetRequest : public Request{
public:
	using super = Request;
	
public:
    CivetRequest(struct mg_connection *conn);
	CivetRequest(const CivetRequest & request);
    virtual ~CivetRequest(){}

protected:
    mg_connection * getConnection() const;

public: 
    std::string getHeaderValue(
        const std::string & headerName, 
        const std::string & defaultValue=std::string()) const override;

public: //Read data
    bool hasContent() override;
    std::istream & getContent() override;
    const std::istream & getContent() const override;
    
	bool hasData() override;
    int ignoreData(int maxBytes = -1) override;
	int readData(std::string * output, int maxBytes = -1) override;
	int readData(std::ostream * output, int maxBytes = -1) override;
	
	/** Read until find the string 'str' or end the data to read.
	 *  If str is empty returns true immediately.
	 *  @param str the string to be searched.
	 *  @param output a pointer to a string where all content readed will be set.
	 *  @return if the string was found.*/
	bool readUntilFind(const std::string & str, std::string * output=NULL, int maxBytes=-1) override;
	
protected:	
    void extractRequestData(mg_connection * connection);
    
private:
	struct mg_connection * connection;
    std::shared_ptr<CivetWebReader> connectionReader;
    std::shared_ptr<BufferedStreamReader> bufferedConnection;
    std::istream stream;
};

}//namespace
}//namespace

#endif // CIVETREQUEST_H
