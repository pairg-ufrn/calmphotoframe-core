#include "CivetWebControllerAdapter.h"

#include "network/controller/WebController.h"

#include "CivetRequest.h"
#include "network/Response.h"

namespace calmframe {

CivetWebControllerAdapter::CivetWebControllerAdapter(network::WebController * controller)
    : webController(controller)
{}

bool CivetWebControllerAdapter::handleRequest(CivetServer * server, mg_connection * conn){
    network::CivetRequest request(conn);
    network::Response response(server, conn);
    
    return webController->onRequest(request, response);  
}

}//namespace
