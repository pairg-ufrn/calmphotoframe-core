#ifndef CIVETWEBCONTROLLERADAPTER_H
#define CIVETWEBCONTROLLERADAPTER_H

#include "CivetServer.h"

namespace calmframe {
namespace network {
    class WebController;
}//namespace

class CivetWebControllerAdapter : public CivetHandler{
    network::WebController * webController;

public: 
    CivetWebControllerAdapter(network::WebController * controller);

    bool handleRequest(CivetServer *server, struct mg_connection *conn);

    bool handleGet      (CivetServer *server, mg_connection *conn)override {return handleRequest(server, conn);}
    bool handleHead     (CivetServer *server, mg_connection *conn)override {return handleRequest(server, conn);}
    bool handleOptions  (CivetServer *server, mg_connection *conn)override {return handleRequest(server, conn);}
    bool handleDelete   (CivetServer *server, mg_connection *conn)override {return handleRequest(server, conn);}
    bool handlePatch    (CivetServer *server, mg_connection *conn)override {return handleRequest(server, conn);}
    bool handlePost     (CivetServer *server, mg_connection *conn)override {return handleRequest(server, conn);}
    bool handlePut      (CivetServer *server, mg_connection *conn)override {return handleRequest(server, conn);}
};

}//namespace

#endif // CIVETWEBCONTROLLERADAPTER_H
