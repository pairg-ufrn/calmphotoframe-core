#ifndef CIVETWEBREADER_H
#define CIVETWEBREADER_H

#include <streambuf>
#include "civetweb.h"

#include "network/readers/StreamReader.hpp"

namespace calmframe{
namespace network {

class CivetWebReader : public StreamReader{
    private:
        mg_connection * connection;
    public:
        CivetWebReader(mg_connection * connection)
            : connection(connection)
        {}

        std::streamsize read(char * buffer, const std::streamsize & readLimit){
            int readedBytes = mg_read(connection, (void *)buffer, (size_t)readLimit);

            return readedBytes <= 0 ? std::streambuf::traits_type::eof() : readedBytes;
        }
};

}
}//namespace

#endif // CIVETWEBREADER_H
