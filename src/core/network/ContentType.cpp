/*
 * ContentType.cpp
 *
 *  Created on: 19/09/2014
 *      Author: noface
 */

#include "ContentType.h"

#include "utils/StringUtils.h"

using namespace calmframe::utils;
using namespace std;

#include "utils/Log.h"
#include "ContentTypes.hpp"

#include "utils/Macros.h"

namespace calmframe {
namespace network {

#define TYPE_SEPARATOR '/'
#define PARAMETER_SEPARATOR ';'

ContentType::ContentType()
    : super(Headers::ContentType, std::string())
    , type(Types::TEXT)
    , subtype(SubTypes::PLAIN)
{
    updateValue();
}
ContentType::ContentType(const std::string& contentType)
    : super(Headers::ContentType, std::string())
    , type(Types::TEXT)
    , subtype(SubTypes::PLAIN)
{
    if(!contentType.empty()){
        setContentType(contentType);
    }
}
ContentType::ContentType(const std::string& type,const std::string& subtype, const std::string & parametersString)
    : super(Headers::ContentType, std::string())
    , type(type)
	, subtype(subtype)
{
    setParameters(parametersString);
    updateValue();
}

void ContentType::setContentType(const string &contentType){
    StringUtils::instance().split(contentType, TYPE_SEPARATOR
                                  , this->type, this->subtype, StringUtils::SPLIT_TRIM);
    //Obtem parametros
    extractParameter();
    updateValue();
}

std::string ContentType::getParameter(const std::string& param) const {
    map<string,string>::const_iterator paramPair = this->parameters.find(param);
    if(paramPair != parameters.end()){
        return string(paramPair->second);
    }
    return "";
}

bool ContentType::is(const ContentType & other) const{
    return is(other.getType(), other.getSubtype());
}

bool ContentType::is(string type, string subtype) const{
    return this->type == type && this->subtype == subtype;
}

void ContentType::extractParameter() {
    //Obtem parametros
    string parameterString;
    string subtypeTmp;
    StringUtils::instance().split(subtype, PARAMETER_SEPARATOR, subtypeTmp,
			parameterString, StringUtils::SPLIT_TRIM);
	subtype = subtypeTmp;
	if (!parameterString.empty()) {
		setParameters(parameterString);
	}
}

ContentType &ContentType::operator=(const Header &otherHeader){
    this->setValue(otherHeader.getValue());
    return *this;
}
void ContentType::setParameters(const std::string & stringParameters){
    /*TODO: lidar com parâmetros entre aspas.
	 * Ex.:Content-Type: multipart/mixed; boundary="gc0p4Jq0M:2Yt08jU534c0p"
	 * 		o boundary será gc0p4Jq0M:2Yt08jU534c0p (sem as aspas), mas estas são necessárias,
	 * 		caso contrário, pois o ':' é um caractere especial.
	 * */
	const StringUtils & utils = StringUtils::instance();
	string otherParameters(stringParameters);
	do{

		string parameter;
		utils.split(otherParameters, PARAMETER_SEPARATOR, parameter, otherParameters
				, StringUtils::SPLIT_TRIM);
		if(!parameter.empty()){
			string header, value;
			utils.split(parameter, '=', header, value, StringUtils::SPLIT_TRIM);
			this->parameters[header] = value;
		}
	}while(!otherParameters.empty());
}
ContentType::operator std::string() const{
	return this->toString();
}

void ContentType::updateValue(){
	if(type.empty() && subtype.empty()){
        super::setValue(string());
        return;
	}
	string contentType;
	contentType.append(type);
	contentType.push_back(TYPE_SEPARATOR);
	contentType.append(subtype);

	map<string,string>::const_iterator it = this->parameters.begin();
	for(; it != parameters.end(); ++it){
        contentType.append("; ").append(it->first).append("=").append(it->second);
	}

    super::setValue(contentType);
}

void calmframe::network::ContentType::setName(const std::string & UNUSED_PARAM(name)){
    /*ignores. ContentType name should not change.*/
}

} /* namespace network */
} /* namespace calmframe */

