#ifndef COOKIE_H
#define COOKIE_H

#include "ParameterizedHeader.h"
#include <string>
#include <map>

namespace calmframe {
namespace network{

namespace CookieParameters{
    static const char * Path     = "Path";
    static const char * Expires  = "Expires";
    static const char * Secure   = "Secure";
    static const char * HttpOnly = "HttpOnly";
    static const char * Domain   = "Domain";
}
class Cookie : public ParameterizedHeader
{
public:
    typedef ParameterizedHeader super;
public:
    Cookie();
    Cookie(const std::string & cookieName, const std::string & value);
    Cookie(const Header & header);
    ~Cookie();
private:
    std::string cookieName, value;
    std::string headerValue;
public:
    const std::string getCookieName() const;
    const std::string getCookieValue() const;
    const std::string getCookiePath() const;

    /** @Override*/
    virtual const std::string & getValue() const;
    /** @Override*/
    virtual void setValue(const std::string& value);

    void setCookieName (const std::string & cookieName );
    void setCookieValue(const std::string & value);
    void setCookiePath (const std::string & cookiePath );

protected:
    /** @Override*/
    virtual void decodeHeaderValue(const std::string & value);
    /** @Override*/
    virtual void updateValue();
};

}//namespace
}//namespace

#endif // COOKIE_H
