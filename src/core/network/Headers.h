/*
 * Headers.hpp
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#ifndef NETWORK_HEADERS_H_
#define NETWORK_HEADERS_H_

namespace calmframe {
namespace network {

namespace Headers {
    const char * const AccessControlAllowOrigin        = "Access-Control-Allow-Origin";
    const char * const AccessControlAllowCredentials   = "Access-Control-Allow-Credentials";
    const char * const AccessControlAllowMethods       = "Access-Control-Allow-Methods";
    const char * const AccessControlAllowHeaders       = "Access-Control-Allow-Headers";
    const char * const AccessControlRequestMethod      = "Access-Control-Request-Method";
    /** Used in preflight CORS requests. A list of header names, separated by ','.
        Asks permission to server to use those headers on actual request.    */
    const char * const AccessControlRequestHeaders     = "Access-Control-Request-Headers";

    const char * const ContentType        = "Content-Type";
    const char * const ContentLength      = "Content-Length";
    const char * const ContentDisposition = "Content-Disposition";
    const char * const Cookie             = "Cookie";
    const char * const Date               = "Date";
    const char * const Origin             = "Origin";
    const char * const SetCookie          = "Set-Cookie";

    /** Application defined header, used to send request token.*/
    const char * const XToken              = "X-Token";
}

} /* namespace network */
} /* namespace calmframe */

#endif /* NETWORK_HEADERS_H_ */
