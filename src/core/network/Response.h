/*
 * Response.h
 *
 *  Created on: 18/09/2014
 *      Author: noface
 */

#ifndef RESPONSE_H_
#define RESPONSE_H_

#include <network/StatusResponse.h>
#include "civetweb.h"
#include "CivetServer.h"
#include <string>
#include "network/Header.h"
#include "network/ContentType.h"
#include "serialization/Serializer.h"
#include <sstream>

namespace calmframe {
namespace network {

class Response {
private: //types
    typedef std::map<std::string, Header> HeadersMap;
private: //Civetweb
	CivetServer *server;
	struct mg_connection * connection;
protected:
    StatusResponse statusResponse;
	HeadersMap headers;

    ContentType contentType;

    bool sentHeaders;
private:
    std::stringstream localBuffer;
private: //Impedir cópia (por hora)
    Response();
    Response & operator=(const Response & response);
public:
    Response(CivetServer *server, struct mg_connection *conn);
    virtual ~Response();

    const ContentType & getContentType() const;
    ContentType & getContentType();
    void setContentType(const ContentType &value);

    /** If an header with name 'name' was set by {@link #putHeader putHeader}, returns an reference to it.
        Otherwise, returns an empty calmframe::network::Header, with no name and value.
    */
    const Header & getHeader(const std::string & name) const;
    Header & getHeader(const std::string & name);

    bool hasSentHeaders() const;
    void putHeader(const std::string & name, const std::string & value);
    void putHeader(const Header & header);
    void clearHeaders();
    void removeHeader(const Header & header);
    void removeHeader(const std::string & headerName);

    StatusResponse & statusLine();
    const StatusResponse & statusLine() const;
    void setStatusLine(const StatusResponse & statusLine);

    virtual void sendHeaders();
    virtual void send(const std::string & data);
    virtual void sendBinary(const void * data, size_t len);
    virtual void sendFile(const std::string & filepath);
    virtual void sendFile(const char * filepath);

    friend Response & operator<<(Response & response, const std::string & str);
    friend Response & operator<<(Response & response, const Serializer & serializer);
protected:
    virtual void write(const std::string & data);
    virtual void write(const char * data);
    virtual void sendStatusLine();
    virtual void finishHeaders();

    virtual void sendStatus(const StatusResponse & statusResponse);
    virtual void sendHeader(const Header & header);
private:
    void clearLocalBuffer();
};

} /* namespace network */
} /* namespace calmframe */

#endif /* RESPONSE_H_ */
