#include "UrlQuery.h"

#include <sstream>

#include "utils/StringUtils.h"

namespace calmframe {
namespace network {

using utils::StringUtils;
using std::vector;
using std::string;

namespace Constants {
    static const char PARAM_SEP = '&';
    static const char PAIR_SEP = '=';
}//namespace

UrlQuery::UrlQuery(std::initializer_list<UrlQuery::ParamMap::value_type> values)
    : _params(values)
{}

UrlQuery::UrlQuery(const UrlQuery::ParamMap & params)
    : _params(params)
{}

bool UrlQuery::empty() const{
    return _params.empty();
}

bool UrlQuery::contains(const Key & key) const{
    return _params.find(key) != _params.end();
}

const UrlQuery::Value &UrlQuery::get(const UrlQuery::Key & key, const UrlQuery::Value & defaultvalue) const{
    auto iter = _params.find(key);
    if(iter == _params.end()){
        return defaultvalue;
    }
    
    return iter->second;
}

UrlQuery &UrlQuery::add(const Key & key, const Key & value){
    _params.insert({key, value});
    return *this;
}

UrlQuery &UrlQuery::put(const UrlQuery::Key & key, const UrlQuery::Key & value){
    return add(key, value);
}

UrlQuery &UrlQuery::remove(const UrlQuery::Key & key){
    _params.erase(key);
    return *this;
}

UrlQuery &UrlQuery::clear(){
    _params.clear();
    return *this;
}

UrlQuery &UrlQuery::set(const std::string & queryStr){
    this->clear();
    
    vector<string> pairsList = StringUtils::instance().splitAndTrim(queryStr, Constants::PARAM_SEP);
    for(const auto & pairStr : pairsList){
        std::string paramTmp, valueTmp;
        StringUtils::instance().split(pairStr, Constants::PAIR_SEP, paramTmp, valueTmp);

        add(paramTmp, valueTmp);
    }
    
    return *this;
}

const UrlQuery::ParamMap &UrlQuery::params() const{
    return _params;
}

std::string UrlQuery::toString() const{ 
    std::stringstream out;
    out << *this;
    return out.str();
}

}//namespace
}//namespace

std::ostream & operator<<(std::ostream & out, const calmframe::network::UrlQuery & query){
    bool first = true;
    for(auto p : query.params()){
        if(!first){
            out << calmframe::network::Constants::PARAM_SEP;
        }
        out << p.first << calmframe::network::Constants::PAIR_SEP << p.second;
        
        first = false; 
    }
    
    return out;
}
