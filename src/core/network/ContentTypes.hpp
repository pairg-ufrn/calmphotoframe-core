#ifndef CONTENTTYPES_HPP
#define CONTENTTYPES_HPP

#include "ContentType.h"

namespace calmframe{
namespace network{

    namespace Types{
        static const char * APPLICATION = "application";
        static const char * IMAGE       = "image";
        static const char * TEXT        = "text";
        static const char * MULTIPART   = "multipart";
    }
    namespace SubTypes{
        /* ******************************** Application Subtypes *********************************/
        static const char * JSON = "json";

        /* ********************************** Image Subtypes ************************************/
        static const char * PNG = "png";
        static const char * JPEG = "jpeg";

        /* ********************************** Text Subtypes ************************************/
        static const char * PLAIN = "plain";

        /* ********************************** Multipart Subtypes *********************************/
        static const char * FORMDATA = "form-data";
    }
    namespace ContentTypes{
        static const ContentType JSON = ContentType(Types::APPLICATION,SubTypes::JSON,"encoding=UTF-8");
        static const ContentType IMAGE_JPEG = ContentType(Types::IMAGE,SubTypes::JPEG);
        static const ContentType TEXT_PLAIN = ContentType(Types::TEXT,SubTypes::PLAIN);
        static const ContentType MULTIPART_FORM = ContentType(Types::MULTIPART,SubTypes::FORMDATA);
    }

}//network
}//calmframe

#endif // CONTENTTYPES_HPP
