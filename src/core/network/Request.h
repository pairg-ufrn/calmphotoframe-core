/*
 * Request.h
 *
 *  Created on: 18/09/2014
 *      Author: noface
 */

#ifndef REQUEST_H_
#define REQUEST_H_

#include <string>
#include <istream>
#include <map>

#include "ContentType.h"
#include "Url.h"

namespace calmframe {
namespace network {

//Forward-declaration
class RequestHandler;

class Request { //TODO: separar implementação de interface
public:
    typedef std::map<std::string, std::string> ParamMap;
    typedef ParamMap::iterator ParamIter;
    typedef ParamMap::const_iterator ConstParamIter;

protected:
    //TODO: change to enum
    std::string method;
    
	ContentType contentType;

    ParamMap parameters;
    Url _url;
    mutable std::string urlCache;

public:
    Request()=default;
	Request(const Request & request);
	virtual ~Request(){}

public: //Getters and Setters
    virtual std::string getMethod() const;
    
    virtual const Url & url() const;
    virtual void url(const Url & url);
    
    /** @deprecated*/
    virtual const std::string & getUrl() const;
    /** @deprecated*/
    virtual void setUrl(const std::string & url);
    
    virtual const ContentType & getContentType() const;

    /** @deprecated - use url
     * 
     * Return if param exist on query
     *
     * @param param - the parameter name to search
     * @param outValue - if this pointer is not NULL and param is found, put value in this string
     * @return true if param was found*/
    virtual bool hasQueryParameter(const std::string & param) const;
    /**  @deprecated - use url*/
    virtual std::string getQueryParameter(const std::string & paramName
                                   , const std::string & defaultValue=std::string()) const;

    /**
    * Get a parameter put on the request
    */
    virtual const std::string & getParameter(const std::string & paramName
                                   , const std::string & defaultValue) const;

    virtual std::string getParameter(const std::string & paramName) const;
    virtual bool hasParam(const std::string & paramName) const;
    /**
    * Store parameter on request.
    * Can be used to store some value to be processed by other object
    */
    virtual void putParameter(const std::string & paramName, const std::string & value);

public:
	virtual bool containsHeader(const std::string & headerName) const;
    virtual std::string getHeaderValue(
            const std::string & headerName, 
            const std::string & defaultValue=std::string()) const = 0;

public: //Read data
    virtual bool hasContent()=0;
	virtual bool hasData()=0;
    virtual int ignoreData(int maxBytes = -1)=0;
    virtual std::istream & getContent()=0;
    virtual const std::istream & getContent() const;
	virtual int readData(std::string * output, int maxBytes = -1)=0;
	virtual int readData(std::ostream * output, int maxBytes = -1)=0;
	/** Read until find the string 'str' or end the data to read.
	 *  If str is empty returns true immediately.
	 *  @param str the string to be searched.
	 *  @param output a pointer to a string where all content readed will be set.
	 *  @return if the string was found.*/
	virtual bool readUntilFind(const std::string & str, std::string * output=NULL, int maxBytes=-1)=0;
	
public: //Save data
	virtual bool saveData(const std::string & path);
	virtual bool saveData(std::ostream * output);
};

} /* namespace network */
} /* namespace calmframe */

#endif /* REQUEST_H_ */
