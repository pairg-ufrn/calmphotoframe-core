/*
 * StatusResponse.cpp
 *
 *  Created on: 27/11/2014
 *      Author: noface
 */

#include <string>
#include "StatusResponse.h"

namespace calmframe {
namespace network{

StatusResponse::StatusResponse() : statusCode(0), message("")
{}

StatusResponse::StatusResponse(int code, const std::string &message)
: statusCode(code), message(message)
{}

StatusResponse::~StatusResponse(){}

void StatusResponse::set(const StatusResponse &otherStatus){
    this->set(otherStatus.getStatusCode(), otherStatus.getMessage());
}

void StatusResponse::set(int code, const std::string &message){
    this->setMessage(message);
    this->setStatusCode(code);
}

bool StatusResponse::operator==(const StatusResponse &other) const{
    return this->getStatusCode() == other.getStatusCode()
    && this->getMessage() == other.getMessage();
}

}

} /* namespace calmframe */
