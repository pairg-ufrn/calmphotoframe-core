#ifndef URL_H
#define URL_H

#include <string>
#include <vector>
#include <map>
#include <sstream>

#include "UrlPath.h"
#include "UrlQuery.h"

namespace calmframe {
namespace network {

class Url
{
public:
    //TODO: allow more than one instance of the same key on the parameters
    typedef std::map<std::string, std::string> ParamMap;
    typedef ParamMap::iterator ParamIter;
    typedef ParamMap::const_iterator ConstParamIter;

//    typedef std::vector<std::string> PathsList;
    using PathsList = UrlPath::PathList;

public:
    static PathsList splitUrl(const std::string & url
                            , std::string * outPath     = NULL
                            , std::string * outQuery    = NULL
                            , std::string * outFragment = NULL
                            , std::string * outHost     = NULL
                            , std::string * outPort     = NULL
                            , std::string * outScheme   = NULL);


    static void joinPaths(std::stringstream & sstream, const PathsList& paths);
    static std::string joinPaths(const PathsList& paths);
    static PathsList splitPaths(const std::string & pathsStr);

public:
    Url();
    Url(const char * url);
    Url(const std::string & url);
    virtual ~Url();

    virtual void set(const std::string & url);

    const UrlPath & path() const;
    UrlPath & path();
    Url & path(const UrlPath & path);
    
    const UrlQuery & query() const;
    UrlQuery & query();
    Url & query(const UrlQuery & query);
    
    virtual const std::string & getFragment() const;

    const std::string & getScheme() const;
    void setScheme(const std::string &value);
    
    const std::string &  getHost() const;
    void setHost(const std::string &value);

    int getPort() const;
    void setPort(int value);

    virtual std::string toString() const;
    
protected:
    UrlPath _path;
    UrlQuery _query;
    std::string fragment;
    std::string scheme;
    std::string host;
    int port;
};

}//namespace
}//namespace

#endif // URL_H
