#ifndef NETWORKFWD_H
#define NETWORKFWD_H

namespace calmframe {
namespace network {
    class Request;
    class Response;
    class StatusResponse;
}//namespace
}//namespace

#endif // NETWORKFWD_H

