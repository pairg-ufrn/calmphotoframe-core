/*
 * Header.cpp
 *
 *  Created on: 28/11/2014
 *      Author: noface
 */

#include <network/Header.h>
#include <sstream>

using namespace std;

#define CRLF "\r\n"
namespace calmframe {
namespace network {

Header &Header::operator=(const Header &other){
    this->setName(other.getName());
    this->setValue(other.getValue());

    return *this;
}

bool Header::empty() const{
    return getName().empty() && getValue().empty();
}

Header::operator std::string() const{
    return this->toString();
}
std::string Header::toString() const{
	stringstream stringStream;
	stringStream << *this;
    return stringStream.str();
}

void Header::toStream(std::ostream& out) const{
    out << getName() << ": "<< getValue();
}

std::ostream& operator<< (std::ostream &out, const Header & header){
//    out << header.getName() << ": "<<header.getValue();
    header.toStream(out);
	return out;
}

} /* namespace network */
} /* namespace calmframe */
