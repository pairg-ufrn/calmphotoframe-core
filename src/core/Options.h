#ifndef OPTIONS
#define OPTIONS

/** Global options definition.*/
namespace Options{
    constexpr const char * const SERVER_NAME = "serverName";
    
    constexpr const char * const BUFFER_SIZE = "buffer_size";
    constexpr const char * const PORT        = "port";
    constexpr const char * const BASE_DIR 	 = "base_dir";
    /** Local em que os arquivos de dados da aplicação devem ser armazenados
     * (ex.: banco de dados, arquivos de log, etc...)*/
    constexpr const char * const DATA_DIR 	 = "data_dir";
    /** Caminho para o arquivo de configuração do Core.
     * Não é esperado que esta opção seja utilizada no arquivo de configuração, mas
     * via linha de comando(se suportado) ou outro meio.
    */
    constexpr const char * const CONF_FILE 	 = "conf_file";
    constexpr const char * const IMAGE_DIR 	 = "image_dir";
    constexpr const char * const TMP_DIR 	 = "tmp_dir";
    constexpr const char * const LOG_DIR      = "log_dir";
    constexpr const char * const LOG_FILE     = "log_file";
    /** Especifica o nível de log a ser exibido.
        Valores válidos: Debug, Information, Warning, Error (ver Log.h)*/
    constexpr const char * const LOG_LEVEL    = "log_level";
    constexpr const char * const SESSION_TIME = "session_time";

    constexpr const char * const DATABASE_TIMEOUT = "database_timeout";

    /** (Booleano) Define se cookies serão utilizados apenas sobre HTTPS ou não (default é false).*/
    constexpr const char * const UNSAFE_COOKIE = "unsafe_cookie";
namespace Defaults{
    constexpr const char * const CONFIGURATION_FILENAME = "conf.json";

    //TODO: modificar valores para int
    constexpr const char * const BUFFER_SIZE = "10240"; //1MB
    constexpr const char * const PORT        = "6413";
    constexpr const char * const DATA_DIR    = ".";
    constexpr const char * const CONF_FILE 	= "conf.json";
    constexpr const char * const LOG_DIR 	= ".";
    constexpr const char * const LOG_FILE 	= "calmphotoframe.log";
    constexpr const char * const TMP_DIR     = "/tmp";
    constexpr const char * const SESSION_TIME  = "1800"; //30min (em segundos)

    static const int DATABASE_TIMEOUT = 4 * 60 * 1000;//4s (em milisegundos)

#ifdef ANDROID
    static const char* IMAGE_DIR = "/sdcard/Images";
#else
    static const char* IMAGE_DIR = "./photos";
#endif
} //Defaults
} //Options

#endif // OPTIONS

