#ifndef SESSIONCONTROLLER_H
#define SESSIONCONTROLLER_H

#include <string>

#include "network/controller/WebController.h"
#include "serialization/json/JsonSerializer.h"

namespace Constants{
    static const char * sessionCookieName = "token";
namespace ApiPaths {
    static const char * loginCommand  = "login";
    static const char * logoffCommand  = "logoff";
    static const char * checkSessionCommand  = "isconnected";
}
}

namespace calmframe {

class Session;
class UserLoginInfo;

using calmframe::network::Request;
using calmframe::network::Response;

class SessionController : public network::WebController
{
private:
    JsonSerializer serializer;
public:
    SessionController(const std::string & baseUrl="");
    virtual ~SessionController();

    virtual bool onGet(network::Request &request, network::Response &response);
    virtual bool onPost(network::Request &request, network::Response &response);
protected:
    virtual bool authenticate(const UserLoginInfo &loginInfo, Session &session);
    virtual bool doLogin(network::Request &request, network::Response &response);
    virtual bool doLogoff(network::Request &request, network::Response &response);
    virtual bool checkSession(network::Request &request, network::Response &response);

    bool isCommandRequest(network::Request &request, const std::string &commandName);
    void sendLoginResponse(Response &response, const Session &session);
    void sendUnauthorizedResponse(Response &response);
    std::string getRelativeUrl(Request &request);
    std::string getTokenValue(Request &request);
    void putSessionCookie(network::Response &response, const Session &session);
};

}
#endif // USERCONTROLLER_H
