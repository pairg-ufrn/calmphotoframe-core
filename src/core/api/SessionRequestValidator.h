#ifndef SESSIONREQUESTVALIDATOR_H
#define SESSIONREQUESTVALIDATOR_H

#include "network/RequestValidator.h"
#include "network/controller/ApiRoute.h"

#include <string>
#include <vector>

namespace calmframe {

using network::RequestValidator;
using network::Request;

class SessionManager;

class SessionRequestValidator : public RequestValidator{
public:
    typedef std::vector<ApiRoute> RoutesCollection;

private:
    SessionManager * sessionManager;
    RoutesCollection nonAuthenticatedRoutes;

public:
    SessionRequestValidator(SessionManager * sessionManager = NULL);
    ~SessionRequestValidator();
    virtual bool validateRequest(Request & request) const override;
    std::string getToken(const Request &request) const;

    SessionRequestValidator & putNonAuthenticatedRoute(const ApiRoute & route);
    bool matchNonAuthenticatedRoute(const Request & request) const;

    SessionManager & getSessionManager();
    const SessionManager & getSessionManager() const;
};

}//namespace

#endif // SESSIONREQUESTVALIDATOR_H
