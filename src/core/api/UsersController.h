#ifndef USERSCONTROLLER_H
#define USERSCONTROLLER_H

#include "api/CollectionWebController.h"

namespace calmframe {

class AccountManager;
class SessionManager;
class User;

namespace api {
    class RequestHelper;
}//namespace

class UsersController : public network::CollectionWebController
{
private:
    AccountManager * accountManager;

public:
    UsersController(AccountManager * manager=NULL);

    AccountManager * getAccountManagerPtr() const;
    const AccountManager & getAccountManager() const;
    AccountManager & getAccountManager();

    void setAccountManager(AccountManager * value);

public:
    virtual bool onRequest(network::Request &request, network::Response &response);
    virtual bool existMember(long id);
    virtual bool onGetMember(network::Request &request, network::Response &response, long memberId, network::Url &relativeUrl);
    virtual bool onListAll(network::Request &request, network::Response &response);
    virtual bool onCreateMember(network::Request &request, network::Response &response);
    virtual bool onUpdateMember(network::Url &relativeUrl, network::Request &request, network::Response &response, long memberId);
    virtual bool onDeleteMember(network::Request &request, network::Response &response, long memberId);

    bool onGetUser(network::Request & request, network::Response & response, long memberId);
    
    bool onGetProfileImage(network::Request & request, network::Response & response, long memberId);
    
protected:
    /** Verify with url contains alias and updates it if needed.*/
    void redirectUrlAlias(network::Request& request, long userId);
    
    bool onChangePassword(long memberId, api::RequestHelper & helper);
    bool updateUser(long memberId, api::RequestHelper & helper);
    bool onUpdateUserProfileImage(api::RequestHelper & reqHelper, long memberId);
    bool onActivateUser(api::RequestHelper & reqHelper, long memberId);
    
    bool ensureIsAdminRequest(api::RequestHelper & helper);
    bool ensureIsSameUser(api::RequestHelper & helper, long memberId);
    bool ensureIsSameUserOrIsAdmin(api::RequestHelper & helper, long memberId);
    bool ensureCanUpdateUser(api::RequestHelper & helper, const User & user);

    void sendInsufficientPrivilegesError(api::RequestHelper & helper);
    void sendInsufficientPrivilegesError(api::RequestHelper & helper, const std::string & errMsg);
    long getRequestUserId(api::RequestHelper & helper);
};

}//namespace


#endif // USERSCONTROLLER_H
