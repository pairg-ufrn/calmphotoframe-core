#include "EventsController.h"

#include "Api.h"
#include "RequestHelper.h"

#include "services/EventsManager.h"
#include "serialization/json/JsonSerializer.h"
#include "network/ContentTypes.hpp"

#include "utils/StringCast.h"

using namespace calmframe::api;
using namespace calmframe::network;

using std::string;

namespace calmframe {

bool EventsController::existMember(long id){
    return getManager().existEvent(id);
}

bool EventsController::onListAll(network::Request & request, network::Response & response){
    const string & limitStr = request.getQueryParameter(api::parameters::limit);

    auto events = getManager().listRecentEvents(utils::StrCast().tryToInt(limitStr, -1));

    JsonSerializer serializer;

    RequestHelper requestHelper(request, response);
    requestHelper.sendResponseCollection(api::fields::events, events, serializer, ContentTypes::JSON);

    return true;
}

EventsManager &EventsController::getManager(){
    return EventsManager::instance();
}

}//namespace
