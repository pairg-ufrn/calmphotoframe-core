#ifndef REQUESTHELPER_H
#define REQUESTHELPER_H

#include "network/NetworkFwd.h"
#include "network/Request.h"
#include "network/ContentType.h"
#include "serialization/Serializer.h"
#include "serialization/SerializersFwd.h"

#include "ResponseBuilder.h"

namespace calmframe {
namespace api {

using ::calmframe::network::Request;
using ::calmframe::network::Response;

class RequestHelper
{
public:
    RequestHelper(Request & request, Response & response);
    RequestHelper(Request * request, Response * response);
    virtual ~RequestHelper(){}
public:
    template<typename Entity, typename DeserializerType>
    Entity readRequest();

    template<typename T>
    T readRequest(Deserializer & deserializer);
    Deserializable & readRequest(Deserializer & deserializer
                                , Deserializable & outDeserializable);
    template<typename DeserializerType>
    Deserializable & readRequest(Deserializable & deserializable);

    bool receiveEntity(Deserializer & deserializer
                        , Deserializable & outDeserializable
                        , const network::ContentType & requiredType);

    template<typename DeserializerType>
    bool receiveEntity(Deserializable & outDeserializable, const network::ContentType & requiredType){
        DeserializerType deserializer;
        return this->receiveEntity(deserializer, outDeserializable, requiredType);
    }

    ResponseBuilder respond(Serializer * serializer, const network::ContentType& contentType) const;

    template<typename SerializerType>
    void sendResponse(const Serializable & serializable, const network::ContentType & contentType);

    void sendResponse(const Serializable & serializable, Serializer & serializer
                    , const network::ContentType & contentType);

    template<typename Collection>
    void sendResponseCollection(const std::string & collField, const Collection & coll
            , Serializer & serializer, const network::ContentType & contentType);

    template<typename SerializerT, typename Collection>
    void sendResponseCollection(const std::string & collField, const Collection & coll
            , const network::ContentType & contentType);

    long getParameter(const std::string & paramName, long defaultValue);

    bool queryParameter(const std::string & paramName, bool defaultValue);
public:
    bool validateRequestType(const network::ContentType & expectedType
                            , bool ignoreOnEmptyRequest=false);

    void sendInvalidJsonError();
    void sendBadRequestError(const std::string & errMsg);
    void sendError(const network::StatusResponse & status, const std::string & errMsg);
    void sendEmpty(const network::StatusResponse & status);

    network::Request & getRequest() const;
    void setRequest(network::Request * value);
    network::Response & getResponse() const;
    void setResponse(network::Response * value);

    void sendSerializerContent(Response& response, Serializer& serializer);
    
protected:
    Response & prepareResponse(const network::ContentType& contentType, Serializer& serializer);
    network::Response & sendStatus(const network::StatusResponse& status);
    
private:
    network::Request * request;
    network::Response * response;
};

template<typename Entity, typename DeserializerType>
Entity RequestHelper::readRequest()
{
    DeserializerType deserializer;
    return readRequest<Entity>(deserializer);
}

template<typename T>
T RequestHelper::readRequest(Deserializer & deserializer)
{
    T item;
    readRequest(deserializer, item);
    return item;
}

template<typename DeserializerType>
Deserializable & RequestHelper::readRequest(Deserializable & deserializable){
    DeserializerType deserializer;
    return readRequest(deserializer, deserializable);
}

template<typename SerializerType>
void RequestHelper::sendResponse(const Serializable & serializable, const network::ContentType & contentType)
{
    SerializerType serializer;
    sendResponse(serializable, serializer, contentType);
}


template<typename Collection>
void RequestHelper::sendResponseCollection(const std::string & collField
    , const Collection & coll, Serializer & serializer, const network::ContentType & contentType)
{
    Response & response = prepareResponse(contentType, serializer);
    serializer.putCollection(collField, coll.begin(), coll.end());

    sendSerializerContent(response, serializer);
}

template<typename SerializerT, typename Collection>
void RequestHelper::sendResponseCollection(const std::string & collField
    , const Collection & coll, const network::ContentType & contentType)
{
    SerializerT serializer;
    sendResponseCollection(collField, coll, serializer, contentType);
}

}//namespace
}//namespace

#endif // REQUESTHELPER_H
