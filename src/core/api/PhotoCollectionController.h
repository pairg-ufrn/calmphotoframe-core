#ifndef PHOTOCOLLECTIONCONTROLLER_H
#define PHOTOCOLLECTIONCONTROLLER_H

#include "api/CollectionWebController.h"
#include "serialization/json/JsonSerializer.h"

#include <set>

namespace calmframe {

class PhotoCollectionManager;
class CollectionsOperation;
class PhotoCollection;

namespace api {
    class RequestHelper;
}//namespace

using calmframe::network::Request;
using calmframe::network::Response;

namespace CollectionsControllerFields {
    static const char * GetRootSubpath = "root";
    static const char * BulkActionSubpath = "bulk";
    static const char * MoveItemsSubpath = "move-items";

//    static const char * CollectionsField = "collections";
    static const char * PhotoCollectionIdsField = "collectionIds";
}//namespace

class PhotoCollectionController : public network::CollectionWebController
{
    typedef network::CollectionWebController super;
public:
    static const char * ADD_PHOTOS_SUBPATH;
    static const char * REMOVE_PHOTOS_SUBPATH;
private:
    PhotoCollectionManager * collectionManager;

    //TODO: remover dependência direta com JsonSerializer
    JsonSerializer serializer;
public:
    PhotoCollectionController(PhotoCollectionManager * collectionManager=NULL);

    virtual bool onRequest(network::Request &request, network::Response &response);
    virtual bool onDelete(network::Request &request, network::Response &response);

    PhotoCollectionManager & getCollectionManager();
    const PhotoCollectionManager & getCollectionManager() const;
    void setCollectionManager(PhotoCollectionManager *value);

    void handleUpdateMember(Request &request, Response &response, PhotoCollection & collection);
    bool checkJsonContent(Request &request, Response &response);
    bool validateCollectionsOperation(CollectionsOperation& outCollOp, Response &response);
protected:
    void sendInvalidRequestTypeMessage(Request &request, Response &response);
    void sendPhotoCollection(const PhotoCollection & photoCollection, const api::RequestHelper & reqHelper);
protected:
    bool isAValidId(long id) const;
    virtual bool existMember(long id);
    virtual bool tryGetMemberId(const network::Request &request, long *idOut, network::Url *outUrl) const;

    long getIdParameter(const network::Url & relativeUrl, const Request& request) const;

    virtual bool onListAll(network::Request &request, network::Response &response);
    /** Callback to handle requests for an PhotoCollection member.
        This method is called only if existId(memberId) return true.*/
    virtual bool onGetMember(network::Request &request, network::Response &response, long memberId, network::Url & relativeUrl);
    virtual bool onGetSubpathRequest(network::Request &request, network::Response &response, network::Url & relativeUrl);


    virtual bool onCreateMember(Request &request, Response &response);
    virtual bool onUpdateMember(network::Url & relativeUrl,
                    network::Request & request, network::Response & response, long memberId);

    virtual bool onPostSubpathRequest(network::Url &relativeUrl,
                    network::Request &request, network::Response &response);

    virtual bool onBulkDeleteRequest(network::Request &request, Response &response);

    virtual bool onDeleteMember(network::Request &request, network::Response &response, long memberId);

    bool onPhotoCollectionOperationRequest(Request &request, Response &response);
    void onMoveCollectionItemsRequest(Request &request, Response &response, long memberId);
protected:
    std::vector<PhotoCollection> listCollections(long ctxId);

    long getCtxIdParam(const network::Request & request) const;
    bool expandItemsParam(Request &request) const;

    /** Returns the @code{contextId}, if valid, or the id of the context that contains @code{collectionId}.*/
    long getContextId(long contextId, long collectionId);
    long getParentCollectionId(JsonSerializer & serializer);
    bool contextIsValid(long contextId) const;

    bool validateCollectionsOpRequest(network::Request &request, Response &response
                                    , CollectionsOperation &outCollOp);
    bool validateCollectionsOpRequest(network::Request &request, Response &response
                                    , CollectionsOperation &outCollOp
                                    , int &forceOpType);
    void executeCollectionsOperation(CollectionsOperation collOp, int operationType);

    bool isJsonRequest(const network::Request &request);

    std::set<long> deserializePhotoIds(const std::string &memberName);
    void sendPhotoIds(const std::set<long> & photoIds, Response &response);
    void sendSerializerJsonContent(Response &response);
    std::string getInvalidTypeMessage(const network::ContentType & receivedType);
};

}//namespace
#endif // PHOTOCOLLECTIONCONTROLLER_H
