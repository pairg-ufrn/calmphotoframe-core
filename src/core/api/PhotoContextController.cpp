#include "PhotoContextController.h"

#include "PhotoCollectionController.h"

#include "Api.h"
#include "RequestHelper.h"
#include "SessionRequestHelper.h"

#include "model/photo/PhotoContext.h"
#include "services/PhotoContextManager.h"
#include "exceptions/NotFoundMember.h"
#include "exceptions/InsufficientPermissionException.h"

#include "network/ContentTypes.hpp"
#include "network/StatusResponses.h"
#include "network/Url.h"

#include "utils/CommonExceptions.h"
#include "utils/StringCast.h"
#include "utils/Log.h"

#include <vector>

namespace calmframe {

using namespace std;
using namespace network;
using namespace api;
using namespace utils;

class PhotoContextWithCollaborators: public Deserializable{
public:
    PhotoContext context;
    PhotoContextManager::CollaboratorsList collaborators;

    PhotoContextWithCollaborators()
        : context()
        , collaborators()
        , hasCollabs(false)
    {}

    void deserialize(const GenericContainer & deserializer);

    bool hasCollaborators() const{ return hasCollabs;}

private:
    bool hasCollabs;
};

PhotoContextController::PhotoContextController(PhotoContextManager * ctxManager)
: contextManager(ctxManager)
{
    setRootPath(api::paths::contexts);
}

PhotoContextManager * PhotoContextController::getContextManager() const{
    CHECK_NOT_NULL(contextManager);
    return contextManager;
}

void PhotoContextController::setContextManager(PhotoContextManager * value){
    contextManager = value;
}

bool PhotoContextController::onRequest(Request & request, Response & response){
    Url relativeUrl = getRelativeUrl(request);

    putContextIdParameter(relativeUrl, request);
    return handleRequest(relativeUrl, request, response);
}

void PhotoContextController::putContextIdParameter(const Url & relativeUrl, Request& request){
    string firstPath = relativeUrl.path().get(0);
    string ctxIdParam = request.getParameter(api::parameters::PhotoContextId);
    if(ctxIdParam == api::paths::defaultContext
        || (ctxIdParam.empty() && firstPath == api::paths::defaultContext))
    {
        //TODO: avoid get context twice
        long userId = getUserId(request);
        long defaultCtxId = getContextManager()->getDefaultContext(userId).getId();
        request.putParameter(api::parameters::PhotoContextId, StrCast().toString(defaultCtxId));
    }
    else if(ctxIdParam.empty() && StrCast().isNumber(firstPath)){
        request.putParameter(api::parameters::PhotoContextId, firstPath);
    }
}

bool PhotoContextController::handleRequest(Url & relativeUrl, Request& request, Response& response){

    if(relativeUrl.path().count() > 1 && request.hasParam(api::parameters::PhotoContextId)){
        const std::string & subPath = relativeUrl.path().get(1);
        if(subPath == api::paths::collections || subPath == api::paths::photos){
            relativeUrl.path().remove(0);
            return forward(request, response, relativeUrl);
        }
    }
    return super::onRequest(request, response);
}

bool PhotoContextController::existMember(long id){
    return getContextManager()->existContext(id);
}

bool PhotoContextController::onListAll(calmframe::network::Request & request, Response &response){
    long userId = SessionRequestHelper::getSessionUser(request);
    auto contexts = getContextManager()->listWithPermission(userId, PermissionLevel::Read);

    RequestHelper helper(request, response);
    helper.sendResponseCollection<JsonSerializer>(api::fields::contexts, contexts, ContentTypes::JSON);

    return true;
}

bool PhotoContextController::onGetMember(Request &request, Response &response, long memberId, Url &relativeUrl)
{
    PhotoContext context = getContextManager()->getContext(memberId);
    return onGetContextRequest(request, response, relativeUrl, context);
}

bool PhotoContextController::onGetSubpathRequest(Request & request, Response & response, Url & relativeUrl){
    if(relativeUrl.path().get(0) == api::paths::defaultContext){
        long userId = getUserId(request);
        PhotoContext defaultContext = getContextManager()->getDefaultContext(userId);
        request.putParameter(api::parameters::PhotoContextId
                                , StrCast().toString(defaultContext.getId()));

        return onGetContextRequest(request, response, relativeUrl, defaultContext);
    }

    return false;
}

bool PhotoContextController::onGetContextRequest(Request & request, Response & response, Url & relativeUrl, PhotoContext & context){
    long userId = getUserId(request);
    getContextManager()->assertUserHasPermission(userId, context.getId(), PermissionLevel::Read);

    if(relativeUrl.path().count() == 1){
        sendGetContextResponse(request, response, context, userId);
        return true;
    }
    else if(relativeUrl.path().get(1) == api::paths::collaborators){//Get collaborators from member
        return onGetContextCollaborators(request, response, context.getId());
    }

    return false;
}

bool PhotoContextController::onCreateMember(Request & request, Response & response){
    RequestHelper controllerHelper(request, response);
    PhotoContext requestCtx;

    if(receiveContext(controllerHelper, requestCtx)){
        long userId = SessionRequestHelper::getSessionUser(request);
        PhotoContext createdCtx = getContextManager()->createContext(requestCtx, userId);
        controllerHelper.sendResponse<JsonSerializer>(createdCtx, ContentTypes::JSON);
    }

    return true;
}

bool PhotoContextController::onUpdateMember(Url &
    , Request & request, Response & response, long memberId)
{
    RequestHelper controllerHelper(request, response);

    PhotoContextWithCollaborators requestCtx;
    requestCtx.context = getContextManager()->getContext(memberId);

    if(receiveContext(controllerHelper, requestCtx)){
        long userId = getUserId(request);

        PhotoContext updatedCtx = requestCtx.hasCollaborators()
            ? getContextManager()->updateContext(memberId, requestCtx.context, requestCtx.collaborators, userId)
            : getContextManager()->updateContext(memberId, requestCtx.context, userId);
        controllerHelper.sendResponse<JsonSerializer>(updatedCtx, ContentTypes::JSON);
    }

    return true;
}

bool PhotoContextController::onDeleteMember(Request & request, Response & response, long memberId){
    getContextManager()->assertUserHasPermission(getUserId(request), memberId, PermissionLevel::Admin);

    const PhotoContext removedContext = getContextManager()->deleteContext(memberId);
    sendContextResponse(request, response, removedContext);
    return true;
}

bool PhotoContextController::onGetContextCollaborators(Request & request, Response & response, long memberId){
    auto collaborators = getContextManager()->listCollaborators(memberId);

    RequestHelper controllerHelper(request, response);
    controllerHelper.sendResponseCollection<JsonSerializer>(api::fields::collaborators, collaborators, ContentTypes::JSON);

    return true;
}

template<typename Entity>
static bool receiveEntity(api::RequestHelper & controllerHelper, Entity & outEntity){
    try{
        if(controllerHelper.validateRequestType(ContentTypes::JSON, true)){
            outEntity = controllerHelper.readRequest<Entity, JsonSerializer>();
            return true;
        }
    }catch(const CommonExceptions::ParseException & ex){
        controllerHelper.sendInvalidJsonError();
    }

    return false;
}

bool PhotoContextController::receiveContext(RequestHelper & controllerHelper, PhotoContext & outReceivedCtx)
{
    return receiveEntity(controllerHelper, outReceivedCtx);
}

bool PhotoContextController::receiveContext(RequestHelper & controllerHelper, PhotoContextWithCollaborators & out){
    return receiveEntity(controllerHelper, out);
}

void PhotoContextController::sendContextResponse(Request & request, Response & response, const PhotoContext & photoContext){
    RequestHelper requestHelper(request, response);
    requestHelper.sendResponse<JsonSerializer>(photoContext, ContentTypes::JSON);
}

void PhotoContextController::sendGetContextResponse(Request& request, Response& response, PhotoContext& context, long userId)
{
    RequestHelper requestHelper(request, response);

    if(!requestHelper.queryParameter(api::parameters::include_permission, false)){
        requestHelper.sendResponse<JsonSerializer>(context, ContentTypes::JSON);
    }
    else{
        Permission perm = getContextManager()->getUserPermission(context.getId(), userId);
        auto ctxWithPermission = ProtectedEntity<PhotoContext>(perm, context);
        requestHelper.sendResponse<JsonSerializer>(ctxWithPermission, ContentTypes::JSON);
    }
}

void PhotoContextWithCollaborators::deserialize(const GenericContainer & deserializer){
    context.deserialize(deserializer);

    if(deserializer.contains(api::fields::collaborators)){
        GenericIterator iter = deserializer.get(api::fields::collaborators).as<GenericIterator>();
        auto outIter = std::back_inserter(collaborators);
        iter.asCollection(outIter, Deserializer::converter<PhotoContextManager::Collaborator>);

        this->hasCollabs = true;
    }
}

}//namespace

