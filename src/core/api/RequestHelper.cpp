#include "RequestHelper.h"

#include "network/Response.h"
#include "network/StatusResponses.h"
#include "serialization/Deserializable.hpp"
#include "serialization/Deserializer.h"
#include "utils/CommonExceptions.h"
#include "utils/StringCast.h"

namespace calmframe {
namespace api {

namespace Errors {
    static const char * InvalidTypeMessage = "Invalid request type";
    static const char * InvalidJsonMessage = "Received invalid json message";
    static const char * ParseError = "The received content could not be parsed";
}//namespace

using namespace network;
using namespace utils;

using std::string;

Request &RequestHelper::getRequest() const{
    CHECK_NOT_NULL(request);
    return * request;
}

void RequestHelper::setRequest(network::Request * value)
{
    request = value;
}

Response &RequestHelper::getResponse() const
{
    CHECK_NOT_NULL(response);
    return *response;
}

void RequestHelper::setResponse(network::Response * value)
{
    response = value;
}
RequestHelper::RequestHelper(Request & request, Response & response)
: request(&request)
, response(&response)
{}

RequestHelper::RequestHelper(Request * request, Response * response)
    : request(request)
    , response(response)
{}

Deserializable &RequestHelper::readRequest(Deserializer & deserializer, Deserializable & outDeserializable)
{
    getRequest().getContent() >> deserializer >> outDeserializable;
    return outDeserializable;
}

bool RequestHelper::receiveEntity(Deserializer & deserializer, Deserializable & outDeserializable
    , const ContentType & requiredType)
{
    try{
        if(validateRequestType(requiredType, true)){
            readRequest(deserializer, outDeserializable);
            return true;
        }
        else{
            sendBadRequestError(Errors::InvalidTypeMessage);
        }
    }catch(const CommonExceptions::ParseException){
        sendBadRequestError(Errors::ParseError);
    }

    return false;
}

ResponseBuilder RequestHelper::respond(Serializer * serializer, const ContentType & contentType) const{
    return ResponseBuilder(response, serializer).prepare(contentType);
}


void RequestHelper::sendResponse(const Serializable & serializable, Serializer & serializer
    , const ContentType & contentType)
{
    Response& response = prepareResponse(contentType, serializer);
    serializable >> serializer;
    sendSerializerContent(response, serializer);
}

Response & RequestHelper::prepareResponse(const ContentType& contentType, Serializer& serializer)
{
    Response & response = getResponse();
    response.setContentType(contentType);
    response.sendHeaders();
    serializer.clear();

    return response;
}

void RequestHelper::sendSerializerContent(Response& response, Serializer& serializer)
{
    response << serializer;
}

long RequestHelper::getParameter(const string & paramName, long defaultValue){
    return utils::StrCast().tryToLong(getRequest().getParameter(paramName), defaultValue);
}

bool RequestHelper::queryParameter(const string & paramName, bool defaultValue){
    const string paramValue = request->getQueryParameter(paramName);
    return paramValue.empty() ? defaultValue : StrCast().toBoolean(paramValue);
}

bool RequestHelper::validateRequestType(const ContentType & expectedType, bool ignoreOnEmptyRequest)
{
    Request & request = getRequest();
    if(ignoreOnEmptyRequest && !request.hasContent()){
        return true;
    }
    if(!request.getContentType().is(expectedType)){
        sendBadRequestError(Errors::InvalidTypeMessage);
        return false;
    }
    return true;
}

void RequestHelper::sendInvalidJsonError()
{
    sendBadRequestError(Errors::InvalidJsonMessage);
}

void RequestHelper::sendBadRequestError(const string & errMsg)
{
    sendError(StatusResponses::BAD_REQUEST, errMsg);
}

void RequestHelper::sendError(const StatusResponse & status, const string & errMsg){
    sendStatus(status) << errMsg;
}

void RequestHelper::sendEmpty(const network::StatusResponse & status){
    sendStatus(status);
}

Response & RequestHelper::sendStatus(const network::StatusResponse& status){
    Response & response = getResponse();
    response.setStatusLine(status);
    response.sendHeaders(); 
    
    return response;
}

}//namespace
}//namespace
