#ifndef API_H
#define API_H

#include <string>

#define IMUTABLE_CSTR const constexpr char * const

namespace calmframe {
namespace api {

namespace paths {
    IMUTABLE_CSTR photos      = "photos";
    IMUTABLE_CSTR collections = "collections";
    IMUTABLE_CSTR contexts    = "contexts";
    IMUTABLE_CSTR users       = "users";
    IMUTABLE_CSTR events      = "events";
    IMUTABLE_CSTR collaborators = "collaborators";

    //subpaths
    IMUTABLE_CSTR current_user = "me";
    IMUTABLE_CSTR change_password = "password";
    IMUTABLE_CSTR user_profile_image = "profileImage";
    IMUTABLE_CSTR user_active = "active";

    IMUTABLE_CSTR defaultContext = "default";
    IMUTABLE_CSTR rootCollection = "root";
    
}//namespace

namespace entries{
    std::string makeEntry(const std::string & path);

    const std::string photos      = makeEntry(paths::photos);
    const std::string collections = makeEntry(paths::collections);
    const std::string contexts    = makeEntry(paths::contexts);
    const std::string session     = makeEntry("session");
    const std::string users       = makeEntry(paths::users);
    const std::string events      = makeEntry(paths::events);
}

namespace parameters {
    IMUTABLE_CSTR PhotoCollectionId = "collection";
    IMUTABLE_CSTR PhotoContextId    = "context";
    IMUTABLE_CSTR userId            = "userId";
    IMUTABLE_CSTR recursive          = "recursive";
    IMUTABLE_CSTR query              = "query";
    IMUTABLE_CSTR token              = "token";
    IMUTABLE_CSTR limit              = "limit";
    IMUTABLE_CSTR include_permission = "include-permission";

    IMUTABLE_CSTR expandItems = "expand_items";
}//namespace

namespace fields {
    IMUTABLE_CSTR contexts    = paths::contexts;
    IMUTABLE_CSTR collections = paths::collections;
    IMUTABLE_CSTR photos      = paths::photos;
    IMUTABLE_CSTR users       = paths::users;
    IMUTABLE_CSTR events      = paths::events;
    IMUTABLE_CSTR collaborators = paths::collaborators;
    IMUTABLE_CSTR parentCollectionId  = "parentId";

    IMUTABLE_CSTR password = paths::change_password;
    IMUTABLE_CSTR user_active = paths::user_active;
    IMUTABLE_CSTR subcollections = "subcollections";
}//namespace

namespace cookies {
    IMUTABLE_CSTR sessionCookie = parameters::token;
}//namespace

}//namespace

    inline std::string api::entries::makeEntry(const std::string & path){
        return "/" + path;
    }
}//namespace

#endif // API_H

