#include "Api.h"
#include "PhotoFrameApi.h"

#include "network/Url.h"
#include "network/controller/WebController.h"
#include "network/controller/ControllerRouter.h"
#include "network/controller/CollectionControllerRouter.h"
#include "network/controller/SecurityController.h"
#include "network/controller/CORSController.h"
#include "network/controller/NotFoundController.h"
#include "network/controller/ExceptionHandlerController.h"

#include "api/SessionRequestValidator.h"
#include "api/SessionController.h"
#include "api/UsersController.h"
#include "api/PhotoController.h"
#include "api/PhotoCollectionController.h"
#include "api/PhotoContextController.h"
#include "api/EventsController.h"
#include "api/controllers/ApiController.h"

#include "services/PhotoManagersLayer.h"
#include "services/AccountManager.h"

#include "utils/NullPointerException.hpp"
#include "utils/IllegalStateException.h"

namespace calmframe {

using namespace network;
using namespace api;

PhotoFrameApi::PhotoFrameApi()
    : apiRoot(NULL)
{
}

PhotoFrameApi::~PhotoFrameApi()
{
    if(isInitialized()){
        destroy();
    }
}

WebController *PhotoFrameApi::getApiRoot()
{
    return apiRoot;
}

template<typename T>
static T * putController(PhotoFrameApi::ControllersCollection & controllersColl, T * controller){
    controllersColl.insert(controller);
    return controller;
}

template<typename T>
static T * putApiController(ControllerRouter* api, const std::string & apiEntry
    , PhotoFrameApi::ControllersCollection & controllersColl, T * controller)
{
    putController(controllersColl, controller);
    api->add(apiEntry, controller);
    controller->setRouter(api);

    return controller;
}

template<typename T>
void destroyPointer(T* & ptr){
    if(ptr != NULL){
        delete ptr;
        ptr = NULL;
    }
}

static void registerApi(PhotoFrameApi::ControllersCollection & ctrls
                , ControllerRouter* api, PhotoManagersLayer * managers, AccountManager * accountManager)
{
    PhotoManager             * photoManager  = &managers->getPhotoManager();
    PhotoCollectionManager * collManager = &managers->getCollectionsManager();
    PhotoContextManager    * ctxManager  = &managers->getContextManager();

    putApiController(api, api::entries::contexts   , ctrls, new PhotoContextController (ctxManager));
    putApiController(api, api::entries::collections, ctrls, new PhotoCollectionController(collManager));
    putApiController(api, api::entries::photos     , ctrls, new PhotoController(photoManager));
    putApiController(api, api::entries::users      , ctrls, new UsersController(accountManager));
    putApiController(api, api::entries::events     , ctrls, new EventsController());
}

void PhotoFrameApi::initialize(PhotoManagersLayer * managersLayer, AccountManager * accountManager){
    if(isInitialized()){
        throw CommonExceptions::IllegalStateException("PhotoFrameApi instance is already initialized.");
    }
    CHECK_NOT_NULL(managersLayer);

    //TODO: use shared_ptr

    ControllerRouter * apiRouter = putController(controllers, new ControllerRouter());

    registerApi(controllers, apiRouter, managersLayer, accountManager);
    //Signup route should not require authentication
    validator.putNonAuthenticatedRoute(ApiRoute(api::entries::users, HttpMethods::POST));
    validator.putNonAuthenticatedRoute(ApiRoute("/login", HttpMethods::POST));

    SecurityController * securityController = putController(controllers, new SecurityController());
    securityController->setValidator(&this->validator);
    securityController->addController(apiRouter);

    CollectionControllerRouter * sequenceRouter = putController(controllers, new CollectionControllerRouter());
    sequenceRouter->addController(putController(controllers, new CORSController()));
    sequenceRouter->addController(securityController);
    sequenceRouter->addController(putController(controllers, new ApiController()));
    sequenceRouter->addController(putController(controllers, new SessionController()));
    sequenceRouter->addController(putController(controllers, new NotFoundController()));

    ExceptionHandlerController * excController = putController(controllers, new ExceptionHandlerController());
    excController->setDelegateController(sequenceRouter);

    this->apiRoot = excController;
}

void PhotoFrameApi::destroy(){
    for(ControllersCollection::iterator iter = controllers.begin();
            iter != controllers.end(); ++iter)
    {
        WebController * controller = *iter;
        destroyPointer(controller);
    }
    controllers.clear();
    apiRoot = NULL;
}

bool PhotoFrameApi::isInitialized() const{
    return apiRoot != NULL;
}

}//namespace

