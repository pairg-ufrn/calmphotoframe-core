#ifndef PHOTOCONTROLLER_H
#define PHOTOCONTROLLER_H

#include <memory>

#include "model/photo/Photo.h"
#include "services/PhotoManager.h"

#include "api/CollectionWebController.h"
#include "network/Response.h"
#include "network/Request.h"

namespace calmframe {
namespace network {
    class MultipartRequestReader;
}//namespace
}//namespace

namespace calmframe{
namespace api{

using namespace calmframe::network;

class PhotoController : public CollectionWebController{
public:
    typedef CollectionWebController super;
    using MultipartReaderPtr = std::shared_ptr<MultipartRequestReader>;

private:
    PhotoManager * photoManager;
    MultipartReaderPtr multipartReader;
    
public:
    PhotoController(PhotoManager * photoManager);

    long getContextIdParam(Request &request) const;
    bool collectionIsValid(long collId) const;
    bool contextIsValid(long ctxId) const;
    std::vector<Photo> listPhotos(std::string querySearchParam, Request &request);
    
    MultipartReaderPtr getMultipartReader() const;
    void setMultipartReader(MultipartReaderPtr multipartReader);
    
protected: //CRUD methods
    virtual bool existMember(long id);
    virtual bool onListAll(network::Request & request, network::Response & response);
    virtual bool onGetMember(network::Request & request, network::Response & response
                                    , long memberId, Url & relativeUrl);
    virtual bool onCreateMember(network::Request & request, network::Response & response);
    virtual bool onUpdateMember(Url & relativeUrl
                                    , network::Request & request
                                    , network::Response & response
                                    , long memberId);
    virtual bool onDeleteMember(network::Request & request, network::Response & response
                                       , long memberId);

private:
    void assertPhotoManagerNotNull() const;

    long getCollectionId(Request &request) const;
    long requireCollectionId(Request &request) const;

protected: //Photo creation auxiliary methods
    virtual bool onGetPhotoProperty(network::Request & request, network::Response & response
        , long memberId , const std::string & memberProperty) const;

    virtual Photo createPhoto(network::Request & request) const;
    Photo createPhotoFromImageContent(Request &request, long collectionId, long userId) const;

    Photo createPhotoFromMultipart(network::Request & request, long collectionId, long userId) const;
    void receivePhotoCreationParts(network::Request& request, Image & tmpImage
                                   , PhotoDescription * photoDescription
                                   , std::string * originalFilename = NULL) const;
    void receiveImage(Request* request, Image & image) const;
    void receivePhotoDescription(Request* part
                                        , PhotoDescription *description
                                        , std::string * originalFilename) const;

    Photo updatePhoto(network::Request& request, const Photo & photoToUpdate);
    long getSessionUser(const Request & request) const;
};

}//api
}//calmframe

#endif // PHOTOCONTROLLER_H
