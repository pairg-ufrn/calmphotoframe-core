#include "ApiController.h"

#include "api/SessionRequestHelper.h"

#include "Options.h"
#include "data/Data.h"
#include "Environment.h"

#include "api/RequestHelper.h"
#include "network/ContentTypes.hpp"
#include "network/StatusResponses.h"
#include "services/SessionManager.h"
#include "serialization/json/JsonSerializer.h"
#include "serialization/GenericField.h"

namespace calmframe {

using namespace data;

bool ApiController::onGet(network::Request & request, network::Response & response){
    if(request.url().path().count() > 0){
        return false;
    }
    
    api::RequestHelper helper{request, response};
    sendServerInfo(helper);
    
    return true;
}

bool ApiController::onPost(network::Request & request, network::Response & response){
    if(request.url().path().count() > 0){
        return false;
    }
    
    GenericField<std::string> serverName(ServerInfo::Fields::serverName);
    
    api::RequestHelper helper{request, response};
    JsonSerializer serializer;
    
    if(helper.receiveEntity(serializer, serverName, network::ContentTypes::JSON)){
        if(validateUpdateServerRequest(helper)){
            //TODO: move this code to service layer
            Data::instance().configs().put(Options::SERVER_NAME, serverName.getValue());
            
            sendServerInfo(helper);
        }
    
        return true;
    }

    return true;
}

ServerInfo ApiController::buildServerInfo(){
    ServerInfo info;
    info.setServerName(data::Data::instance().configs().get(Options::SERVER_NAME));
    info.setAppVersion(Environment::instance().getAppVersion());
    
    return info;
}

void ApiController::sendServerInfo(api::RequestHelper & helper){
    auto serverInfo = buildServerInfo();
    helper.sendResponse<JsonSerializer>(serverInfo, network::ContentTypes::JSON);    
}

bool ApiController::validateUpdateServerRequest(api::RequestHelper & helper){
    long userId = SessionRequestHelper::getSessionUser(helper.getRequest());
    UserAccount account;
    
    if(userId == User::INVALID_ID || Data::instance().users().get(userId, account) == false){
        helper.sendError(network::StatusResponses::UNAUTHORIZED, "There is no logged in user");
        return false;
    }    
    
    if(!account.isAdmin()){
        helper.sendError(network::StatusResponses::FORBIDDEN, "User needs to be admin");
        return false;
    }
    
    return true;
}

}//namespace
