#ifndef APICONTROLLER_H
#define APICONTROLLER_H

#include "network/controller/WebController.h"

#include "model/ServerInfo.h"

namespace calmframe {
namespace api {
    class RequestHelper;
}//namespace

class ApiController : public network::WebController{
public:
    bool onGet(network::Request & request, network::Response & response) override;
    bool onPost(network::Request &request, network::Response &response) override;
    
public:
    ServerInfo buildServerInfo();
    bool validateUpdateServerRequest(api::RequestHelper & helper);
    void sendServerInfo(api::RequestHelper & helper);
};

}//namespace

#endif // APICONTROLLER_H
