#ifndef INVALIDREQUESTEXCEPTION_H
#define INVALIDREQUESTEXCEPTION_H

#include "exceptions/ApiExceptions.h"
#include <string>

namespace calmframe {
namespace api {

EXCEPTION_SUBCLASS(InvalidRequestException, ClientError);

} /* namespace api */
} /* namespace calmframe */

#endif // INVALIDREQUESTEXCEPTION_H
