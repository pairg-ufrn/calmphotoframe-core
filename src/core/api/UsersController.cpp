﻿#include "UsersController.h"

#include "api/Api.h"
#include "api/RequestHelper.h"

#include "../model/user/UserAccount.h"
#include "../model/user/UserLoginInfo.h"
#include "model/image/ImageExternalStream.h"

#include "services/AccountManager.h"

#include "api/InvalidRequestException.h"
#include "exceptions/UnsupportedContentTypeError.h"
#include "exceptions/NotFoundMember.h"

#include "network/Request.h"
#include "network/Response.h"
#include "network/ContentTypes.hpp"
#include "network/Url.h"
#include "network/Cookie.h"
#include "network/StatusResponses.h"

#include "serialization/json/JsonSerializer.h"
#include "serialization/GenericField.h"

#include "utils/CommonExceptions.h"
#include "utils/StringCast.h"

#include "utils/Log.h"

namespace calmframe {

using namespace network;
using namespace api;
using namespace utils;

using namespace std;

namespace Errors {
    const string DuplicateLogin = "Duplicate member error, login already exist";
    const string insufficientPrivileges = "Insufficient privileges to execute this operation";
    const string updateFieldsForbidden  = "Trying to change fields that require privileged permissions";
    constexpr const char * isNotAccountOwner = "Only account owner can execute this operation";
    
}//namespace

/** Helper class to serialize a collection of users*/
class UsersListContainer : public Serializable{
    vector<User> & users;
public:
    UsersListContainer(vector<User> & users)
        : users(users)
    {}

    virtual void serialize(Serializer & serializer) const{
        serializer.putCollection(api::fields::users, users.begin(), users.end());
    }
};

class PasswordInfo : public Deserializable{
public:
    std::string password;

    void deserialize(const GenericContainer & deserializer){
        this->password = deserializer.get(api::fields::password).asString();
    }
};


UsersController::UsersController(AccountManager * manager)
    : accountManager(manager)
{}

AccountManager * UsersController::getAccountManagerPtr() const{
    return accountManager;
}

const AccountManager &UsersController::getAccountManager() const{
    CHECK_NOT_NULL(accountManager);
    return *accountManager;
}

AccountManager &UsersController::getAccountManager(){
    CHECK_NOT_NULL(accountManager);
    return *accountManager;
}

void UsersController::setAccountManager(AccountManager * value){
    accountManager = value;
}

bool UsersController::onRequest(Request & request, Response & response){

    string cookieHeaderValue = request.getHeaderValue(Headers::Cookie);

    ParameterizedHeader cookieHeader(Headers::Cookie, cookieHeaderValue,';','=');
    string sessionToken = cookieHeader.getParameter(api::cookies::sessionCookie);

    long userId = getAccountManager().getSessionUser(sessionToken);
    if(userId != User::INVALID_ID){
        request.putParameter(api::parameters::userId, StrCast().toString(userId));
    }

    redirectUrlAlias(request, userId);

    return super::onRequest(request, response);
}

void UsersController::redirectUrlAlias(Request& request, long userId)
{
    Url relative = getRelativeUrl(request);
    
    if(relative.path().get(0) == api::paths::current_user && userId != User::INVALID_ID){
        Url fullUrl = request.url();
        UrlPath & path = fullUrl.path();
        
        for (size_t i = 0; i < path.count(); ++i) {
            if(path.get(i) == api::paths::current_user){
                path.set(i, StrCast().toString(userId));
                request.url(fullUrl);
                break;
            }
        }
    }    
}

bool UsersController::existMember(long id){
    return getAccountManager().exist(id);
}


bool UsersController::onListAll(Request & request, Response & response){
    RequestHelper helper(request, response);

    vector<User> users = getAccountManager().listUsers();

    helper.sendResponse<JsonSerializer>(UsersListContainer(users), ContentTypes::JSON);

    return true;
}
bool UsersController::onGetMember(Request & request, Response & response, long memberId, Url & relativeUrl){
    if(relativeUrl.path().count() == 1){
        return onGetUser(request, response, memberId);
    }
    else 
    if(relativeUrl.path().count() == 2 
        && relativeUrl.path().get(1) == api::paths::user_profile_image)
    {
        return onGetProfileImage(request, response, memberId);
    }
    
    return false;
}

bool UsersController::onGetUser(Request& request, Response& response, long memberId){
    RequestHelper helper(request, response);
    User user = getAccountManager().getUser(memberId);
    helper.sendResponse<JsonSerializer>(user, ContentTypes::JSON);    
    
    return true;
}

bool UsersController::onGetProfileImage(Request&, Response& response, long memberId)
{
    auto image = getAccountManager().getProfileImage(memberId);    

    if(!image){
        throw NotFoundMember();    
    }
    
    response.setStatusLine(StatusResponses::OK);
    response.sendFile(image->getImagePath());

    return true;
}

bool UsersController::onCreateMember(Request & request, Response & response){
    CHECK_NOT_NULL(getAccountManagerPtr());

    JsonSerializer serializer;
    RequestHelper helper(request, response);

    UserLoginInfo loginInfo;
    if(helper.receiveEntity(serializer, loginInfo, ContentTypes::JSON)){
        try{
            //Account start activated if creator user is admin
            bool startActivated = getAccountManager().isAdmin(getRequestUserId(helper));

            UserAccount account = getAccountManagerPtr()->create(loginInfo, startActivated);
            helper.sendResponse(account.getUser(), serializer, ContentTypes::JSON);
        }
        catch(CommonExceptions::IllegalArgumentException & ex){
            helper.sendBadRequestError(ex.getMessage());
        }
    }

    return true;
}

bool UsersController::onUpdateMember(Url & url, Request & request, Response & response, long memberId){
    RequestHelper helper(request, response);
    
    if(url.path().count() == 1){
        return updateUser(memberId, helper);
    }
    else if(url.path().count() == 2){
        const auto & field = url.path().get(1);
        
        if(field == api::paths::change_password){
            return onChangePassword(memberId, helper);
        }
        else if(field == api::paths::user_profile_image){
            return onUpdateUserProfileImage(helper, memberId);
        }
        else if(field == api::paths::user_active){
            return onActivateUser(helper, memberId);
        }
    }

    return false;
}

bool UsersController::updateUser(long memberId, RequestHelper & helper)
{
    JsonSerializer serializer;

    User user = getAccountManager().getUser(memberId);
    if(helper.receiveEntity(serializer, user, ContentTypes::JSON)){
        if(ensureCanUpdateUser(helper, user)){
            getAccountManager().updateUser(user);
            helper.sendResponse(user, serializer, ContentTypes::JSON);
        }
    } 
    
    return true;   
}

bool UsersController::onUpdateUserProfileImage(RequestHelper & reqHelper, long memberId){
    UnsupportedContentTypeError::checkType(reqHelper.getRequest().getContentType(), {Types::IMAGE, ""});

    ImageExternalStream streamImage{&reqHelper.getRequest().getContent()};
    
    try{
        getAccountManager().updateProfileImage(memberId, streamImage);
        
        reqHelper.sendEmpty(StatusResponses::OK);
    }
    catch(const CommonExceptions::IOException &){
        throw InvalidRequestException("Failed to update profile image. The image may be invalid.");
    }
    
    return true;
}

bool UsersController::onActivateUser(RequestHelper & reqHelper, long memberId){
    JsonSerializer serializer;
    
    GenericField<bool> isActive{api::fields::user_active};
    
    if(ensureIsAdminRequest(reqHelper) 
        && reqHelper.receiveEntity(serializer, isActive, ContentTypes::JSON))
    {
        auto updatedUser = getAccountManager().setAccountEnabled(memberId, isActive.getValue());
        
        isActive.setValue(updatedUser.isActive());
        reqHelper.sendResponse(isActive, serializer, ContentTypes::JSON);
    }
    
    return true;
}

bool UsersController::onChangePassword(long memberId, RequestHelper & helper){
    JsonSerializer serializer;
    
    PasswordInfo password;
    
    if(helper.receiveEntity(serializer, password, ContentTypes::JSON)){
        getAccountManager().updatePassword(memberId, password.password);
        
        helper.sendEmpty(StatusResponses::OK);    
    }
    
    return true;
}

bool UsersController::onDeleteMember(Request & request, Response & response, long memberId){
    RequestHelper helper(request, response);

    if(ensureIsSameUserOrIsAdmin(helper, memberId)){
        User removedUser = getAccountManager().removeUser(memberId);

        helper.sendResponse<JsonSerializer>(removedUser, ContentTypes::JSON);
    }

    return true;
}

bool UsersController::ensureIsSameUserOrIsAdmin(RequestHelper & helper, long memberId){
    long sessionUserId = getRequestUserId(helper);
    bool hasPermission = (sessionUserId == memberId || getAccountManager().isAdmin(sessionUserId));

    if(!hasPermission){
        sendInsufficientPrivilegesError(helper);
    }

    return hasPermission;
}

bool UsersController::ensureCanUpdateUser(RequestHelper & helper, const User & user){
    try{
        if(ensureIsSameUser(helper, user.getId())){
            
            long sessionUserId = this->getRequestUserId(helper);
            getAccountManager().ensureUpdatePermission(sessionUserId, user);
            return true;
        }
    }
    catch(UpdateFieldsForbiddenException &){
        sendInsufficientPrivilegesError(helper, Errors::updateFieldsForbidden);
    }
    catch(InsufficientPermissionException &){
        sendInsufficientPrivilegesError(helper);
    }
    return false;
}
bool UsersController::ensureIsAdminRequest(RequestHelper & helper){
    long userId = getRequestUserId(helper);

    if(!getAccountManager().isAdmin(userId)){
        sendInsufficientPrivilegesError(helper);

        return false;
    }

    return true;
}

bool UsersController::ensureIsSameUser(RequestHelper & helper, long memberId)
{
    if(getRequestUserId(helper) != memberId){
        sendInsufficientPrivilegesError(helper, Errors::isNotAccountOwner);
    
        return false;
    }
    return true;
}

void UsersController::sendInsufficientPrivilegesError(RequestHelper & helper){
    sendInsufficientPrivilegesError(helper, Errors::insufficientPrivileges);
}

void UsersController::sendInsufficientPrivilegesError(RequestHelper & helper, const string & errMsg){
    helper.sendError(StatusResponses::FORBIDDEN, errMsg);
}

long UsersController::getRequestUserId(RequestHelper & helper){
    return helper.getParameter(api::parameters::userId, User::INVALID_ID);
}

}//namespace



