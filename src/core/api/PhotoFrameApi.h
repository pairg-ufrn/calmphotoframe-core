#ifndef APIBUILDER_H
#define APIBUILDER_H

#include <set>

#include "api/SessionRequestValidator.h"

namespace calmframe {
    class PhotoManagersLayer;
    class AccountManager;
    namespace network { class WebController;}
}


namespace calmframe {

using ::calmframe::network::WebController;

//TODO: prevent copy constructor and assignment operator
class PhotoFrameApi{

public:
    typedef std::set<network::WebController *> ControllersCollection;
private:
    ControllersCollection controllers;
    WebController * apiRoot;

    //TODO: store only pointer to RequestValidator (remove dependency to implementation)
    SessionRequestValidator validator;
public:
    PhotoFrameApi();
    virtual ~PhotoFrameApi();

    void initialize(PhotoManagersLayer * photoManagersLayer, AccountManager * accountManager);
    void destroy();
    bool isInitialized() const;

    WebController * getApiRoot();
};

}//namespace

#endif // APIBUILDER_H
