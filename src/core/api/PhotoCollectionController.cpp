#include "PhotoCollectionController.h"

#include "Api.h"
#include "api/PhotoController.h"
#include "api/RequestHelper.h"

#include "model/photo/PhotoContext.h"
#include "model/CollectionsOperation.h"
#include "services/PhotoCollectionManager.h"
#include "network/ContentTypes.hpp"
#include "network/Url.h"
#include "network/StatusResponses.h"

#include "exceptions/NotFoundMember.h"

#include "utils/CommonExceptions.h"
#include "utils/StringCast.h"
#include "utils/StringUtils.h"
#include "utils/Log.h"

#include <iterator>

namespace calmframe {

using namespace calmframe::network;
using namespace calmframe::api;
using namespace utils;

using namespace std;

using CommonExceptions::ParseException;

const char * PhotoCollectionController::ADD_PHOTOS_SUBPATH   = "add-photos";
const char * PhotoCollectionController::REMOVE_PHOTOS_SUBPATH = "remove-photos";

namespace Errors {
    constexpr const char * const MissingContext = "Request require a context id parameter.";

    namespace Constants {
        static const char * NullCollectionManager = "PhotoCollectionManager instance is NULL";
        static const char * InvalidTypeMessage = "Invalid request type";
        static const char * InvalidJsonMessage = "Received invalid json message";
        static const string InvalidOperationType = string("Invalid or missing value in '")
                                                      .append(CollectionsOperationFields::OPERATION_TYPE)
                                                      .append("' field.");
        static const string MissingPhotosParameterError = string("Missing '")
                                                      .append(PhotoCollectionFields::PHOTO_IDS)
                                                      .append("' parameter on content.");
        static const string MissingContextIdParameter = string("Missing '")
                                                       .append(api::parameters::PhotoContextId)
                                                       .append("' parameter on request.");
    }//namespace

    static string getMissingMemberError(const string & paramName){
        return string("Missing '").append(paramName).append("' member on content.");
    }
    static string getMissingMembersError(const string & member1, const string & member2){
        return string("Missing member '").append(member1).append("' or '")
                                  .append(member2).append("'on request content.");
    }

    static const string CreateColl_NoParentOrContext =
            "Use a context subpath (ex.: /contexts/<context id>/collections)"
            " or include on the request body the parent id "
            "(ex: \"" + string(api::fields::parentCollectionId) + "\" "
            ": <parent collection id>)";
}//namespace


PhotoCollectionController::PhotoCollectionController(PhotoCollectionManager *collectionManager)
    : collectionManager(collectionManager)
{
    setRootPath(api::paths::collections);
}

bool PhotoCollectionController::onRequest(Request & request, Response & response)
{
    Url relativeUrl = getRelativeUrl(request);

    long idParam = getIdParameter(relativeUrl, request);
    if(idParam != PhotoCollection::INVALID_ID){
        request.putParameter(api::parameters::PhotoCollectionId, StrCast().toString(idParam));
    }

    if(relativeUrl.path().count() > 1 
            && relativeUrl.path().get(1) == api::paths::photos)
    {
        relativeUrl.path().remove(0);
        return forward(request, response, relativeUrl);
    }

    return super::onRequest(request, response);
}

const PhotoCollectionManager &PhotoCollectionController::getCollectionManager() const{
    CHECK_NOT_NULL(collectionManager);
    return *collectionManager;
}

PhotoCollectionManager & PhotoCollectionController::getCollectionManager()
{
    CHECK_NOT_NULL(collectionManager);
    return *collectionManager;
}

bool PhotoCollectionController::isAValidId(long id) const
{
    return id >= 0;
}

void PhotoCollectionController::setCollectionManager(PhotoCollectionManager *value)
{
    collectionManager = value;
}

bool PhotoCollectionController::onDelete(Request &request, Response &response)
{
    try{
        bool handled = super::onDelete(request, response);
        return handled;
    }
    catch(CommonExceptions::IllegalArgumentException & argException){
        sendBadRequestMessage(response, argException.what());
        return true;
    }
}

bool PhotoCollectionController::existMember(long id){
    return getCollectionManager().existCollection(id);
}

long PhotoCollectionController::getIdParameter(const Url & relativeUrl, const Request& request) const
{
    long collId = PhotoCollection::INVALID_ID;

    const string & collParam = request.hasParam(api::parameters::PhotoCollectionId)
                                        ? request.getParameter(api::parameters::PhotoCollectionId)
                                        : relativeUrl.path().get(0);

    if(collParam == api::paths::rootCollection){
        long ctxId = getCtxIdParam(request);
        if(ctxId != PhotoContext::INVALID_ID){
            collId = getCollectionManager().getContextRootId(ctxId);
        }
    }
    else if(!StringCast::cast().tryToLong(collId, collParam)){
        collId = StringCast::cast().tryToLong(relativeUrl.path().get(0), collId);
    }

    return collId;
}

bool PhotoCollectionController::tryGetMemberId(const Request & request, long * idOut, Url * outUrl) const
{
    Url relativeUrl = getRelativeUrl(request);
    long collId = getIdParameter(relativeUrl, request);

    if(idOut != NULL){
        *idOut = collId;
    }
    if(outUrl != NULL){
        *outUrl = relativeUrl;
    }
    return collId != PhotoCollection::INVALID_ID;
}

bool PhotoCollectionController::onGetSubpathRequest(Request &request, Response &response, network::Url & relativeUrl)
{
    if(relativeUrl.path().get(0) == CollectionsControllerFields::GetRootSubpath){
        long ctxId = getCtxIdParam(request);
        long rootId = ctxId == PhotoContext::INVALID_ID
                                                    ? PhotoCollection::INVALID_ID
                                                    : getCollectionManager().getContextRootId(ctxId);

        if(rootId == PhotoCollection::INVALID_ID){
            sendNotFoundMessage(response);
            return true;
        }
        return onGetMember(request, response, rootId, relativeUrl);
    }
    return false;
}

bool PhotoCollectionController::onGetMember(Request &request, Response &response, long collectionId, Url &)
{
    PhotoCollection photoCollection = getCollectionManager().getCollection(collectionId);

    super::ensureUserPermission(request, photoCollection.getParentContext(), PermissionLevel::Read);

    sendPhotoCollection(photoCollection, RequestHelper(request, response));

    return true;
}

void PhotoCollectionController::sendPhotoCollection(
    const PhotoCollection & photoCollection,
    const RequestHelper & reqHelper)
{
    auto response = reqHelper.respond(&serializer, ContentTypes::JSON)
                    .add(photoCollection);

    if(expandItemsParam(reqHelper.getRequest())){
        long id = photoCollection.getId();
        auto photos       = getCollectionManager().getCollectionPhotos(id);
        auto collections  = getCollectionManager().getSubCollections(id);

        response.addItems(api::fields::photos, photos)
                .addItems(api::fields::subcollections, collections);
    }

    response.send();
}

bool PhotoCollectionController::validateCollectionsOpRequest(Request &request, Response &response, CollectionsOperation &outCollOp)
{
    int opNum = CollectionsOperation::UNKNOWN;
    return validateCollectionsOpRequest(request, response, outCollOp, opNum);
}

bool PhotoCollectionController::onPhotoCollectionOperationRequest(Request &request, Response &response)
{
    CollectionsOperation collOp;
    if(!validateCollectionsOpRequest(request, response, collOp)){
        return true;
    }

    long ctxId = getCtxIdParam(request);
    //TODO: talvez o id de contexto seja necessário apenas para operação de remoção (e talvez de mover)
    if(ctxId == PhotoContext::INVALID_ID){
        sendBadRequestMessage(response, Errors::Constants::MissingContextIdParameter);
        return true;
    }

    getCollectionManager().executeCollectionOperation(collOp, getCollectionManager().getContextRootId(ctxId));
    vector<PhotoCollection> changedCollections = getCollectionManager()
                                                    .listIn(collOp.getTargetCollectionIds());

    serializer.clear();
    serializer.putCollection(api::fields::collections,changedCollections.begin(), changedCollections.end());

    sendSerializerJsonContent(response);

    return true;
}

bool PhotoCollectionController::onCreateMember(Request & request, Response & response){
    RequestHelper controllerHelper(request, response);
    PhotoCollection requestColl;
    JsonSerializer serializer;

    if(controllerHelper.receiveEntity(serializer, requestColl, ContentTypes::JSON)){
        long parentId = getParentCollectionId(serializer);
        long contextId = getCtxIdParam(request);

        if(parentId == PhotoCollection::INVALID_ID && contextId == PhotoCollection::INVALID_CONTEXT_ID){
            controllerHelper.sendBadRequestError(Errors::CreateColl_NoParentOrContext);
            return true;
        }

        ensureUserPermission(request, getContextId(contextId, parentId), PermissionLevel::Write);

        PhotoCollection createdColl = getCollectionManager().createCollection(requestColl, parentId, contextId);
        controllerHelper.sendResponse(createdColl, serializer, ContentTypes::JSON);
    }

    return true;
}

bool PhotoCollectionController::onUpdateMember(Url &relativeUrl, Request &request, Response &response, long memberId)
{
    if(!isJsonRequest(request)){
        sendInvalidRequestTypeMessage(request, response);
        return true;
    }

    PhotoCollection collection = getCollectionManager().getCollection(memberId);
    ensureUserPermission(request, collection.getParentContext(), PermissionLevel::Write);

    if(relativeUrl.path().count() >= 2){
        if(relativeUrl.path().get(1) == CollectionsControllerFields::MoveItemsSubpath){
            onMoveCollectionItemsRequest(request, response, memberId);
            return true;
        }
        return false;
    }

    handleUpdateMember(request, response, collection);
    return true;
}

void PhotoCollectionController::handleUpdateMember(Request &request, Response &response, PhotoCollection & collection){
    RequestHelper requestHelper(request, response);

    if(requestHelper.receiveEntity<JsonSerializer>(collection, ContentTypes::JSON)){
        collection = getCollectionManager().updateCollection(collection);

        requestHelper.sendResponse<JsonSerializer>(collection, ContentTypes::JSON);
    }
}

bool PhotoCollectionController::onListAll(Request &request, Response &response){
    RequestHelper helper(request, response);

    long contextId = getCtxIdParam(request);
    if(!contextIsValid(contextId)){
        helper.sendBadRequestError(Errors::MissingContext);
        return true;
    }

    ensureUserPermission(request, contextId, PermissionLevel::Read);

    vector<PhotoCollection> collections = listCollections(contextId);
    helper.sendResponseCollection<JsonSerializer>(api::fields::collections, collections, ContentTypes::JSON);

    return true;
}

vector<PhotoCollection> PhotoCollectionController::listCollections(long ctxId){
    return getCollectionManager().listFromContext(ctxId);
}

bool PhotoCollectionController::onPostSubpathRequest(Url& relativeUrl, Request &request, Response &response)
{
    if(relativeUrl.path().get(0, string()) == CollectionsControllerFields::BulkActionSubpath){
        return onPhotoCollectionOperationRequest(request, response);
    }
    return false;
}

void PhotoCollectionController::onMoveCollectionItemsRequest(Request &request, Response &response, long memberId)
{
    if(!checkJsonContent(request, response)){
        return;
    }

    CollectionsOperation collectionOperation;

    int opType = CollectionsOperation::MOVE;
    if(validateCollectionsOpRequest(request, response, collectionOperation, opType)){
       PhotoCollection changedCollection = getCollectionManager().moveCollectionItems(memberId, collectionOperation);
       serializer.clear();
       changedCollection >> serializer;

       sendSerializerJsonContent(response);
       //TODO: response
    }
}

bool PhotoCollectionController::onDeleteMember(Request & request, Response &response, long memberId)
{
    PhotoCollection collRemoved = getCollectionManager().getCollection(memberId);

    ensureUserPermission(request, collRemoved.getParentContext(), PermissionLevel::Write);

    RequestHelper requestHelper(request, response);
    if(getCollectionManager().removeCollection(memberId, collRemoved)){
        requestHelper.sendResponse<JsonSerializer>(collRemoved, ContentTypes::JSON);
    }
    else{
        requestHelper.sendError(StatusResponses::SERVER_ERROR, "Could not remove this collection.");
    }

    return true;
}

bool PhotoCollectionController::onBulkDeleteRequest(Request &request, Response &response)
{
    if(!request.getContentType().is(Types::APPLICATION, SubTypes::JSON)){
        sendBadRequestMessage(response, Errors::Constants::InvalidTypeMessage);
        return true;
    }

    set<long> collectionIds;
    try {
        serializer.clear();
        request.getContent() >> serializer;
        collectionIds = deserializePhotoIds(CollectionsControllerFields::PhotoCollectionIdsField);
    } catch (ParseException &) {
        sendBadRequestMessage(response, Errors::Constants::InvalidJsonMessage);
        return true;
    }

    vector<PhotoCollection> removedCollections = getCollectionManager().removeCollections(collectionIds);

    serializer.clear();
    serializer.putCollection(api::fields::collections, removedCollections.begin(), removedCollections.end());
    this->sendSerializerJsonContent(response);

    return true;
}

long PhotoCollectionController::getCtxIdParam(const Request &request) const
{
    const string ctxParam = request.getParameter(api::parameters::PhotoContextId);
    return StringCast::cast().tryToLong(ctxParam, PhotoContext::INVALID_ID);
}

set<long> PhotoCollectionController::deserializePhotoIds(const std::string & memberName)
{
    set<long> photoIds;
    serializer.getChildCollection(memberName
                                      , std::inserter<set<long> >(photoIds, photoIds.end())
                                      , GenericIterator::NumberConverter<long>());

    return photoIds;
}

bool PhotoCollectionController::expandItemsParam(Request &request) const
{
    string expandItemsParam = request.getQueryParameter(api::parameters::expandItems);

    return utils::StringCast::instance().toBoolean(expandItemsParam);
}

long PhotoCollectionController::getContextId(long contextId, long collectionId){
    if(contextIsValid(contextId)){
        return contextId;
    }
    return getCollectionManager().getCollectionContext(collectionId);
}

long PhotoCollectionController::getParentCollectionId(JsonSerializer & serializer)
{
    return ((Deserializer &)serializer)
                            .get(api::fields::parentCollectionId, PhotoCollection::INVALID_ID)
                            .asNumber<long>();
}

bool PhotoCollectionController::contextIsValid(long contextId) const{
   return contextId != PhotoCollection::INVALID_CONTEXT_ID;
}

void PhotoCollectionController::sendPhotoIds(const std::set<long> & photoIds, Response &response)
{
    serializer.clear();
    serializer.putCollection(PhotoCollectionFields::PHOTO_IDS, photoIds.begin(), photoIds.end());

    response.setStatusLine(StatusResponses::OK);
    sendSerializerJsonContent(response);
}

void PhotoCollectionController::sendSerializerJsonContent(Response &response)
{
    response.setContentType(ContentTypes::JSON);
    response.sendHeaders();
    response << serializer;
}

void PhotoCollectionController::sendInvalidRequestTypeMessage(Request &request, Response &response)
{
    string errMsg = getInvalidTypeMessage(request.getContentType());
    sendBadRequestMessage(response, errMsg);
}

string PhotoCollectionController::getInvalidTypeMessage(const ContentType & receivedType){
    return string(Errors::Constants::InvalidTypeMessage).append(" '").append(receivedType.toString()).append("'");
}

bool PhotoCollectionController::checkJsonContent(Request &request, Response &response)
{
    if(!isJsonRequest(request)){
        sendInvalidRequestTypeMessage(request, response);
        return false;
    }
    return true;
}
bool PhotoCollectionController::isJsonRequest(const Request &request)
{
    return request.getContentType().is(Types::APPLICATION, SubTypes::JSON);
}

bool PhotoCollectionController::validateCollectionsOpRequest(Request & request
        , Response &response, CollectionsOperation & outCollOp
        , int &forceOpType)
{
    if(!checkJsonContent(request, response)){
        return false;
    }
    try {
        request.getContent() >> this->serializer >> outCollOp;
    } catch (ParseException) {
        this->sendBadRequestMessage(response, Errors::Constants::InvalidJsonMessage);
        return false;
    }

    if(forceOpType != CollectionsOperation::UNKNOWN){
        if(outCollOp.getOperationType() == CollectionsOperation::UNKNOWN
                || outCollOp.getOperationType() == CollectionsOperation::MOVE)
        {
            outCollOp.setOperationType(CollectionsOperation::MOVE);
        }
        else{
            outCollOp.setOperationType(CollectionsOperation::UNKNOWN);
        }
    }

    return validateCollectionsOperation(outCollOp, response);
}


bool PhotoCollectionController::validateCollectionsOperation(CollectionsOperation& outCollOp, Response &response)
{
    //TODO: seria melhor checar pelos dois erros e comunicá-los de uma vez só
    if(outCollOp.getPhotoIds().empty() && outCollOp.getSubCollectionsIds().empty()){

        string errMsg = Errors::getMissingMembersError(CollectionsOperationFields::PHOTO_ITEMS_IDS
                                                     , CollectionsOperationFields::SUBCOLLECTIONS_IDS);
        sendBadRequestMessage(response, errMsg);
        return false;
    }
    if(outCollOp.getTargetCollectionIds().empty()){
        string errMsg = Errors::getMissingMemberError(CollectionsOperationFields::COLLECTIONS_TARGETS_IDS);
        sendBadRequestMessage(response, errMsg);
        return false;
    }
    if(outCollOp.getOperationType() == CollectionsOperation::UNKNOWN){
        sendBadRequestMessage(response, Errors::Constants::InvalidOperationType);
        return false;
    }
    return true;
}

} //namespace calmframe

