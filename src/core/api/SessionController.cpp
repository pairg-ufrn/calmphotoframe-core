#include "SessionController.h"

#include "Configurations.h"
#include "Options.h"

#include "data/Data.h"

#include "network/Request.h"
#include "network/Response.h"
#include "network/StatusResponses.h"
#include "network/Cookie.h"
#include "network/ContentTypes.hpp"

#include "../model/user/UserLoginInfo.h"
#include "services/SessionManager.h"

#include "utils/Log.h"
#include "utils/StringUtils.h"

#include "utils/CommonExceptions.h"

#include "json/json.h"

namespace calmframe{

using namespace network;
using namespace data;
using namespace utils;

using CommonExceptions::ParseException;
using std::string;

SessionController::SessionController(const std::string &baseUrl)
    : WebController(baseUrl)
{
}

SessionController::~SessionController()
{}

//FIXME: utilizando apenas para teste
bool SessionController::onGet(Request &request, Response &response){
    if(isCommandRequest(request, Constants::ApiPaths::checkSessionCommand)){
        return checkSession(request, response);
    }
    else if(!isCommandRequest(request,Constants::ApiPaths::loginCommand)
            && !isCommandRequest(request,Constants::ApiPaths::logoffCommand))
    {
        return false;
    }

    //FIXME: remover isto (código apenas para testes)
    //response.sendFile("test/manual/TestLoginForm.html");
//    return true;

    return false;
}

bool SessionController::onPost(Request &request, Response &response){
    try{
        if(isCommandRequest(request, Constants::ApiPaths::loginCommand)){
            return doLogin(request, response);
        }
        else if(isCommandRequest(request, Constants::ApiPaths::logoffCommand)){
            return doLogoff(request, response);
        }

        return false;
    }
    catch(ParseException & ex){
        response.setStatusLine(StatusResponses::BAD_REQUEST);
        response.sendHeaders();
        response.send("Invalid message format");
    }
    catch(...){
        response.setStatusLine(StatusResponses::SERVER_ERROR);
        response.sendHeaders();
        response.send("Internal Error!");
    }

    return true;
}

bool SessionController::isCommandRequest(Request &request, const std::string & commandName){
    string relativeUrl = getRelativeUrl(request);
    return !relativeUrl.empty()
            && StringUtils::instance().stringEndsWith(relativeUrl, commandName);
}

void SessionController::sendLoginResponse(Response &response, const Session & session)
{
    putSessionCookie(response, session);
    response.setStatusLine(StatusResponses::OK);
    response.setContentType(ContentTypes::JSON);

    Json::Value jsonToken(Json::objectValue);
    jsonToken["token"] = session.getToken();

    response.sendHeaders();
    response.send(jsonToken.toStyledString());
}

bool SessionController::doLogin(Request &request, Response &response)
{
    request.getContent() >> this->serializer;

    UserLoginInfo loginInfo;
    serializer.deserialize(loginInfo);

    Session session;
    if(authenticate(loginInfo, session)){
        sendLoginResponse(response, session);
    }
    else{
        sendUnauthorizedResponse(response);
    }

    return true;
}

std::string SessionController::getTokenValue(Request &request){
    string cookiesValue = request.getHeaderValue(Headers::Cookie);

    if(!cookiesValue.empty()){
        ParameterizedHeader cookiesHeader(Headers::Cookie, cookiesValue);
        return cookiesHeader.getParameter(Constants::sessionCookieName);
    }

    return string();
}

bool SessionController::doLogoff(Request &request, Response &response){
    string tokenValue = getTokenValue(request);

    if(!tokenValue.empty()){
        SessionManager::instance().finishSession(tokenValue);
    }

    response.setStatusLine(StatusResponses::OK);
    response.putHeader(Headers::ContentLength, "0");
    response.sendHeaders();
    response.send("");

    return true;
}

void SessionController::sendUnauthorizedResponse(Response &response)
{
    response.setStatusLine(StatusResponses::UNAUTHORIZED);
    response.sendHeaders();
    response.send("");
}

bool SessionController::checkSession(Request &request, Response &response)
{
    string tokenValue = getTokenValue(request);
    if(!tokenValue.empty() && SessionManager::instance().validateAndUpdateSession(tokenValue)){
        Session session;
        if(Data::instance().sessions().getByToken(tokenValue, session)){
            serializer.clear();
            serializer.put(FieldNames::Session::EXPIRE_TIMESTAMP, session.getExpireTimestamp());
            response.setContentType(ContentTypes::JSON);
            response.sendHeaders();
            response << serializer;
            return true;
        }
    }

    sendUnauthorizedResponse(response);
    return true;
}

bool SessionController::authenticate(const UserLoginInfo & loginInfo, Session & session){
    UserAccount userAccount;
    if(Data::instance().users().getByLogin(loginInfo.getLogin(), userAccount)){
        session = SessionManager::instance().startSession(userAccount, loginInfo.getPassword());

        return (session.getUserId() == userAccount.getId());
    }
    return false;
}
void SessionController::putSessionCookie(Response &response, const Session & session)
{
    Cookie cookie;
    cookie.setCookieName(Constants::sessionCookieName);
    cookie.setCookieValue(session.getToken());
    cookie.setCookiePath("/");
    cookie.putParameter(CookieParameters::HttpOnly,"");

    if(!Configurations::instance().getBoolean(Options::UNSAFE_COOKIE, false)){
        cookie.putParameter(CookieParameters::Secure,"");
    }

    response.putHeader(cookie);
}

string SessionController::getRelativeUrl(Request &request){
    const string & requestUrl = request.getUrl();
    const string & controllerUrl = getBaseUrl();

    if(StringUtils::instance().stringStartsWith(requestUrl,controllerUrl)){
        return requestUrl.substr(controllerUrl.size());
    }

    return string();
}

}
