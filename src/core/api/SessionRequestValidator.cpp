#include "SessionRequestValidator.h"

#include "SessionRequestHelper.h"

#include "network/Request.h"
#include "network/ParameterizedHeader.h"
#include "network/Headers.h"
#include "network/controller/ApiRoute.h"

#include "services/SessionManager.h"

#include "utils/NullPointerException.hpp"

#include <algorithm>

namespace calmframe {
using namespace network;
using std::string;

namespace Constants {
    static const char * TokenParameter = "token";
}

SessionRequestValidator::SessionRequestValidator(SessionManager * sessionManager)
    : sessionManager(sessionManager)
{
    if(this->sessionManager == NULL){//TODO: eliminar uso de singleton
        this->sessionManager = &SessionManager::instance();
    }
}

SessionRequestValidator::~SessionRequestValidator()
{}

std::string SessionRequestValidator::getToken(const Request &request) const
{
    return SessionRequestHelper::getToken(request);
}

SessionManager &SessionRequestValidator::getSessionManager(){
    CHECK_NOT_NULL(sessionManager);
    return *this->sessionManager;
}

const SessionManager &SessionRequestValidator::getSessionManager() const{
    CHECK_NOT_NULL(sessionManager);
    return *this->sessionManager;
}

bool SessionRequestValidator::validateRequest(calmframe::network::Request & request) const{
    if(matchNonAuthenticatedRoute(request)){
        return true;
    }
    string token = getToken(request);

    if(!token.empty()){
        return getSessionManager().validateAndUpdateSession(token);
    }

    //Reject request with empty token cookie
    return false;
}

SessionRequestValidator &SessionRequestValidator::putNonAuthenticatedRoute(const ApiRoute & route){
    this->nonAuthenticatedRoutes.push_back(route);
    std::sort(nonAuthenticatedRoutes.begin(), nonAuthenticatedRoutes.end());

    return *this;
}

bool SessionRequestValidator::matchNonAuthenticatedRoute(const Request & request) const{
    //TODO: não considerar apenas rotas iguais
    ApiRoute routeToMatch(request.getUrl(), HttpMethods::fromString(request.getMethod()));
    return std::binary_search(nonAuthenticatedRoutes.begin(), nonAuthenticatedRoutes.end(), routeToMatch);
}

}//namespace

