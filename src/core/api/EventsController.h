#ifndef EVENTSCONTROLLER_H
#define EVENTSCONTROLLER_H

#include "CollectionWebController.h"

namespace calmframe {

class EventsManager;

class EventsController : public network::CollectionWebController{
public:
    bool existMember(long id) override;

    bool onListAll(network::Request &request, network::Response &response);

protected:
    EventsManager & getManager();
};

}//namespace

#endif // EVENTSCONTROLLER_H
