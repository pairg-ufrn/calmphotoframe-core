#ifndef PHOTOCONTEXTCONTROLLER_H
#define PHOTOCONTEXTCONTROLLER_H

#include "api/CollectionWebController.h"
#include "serialization/json/JsonSerializer.h"

namespace calmframe {
    class PhotoCollectionController;

namespace api {
    class RequestHelper;
}//namespace

class PhotoContext;
class PhotoContextManager;

using ::calmframe::network::Request;
using ::calmframe::network::Response;
using ::calmframe::network::Url;

namespace PhotoContextControllerFields {
    static const char * CollectionsPath = "collections";
}//namespace

class PhotoContextWithCollaborators;

class PhotoContextController : public network::CollectionWebController
{
public:
    typedef network::CollectionWebController super;
private:
    JsonSerializer serializer;
    PhotoContextManager * contextManager;
public:
    PhotoContextController(PhotoContextManager * ctxManager=NULL);

    PhotoContextManager * getContextManager() const;
    void setContextManager(PhotoContextManager * value);

    bool onRequest(network::Request &request, network::Response &response);
    bool handleRequest(network::Url & relativeUrl, Request& request, Response& response);
    void sendGetContextResponse(Request& request, Response& response, PhotoContext& context, long userId);
protected:
    void putContextIdParameter(const network::Url & relativeUrl, Request& request);

    virtual bool existMember(long id);

    virtual bool onListAll(Request & request, Response &response);
    virtual bool onGetMember(Request &request, Response &response, long memberId, Url &relativeUrl);
    virtual bool onGetSubpathRequest(Request &request, Response &response, Url &relativeUrl);

    virtual bool onCreateMember(network::Request &request, network::Response &response);
    virtual bool onUpdateMember(Url &relativeUrl
                    , Request &request, Response &response, long memberId);
    virtual bool onDeleteMember(network::Request &request, network::Response &response
                    , long memberId);

    bool onGetContextRequest(Request & request, Response & response, Url & relativeUrl, PhotoContext & ctx);
private:
    bool onGetContextCollaborators(Request &request, Response &response, long memberId);
    bool receiveContext(api::RequestHelper & controllerHelper, PhotoContext & outReceivedCtx);
    bool receiveContext(api::RequestHelper & controllerHelper, PhotoContextWithCollaborators & out);
    void sendContextResponse(Request &request, Response &response, const PhotoContext & photoContext);
};

}//namespace
#endif // PHOTOCONTEXTCONTROLLER_H
