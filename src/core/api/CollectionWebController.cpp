#include "CollectionWebController.h"

#include "SessionRequestHelper.h"

#include "exceptions/NotFoundMember.h"
#include "exceptions/InsufficientPermissionException.h"

#include "network/Url.h"
#include "network/StatusResponses.h"
#include "network/controller/RequestRouter.h"

#include "utils/StringUtils.h"
#include "utils/StringCast.h"

#include "utils/Log.h"

namespace calmframe {
namespace network {

using namespace calmframe::utils;

using std::string;

CollectionWebController::CollectionWebController()
    : requestRouter(NULL)
{}

std::string CollectionWebController::getRootPath() const{
    return rootPath;
}

void CollectionWebController::setRootPath(const std::string & value){
    rootPath = value;
}

RequestRouter *CollectionWebController::getRouter(){
    return this->requestRouter;
}
const RequestRouter *CollectionWebController::getRouter() const{
    return this->requestRouter;
}
void CollectionWebController::setRouter(RequestRouter * router){
    this->requestRouter = router;
}

bool CollectionWebController::onRequest(Request & request, Response & response)
{
    try{
        return super::onRequest(request, response);
    }
    catch(NotFoundMember & ex){
        logDebug("Got not found member exception: %s", ex.what());
        sendNotFoundMessage(response, ex.getMessage());
        return true;
    }
    catch(const InsufficientPermissionException &){
        sendErrorMessage(response, StatusResponses::FORBIDDEN
                                    , "User does not have sufficient privileges to update this context.");
        return true;
    }
}

bool CollectionWebController::onGet(calmframe::network::Request &request, calmframe::network::Response &response){
    Url url;
    long id = -1;
    if(tryGetMemberId(request, &id, &url)){

        if(!existMember(id)){
            sendNotFoundMessage(response);
            return true;
        }

        return this->onGetMember(request, response, id, url);
    }
    else if(url.path().count() == 0){
        return onListAll(request,response);
    }
    else{//não tem id
        return onGetSubpathRequest(request, response, url);
    }
}

bool CollectionWebController::onPost(calmframe::network::Request &request, calmframe::network::Response &response){

    Url url;
    long id = -1;
    if(tryGetMemberId(request, &id, &url)){
        if(!existMember(id)){
            sendNotFoundMessage(response);
            return true;
        }
        return onUpdateMember(url, request,response,id);
    }
    else if(url.path().empty()){
        return onCreateMember(request, response);
    }
    else{
        return onPostSubpathRequest(url, request, response);
    }

    return false;
}

bool CollectionWebController::onPut(calmframe::network::Request &request, calmframe::network::Response &response){
   Url url;
   long id = tryGetMemberId(request, &url);
   if(isAValidId(id) && url.path().count() == 1){
       return onUpdateMember(url, request,response,id);
   }
   return false;
}

bool CollectionWebController::onDelete(calmframe::network::Request &request, calmframe::network::Response &response){
    Url url;
    long id = tryGetMemberId(request, &url);
    if(isAValidId(id) && url.path().count() == 1){
        return onDeleteMember(request,response,id);
    }
    else if(url.path().count() == 0){
        return onBulkDeleteRequest(request, response);
    }

    return false;
}

long CollectionWebController::tryGetMemberId(const Request &request, Url *outUrl) const{
    long id = -1;

    tryGetMemberId(request, &id, outUrl);

    return id;
}

bool CollectionWebController::tryGetMemberId(const Request &request, long *idOut, Url * outUrl) const
{
//    Url url(getRelativePath(request));
    Url url = getRelativeUrl(request);

    if(outUrl != NULL){
        *outUrl = url;
    }

    long id;
    if(StringCast::cast().tryToLong(id, url.path().get(0,string()))){
        if(idOut != NULL){
            *idOut = id;
        }
        return true;
    }
    return false;
}

bool CollectionWebController::isAValidId(long id) const{
    return id > 0;
}

Url CollectionWebController::getRelativeUrl(const Request & request) const
{
    Url url(request.getUrl());

    int rootIndex;
    for(rootIndex=url.path().count() - 1; rootIndex >= 0; --rootIndex){
        if(url.path().get(rootIndex) == getRootPath()){
            break;
        }
    }
    //Removes paths until 'root path'
    int initPathCount = (int)url.path().count();
    for(int i=0; i <= rootIndex && i < initPathCount; ++i){
        url.path().remove(0);
    }

    return url;
}

long CollectionWebController::getUserId(const Request & request){
    return SessionRequestHelper::getSessionUser(request);
}

Permission CollectionWebController::ensureUserPermission(long userId, long ctxId, const PermissionLevel & requiredPermission){
    return this->permissionsVerifier.assertUserHasPermission(userId, ctxId, requiredPermission);
}

Permission CollectionWebController::ensureUserPermission(const Request & request, long ctxId, const PermissionLevel & requiredPermission)
{
    return ensureUserPermission(getUserId(request), ctxId, requiredPermission);
}

void CollectionWebController::sendNotFoundMessage(Response &response, const string &errMessage){
    sendErrorMessage(response, StatusResponses::NOT_FOUND, errMessage);
}

void CollectionWebController::sendBadRequestMessage(Response &response, const string &errMessage){
    sendErrorMessage(response, StatusResponses::BAD_REQUEST, errMessage);
}

void CollectionWebController::sendInternalErrorMessage(Response &response, const string &errMessage){
    sendErrorMessage(response, StatusResponses::SERVER_ERROR, errMessage);
}

void CollectionWebController::sendErrorMessage(Response &response, const StatusResponse &status, const string &errMessage){
    response.setStatusLine(status);
    response.sendHeaders();
    if(errMessage.empty() == false){
        response << errMessage;
    }
}

bool CollectionWebController::forward(Request & request, Response & response, const Url & url)
{
    if(getRouter() == NULL){
        return false;
    }
    return getRouter()->forward(request, response, url);
}

bool CollectionWebController::onListAll(Request &, Response &)
{
    return false;
}

bool CollectionWebController::onGetMember(Request &, Response &, long /*memberId*/, Url &)
{
    return false;
}

bool CollectionWebController::onGetSubpathRequest(Request &, Response &, Url & /*relativeUrl*/)
{
    return false;
}

bool CollectionWebController::onCreateMember(Request &, Response &){
    return false;
}

bool CollectionWebController::onUpdateMember(Url &, Request &, Response &, long /*memberId*/)
{
    return false;
}

bool CollectionWebController::onUpdateMemberPropertyRequest(Request &, Response &
    , long /*memberId*/, const string & /*memberProperty*/)
{
    return false;
}

bool CollectionWebController
     ::onPostSubpathRequest(Url & /*relUrl*/, Request &, Response &)
{
    return false;
}

bool CollectionWebController::onBulkDeleteRequest(Request &, Response &)
{
    return false;
}

bool CollectionWebController::onDeleteMember(Request &, Response &
                                                          , long /*memberId*/)
{
    return false;
}


}//namespace
}//namespace
