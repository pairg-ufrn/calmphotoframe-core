#include "ResponseBuilder.h"

#include "network/Response.h"
#include "utils/CommonExceptions.h"

using namespace calmframe::network;

namespace calmframe {

ResponseBuilder::ResponseBuilder(network::Response * response, Serializer * serializer)
    : response(response)
    , serializer(serializer)
{}

ResponseBuilder & ResponseBuilder::prepare(const network::ContentType & contentType){
    Response & response = getResponse();
    response.setContentType(contentType);
    getSerializer().clear();

    return *this;
}

ResponseBuilder &ResponseBuilder::add(const Serializable & entity){
    getSerializer() << entity;

    return *this;
}

void ResponseBuilder::send(){
    Response & r = getResponse();
    r.sendHeaders();
    r << getSerializer();
}

Serializer &ResponseBuilder::getSerializer(){
    CHECK_NOT_NULL(serializer);
    return *serializer;
}

network::Response &ResponseBuilder::getResponse(){
    CHECK_NOT_NULL(response);
    return *response;
}

}//namespace
