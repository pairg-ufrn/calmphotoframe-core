#ifndef COLLECTIONWEBCONTROLLER_H
#define COLLECTIONWEBCONTROLLER_H

#include "network/controller/WebController.h"
#include "network/Response.h"
#include "network/Request.h"

#include "services/PermissionsVerifier.h"

#include <vector>

namespace calmframe{

class RequestRouter;
class InsufficientPermissionException;

namespace network{

class Request;
class Response;
class Url;

class CollectionWebController : public WebController{
public:
    typedef WebController super;
private:
    /** @deprecated - this should not be handled by the controller.
     * This path is used as reference to find where begins the relative path of this controller*/
    std::string rootPath;
    RequestRouter * requestRouter;
    PermissionsVerifier permissionsVerifier;
public:
    CollectionWebController();

    std::string getRootPath() const;
    void setRootPath(const std::string & value);

    RequestRouter * getRouter();
    const RequestRouter * getRouter() const;
    void setRouter(RequestRouter * router);

public: //WebController overriden methods
    virtual bool onRequest(Request &request, Response &response);
    virtual bool onGet(Request & request, Response & response);
    virtual bool onPost(Request &request, Response &response);
    virtual bool onPut(Request &request, Response &response);
    virtual bool onDelete(Request &request, Response &response);

protected: //CRUD methods
    virtual bool onListAll(network::Request & request, network::Response & response);

    virtual bool onGetMember(
                    network::Request & request, network::Response & response, long memberId, Url & relativeUrl);

    virtual bool onGetSubpathRequest(network::Request &request, network::Response &response, Url &relativeUrl);

    virtual bool onCreateMember(network::Request & request, network::Response & response);

    virtual bool onUpdateMember(Url & relativeUrl,
                    network::Request & request, network::Response & response, long memberId);

    virtual bool onUpdateMemberPropertyRequest(network::Request & request, network::Response & response
                                    , long memberId, const std::string & memberProperty);

    virtual bool onPostSubpathRequest(Url &relUrl,
                    network::Request &request, network::Response &response);

    virtual bool onBulkDeleteRequest(network::Request & request, network::Response & response);

    virtual bool onDeleteMember(
                    network::Request & request, network::Response & response, long memberId);

protected: //auxiliary methods
    virtual bool forward(network::Request & request, network::Response & response, const network::Url & url);

    /** deprecated*/
    Url getRelativeUrl(const network::Request & request) const;
    virtual long getUserId(const network::Request& request);

    /** Verify if the specified user has the required permissionLevel on the given context.
        @throws calmframe::InsufficientPermissionException if the user has not the required permission level.
    */
    virtual Permission ensureUserPermission(long userId, long ctxId, const PermissionLevel & requiredPermission);

    /** Do the same as #ensureUserPermission(long userId, long ctxId, const PermissionLevel &)
        but extracts the user id from the request object using #getUserId(const network::Request&)*/
    virtual Permission ensureUserPermission(const network::Request & request, long ctxId
        , const PermissionLevel & requiredPermission);

    virtual long tryGetMemberId(const network::Request & request, Url * outUrl=NULL) const;
    virtual bool tryGetMemberId(const network::Request & request, long * idOut, Url *outUrl=NULL) const;
    virtual bool isAValidId(long id) const;
    virtual bool existMember(long id) = 0;
protected: //auxiliary methods (send error messages)
    void sendNotFoundMessage(network::Response& response, const std::string & errMessage = std::string());

    void sendBadRequestMessage(network::Response& response, const std::string & errMessage = std::string());
    void sendInternalErrorMessage(network::Response& response, const std::string & errMessage = std::string());
    void sendErrorMessage(network::Response& response
                          , const StatusResponse & status
                          , const std::string & errMessage = std::string());
};

}//api
}//calmframe

#endif // COLLECTIONWEBCONTROLLER_H
