#include "PhotoController.h"

#include <exception>
#include <fstream>

#include "Api.h"
#include "SessionRequestHelper.h"
#include "RequestHelper.h"

#include "network/ContentType.h"
#include "network/ContentTypes.hpp"
#include "network/Url.h"
#include "network/MultipartRequestReader.h"

#include "model/image/ImageExternalStream.h"
#include "model/image/TemporaryImage.h"
#include "model/permissions/ProtectedEntity.h"

#include "services/EventsManager.h"
#include "services/SessionManager.h"

#include "serialization/json/JsonSerializer.h"

#include "utils/StringUtils.h"
#include "utils/StringCast.h"
#include "utils/FileUtils.h"
#include "utils/Log.h"

#include "exceptions/NotFoundMember.h"
#include "utils/CommonExceptions.h"
#include "InvalidRequestException.h"

namespace calmframe{
namespace api{

using namespace calmframe::network;
using namespace calmframe::utils;
using namespace std;

namespace Constants{
    static const char * IMAGE_SUB_PATH = "image";
    static const char * THUMBNAIL_SUB_PATH = "thumbnail";
    static const char URL_SEP_CHAR = '/';

namespace Default{
    static const int THUMBNAIL_SIZE = 200;
}
}

namespace Errors {
    constexpr const char * const MissingContextOrCollectionId =
        "Request require a valid collection or context id";

    static const string MissingCollectionId = string("Missing or invalid '")
                                             .append(api::parameters::PhotoCollectionId)
                                             .append("' parameter on request.");
}//namespace

PhotoController::PhotoController(PhotoManager * photoManager)
    : photoManager(photoManager)
    , multipartReader(new MultipartRequestReader())
{
    setRootPath(api::paths::photos);
}

bool PhotoController::existMember(long id){
    return photoManager->existPhoto(id);
}

bool PhotoController::onListAll(Request &req, Response &res){
    assertPhotoManagerNotNull();

    auto photos = listPhotos(req.getQueryParameter(api::parameters::query), req);

    RequestHelper reqHelper(req, res);
    reqHelper.sendResponseCollection<JsonSerializer>(api::fields::photos, photos, ContentTypes::JSON);

    return true;
}

bool PhotoController::onGetMember(Request &request, Response &response, long memberId, Url & relativeUrl){
    assertPhotoManagerNotNull();

    Photo photo = photoManager->getPhoto(memberId);
    Permission userPermission = ensureUserPermission(request, photo.getContextId(), PermissionLevel::Read);

    if(relativeUrl.path().count() > 1){
        return onGetPhotoProperty(request, response, memberId, relativeUrl.path().get(1));
    }

    bool include_permission =
        StrCast().tryToBool(request.getQueryParameter(api::parameters::include_permission), false);

    RequestHelper reqHelper(request, response);
    if(!include_permission){
        reqHelper.sendResponse<JsonSerializer>(photo,ContentTypes::JSON);
    }
    else{
        reqHelper.sendResponse<JsonSerializer>(ProtectedEntity<Photo>(userPermission, photo), ContentTypes::JSON);
    }

    return true;
}

bool PhotoController::onGetPhotoProperty(Request &, Response &response, long memberId, const string &memberProperty) const
{
    assertPhotoManagerNotNull();

    if(memberProperty == Constants::IMAGE_SUB_PATH){
        Image image = this->photoManager->getImage(memberId);
        response.sendFile(image.getImagePath());

        return true;
    }
    else if(memberProperty == Constants::THUMBNAIL_SUB_PATH){
        int thumbnailSize = Constants::Default::THUMBNAIL_SIZE;
        //TODO: permitir alterar tamanho de thumbnail

        Image image = this->photoManager->getThumbnail(memberId, thumbnailSize);
        response.sendFile(image.getImagePath());

        return true;
    }
    logInfo("PhotoController: \t" "Unknown property '%s' of member %ld", memberProperty.c_str(), memberId);
    return false;
}

bool PhotoController::onCreateMember(Request &request, Response &response){
    assertPhotoManagerNotNull();

    RequestHelper reqHelper(request, response);
    try{
        Photo photo = createPhoto(request);
        reqHelper.sendResponse<JsonSerializer>(photo, ContentTypes::JSON);
        return true;
    }
    catch(InvalidRequestException & ex){
        sendBadRequestMessage(response,ex.what());
        return true;
    }

    return false;
}

Photo PhotoController::createPhoto(Request &request) const{
    long collectionId = requireCollectionId(request);
    long userId = getSessionUser(request);

    if(request.getContentType().getType() == Types::IMAGE){
        return createPhotoFromImageContent(request, collectionId, userId);
    }
    else if(request.getContentType().getType() == Types::MULTIPART){
        return createPhotoFromMultipart(request, collectionId, userId);
    }
    else{
        throw InvalidRequestException("Invalid content type: " + request.getContentType().toString());
    }
}

Photo PhotoController::updatePhoto(Request & request, const Photo & photoToUpdate){
//    calmframe::data::DataTransaction transaction(calmframe::data::Data::instance());

    long userId = getSessionUser(request);

    return photoManager->updatePhoto(photoToUpdate, userId);
}

long PhotoController::getSessionUser(const Request& request) const{
    return SessionRequestHelper::getSessionUser(request);
}

bool PhotoController::onUpdateMember(Url &, Request &request, Response &response, long memberId)
{
    assertPhotoManagerNotNull();

    Photo photoToUpdate = this->photoManager->getPhoto(memberId);
    ensureUserPermission(request, photoToUpdate.getContextId(), PermissionLevel::Write);

    RequestHelper requestHelper(request, response);

    JsonSerializer serializer;
    //TODO: limitar tamanho do arquivo
    if(requestHelper.receiveEntity(serializer, photoToUpdate, ContentTypes::JSON)){
        Photo updatedPhoto = updatePhoto(request, photoToUpdate);

        requestHelper.sendResponse(updatedPhoto, serializer, ContentTypes::JSON);
    }

    return true;
}

bool PhotoController::onDeleteMember(Request & request, Response &response, long memberId){
    Photo photoToDelete = photoManager->getPhoto(memberId);
    ensureUserPermission(request, photoToDelete.getContextId(),PermissionLevel::Write);

    Photo photo = photoManager->deletePhoto(memberId, getSessionUser(request));

    RequestHelper requestHelper(request, response);

    if(photo.getId() != Photo::UNKNOWN_ID){
        requestHelper.sendResponse<JsonSerializer>(photo, ContentTypes::JSON);
    }
    else{
        sendNotFoundMessage(response);
    }

    return true;
}


std::vector<Photo> PhotoController::listPhotos(string querySearchParam, Request &request)
{
    std::vector<Photo> photos;

    long collId = getCollectionId(request);
    if(collectionIsValid(collId)){
        string recursiveParam = request.getQueryParameter(api::parameters::recursive);
        bool recursive = StrCast().cast().tryToBool(recursiveParam);

        long collContext = photoManager->getCollectionCtxId(collId);
        ensureUserPermission(request, collContext, PermissionLevel::Read);

        photos = photoManager->getPhotosInCollection(collId, recursive, querySearchParam);
    }
    else{
        long ctxId = getContextIdParam(request);

        if(!contextIsValid(ctxId)){
            throw InvalidRequestException(Errors::MissingContextOrCollectionId);
        }

        ensureUserPermission(request, ctxId, PermissionLevel::Read);

        //TODO: GET photo by context
        photos = photoManager->getPhotosInContext(ctxId, querySearchParam);
    }

    return photos;
}

PhotoController::MultipartReaderPtr PhotoController::getMultipartReader() const{
    return this->multipartReader;
}

void PhotoController::setMultipartReader(PhotoController::MultipartReaderPtr multipartReader){
    this->multipartReader = multipartReader;
}


void PhotoController::assertPhotoManagerNotNull() const{
    CHECK_NOT_NULL(photoManager);
}

long PhotoController::getCollectionId(Request &request) const
{
    string collIdParam = request.getParameter(api::parameters::PhotoCollectionId);
    return StrCast().tryToLong(collIdParam, -1);
}

long PhotoController::getContextIdParam(Request &request) const{
    return StrCast().tryToLong(request.getParameter(api::parameters::PhotoContextId), -1);
}

long PhotoController::requireCollectionId(Request &request) const
{
    long collectionId = getCollectionId(request);
    if(!collectionIsValid(collectionId)){
        long ctxId = getContextIdParam(request);
        if(contextIsValid(ctxId)){
            collectionId = this->photoManager->getRootPhotoCollectionId(ctxId);
        }
    }
    if(!collectionIsValid(collectionId)){
        throw InvalidRequestException(Errors::MissingCollectionId);
    }
    return collectionId;
}

bool PhotoController::collectionIsValid(long collId) const{
    return collId >= 0;
}

bool PhotoController::contextIsValid(long ctxId) const{
    return ctxId >=0;
}

Photo PhotoController::createPhotoFromImageContent(Request &request, long collectionId, long userId) const{
    //TODO: avoid double copy of image
    TemporaryImage tmpImage;
    receiveImage(&request, tmpImage);

    Photo createdPhoto = photoManager->createPhoto(tmpImage, collectionId, userId);
    tmpImage.destroy();

    return createdPhoto;
}

Photo PhotoController::createPhotoFromMultipart(Request &request, long collectionId, long userId) const{
    //TODO: avoid double copy of image
    PhotoDescription photoDescription;
    string originalFilename;
    TemporaryImage tmpImage;
    receivePhotoCreationParts(request, tmpImage, &photoDescription, &originalFilename);

    if(!tmpImage.isValid()){
        throw CommonExceptions::IOException("Could not create image file");
    }

    Photo createdPhoto = this->photoManager->createPhoto(tmpImage
                                                        , photoDescription
                                                        , originalFilename
                                                        , collectionId
                                                        , userId);

    tmpImage.destroy();

    return createdPhoto;
}

void PhotoController::receivePhotoCreationParts(Request &request, Image & tmpImage
                                                , PhotoDescription  * photoDescription, string *originalFilename) const
{
    bool receivedImage = false, receivedDescription = false;
    
    MultipartRequestReader::PartPtr part;
    
    if(!multipartReader->start(&request) 
        || !(part = multipartReader->next()))
    {
        throw InvalidRequestException("Could not read first part. Request is not a valid multipart request");
    }
    
    do{
        if(part->getContentType().getType() == Types::IMAGE){
            if(!receivedImage){
                receiveImage(part.get(), tmpImage);
                receivedImage = true;
            }
            else{
                throw InvalidRequestException("Multipart message should contain only one image part");
            }
        }
        else if(part->getContentType().is(Types::APPLICATION,SubTypes::JSON)){
            if(!receivedDescription){
                receivePhotoDescription(part.get(), photoDescription, originalFilename);
                receivedDescription = true;
            }
            else{
                throw InvalidRequestException("Multipart message should contain only one json part");
            }
        }
        else{
            std::stringstream errMessage;
            errMessage << "Invalid part Content-Type" << part->getContentType().getValue()
                       << " on Multipart message. "
                       << "Only allowed " << ContentTypes::JSON.getValue()
                       << " or " << Types::IMAGE << "/*";
            throw InvalidRequestException(errMessage.str());
        }

        part = multipartReader->next();
    } while(part);

    if(!receivedImage){
        throw InvalidRequestException("Multipart message should contain at least an image part.");
    }
}

void PhotoController::receiveImage(Request *request, Image &image) const{
    string tmpFileName = FileUtils::instance().generateTmpFilepath();
    ofstream imageTmpFile(tmpFileName.c_str());
    if(!imageTmpFile.good()){
        throw CommonExceptions::IOException("Could not create file at: \"" + tmpFileName + "\"");
    }

    imageTmpFile << request->getContent().rdbuf();
    imageTmpFile.close();

    image.setImagePath(tmpFileName);
    image.setType(request->getContentType().getSubtype());
}

void PhotoController::receivePhotoDescription(Request *part, PhotoDescription * description, string *originalFilename) const{
    JsonSerializer serializer;

    Photo tmpPhoto;
    part->getContent() >> serializer >> tmpPhoto;
    if(description != NULL){
        *description = tmpPhoto.getDescription();
    }
    if(originalFilename != NULL){
        //Apenas obtém o nome do arquivo
        GenericContainer metadata = serializer.getDeserialized().get(PhotoFields::METADATA, GenericContainer()).as<GenericContainer>();
        *originalFilename = metadata.get(PhotoMetadataFields::ORIGINALFILENAME, "").asString();
    }
}

} //namespace api
} //namespace calmframe
