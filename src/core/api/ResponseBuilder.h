#ifndef RESPONSEBUILDER_H
#define RESPONSEBUILDER_H

#include "serialization/SerializersFwd.h"
#include "serialization/Serializer.h"

#include "network/NetworkFwd.h"
#include "network/ContentType.h"

namespace calmframe {

class ResponseBuilder{
public:
    ResponseBuilder(network::Response * response, Serializer * serializer);

    ResponseBuilder & prepare(const network::ContentType & contentType);
    ResponseBuilder & add(const Serializable & entity);

    template<typename Coll>
    ResponseBuilder & addItems(const std::string & name, const Coll & items);

    void send();
public:
    Serializer & getSerializer();
    network::Response & getResponse();
protected:
private:
    network::Response * response;
    Serializer * serializer;
};

template<typename Coll>
ResponseBuilder &ResponseBuilder::addItems(const std::string & name, const Coll & items){
    serializer->putCollection(name, items);
    return *this;
}

}//namespace

#endif // RESPONSEBUILDER_H
