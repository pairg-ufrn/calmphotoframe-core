#include "SessionRequestHelper.h"

#include "Api.h"
#include "network/Headers.h"
#include "network/ParameterizedHeader.h"

#include "../services/SessionManager.h"

namespace calmframe {

using namespace std;
using namespace ::calmframe::network;

std::string SessionRequestHelper::getToken(const Request & request){
    if(request.containsHeader(Headers::Cookie)){
        string cookieHeaderValue = request.getHeaderValue(Headers::Cookie);
        ParameterizedHeader cookieHeader(Headers::Cookie, cookieHeaderValue,';','=');

        return cookieHeader.getParameter(api::cookies::sessionCookie);
    }
    else if(request.containsHeader(Headers::XToken)){
        return request.getHeaderValue(Headers::XToken);
    }

    if(request.getMethod() == "GET"){
        return request.getQueryParameter(api::parameters::token, string());
    }
    return string();
}

long SessionRequestHelper::getSessionUser(const Request & request){
    const std::string sessionToken = getToken(request);

    //TODO: maybe enforce that sessionToken exist/is valid
    return sessionToken.empty() ? User::INVALID_ID : SessionManager::instance().getSessionUser(sessionToken);
}

}//namespace
