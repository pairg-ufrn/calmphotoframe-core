#ifndef SESSIONREQUESTHELPER_H
#define SESSIONREQUESTHELPER_H

#include <string>
#include "network/Request.h"

namespace calmframe {

class SessionRequestHelper{
public:
    static std::string getToken(const network::Request & request);

    /** Return the user id associated with the given request or @code{User::INVALID_ID} if not found*/
    static long getSessionUser(const network::Request& request);
};

}//namespace

#endif // SESSIONREQUESTHELPER_H
