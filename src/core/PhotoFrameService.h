#ifndef PHOTOFRAMESERVICE_H
#define PHOTOFRAMESERVICE_H

#include <map>
#include <string>
#include <set>
#include <memory>

#include "api/PhotoFrameApi.h"
#include "services/PhotoManagersLayer.h"
#include "services/AccountManager.h"

//Forward declaring classes to reduce dependencies when building/linking the program
class CivetServer;

namespace calmframe{

class CivetWebControllerAdapter;
class Configurations;

class PhotoFrameService{
private:
    using ServerPtr = std::shared_ptr<CivetServer>;
    ServerPtr server;
    std::shared_ptr<CivetWebControllerAdapter> civetwebApiAdapter;

    PhotoManagersLayer photoManagers;
    AccountManager accountManager;
    PhotoFrameApi photoFrameApi;
public:
    PhotoFrameService();
    PhotoFrameService(const PhotoFrameService & other);
    PhotoFrameService & operator=(const PhotoFrameService &);

    bool startService();
    bool startService(const calmframe::Configurations & options);
    void stopService();
    bool isRunning() const;

    void initializeManagers();

    std::string startLocalSession(const std::string &clientSecret);

    PhotoManager & getPhotoFrame();
    const PhotoManager & getPhotoFrame() const;

    PhotoContextManager & getContextManager();
    const PhotoContextManager & getContextManager() const;
    void setupBusinessLayer(const Configurations &configurations);
protected:
    bool setupService(ServerPtr civetServer, const calmframe::Configurations & configurations);
    std::string setupDirectories(const Configurations &configurations);
    void setupApi(ServerPtr civetServer);

    ServerPtr createServer(const calmframe::Configurations & configurations);
};

}//namespace calmframe

#endif
