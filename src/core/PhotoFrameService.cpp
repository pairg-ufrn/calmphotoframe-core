#include "PhotoFrameService.h"

#include "Options.h"
#include "Configurations.h"
#include "Environment.h"

#include "data/Data.h"
#include "exceptions/DatabaseException.h"

#include "network/controller/WebController.h"
#include "services/AccountManager.h"
#include "services/SessionManager.h"

#include "utils/Log.h"
#include "utils/StringCast.h"
#include "utils/FileUtils.h"

//civetweb
#include "CivetServer.h"
#include "network/civetweb/CivetWebControllerAdapter.h"

namespace calmframe {

namespace Constants{
namespace PhotoFrameService{
    const char * OPTIONS_END = 0;
    const char * ERROR_NullPhotoFrame = "In PhotoFrameService, PhotoFrame is NULL.";
}
}

using namespace std;
using namespace calmframe;
using namespace calmframe::utils;
using namespace calmframe::network;

PhotoFrameService::PhotoFrameService()
    : server(NULL)
{
}

bool PhotoFrameService::isRunning() const{
    return this->server != NULL && this->server->getContext() != NULL;
}

void PhotoFrameService::initializeManagers()
{
    try{
        photoManagers.initialize();
    }
    catch(const std::exception & ex){
        logWarning("Could not initialize photo managers, due to exception:\n%s", ex.what());
    }
}

string PhotoFrameService::startLocalSession(const std::string & clientSecret){
    Session localSession = SessionManager::instance().startLocalSession(clientSecret);

    return localSession.getToken();
}

PhotoManager &PhotoFrameService::getPhotoFrame(){
    return this->photoManagers.getPhotoManager();
}

const PhotoManager &PhotoFrameService::getPhotoFrame() const{
    return this->photoManagers.getPhotoManager();
}

PhotoContextManager &PhotoFrameService::getContextManager(){
    return this->photoManagers.getContextManager();
}

const PhotoContextManager &PhotoFrameService::getContextManager() const{
    return this->photoManagers.getContextManager();
}

bool PhotoFrameService::startService(){
    return this->startService(Configurations::instance());
}

string getOption(const std::map<std::string, std::string >& options
        , const string & optionName,  const string & defaultValue)
{
    std::map<std::string, std::string>::const_iterator optionEntry = options.find(optionName);
    string option = (optionEntry == options.end()? defaultValue : optionEntry->second.c_str());
    return option;
}

bool PhotoFrameService::startService(const Configurations &options){
    if(isRunning()){
        logWarning("PhotoFrameService:\t" "trying to initialize service already running.");
        return true;
    }

    auto civetServer = createServer(options);

    if(civetServer->getContext() == NULL){
        civetServer.reset();
        return false;
    }
    else if(!setupService(civetServer, options)){
        stopService();
        return false;
    }
    return true;
}

void PhotoFrameService::stopService(){
    this->server.reset();
    photoFrameApi.destroy();
}

PhotoFrameService::ServerPtr PhotoFrameService::createServer(const Configurations & configurations) {
    string portsOpt     = configurations.get(Options::PORT             , Options::Defaults::PORT);
    //Opções do civetweb
    const char * optionsArray[] = {
        //TODO: desabilitar document_root (utilizando image dir por hora)
       "document_root", configurations.get(Options::IMAGE_DIR, Options::Defaults::IMAGE_DIR).c_str(),
       "enable_directory_listing", "no",
       "listening_ports", portsOpt.c_str()
        , calmframe::Constants::PhotoFrameService::OPTIONS_END
     };
    return std::make_shared<CivetServer>(optionsArray);
}

bool PhotoFrameService::setupService(ServerPtr civetServer, const Configurations &configurations){
    try{
        setupBusinessLayer(configurations);
        setupApi(civetServer);
    }
    catch(const DatabaseCreationException & ex){
        throw;    
    }
    catch(const std::exception & ex){
        logError("Could not initialize service, due to exception:\n%s", ex.what());
        return false;
    }
    return true;
}

string PhotoFrameService::setupDirectories(const Configurations &configurations){
    string imgDir = Environment::instance().getImageDir(configurations);
    string tmpDir = configurations.get(Options::TMP_DIR, Files().getDefaultTmpDir());

    logInfo("Image dir = %s", imgDir.c_str());
    logInfo("Temp dir = %s", tmpDir.c_str());

    FileUtils::instance().createDirIfNotExists(imgDir,true);
    FileUtils::instance().createDirIfNotExists(tmpDir,true);
    FileUtils::instance().setDefaultTmpDir(tmpDir);

    return imgDir;
}

void PhotoFrameService::setupBusinessLayer(const Configurations &configurations){
    string imgDir = setupDirectories(configurations);
    getPhotoFrame().setImagesDir(imgDir);
    this->accountManager.setImagesDir(Files().joinPaths(imgDir, "users"));
    this->accountManager.initialize();

    photoManagers.initialize();
}

void PhotoFrameService::setupApi(ServerPtr civetServer){
    this->server = civetServer;

    photoFrameApi.initialize(&this->photoManagers, &accountManager);
    
    civetwebApiAdapter = std::make_shared<CivetWebControllerAdapter>(photoFrameApi.getApiRoot());
    civetServer->addHandler("/", civetwebApiAdapter.get());
}

} //namespace calmframe
